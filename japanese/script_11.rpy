# game/script.rpy:6973
translate japanese choice_13_end_4f09cad4:

    toshu_t "Back to school again! That two-week vacation sure passed by quick!"
    toshu_t "再び学校に戻りましょう！その2週間の休暇は確実に素早く過ぎました！"

# game/script.rpy:6974
translate japanese choice_13_end_931bd042:

    toshu_t "A lot of things happened… Masaru and Ichiru frequently go to my house!"
    toshu_t "多くのことが起こった… MasaruとIchiruは頻繁に私の家に行く！"

# game/script.rpy:6975
translate japanese choice_13_end_e9cd51cf:

    toshu_t "We often hangout together. They surely changed a lot!"
    toshu_t "私たちはしばしば一緒にハングアウトします。彼らは確かに多くを変えた！"

# game/script.rpy:6976
translate japanese choice_13_end_17f40bb2:

    toshu_t "I think I've gotten a lot closer to them!"
    toshu_t "私は彼らにもっと近づいたと思う！"

# game/script.rpy:6985
translate japanese choice_13_end_690c16d2:

    ichiru "Yo! Toshu!"
    ichiru "Yo! Toshu!"

# game/script.rpy:6986
translate japanese choice_13_end_8559e77e:

    toshu "Wow! Ichiru, you sure are early today."
    toshu "うわー！イチール、今日は早い。"

# game/script.rpy:6988
translate japanese choice_13_end_80d0a758:

    ichiru "Of course!"
    ichiru "もちろん！"

# game/script.rpy:6990
translate japanese choice_13_end_21352924:

    saki "W-wow…!"
    saki "Wワウ…！"

# game/script.rpy:6991
translate japanese choice_13_end_42101579:

    saki "Mr. Yanai is present today? That's surprising!"
    saki "柳井さんは今日出席していますか？それは驚くべきことです！"

# game/script.rpy:6993
translate japanese choice_13_end_11d21302:

    ichiru "You're exaggerating, lady!"
    ichiru "あなたは誇張しています、女性！"

# game/script.rpy:6995
translate japanese choice_13_end_a9c5dba1:

    toshu "Sorry about that, Ms. Saki."
    toshu "申し訳ありません、咲さん。"

# game/script.rpy:6996
translate japanese choice_13_end_59170f7a:

    saki "Ah, by the way, Mr. Tadano asked me to excuse you both from class."
    saki "ああ、ちなみに忠野先生は、皆さんに授業からお互いへの言い訳をお願いしました。"

# game/script.rpy:6997
translate japanese choice_13_end_4d6acd02:

    saki "He said they're waiting for you at the baseball field."
    saki "彼は野球場であなたを待っていると言いました。"

# game/script.rpy:6999
translate japanese choice_13_end_17fc6464:

    saki "Speaking of baseball fields!"
    saki "野球場といえば！"

# game/script.rpy:7000
translate japanese choice_13_end_4b6f4c63:

    saki "Did you know that our baseball field is one of the most popular in the country?"
    saki "私たちの野球場は、この国で最も人気のある野球場の1つであることをご存知ですか？"

# game/script.rpy:7001
translate japanese choice_13_end_84c32895:

    saki "Lots of baseball teams travel a long way just to use our field!"
    saki "多くの野球チームが、私たちのフィールドを使うために長い道のりを歩いています！"

# game/script.rpy:7002
translate japanese choice_13_end_b7418639:

    saki "Interesting right?!"
    saki "面白い権利？"

# game/script.rpy:7003
translate japanese choice_13_end_54aa03cb:

    saki "But at the same time it is ironic! Because we never won any competitions so far!"
    saki "しかし、それと同時に、それは皮肉です！これまでのところ、どんな競争も勝利したことがないからです！"

# game/script.rpy:7004
translate japanese choice_13_end_94f4347a:

    saki "Ah competitions! I heard you are going to join the upcoming baseball tournament this coming spring!"
    saki "ああ競争！今度の春、来るべき野球大会に参加しようと思っています！"

# game/script.rpy:7005
translate japanese choice_13_end_459de56a:

    saki "I wish you good luck! For our schoo--"
    saki "がんばって！私たちのschooのために -"

# game/script.rpy:7007
translate japanese choice_13_end_21b34dd9:

    ichiru "Hey Lady! When are you going to stop talking?!"
    ichiru "ちょっと女性！いつ話しを止めるつもりですか？"

# game/script.rpy:7008
translate japanese choice_13_end_7ea9e5b7:

    ichiru "I thought that GENJI is looking for us!"
    ichiru "ガンジーが私たちを探していると思った！"

# game/script.rpy:7010
translate japanese choice_13_end_f52251ed:

    toshu "I-Ichiru!!"
    toshu "I-Ichiru!!"

# game/script.rpy:7012
translate japanese choice_13_end_0dbb1e19:

    saki "Oh dear, I'm terribly sorry Mr. Yanai!"
    saki "名前：ああ、私はひどく柳生ごめんなさい！"

# game/script.rpy:7014
translate japanese choice_13_end_c76ac0e4:

    toshu "Ms. Saki, I'm so sorry for Ichiru's behaviour!"
    toshu "名前：咲、私はイチールの行動のために申し訳ありません！"

# game/script.rpy:7016
translate japanese choice_13_end_7532d2ad:

    ichiru "Eh?? What did I do wrong??"
    ichiru "え？私は何を間違えたのですか？"

# game/script.rpy:7018
translate japanese choice_13_end_95580aba:

    toshu "Heyy! Ichiru! Apologize to Ms. Saki!"
    toshu "Heyy! Ichiru! Apologize to Ms. Saki!"

# game/script.rpy:7020
translate japanese choice_13_end_d108f950:

    ichiru "B-but!"
    ichiru "B-but！"

# game/script.rpy:7021
translate japanese choice_13_end_cebb1811:

    toshu "Ichiruuu!"
    toshu "Ichiruuu!"

# game/script.rpy:7022
translate japanese choice_13_end_bbb569fc:

    ichiru "Hnn! Okay, okay, Toshu!"
    ichiru "Hnn! Okay, okay, Toshu!"

# game/script.rpy:7024
translate japanese choice_13_end_c404fcfb:

    ichiru "I'm sorry Ms. Saki…"
    ichiru "ごめんなさい…"

# game/script.rpy:7025
translate japanese choice_13_end_d6139c33:

    saki "Wow… Mr. Kanada… I never see anyone--"
    saki "うわー…金田さん…私は誰も見たことがありません -"

# game/script.rpy:7027
translate japanese choice_13_end_88756a4c:

    ichiru "Don't get any ideas! I only followed Toshu!"
    ichiru "アイデアを得ないでください！私はToshuだけに従った！"

# game/script.rpy:7029
translate japanese choice_13_end_77be4afa:

    ichiru "C-come on Toshu! Let's go to Genji!"
    ichiru "C-come on Toshu! Let's go to Genji!"

# game/script.rpy:7031
translate japanese choice_13_end_9c4e832a:

    toshu "O-Okay Ichiru! See you later Ms. Saki…"
    toshu "O-Okay Ichiru! See you later Ms. Saki…"

# game/script.rpy:7036
translate japanese choice_13_end_5c99ee1c:

    saki "I'm so happy for you Ichiru. You finally made friends."
    saki "イチールにはとても嬉しいです。あなたはついに友達を作りました。"

# game/script.rpy:7045
translate japanese choice_13_end_a323f7d7:

    genji "HEY! Toshu and Ichiru!"
    genji "HEY! Toshu and Ichiru!"

# game/script.rpy:7047
translate japanese choice_13_end_60f697d4:

    toshu "Hi, Coach!"
    toshu "こんにちは、コーチ！"

# game/script.rpy:7049
translate japanese choice_13_end_08ac252c:

    ichiru "‘Sup Genji?! How's your so-called 'manhood' doing?"
    ichiru "\'元Genji！あなたのいわゆる「男らしさ」はどうやっていますか？"

# game/script.rpy:7051
translate japanese choice_13_end_6602396a:

    genji "Hngh! Even after winter break, you didn't change!"
    genji "Hngh！冬休みの後でさえ、あなたは変わらなかった！"

# game/script.rpy:7053
translate japanese choice_13_end_fef73a5a:

    ichiru "Pffff! Of course. I missed--"
    ichiru "パフ！もちろん。私は逃した -"

# game/script.rpy:7055
translate japanese choice_13_end_2a0e0560:

    genji "Missed me? Yeah I know you don't have to say it! I missed you too you little delinquent!"
    genji "私を逃した？うん、私はあなたがそれを言う必要はないことを知っている！私はあまりにもあなたが少し遅れてあなたを逃した！"

# game/script.rpy:7057
translate japanese choice_13_end_207ed6c8:

    ichiru "Shut up! I missed 'teasing' you! Don't make any assumptions you stupid old man!"
    ichiru "黙れ！私はあなたを「いじめる」ことを忘れました！あなたは愚かな老人を前提にしてはいけません！"

# game/script.rpy:7059
translate japanese choice_13_end_fdf1671d:

    genji "Gahahah!"
    genji "Gahahah!"

# game/script.rpy:7061
translate japanese choice_13_end_36804851:

    masaru "It looks pretty lively here."
    masaru "ここではかなり活発に見えます。"

# game/script.rpy:7063
translate japanese choice_13_end_d7281f8d:

    toshu "Captain Masaru!"
    toshu "キャプテンマサル！"

# game/script.rpy:7065
translate japanese choice_13_end_99e70ecd:

    genji "Oh, Masaru! Tell them the good news!"
    genji "ああ、元大！彼らに良い知らせをしてください！"

# game/script.rpy:7067
translate japanese choice_13_end_fcff3cde:

    ichiru "Good news?"
    ichiru "良いニュース？"

# game/script.rpy:7069
translate japanese choice_13_end_eff0b183:

    masaru "Yes. Coach has requested a make-up exam for me!"
    masaru "はい。コーチが私のためにメイクアップ試験をリクエストしました！"

# game/script.rpy:7071
translate japanese choice_13_end_92104702:

    masaru "And… I'm going to go for it. I'm gonna study twice as hard!"
    masaru "そして…私はそれに行くつもりです。私は2倍の勉強をします！"

# game/script.rpy:7073
translate japanese choice_13_end_2542f29e:

    ichiru "Cool!!"
    ichiru "クール！！"

# game/script.rpy:7075
translate japanese choice_13_end_baf589b4:

    masaru "Hopefully, I'll pass it this time!"
    masaru "うまくいけば、私は今度はそれを渡すでしょう！"

# game/script.rpy:7077
translate japanese choice_13_end_42625d82:

    toshu "Of course you can, Masaru!"
    toshu "もちろん、大丈夫です！"

# game/script.rpy:7079
translate japanese choice_13_end_d2d40192:

    genji "Yeah! Just do your best."
    genji "うん！最善を尽くしなさい。"

# game/script.rpy:7081
translate japanese choice_13_end_ac8808f0:

    genji "If you still have trouble, you can always go to me."
    genji "まだ問題があれば、いつでも私に行くことができます。"

# game/script.rpy:7083
translate japanese choice_13_end_f07263b1:

    genji "However, his scholarship was terminated because of that failing grade."
    genji "しかし、彼の奨学金は、その学年の不合格のために終了しました。"

# game/script.rpy:7085
translate japanese choice_13_end_6bb2d2bd:

    toshu "Wh-whaaat?!"
    toshu "名前：Wh-ワアアット?!"

# game/script.rpy:7087
translate japanese choice_13_end_4d4c876e:

    ichiru "Does that mean… Masaru will have to leave this school…?"
    ichiru "それは…マサルはこの学校を離れなければならないのだろうか？"

# game/script.rpy:7089
translate japanese choice_13_end_183d2fb5:

    genji "Do you guys seriously think I would let that happen?!"
    genji "あなたは真剣に私はそれが起こることを考えていますか？"

# game/script.rpy:7091
translate japanese choice_13_end_e0ef7ee0:

    genji "The awesome GENJI has sponsored Masaru's tuition for the remaining span of this schoolyear!"
    genji "すごいGENJIはこの学校の残りの期間のためにMasaruの授業料を後援しました！"

# game/script.rpy:7093
translate japanese choice_13_end_d1a4605a:

    toshu "WOWWWW!!!"
    toshu "WOWWWW !!!"

# game/script.rpy:7095
translate japanese choice_13_end_31e883ad:

    ichiru "H-how?!"
    ichiru "名前：どのように！"

# game/script.rpy:7097
translate japanese choice_13_end_a7fb8697:

    genji "Already forgot that your coach here has a second job?!"
    genji "あなたのコーチがここに第二の仕事を持っていることを既に忘れてしまったのですか？"

# game/script.rpy:7099
translate japanese choice_13_end_12cac2cd:

    ichiru "Oh yeah right, that preschool teacher job? What a ped--"
    ichiru "ああええ、その就学前の先生の仕事ですか？どのようなペット -"

# game/script.rpy:7101
translate japanese choice_13_end_5a39901d:

    toshu "YOU'RE REALLY AWESOME COACH GENJI!!!"
    toshu "あなたは本当にコーチなんてすごいんですか？"

# game/script.rpy:7103
translate japanese choice_13_end_ee91f51d:

    genji "Of course! I love each and every one of you like my own children!"
    genji "もちろん！私はあなた一人ひとりが自分の子供が好きです！"

# game/script.rpy:7105
translate japanese choice_13_end_547c8a53:

    masaru "I really can't express how grateful I am to Coach…"
    masaru "私は本当にコーチに感謝しています…"

# game/script.rpy:7107
translate japanese choice_13_end_98648183:

    masaru "I'll surely repay Coach somehow… someday…"
    masaru "私は確かに何とかコーチに返済するつもりです…いつか…"

# game/script.rpy:7109
translate japanese choice_13_end_9ea15420:

    genji "Don't think about that boy! I always got your back!"
    genji "その少年について考えないでください！私はいつもあなたの背中を持っています！"

# game/script.rpy:7111
translate japanese choice_13_end_ac731bf2:

    toshu "That's such awesome news!!"
    toshu "それはすごいニュースです！"

# game/script.rpy:7113
translate japanese choice_13_end_301d4317:

    ichiru "Yeah! Me and Toshu will totally help you with your studies!"
    ichiru "うん！私と東武は、あなたの研究を完全に助けます！"

# game/script.rpy:7115
translate japanese choice_13_end_79d2ed5a:

    ichiru "Oh, by the way, Ms. Hanazaki said you need us for something?"
    ichiru "ああ、ちなみに、花崎さんは、私たちに何か必要があると言いましたか？"

# game/script.rpy:7116
translate japanese choice_13_end_5d264009:

    ichiru "Why did you need us so much that you excused us from our class?"
    ichiru "なぜあなたは私たちがとても必要なので、私たちのクラスから私たちを免除したのですか？"

# game/script.rpy:7118
translate japanese choice_13_end_46742da0:

    genji "Ah! Good question Ichiru!"
    genji "ああ！いい質問Ichiru！"

# game/script.rpy:7120
translate japanese choice_13_end_f1ad1953:

    genji "We will train of course!! WHOLE DAY!"
    genji "私たちはもちろん訓練します！全日！"

# game/script.rpy:7122
translate japanese choice_13_end_8698014f:

    ichiru "OKAY, BYE!"
    ichiru "了解です。バイバイ！"

# game/script.rpy:7124
translate japanese choice_13_end_45b5ad08:

    genji "And where do you think you're going?!"
    genji "そして、あなたはどこに行くと思いますか？"

# game/script.rpy:7126
translate japanese choice_13_end_0bc3b782:

    ichiru "OW OW!! Stop pinching my cheeks!"
    ichiru "OW OW！私の頬を挟むのを止めよう！"

# game/script.rpy:7128
translate japanese choice_13_end_42b4c288:

    genji "I already got permission from the principal to excuse you from your classes!"
    genji "私はすでにあなたのクラスからあなたの言い訳をするためにプリンシパルの許可を得ました！"

# game/script.rpy:7129
translate japanese choice_13_end_327540e0:

    genji "We only have two weeks left before the tournament!"
    genji "我々はトーナメント前に2週間しか残っていない！"

# game/script.rpy:7130
translate japanese choice_13_end_7abde416:

    genji "We will work our asses off until sundown!!"
    genji "私たちは日没まで尻を切る！"

# game/script.rpy:7132
translate japanese choice_13_end_81edc321:

    ichiru "HNGH!"
    ichiru "HNGH！"

# game/script.rpy:7134
translate japanese choice_13_end_8e56d060:

    toshu "R-Really?"
    toshu "R-本当ですか？"

# game/script.rpy:7136
translate japanese choice_13_end_bef2607e:

    genji "REALLY!"
    genji "本当に！"

# game/script.rpy:7138
translate japanese choice_13_end_f67721cc:

    ichiru "Toshu!! Let's just go back to class!"
    ichiru "Toshu！クラスに戻ってみましょう！"

# game/script.rpy:7161
translate japanese choice_14_A_3ae9baa9:

    toshu "But the tournament is really close! And besides, we're excused for class already!"
    toshu "しかし、トーナメントは本当に近いです！そして、私たちはすでにクラスのために免除されています！"

# game/script.rpy:7163
translate japanese choice_14_A_0ecab706:

    ichiru "Hnghhh…"
    ichiru "Hnghhh …"

# game/script.rpy:7164
translate japanese choice_14_A_f5ceea2b:

    ichiru "Okay… fine!"
    ichiru "じゃ、いいよ！"

# game/script.rpy:7166
translate japanese choice_14_A_b5de8f99:

    genji "This is no time to be playing around! We got a tournament to win!"
    genji "これは周り遊ぶ時間ではありません！私たちは勝つためにトーナメントを持っています！"

# game/script.rpy:7167
translate japanese choice_14_A_8c63f2df:

    genji "Change into your uniforms and we'll head straight to practice!"
    genji "あなたのユニフォームに変更し、私たちはまっすぐに練習に向かいます！"

# game/script.rpy:7168
translate japanese choice_14_A_464f58d8:

    toshu "Yes, Coach!"
    toshu "はい、コーチ！"

# game/script.rpy:7173
translate japanese choice_14_B_32bd9274:

    toshu "Yeah, Ichiru, I think we should head back to class…"
    toshu "うん、Ichiru、私たちはクラスに戻るべきだと思う…"

# game/script.rpy:7174
translate japanese choice_14_B_63809e5d:

    toshu "Ms. Saki's lecture might be important for our final exams!"
    toshu "咲さんの講義は、最終試験のために重要かもしれません！"

# game/script.rpy:7176
translate japanese choice_14_B_c30e9cd5:

    genji "I already got the handouts for all the lectures you guys are gonna miss!"
    genji "私はすでにあなたが見逃しているすべての講義のための配布資料を手に入れました！"

# game/script.rpy:7177
translate japanese choice_14_B_3553a988:

    genji "There's really no excuse to be missing out this practice!"
    genji "実際にこの練習を逃してしまう言い訳はありません！"

# game/script.rpy:7179
translate japanese choice_14_B_0ecab706:

    ichiru "Hnghhh…"
    ichiru "Hnghhh …"

# game/script.rpy:7180
translate japanese choice_14_B_f5ceea2b:

    ichiru "Okay… fine!"
    ichiru "じゃ、いいよ！"

# game/script.rpy:7182
translate japanese choice_14_B_b5de8f99:

    genji "This is no time to be playing around! We got a tournament to win!"
    genji "これは周り遊ぶ時間ではありません！私たちは勝つためにトーナメントを持っています！"

# game/script.rpy:7183
translate japanese choice_14_B_8c63f2df:

    genji "Change into your uniforms and we'll head straight to practice!"
    genji "あなたのユニフォームに変更し、私たちはまっすぐに練習に向かいます！"

# game/script.rpy:7185
translate japanese choice_14_B_464f58d8:

    toshu "Yes, Coach!"
    toshu "はい、コーチ！"

# game/script.rpy:7191
translate japanese choice_14_C_c5c4b3be:

    toshu "Ichiru! I thought you won't skip classes anymore?"
    toshu "Ichiru！私はもうクラスをスキップしないと思った？"

# game/script.rpy:7193
translate japanese choice_14_C_62ab47a9:

    ichiru "But it's whole day practice… like that one from the camp… I'm so lazy…!"
    ichiru "しかし、それは一日の練習です…キャンプからのもののように…私はとても怠惰です…！"

# game/script.rpy:7195
translate japanese choice_14_C_e6e5da4c:

    genji "This is no time to be fooling around! We got a tournament to win!"
    genji "これはだまされる時間ではありません！私たちは勝つためにトーナメントを持っています！"

# game/script.rpy:7196
translate japanese choice_14_C_0ecab706:

    ichiru "Hnghhh…"
    ichiru "Hnghhh …"

# game/script.rpy:7197
translate japanese choice_14_C_f5ceea2b:

    ichiru "Okay… fine!"
    ichiru "じゃ、いいよ！"

# game/script.rpy:7199
translate japanese choice_14_C_e3da668d:

    ichiru "At least, Toshu's here with me!"
    ichiru "少なくとも、Toshuは私とここにいる！"

# game/script.rpy:7200
translate japanese choice_14_C_8c63f2df:

    genji "Change into your uniforms and we'll head straight to practice!"
    genji "あなたのユニフォームに変更し、私たちはまっすぐに練習に向かいます！"

# game/script.rpy:7201
translate japanese choice_14_C_464f58d8:

    toshu "Yes, Coach!"
    toshu "はい、コーチ！"

# game/script.rpy:7207
translate japanese choice_14_D_331935c8:

    toshu "I think we should help Masaru first with his studies…"
    toshu "私は彼の研究でMasaruを最初に助けなければならないと思います…"

# game/script.rpy:7209
translate japanese choice_14_D_e98aa98d:

    masaru "Thanks for the thought, Toshu. But I've already decided to practice for today."
    masaru "考えてくれてありがとう、Toshu。しかし、私はすでに今日のために練習することに決めました。"

# game/script.rpy:7211
translate japanese choice_14_D_c30e9cd5:

    genji "I already got the handouts for all the lectures you guys are gonna miss!"
    genji "私はすでにあなたが見逃しているすべての講義のための配布資料を手に入れました！"

# game/script.rpy:7212
translate japanese choice_14_D_3553a988:

    genji "There's really no excuse to be missing out this practice!"
    genji "実際にこの練習を逃してしまう言い訳はありません！"

# game/script.rpy:7214
translate japanese choice_14_D_0ecab706:

    ichiru "Hnghhh…"
    ichiru "Hnghhh …"

# game/script.rpy:7215
translate japanese choice_14_D_f5ceea2b:

    ichiru "Okay… fine!"
    ichiru "じゃ、いいよ！"

# game/script.rpy:7217
translate japanese choice_14_D_b5de8f99:

    genji "This is no time to be playing around! We got a tournament to win!"
    genji "これは周り遊ぶ時間ではありません！私たちは勝つためにトーナメントを持っています！"

# game/script.rpy:7218
translate japanese choice_14_D_8c63f2df:

    genji "Change into your uniforms and we'll head straight to practice!"
    genji "あなたのユニフォームに変更し、私たちはまっすぐに練習に向かいます！"

# game/script.rpy:7220
translate japanese choice_14_D_464f58d8:

    toshu "Yes, Coach!"
    toshu "はい、コーチ！"

# game/script.rpy:7239
translate japanese after_minigame_5_67f80de9:

    toshu_t "My body was aching…"
    toshu_t "私の体は痛い"

# game/script.rpy:7240
translate japanese after_minigame_5_c0d0ce60:

    toshu_t "Eventually feeling numb from exhaustion…"
    toshu_t "最終的に枯渇から麻痺を感じる…"

# game/script.rpy:7241
translate japanese after_minigame_5_ecb3b89a:

    toshu_t "Our training today was surely extra tough."
    toshu_t "今日の私たちのトレーニングは確かに厳しいものでした。"

# game/script.rpy:7251
translate japanese after_minigame_5_701e6770:

    ichiru "Ugh…"
    ichiru "うん…"

# game/script.rpy:7252
translate japanese after_minigame_5_1a725357:

    masaru "Hnn…"
    masaru "Hnn …"

# game/script.rpy:7253
translate japanese after_minigame_5_d3433662:

    toshu "I'm so exhausted!"
    toshu "私はとても疲れている！"

# game/script.rpy:7255
translate japanese after_minigame_5_fb5c7e54:

    genji "That's it for today!"
    genji "それは今日のそれです！"

# game/script.rpy:7256
translate japanese after_minigame_5_bab889b2:

    genji "Tomorrow, you will have another beating!"
    genji "明日、別の殴打をするでしょう！"

# game/script.rpy:7258
translate japanese after_minigame_5_ef2ff0f4:

    ichiru "Give us a break old man!!"
    ichiru "私たちに休憩の老人を与える!!"

# game/script.rpy:7260
translate japanese after_minigame_5_2a1fb0e1:

    genji "I can't, especially now that the competition is nearing!"
    genji "私はできません、特に競争が近づいている今！"

# game/script.rpy:7262
translate japanese after_minigame_5_8f0666a6:

    genji "You guys should be full of motivation right now!"
    genji "あなたたちは今、モチベーションがいっぱいでなければなりません！"

# game/script.rpy:7264
translate japanese after_minigame_5_27d952c1:

    genji "This club's future depends on that tournament! NO PRESSURE!"
    genji "このクラブの未来はそのトーナメントに依存します！プレッシャーはない！"

# game/script.rpy:7266
translate japanese after_minigame_5_419c0495:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:7268
translate japanese after_minigame_5_6fce9768:

    toshu "Now that Coach has mentioned it…"
    toshu "今コーチがそれを言いました…"

# game/script.rpy:7270
translate japanese after_minigame_5_9be57e4e:

    # toshu "Captain Masaru? What happened with you and the music club?"
    toshu "キャプテン、ミュージッククラブはどうなったんでしょうか？"

# game/script.rpy:7272
translate japanese after_minigame_5_4b4a23f2:

    # ichiru "Yeah, actually, I was thinking about that as well."
    ichiru "ああ、俺も気になってたんだよな。"

# game/script.rpy:7274
translate japanese after_minigame_5_5ed38ba9:

    # masaru "Well…"
    masaru "ああ…"

# game/script.rpy:7276
translate japanese after_minigame_5_a19e362b:

    # masaru "Unfortunately… I got kicked out of it because of my failing grades…"
    masaru "残念ながら…成績が落ちたせいで休部することになったんだ…"

# game/script.rpy:7277
translate japanese after_minigame_5_fefa70ce:

    # masaru "They said, I should focus more on my studies."
    masaru "お前はもっと勉強に専念しろってさ。"

# game/script.rpy:7279
translate japanese after_minigame_5_57805543:

    masaru "And they thought that my co-curricular activities are hindering my studies."
    masaru "そして彼らは、私の同僚の活動が私の研究を妨げていると思っていました。"

# game/script.rpy:7281
translate japanese after_minigame_5_0ddd115b:

    # ichiru "It's hard to be in two clubs in the first place…"
    ichiru "そもそも２つのクラブを掛け持ちするのは大変だよな…"

# game/script.rpy:7283
translate japanese after_minigame_5_0f96f352:

    # masaru "I can only go back to the music club after I pass my exams."
    masaru "試験に合格しないとミュージッククラブに戻れないんだ。"

# game/script.rpy:7285
translate japanese after_minigame_5_0d376af9:

    # masaru "I love music. It's very important for me."
    masaru "俺は音楽が好きだ。俺にとって無くてはならないものなんだ。"

# game/script.rpy:7286
translate japanese after_minigame_5_6c90a232:

    ichiru "Well, you gotta choose between baseball and music. It's hard to handle two clubs at once."
    ichiru "まあ、あなたは野球と音楽のどちらかを選ぶ必要があります。一度に2つのクラブを扱うのは難しいです。"

# game/script.rpy:7287
translate japanese after_minigame_5_1ab74b13:

    ichiru "But what's more important right now is to really excel with your studies."
    ichiru "しかし、今重要なのはあなたの研究で本当に優れていることです。"

# game/script.rpy:7289
translate japanese after_minigame_5_f2f0edcb:

    ichiru "For a normal person it IS hard to handle it."
    ichiru "普通の人にとっては、それを扱うことは難しいです。"

# game/script.rpy:7291
translate japanese after_minigame_5_2516dac1:

    # masaru "If my mom were here she would be pretty disappointed…"
    masaru "母さんが知ったらガッカリするだろうな…"

# game/script.rpy:7293
translate japanese after_minigame_5_4c75826f:

    # toshu "Don't think like that!"
    toshu "そんな風に考えないで下さい！"

# game/script.rpy:7295
translate japanese after_minigame_5_06c3c791:

    genji "Hey! We won't make any progress if we don't want to move forward!"
    genji "ねえ！私たちが前進したくなければ、進歩はしません！"

# game/script.rpy:7297
translate japanese after_minigame_5_5ad2cf21:

    genji "MASARU! I will allow you to not attend the practice tomorrow!"
    genji "MASARU！私はあなたが明日の練習に出席できないようにします！"

# game/script.rpy:7299
translate japanese after_minigame_5_115fe9ca:

    genji "In one condition!"
    genji "ある条件で！"

# game/script.rpy:7300
translate japanese after_minigame_5_d823e23b:

    genji "If you promise me to study your ass off all day!"
    genji "あなたが一日中あなたのお尻を勉強することを約束したら！"

# game/script.rpy:7302
translate japanese after_minigame_5_63e18cd4:

    ichiru "Hnn! Heyy not fair! We have to study too!"
    ichiru "Hnn！ちょっと公正じゃない！私たちも勉強しなければなりません！"

# game/script.rpy:7304
translate japanese after_minigame_5_8ba57a8d:

    # ichiru "How come Masaru get special treatment?"
    ichiru "なんでMasaruだけ特別扱いなんだよ？"

# game/script.rpy:7305
translate japanese after_minigame_5_8f9c9f83:

    ichiru "How come he won't have to go through that gruesome training?!"
    ichiru "どのように彼はそのぞっとする訓練を通過する必要はありませんか？"

# game/script.rpy:7307
translate japanese after_minigame_5_133c39da:

    genji "Ichiru! Masaru needs it! Be sensitive with your friends!"
    genji "Ichiru！マサルはそれが必要です！あなたの友人に敏感になりなさい！"

# game/script.rpy:7309
translate japanese after_minigame_5_24b49115:

    # ichiru "Ahh…"
    ichiru "ああ…"

# game/script.rpy:7311
translate japanese after_minigame_5_e17633c4:

    genji "I'm trying my best to make all of you guys in top condition when we play for the tournament."
    genji "私はトーナメントのためにプレーするときに皆さんをトップコンディションにするために全力を尽くしています。"

# game/script.rpy:7313
translate japanese after_minigame_5_f415482d:

    genji "Because… I care about you all!"
    genji "なぜなら…私は皆あなたを気にしています！"

# game/script.rpy:7315
translate japanese after_minigame_5_28c11976:

    ichiru "I'm sorry, Coach…"
    ichiru "すみません、コーチ…"

# game/script.rpy:7317
translate japanese after_minigame_5_31dd79e4:

    genji "Okay, Masaru! I expect you to study tomorrow, okay?"
    genji "さて、大丈夫！私はあなたが明日勉強してくれることを期待しています。"

# game/script.rpy:7319
translate japanese after_minigame_5_61763abf:

    genji "Pass that makeup exam and make me proud!"
    genji "そのメイクアップ試験に合格し、私を誇りに思うようにしてください！"

# game/script.rpy:7321
translate japanese after_minigame_5_a4f739e9:

    # masaru "Thank you Coach! I will!"
    masaru "ありがとうございますコーチ！ 約束します！"

# game/script.rpy:7322
translate japanese after_minigame_5_1ab71bbe:

    # genji "Alright! Get changed! I'll see you guys tomorrow!"
    genji "よし！ お前らもう着替えろ！ また明日な！"

# game/script.rpy:7331
translate japanese after_minigame_5_c3a04dc1:

    # ichiru "Fuwaaaahhh! I wanna go hooommeee."
    ichiru "ふわあああぁぁぁ！ 早く帰りてええぇ。"

# game/script.rpy:7332
translate japanese after_minigame_5_894ff0f7:

    # toshu "Me too…! I'm so so so tired…!"
    toshu "僕もだよ…！ めちゃくちゃ疲れた…！"

# game/script.rpy:7333
translate japanese after_minigame_5_84cdf2c9:

    ichiru "And the thought that we're gonna be like this again, tomorrow…"
    ichiru "そして明日、私たちは再びこのようになると思っています…"

# game/script.rpy:7334
translate japanese after_minigame_5_24b49115_1:

    # ichiru "Ahh…"
    ichiru "ああ…"

# game/script.rpy:7336
translate japanese after_minigame_5_9a95221f:

    # masaru "I'm sorry… I won't be able to accompany you guys…"
    masaru "ゴメンな… 練習に参加できなくて…"

# game/script.rpy:7338
translate japanese after_minigame_5_248d464f:

    # toshu "Eh? Don't worry about it! We understand!"
    toshu "えっ？ 大丈夫ですよ！ 分かってますから！"

# game/script.rpy:7340
translate japanese after_minigame_5_b38c57bd:

    # masaru "I have two exams coming up this week…"
    masaru "今週試験が２つあるんだ…"

# game/script.rpy:7342
translate japanese after_minigame_5_20a77b78:

    # masaru "Ughh…"
    masaru "ううう…"

# game/script.rpy:7343
translate japanese after_minigame_5_732e4a03:

    # masaru "Why am I not a genius like Ichiru?"
    masaru "Ichiruみたいに頭が良かったらなあ。"

# game/script.rpy:7344
translate japanese after_minigame_5_37ef61d8:

    # toshu_t "I should say something to cheer him up…"
    toshu_t "何か言って励まさないと…"

# game/script.rpy:7368
translate japanese choice_15_A_5ad36a2d:

    toshu "You can do it! If Ichiru and I can do it, then you should as well!"
    toshu "あなたはそれをすることができます！イチールと私がそれをやることができれば、あなたもそうするべきです！"

# game/script.rpy:7370
translate japanese choice_15_A_44b05394:

    ichiru "Yeah! You don't have to be a genius to be able to pass an exam."
    ichiru "うん！試験に合格するには、天才である必要はありません。"

# game/script.rpy:7372
translate japanese choice_15_A_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:7374
translate japanese choice_15_A_6fa0b016:

    masaru "You're right… sulking about it won't solve anything…"
    masaru "あなたは正しいと思うが、それについては何も解決しないだろう…"

# game/script.rpy:7376
translate japanese choice_15_A_1eea2e95:

    masaru "I should really just do my best!"
    masaru "私は本当にベストを尽くすべきです！"

# game/script.rpy:7378
translate japanese choice_15_A_f9d806aa:

    toshu "We are always here to help!"
    toshu "私たちはいつも助けに来ています！"

# game/script.rpy:7380
translate japanese choice_15_A_6a2828e5:

    ichiru "That's right!"
    ichiru "そのとおり！"

# game/script.rpy:7382
translate japanese choice_15_A_c088a95b:

    # masaru "Thanks guys…"
    masaru "みんなありがとう…"

# game/script.rpy:7384
translate japanese choice_15_A_26c3056f:

    masaru "Especially you, Toshu! You really know what to say to cheer me up!"
    masaru "特にあなた、Toshu！あなたは本当に私を応援する言い方を知っています！"

# game/script.rpy:7390
translate japanese choice_15_B_1ecf4af7:

    toshu "The Masaru we know does not give up!"
    toshu "私達が知っている優はあきらめない！"

# game/script.rpy:7392
translate japanese choice_15_B_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:7394
translate japanese choice_15_B_6fa0b016:

    masaru "You're right… sulking about it won't solve anything…"
    masaru "あなたは正しいと思うが、それについては何も解決しないだろう…"

# game/script.rpy:7396
translate japanese choice_15_B_1eea2e95:

    masaru "I should really just do my best!"
    masaru "私は本当にベストを尽くすべきです！"

# game/script.rpy:7398
translate japanese choice_15_B_f9d806aa:

    toshu "We are always here to help!"
    toshu "私たちはいつも助けに来ています！"

# game/script.rpy:7400
translate japanese choice_15_B_6a2828e5:

    ichiru "That's right!"
    ichiru "そのとおり！"

# game/script.rpy:7402
translate japanese choice_15_B_c088a95b:

    # masaru "Thanks guys…"
    masaru "みんなありがとう…"

# game/script.rpy:7404
translate japanese choice_15_B_26c3056f:

    masaru "Especially you, Toshu! You really know what to say to cheer me up!"
    masaru "特にあなた、Toshu！あなたは本当に私を応援する言い方を知っています！"

# game/script.rpy:7409
translate japanese choice_15_C_5e27484b:

    toshu "Everyone has something they're good with and something they're not!"
    toshu "誰もが彼らが良いと何かを持っていない何かがあります！"

# game/script.rpy:7410
translate japanese choice_15_C_dcd79b1a:

    toshu "Don't compare yourself!"
    toshu "自分自身を比較しないでください！"

# game/script.rpy:7412
translate japanese choice_15_C_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:7414
translate japanese choice_15_C_4be744de:

    masaru "You're right… Sulking about it won't solve anything…"
    masaru "あなたは正しい…それについては何も解決しません…"

# game/script.rpy:7416
translate japanese choice_15_C_1eea2e95:

    masaru "I should really just do my best!"
    masaru "私は本当にベストを尽くすべきです！"

# game/script.rpy:7421
translate japanese choice_15_D_35f7596d:

    toshu "At least you're smoking hot! Especially in that jockstrap!"
    toshu "少なくともあなたは熱く煙っています！特にそのジョックストラップで！"

# game/script.rpy:7423
translate japanese choice_15_D_61d9179c:

    ichiru "H-hey! Am I not HOT too?!"
    ichiru "H-ねえ！私はあまりにも暑いですか？"

# game/script.rpy:7425
translate japanese choice_15_D_787123ca:

    masaru "Hahahaha!!"
    masaru "ハハハハ!!"

# game/script.rpy:7426
translate japanese choice_15_D_70e51e2f:

    masaru "Thanks for making me laugh, Toshu!"
    masaru "私を笑わせてくれてありがとう、Toshu！"

# game/script.rpy:7428
translate japanese choice_15_D_40a0cf1a:

    masaru "You really know what to say to cheer me up!"
    masaru "あなたは本当に私を応援する言い方を知っています！"
