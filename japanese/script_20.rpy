# game/script.rpy:9930
translate japanese ending_ibe_mne_4760d0aa:

    toshu_t "The weekend has passed after that intense tournament."
    toshu_t "その激しいトーナメントの後、週末が過ぎました。"

# game/script.rpy:9931
translate japanese ending_ibe_mne_1aa1bffb:

    toshu_t "I want to know how my friends have been doing…"
    toshu_t "私の友達がどうしているのか知りたい"

# game/script.rpy:9939
translate japanese ending_ibe_mne_2d78b941:

    toshu "I… it's pretty quiet around…"
    toshu "私は…周りはかなり静かです…"

# game/script.rpy:9941
translate japanese ending_ibe_mne_b65efb71:

    toshu "It totally feels different without Ichiru or Masaru…"
    toshu "イチールやマサルなしで全く違う感じ…"

# game/script.rpy:9942
translate japanese ending_ibe_mne_548b7b6a:

    toshu "Where is everyone…?"
    toshu "みんなはどこですか…？"

# game/script.rpy:9944
translate japanese ending_ibe_mne_adf24c14:

    random "HEY, YOU!"
    random "ねえ！"

# game/script.rpy:9946
translate japanese ending_ibe_mne_f65923fa:

    tomoka "You're that twerp that Ichiru always mention about right?"
    tomoka "あなたはIchiruが常に正しいことを言及するtwerpですか？"

# game/script.rpy:9948
translate japanese ending_ibe_mne_730389cc:

    toshu "Tomoka?"
    toshu "Tomoka?"

# game/script.rpy:9950
translate japanese ending_ibe_mne_4d014980:

    tomoka "That's PRINCESS Saji to you!"
    tomoka "それはあなたに君臨するサジです！"

# game/script.rpy:9952
translate japanese ending_ibe_mne_3a2718b4:

    toshu "W-what happened with Ichiru?"
    toshu "イチールに何が起こったの？"

# game/script.rpy:9954
translate japanese ending_ibe_mne_22967a3d:

    tomoka "Oh, You didn't know?"
    tomoka "ああ、あなたは知らなかった？"

# game/script.rpy:9957
translate japanese ending_ibe_mne_951ac901:

    tomoka "He already transferred away from this dump!"
    tomoka "彼はすでにこのダンプから移った！"

# game/script.rpy:9958
translate japanese ending_ibe_mne_df59d8b3:

    tomoka "He's with me now~"
    tomoka "彼は今私と一緒だ〜"

# game/script.rpy:9960
translate japanese ending_ibe_mne_686ed303:

    tomoka "HOHOHOHOHO~!!"
    tomoka "ホホロホロ〜!!"

# game/script.rpy:9962
translate japanese ending_ibe_mne_9468668c:

    toshu "Wh-What!!??? What do you mean??!"
    toshu "Whーホワット!!どういう意味ですか？？！"

# game/script.rpy:9963
translate japanese ending_ibe_mne_9f21ec60:

    toshu "I thought he's gonna--"
    toshu "私は彼が〜と思った。"

# game/script.rpy:9965
translate japanese ending_ibe_mne_a0681fa2:

    tomoka "SHUT UP!"
    tomoka "黙れ！"

# game/script.rpy:9967
translate japanese ending_ibe_mne_6ffac97d:

    tomoka "My dad couldn't care less about what happens in that shitty tournament!"
    tomoka "私のお父さんは、そのくそったトーナメントで何が起こるかについてはあまり気にすることができませんでした！"

# game/script.rpy:9968
translate japanese ending_ibe_mne_6cd674c5:

    tomoka "He will do whatever as I please~"
    tomoka "彼は私が気に入っているように何でもするだろう〜"

# game/script.rpy:9970
translate japanese ending_ibe_mne_451ee9a1:

    toshu "I… I don't believe you!"
    toshu "私は…あなたを信じていない！"

# game/script.rpy:9972
translate japanese ending_ibe_mne_fe7da577:

    tomoka "Would you stop living in your make-believe worthless-world, you ignorant fool!"
    tomoka "あなたは無益な世界を作る、あなたは無知な愚か者を作ることをやめようと思いますか？"

# game/script.rpy:9974
translate japanese ending_ibe_mne_acddb379:

    tomoka "Oh! Dear me, what am I doing?"
    tomoka "ああ！親愛なる私、私は何をしていますか？"

# game/script.rpy:9976
translate japanese ending_ibe_mne_bcb65f18:

    tomoka "I was just filing my dear Ichiru's transfer papers and I stumbled upon this trash!"
    tomoka "私はちょうど私の親愛なるIchiruの転送用紙を提出していたと私はこのゴミにつまずいた！"

# game/script.rpy:9977
translate japanese ending_ibe_mne_af0c4b35:

    tomoka "Hmph! I'm have other important things to do!"
    tomoka "Hmph！私は他にも重要なことをしています！"

# game/script.rpy:9979
translate japanese ending_ibe_mne_cf008a72:

    tomoka "Good bye, TRASH!"
    tomoka "さようなら、TRASH！"

# game/script.rpy:9981
translate japanese ending_ibe_mne_53ca8cac:

    tomoka "And by the way, I can't believe Ichiru called you friend! It makes me so sick!"
    tomoka "ところで、私はIchiruがあなたを友人と呼ぶとは信じられません！それは私がとても病気になります！"

# game/script.rpy:9983
translate japanese ending_ibe_mne_f272b826:

    tomoka "HOHOHOHOOHOH~! WAHOHOHOHOH~!!!"
    tomoka "ホホロホアホ〜！ワハ！"

# game/script.rpy:9985
translate japanese ending_ibe_mne_8d32682d:

    toshu_t "I didn't believe what Tomoka said. Not even one bit…"
    toshu_t "私はTomokaが言ったことを信じていませんでした。 1ビットでも…"

# game/script.rpy:9986
translate japanese ending_ibe_mne_4fe85f52:

    toshu_t "Ichiru… my dear friend…"
    toshu_t "Ichiru …親愛なる友人…"

# game/script.rpy:9987
translate japanese ending_ibe_mne_381aa84f:

    toshu_t "Would he really leave… without saying goodbye?"
    toshu_t "彼は本当にお別れを言わずに去るだろうか？"

# game/script.rpy:9995
translate japanese ending_ibe_mne_aa5abefd:

    masaru "Toshu…!"
    masaru "Toshu…!"

# game/script.rpy:9996
translate japanese ending_ibe_mne_5be77f88:

    toshu "Masaru! Is it true that Ichiru will leave?!"
    toshu "マサル！ Ichiruが残すのは本当ですか？"

# game/script.rpy:9998
translate japanese ending_ibe_mne_729d2365:

    toshu "Please tell me that what Tomoka's saying is a lie!"
    toshu "トモカが言っていることは嘘だと教えてください！"

# game/script.rpy:9999
translate japanese ending_ibe_mne_edf48b66:

    toshu "Is it true that Ichiru already left?!"
    toshu "Ichiruはすでに残っていることは本当ですか？"

# game/script.rpy:10001
translate japanese ending_ibe_mne_be0c99b0:

    masaru "I'm sorry, Toshu…"
    masaru "I'm sorry, Toshu…"

# game/script.rpy:10002
translate japanese ending_ibe_mne_ed718247:

    masaru "But I'm afraid she's telling the truth."
    masaru "しかし、彼女は真実を伝えているのではないかと心配しています。"

# game/script.rpy:10004
translate japanese ending_ibe_mne_c5264692:

    masaru "What's worse is…"
    masaru "悪いのは…"

# game/script.rpy:10006
translate japanese ending_ibe_mne_69c8e003:

    masaru "I have to leave as well…"
    masaru "私も一緒に出発しなければならない…"

# game/script.rpy:10008
translate japanese ending_ibe_mne_306fd507:

    toshu "W-wha…? Why? Why you too?!!"
    toshu "W-ワ？…？どうして？あなたもなぜですか？"

# game/script.rpy:10009
translate japanese ending_ibe_mne_3c952e7c:

    masaru "My scholarship got completely terminated because I failed my exam… again."
    masaru "私の奨学金は私の試験に失敗したために完全に終了しました…再び。"

# game/script.rpy:10011
translate japanese ending_ibe_mne_841f8f36:

    masaru "I don't have enough resources to continue studying here…"
    masaru "私はここで勉強を続けるのに十分なリソースがありません…"

# game/script.rpy:10013
translate japanese ending_ibe_mne_fa1d52e7:

    genji "I really don't know what to do…"
    genji "私は本当に何をすべきかわからない…"

# game/script.rpy:10015
translate japanese ending_ibe_mne_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:10017
translate japanese ending_ibe_mne_fa4a54a0:

    masaru "The music club didn't accept me back either…"
    masaru "音楽クラブは私を受け入れなかった…"

# game/script.rpy:10019
translate japanese ending_ibe_mne_1b1c820b:

    masaru "I will have to find a part time job first…"
    masaru "私はパートタイムの仕事を最初に見つけなければなりません…"

# game/script.rpy:10021
translate japanese ending_ibe_mne_a9a688b8:

    masaru "I want to save money as soon as I can to be able to finish my studies…"
    masaru "私は私の研究を終えることができるようになるとすぐにお金を節約したいです…"

# game/script.rpy:10023
translate japanese ending_ibe_mne_e1e5086d:

    toshu "I…"
    toshu "私…"

# game/script.rpy:10027
translate japanese ending_ibe_mne_e67d0f7e:

    toshu "It's not fair!"
    toshu "それは公正ではない！"

# game/script.rpy:10028
translate japanese ending_ibe_mne_95445740:

    toshu "Why do you two have to leave?!"
    toshu "なぜあなたは2つを残す必要がありますか？"

# game/script.rpy:10029
translate japanese ending_ibe_mne_12ce9a9e:

    masaru "I'm really sorry, Toshu."
    masaru "私は本当に申し訳ありません、Toshu。"

# game/script.rpy:10030
translate japanese ending_ibe_mne_1bd1a137:

    toshu "After all the times we spent together!"
    toshu "私たちは一緒に過ごしたすべての時間の後に！"

# game/script.rpy:10031
translate japanese ending_ibe_mne_03c5e25e:

    toshu "I'm such a useless friend!"
    toshu "私はそのような無駄な友人です！"

# game/script.rpy:10032
translate japanese ending_ibe_mne_8dea0d28:

    toshu "I couldn't do anything to prevent this from happening!"
    toshu "私はこれを防ぐために何もできませんでした！"

# game/script.rpy:10034
translate japanese ending_ibe_mne_553f0905:

    toshu_t "The tears from my eyes refused to stop from falling…"
    toshu_t "私の目から涙が落ちるのを止めなかった…"

# game/script.rpy:10035
translate japanese ending_ibe_mne_ff490676:

    toshu_t "I couldn't describe the sadness I felt…"
    toshu_t "私が感じた悲しみを説明できませんでした…"

# game/script.rpy:10036
translate japanese ending_ibe_mne_439f849b:

    toshu_t "I've always said to my friends…"
    toshu_t "私はいつも私の友達に言った…"

# game/script.rpy:10037
translate japanese ending_ibe_mne_3086fc3a:

    toshu_t "To never give up even when things goes bad…"
    toshu_t "物事が悪くなっても決してあきらめないために…"

# game/script.rpy:10038
translate japanese ending_ibe_mne_abb3df94:

    toshu_t "But it looks like… this time…"
    toshu_t "しかし、今のように見える…"

# game/script.rpy:10039
translate japanese ending_ibe_mne_086b4b99:

    toshu_t "I am the one… who will break down…"
    toshu_t "私はその人です…誰が倒れるでしょう…"
