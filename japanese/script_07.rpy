# game/script.rpy:2941
translate japanese after_minigame_4_16246775:

    # genji "WAAAAAAAKE UUUUUUUP!!!"
    genji "起きろおおおぉぉぉっ！！！"

# game/script.rpy:2950
translate japanese after_minigame_4_9adee6ba:

    # masaru "What is it? So early in the morning…"
    masaru "何ですか？ こんな朝早く…"

# game/script.rpy:2951
translate japanese after_minigame_4_348a0ab5:

    masaru "Toshu and Ichiru is still asleep…"
    masaru "ToshuとIchiruはまだ眠ってますよ…"

# game/script.rpy:2952
translate japanese after_minigame_4_45d23af9:

    # genji "WAKE UP YOU LAZY BRATS!!"
    genji "寝坊助ども起きろ！！"

# game/script.rpy:2954
translate japanese after_minigame_4_b4783142:

    # toshu "Hmmmnnn…!"
    toshu "うーん…！"

# game/script.rpy:2956
translate japanese after_minigame_4_6f305840:

    # ichiru "What is it Genji…?"
    ichiru "何だよGenji…？"

# game/script.rpy:2958
translate japanese after_minigame_4_c83fe4aa:

    # genji "COACH GENJI!"
    genji "Genjiコーチだ！"

# game/script.rpy:2959
translate japanese after_minigame_4_972ffa81:

    # ichiru "Yeah, yeah, Coach Genji…"
    ichiru "は、はい。Genjiコーチ…"

# game/script.rpy:2961
translate japanese after_minigame_4_5642bd9f:

    ichiru "Wait! It's like 4:00 in the morning!! Who wakes up at freaking 4:00?!"
    ichiru "待つ！それは朝の4時のようなものです！誰が4時に夢中になるの？"

# game/script.rpy:2963
translate japanese after_minigame_4_d24be98a:

    genji "C'mon guys I'm gonna make an important announcement!"
    genji "私は大事な発表をするつもりだよ！"

# game/script.rpy:2964
translate japanese after_minigame_4_f6e96f3c:

    genji "Pack your things up! All your teammates are already waiting outside."
    genji "あなたのものを詰める！すべてのチームメイトはすでに外で待っています。"

# game/script.rpy:2967
translate japanese after_minigame_4_1e8f6c6e:

    ichiru "Hngghh… I'm still sleepy!"
    ichiru "Hngghh …私はまだ眠い！"

# game/script.rpy:2968
translate japanese after_minigame_4_12d4b439:

    ichiru "He must be plotting something again…"
    ichiru "彼は再び何かをプロットしなければならない…"

# game/script.rpy:2970
translate japanese after_minigame_4_36d38437:

    masaru "Let's just go guys, looks like Coach has something important to say."
    masaru "コーチは何か重要なことを言うように見えるみたいだ。"

# game/script.rpy:2980
translate japanese after_minigame_4_b80a3f74:

    # genji "EVERYONE LISTEN UP!"
    genji "みんな聞いてくれ！"

# game/script.rpy:2982
translate japanese after_minigame_4_801b9f66:

    ichiru "He looks rather excited."
    ichiru "彼はかなり興奮しているようだ。"

# game/script.rpy:2984
translate japanese after_minigame_4_4ae34b39:

    genji "Okay, I know you all had a rough time yesterday…"
    genji "さて、昨日皆さんみたいな時間があったのを知っています…"

# game/script.rpy:2986
translate japanese after_minigame_4_431c68e2:

    genji "So I decided to make this day EVEN HARDER! HAHAHA!!!"
    genji "だから私はこの日をもっと厳しくすることに決めました！ははは！！！"

# game/script.rpy:2989
translate japanese after_minigame_4_20274b56:

    # ichiru "Whaaat?!"
    ichiru "何だって？！"

# game/script.rpy:2991
translate japanese after_minigame_4_bed4f50e:

    # genji "We're going on an exciting adventure today!"
    genji "今日はこれからエキサイティングな冒険に出発だ！"

# game/script.rpy:2992
translate japanese after_minigame_4_b78b465b:

    genji "Provided with a map, everyone has to go around the forest surrounding the mountain."
    genji "地図があれば、誰もが山を囲む森を回らなければならない。"

# game/script.rpy:2993
translate japanese after_minigame_4_f35bf709:

    genji "We'll all meet here back at camp. Just follow the map and going around the forest will be an easy task!"
    genji "私たちはすべてここでキャンプに戻って会います。ちょうど地図に従って、フォレストの周りを移動するのは簡単な作業になります！"

# game/script.rpy:2995
translate japanese after_minigame_4_cf787be8:

    toshu "WOOOOWWW!!!"
    toshu "WOOOOWWW !!!"

# game/script.rpy:2996
translate japanese after_minigame_4_84a39d69:

    genji "This is also part of your training to strengthen your stamina, endurance and most of all, teamwork."
    genji "これはあなたのスタミナ、持久力、そして何よりもチームワークを強化するためのトレーニングの一環です。"

# game/script.rpy:2998
translate japanese after_minigame_4_7dc1450b:

    # masaru "Teamwork?"
    masaru "チームワーク？"

# game/script.rpy:3000
translate japanese after_minigame_4_d49c298d:

    genji "Of course! You won't venture in the woods alone."
    genji "もちろん！あなたは森の中だけで冒険しません。"

# game/script.rpy:3002
translate japanese after_minigame_4_22688b60:

    genji "Each of you can choose who you want to be paired up with."
    genji "それぞれの人は、ペアリングをしたい相手を選ぶことができます。"

# game/script.rpy:3004
translate japanese after_minigame_4_d38fc717:

    ichiru "This is starting to sound great!"
    ichiru "これは素晴らしい音を出し始めています！"

# game/script.rpy:3006
translate japanese after_minigame_4_a4042011:

    genji "Yes I'm great like that, I know haha!!"
    genji "はい、私はそれのように素晴らしいです、私は笑を知っている！"

# game/script.rpy:3008
translate japanese after_minigame_4_a883a77e:

    ichiru "I meant the activity not you--"
    ichiru "私はあなたの活動ではないことを意味しました -"

# game/script.rpy:3010
translate japanese after_minigame_4_041fe842:

    genji "ANYWAY, I prepared a special prize for the first pair to make it back here at the camp!"
    genji "とにかく、私はキャンプでここに戻ってくるために最初のペアのための特別な賞を準備しました！"

# game/script.rpy:3012
translate japanese after_minigame_4_39627771:

    toshu "Waaahhh!! A PRIZE! SO COOL!!!"
    toshu "Waaahhh !!賞品！とても冷たい!!!"

# game/script.rpy:3014
translate japanese after_minigame_4_a626a91e:

    genji "I know right, I'm so cool~"
    genji "私は正しいことを知っている、私はとても涼しい〜"

# game/script.rpy:3015
translate japanese after_minigame_4_9d037664:

    ichiru "Toshu meant the prize, you old ma--"
    ichiru "東武は賞品を意味していました、"

# game/script.rpy:3017
translate japanese after_minigame_4_339326c2:

    genji "OKAY! You guys decide with your pair so we can get this thing started!"
    genji "はい！あなたたちはあなたのペアで決めるので、このことを始めることができます！"

# game/script.rpy:3019
translate japanese after_minigame_4_a642d241:

    toshu "WE CAN EXPLORE THE WOODS! THIS IS GONNA BE SO MUCH FUN!!!"
    toshu "私たちはおとぎ話を見いだすことができます！これはすごく楽しいです！"

# game/script.rpy:3021
translate japanese after_minigame_4_259657b9:

    masaru "Toshu have you decided who you want to team up with?"
    masaru "あなたは誰とチームを組むか決めましたか？"

# game/script.rpy:3023
translate japanese after_minigame_4_4f832c0e:

    ichiru "Pair up with me Toshu!"
    ichiru "Toshuと私とペア！"

# game/script.rpy:3049
translate japanese choice_8_A_c7ff48ae:

    toshu "I'm not sure…"
    toshu "よく分かりません…"

# game/script.rpy:3051
translate japanese choice_8_A_fd4d860f:

    # toshu "Coach, can you decide for me?"
    toshu "コーチ、決めてもらっていいですか？"

# game/script.rpy:3053
translate japanese choice_8_A_89aeede1:

    genji "Oh me eh? But… I guess you should take Masaru with you."
    genji "ああ私ええ？でも…あなたと一緒に君を連れていくべきだと思う。"

# game/script.rpy:3055
translate japanese choice_8_A_60a3908f:

    genji "I want to keep an eye out to Ichiru here. I'm sure he's going to set up pranks to me again."
    genji "私はここにIchiruに目を留めたい。彼は再び私にいたずらをするつもりだと確信しています。"

# game/script.rpy:3057
translate japanese choice_8_A_b053cb99:

    ichiru "NO!!! Tofuu!! Let me go with you!!!"
    ichiru "NO !!!Tofu！私はあなたと一緒に行こう！"

# game/script.rpy:3059
translate japanese choice_8_A_8503509b:

    genji "No can do! I already assigned him with Masaru."
    genji "いいえ！私はすでに彼にマサルを割り当てた。"

# game/script.rpy:3061
translate japanese choice_8_A_8c585236:

    genji "And besides I'm sure Masaru can handle him fine."
    genji "そして、私はマサルが彼をうまくやることができると確信しています。"

# game/script.rpy:3063
translate japanese choice_8_A_55379157:

    genji "It's Masaru we are talking about after all!"
    genji "結局のところ私たちが話しているのはマサルです！"

# game/script.rpy:3065
translate japanese choice_8_A_64c81fd6:

    masaru "E-eh…"
    masaru "E-eh …"

# game/script.rpy:3067
translate japanese choice_8_A_ab3ff392:

    toshu "It's okay Captain! It's gonna be fun!"
    toshu "それは大丈夫だキャプテン！それは楽しみになるよ！"

# game/script.rpy:3069
translate japanese choice_8_A_8774ef31:

    genji "OKAY it's settled! Ichiru let's go!"
    genji "それは解決しました！Ichiruは行こう！"

# game/script.rpy:3071
translate japanese choice_8_A_2c077275:

    ichiru "Hmph! whatever Genji!! You probably just want me in your team because you are a ped--"
    ichiru "Hmph！Genjiは何でも！あなたはたぶんあなたのチームに私が欲しいのは、"

# game/script.rpy:3073
translate japanese choice_8_A_9d909cf3:

    genji "Our super forest adventure begins now!"
    genji "私たちのスーパーフォレストの冒険は今始まります！"

# game/script.rpy:3078
translate japanese choice_8_A_da04a35e:

    toshu "Let's go Captain! Let's win that prize!"
    toshu "キャプテンに行きましょう！その賞に勝ちましょう！"

# game/script.rpy:3080
translate japanese choice_8_A_7f87c4e2:

    masaru "A-Ahh sure… d-don't worry too much about the prize."
    masaru "A-Ahh確かに…賞品についてはあまり心配しないでください。"

# game/script.rpy:3086
translate japanese choice_8_B_26667c6a:

    toshu "I wanna team up with Ichiru!"
    toshu "Ichiruと一緒にチームを作りたい！"

# game/script.rpy:3088
translate japanese choice_8_B_86fb9661:

    ichiru "Awesome! You won't regret it!!"
    ichiru "驚くばかり！あなたはそれを後悔しません！"

# game/script.rpy:3090
translate japanese choice_8_B_457961ba:

    ichiru "I'm so happy you want me to be your partner~"
    ichiru "私はあなたが私があなたのパートナーになりたいと幸せです〜"

# game/script.rpy:3091
translate japanese choice_8_B_a23abd57:

    ichiru "Let's win that prize!"
    ichiru "その賞に勝ちましょう！"

# game/script.rpy:3093
translate japanese choice_8_B_555a3526:

    genji "No can do Ichiru."
    genji "No can do Ichiru."

# game/script.rpy:3094
translate japanese choice_8_B_6d261df7:

    genji "You are coming with me."
    genji "あなたは私と一緒に来ています。"

# game/script.rpy:3096
translate japanese choice_8_B_03161aed:

    ichiru "WHAT?? WHAT??!! WHAT!!!"
    ichiru "何？？何？？！！何！！！"

# game/script.rpy:3097
translate japanese choice_8_B_1eb1c5ef:

    ichiru "Put me down you annoying old man!! Heyy!!!"
    ichiru "あなたを迷惑な老人にしてください！ Heyy !!!"

# game/script.rpy:3099
translate japanese choice_8_B_737d2bcf:

    genji "Toshu, I'm going to assign you with Masaru."
    genji "Toshu、私はMasaruにあなたを割り当てます。"

# game/script.rpy:3101
translate japanese choice_8_B_d5aa8bc9:

    toshu "Ahh… okay…?"
    toshu "ああ…大丈夫…？"

# game/script.rpy:3103
translate japanese choice_8_B_733830cf:

    genji "Well, looks like everyone already set up!"
    genji "まあ、誰もが既に設定されているように見えます！"

# game/script.rpy:3105
translate japanese choice_8_B_9d909cf3:

    genji "Our super forest adventure begins now!"
    genji "私たちのスーパーフォレストの冒険は今始まります！"

# game/script.rpy:3107
translate japanese choice_8_B_fc8c2a4b:

    ichiru "NOOOOO!"
    ichiru "NOOOOO！"

# game/script.rpy:3112
translate japanese choice_8_B_da04a35e:

    toshu "Let's go Captain! Let's win that prize!"
    toshu "キャプテンに行きましょう！その賞に勝ちましょう！"

# game/script.rpy:3114
translate japanese choice_8_B_7f87c4e2:

    masaru "A-Ahh sure… d-don't worry too much about the prize."
    masaru "A-Ahh確かに…賞品についてはあまり心配しないでください。"

# game/script.rpy:3120
translate japanese choice_8_C_23713420:

    toshu "I can't decide who."
    toshu "私は誰を決定することはできません。"

# game/script.rpy:3122
translate japanese choice_8_C_7dd380b0:

    ichiru "I volunteer to be your partner Toshu!"
    ichiru "私はあなたのパートナーであるToshuにボランティア！"

# game/script.rpy:3124
translate japanese choice_8_C_8b5bf1ea:

    ichiru "I will win that prize for you!"
    ichiru "私はあなたのためにその賞を獲得します！"

# game/script.rpy:3126
translate japanese choice_8_C_9ca3293d:

    toshu "Sure thing!"
    toshu "確実なこと！"

# game/script.rpy:3128
translate japanese choice_8_C_edb7dcf1:

    masaru "Coach, what about me?"
    masaru "コーチ、私はどう？"

# game/script.rpy:3130
translate japanese choice_8_C_2b713dc4:

    genji "Don't worry Masaru. I'll assign you with someone else."
    genji "まるを心配しないでください。私はあなたに他の人を割り当てます。"

# game/script.rpy:3132
translate japanese choice_8_C_f1e30db6:

    genji "It's settled. Toshu and Ichiru will be the first one to go."
    genji "それは解決した。ToshuとIchiruが最初に行くだろう。"

# game/script.rpy:3134
translate japanese choice_8_C_6f80880c:

    ichiru "I'm going to win Toshu that prize!"
    ichiru "私はその賞を獲得するつもりです！"

# game/script.rpy:3136
translate japanese choice_8_C_ebb694db:

    toshu "I'm so excited!!"
    toshu "私はとても興奮しています！！"

# game/script.rpy:3138
translate japanese choice_8_C_733830cf:

    genji "Well, looks like everyone already set up!"
    genji "まあ、誰もが既に設定されているように見えます！"

# game/script.rpy:3140
translate japanese choice_8_C_9d909cf3:

    genji "Our super forest adventure begins now!"
    genji "私たちのスーパーフォレストの冒険は今始まります！"

# game/script.rpy:3146
translate japanese choice_8_D_f2b8dab4:

    toshu "Captain! Can I be your partn--"
    toshu "キャプテン！私はあなたの仲間になれますか？"

# game/script.rpy:3148
translate japanese choice_8_D_4d2b02a6:

    ichiru "Toshuuu!!!!! I'll be your partner!!"
    ichiru "Toshuuu !!!!!私はあなたのパートナーになるでしょう！"

# game/script.rpy:3150
translate japanese choice_8_D_2a616c78:

    toshu "E-eh?"
    toshu "E-え？"

# game/script.rpy:3152
translate japanese choice_8_D_f5812ff9:

    genji "Haha! Looks like Ichiru got him first! Tough luck, Masaru!"
    genji "ハハ！ Ichiruが彼を最初に得たように見える！タフ運、マサル！"

# game/script.rpy:3154
translate japanese choice_8_D_51fc9ce8:

    masaru "Ahh… it's okay, Coach."
    masaru "ああ…大丈夫です、コーチ。"

# game/script.rpy:3156
translate japanese choice_8_D_2b713dc4:

    genji "Don't worry Masaru. I'll assign you with someone else."
    genji "まるを心配しないでください。私はあなたに他の人を割り当てます。"

# game/script.rpy:3158
translate japanese choice_8_D_f1e30db6:

    genji "It's settled. Toshu and Ichiru will be the first one to go."
    genji "それは解決した。ToshuとIchiruが最初に行くだろう。"

# game/script.rpy:3160
translate japanese choice_8_D_1fde8d39:

    ichiru "I will win that prize for you! I'll make sure you won't regret having me as your partner! Hehehe!"
    ichiru "私はあなたのためにその賞を獲得します！私はあなたのパートナーとして私を持つことを後悔しないことを確認します！ Hehehe！"

# game/script.rpy:3162
translate japanese choice_8_D_7acbc153:

    toshu "WOW! We really can win that prize?! I'm so excited!"
    toshu "うわー！私たちは本当にその賞を獲得することができますか？私はとても興奮しています！"

# game/script.rpy:3163
translate japanese choice_8_D_04dfa4e1:

    ichiru "HELL YEAH!"
    ichiru "こんにちはよ！"

# game/script.rpy:3165
translate japanese choice_8_D_45f18353:

    ichiru "See you at the finish line, losers!"
    ichiru "フィニッシュライン、敗者で会いましょう！"

# game/script.rpy:3172
translate japanese route_masaru_1_bd329447:

    toshu_t "With Captain Masaru as my partner, I had nothing to worry about as we went deeper in the forest."
    toshu_t "Masaru大尉が私のパートナーである私は、私たちが森の中で深く行くにつれて心配する必要は全くありませんでした。"

# game/script.rpy:3173
translate japanese route_masaru_1_dfabee53:

    # toshu_t "Captain was really focused with the map Coach gave. I tried not to distract him."
    toshu_t "キャプテンはコーチがくれた地図に本当に集中していた。僕はキャプテンの邪魔をしないようにした。"

# game/script.rpy:3174
translate japanese route_masaru_1_ebf3a8e2:

    # toshu_t "We walked for hours and hours…"
    toshu_t "私たちは何時間も歩いた…"

# game/script.rpy:3175
translate japanese route_masaru_1_7f69a448:

    # toshu_t "But… I think… we're having trouble…"
    toshu_t "でも…何かが…何かがおかしい…"

# game/script.rpy:3176
translate japanese route_masaru_1_39b885fd:

    # toshu_t "I think we're lost."
    toshu_t "道に迷ったみたいだ。"

# game/script.rpy:3184
translate japanese route_masaru_1_5b8ba9f2:

    # toshu "C-Captain, we've been walking for half the day now…"
    toshu "キャ、キャプテン。僕たちもう半日は歩いてますよね…"

# game/script.rpy:3185
translate japanese route_masaru_1_a5109044:

    toshu "And… I think it's gonna rain too, we better hurry."
    toshu "そして…私もそれは雨が降るだろうと思う、私たちは急いでいる。"

# game/script.rpy:3187
translate japanese route_masaru_1_970b9466:

    # masaru "Ahh! I'm so sorry Toshu…"
    masaru "ああ！ 本当にすまない、Toshu…"

# game/script.rpy:3188
translate japanese route_masaru_1_95965e67:

    # masaru "I'm trying hard but I really can't understand this map that was given to us."
    masaru "一生懸命頑張ってはいるんだが、与えられたこの地図が全然読めないんだ。"

# game/script.rpy:3189
translate japanese route_masaru_1_fa1fab88:

    masaru "I just try to follow it, but it doesn't lead us to anywhere."
    masaru "私はそれに従うことを試みるが、それは私達をどこにでも導かない。"

# game/script.rpy:3191
translate japanese route_masaru_1_8304e107:

    # toshu "Uhm…"
    toshu "うう…"

# game/script.rpy:3193
translate japanese route_masaru_1_80e0d815:

    # masaru "W-wait a minute…"
    masaru "ちょ、ちょっと待ってくれ…"

# game/script.rpy:3195
translate japanese route_masaru_1_079a2429:

    # toshu "What is it, Captain?"
    toshu "キャプテン、どうかしました？"

# game/script.rpy:3197
translate japanese route_masaru_1_6926dc90:

    # masaru "I knew it…"
    masaru "分かったぞ…"

# game/script.rpy:3198
translate japanese route_masaru_1_2ee59394:

    # masaru "This tree…"
    masaru "この木…"

# game/script.rpy:3199
translate japanese route_masaru_1_cac0208b:

    # masaru "We've been going in circles all this time…"
    masaru "俺たちはずっと同じところをグルグル回っていたみたいだ…"

# game/script.rpy:3201
translate japanese route_masaru_1_368f9016:

    toshu "Owww… I wish I knew how to read that map. I'm just as clueless as you are…"
    toshu "私はその地図を読む方法を知りたがっています。私はあなたと同じように無知です…"

# game/script.rpy:3203
translate japanese route_masaru_1_4a14abe4:

    # masaru "I… I'm sorry Toshu, I'm not really good at these kind of stuff…"
    masaru "す…すまない、Toshu。俺はこういうのが全然ダメなんだ…"

# game/script.rpy:3205
translate japanese route_masaru_1_a490f9f3:

    # toshu "At least we're together, Captain!"
    toshu "僕も似たようなもんですよ、キャプテン！"

# game/script.rpy:3207
translate japanese route_masaru_1_0acd4481:

    toshu "How about we try to follow our instincts instead? Like, you know, marking the trees or something?"
    toshu "代わりに、私たちは本能に従うことを試みますか？木や何かをマーキングしているように、あなたは知っていますか？"

# game/script.rpy:3209
translate japanese route_masaru_1_258dd257:

    masaru "I want to confess something…"
    masaru "私は何かを告白する…"

# game/script.rpy:3211
translate japanese route_masaru_1_3accebd1:

    # toshu "Captain???"
    toshu "キャプテン？？？"

# game/script.rpy:3213
translate japanese route_masaru_1_adb4261a:

    masaru "Since the beginning, I really don't know where we are going."
    masaru "最初から私はどこに行くのか本当に分かりません。"

# game/script.rpy:3214
translate japanese route_masaru_1_2bd03c61:

    masaru "I've been going in random directions all this time…"
    masaru "今回私はランダムな方向に行ってきました…"

# game/script.rpy:3216
translate japanese route_masaru_1_a168e7ed:

    # masaru "I… I've always been the type who has no sense of direction…"
    masaru "お、俺は方向音痴なんだ…"

# game/script.rpy:3218
translate japanese route_masaru_1_19be29b9:

    toshu "You should've told me from the start, Captain! So we could've asked more guidance from Coach."
    toshu "あなたは当初から教えてくれたはずです、キャプテン！だから、コーチからのより多くの指導を頼むことができた。"

# game/script.rpy:3220
translate japanese route_masaru_1_2dd40967:

    masaru "You looked so happy when you heard I was your partner."
    masaru "あなたが私があなたのパートナーだと聞いたとき、あなたはとても嬉しそうでした。"

# game/script.rpy:3222
translate japanese route_masaru_1_a724122a:

    masaru "I didn't want to ruin your excitement."
    masaru "私はあなたの興奮を台無しにしたくなかった。"

# game/script.rpy:3224
translate japanese route_masaru_1_a6d6d6fc:

    # masaru "I'm sorry… I let you down…"
    masaru "ゴメンな…ガッカリしただろう…"

# game/script.rpy:3225
translate japanese route_masaru_1_3a1e894a:

    toshu "It's really okay Captain. It's not your fault, please don't be sad anymore!"
    toshu "本当に大丈夫です。キャプテン。あなたのせいではありません、もう悲しんではいけません！"

# game/script.rpy:3226
translate japanese route_masaru_1_88136602:

    masaru "I totally look lame…"
    masaru "私は完全に不自然に見える…"

# game/script.rpy:3227
translate japanese route_masaru_1_dcdc9689:

    masaru "I can't do anything right…"
    masaru "私は何か正しいことをすることはできません…"

# game/script.rpy:3228
translate japanese route_masaru_1_475fd9ed:

    masaru "I can't make you laugh just the way Ichiru can…"
    masaru "イチールができるようにあなたを笑わせることはできません…"

# game/script.rpy:3230
translate japanese route_masaru_1_637f95e1:

    # masaru "I can't set up a tent, more so read a map."
    masaru "俺はテントを張ることもできないし、地図を読むのはもっと苦手だ。"

# game/script.rpy:3232
translate japanese route_masaru_1_047769df:

    # masaru "I'm such a loser…"
    masaru "俺はダメな奴だ…"

# game/script.rpy:3234
translate japanese route_masaru_1_125ec8ec:

    # toshu "Don't say that Captain!"
    toshu "そんなこと言わないで下さい、キャプテン！"

# game/script.rpy:3235
translate japanese route_masaru_1_df0a6e7b:

    # toshu "You are not a loser!"
    toshu "キャプテンはダメなんかじゃありません！"

# game/script.rpy:3238
translate japanese route_masaru_1_0fa498c8:

    toshu "B-Because! I… I really--"
    toshu "B  - だから！私は…本当に -"

# game/script.rpy:3243
translate japanese route_masaru_1_93f22fb7:

    # masaru "I-it's raining!! We better find shelter."
    masaru "うわ、降ってきたぞ！ 雨宿りできる場所を探さないと。"

# game/script.rpy:3244
translate japanese route_masaru_1_42a972e4:

    # toshu "Let's go behind that tree over there!"
    toshu "後ろのあの木まで行きましょう！"

# game/script.rpy:3246
translate japanese route_masaru_1_13960576:

    masaru "We got ourselves wet… I feel like a total failure…"
    masaru "私たちは自分自身を濡らした…私は完全な失敗のように感じる…"

# game/script.rpy:3248
translate japanese route_masaru_1_a7596402:

    # toshu "Please don't say that, Captain!"
    toshu "そんなこと言わないで下さい、キャプテン！"

# game/script.rpy:3250
translate japanese route_masaru_1_47ff9f88:

    toshu "Please don't be sad! At least we are together right?"
    toshu "悲しくならないでください！少なくとも私たちは一緒になっていますか？"

# game/script.rpy:3252
translate japanese route_masaru_1_1ce8c6c2:

    toshu "Maybe we can set up a fire or something later… I'm pretty sure it will get cold."
    toshu "多分、私たちは火や何かを設定することができます…私はかなり冷たくなるだろうと確信しています。"

# game/script.rpy:3254
translate japanese route_masaru_1_79fec072:

    masaru "Ahh… I didn't bring any matches or lighter. I… I don't know how to make a fire…"
    masaru "ああ…私はマッチやライターを持っていませんでした。私は…火を作る方法を知らない…"

# game/script.rpy:3256
translate japanese route_masaru_1_fc363801:

    masaru "I feel so bad! I shouldn't have been your partner here. I'm so useless…"
    masaru "私はとても悪い気がする！私はここであなたのパートナーだったはずはありません。私はとても役に立たない…"

# game/script.rpy:3258
translate japanese route_masaru_1_c25c19ce:

    toshu "Stop panicking, Captain… we'll be fine."
    toshu "パニックを止めて、キャプテン…うまくいくよ。"

# game/script.rpy:3260
translate japanese route_masaru_1_16ab1435:

    masaru "You're right… what we can only do for now is just wait for them to search for us…"
    masaru "あなたは正しい…私たちが今できることは、彼らが私たちを検索するのを待つことだけです…"

# game/script.rpy:3261
translate japanese route_masaru_1_5461fb86:

    masaru "They won't be able to find us if we move around too much as well…"
    masaru "私たちがあまりにもあまりにも動き回ると、彼らは私たちを見つけることができません…"

# game/script.rpy:3263
translate japanese route_masaru_1_468de9df:

    toshu "Alright…"
    toshu "まあ…"

# game/script.rpy:3265
translate japanese route_masaru_1_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:3266
translate japanese route_masaru_1_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:3271
translate japanese route_masaru_1_f4933cd1:

    toshu_t "It was that awkward silence that led us into something that… I didn't expect."
    toshu_t "それは私たちを何かに導いた厄介な沈黙でした…私は期待していませんでした。"

# game/script.rpy:3273
translate japanese route_masaru_1_896303ad:

    # masaru "Hey, I… u-uhh… can I tell you something?"
    masaru "なあ、ちょっと…その…言いたいことがあるんだが、いいか？"

# game/script.rpy:3275
translate japanese route_masaru_1_079a2429_1:

    # toshu "What is it, Captain?"
    toshu "何ですか、キャプテン？"

# game/script.rpy:3277
translate japanese route_masaru_1_278e7dc2:

    # masaru "It's about last night…"
    masaru "昨夜のことなんだが…"

# game/script.rpy:3278
translate japanese route_masaru_1_37c55f8e:

    # toshu "Ehh? What about last night?"
    toshu "え？ 昨夜が何ですか？"

# game/script.rpy:3280
translate japanese route_masaru_1_218f5c83:

    # masaru "Well, I was… kinda awake when you did the thing you did last night."
    masaru "その…昨夜お前がしてるときに目が覚めたんだ。"

# game/script.rpy:3282
translate japanese route_masaru_1_d985cbc5:

    masaru "You know, you… ermm fap--"
    masaru "あなたは知っています、あなたは… ermm fap--"

# game/script.rpy:3284
translate japanese route_masaru_1_d1b5dd56:

    # toshu "AH!!"
    toshu "ええーっ！"

# game/script.rpy:3286
translate japanese route_masaru_1_324e822a:

    # toshu "WAHH!! I'm so sorry!! I didn't know you were awake that time Captain!!!"
    toshu "わああ！！ すいません！！ キャプテンが起きてるなんて知りませんでした！！！"

# game/script.rpy:3287
translate japanese route_masaru_1_f9e66e4b:

    # toshu "This is so embarrassing!!"
    toshu "恥ずかしい～！！"

# game/script.rpy:3289
translate japanese route_masaru_1_2c3f4f27:

    # masaru "I was really shocked, you got up and you suddenly do it."
    masaru "本当にビックリした。お前が起き上がって、いきなり始めるもんだから。"

# game/script.rpy:3290
translate japanese route_masaru_1_7d6ed66f:

    # masaru "I didn't want to disturb you though."
    masaru "邪魔しちゃ悪いな、と思って。"

# game/script.rpy:3292
translate japanese route_masaru_1_2ae0a892:

    toshu "I want to run away! Of all people, why Captain has to find out about it."
    toshu "私は逃げ出したい！すべての人のなかで、キャプテンがなぜそれについて調べなければならないのか。"

# game/script.rpy:3294
translate japanese route_masaru_1_57599d46:

    masaru "But there's nothing to be embarrassed about."
    masaru "しかし、恥ずかしいことは何もありません。"

# game/script.rpy:3295
translate japanese route_masaru_1_0b95615c:

    masaru "In fact, I used to do it all the time as well…"
    masaru "実際、私はいつもそれをやっていました…"

# game/script.rpy:3297
translate japanese route_masaru_1_dd6828fc:

    masaru "You…"
    masaru "君は…"

# game/script.rpy:3298
translate japanese route_masaru_1_e2b35877:

    masaru "…looked very cute doing that…"
    masaru "…そのことをとてもかわいく見えました…"

# game/script.rpy:3300
translate japanese route_masaru_1_71ecb01a:

    toshu "C-Captain?!"
    toshu "C-キャプテン?!"

# game/script.rpy:3302
translate japanese route_masaru_1_d70d6f21:

    masaru "I-I'm sorry… I'm making you fluster…"
    masaru "私は - すみません…私はあなたをうんざりさせています…"

# game/script.rpy:3303
translate japanese route_masaru_1_ffe93505:

    masaru "I'm pretty sure no one wants other people finding out about their private time…"
    masaru "誰も自分のプライベートタイムを知りたくないとは思っていません…"

# game/script.rpy:3305
translate japanese route_masaru_1_364f49a2_1:

    # toshu "…"
    toshu "…"

# game/script.rpy:3307
translate japanese route_masaru_1_28b260c3:

    masaru "You're--"
    masaru "あなたは -"

# game/script.rpy:3308
translate japanese route_masaru_1_dd25f0e6:

    masaru "…down there."
    masaru "…そこに。"

# game/script.rpy:3309
translate japanese route_masaru_1_9ad472ea:

    toshu "N-no, Captain! Don't look!"
    toshu "いいえ、キャプテン！見ないでください！"

# game/script.rpy:3311
translate japanese route_masaru_1_7406c950:

    masaru "It's alright Toshu. It's really perfectly normal."
    masaru "それは大丈夫です。それは本当に完全に正常です。"

# game/script.rpy:3312
translate japanese route_masaru_1_364f49a2_2:

    # toshu "…"
    toshu "…"

# game/script.rpy:3313
translate japanese route_masaru_1_617ccdc1_1:

    # masaru "…"
    masaru "…"

# game/script.rpy:3317
translate japanese route_masaru_1_a883bb1e:

    toshu "T-the rain stopped."
    toshu "T-雨が止んだ。"

# game/script.rpy:3318
translate japanese route_masaru_1_366d723a:

    toshu "M-maybe w-we can go find them now."
    toshu "M-多分私たちは今それらを見つけることができます。"

# game/script.rpy:3320
translate japanese route_masaru_1_a99ef5d5:

    masaru "W-wait Toshu!"
    masaru "Toshu！"

# game/script.rpy:3322
translate japanese route_masaru_1_99f1da07:

    masaru "Uhh… wanna do it together?"
    masaru "うーん…一緒にやりたい？"

# game/script.rpy:3324
translate japanese route_masaru_1_b855a648:

    toshu "W-what?!"
    toshu "W何？"

# game/script.rpy:3326
translate japanese route_masaru_1_37c27a40:

    masaru "It's okay, no one will see us anyway! I promise I'll be quick!"
    masaru "大丈夫です、とにかく誰も私たちを見ることはありません！私は速くなることを約束します！"

# game/script.rpy:3328
translate japanese route_masaru_1_13ee256e:

    toshu "A-are you sure, Captain?"
    toshu "A-確かです、キャプテン？"

# game/script.rpy:3330
translate japanese route_masaru_1_c9e91f46:

    toshu_t "I couldn't believe it was happening."
    toshu_t "私はそれが起こっていたとは信じられませんでした。"

# game/script.rpy:3331
translate japanese route_masaru_1_ee2b7b68:

    toshu_t "Captain asked me something I really never expected from him…"
    toshu_t "キャプテンは、私が本当に彼から期待したことがないことを私に尋ねました…"

# game/script.rpy:3336
translate japanese route_masaru_1_2154ab45:

    masaru "P-please… h-help yourself Toshu."
    masaru "P  -  please …あなた自身をToshu助けてください。"

# game/script.rpy:3337
translate japanese route_masaru_1_da517cbe:

    toshu_t "Captain's dick!!"
    toshu_t "キャプテンのディック!!"

# game/script.rpy:3338
translate japanese route_masaru_1_afba7030:

    toshu_t "It's so big… I've never seen anything like it…"
    toshu_t "それはとても大きいです…私はそれのようなものを見たことがない…"

# game/script.rpy:3340
translate japanese route_masaru_1_7f602749:

    toshu "Hnn…"
    toshu "Hnn …"

# game/script.rpy:3341
translate japanese route_masaru_1_efe9283e:

    masaru "Ahh…"
    masaru "ああ…"

# game/script.rpy:3343
translate japanese route_masaru_1_f35e6b1d:

    masaru "It feels really good, Toshu."
    masaru "それは本当に良いと感じます。"

# game/script.rpy:3344
translate japanese route_masaru_1_bc1760ae:

    masaru "I'm gonna--"
    masaru "するつもり -"

# game/script.rpy:3346
translate japanese route_masaru_1_93460c87:

    masaru "Ahh!!!"
    masaru "ああ！"

# game/script.rpy:3348
translate japanese route_masaru_1_d4445c5d:

    toshu "Fwah!"
    toshu "フワ！"

# game/script.rpy:3350
translate japanese route_masaru_1_3f971bfb:

    masaru "I-I'm sorry Toshu…"
    masaru "I-I'm sorry Toshu…"

# game/script.rpy:3351
translate japanese route_masaru_1_84339f75:

    masaru "I made a mess over your face…"
    masaru "私はあなたの顔をめちゃくちゃにしました…"

# game/script.rpy:3353
translate japanese route_masaru_1_c9cdd6f6:

    toshu_t "It's the first time I have done anything like that…"
    toshu_t "私がそのようなことをしたのは初めてです…"

# game/script.rpy:3354
translate japanese route_masaru_1_276c012a:

    toshu_t "It felt good."
    toshu_t "それは良い気分でした。"

# game/script.rpy:3355
translate japanese route_masaru_1_5ecae83e:

    toshu_t "And the fact that I did that to Captain Masaru… makes it even better."
    toshu_t "そして、私がマサル大尉にしたことは、それをさらに良くする。"

# game/script.rpy:3356
translate japanese route_masaru_1_11ceaa0a:

    toshu_t "After a little clean-up, we've decided to wait on the same spot waiting for someone to come for us."
    toshu_t "ちょっとクリーンアップした後、誰かが私たちのために来るのを待っている同じ場所で待つことにしました。"

# game/script.rpy:3357
translate japanese route_masaru_1_bcc7f805:

    narrator "1 hour later…"
    narrator "1時間後…"

# game/script.rpy:3365
translate japanese route_masaru_1_51a2f83b:

    toshu "Good thing the rain stopped."
    toshu "雨が降った良い事。"

# game/script.rpy:3367
translate japanese route_masaru_1_ee179ede:

    masaru "The stars are so bright… and there are fireflies as well…"
    masaru "星はとても明るいですし、ホタルもあります…"

# game/script.rpy:3369
translate japanese route_masaru_1_1a0c14d1:

    toshu "Yeah they are so beautiful!"
    toshu "うん、彼らはとても美しいです！"

# game/script.rpy:3371
translate japanese route_masaru_1_731d9ef9:

    masaru "It's getting late…"
    masaru "遅くなってきたよ…"

# game/script.rpy:3373
translate japanese route_masaru_1_42504c20:

    masaru "Will they ever find us?"
    masaru "彼らは私たちを見つけるだろうか？"

# game/script.rpy:3375
translate japanese route_masaru_1_c474c925:

    random "TOFUUU!!!!"
    random "TOFUUU !!!!"

# game/script.rpy:3377
translate japanese route_masaru_1_7a74d1c2:

    toshu "I-is that?!"
    toshu "それは私ですか？"

# game/script.rpy:3379
translate japanese route_masaru_1_5314e49f:

    masaru "They f-found us…!"
    masaru "彼らは私たちを見つけた…！"

# game/script.rpy:3384
translate japanese route_masaru_1_61c0a070:

    toshu "Ichiru! Coach Genji!"
    toshu "Ichiru! Coach Genji!"

# game/script.rpy:3385
translate japanese route_masaru_1_3e0a547e:

    ichiru "Where have you two been? We have been worried sick!!"
    ichiru "あなたはどこにいましたか？私たちは病気になることを心配しています！"

# game/script.rpy:3387
translate japanese route_masaru_1_fbd047c4:

    genji "Yeah, where did you guys run off to? The whole club has been looking everywhere!"
    genji "ええ、どこに逃げましたか？クラブ全体がどこからでも見ています！"

# game/script.rpy:3389
translate japanese route_masaru_1_372141a2:

    masaru "It's all my fault Coach…"
    masaru "それは私のすべての失敗コーチです…"

# game/script.rpy:3390
translate japanese route_masaru_1_26597b80:

    masaru "I got us lost and I don't have any survival skills…"
    masaru "私は失われてしまったし、生存スキルはない…"

# game/script.rpy:3392
translate japanese route_masaru_1_6cc6056e:

    masaru "I really don't understand this map that you provided us…"
    masaru "あなたが私たちに提供したこの地図を本当に理解できません…"

# game/script.rpy:3393
translate japanese route_masaru_1_6b831e80:

    masaru "See? Is this map really correct?"
    masaru "見る？この地図は本当に正しいですか？"

# game/script.rpy:3395
translate japanese route_masaru_1_512c43c6:

    ichiru "It's because you are reading it upside down, you idiot!!"
    ichiru "それはあなたがそれを逆さまに読んでいるからです、あなたはばかです！"

# game/script.rpy:3397
translate japanese route_masaru_1_25f2c46c:

    masaru "Ehh? How am I supposed to know that?"
    masaru "え？私はそれをどのように知っていますか？"

# game/script.rpy:3399
translate japanese route_masaru_1_6f64976c:

    ichiru "I knew that I should've been the one who paired with Tofu!"
    ichiru "私はTofuと組んでいたはずだったはずだったことを知っていた！"

# game/script.rpy:3401
translate japanese route_masaru_1_7c4731e7:

    masaru "I'm sorry… I really am…"
    masaru "申し訳ありません…私は本当に…"

# game/script.rpy:3403
translate japanese route_masaru_1_f43526f1:

    genji "I didn't know that you are not good with direction."
    genji "私はあなたが方向性に乏しいことを知らなかった。"

# game/script.rpy:3404
translate japanese route_masaru_1_4d85003d:

    genji "You should've told us before, so that I could've paired you with Ichiru."
    genji "あなたはIchiruとあなたをペアにすることができたので、私たちに前に言っておくべきです。"

# game/script.rpy:3406
translate japanese route_masaru_1_33b2b5d2:

    genji "We were the first one to get to the destination."
    genji "我々は目的地に行く最初の人だった。"

# game/script.rpy:3408
translate japanese route_masaru_1_6215816a:

    ichiru "What do you mean 'we'? You just followed me until we reached the goal, Genji."
    ichiru "あなたは「私たち」を意味しますか？私たちが目標に達するまで、あなたは私にちょうど従った、Genji。"

# game/script.rpy:3410
translate japanese route_masaru_1_cd02a15e:

    genji "Anyway, I'm glad you guys are safe, we should head back to the finish line. Everyone is worried sick."
    genji "とにかく、あなたたちが安全だとうれしいです、我々はフィニッシュラインに戻るべきです。誰もが病気になることを心配しています。"

# game/script.rpy:3412
translate japanese route_masaru_1_96e52444:

    masaru "I'm sorry again…"
    masaru "もう一度申し訳ありません…"

# game/script.rpy:3413
translate japanese route_masaru_1_0e0f9401:

    masaru "I'm sorry too, Toshu…"
    masaru "Toshu …あまりにも申し訳ありませんが、Toshu …"

# game/script.rpy:3415
translate japanese route_masaru_1_7a8abe4d:

    toshu "I told you, Captain. Stop worrying about it! We're safe now!"
    toshu "私はあなたに言った、キャプテン。それについて心配しないで！私たちは今安全です！"

# game/script.rpy:3417
translate japanese route_masaru_1_9583f226:

    toshu "C'mon let's go back!"
    toshu "さあ帰ろう！"

# game/script.rpy:3424
translate japanese route_ichiru_1_f5b18242:

    toshu_t "As me and Ichiru went through the woods, I could totally see how experienced he is."
    toshu_t "私とIchiruが森を通り抜けたとき、私は彼がどれほど熟練しているかを完全に知ることができました。"

# game/script.rpy:3425
translate japanese route_ichiru_1_fb48d389:

    toshu_t "He is walking around the woods as if it's his own backyard!"
    toshu_t "彼は自分の裏庭のように森の周りを歩いています！"

# game/script.rpy:3426
translate japanese route_ichiru_1_151db45b:

    toshu_t "He's even so prepared, bringing every supply one would ever need."
    toshu_t "彼は準備が整っていて、必要なものをすべて用意しています。"

# game/script.rpy:3434
translate japanese route_ichiru_1_c248ff03:

    ichiru "We're close!"
    ichiru "私たちは近くにいる！"

# game/script.rpy:3436
translate japanese route_ichiru_1_7832844a:

    toshu "Wow, Ichiru! You're barely looking at the map! How do you know we're going to the right way?"
    toshu "うわー、Ichiru！あなたはほとんど地図を見ていない！私たちが正しい方法で行くことをどのように知っていますか？"

# game/script.rpy:3438
translate japanese route_ichiru_1_9ae381c1:

    ichiru "Psssh! I only need one look at the map, we'll be there first!"
    ichiru "Psssh！私は地図を一度見るだけで、まずそこにいます！"

# game/script.rpy:3439
translate japanese route_ichiru_1_02452cf8:

    ichiru "We'll pretty much win the prize!"
    ichiru "私たちはかなり多くの賞を獲得します！"

# game/script.rpy:3441
translate japanese route_ichiru_1_39b39cc2:

    toshu "It's really amazing how you know so much about hiking."
    toshu "ハイキングについて知っていることは本当に素晴らしいです。"

# game/script.rpy:3443
translate japanese route_ichiru_1_e33396c7:

    ichiru "I think it's 'cause my mom used to always give me tips on how to survive in the wild."
    ichiru "私はそれは、私のお母さんが野生で生き残るためのヒントをいつも私に伝えていたからだと思う。"

# game/script.rpy:3444
translate japanese route_ichiru_1_bdcdab77:

    ichiru "My parents used to bring me along in their trips. They're such an adventurous pair."
    ichiru "私の両親は私の旅の中で私を連れて来ました。彼らはそのような冒険的なペアです。"

# game/script.rpy:3446
translate japanese route_ichiru_1_9da323f4:

    ichiru "Hiking, diving, hunting, fishing, name it all! I've done them at least once with my parents!"
    ichiru "ハイキング、ダイビング、狩猟、釣り、名前はすべて！私は両親と少なくとも一度はやった！"

# game/script.rpy:3448
translate japanese route_ichiru_1_05000073:

    toshu "W-wooow… I really don't think I could get out of this forest by myself. I don't feel very useful…"
    toshu "W-wooow …私は本当に自分でこの森から出ることができないと思う。私はとても役に立たないと感じません…"

# game/script.rpy:3450
translate japanese route_ichiru_1_ac8625e1:

    ichiru "Nahh, I couldn't do it without you."
    ichiru "ナア、私はあなたなしではできなかった。"

# game/script.rpy:3452
translate japanese route_ichiru_1_618fde53:

    toshu "Ehh… I didn't do anything at all. I just followed you until we get here."
    toshu "私は何もしなかった。私がここに来るまで、あなたにちょうど従った。"

# game/script.rpy:3454
translate japanese route_ichiru_1_c1efe2d4:

    ichiru "Not really, you really made me motivated and made this hiking enjoyable!"
    ichiru "本当に、あなたは本当に私に動機を与え、このハイキングを楽しませました！"

# game/script.rpy:3456
translate japanese route_ichiru_1_91c9327f:

    ichiru "I really like hanging out with you!"
    ichiru "私は本当にあなたと出会うのが好きです！"

# game/script.rpy:3458
translate japanese route_ichiru_1_b7291dec:

    toshu "Me too!"
    toshu "私も！"

# game/script.rpy:3460
translate japanese route_ichiru_1_443e215b:

    ichiru "Anyway, I think we're really way ahead of everyone else."
    ichiru "とにかく、私たちは本当に他の誰よりも先だと思います。"

# game/script.rpy:3462
translate japanese route_ichiru_1_359df2a2:

    ichiru "We're a few meters away back to the campsite, and this is the only passage to get back there."
    ichiru "私たちはキャンプ場まで数メートルのところにあり、そこに戻るための唯一の通路です。"

# game/script.rpy:3463
translate japanese route_ichiru_1_4593130c:

    ichiru "How about we just wait for someone to come this way?"
    ichiru "私たちはただ誰かがこのように来るのを待っていますか？"

# game/script.rpy:3464
translate japanese route_ichiru_1_3f5c0881:

    ichiru "We can take a bit rest if you want?"
    ichiru "あなたが望むなら、ちょっと休むことができますか？"

# game/script.rpy:3466
translate japanese route_ichiru_1_a1045f33:

    toshu "Y-yeah… my feet is kinda feeling tired already."
    toshu "Y-yeah …私の足はもうちょっと疲れています。"

# game/script.rpy:3468
translate japanese route_ichiru_1_b501762e:

    ichiru "Here's a good spot to rest. Let's relax a bit shall we?"
    ichiru "ここに休むべき場所があります。私たちはちょっとリラックスしましょうか？"

# game/script.rpy:3470
translate japanese route_ichiru_1_89008fd8:

    toshu_t "Resting our feet, Ichiru took out food from his bag."
    toshu_t "私の足を休めて、Ichiruは袋から食べ物を取り出した。"

# game/script.rpy:3471
translate japanese route_ichiru_1_0a9c07f1:

    toshu_t "His readiness makes him an impressive camper."
    toshu_t "彼の準備は彼を印象的なキャンピングカーにする。"

# game/script.rpy:3472
translate japanese route_ichiru_1_e2011e94:

    toshu_t "The sun finally set, and the sky quickly turn dark."
    toshu_t "最終的に太陽が沈み、空は素早く暗くなる。"

# game/script.rpy:3476
translate japanese route_ichiru_1_b7b44dbb:

    ichiru "Hmm… it's already dark. I can't believe they still haven't found their way back to camp."
    ichiru "うーん…それはすでに暗いです。私は彼らがまだキャンプに戻る道を見つけていないとは信じられません。"

# game/script.rpy:3478
translate japanese route_ichiru_1_f732da39:

    ichiru "Let's start a campfire."
    ichiru "キャンプファイヤーを始めましょう。"

# game/script.rpy:3480
translate japanese route_ichiru_1_aaa8a653:

    toshu "I don't know how to make one…"
    toshu "私は1つを作る方法を知らない…"

# game/script.rpy:3482
translate japanese route_ichiru_1_6ebbd3cb:

    ichiru "No problem! That's why I'm here! Watch and learn!"
    ichiru "問題ない！それが私がここにいる理由です！見て学ぶ！"

# game/script.rpy:3484
translate japanese route_ichiru_1_9c79af9f:

    toshu_t "Swiftly, Ichiru gathered some twigs and rocks."
    toshu_t "すばやく、Ichiruは小枝と岩を集めました。"

# game/script.rpy:3485
translate japanese route_ichiru_1_3c153825:

    toshu_t "In less than a minute, he sparked an ember, and then made fire."
    toshu_t "1分もたたないうちに、彼は火を燃やし火を作った。"

# game/script.rpy:3488
translate japanese route_ichiru_1_d613177b:

    toshu "A-amazing!!!"
    toshu "素晴らしい！"

# game/script.rpy:3490
translate japanese route_ichiru_1_af54d078:

    ichiru "Piece of cake!"
    ichiru "ケーキの一片！"

# game/script.rpy:3492
translate japanese route_ichiru_1_9332692e:

    toshu "You're really impressive, Ichiru!"
    toshu "あなたは本当に印象的な、Ichiru！"

# game/script.rpy:3494
translate japanese route_ichiru_1_18990541:

    ichiru "Thanks!"
    ichiru "ありがとう！"

# game/script.rpy:3500
translate japanese route_ichiru_1_da7db662:

    toshu_t "We were enjoying the warmth from the campfire, until Ichiru broke the silence."
    toshu_t "私たちは、イチルが沈黙を破るまで、キャンプファイヤーから暖かさを楽しんでいました。"

# game/script.rpy:3502
translate japanese route_ichiru_1_a3a371e5:

    ichiru "Ahh… anyway, there's something in my head that I really want to talk to you about."
    ichiru "ああ…とにかく、私の頭の中には本当にあなたと話したいことがあります。"

# game/script.rpy:3504
translate japanese route_ichiru_1_5acbc746:

    toshu "Ehh? What is it Ichiru?"
    toshu "え？それは何ですか？"

# game/script.rpy:3505
translate japanese route_ichiru_1_4973adda:

    ichiru "Well…"
    ichiru "まあ…"

# game/script.rpy:3507
translate japanese route_ichiru_1_2856d98a:

    toshu_t "It was that question that led us into something that… I didn't expect."
    toshu_t "それは私たちを何かに導いたその質問でした…私は期待していませんでした。"

# game/script.rpy:3509
translate japanese route_ichiru_1_c99158f1:

    ichiru "Well… hmm how do I say this politely?"
    ichiru "まあ…うーん、私はこれを丁寧にどのように言いますか？"

# game/script.rpy:3511
translate japanese route_ichiru_1_23c163c5:

    ichiru "I saw you fapping last night."
    ichiru "私はあなたが昨晩盗聴するのを見た。"

# game/script.rpy:3513
translate japanese route_ichiru_1_0f05f136:

    toshu "WHAAT?! Y-you were awake that time?"
    toshu "何？その時、あなたは目を覚ましましたか？"

# game/script.rpy:3515
translate japanese route_ichiru_1_2b9cb37a:

    ichiru "Yeah! You looked so cute touching yourself like that! Hahaha!"
    ichiru "うん！あなたはとてもかわいいように見えました。ははは！"

# game/script.rpy:3517
translate japanese route_ichiru_1_be33b5a7:

    toshu "Oh no…"
    toshu "あらいやだ…"

# game/script.rpy:3519
translate japanese route_ichiru_1_ee8703ff:

    ichiru "You look like you were enjoying yourself, I didn't want to disturb you."
    ichiru "あなたは自分を楽しんでいるように見えますが、私はあなたに邪魔したくありませんでした。"

# game/script.rpy:3520
translate japanese route_ichiru_1_d59b7248:

    ichiru "I remember how I always fapped in the locker room after every practice."
    ichiru "すべての練習の後、私がいつもロッカールームでどうやって戸惑ったのか覚えています。"

# game/script.rpy:3522
translate japanese route_ichiru_1_82e1271c:

    toshu "W-whaaaat…?"
    toshu "インwhaaaat …？"

# game/script.rpy:3524
translate japanese route_ichiru_1_364fba58:

    ichiru "H-hey! It's perfectly normal to do it!"
    ichiru "H-ねえ！それは正常にそれを行うには正常です！"

# game/script.rpy:3525
translate japanese route_ichiru_1_9fcb0e14:

    toshu "Wait… I'm having a hard time processing these so suddenly."
    toshu "待って…私はこれらを突然処理するのに苦労している。"

# game/script.rpy:3527
translate japanese route_ichiru_1_cfdbc04b:

    ichiru "You are enjoying this kind of talk aren't you?"
    ichiru "あなたはこの種の話を楽しんでいますか？"

# game/script.rpy:3529
translate japanese route_ichiru_1_483ef6b5:

    toshu "N-n-no, Ichiru. It's not like that!"
    toshu "Nn-no、Ichiru。それはそうではありません！"

# game/script.rpy:3530
translate japanese route_ichiru_1_8874184a:

    ichiru "Your body says otherwise, you got a boner."
    ichiru "あなたの体はそうでなければ、あなたは骨抜きを持っています。"

# game/script.rpy:3532
translate japanese route_ichiru_1_6ee6891a:

    toshu "W-wha…!!"
    toshu "イン義和… !!"

# game/script.rpy:3533
translate japanese route_ichiru_1_db340feb:

    ichiru "But you know~ you don't need to hide it."
    ichiru "しかし、あなたが知っている〜あなたはそれを隠す必要はありません。"

# game/script.rpy:3535
translate japanese route_ichiru_1_e3ce915d:

    ichiru "Ichiru is always here to save the day~"
    ichiru "イチールはいつもここに救うために〜"

# game/script.rpy:3540
translate japanese route_ichiru_1_0adf51d0:

    ichiru "Hehe! It looks so cute."
    ichiru "Hehe！とてもかわいいですね。"

# game/script.rpy:3541
translate japanese route_ichiru_1_49a90fea:

    toshu "Wh-what are you doing, Ichiru?"
    toshu "名前：Wh-何していますか、Ichiruですか？"

# game/script.rpy:3542
translate japanese route_ichiru_1_178530e8:

    ichiru "Just relax Tofu~"
    ichiru "ただTofuをリラックス〜"

# game/script.rpy:3544
translate japanese route_ichiru_1_634ba741:

    toshu "Hnnn!"
    toshu "Hnnn！"

# game/script.rpy:3546
translate japanese route_ichiru_1_3dea2d56:

    toshu "Ichiru! If you continue that I'm gonna--"
    toshu "Ichiru！もしあなたがそれを続けるなら、私は、"

# game/script.rpy:3548
translate japanese route_ichiru_1_823a6fc2:

    toshu "H-haa…"
    toshu "H-HAA …"

# game/script.rpy:3549
translate japanese route_ichiru_1_009f1b1c:

    ichiru "Don't worry I won't let you do it so quickly."
    ichiru "心配しないで、私はあなたがそれをすばやくやることはできません。"

# game/script.rpy:3550
translate japanese route_ichiru_1_aa09d410:

    ichiru "I'm gonna make sure you enjoy it!"
    ichiru "私はあなたがそれを楽しむことを確認するつもりです！"

# game/script.rpy:3552
translate japanese route_ichiru_1_d32ccf8e:

    toshu "Wahh! Ichiru!"
    toshu "Wahh! Ichiru!"

# game/script.rpy:3554
translate japanese route_ichiru_1_1f3e0632:

    toshu "Ichiru! That--"
    toshu "Ichiru！それ -"

# game/script.rpy:3555
translate japanese route_ichiru_1_a546275e:

    toshu "I-I'm gonna cum… Ichiru."
    toshu "私は兼ねるつもりです…Ichiru。"

# game/script.rpy:3557
translate japanese route_ichiru_1_142dc217:

    toshu "Wahh! I'm sorry!"
    toshu "ワー！ごめんなさい！"

# game/script.rpy:3559
translate japanese route_ichiru_1_18e6fac5:

    ichiru "Fwah!"
    ichiru "フワ！"

# game/script.rpy:3561
translate japanese route_ichiru_1_85c43cd3:

    ichiru "That tasted delicious!"
    ichiru "それはおいしかった！"

# game/script.rpy:3562
translate japanese route_ichiru_1_410db4d4:

    ichiru "I expected nothing less from Tofu!"
    ichiru "私はTofuから何も期待していなかった！"

# game/script.rpy:3564
translate japanese route_ichiru_1_c9cdd6f6:

    toshu_t "It's the first time I have done anything like that…"
    toshu_t "私がそのようなことをしたのは初めてです…"

# game/script.rpy:3565
translate japanese route_ichiru_1_276c012a:

    toshu_t "It felt good."
    toshu_t "それは良い気分でした。"

# game/script.rpy:3566
translate japanese route_ichiru_1_664c800e:

    toshu_t "And the fact that Ichiru did it for me… makes it even better."
    toshu_t "そして、イチールが私のためにやったという事実は、それをさらに良くする。"

# game/script.rpy:3567
translate japanese route_ichiru_1_22bca400:

    toshu_t "After a little clean-up, we've decided to move straight back to camp."
    toshu_t "ちょっと掃除した後、私たちは真っ直ぐ戻ってキャンプに戻ることに決めました。"

# game/script.rpy:3568
translate japanese route_ichiru_1_fcd7be87:

    narrator "Few minutes later…"
    narrator "数分後に…"

# game/script.rpy:3576
translate japanese route_ichiru_1_3696a289:

    toshu "Wow, when you said we were few meters away, you weren't kidding!"
    toshu "うわー、あなたが数メートル離れていると言ったとき、あなたは冗談ではなかった！"

# game/script.rpy:3577
translate japanese route_ichiru_1_494d192b:

    ichiru "Yup! We're the first ones, even though we waited for them!"
    ichiru "うん！我々はそれらを待っていたにもかかわらず、最初のものです！"

# game/script.rpy:3579
translate japanese route_ichiru_1_7f92e1a7:

    toshu "HOORAY!"
    toshu "HOORAY！"

# game/script.rpy:3581
translate japanese route_ichiru_1_bf8ba5d2:

    genji "Hey! You two! How long have you guys been here?"
    genji "ねえ！あなた方二人！あなたはここにどれくらい滞在していますか？"

# game/script.rpy:3583
translate japanese route_ichiru_1_70dfe640:

    ichiru "Pfft! You guys are so slow. We've been here for an hour or more already!"
    ichiru "Pfft！あなたたちはとても遅いです。私たちはすでに1時間以上ここにいます！"

# game/script.rpy:3585
translate japanese route_ichiru_1_475b9bf6:

    genji "That's… really fast…!"
    genji "それは…本当に速い…！"

# game/script.rpy:3587
translate japanese route_ichiru_1_9f774fe0:

    toshu "Ichiru lead the way until here, he is really awesome!"
    toshu "イチルはここまで道を進み、彼は本当にすごい！"

# game/script.rpy:3589
translate japanese route_ichiru_1_9c03284d:

    ichiru "See? I told you I will win you that prize, Tofu!"
    ichiru "見る？私はあなたに賞金、Tofuを獲得すると言った！"

# game/script.rpy:3591
translate japanese route_ichiru_1_650de195:

    toshu "Umm Coach, since we were the first one to go here, what's our prize??"
    toshu "うーんコーチ、私たちはここに行く最初だったので、私たちの賞は何ですか？"

# game/script.rpy:3593
translate japanese route_ichiru_1_99c78fab:

    toshu "I'm really curious of what it is!"
    toshu "私はそれが本当に好奇心だ！"

# game/script.rpy:3595
translate japanese route_ichiru_1_1d263926:

    ichiru "Yeah, what's the prize, Genji?!"
    ichiru "ええ、Genjiは何ですか？"

# game/script.rpy:3597
translate japanese route_ichiru_1_33036f81:

    genji "Oh, the prize?"
    genji "ああ、賞金？"

# game/script.rpy:3599
translate japanese route_ichiru_1_532ff53a:

    genji "Ahh, you already got the prize!"
    genji "ああ、あなたはすでに賞金を持っています！"

# game/script.rpy:3601
translate japanese route_ichiru_1_667b693e:

    genji "It's the essence of teamwork, trust, and your relationship with each other!"
    genji "それは、チームワーク、信頼、お互いの関係の本質です！"

# game/script.rpy:3603
translate japanese route_ichiru_1_1e7e3eb5:

    genji "Isn't that a worth enough prize?"
    genji "それは十分な価値のある賞ではありませんか？"

# game/script.rpy:3605
translate japanese route_ichiru_1_419c0495:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:3607
translate japanese route_ichiru_1_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:3609
translate japanese route_ichiru_1_06288ea0:

    toshu "EEHHH?!!"
    toshu "エイフ？"

# game/script.rpy:3611
translate japanese route_ichiru_1_85b28cd0:

    ichiru "HEY GENJI! WHAT BULLSHIT ARE YOU TALKING ABOUT?!"
    ichiru "HEY GENJI！あなたはどんなことを話していますか？"

# game/script.rpy:3613
translate japanese route_ichiru_1_07271dae:

    genji "*sigh* I knew this would happen."
    genji "*一息*私はこれが起こることを知っていた。"

# game/script.rpy:3614
translate japanese route_ichiru_1_d99b47e9:

    genji "I just wanted you guys to be motivated in this activity."
    genji "私はあなたにこの活動で動機付けされることを望んでいました。"

# game/script.rpy:3616
translate japanese route_ichiru_1_8f50ffa2:

    genji "I know you guys will complain and won't even participate if I say there isn't any prize--"
    genji "私は皆さんが不平を言うことを知っていますし、賞品がないと言うと参加しないことさえあります。"

# game/script.rpy:3618
translate japanese route_ichiru_1_7e2e9499:

    ichiru "ARGHH!! Don't give me that crap GENJI!!!"
    ichiru "ARGHH !!あの駄目を私に与えないでください。ガンジー！"

# game/script.rpy:3619
translate japanese route_ichiru_1_e2ac0979:

    ichiru "Tofu here was really expecting that prize!"
    ichiru "ここのTofuは本当にその賞を期待していた！"

# game/script.rpy:3621
translate japanese route_ichiru_1_24cece36:

    genji "C-calm down boy…!"
    genji "C-落ち着いた少年…！"

# game/script.rpy:3623
translate japanese route_ichiru_1_a1a82122:

    genji "The main purpose of this hike was not only to increase your stamina but also to have fun."
    genji "このハイキングの主な目的は、あなたのスタミナを増やすだけでなく、楽しむことでした。"

# game/script.rpy:3624
translate japanese route_ichiru_1_24c7b999:

    ichiru "SHUT UP! I wanted to make Toshu happy by winning him the prize!"
    ichiru "黙れ！私は彼に賞金をあげて東照を幸せにしたいと思っていました！"

# game/script.rpy:3626
translate japanese route_ichiru_1_73d4ab04:

    toshu "Ichiruu!!"
    toshu "Ichiruu!!"

# game/script.rpy:3629
translate japanese route_ichiru_1_5088db37:

    ichiru "Owiee Tofu! You didn't have to hit me…"
    ichiru "オーエーエTofu！あなたは私を打つ必要はなかった…"

# game/script.rpy:3631
translate japanese route_ichiru_1_b4ff8d45:

    toshu "Don't be mean to Coach Genji!"
    toshu "Genji長には意味がありません！"

# game/script.rpy:3632
translate japanese route_ichiru_1_716e9eff:

    toshu "It's okay if there isn't any prize."
    toshu "賞品がなければ大丈夫です。"

# game/script.rpy:3634
translate japanese route_ichiru_1_53da30ed:

    toshu "I really had a great time with you anyway!"
    toshu "私はとにかくあなたと本当に楽しい時間を過ごしました！"

# game/script.rpy:3635
translate japanese route_ichiru_1_aaf7efe3:

    toshu "That's what matters right?"
    toshu "それが重要なのは何ですか？"

# game/script.rpy:3637
translate japanese route_ichiru_1_ab55b260:

    ichiru "Ohhh…"
    ichiru "ああ…"

# game/script.rpy:3638
translate japanese route_ichiru_1_39d20c35:

    ichiru "Okay then… if you say so."
    ichiru "さて、あなたがそう言うならば…。"

# game/script.rpy:3640
translate japanese route_ichiru_1_2e3d33e2:

    genji "Wow… I never saw anyone else who can make Ichiru behave like that…"
    genji "うわー…イチローのように振る舞わせることができる誰も見たことがない…"

# game/script.rpy:3642
translate japanese route_ichiru_1_76fa742f:

    genji "I thought I'm going to have another beating with this spoiled brat!"
    genji "私はこの甘やかされたばかに別の殴打をするつもりだと思った！"

# game/script.rpy:3644
translate japanese route_ichiru_1_dfb0e11e:

    ichiru "What did you just call me?!!"
    ichiru "何を私に電話したのですか？"

# game/script.rpy:3646
translate japanese route_ichiru_1_0d73e567:

    genji "Haha! Anyway, looks like everyone is here now."
    genji "ハハ！とにかく誰もがここにいるみたいです。"

# game/script.rpy:3648
translate japanese route_ichiru_1_0fcd373e:

    ichiru "No, wait! Masaru's team isn't here yet."
    ichiru "いや、待って！マサルのチームはまだここにいません。"

# game/script.rpy:3650
translate japanese route_ichiru_1_848c2440:

    toshu "It's really late. Where's Captain?"
    toshu "本当に遅れました。キャプテンはどこですか？"

# game/script.rpy:3652
translate japanese route_ichiru_1_4bd3bfc1:

    ichiru "Should we go look for him Genji?"
    ichiru "彼はGenjiを探しに行くべきですか？"

# game/script.rpy:3654
translate japanese route_ichiru_1_600a1a99:

    genji "Let's wait for a little more. Maybe they just had a wrong detou--"
    genji "ちょっと待ってみましょう。たぶん彼らは間違ったdetouを持っていたかもしれません -"

# game/script.rpy:3655
translate japanese route_ichiru_1_4b688a4b:

    random "Coach!"
    random "コーチ！"

# game/script.rpy:3657
translate japanese route_ichiru_1_d1d2ef5f:

    genji "Ohh, It's Masaru's partner, Ryota."
    genji "ああ、マサルのパートナー、リョタ。"

# game/script.rpy:3659
translate japanese route_ichiru_1_fa50e243:

    genji "See? I told you they're just a bit late!"
    genji "見る？私は彼らがちょっと遅れていると言った！"

# game/script.rpy:3660
translate japanese route_ichiru_1_e08aef65:

    toshu "But I don't see Captain anywhere…"
    toshu "しかし、キャプテンはどこにも見えません…"

# game/script.rpy:3665
translate japanese route_ichiru_1_f16b105c:

    ryota "Coach!!! I lost Masaru in the woods I can't find him anywhere! Please help!"
    ryota "コーチ！！！私はどこにでも彼を見つけることができない森で大丸を失った！助けてください！"

# game/script.rpy:3667
translate japanese route_ichiru_1_4121293f:

    genji "Ehh?? What happened?"
    genji "Ehh？何が起こった？"

# game/script.rpy:3668
translate japanese route_ichiru_1_1a59aaa0:

    ryota "Well… I just turned around to pee beside a tree and the next thing I knew he was gone."
    ryota "まあ…私は木の葉の上でおしっこに回って、私は彼が行っていたことを知っていた。"

# game/script.rpy:3670
translate japanese route_ichiru_1_5db0bf60:

    genji "What?! I'll look for him, quick!!!"
    genji "何？！私は彼を探すだろう、速い!!!"

# game/script.rpy:3672
translate japanese route_ichiru_1_ffe18eb6:

    ichiru "Let me join you, Genji!"
    ichiru "Genjiに参加しましょう！"

# game/script.rpy:3674
translate japanese route_ichiru_1_5283968e:

    toshu "Let me go as well, Coach!"
    toshu "私も一緒に行きましょう、コーチ！"

# game/script.rpy:3676
translate japanese route_ichiru_1_f2ffbe61:

    genji "Okay! Let's go!"
    genji "はい！行こう！"

# game/script.rpy:3683
translate japanese route_ichiru_1_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:3684
translate japanese route_ichiru_1_e1efd052:

    masaru "This is so messed up… I'm really lost…"
    masaru "これはとてもうんざりです…私は本当に失われています…"

# game/script.rpy:3686
translate japanese route_ichiru_1_c143e492:

    random "MASARUU!!!"
    random "MASARUU!!!"

# game/script.rpy:3688
translate japanese route_ichiru_1_56f57899:

    random "CAPTAIN!!"
    random "キャプテン！！"

# game/script.rpy:3690
translate japanese route_ichiru_1_42d5c991:

    masaru "Is that…?"
    masaru "それは…？"

# game/script.rpy:3692
translate japanese route_ichiru_1_27da365b:

    masaru "ICHIRUUUU!!! TOSHU!!! Over here!!"
    masaru "ICHIRUUUU!!! TOSHU!!! Over here!!"

# game/script.rpy:3697
translate japanese route_ichiru_1_0144113b:

    genji "What happened??? Why did you get lost??"
    genji "何が起こった？？？なぜ迷子になったのですか？"

# game/script.rpy:3699
translate japanese route_ichiru_1_f9a76e02:

    masaru "I'm sorry!"
    masaru "ごめんなさい！"

# game/script.rpy:3701
translate japanese route_ichiru_1_967640b2:

    masaru "I thought I was walking along with Ryota when suddenly I just noticed he was gone…"
    masaru "私は突然私が彼がいなくなったことに気づいたとき私がRyotaと一緒に歩いていると思った…"

# game/script.rpy:3702
translate japanese route_ichiru_1_7128f381:

    masaru "The map was with him… I didn't know what I should do."
    masaru "地図は彼と一緒だった…私は何をすべきか分からなかった。"

# game/script.rpy:3704
translate japanese route_ichiru_1_7f337549:

    masaru "So I just stayed in one place…"
    masaru "だから私はちょうど1つの場所にとどまった…"

# game/script.rpy:3706
translate japanese route_ichiru_1_bbeaf89e:

    ichiru "You could've at least lit up a fire to keep yourself warm. It's freezing here!"
    ichiru "あなたは自分自身を暖かく保つために少なくとも火をつけていた可能性があります。ここで凍っている！"

# game/script.rpy:3708
translate japanese route_ichiru_1_15d72945:

    masaru "I… I don't know how to…"
    masaru "私は…どのようにするかわからない…"

# game/script.rpy:3710
translate japanese route_ichiru_1_bb83f7f8:

    toshu "What's important now is Captain's safe!"
    toshu "今重要なのはキャプテンの安全です！"

# game/script.rpy:3712
translate japanese route_ichiru_1_d7475d75:

    genji "Yeah! Let's head back to the camp! I'm sure all of you are exhausted and starving!"
    genji "うん！キャンプに戻りましょう！私はあなたのすべてが疲れて飢えていると確信しています！"

# game/script.rpy:3723
translate japanese choice_8_end_29898652:

    ichiru "I really can't believe Coach made up an imaginary prize so that we can be motivated in that activity!"
    ichiru "私は本当にコーチが想像上の賞をつくったので、私たちはその活動に動機づけることができないと信じられない！"

# game/script.rpy:3725
translate japanese choice_8_end_d27a2d54:

    masaru "I have a confession to make…"
    masaru "私は告白して…"

# game/script.rpy:3726
translate japanese choice_8_end_b3b95e83:

    masaru "I actually knew from the start that there wouldn't be any prize… Coach Genji told me before we began."
    masaru "私は実際に賞金がないということを当初から知っていた。元祖コーチは、私たちが始まる前に私に言った。"

# game/script.rpy:3728
translate japanese choice_8_end_7de96870:

    ichiru "EHHH?!! Why didn't you tell us!! I thought you were at our side!!"
    ichiru "EHHH？!!なぜあなたは私たちに言いませんでしたか？私はあなたが私たちの側にいると思った！"

# game/script.rpy:3730
translate japanese choice_8_end_942b1762:

    toshu "Waahh… no wonder Captain didn't sound that excited…"
    toshu "Waahh …キャプテンは興奮したように聞こえませんでした…"

# game/script.rpy:3732
translate japanese choice_8_end_b8c7738d:

    masaru "You guys were really thrilled with it, I felt guilty to spoil the fun just like that…"
    masaru "あなたは本当にそれに興奮していた、私はそのような楽しみを台無しに罪悪感を感じた…"

# game/script.rpy:3734
translate japanese choice_8_end_46d2e1fb:

    masaru "I feel bad I made you guys worry for being lost…"
    masaru "私はあなたが迷子になることを心配させた悪い気分…"

# game/script.rpy:3735
translate japanese choice_8_end_a5ea4dbc:

    toshu "You're still worrying about that, Captain? We told you, it's okay!"
    toshu "あなたはまだそれについて心配しています、キャプテン？私たちはあなたに言った、それは大丈夫です！"

# game/script.rpy:3737
translate japanese choice_8_end_8ec38fda:

    masaru "But…"
    masaru "しかし…"

# game/script.rpy:3739
translate japanese choice_8_end_80969b7f:

    ichiru "No buts. Just forget about it, we all had a rough day!"
    ichiru "いいえ。ちょうどそれを忘れる、私たちは皆荒い一日を持っていた！"

# game/script.rpy:3741
translate japanese choice_8_end_342c8ff4:

    masaru "O-okay…"
    masaru "O-okay…"

# game/script.rpy:3743
translate japanese choice_8_end_526f06a0:

    genji "Great job today everyone! I hope you guys learned the value of teamwork in this exercise."
    genji "今日は素晴らしい人！この演習では、皆さんがチームワークの価値を知ったことを願っています。"

# game/script.rpy:3744
translate japanese choice_8_end_bcff1f2e:

    genji "Chemistry between teammates is important in baseball!"
    genji "チームメイト間の化学は野球で重要です！"

# game/script.rpy:3745
translate japanese choice_8_end_d52c6a8f:

    genji "Okay! That's all for today! You guys can sleep in your respective tents now."
    genji "はい！それが今日のすべてです！皆さんは今あなたのそれぞれのテントで寝ることができます。"

# game/script.rpy:3747
translate japanese choice_8_end_1775adb1:

    genji "Tomorrow will be our last day! I'm going to prepare something special for you guys!~"
    genji "明日が最後の日になります！私はあなたのために特別な何かを準備するつもりだ！〜"

# game/script.rpy:3749
translate japanese choice_8_end_8e284f8c:

    masaru "Okay, Coach!"
    masaru "さて、コーチ！"

# game/script.rpy:3751
translate japanese choice_8_end_aa5f111f:

    toshu "Goodnight, Coach."
    toshu "グッドナイト、コーチ。"

# game/script.rpy:3753
translate japanese choice_8_end_0bc73ca8:

    ichiru "See you tomorrow, GENJI! Haha!"
    ichiru "明日お会いしましょう、元気！ハハ！"

# game/script.rpy:3756
translate japanese choice_8_end_e9caec2a:

    ichiru "He looks so excited. I'm pretty sure he has another thing up in his sleeve…"
    ichiru "彼はとても興奮しているようだ。私は彼が自分の袖に別のものを持っていると確信しています…"

# game/script.rpy:3758
translate japanese choice_8_end_dc8437e2:

    masaru "It seems so… I hope it's not hiking again…"
    masaru "それはそうだ…私は再びハイキングではないことを願っています…"

# game/script.rpy:3760
translate japanese choice_8_end_b8b2aadb:

    toshu "I'm getting excited again!"
    toshu "私は再び興奮しています！"

# game/script.rpy:3762
translate japanese choice_8_end_6bc1c3cf:

    ichiru "Hey, let's go back to the tent!"
    ichiru "ねえ、テントに戻りましょう！"

# game/script.rpy:3771
translate japanese choice_8_end_98d1fe5f:

    toshu "Fwaaah! This tent is really comfortable!"
    toshu "フアワ！このテントは本当に快適です！"

# game/script.rpy:3773
translate japanese choice_8_end_f8e62f1d:

    masaru "We sure had a very long day…"
    masaru "非常に長い一日だったと確信しています…"

# game/script.rpy:3775
translate japanese choice_8_end_496ddaa8:

    toshu "A lot has happened since the start of this trip!"
    toshu "この旅行の開始以来、多くのことが起こっています！"

# game/script.rpy:3777
translate japanese choice_8_end_d4b84555:

    ichiru "Can't believe this is our last night here at camp."
    ichiru "これがキャンプでの最後の夜だとは信じられない。"

# game/script.rpy:3779
translate japanese choice_8_end_92416c63:

    toshu "I'm so happy being with you guys!"
    toshu "私はあなたたちと一緒にいることにとても満足しています！"

# game/script.rpy:3780
translate japanese choice_8_end_3dd37e2b:

    toshu "I wish we always have fun like this!"
    toshu "私たちはいつもこのような楽しみがあることを願っています！"

# game/script.rpy:3782
translate japanese choice_8_end_bb026417:

    ichiru "Yeah! I never thought camping with friends would be twice as fun!"
    ichiru "うん！私は友人とのキャンプは楽しみの2倍だろうと思ったことはありません！"

# game/script.rpy:3784
translate japanese choice_8_end_66a3b632:

    masaru "I'm not fond with camping, but as long as I'm with you guys. Everything seems to be always great."
    masaru "私はキャンプを好きではありませんが、私はあなたと一緒にいる限り。すべてが常に素晴らしいようです。"

# game/script.rpy:3786
translate japanese choice_8_end_abcf63f8:

    toshu_t "I've never made good friends like Ichiru and Masaru!"
    toshu_t "IchiruとMasaruのような良い友達を作ったことはありません！"

# game/script.rpy:3787
translate japanese choice_8_end_8bb90dba:

    toshu_t "So this is the feeling…"
    toshu_t "これは感情です…"

# game/script.rpy:3788
translate japanese choice_8_end_73b6878e:

    toshu_t "The feeling of knowing someone is there to make you day bright whenever you are down!"
    toshu_t "誰かを知る気持ちは、あなたがダウンしているときはいつでもあなたを明るくすることです！"

# game/script.rpy:3789
translate japanese choice_8_end_73664faf:

    toshu_t "I will surely cherish my time with them!"
    toshu_t "私は確かに彼らと一緒に私の時間を大切にします！"
