# TODO: Translation updated at 2017-11-18 06:46

# game/script.rpy:29
translate japanese start_75e9d90c:

    # random_t "Today's the big day!"
    random_t "今日は特別な日だ！"

# game/script.rpy:30
translate japanese start_f0873061:

    # random_t "Winning or losing, having my friends by my side is what matters the most!"
    random_t "勝負は結果じゃない。そばに友達がいてくれるってことが一番大事なんだ！"

# game/script.rpy:31
translate japanese start_86e7c9cb:

    # random_t "We've been through a lot to get where we are right now…"
    random_t "今日までたくさん練習してきたんだ……。"

# game/script.rpy:32
translate japanese start_ff540771:

    # random_t "I'll definitely do my best and make everyone proud!"
    random_t "全力を出し切ってやるぞ！"

# game/script.rpy:34
translate japanese start_7394ed5b:

    # random_t "This is our story!"
    random_t "これは僕たちの物語だ！"

# game/script.rpy:38
translate japanese start_8d193e51:

    # narrator "4 months earlier…"
    narrator "4ヶ月前……。"
