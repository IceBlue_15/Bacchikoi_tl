# game/script.rpy:49
translate japanese start_57030b28:

    # random_t "I'm so excited!"
    random_t "僕はとってもワクワクしてるんだ！"

# game/script.rpy:50
translate japanese start_47ce9f0f:

    # random_t "It's my first day of school! I hope I meet a lot of new friends here."
    random_t "今日は転校初日！　友達たくさんできるといいな。"

# game/script.rpy:51
translate japanese start_bf04a2a5:

    # random_t "Wow, there sure are a lot of people around."
    random_t "うわー、いっぱい人がいるなー。"

# game/script.rpy:52
translate japanese start_683fe6d6:

    # random_t "I hope I don't get lost."
    random_t "道に迷わないようにしないと。"

# game/script.rpy:59
translate japanese start_a1831268:

    # random_t "Hmm… where should I go…? Let's see…"
    random_t "うーん……どこに行けばいいんだろ…？　どれどれ…"

# game/script.rpy:62
translate japanese start_014656a6:

    # random_t "Ahhh! I'm gonna be late!"
    random_t "あ～！　遅刻しちゃうよ！"

# game/script.rpy:63
translate japanese start_45dee500:

    # random_t "There it is!"
    random_t "あっ、ここだ！"

# game/script.rpy:71
translate japanese start_ff4b2ed4:

    # random "Please take your seats."
    random "みなさん席に着いてください。"

# game/script.rpy:72
translate japanese start_9505e01a:

    # random_t "Whew… I barely made it in time."
    random_t "ふぅ……。ギリギリ間に合ったよ。"

# game/script.rpy:74
translate japanese start_927b898e:

    # saki "Hello class, my name is Saki Hanazaki. I will be your homeroom teacher from now on."
    saki "みなさんこんにちは、私の名前はSaki Hanazakiです。今日からみなさんの担任になります。"

# game/script.rpy:75
translate japanese start_b0c5435c:

    # saki "Before we begin, let's welcome our new transfer student."
    saki "授業の前に、転校生を紹介しますね。"

# game/script.rpy:76
translate japanese start_677db768:

    # saki "Why don't you introduce yourself to the class?"
    saki "自己紹介をお願いします。"

# game/script.rpy:79
translate japanese start_e69565db:

    # random "Ack…!!"
    random "えっ……！！"

# game/script.rpy:80
translate japanese start_2aa0dee4:

    # random_t "Hngh… I didn't think I wouldn't get this much attention on the first day of class."
    random_t "ムムム……転校初日にこんなに注目されるとは思わなかった。"

# game/script.rpy:82
translate japanese start_6851a53c:

    # random_t "Well, here goes…"
    random_t "よし、行くぞ……！"

# game/script.rpy:84
translate japanese start_a75192da:

    # toshu "Hello everyone! My name is Toshu Kanada."
    toshu "みなさん初めまして！　僕の名前はToshu Kanadaです。"

# game/script.rpy:85
translate japanese start_27b54f50:

    # toshu "I am a transfer student from Fudan Academy."
    toshu "Fudan Academyから転校して来ました。"

# game/script.rpy:86
translate japanese start_5a1b6caf:

    # toshu "It's a pleasure to meet you all!"
    toshu "早くみんなと友達になりたいと思っています！"

# game/script.rpy:88
translate japanese start_48b7ea71:

    # saki "What made you want to go to this school, Mr. Kanada?"
    saki "Kanadaくんはどうしてこの学校に転校して来たんですか？"

# game/script.rpy:91
translate japanese start_334f4f50:

    # toshu "One of the reasons I moved is because I heard that Yakyusha Academy is a really awesome school for athletes!"
    toshu "僕が転校した理由の１つは、Yakyushaアカデミーが運動選手にとって本当にすばらしい学校だと聞いたからです！"

# game/script.rpy:92
translate japanese start_ad1d2930:

    # toshu "I love playing baseball! I played it a lot back then on my previous school, It was really fun!"
    toshu "僕は野球が大好きです！　前の学校でもたくさん野球をしました、本当に楽しかったです！"

# game/script.rpy:93
translate japanese start_caa28236:

    # toshu "But my transfer is mainly because my family moved nearby here."
    toshu "でも僕の転校の一番の理由は、僕の家族がこの近くに引越したからです。"

# game/script.rpy:95
translate japanese start_f3ebbb00:

    # toshu "I hope that I'll make a lot of new friends!"
    toshu "僕はたくさんの新しい友達を作りたいと思っています！"

# game/script.rpy:96
translate japanese start_cf117b83:

    # saki "I'm sure you'll enjoy your stay here at Yakyusha Academy!"
    saki "Yakyusha Academyでの日々が楽しいものになるって、先生信じてるわ！"

# game/script.rpy:97
translate japanese start_573ca9d2:

    # saki "Well then, let's begin with today's lesson."
    saki "では、今日の授業を始めましょう。"

# game/script.rpy:99
translate japanese start_f5058b80:

    # toshu_t "That ended better than expected. *sigh*"
    toshu_t "思ったよりうまくいったぞ。（ふぅ）"

# game/script.rpy:100
translate japanese start_70ac7db6:

    # toshu_t "I hope I didn't say anything stupid."
    toshu_t "変なこと言ってないといいけど。"

# game/script.rpy:108
translate japanese start_15605737:

    # toshu_t "It seems I expected too much from my first day of class. It was rather boring and time flew before I could notice."
    toshu_t "転校初日に変な期待をし過ぎてたみたい。授業はつまんないし、気が付いたらもうこんな時間だよ。"

# game/script.rpy:109
translate japanese start_db77cfa3:

    # toshu_t "I haven't even made any friends."
    toshu_t "友達一人もできなかったよ。"

# game/script.rpy:110
translate japanese start_ed013563:

    # toshu_t "Everyone was so serious with their studies."
    toshu_t "みんな真面目に授業を受けてたな。"

# game/script.rpy:111
translate japanese start_2f0cd419:

    # toshu_t "I hope I can keep up with them somehow…"
    toshu_t "何とかみんなに付いていけるといいんだけど……。"

# game/script.rpy:113
translate japanese start_6028f31e:

    # toshu_t "I wonder what should I do now?"
    toshu_t "いまからどうすればいいんだっけ？"

# game/script.rpy:114
translate japanese start_3516b926:

    # toshu_t "…"
    toshu_t "……………。"

# game/script.rpy:115
translate japanese start_66f889be:

    # toshu_t "Hmm? What's that poster?"
    toshu_t "ん？　このポスターは何だ？"

# game/script.rpy:120
translate japanese start_56911998:

    # toshu "'Is your school life boring?'"
    toshu "「学校生活退屈してない？」"

# game/script.rpy:121
translate japanese start_f1ceccc1:

    # toshu_t "I guess, yeah but it's still the first day…"
    toshu_t "そうだけど、まだ一日目だし……。"

# game/script.rpy:122
translate japanese start_fe2703c2:

    # toshu "'Don't you have any friends?'"
    toshu "「友達がいないの？」"

# game/script.rpy:123
translate japanese start_d96fbb7a:

    # toshu_t "Uhh… yes?"
    toshu_t "うーん……。はい、かな？"

# game/script.rpy:124
translate japanese start_e967f341:

    # toshu "'Worry no more! Our super awesome baseball club has got it all for you! We'll be best buddies!!!'"
    toshu "「もう心配御無用だ！　この超絶素晴らしい野球部が全部解決してあげる。君は最高の仲間だ！！！」"

# game/script.rpy:125
translate japanese start_8a3469e7:

    # toshu_t "Baseball?! W-woooow! This poster described my first day here perfectly!!"
    toshu_t "野球？！　う、うわぁっ！　今日という日はこのポスターを見つけるためにあったんだ！！"

# game/script.rpy:126
translate japanese start_7209aa8d:

    # toshu_t "I wanna check this out right now!"
    toshu_t "今すぐチェックしなきゃ！"

# game/script.rpy:133
translate japanese start_24c65130:

    # toshu_t "A-amazing…!! Yakyusha Academy's baseball field is so much bigger than my previous school's!"
    toshu_t "す、すごい……！！　Yakyusha Academyの野球場は前の学校のよりもずっと大きいや！"

# game/script.rpy:134
translate japanese start_08782c42:

    # toshu_t "The players are working so hard!"
    toshu_t "野球部員が一生懸命練習してるな！"

# game/script.rpy:136
translate japanese start_0d2b7246:

    # toshu_t "I feel I won't be able to match their skills… I'm not confident they'll take me in…"
    toshu_t "みんなについて行ける気がしないよ……。部員になれる自信がないや……。"

# game/script.rpy:138
translate japanese start_2db3c7c1:

    # toshu_t "Wait… what's that…?"
    toshu_t "えっ……。あれは何？"

# game/script.rpy:143
translate japanese start_11145d2d:

    # toshu "…!!!"
    toshu "……！！！"

# game/script.rpy:148
translate japanese start_0053c69e:

    # random "That was close!"
    random "危ないっ！"

# game/script.rpy:149
translate japanese start_a4508ed5:

    # toshu "W-whaa…"
    toshu "う、うわあぁ……。"

# game/script.rpy:150
translate japanese start_116deb7f:

    # random "Are you hurt?"
    random "ケガはない？"

# game/script.rpy:151
translate japanese start_92226009:

    # toshu "N-no, I'm ok."
    toshu "は、はい。大丈夫です。"

# game/script.rpy:158
translate japanese start_1de61edc:

    # random "What are you doing here? Only baseball club members are allowed inside the field."
    random "こんなところで何をしているんだい？　フィールドに入っていいのは野球部員だけだよ。"

# game/script.rpy:160
translate japanese start_6355974e:

    # toshu "I'm so sorry!"
    toshu "すいません！"

# game/script.rpy:163
translate japanese start_a7e3ba8d:

    # random "Ah…! Ahh! No, it's no big deal."
    random "あ……！　いやいや、謝らなくてもいいんだけど。"

# game/script.rpy:165
translate japanese start_fac2e67d:

    # random "Did the principal send you here?"
    random "校長先生にここに来るように言われたの？"

# game/script.rpy:167
translate japanese start_ea345f66:

    # random "Oh… I hope Ichiru didn't cause any trouble again."
    random "ああ……。Ichiruがまた何かしでかしたんじゃなきゃいいんだけど。"

# game/script.rpy:169
translate japanese start_8e73eea1:

    # toshu "I-I was… j-just… watching."
    toshu "ぼ、僕は……た、ただ……見てただけで。"

# game/script.rpy:171
translate japanese start_7bc9ff01:

    # random "Oh, a supporter, I see! What's your name?"
    random "ああ、応援してくれてたのか！　君の名前は何て言うの？"

# game/script.rpy:172
translate japanese start_fbfb044c:

    # toshu "I'm Toshu Kanada… a transfer student from class A."
    toshu "僕の名前はToshu Kanadaです……。Ａクラスに転校して来ました。"

# game/script.rpy:173
translate japanese start_ecbbc31c:

    # toshu "…and I'm here about the poster."
    toshu "ポスターを見てここに来たんですけど……。"

# game/script.rpy:175
translate japanese start_bf323acd:

    # random "Poster?"
    random "ポスター？"

# game/script.rpy:177
translate japanese start_7fef1238:

    # toshu "Your club advertisement? I… was wondering where can I sign up to join the baseball club."
    toshu "この部の勧誘ポスターだと思うんですけど。ど、どこに行けば入部できるのかなって思いまして。"

# game/script.rpy:179
translate japanese start_f9540027:

    # random "Oh! THAT poster!"
    random "ああ！　あのポスター！"

# game/script.rpy:180
translate japanese start_ca312022:

    # random "That's great!! We really need new members to join us. The baseball club hasn't been growing lately…"
    random "それはスゴイや！！　新入部員が欲しかったんだよ。ここ最近、全然部員が増えなくて……。"

# game/script.rpy:182
translate japanese start_0700beea:

    # toshu "Ehh? Really? But it looks like the baseball team is really lively."
    toshu "え？　本当ですか？　でも、野球部の活動はとても活発に見えますよ。"

# game/script.rpy:184
translate japanese start_1fde55f9:

    # random "Oh hahaha! Well… how should I put this…"
    random "ああ、ハハハ！　まあ……何て言えばいいか……。"

# game/script.rpy:186
translate japanese start_376e82ae:

    # random "Some of them are from other school."
    random "他の学校の野球部員も混ざってるんだよ。"

# game/script.rpy:187
translate japanese start_f84f248f:

    # random "They're just using the school's baseball field for practice."
    random "あの人たちはこの学校の野球場を練習に使ってるんだ。"

# game/script.rpy:188
translate japanese start_3b8865fc:

    # random "Since our school's baseball field is quite wide."
    random "うちの学校の野球場はかなり広いからね。"

# game/script.rpy:190
translate japanese start_db09a7db:

    # toshu "Oh… I see."
    toshu "ああ……なるほど。"

# game/script.rpy:192
translate japanese start_5448dff2:

    # random "Yeah, it's very sad, our enemies are playing in our own backyard."
    random "ああ、悔しい話だよ、敵チームが俺たちの庭でプレイしてるなんてね。"

# game/script.rpy:194
translate japanese start_a56f342a:

    # random "Oh, how rude of me! I forgot to introduce myself!"
    random "おっと失礼！　自己紹介がまだだったね！"

# game/script.rpy:196
translate japanese start_6d6983e3:

    # masaru "My name is Masaru Nakahara, I'm the captain of the baseball club. Nice to meet you Toshu!"
    masaru "俺の名前はMasaru Nakahara。野球部のキャプテンをやってるんだ。よろしくToshuくん！"

# game/script.rpy:197
translate japanese start_6f2dd52b:

    # narrator "*Masaru offers his hand*"
    narrator "（Masaruは手を差し出した。）"

# game/script.rpy:199
translate japanese start_6bb92680:

    # toshu_t "He seems to be a nice guy."
    toshu_t "うわぁっ、いい人だー。"

# game/script.rpy:200
translate japanese start_98ad6b61:

    # narrator "*Toshu shakes Masaru's hand*"
    narrator "（ToshuはMasaruと握手した。）"

# game/script.rpy:202
translate japanese start_05c85e7f:

    # toshu "It's nice to meet you too, Captain Masaru…!"
    toshu "よろしくお願いします、Masaruキャプテン……！"

# game/script.rpy:204
translate japanese start_0546c2b6:

    # masaru "Anyway, let me introduce you to your teammates!"
    masaru "じゃあ、君のチームメイトを紹介するね！"

# game/script.rpy:206
translate japanese start_50c0983a:

    # toshu "Already??"
    toshu "えぇっ、もう？？"

# game/script.rpy:207
translate japanese start_05e6ccd5:

    # toshu "M-my teammates??"
    toshu "ぼ、僕のチームメート？？"

# game/script.rpy:209
translate japanese start_b8720f62:

    # masaru "You're officially a member of the club now!"
    masaru "君はもうこの部の正式なメンバーだよ！"

# game/script.rpy:211
translate japanese start_c9e7bc60:

    # toshu "So fast!!"
    toshu "は、早っ！！"

# game/script.rpy:212
translate japanese start_162cbe52:

    # narrator "*Masaru drags Toshu while holding his hand*"
    narrator "（MasaruがToshuを引きずっていく。）"

# game/script.rpy:214
translate japanese start_11885408:

    # toshu "C-Captain, shouldn't we handle my club application first…?!"
    toshu "キャ……キャプテン、入部届とか書かなくてもいいんですか……？！"

# game/script.rpy:216
translate japanese start_572d6206:

    # masaru "You can leave that to me!"
    masaru "そんなの俺に任せてくれればいいから！"

# game/script.rpy:218
translate japanese start_85c85d9d:

    # masaru "Ichiruuu!!"
    masaru "Ichiruuu！！"

# game/script.rpy:219
translate japanese start_a7c566da:

    # masaru "We have a new recruit!!"
    masaru "新入部員だぞ！！"

# game/script.rpy:224
translate japanese start_5c8f5351:

    # ichiru "Hmm?"
    ichiru "ん？"

# game/script.rpy:226
translate japanese start_89f18b3c:

    # toshu_t "Is this boy really a member of the club?"
    toshu_t "この人って本当に野球部員なのかな？"

# game/script.rpy:227
translate japanese start_2a0ab293:

    # toshu_t "Wow he really eats a lot."
    toshu_t "うわー、この人本当によく食べるな。"

# game/script.rpy:235
translate japanese start_a9f9948b:

    # ichiru "Ohh~! Who is this Captain?"
    ichiru "お〜！　ん、キャプテン、こいつ誰？"

# game/script.rpy:236
translate japanese start_fe138d12:

    # masaru "This is Toshu, he's a transfer student from class A!"
    masaru "こいつはToshuくん、Ａクラスの転校生だ！"

# game/script.rpy:238
translate japanese start_3fc348a9:

    # ichiru "Oh, class A?"
    ichiru "Ａクラス？"

# game/script.rpy:239
translate japanese start_3b26a961:

    # ichiru "He's from the same class as me?"
    ichiru "俺と同じクラス？"

# game/script.rpy:241
translate japanese start_0bebb2f0:

    # masaru "I was expecting you guys to have already met since you're classmates."
    masaru "クラスメートなら、もう会ってるかと思ったんだけど。"

# game/script.rpy:243
translate japanese start_a370e8e4:

    # toshu "R-really?!"
    toshu "ほ、本当ですか？！"

# game/script.rpy:245
translate japanese start_052e889e:

    # toshu "I'm sorry but I don't think he was present while--"
    toshu "ごめんなさい、教室では見かけなかったような……。"

# game/script.rpy:247
translate japanese start_2f1013c2:

    # ichiru "*whisper* Shh!! Shh!! Don't tell Masaru I skippe--"
    ichiru "（ヒソヒソ声で)）シーッ！！　授業サボったのバレるだろ──"

# game/script.rpy:250
translate japanese start_6d1f4979:

    # narrator "*Masaru hits Ichiru on the head*"
    narrator "（MasaruがIchiruの頭を叩く）"

# game/script.rpy:252
translate japanese start_5e3fd8ba:

    # masaru "What? You skipped class again?? When will you change that awful habit of yours, Ichiru?"
    masaru "何？　また授業をサボっただって？？　いつになったらその悪い癖を直すんだ、Ichiru？"

# game/script.rpy:254
translate japanese start_182d5e34:

    # ichiru "Owwiiiee…"
    ichiru "いてて……。"

# game/script.rpy:256
translate japanese start_51d41cd4:

    # ichiru "You didn't have to hit me that hard Masaru."
    ichiru "そんなに強く叩くことないだろ。"

# game/script.rpy:258
translate japanese start_eeebc320:

    # masaru "Anyway Toshu, this is Ichiru Yanai."
    masaru "とにかくToshuくん、こいつがIchiru Yanaiだ。"

# game/script.rpy:259
translate japanese start_3570008c:

    # masaru "He's the team's best clean-up hitter but beware he can be an airhead and a pervert sometimes."
    masaru "こいつはチーム最高のクリーンナップだが、バカで変態だから気を付けるんだぞ。"

# game/script.rpy:261
translate japanese start_a93acc8d:

    # ichiru "I wouldn't say that I'm the best Masaru…"
    ichiru "最高だなんて言われると照れちまうな──"

# game/script.rpy:263
translate japanese start_bdbe2207:

    # ichiru "Wait!! What do you mean pervert?!"
    ichiru "……ってオイ！！　変態ってどういう意味だよ？"

# game/script.rpy:264
translate japanese start_b8442c3f:

    # ichiru "I am not a pervert!"
    ichiru "俺は変態じゃねーよ！"

# game/script.rpy:266
translate japanese start_8db62782:

    # ichiru "If there's anyone who is perverted in our team. It would be that stupid Coach."
    ichiru "うちのチームの変態っつったら、あのアホコーチのことだろ。"

# game/script.rpy:268
translate japanese start_7d94a8c8:

    # masaru "Now, now, don't throw names to our new recruit."
    masaru "おいおい、新入部員に変なこと教えるなよ。"

# game/script.rpy:269
translate japanese start_d48c45d5:

    # masaru "He might have second thought of joining our club."
    masaru "入部を考え直すかも知れないだろ。"

# game/script.rpy:271
translate japanese start_a13a9b74:

    # ichiru "He won't! He looks like the loyal-type kind of member!"
    ichiru "そんなことねーよ！　こいつは誠実そうだからな！"

# game/script.rpy:272
translate japanese start_74dd58bd:

    # narrator "*Grabs Toshu's hand and shakes it*"
    narrator "（Toshuの手を握り、ブンブンと振る。）"

# game/script.rpy:274
translate japanese start_b8e0e1f0:

    # ichiru "Hello there~! Nice to meet you, Tofu!"
    ichiru "やあやあこんにちは〜！　はじめましてTofuくん！"

# game/script.rpy:276
translate japanese start_2c9f5b4b:

    # toshu "I-it's Toshu."
    toshu "ぼ、僕の名前はToshuだよ……。"

# game/script.rpy:278
translate japanese start_ba86833b:

    # toshu "It's nice to meet you too Ichiru."
    toshu "こちらこそよろしくね、Ichiruくん。"

# game/script.rpy:279
translate japanese start_3431d70b:

    # toshu "And don't worry, I love playing baseball!"
    toshu "大丈夫、僕は野球をするのが大好きなんだ！"

# game/script.rpy:280
translate japanese start_b71c14ff:

    # toshu "I'm sure I'll stay!"
    toshu "ちゃんと入部するよ！"

# game/script.rpy:282
translate japanese start_ad3cb560:

    # masaru "I guess you're right Ichiru, he does seem loyal."
    masaru "Ichiruの言う通り、誠実そうな子だね。"

# game/script.rpy:284
translate japanese start_2e8f4692:

    # ichiru "Hahaha! When was I wrong to begin with?"
    ichiru "ハハハ！ 俺はいつでも正しいんだ！"

# game/script.rpy:286
translate japanese start_63e13ad8:

    # ichiru "Anyway, welcome to the club Tofu~ I'll be your best buddy!"
    ichiru "何はともあれ、野球部にようこそ、Tofu〜。俺たち、最高の友達になれるぜ！"

# game/script.rpy:287
translate japanese start_afaf9846:

    # narrator "*Still shaking Toshu's hand*"
    narrator "（まだToshuの手を振っている。）"

# game/script.rpy:289
translate japanese start_7729b5a0:

    # masaru "You guys are going to get along so great."
    masaru "お前らはうまくやっていけそうだな。"

# game/script.rpy:291
translate japanese start_bddc8de6:

    # masaru "Since you are now an official member Toshu, here's a brief information about the club."
    masaru "Toshu、これで正式なメンバーになったわけだから、この部について軽く説明しておくね。"

# game/script.rpy:292
translate japanese start_8e151743:

    # masaru "You're the first one to join the club this school year."
    masaru "君は本年度初の新入部員だ。"

# game/script.rpy:293
translate japanese start_3dbe8e0a:

    # masaru "Since most of the students choose basketball, football or swimming."
    masaru "ほとんどの生徒はバスケ、サッカー、水泳部に行くからね。"

# game/script.rpy:295
translate japanese start_17783f46:

    # ichiru "I joined last year!"
    ichiru "俺は去年入部したんだぜ！"

# game/script.rpy:297
translate japanese start_b7141cbe:

    masaru "And most of the members here are like that."
    masaru "ここのメンバーのほとんどはそうです。"

# game/script.rpy:298
translate japanese start_89a26435:

    masaru "The same people have been in Yakyusha Baseball Club for quite a while now…"
    masaru "同じ人たちがかなりの間、野球倶楽部にいました…"

# game/script.rpy:299
translate japanese start_cf3b5e08:

    # masaru "We rarely get any new members here."
    masaru "ここ最近は新入部員はほとんどいないんだ。"

# game/script.rpy:301
translate japanese start_9a7f6fe1:

    # toshu "T-that's terrible…"
    toshu "そ、それは大変ですね……。"

# game/script.rpy:303
translate japanese start_d813bd2d:

    ichiru "And every year we lose a member or two which is really sad as well."
    ichiru "毎年、本当に悲しいメンバーも2人も失います。"

# game/script.rpy:304
translate japanese start_16b1a63c:

    ichiru "They prefer joining other clubs, since the baseball club hasn't won any tournaments before."
    ichiru "彼らは野球クラブがこれまでにどんなトーナメントも獲得していないので、他のクラブへの参加を好む。"

# game/script.rpy:306
translate japanese start_3b1fc63e:

    toshu "But you guys are already good players. I was observing a while ago…"
    toshu "しかし、皆さんはすでに良い選手です。私はしばらく前に観察していた…"

# game/script.rpy:308
translate japanese start_14125e83:

    ichiru "Yeah, some of us are already skilled, but in the end, it's all about teamwork."
    ichiru "ええ、私たちのうちのいくつかはすでに熟練していますが、最終的にはチームワークに関するものです。"

# game/script.rpy:310
translate japanese start_c9fd80a2:

    masaru "I know this club is a bit underrated, but I'll do my best as the captain to make this baseball club be able to reach high next tournament!"
    masaru "私はこのクラブが少し過小評価されていることを知っていますが、私はこの野球クラブが次の大会に達することができるように大尉として最善を尽くします！"

# game/script.rpy:312
translate japanese start_b7db939d:

    masaru "So I hope you can get along with everyone and enjoy playing baseball."
    masaru "だから皆と付き合って野球を楽しんでください。"

# game/script.rpy:314
translate japanese start_87de62b9:

    # toshu "Y-yes, Captain!"
    toshu "は……はい、キャプテン！"

# game/script.rpy:316
translate japanese start_ec82647b:

    # ichiru "Haha! Welcome to the team buddy!"
    ichiru "ハハハ！　これでチームの一員だな！"

# game/script.rpy:318
translate japanese start_d0957250:

    # toshu "Thank you!"
    toshu "うん、ありがとう！"

# game/script.rpy:320
translate japanese start_13d66a8b:

    masaru "You can start your regular training session next week since you are a fresh transferee."
    masaru "あなたは新鮮な譲受人であるため、来週あなたの定期的なトレーニングセッションを開始することができます。"

# game/script.rpy:321
translate japanese start_342866ae:

    masaru "And I still need to formalize your application to the Coach!"
    masaru "そして、私はまだコーチにあなたのアプリケーションを正式にする必要があります！"

# game/script.rpy:323
translate japanese start_8cfa9e06:

    # toshu "Yes, Captain…! I'll do my best!"
    toshu "はい、キャプテン……！　一生懸命頑張ります！"

# game/script.rpy:324
translate japanese start_a50194a6:

    # toshu "Thank you for taking care of my application."
    toshu "入部の世話をしてくれてありがとうございます。"

# game/script.rpy:326
translate japanese start_d666c0b2:

    toshu_t "Just when I thought that that first day wasn't going to end well, I was wrong!"
    toshu_t "その最初の日がうまく終わらないと思ったとき、私は間違っていました！"

# game/script.rpy:327
translate japanese start_b02b31c1:

    toshu_t "I made two very admirable and quite contrasting friends."
    toshu_t "私は2人の非常に素晴らしいとかなり対照的な友人を作った。"

# game/script.rpy:328
translate japanese start_6506544c:

    toshu_t "One is a very carefree and energetic person."
    toshu_t "1つはとても気さくで元気な人です。"

# game/script.rpy:329
translate japanese start_6f3272a6:

    # toshu_t "While the other seems to be kind and responsible."
    toshu_t "もう一人は優しくて責任感のある人だ。"

# game/script.rpy:330
translate japanese start_f4d1b06a:

    toshu_t "Not to mention, he's really handsome too."
    toshu_t "もちろん、彼は本当にハンサムです。"

# game/script.rpy:331
translate japanese start_762b2a41:

    toshu_t "I got along really well with them these past days."
    toshu_t "私はこれらの過去の日々に本当にうまくやってきました。"

# game/script.rpy:332
translate japanese start_61315796:

    toshu_t "They made me feel welcome… and… and…"
    toshu_t "彼らは私を歓迎していると感じさせました…そして…そして…"

# game/script.rpy:333
translate japanese start_156b4c35:

    toshu_t "Well, it's a new feeling that I am not used to."
    toshu_t "まあ、それは私が慣れていない新しい気分です。"
