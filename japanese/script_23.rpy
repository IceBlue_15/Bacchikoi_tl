# game/script.rpy:10287
translate japanese ending_ine_mne_1657c646:

    toshu "*sigh* …"
    toshu "*一口*"

# game/script.rpy:10289
translate japanese ending_ine_mne_d3787d44:

    toshu "Ichiru and Masaru both didn't contact ever since they left school…"
    toshu "イチルとマサルは学校を去って以来ずっと連絡を取りませんでした…"

# game/script.rpy:10290
translate japanese ending_ine_mne_b2221946:

    toshu "I felt like all of our time together… was not important for them…"
    toshu "私は一緒に私たちの時間のすべてのように感じた…それらのために重要ではなかった…"

# game/script.rpy:10291
translate japanese ending_ine_mne_80f7e019:

    toshu "This is so familiar… I transferred to Yakyusha to make friends…"
    toshu "これはおなじみです…私は友達を作るためにヤクシュサに移りました…"

# game/script.rpy:10292
translate japanese ending_ine_mne_5fd8e9a9:

    toshu "Now I lost my friends again…"
    toshu "今私は再び友達を失った…"

# game/script.rpy:10294
translate japanese ending_ine_mne_4e2a41ce:

    toshu "Do I have to start all over again?"
    toshu "私は何度も何度もやり直す必要がありますか？"

# game/script.rpy:10296
translate japanese ending_ine_mne_f933c8ab:

    toshu "I should stop thinking this way!"
    toshu "私はこのように考えなくてはなりません！"

# game/script.rpy:10297
translate japanese ending_ine_mne_c8abdbc8:

    toshu "Or else I will break down to tears ag--"
    toshu "そうでなければ私は涙に砕きます。"

# game/script.rpy:10300
translate japanese ending_ine_mne_e9b5b288:

    toshu "A-auntie?"
    toshu "おばあさん？"

# game/script.rpy:10302
translate japanese ending_ine_mne_44a3b751:

    toshu "I'm coming!!"
    toshu "今行ってる！！"

# game/script.rpy:10305
translate japanese ending_ine_mne_37b877c9:

    toshu "Auntie, I didn't know you will go home toda--"
    toshu "叔母さん、私はあなたが家に帰るのを知らなかった -"

# game/script.rpy:10306
translate japanese ending_ine_mne_025e069b:

    random "Who are you calling auntie?"
    random "あなたは誰ですか？"

# game/script.rpy:10307
translate japanese ending_ine_mne_830d2510:

    random "Yeah! Did you already forget about us, Tofu!"
    random "うん！Tofu、私たちについてもう忘れましたか？"

# game/script.rpy:10308
translate japanese ending_ine_mne_57134ae8:

    toshu "I…ICHIRU! MASARU!!"
    toshu "I…ICHIRU! MASARU!!"

# game/script.rpy:10317
translate japanese ending_ine_mne_38da2faa:

    masaru "You don't have to call me captain anymore, Toshu!"
    masaru "あなたはもう私のキャプテン、Toshuを呼び出す必要はありません！"

# game/script.rpy:10319
translate japanese ending_ine_mne_170d3c64:

    ichiru "Masaru and I wanted to visit you!"
    ichiru "マサルと私はあなたを訪ねたかった！"

# game/script.rpy:10321
translate japanese ending_ine_mne_d697b122:

    ichiru "We haven't seen or talked with you for a long time."
    ichiru "私たちは長い間あなたを見たり話したりしていません。"

# game/script.rpy:10323
translate japanese ending_ine_mne_88c36650:

    masaru "We really missed you!"
    masaru "私たちは本当にあなたを逃した！"

# game/script.rpy:10325
translate japanese ending_ine_mne_8e448ef1:

    toshu "I missed you two as well…"
    toshu "私はあなたも2つが恋しい…"

# game/script.rpy:10327
translate japanese ending_ine_mne_87d895e8:

    ichiru "Toshu… we're really sorry that we left without properly saying goodbye…"
    ichiru "Toshu …さよならを正しく言わずに出発して本当に残念です…"

# game/script.rpy:10330
translate japanese ending_ine_mne_0d639d2f:

    ichiru "My dad convinced me to follow his plans for now."
    ichiru "私のお父さんは今私の計画に従うように私に納得させました。"

# game/script.rpy:10331
translate japanese ending_ine_mne_86a058ee:

    ichiru "He promised me he will give me the freedom with my decisions after I finish my studies."
    ichiru "彼は、私が勉強を終えた後、彼が私の意思決定で私に自由を与えることを約束しました。"

# game/script.rpy:10333
translate japanese ending_ine_mne_33a61e07:

    ichiru "Believe me…I really broke down after that…"
    ichiru "私を信じて…それ以来、本当に壊れました…"

# game/script.rpy:10334
translate japanese ending_ine_mne_915a1af5:

    ichiru "I needed time to calm myself down…"
    ichiru "私は自分を落ち着かせる時間が必要でした…"

# game/script.rpy:10336
translate japanese ending_ine_mne_eeac6898:

    masaru "Me too, Toshu…"
    masaru "Me too, Toshu…"

# game/script.rpy:10337
translate japanese ending_ine_mne_6178c5e8:

    masaru "I was so busy hunting for a part time job after I got kicked out of school"
    masaru "私は学校から追い出された後、パートタイムの仕事のために忙しく忙しかった"

# game/script.rpy:10339
translate japanese ending_ine_mne_32727ec3:

    masaru "I want to be able to continue my studies as soon as I can…"
    masaru "できるだけ早く研究を続けたいと思っています…"

# game/script.rpy:10341
translate japanese ending_ine_mne_42bc228b:

    masaru "We're really sorry, leaving you like that…"
    masaru "私たちは本当にすみません、あなたのように残して…"

# game/script.rpy:10343
translate japanese ending_ine_mne_06e1e8af:

    toshu "Y…you guys!"
    toshu "Y …あなたたちよ！"

# game/script.rpy:10345
translate japanese ending_ine_mne_61ffbc0f:

    ichiru "None of us wanted to leave each other, right?"
    ichiru "私たちの誰もお互いに離れたくなかったのですよね？"

# game/script.rpy:10347
translate japanese ending_ine_mne_7757200e:

    masaru "We will never abandon our special friend who had always been there for us…"
    masaru "私たちは、常に私たちのためにそこにいた特別な友人を放棄することはありません…"

# game/script.rpy:10350
translate japanese ending_ine_mne_7eae349a:

    masaru "You're the one who taught us to never give up!"
    masaru "あなたは決してあきらめないように私たちに教えた人です！"

# game/script.rpy:10352
translate japanese ending_ine_mne_da54c89c:

    ichiru "You are the only person who listened to us."
    ichiru "あなたは私たちの声を聞いた唯一の人です。"

# game/script.rpy:10353
translate japanese ending_ine_mne_9d6d9ab0:

    masaru "The person who made us feels special…"
    masaru "私たちを特別な気分にさせた人…"

# game/script.rpy:10355
translate japanese ending_ine_mne_c2f78ba2:

    ichiru "And you taught us to value friendship!"
    ichiru "そしてあなたは友情を大切にするよう教えられました！"

# game/script.rpy:10357
translate japanese ending_ine_mne_b80ce3f6:

    masaru "You made our lives much better, Toshu!"
    masaru "あなたは私たちの生活をはるかに良くしました。"

# game/script.rpy:10359
translate japanese ending_ine_mne_a77f4544:

    ichiru "Let's make a promise!"
    ichiru "約束をしましょう！"

# game/script.rpy:10361
translate japanese ending_ine_mne_c6af9831:

    ichiru "That even we're all going on separate ways, we will all be the best of friends!"
    ichiru "私たちはすべて別々の方法で行っていますが、私たちはすべて最高の友人になるでしょう！"

# game/script.rpy:10363
translate japanese ending_ine_mne_9b9a90f9:

    masaru "The time we spent together was real! Seeing each other laugh, smile and shed tears together…"
    masaru "私たちが一緒に過ごした時は本当だった！お互いの笑い声を見て、笑顔で一緒に涙を流す…"

# game/script.rpy:10364
translate japanese ending_ine_mne_cdc936c7:

    masaru "It's … the best feeling in the world…"
    masaru "それは…世界最高の気分です…"

# game/script.rpy:10366
translate japanese ending_ine_mne_6433351f:

    ichiru "Goodbyes are not forever…"
    ichiru "さよならは永遠ではありません…"

# game/script.rpy:10367
translate japanese ending_ine_mne_0b9a4306:

    ichiru "Goodbyes are not the end…"
    ichiru "さよならは終わりではありません…"

# game/script.rpy:10369
translate japanese ending_ine_mne_1b8beea9:

    ichiru "It simply mean… we will miss you!"
    ichiru "それは単にあなたが恋しくなることを意味します！"

# game/script.rpy:10370
translate japanese ending_ine_mne_ba6811c7:

    ichiru "Until we meet again!"
    ichiru "また会うまで！"

# game/script.rpy:10374
translate japanese ending_ine_mne_879ee7b2:

    masaru "We'll always be there for each other!"
    masaru "私たちはいつもお互いのためにそこにいるよ！"

# game/script.rpy:10375
translate japanese ending_ine_mne_638d87df:

    ichiru "It's a promise!"
    ichiru "それは約束だ！"

# game/script.rpy:10377
translate japanese ending_ine_mne_be12d1d5:

    toshu_t "In the end…"
    toshu_t "最終的には…"

# game/script.rpy:10378
translate japanese ending_ine_mne_0fb0183a:

    toshu_t "The pain I felt from saying goodbye was nothing compared…"
    toshu_t "さよならを言うことから感じた痛みは、何も比較されませんでした…"

# game/script.rpy:10379
translate japanese ending_ine_mne_f0f24371:

    toshu_t "…from the happiness I felt from the friends I've made."
    toshu_t "…私が作った友達から感じた幸福から。"
