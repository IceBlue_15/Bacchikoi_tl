# game/script.rpy:10167
translate japanese ending_ine_mne_4760d0aa:

    toshu_t "The weekend has passed after that intense tournament."
    toshu_t "その激しいトーナメントの後、週末が過ぎました。"

# game/script.rpy:10168
translate japanese ending_ine_mne_1aa1bffb:

    toshu_t "I want to know how my friends have been doing…"
    toshu_t "私の友達がどうしているのか知りたい"

# game/script.rpy:10176
translate japanese ending_ine_mne_2d78b941:

    toshu "I… it's pretty quiet around…"
    toshu "私は…周りはかなり静かです…"

# game/script.rpy:10178
translate japanese ending_ine_mne_b65efb71:

    toshu "It totally feels different without Ichiru or Masaru…"
    toshu "イチールやマサルなしで全く違う感じ…"

# game/script.rpy:10179
translate japanese ending_ine_mne_548b7b6a:

    toshu "Where is everyone…?"
    toshu "みんなはどこですか…？"

# game/script.rpy:10181
translate japanese ending_ine_mne_631ec800:

    toshu "Wait! Isn't that…!"
    toshu "待つ！それじゃない…！"

# game/script.rpy:10183
translate japanese ending_ine_mne_419c0495:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:10186
translate japanese ending_ine_mne_fc5cc731:

    toshu "I-Ichiru!"
    toshu "I-Ichiru!"

# game/script.rpy:10188
translate japanese ending_ine_mne_d0beb068:

    ichiru "T-Toshu…"
    ichiru "T-Toshu…"

# game/script.rpy:10189
translate japanese ending_ine_mne_2d21d20b:

    toshu "W-what's wrong, Ichiru?"
    toshu "名前：W何が間違っている、Ichiru？"

# game/script.rpy:10191
translate japanese ending_ine_mne_d78baa59:

    ichiru "*sniff*"
    ichiru "*スニフ*"

# game/script.rpy:10193
translate japanese ending_ine_mne_36fbd142:

    toshu "Why are you crying?!"
    toshu "なぜ泣いているのですか？"

# game/script.rpy:10194
translate japanese ending_ine_mne_5539909d:

    toshu "W-what happened?"
    toshu "W-何が起こったの？"

# game/script.rpy:10196
translate japanese ending_ine_mne_00ec623d:

    ichiru "I'm sorry, TOSHU!"
    ichiru "I'm sorry, TOSHU!"

# game/script.rpy:10197
translate japanese ending_ine_mne_e54a10f9:

    ichiru "I failed you!"
    ichiru "私はあなたに失敗しました！"

# game/script.rpy:10199
translate japanese ending_ine_mne_c5577f0c:

    ichiru "M-my transfer… dad's going to continue it…"
    ichiru "私の移籍…父親はそれを続けるつもりです…"

# game/script.rpy:10200
translate japanese ending_ine_mne_71a46452:

    ichiru "Whether I like it or not…"
    ichiru "私が好きかどうかは…"

# game/script.rpy:10202
translate japanese ending_ine_mne_f6d56761:

    toshu "Wh-What?! What do you mean?!"
    toshu "ホワ - 何？どういう意味ですか？！"

# game/script.rpy:10204
translate japanese ending_ine_mne_3f3d31ae:

    toshu "I thought if your dad--"
    toshu "あなたのお父さんが"

# game/script.rpy:10206
translate japanese ending_ine_mne_ae81368e:

    ichiru "I know!"
    ichiru "知っている！"

# game/script.rpy:10207
translate japanese ending_ine_mne_ae2e208e:

    ichiru "Tomoka's dad rejected my dad's request for reconsideration…"
    ichiru "トモカさんのお父さんが私のお父さんの再審査請求を拒否しました…"

# game/script.rpy:10208
translate japanese ending_ine_mne_b583d29e:

    ichiru "He threathened my dad by removing him from business partnership if he complained more…"
    ichiru "彼はもっと不平を言うなら、ビジネスパートナーシップから彼を取り除くことによって、私の父を脅かす…"

# game/script.rpy:10210
translate japanese ending_ine_mne_4506cb60:

    toshu "I… I c-can't believe this…"
    toshu "私は…信じられない"

# game/script.rpy:10212
translate japanese ending_ine_mne_018b031b:

    ichiru "I am as devastated as you, Toshu!"
    ichiru "私はあなたと同じくらい徹底している！"

# game/script.rpy:10213
translate japanese ending_ine_mne_b7f26f9f:

    ichiru "I never wanted to leave you guys…"
    ichiru "私はあなたたちを去ることは決してなかった…"

# game/script.rpy:10214
translate japanese ending_ine_mne_6a6e2efd:

    ichiru "I'm so sorry…"
    ichiru "ごめんなさい…"

# game/script.rpy:10216
translate japanese ending_ine_mne_06f86a82:

    ichiru "I… have to go…!"
    ichiru "私が行かなければならない…！"

# game/script.rpy:10218
translate japanese ending_ine_mne_57e39c08:

    toshu_t "I couldn't believe what Ichiru said…"
    toshu_t "Ichiruが言ったことを信じられませんでした…"

# game/script.rpy:10219
translate japanese ending_ine_mne_4fe85f52:

    toshu_t "Ichiru… my dear friend…"
    toshu_t "Ichiru …親愛なる友人…"

# game/script.rpy:10220
translate japanese ending_ine_mne_a9fb501a:

    toshu_t "We've been through a lot…"
    toshu_t "私たちはずっと経験してきました…"

# game/script.rpy:10221
translate japanese ending_ine_mne_bf8b3264:

    toshu_t "Why does he still have to leave…?"
    toshu_t "なぜ彼はまだ放置しなければならないのですか？"

# game/script.rpy:10228
translate japanese ending_ine_mne_aa5abefd:

    masaru "Toshu…!"
    masaru "Toshu…!"

# game/script.rpy:10230
translate japanese ending_ine_mne_5be77f88:

    toshu "Masaru! Is it true that Ichiru will leave?!"
    toshu "マサル！ Ichiruが残すのは本当ですか？"

# game/script.rpy:10232
translate japanese ending_ine_mne_e191807b:

    toshu "Please tell me that it isn't true!"
    toshu "それは本当ではないことを教えてください！"

# game/script.rpy:10234
translate japanese ending_ine_mne_be0c99b0:

    masaru "I'm sorry, Toshu…"
    masaru "I'm sorry, Toshu…"

# game/script.rpy:10235
translate japanese ending_ine_mne_787bb622:

    masaru "But I'm afraid he's telling the truth."
    masaru "しかし、私は彼が真実を伝えているのではないかと心配しています。"

# game/script.rpy:10237
translate japanese ending_ine_mne_c5264692:

    masaru "What's worse is…"
    masaru "悪いのは…"

# game/script.rpy:10239
translate japanese ending_ine_mne_69c8e003:

    masaru "I have to leave as well…"
    masaru "私も一緒に出発しなければならない…"

# game/script.rpy:10241
translate japanese ending_ine_mne_306fd507:

    toshu "W-wha…? Why? Why you too?!!"
    toshu "W-ワ？…？どうして？あなたもなぜですか？"

# game/script.rpy:10242
translate japanese ending_ine_mne_3c952e7c:

    masaru "My scholarship got completely terminated because I failed my exam… again."
    masaru "私の奨学金は私の試験に失敗したために完全に終了しました…再び。"

# game/script.rpy:10244
translate japanese ending_ine_mne_841f8f36:

    masaru "I don't have enough resources to continue studying here…"
    masaru "私はここで勉強を続けるのに十分なリソースがありません…"

# game/script.rpy:10246
translate japanese ending_ine_mne_fa1d52e7:

    genji "I really don't know what to do…"
    genji "私は本当に何をすべきかわからない…"

# game/script.rpy:10248
translate japanese ending_ine_mne_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:10250
translate japanese ending_ine_mne_fa4a54a0:

    masaru "The music club didn't accept me back either…"
    masaru "音楽クラブは私を受け入れなかった…"

# game/script.rpy:10252
translate japanese ending_ine_mne_1b1c820b:

    masaru "I will have to find a part time job first…"
    masaru "私はパートタイムの仕事を最初に見つけなければなりません…"

# game/script.rpy:10254
translate japanese ending_ine_mne_a9a688b8:

    masaru "I want to save money as soon as I can to be able to finish my studies…"
    masaru "私は私の研究を終えることができるようになるとすぐにお金を節約したいです…"

# game/script.rpy:10256
translate japanese ending_ine_mne_e1e5086d:

    toshu "I…"
    toshu "私…"

# game/script.rpy:10260
translate japanese ending_ine_mne_e67d0f7e:

    toshu "It's not fair!"
    toshu "それは公正ではない！"

# game/script.rpy:10261
translate japanese ending_ine_mne_95445740:

    toshu "Why do you two have to leave?!"
    toshu "なぜあなたは2つを残す必要がありますか？"

# game/script.rpy:10262
translate japanese ending_ine_mne_12ce9a9e:

    masaru "I'm really sorry, Toshu."
    masaru "私は本当に申し訳ありません、Toshu。"

# game/script.rpy:10263
translate japanese ending_ine_mne_1bd1a137:

    toshu "After all the times we spent together!"
    toshu "私たちは一緒に過ごしたすべての時間の後に！"

# game/script.rpy:10264
translate japanese ending_ine_mne_03c5e25e:

    toshu "I'm such a useless friend!"
    toshu "私はそのような無駄な友人です！"

# game/script.rpy:10265
translate japanese ending_ine_mne_8dea0d28:

    toshu "I couldn't do anything to prevent this from happening!"
    toshu "私はこれを防ぐために何もできませんでした！"

# game/script.rpy:10267
translate japanese ending_ine_mne_553f0905:

    toshu_t "The tears from my eyes refused to stop from falling…"
    toshu_t "私の目から涙が落ちるのを止めなかった…"

# game/script.rpy:10268
translate japanese ending_ine_mne_ff490676:

    toshu_t "I couldn't describe the sadness I felt…"
    toshu_t "私が感じた悲しみを説明できませんでした…"

# game/script.rpy:10269
translate japanese ending_ine_mne_439f849b:

    toshu_t "I've always said to my friends…"
    toshu_t "私はいつも私の友達に言った…"

# game/script.rpy:10270
translate japanese ending_ine_mne_3086fc3a:

    toshu_t "To never give up even when things goes bad…"
    toshu_t "物事が悪くなっても決してあきらめないために…"

# game/script.rpy:10271
translate japanese ending_ine_mne_abb3df94:

    toshu_t "But it looks like… this time…"
    toshu_t "しかし、今のように見える…"

# game/script.rpy:10272
translate japanese ending_ine_mne_086b4b99:

    toshu_t "I am the one… who will break down…"
    toshu_t "私はその人です…誰が倒れるでしょう…"
