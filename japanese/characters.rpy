define genji = Character(
    None,
    window_background = Frame("image/ui/text_genji.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="genji"
    )

define ichiru = Character(
    None,
    window_background = Frame("image/ui/text_ichiru.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="ichiru"
    )

define ichiru_c = Character(
    None,
    window_background = Frame("image/ui/text_ichiru.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="ichiru"
    )

define masaru = Character(
    None,
    window_background = Frame("image/ui/text_masaru.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="masaru"
    )

define masaru_c = Character(
    None,
    window_background = Frame("image/ui/text_masaru.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="masaru"
    )

define saki = Character(
    None,
    window_background = Frame("image/ui/text_saki.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="saki"
    )

define sohma = Character(
    None,
    window_background = Frame("image/ui/text_sohma.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="sohma"
    )

define tomoka = Character(
    None,
    window_background = Frame("image/ui/text_tomoka.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="tomoka"
    )

define toshu = Character(
    None,
    window_background = Frame("image/ui/text_toshu.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="toshu"
    )

define toshu_t = Character(
    None,
    window_background = Frame("image/ui/text_toshu.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    what_italic = True,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="toshu"
    )

define random = Character(
    None,
    window_background = Frame("image/ui/text_random.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="random"
    )

define random_c = Character(
    None,
    window_background = Frame("image/ui/text_random.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="random"
    )

define random_t = Character(
    None,
    window_background = Frame("image/ui/text_random.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    what_italic = True,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="random"
    )

define narrator = Character(
    None,
    window_background = Frame("image/ui/text_narrator.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="random"
    )

define ryota = Character(
    None,
    window_background = Frame("image/ui/text_ryota.png", 1198, 164),
    window_xminimum = 1198,
    window_xmaximum = 1198,
    window_yminimum = 164,
    window_ymaximum = 164,
    window_left_padding = 260,
    window_top_padding = 85,
    what_font = "tl/None/fonts/NotoSansCJKjp-Regular.otf",
    what_size = 20,
    what_xmaximum = 700,
    ctc=anim.Blink("tl/japanese/arrow.png"), image="ryota"
    )