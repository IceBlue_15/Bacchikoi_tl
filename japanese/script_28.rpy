# game/script.rpy:10881
translate japanese ending_ipe_mne_02c316a0:

    # toshu "*sigh*"
    toshu "はぁ……。"

# game/script.rpy:10883
translate japanese ending_ipe_mne_be38dbc2:

    # toshu "Masaru didn't contact ever since he left school…"
    toshu "Masaruさんが学校を去ってからずっと連絡が無い……。"

# game/script.rpy:10884
translate japanese ending_ipe_mne_606f6703:

    # toshu "I'm really worried about him…"
    toshu "本当に心配だよ……。"

# game/script.rpy:10886
translate japanese ending_ipe_mne_0d562fd8:

    # toshu "Will he really leave without properly saying goodbye…?"
    toshu "お別れの挨拶も無しにいなくなっちゃうのかな……？"

# game/script.rpy:10887
translate japanese ending_ipe_mne_f933c8ab:

    # toshu "I should stop thinking this way!"
    toshu "こんなこと考えてちゃダメだ！"

# game/script.rpy:10889
translate japanese ending_ipe_mne_f9a8c7d1:

    # toshu "Ichiru said I shouldn't think too much about this!"
    toshu "Ichiruもあまり考え過ぎるなって言ってたし！"

# game/script.rpy:10892
translate japanese ending_ipe_mne_e9b5b288:

    # toshu "A-auntie?"
    toshu "お、叔母さん？"

# game/script.rpy:10894
translate japanese ending_ipe_mne_44a3b751:

    # toshu "I'm coming!!"
    toshu "今行きます！！"

# game/script.rpy:10897
translate japanese ending_ipe_mne_37b877c9:

    # toshu "Auntie, I didn't know you will go home toda--"
    toshu "叔母さん、今日戻ってくるなんて知らなかっ──"

# game/script.rpy:10898
translate japanese ending_ipe_mne_60c63c3d:

    # random "It's me, Toshu."
    random "俺だよ、Toshu。"

# game/script.rpy:10899
translate japanese ending_ipe_mne_4d9ba6cc:

    # toshu "M…MASARU?!"
    toshu "MA……　MASARUさん？！"

# game/script.rpy:10907
translate japanese ending_ipe_mne_e40cb36b:

    # masaru "Hello, Toshu! I wanted to visit you!"
    masaru "久し振りだな、Toshu！　会いたかったぞ！"

# game/script.rpy:10909
translate japanese ending_ipe_mne_d66c9477:

    # masaru "I haven't seen or talked with you for a long time."
    masaru "話しをするのもしばらく振りだな。"

# game/script.rpy:10911
translate japanese ending_ipe_mne_88f45561:

    # masaru "I really missed you!"
    masaru "本当に寂しかったぞ！"

# game/script.rpy:10913
translate japanese ending_ipe_mne_68a7d1a4:

    # toshu "I missed you too…"
    toshu "僕も……　会いたかった……。"

# game/script.rpy:10915
translate japanese ending_ipe_mne_55028d93:

    # masaru "Toshu… I'm really sorry about last time…"
    masaru "Toshu……　前回のことは本当にゴメンな……。"

# game/script.rpy:10917
translate japanese ending_ipe_mne_89c331a2:

    # masaru "I was struggling to find a way to make my family survive…"
    masaru "俺は家族が生きていくために頑張っていたんだ……。"

# game/script.rpy:10918
translate japanese ending_ipe_mne_e5c04c53:

    # masaru "My little brother counts on me… that's why I'm really working hard to get a job then go back to studying."
    masaru "弟は俺のことを頼りにしている。だから俺は仕事を得るために本当に頑張ってから勉強に戻っていたんだ。"

# game/script.rpy:10921
translate japanese ending_ipe_mne_3e1fdc5b:

    # masaru "Believe me… I really broke down after that…"
    masaru "信じてくれ……　あれ以来、俺は本当に壊れちまってたんだ……。"

# game/script.rpy:10922
translate japanese ending_ipe_mne_4532a59d:

    # masaru "I needed time to calm myself down…"
    masaru "俺にはじっくり考える時間が必要だったんだ……。"

# game/script.rpy:10924
translate japanese ending_ipe_mne_364f49a2_2:

    # toshu "…"
    toshu "……。"

# game/script.rpy:10926
translate japanese ending_ipe_mne_83135bf4:

    # masaru "You know that I never really wanted to leave, right?"
    masaru "俺が本心では去りたくないって分かってるだろ？"

# game/script.rpy:10928
translate japanese ending_ipe_mne_3f867cc3:

    # masaru "I would never abandon such a special friend!"
    masaru "俺は特別な友達を見捨てるようなことはしない！"

# game/script.rpy:10931
translate japanese ending_ipe_mne_39036e98:

    # masaru "You're the one who taught me to never give up!"
    masaru "お前が俺に諦めないことを教えてくれたんだ！"

# game/script.rpy:10933
translate japanese ending_ipe_mne_2df03187:

    # masaru "You always listen to my problems!"
    masaru "お前はいつも俺の抱えてる問題に親身に耳を傾けてくれた！"

# game/script.rpy:10935
translate japanese ending_ipe_mne_69f9e533:

    # masaru "And you taught me the value of friendship!"
    masaru "そして俺に友情とは何かを教えてくれたんだ！"

# game/script.rpy:10936
translate japanese ending_ipe_mne_b9ae280c:

    # masaru "You made my life so much better, Toshu!"
    masaru "Toshu、お前と出会って俺の人生は変わったんだ。"

# game/script.rpy:10938
translate japanese ending_ipe_mne_de66d712:

    # masaru "I'll make a promise to you!"
    masaru "俺は約束するよ！"

# game/script.rpy:10940
translate japanese ending_ipe_mne_fe9790d7:

    # masaru "That even we're going on separate ways, we will still be the best of friends!"
    masaru "俺達は別々の道を行くけど、これからもずっと最高の友達だってな！"

# game/script.rpy:10942
translate japanese ending_ipe_mne_b0015100:

    # toshu "M…Masaru… *sniff*"
    toshu "Ma……　Masaru……。　（グスン）"

# game/script.rpy:10944
translate japanese ending_ipe_mne_a3486e43:

    # masaru "Goodbyes are not forever…"
    masaru "別れは永遠じゃない……。"

# game/script.rpy:10945
translate japanese ending_ipe_mne_0b9a4306:

    # ichiru "Goodbyes are not the end…"
    ichiru "別れは終わりじゃない……。"

# game/script.rpy:10947
translate japanese ending_ipe_mne_668eb78c:

    # masaru "It simply mean… I will miss you!"
    masaru "ただちょっと……　寂しくなるけどな！"

# game/script.rpy:10948
translate japanese ending_ipe_mne_553a2ec3:

    # masaru "We will meet again, for sure!"
    masaru "またきっといつか会おう！"

# game/script.rpy:10950
translate japanese ending_ipe_mne_be12d1d5:

    # toshu_t "In the end…"
    toshu_t "最終的には……"

# game/script.rpy:10951
translate japanese ending_ipe_mne_3a43e6e6:

    # toshu_t "The pain I felt from saying goodbye to Masaru was nothing compared…"
    toshu_t "Masaruさんにさよならを言って感じた痛みは……"

# game/script.rpy:10952
translate japanese ending_ipe_mne_f0f24371:

    # toshu_t "…from the happiness I felt from the friends I've made."
    toshu_t "……Masaruさんたちと出会って得られた幸福に比べれば何でもないものだった。"
