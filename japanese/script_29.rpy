# game/script.rpy:10963
translate japanese ending_ipe_mne_d046dcb9:

    toshu_t "It's been several weeks since Masaru left school."
    toshu_t "マサルが学校を去ってから数週間が経ちました。"

# game/script.rpy:10964
translate japanese ending_ipe_mne_a3053e08:

    toshu_t "Ichiru stood always by my side. We've been hanging out more than usual!"
    toshu_t "Ichiruはいつも私の側に立っていた。私たちはいつも以上にぶらぶらしていました！"

# game/script.rpy:10972
translate japanese ending_ipe_mne_a3a6f5ba:

    # toshu "Fuwaahh… it's weekend again… I'm so bored…"
    toshu "ふわあぁぁ……。もう週末か……。退屈だなぁ……。"

# game/script.rpy:10973
translate japanese ending_ipe_mne_1f4978a2:

    # toshu "I don't have anything to do…"
    toshu "何にもやる気がおきないや……。"

# game/script.rpy:10975
translate japanese ending_ipe_mne_3f9c0ddf:

    toshu "I wish Ichiru is with me everyday. It has really been fun hanging out together with him!"
    toshu "私はイチールが毎日私と一緒にいることを願っています。彼と一緒に遊ぶのは本当に面白かったです！"

# game/script.rpy:10978
translate japanese ending_ipe_mne_1e44abec:

    toshu "Hmm? I thought auntie went to work today?"
    toshu "うーん？今日はおばさんが仕事に行くと思った？"

# game/script.rpy:10981
translate japanese ending_ipe_mne_45497644:

    ichiru "Hi there, Toshu!"
    ichiru "Hi there, Toshu!"

# game/script.rpy:10983
translate japanese ending_ipe_mne_5eff0a80:

    # toshu "Ichiru?!"
    toshu "Ichiru？！"

# game/script.rpy:10985
translate japanese ending_ipe_mne_7001663d:

    ichiru "I have a super-special-awesome-news for you!"
    ichiru "私はあなたのために超特別な素晴らしいニュースを持っています！"

# game/script.rpy:10986
translate japanese ending_ipe_mne_15d970f2:

    # toshu "Wh-what is it…?"
    toshu "え、な、何……？"

# game/script.rpy:10988
translate japanese ending_ipe_mne_4bf6737f:

    ichiru "I finally had the guts to tell dad what I think about Tomoka!"
    ichiru "私はついにトモカについてどう思っているのか、お父さんに伝える勇気がありました！"

# game/script.rpy:10989
translate japanese ending_ipe_mne_471239b0:

    ichiru "My dad told her father how bad Tomoka has been behaving!"
    ichiru "私のお父さんはトモカがどんなに悪い行動をしているのか、彼女の父親に語った"

# game/script.rpy:10991
translate japanese ending_ipe_mne_4ab6914f:

    ichiru "She got a week full of scolding!"
    ichiru "彼女は叱責で一週間を過ごしました！"

# game/script.rpy:10992
translate japanese ending_ipe_mne_f9c5ac7d:

    ichiru "And guess what, she apologized to me!"
    ichiru "そして、彼女は私に謝った！"

# game/script.rpy:10994
translate japanese ending_ipe_mne_2be8bdd8:

    ichiru "She really sounded like a totally different person!"
    ichiru "彼女はまったく違う人のように聞こえました。"

# game/script.rpy:10995
translate japanese ending_ipe_mne_1ef75cd6:

    ichiru "And she wanted to apologize to you too!"
    ichiru "そして彼女はあなたにもあまりにも謝罪したかった！"

# game/script.rpy:10997
translate japanese ending_ipe_mne_7eccf7db:

    toshu "W-whoaaa!!!"
    toshu "インwhoaaa !!!"

# game/script.rpy:10999
translate japanese ending_ipe_mne_756d70e5:

    ichiru "I guess that tournament victory really did the final trick! Haha!"
    ichiru "私はトーナメントの勝利は本当に最後のトリックをしたと思います！ハハ！"

# game/script.rpy:11001
translate japanese ending_ipe_mne_46a7ed64:

    toshu "Ahh… Ichiru…"
    toshu "Ahh… Ichiru…"

# game/script.rpy:11002
translate japanese ending_ipe_mne_2dd4d28c:

    toshu "Speaking of the tournament…"
    toshu "トーナメントといえば…"

# game/script.rpy:11004
translate japanese ending_ipe_mne_018a8493:

    ichiru "Oh?"
    ichiru "ああ？"

# game/script.rpy:11005
translate japanese ending_ipe_mne_a774fa42:

    toshu "I've been thinking about what you told to me way back from the tournament…"
    toshu "私はあなたがトーナメントから帰ってきた方法について考えてきました…"

# game/script.rpy:11007
translate japanese ending_ipe_mne_d387bdfe:

    ichiru "Oh…!"
    ichiru "ああ…！"

# game/script.rpy:11010
translate japanese ending_ipe_mne_e8855e9a:

    toshu "Yeah… you said 'I love you' to me."
    toshu "ええ…あなたは私を愛しています。"

# game/script.rpy:11012
translate japanese ending_ipe_mne_a5fbe55e:

    ichiru "Aaa…"
    ichiru "Aaa …"

# game/script.rpy:11014
translate japanese ending_ipe_mne_df41a72d:

    toshu "Well, I never got the chance to properly respond."
    toshu "まあ、私は決して適切に対応するチャンスがありません。"

# game/script.rpy:11015
translate japanese ending_ipe_mne_5e391acc:

    toshu "I wanted to say that…"
    toshu "私はそれを言いたかった…"

# game/script.rpy:11017
translate japanese ending_ipe_mne_b2e264e1:

    toshu "I love you too, Ichiru!"
    toshu "私もあなたを愛し、Ichiru！"

# game/script.rpy:11019
translate japanese ending_ipe_mne_8c0ea632:

    ichiru "AAAA!!! TOSHU!!!"
    ichiru "AAAA!!! TOSHU!!!"

# game/script.rpy:11020
translate japanese ending_ipe_mne_49266351:

    ichiru "COME HERE!!!"
    ichiru "ここに来て！！！"

# game/script.rpy:11022
translate japanese ending_ipe_mne_dcd633f5:

    toshu_t "Once again, Ichiru's excitement filled the room."
    toshu_t "もう一度、Ichiruの興奮が部屋を満たした。"

# game/script.rpy:11023
translate japanese ending_ipe_mne_4cf3b30f:

    toshu_t "He leaped right over me and started stripping me and himself off."
    toshu_t "彼は私の上を飛び越えて、私と彼自身を剥ぎ取り始めました。"

# game/script.rpy:11024
translate japanese ending_ipe_mne_937c4804:

    toshu_t "But this time around, he was very gentle."
    toshu_t "しかし、今回はとても穏やかでした。"

# game/script.rpy:11025
translate japanese ending_ipe_mne_1c6bb0c9:

    toshu_t "After a sweet kiss… I knew what was coming next."
    toshu_t "甘いキスの後…私は次に来るものを知っていた。"

# game/script.rpy:11030
translate japanese ending_ipe_mne_2957161c:

    ichiru "Finally! My dream came true!"
    ichiru "最後に！私の夢が叶いました！"

# game/script.rpy:11031
translate japanese ending_ipe_mne_5bc2d0b1:

    toshu "D-dream, Ichiru?"
    toshu "D-夢、Ichiru？"

# game/script.rpy:11032
translate japanese ending_ipe_mne_58d3ddc4:

    ichiru "Yes! The moment that you will say that you love me too!"
    ichiru "はい！あなたが私を愛しているとも言える瞬間です！"

# game/script.rpy:11033
translate japanese ending_ipe_mne_49c90610:

    toshu "Of all the times we did it, I always held back myself from admitting that I did have feelings for you."
    toshu "私たちがやったすべての時間の中で、私はいつも私があなたの気持ちを持っていたことを認めないようにしていました。"

# game/script.rpy:11034
translate japanese ending_ipe_mne_69263ddc:

    toshu "But now it sounds all worth the wait!"
    toshu "しかし、今は待っている価値があると思うよ！"

# game/script.rpy:11035
translate japanese ending_ipe_mne_4660e07c:

    ichiru "I want to show you how much I really love you, Toshu!"
    ichiru "私はあなたが本当にあなたを愛していることをあなたに示すために、Toshu！"

# game/script.rpy:11036
translate japanese ending_ipe_mne_f37bd25f:

    ichiru "I can be a gentleman too!"
    ichiru "私も紳士になれます！"

# game/script.rpy:11038
translate japanese ending_ipe_mne_87005b2c:

    ichiru "Nnhh!"
    ichiru "こんにちは！"

# game/script.rpy:11039
translate japanese ending_ipe_mne_90a74b56:

    toshu "Hnn…!"
    toshu "Hnn …！"

# game/script.rpy:11040
translate japanese ending_ipe_mne_ffcb2f96:

    toshu_t "I tried to recollect in my mind the feeling before when Ichiru entered me…"
    toshu_t "イチルが私に入ったときの前の気持ちを思い出してみました…"

# game/script.rpy:11041
translate japanese ending_ipe_mne_5567948b:

    toshu_t "To be doing this with such an awesome and cheerful person like him… I feel really special!"
    toshu_t "彼のような素晴らしい、陽気な人とこれをやっている…私は本当に特別な気がする！"

# game/script.rpy:11042
translate japanese ending_ipe_mne_060cc5e9:

    ichiru "You're really hot inside, Toshu…!"
    ichiru "あなたは本当に暑いですね、Toshu…！"

# game/script.rpy:11043
translate japanese ending_ipe_mne_49d1b24e:

    toshu_t "Ichiru's warm breath dropped over my cheeks."
    toshu_t "イチールの温かい息が私の頬の上に落ちた。"

# game/script.rpy:11044
translate japanese ending_ipe_mne_9aeec61f:

    toshu_t "His thrusts were filled with passion and vigor."
    toshu_t "彼の衝動は情熱と活力で満たされました。"

# game/script.rpy:11046
translate japanese ending_ipe_mne_3aafb216:

    toshu "Nghh! Nghh!!"
    toshu "Nghh！ Nghh !!"

# game/script.rpy:11047
translate japanese ending_ipe_mne_332f7d82:

    ichiru "Of all the times we did this… I can finally do it with you without anything to worry about…"
    ichiru "いつも私たちはこれをやった…私は最終的に心配することなくあなたとそれをすることができます…"

# game/script.rpy:11048
translate japanese ending_ipe_mne_d3193a9a:

    ichiru "I really love you so much, Toshu!!!"
    ichiru "私は本当にあなたをとても愛しています。"

# game/script.rpy:11049
translate japanese ending_ipe_mne_918c5d4a:

    toshu_t "This time, Ichiru's pace went slower but even more powerful with each thrust."
    toshu_t "今回は、イチールのペースはスローになるが、スラストごとにさらに強力になった。"

# game/script.rpy:11050
translate japanese ending_ipe_mne_b42efd2b:

    toshu_t "I can feel him trembling inside me… Ichiru is reaching his limit!"
    toshu_t "私は彼が私の中で震えているのを感じることができます… Ichiruは彼の限界に達している！"

# game/script.rpy:11051
translate japanese ending_ipe_mne_5745570d:

    ichiru "Toshu…! I'm gonna--!"
    ichiru "Toshu…! I'm gonna--!"

# game/script.rpy:11053
translate japanese ending_ipe_mne_c87b58e8:

    toshu "Fuwaaah!!!"
    toshu "Fuwaaah !!!"

# game/script.rpy:11054
translate japanese ending_ipe_mne_6df0475e:

    toshu_t "Ichiru finally released his warm and thick liquid inside me. All of it made its way to my very being."
    toshu_t "Ichiruはやっと彼の暖かくて濃い液体を私の中に出した。すべてが私の存在に向かった。"

# game/script.rpy:11055
translate japanese ending_ipe_mne_2911f923:

    ichiru "Mnnhh!"
    ichiru "Mnnhh！"

# game/script.rpy:11057
translate japanese ending_ipe_mne_947499e2:

    ichiru "Haha! Toshu's all mine now!"
    ichiru "ハハ！ Toshuのすべて私の今！"

# game/script.rpy:11058
translate japanese ending_ipe_mne_10a8a655:

    toshu "Ichiru… that felt really great…"
    toshu "イチル…本当にすばらしいと感じた…"

# game/script.rpy:11059
translate japanese ending_ipe_mne_6b5394ec:

    ichiru "I told you I'm a gentleman!"
    ichiru "私はあなたが紳士だと言った！"

# game/script.rpy:11060
translate japanese ending_ipe_mne_41cc4006:

    ichiru "I really love you, Toshu!"
    ichiru "私は本当にあなたを愛しています！"

# game/script.rpy:11061
translate japanese ending_ipe_mne_3a8c29d3:

    toshu "I love you too, Ichiru!!!"
    toshu "私もあなたを愛して、Ichiru！"

# game/script.rpy:11065
translate japanese ending_ipe_mne_342c19db:

    toshu_t "Ichiru has officially become my boyfriend after that!"
    toshu_t "Ichiruはその後正式に私のボーイフレンドになった！"

# game/script.rpy:11066
translate japanese ending_ipe_mne_97e84569:

    toshu_t "My transfer to Yakyusha brought me to making such great friends!"
    toshu_t "ヤクシュサへの私の移転は私をこのような偉大な友人にしてくれました！"

# game/script.rpy:11067
translate japanese ending_ipe_mne_ce5a33af:

    toshu_t "But the biggest thing I'm grateful for…"
    toshu_t "しかし、私が感謝している最大のこと…"

# game/script.rpy:11068
translate japanese ending_ipe_mne_2c2040a8:

    toshu_t "…is meeting the special person who struck my heart!"
    toshu_t "…私の心を打つ特別な人に会うのです！"

translate japanese strings:

    # script.rpy:550
    old "BORROW ICHIRU'S CLOTHES"
    new "Ichiruのユニフォームを借りる"

    # script.rpy:550
    old "BORROW MASARU'S CLOTHES"
    new "Masaruのユニフォームを借りる"

    # script.rpy:550
    old "PLAY USING SCHOOL UNIFORM"
    new "学校の制服で練習する"

    # script.rpy:550
    old "PLAY NAKED"
    new "裸で練習する"

    # script.rpy:784
    old "I WANT TO TRAIN AS A BATTER"
    new "バッティングの練習がしたい"

    # script.rpy:784
    old "I WANT TO BE A CATCHER TOO!"
    new "僕もキャッチャーになりたい！"

    # script.rpy:784
    old "I WANT TO POLISH MY PITCHING SKILLS"
    new "ピッチングスキルを磨きたい"

    # script.rpy:784
    old "I WANT YOU TO TEACH ME COACH!"
    new "コーチに教えて欲しいです！"

    # script.rpy:986
    old "I WANT TO TAKE A SHOWER TOO"
    new "僕もシャワーを浴びたい"

    # script.rpy:986
    old "I'LL PRACTICE MORE OUTSIDE"
    new "外でもっと練習する"

    # script.rpy:986
    old "I'LL JUST CHANGE MY CLOTHES AS WELL"
    new "僕も着替えるだけにしよう"

    # script.rpy:986
    old "I WANT TO SEE YOU NAKED"
    new "裸が見たい"

    # script.rpy:1558
    old "I WANT TO GO HOME AS WELL… SORRY ICHIRU…"
    new "僕も家に帰りたい……ゴメンねIchiru……"

    # script.rpy:1558
    old "SURE ICHIRU, I'M PRETTY HUNGRY TOO"
    new "いいよIchiru、僕もお腹すいた"

    # script.rpy:1558
    old "CAN'T WE GO ALL TOGETHER?"
    new "一緒に行っちゃダメですか？"

    # script.rpy:1712
    old "I THINK I'LL GO FOR SWEETS TOO…"
    new "私はあまりにもすごく行くつもりだと思う…"

    # script.rpy:1712
    old "MEAT SOUNDS NICE!"
    new "肉の音がいい！"

    # script.rpy:1712
    old "PLEASE PICK FOR ME, ICHIRU"
    new "私、ICHIRUのためにピックアップしてください"

    # script.rpy:1712
    old "CAPTAIN, WHAT DO YOU SUGGEST?"
    new "キャプテン、あなたは何をすべきですか？"

    # script.rpy:2041
    old "WELL…"
    new "まあ…"

    # script.rpy:2041
    old "NOT REALLY!"
    new "あんまり！"

    # script.rpy:2041
    old "…"
    new "…"

    # script.rpy:2240
    old "SIT BESIDE COACH GENJI"
    new "Genjiコーチの隣に座る"

    # script.rpy:2240
    old "SIT BESIDE ICHIRU"
    new "Ichiruの隣に座る"

    # script.rpy:2240
    old "SIT BESIDE MASARU"
    new "Masaruの隣に座る"

    # script.rpy:3029
    old "LET COACH DECIDE"
    new "コーチの決定を下す"

    # script.rpy:3029
    old "TEAM UP WITH ICHIRU"
    new "Ichiruとチームを組む"

    # script.rpy:3029
    old "I CANNOT DECIDE WHO"
    new "誰にするか決められない"

    # script.rpy:3029
    old "TEAM UP WITH MASARU"
    new "Masaruとチームを組む"

    # script.rpy:4044
    old "CAPTAIN MASARU'S BODY IS SO GROWN UP!"
    new "Masaruキャプテンの体はとても立派です！"

    # script.rpy:4044
    old "ICHIRU YOUR HAIR LOOKS SO COOL!"
    new "Ichiruの髪はとても素敵だよ！"

    # script.rpy:4044
    old "I WANNA GROW LIKE CAPTAIN MASARU!"
    new "Masaruキャプテンみたいになりたい！"

    # script.rpy:4044
    old "ICHIRU YOUR SKIN IS SO SMOOTH!"
    new "Ichiru、君の肌はとてもきれいだよ！"

    # script.rpy:4313
    old "I'M NOT FINISHED SCRUBBING CAPTAIN'S BACK!"
    new "キャプテンの背中を流し終わってません！"

    # script.rpy:4313
    old "I WANT TO STAY HERE A BIT MORE"
    new "もうちょっとここにいたい"

    # script.rpy:4313
    old "BUT WHAT ABOUT CAPTAIN MASARU?"
    new "キャプチャについて何ですか？"

    # script.rpy:4313
    old "OKAY ICHIRU, I WILL GO WITH YOU!"
    new "いいよIchiru、一緒に行こう！"

    # script.rpy:4471
    old "BUT ICHIRU, I AM NOT FINISHED TAKING A BATH"
    new "でもIchiru、私は入浴を終えていない"

    # script.rpy:4471
    old "OKAY ICHIRU, I WILL GO WITH YOU"
    new "いいよIchiru、一緒に行こう。"

    # script.rpy:5223
    old "CAPTAIN, PLEASE CHEER UP!"
    new "キャプテン、元気を出してください！"

    # script.rpy:5223
    old "CAPTAIN, DON'T BE SAD!"
    new "キャプテン、悲しまないで！"

    # script.rpy:5223
    old "YEAH! GOOD JOB ICHIRU!"
    new "よ！良い仕事ICHIRU！"

    # script.rpy:5223
    old "WE SHOULD'VE HELPED CAPTAIN WITH HIS STUDIES…"
    new "私たちは彼の研究に助けを借りなければなりません…"

    # script.rpy:5925
    old "ON SECOND THOUGHT…"
    new "二番目に考えて…"

    # script.rpy:5925
    old "BUT…"
    new "しかし…"

    # script.rpy:5925
    old "YEAH, I WILL CALL HIM!"
    new "ああ、私は彼に電話します！"

    # script.rpy:5925
    old "I GUESS I WILL GIVE HIM A CALL THEN"
    new "私は彼に電話すると思う"

    # script.rpy:7144
    old "THE TOURNAMENT IS CLOSE!"
    new "トーナメントが終わりました！"

    # script.rpy:7144
    old "BUT I HAVE TO STUDY FOR FINAL EXAMS…"
    new "しかし、私は最終試験のために勉強しなければなりません…"

    # script.rpy:7144
    old "WHAT DID I TELL YOU ABOUT SKIPPING WORK?"
    new "スキップ作業について教えてください。"

    # script.rpy:7144
    old "MAYBE WE CAN FOCUS WITH OUR STUDIES INSTEAD?"
    new "私たちは私たちの研究に焦点を当てることができますか？"

    # script.rpy:7350
    old "YOU CAN DO IT TOO!"
    new "あなたもそれをやることができます！"

    # script.rpy:7350
    old "DON'T GIVE UP!"
    new "あきらめてはいけない！"

    # script.rpy:7350
    old "DON'T COMPARE YOURSELF!"
    new "あなた自身を比較しないでください！"

    # script.rpy:7350
    old "AT LEAST YOU'RE SMOKING HOT!"
    new "少なくともあなたは喫煙しています！"

    # script.rpy:7518
    old "I SHOULD GO TO THE BASEBALL FIELD!"
    new "私は野球場に行くべきです！"

    # script.rpy:7518
    old "I WANT TO VISIT THE CLASSROOM!"
    new "私は教室に行きたいです！"

    # script.rpy:7518
    old "I MUST PRACTICE FOR THE TOURNAMENT!"
    new "私はトーナメントのために練習しなければなりません！"

    # script.rpy:7518
    old "I HAVE TO STUDY HARDER!"
    new "私は勉強しなければならない！"

    # script.rpy:8420
    old "I WILL FOLLOW HIM AND COMFORT HIM!"
    new "私は彼に従ってくれるでしょう。"

    # script.rpy:8420
    old "MASARU LOOKS LIKE HE'S IN TROUBLE…"
    new "MASARUは彼が悩んでいるように見える…"

    # script.rpy:9354
    old "I SHOULD LEAVE GIVE HIM TIME TO BE ALONE!"
    new "私は離れるべき時を過ごすべきです！"
