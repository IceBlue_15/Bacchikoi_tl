# game/script.rpy:9699
translate japanese ending_ine_mbe_4760d0aa:

    toshu_t "The weekend has passed after that intense tournament."
    toshu_t "その激しいトーナメントの後、週末が過ぎました。"

# game/script.rpy:9700
translate japanese ending_ine_mbe_1aa1bffb:

    toshu_t "I want to know how my friends have been doing…"
    toshu_t "私の友達がどうしているのか知りたい"

# game/script.rpy:9708
translate japanese ending_ine_mbe_2d78b941:

    toshu "I… it's pretty quiet around…"
    toshu "私は…周りはかなり静かです…"

# game/script.rpy:9710
translate japanese ending_ine_mbe_b65efb71:

    toshu "It totally feels different without Ichiru or Masaru…"
    toshu "イチールやマサルなしで全く違う感じ…"

# game/script.rpy:9711
translate japanese ending_ine_mbe_548b7b6a:

    toshu "Where is everyone…?"
    toshu "みんなはどこですか…？"

# game/script.rpy:9713
translate japanese ending_ine_mbe_631ec800:

    toshu "Wait! Isn't that…!"
    toshu "待つ！それじゃない…！"

# game/script.rpy:9715
translate japanese ending_ine_mbe_419c0495:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:9718
translate japanese ending_ine_mbe_fc5cc731:

    toshu "I-Ichiru!"
    toshu "I-Ichiru!"

# game/script.rpy:9720
translate japanese ending_ine_mbe_d0beb068:

    ichiru "T-Toshu…"
    ichiru "T-Toshu…"

# game/script.rpy:9721
translate japanese ending_ine_mbe_2d21d20b:

    toshu "W-what's wrong, Ichiru?"
    toshu "名前：W何が間違っている、Ichiru？"

# game/script.rpy:9723
translate japanese ending_ine_mbe_d78baa59:

    ichiru "*sniff*"
    ichiru "*スニフ*"

# game/script.rpy:9725
translate japanese ending_ine_mbe_36fbd142:

    toshu "Why are you crying?!"
    toshu "なぜ泣いているのですか？"

# game/script.rpy:9726
translate japanese ending_ine_mbe_5539909d:

    toshu "W-what happened?"
    toshu "W-何が起こったの？"

# game/script.rpy:9728
translate japanese ending_ine_mbe_00ec623d:

    ichiru "I'm sorry, TOSHU!"
    ichiru "I'm sorry, TOSHU!"

# game/script.rpy:9729
translate japanese ending_ine_mbe_e54a10f9:

    ichiru "I failed you!"
    ichiru "私はあなたに失敗しました！"

# game/script.rpy:9731
translate japanese ending_ine_mbe_c5577f0c:

    ichiru "M-my transfer… dad's going to continue it…"
    ichiru "私の移籍…父親はそれを続けるつもりです…"

# game/script.rpy:9732
translate japanese ending_ine_mbe_71a46452:

    ichiru "Whether I like it or not…"
    ichiru "私が好きかどうかは…"

# game/script.rpy:9734
translate japanese ending_ine_mbe_f6d56761:

    toshu "Wh-What?! What do you mean?!"
    toshu "ホワ - 何？どういう意味ですか？！"

# game/script.rpy:9736
translate japanese ending_ine_mbe_3f3d31ae:

    toshu "I thought if your dad--"
    toshu "あなたのお父さんが"

# game/script.rpy:9738
translate japanese ending_ine_mbe_ae81368e:

    ichiru "I know!"
    ichiru "知っている！"

# game/script.rpy:9739
translate japanese ending_ine_mbe_ae2e208e:

    ichiru "Tomoka's dad rejected my dad's request for reconsideration…"
    ichiru "トモカさんのお父さんが私のお父さんの再審査請求を拒否しました…"

# game/script.rpy:9740
translate japanese ending_ine_mbe_b583d29e:

    ichiru "He threathened my dad by removing him from business partnership if he complained more…"
    ichiru "彼はもっと不平を言うなら、ビジネスパートナーシップから彼を取り除くことによって、私の父を脅かす…"

# game/script.rpy:9742
translate japanese ending_ine_mbe_4506cb60:

    toshu "I… I c-can't believe this…"
    toshu "私は…信じられない"

# game/script.rpy:9744
translate japanese ending_ine_mbe_018b031b:

    ichiru "I am as devastated as you, Toshu!"
    ichiru "私はあなたと同じくらい徹底している！"

# game/script.rpy:9745
translate japanese ending_ine_mbe_b7f26f9f:

    ichiru "I never wanted to leave you guys…"
    ichiru "私はあなたたちを去ることは決してなかった…"

# game/script.rpy:9746
translate japanese ending_ine_mbe_6a6e2efd:

    ichiru "I'm so sorry…"
    ichiru "ごめんなさい…"

# game/script.rpy:9748
translate japanese ending_ine_mbe_06f86a82:

    ichiru "I… have to go…!"
    ichiru "私が行かなければならない…！"

# game/script.rpy:9750
translate japanese ending_ine_mbe_57e39c08:

    toshu_t "I couldn't believe what Ichiru said…"
    toshu_t "Ichiruが言ったことを信じられませんでした…"

# game/script.rpy:9751
translate japanese ending_ine_mbe_4fe85f52:

    toshu_t "Ichiru… my dear friend…"
    toshu_t "Ichiru …親愛なる友人…"

# game/script.rpy:9752
translate japanese ending_ine_mbe_a9fb501a:

    toshu_t "We've been through a lot…"
    toshu_t "私たちはずっと経験してきました…"

# game/script.rpy:9753
translate japanese ending_ine_mbe_bf8b3264:

    toshu_t "Why does he still have to leave…?"
    toshu_t "なぜ彼はまだ放置しなければならないのですか？"

# game/script.rpy:9761
translate japanese ending_ine_mbe_8fbaf59f:

    genji "Oh, h…hello Toshu."
    genji "Oh, h…hello Toshu."

# game/script.rpy:9763
translate japanese ending_ine_mbe_672bd564:

    toshu "C-Coach!!"
    toshu "Cコーチ!!"

# game/script.rpy:9764
translate japanese ending_ine_mbe_20fdb08e:

    toshu "Did you hear the news about Ichiru?"
    toshu "イチールについてのニュースを聞いたことがありますか？"

# game/script.rpy:9766
translate japanese ending_ine_mbe_e191807b:

    toshu "Please tell me that it isn't true!"
    toshu "それは本当ではないことを教えてください！"

# game/script.rpy:9768
translate japanese ending_ine_mbe_75a12fcd:

    genji "I'm sorry, Toshu…"
    genji "I'm sorry, Toshu…"

# game/script.rpy:9769
translate japanese ending_ine_mbe_7938a828:

    genji "But I'm afraid he's telling the truth."
    genji "しかし、私は彼が真実を伝えているのではないかと心配しています。"

# game/script.rpy:9770
translate japanese ending_ine_mbe_beedc66e:

    genji "What's worse is…"
    genji "悪いのは…"

# game/script.rpy:9772
translate japanese ending_ine_mbe_c5af37f5:

    genji "Masaru… said that… he would also have to leave…"
    genji "マサルも言った…彼はまた出なければならないだろう…"

# game/script.rpy:9773
translate japanese ending_ine_mbe_75c11ed7:

    genji "After this semester… he can't continue anymore…"
    genji "この学期の後…彼はもはや続けることができません…"

# game/script.rpy:9775
translate japanese ending_ine_mbe_c09a1861:

    toshu "W-wha…? Why? Why Masaru too?!!"
    toshu "W-ワ？…？どうして？なぜマサルなの？"

# game/script.rpy:9777
translate japanese ending_ine_mbe_6b8b8e68:

    genji "His scholarship got completely terminated after failing that remedial exam."
    genji "彼の奨学金は、その修復試験に失敗した後、完全に終了しました。"

# game/script.rpy:9778
translate japanese ending_ine_mbe_fdefb178:

    genji "I don't have enough resources to continue helping him either…"
    genji "私は彼を助けるのに十分な資源がありません…"

# game/script.rpy:9780
translate japanese ending_ine_mbe_fa1d52e7:

    genji "I really don't know what to do…"
    genji "私は本当に何をすべきかわからない…"

# game/script.rpy:9782
translate japanese ending_ine_mbe_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:9784
translate japanese ending_ine_mbe_1b2f5d13:

    genji "The music club didn't accept him anymore too…"
    genji "音楽クラブはもはや彼を受け入れなかった…"

# game/script.rpy:9785
translate japanese ending_ine_mbe_f8bb2b87:

    genji "He… will have to find a part time job first…"
    genji "彼は…パートタイムの仕事を最初に見つけなければならないでしょう…"

# game/script.rpy:9786
translate japanese ending_ine_mbe_00062083:

    genji "He told me he wants to save money to finish his studies…"
    genji "彼は自分の研究を終えるためにお金を節約したいと私に言った…"

# game/script.rpy:9788
translate japanese ending_ine_mbe_e1e5086d:

    toshu "I…"
    toshu "私…"

# game/script.rpy:9792
translate japanese ending_ine_mbe_e67d0f7e:

    toshu "It's not fair!"
    toshu "それは公正ではない！"

# game/script.rpy:9793
translate japanese ending_ine_mbe_81cd8af3:

    toshu "Why both of them have to leave?!"
    toshu "なぜ彼らの両方を残す必要がありますか？"

# game/script.rpy:9794
translate japanese ending_ine_mbe_c19dd638:

    genji "I'm really sorry, Toshu."
    genji "私は本当に申し訳ありません、Toshu。"

# game/script.rpy:9795
translate japanese ending_ine_mbe_14ed6880:

    toshu "After all the time we spent together…"
    toshu "一緒に過ごした後も…"

# game/script.rpy:9797
translate japanese ending_ine_mbe_553f0905:

    toshu_t "The tears from my eyes refused to stop from falling…"
    toshu_t "私の目から涙が落ちるのを止めなかった…"

# game/script.rpy:9798
translate japanese ending_ine_mbe_ff490676:

    toshu_t "I couldn't describe the sadness I felt…"
    toshu_t "私が感じた悲しみを説明できませんでした…"

# game/script.rpy:9799
translate japanese ending_ine_mbe_439f849b:

    toshu_t "I've always said to my friends…"
    toshu_t "私はいつも私の友達に言った…"

# game/script.rpy:9800
translate japanese ending_ine_mbe_3086fc3a:

    toshu_t "To never give up even when things goes bad…"
    toshu_t "物事が悪くなっても決してあきらめないために…"

# game/script.rpy:9801
translate japanese ending_ine_mbe_abb3df94:

    toshu_t "But it looks like… this time…"
    toshu_t "しかし、今のように見える…"

# game/script.rpy:9802
translate japanese ending_ine_mbe_086b4b99:

    toshu_t "I am the one… who will break down…"
    toshu_t "私はその人です…誰が倒れるでしょう…"
