# game/script.rpy:1951
translate japanese choice_4_A_end_9f433c84:

    # ichiru "The class was boring as ever!"
    ichiru "いつもながら授業ってつまんねーな！"

# game/script.rpy:1952
translate japanese choice_4_A_end_f17b5bca:

    # ichiru "I miss skipping class…"
    ichiru "今日はサボりそこねちまったぜ…"

# game/script.rpy:1953
translate japanese choice_4_A_end_29515b31:

    # toshu "Is going to class really that bad?? Also, skipping class might cause you to have failing grades!"
    toshu "授業ってそんなにつまんないかな？？ サボってばかりだと成績が落ちちゃうよ！"

# game/script.rpy:1955
translate japanese choice_4_A_end_561ea7d6:

    # ichiru "I'm not really worried about my grades that much."
    ichiru "成績については心配してねーよ。"

# game/script.rpy:1957
translate japanese choice_4_A_end_64a55190:

    # ichiru "The only reason I attend class now, is because you're my classmate!"
    ichiru "俺が授業に出るのはお前がいるからだよ！"

# game/script.rpy:1959
translate japanese choice_4_A_end_fd9d9913:

    # toshu "I'm not sure if I should be happy about that."
    toshu "それって喜んでいいのかな？"

# game/script.rpy:1961
translate japanese choice_4_A_end_6f4ac28a:

    toshu "But still that shouldn't be your reason to go to class! You have to take care of your grades!"
    toshu "しかし、それはあなたの授業に行く理由ではありません！あなたはあなたの成績の世話をする必要があります！"

# game/script.rpy:1963
translate japanese choice_4_A_end_24f206de:

    ichiru "Meh, Let's go to the field, It's already sundown!"
    ichiru "Meh、フィールドに行こう、それはすでに日没です！"

# game/script.rpy:1972
translate japanese choice_4_A_end_481a4f76:

    # masaru "Oh there you are, you two."
    masaru "ああ二人ともそこにいたのか。"

# game/script.rpy:1974
translate japanese choice_4_A_end_747f1655:

    # toshu "Hi Captain!"
    toshu "キャプテン、こんにちは！"

# game/script.rpy:1976
translate japanese choice_4_A_end_b22deef9:

    # ichiru "Sounds like you were looking for us?"
    ichiru "俺たちのこと探してたの？"

# game/script.rpy:1978
translate japanese choice_4_A_end_f963c268:

    # masaru "Yeah, I came to tell you guys that baseball practice today has been cancelled."
    masaru "ああ、今日の練習が中止になったことを伝えに来たんだ。"

# game/script.rpy:1980
translate japanese choice_4_A_end_53e4cef0:

    # ichiru "EHHH?! I was looking forward for today's training!"
    ichiru "ええ～？！ 今日の練習楽しみにしてたのに！"

# game/script.rpy:1982
translate japanese choice_4_A_end_e45b370d:

    ichiru "It's my only time I can hang out with Tofu without being quiet all the time."
    ichiru "いつも静かではなく、Tofuと遊ぶことができるのは私の唯一の時間です。"

# game/script.rpy:1984
translate japanese choice_4_A_end_55a432b3:

    # toshu "Why was it cancelled, Captain?"
    toshu "どうして中止になったんですか、キャプテン？"

# game/script.rpy:1986
translate japanese choice_4_A_end_59f796af:

    masaru "Well, Coach told me he has schedule today as a private tutor for one of his preschool students…"
    masaru "コーチは、今日、就学前の生徒のプライベートチューターとしてスケジュールを決めています…"

# game/script.rpy:1988
translate japanese choice_4_A_end_6db1dee6:

    ichiru "Tch… that creepo."
    ichiru "Tch …そのクリーポ。"

# game/script.rpy:1990
translate japanese choice_4_A_end_657e8bb4:

    masaru "Though, he assigned me to just pass out the news for the club members."
    masaru "しかし、彼は私にクラブ会員のためのニュースを渡すように任命しました。"

# game/script.rpy:1992
translate japanese choice_4_A_end_0d16610a:

    # toshu "News?"
    toshu "お知らせ？"

# game/script.rpy:1994
translate japanese choice_4_A_end_d1dcb59c:

    # masaru "We're going on a field trip."
    masaru "練習合宿をすることになった。"

# game/script.rpy:1996
translate japanese choice_4_A_end_da730841:

    # toshu "WOOOOW!!"
    toshu "うわぁ！"

# game/script.rpy:1997
translate japanese choice_4_A_end_f48948be:

    masaru "There's gonna be a training camp on the mountains outside of the city."
    masaru "街の外の山々にはトレーニングキャンプがあります。"

# game/script.rpy:1998
translate japanese choice_4_A_end_92e1de32:

    masaru "We'll be there for 3 days and 2 nights straight. Here are the waivers he asked me to be signed by your parents."
    masaru "私たちは3日間と2泊まっすぐにそこにいます。彼が私にあなたの両親の署名を求めた権利放棄はここにあります。"

# game/script.rpy:1999
translate japanese choice_4_A_end_5dd4e51a:

    toshu "WOW WOW WOW! This is too exciting!"
    toshu "WOW WOW WOW！これはあまりにもエキサイティングです！"

# game/script.rpy:2001
translate japanese choice_4_A_end_1a30ed49:

    masaru "Here are the waivers. Make sure you get your guardian's signature there, alright?"
    masaru "権利放棄はここにあります。あなたのガーディアンの署名があることを確認してください、よろしいですか？"

# game/script.rpy:2003
translate japanese choice_4_A_end_99403c26:

    ichiru "Awww… why does there have to be a waiver? That sucks…"
    ichiru "A …なぜ免除が必要ですか？それは吸う…"

# game/script.rpy:2004
translate japanese choice_4_A_end_2cc9d0fe:

    masaru "I'm not sure what are the exact details of the training there. Let's just hope Coach will announce it later."
    masaru "私はそこでのトレーニングの正確な内容が何であるか分かりません。コーチが後でそれを発表することを願っています。"

# game/script.rpy:2006
translate japanese choice_4_A_end_1cf917d3:

    ichiru "This is probably one of that GAYJI's lame schemes again…"
    ichiru "これはおそらくゲイジのラメのスキームの1つです…"

# game/script.rpy:2008
translate japanese choice_4_A_end_de3bc1a2:

    # toshu "But! I love field trips! I'm sure it's gonna be fun, especially with you guys!"
    toshu "でも僕、合宿って大好き！ みんなとならきっと楽しくなるね！"

# game/script.rpy:2010
translate japanese choice_4_A_end_6d8fb680:

    # ichiru "Ehh? Really?"
    ichiru "え？ マジか？"

# game/script.rpy:2012
translate japanese choice_4_A_end_cc0d2c77:

    # ichiru "In that case, I love fieldtrips too! I'm excited as well!!"
    ichiru "じゃあ俺も合宿大好き！ ワクワクするぜ！！"

# game/script.rpy:2014
translate japanese choice_4_A_end_e6442047:

    # masaru "…that was a quick turnaround, Ichiru."
    masaru "…気が変わるの早いな、Ichiru。"

# game/script.rpy:2016
translate japanese choice_4_A_end_3a4dc68b:

    # masaru "Anyway, I have to go now, I have a club meeting to attend to."
    masaru "じゃ、クラブ会議があるから俺はそろそろ行くぞ。"

# game/script.rpy:2019
translate japanese choice_4_A_end_cf372772:

    # toshu "Club meeting? I thought the coach isn't present?"
    toshu "クラブ会議？ コーチはいないんじゃなかったでしたっけ？"

# game/script.rpy:2021
translate japanese choice_4_A_end_8eb5f93c:

    ichiru "Ahh, he's keeping it a secret, but he actually is in another club as well, not just baseball."
    ichiru "ああ、彼はそれを秘密にしているが、彼は実際に野球だけでなく、別のクラブにもいる。"

# game/script.rpy:2023
translate japanese choice_4_A_end_b62642b8:

    ichiru "I guess he slipped his tongue eh?"
    ichiru "彼は舌を滑ったと思う？"

# game/script.rpy:2025
translate japanese choice_4_A_end_cca2211f:

    toshu "Ehh? What other club is it?"
    toshu "え？それ以外のクラブは何ですか？"

# game/script.rpy:2027
translate japanese choice_4_A_end_1be62fb8:

    ichiru "Don't tell him I told you, but he is a member of the music club, he is actually a pianist!"
    ichiru "私はあなたに言った彼に言うことはありませんが、彼は音楽クラブのメンバーです、彼は実際にピアニストです！"

# game/script.rpy:2029
translate japanese choice_4_A_end_0c47b5c6:

    toshu "Ohh!! Really?"
    toshu "ああ！本当に？"

# game/script.rpy:2031
translate japanese choice_4_A_end_9a8e7f46:

    toshu "Why keep it a secret?"
    toshu "なぜそれを秘密にしておきますか？"

# game/script.rpy:2033
translate japanese choice_4_A_end_94028173:

    ichiru "Truth is, you're the only one who doesn't know about this yet. He doesn't want you to know for who-knows-what reason."
    ichiru "真実は、あなたはまだこれについて知りません唯一のものです。彼はあなたが誰を知ってもらうことを望んでいません。"

# game/script.rpy:2035
translate japanese choice_4_A_end_49bdfa8a:

    ichiru "He is so lame right?"
    ichiru "彼はあまりにも不自然ですか？"

# game/script.rpy:2057
translate japanese choice_6_A_eff57dfd:

    toshu "W-well…"
    toshu "うーん…"

# game/script.rpy:2058
translate japanese choice_6_A_2f1e0846:

    ichiru "Umm… not exactly lame…! I'm sorry!"
    ichiru "うーん… …ごめんなさい！"

# game/script.rpy:2060
translate japanese choice_6_A_900d0f4f:

    ichiru "I'm sure it's hard for him to be in two clubs at once!"
    ichiru "私は彼が一度に2つのクラブにいることは難しいと確信しています！"

# game/script.rpy:2061
translate japanese choice_6_A_da73306f:

    ichiru "I really shouldn't have said that!"
    ichiru "私は本当にそれを言ってはいけません！"

# game/script.rpy:2063
translate japanese choice_6_A_5ee9bad0:

    toshu "Yeah, he must really like both music and baseball."
    toshu "ええ、彼は本当に音楽と野球の両方を好きでなければなりません。"

# game/script.rpy:2069
translate japanese choice_6_B_fa337a09:

    toshu "I think it's awesome!"
    toshu "私はそれが素晴らしいと思います！"

# game/script.rpy:2071
translate japanese choice_6_B_02970ca6:

    ichiru "Eh? Why so?"
    ichiru "え？なぜそうなのか？"

# game/script.rpy:2072
translate japanese choice_6_B_ee0972ec:

    toshu "It must be hard for him to be in two clubs at once!"
    toshu "彼が一度に2つのクラブにいるのは難しいでしょう！"

# game/script.rpy:2073
translate japanese choice_6_B_06035e00:

    toshu "He must really be passionate in both music and baseball."
    toshu "彼は音楽と野球の両方で本当に情熱的でなければなりません。"

# game/script.rpy:2075
translate japanese choice_6_B_21d75ad7:

    ichiru "Ah… I really shouldn't have said that!"
    ichiru "ああ…私は本当に言ってはいけません！"

# game/script.rpy:2079
translate japanese choice_6_C_22ad9fa9:

    ichiru "Ah! Of course I'm just kidding! Don't take it seriously neh, Tofu?"
    ichiru "ああ！もちろん私は冗談だよ！真剣にねえ、Tofu？"

# game/script.rpy:2081
translate japanese choice_6_C_900d0f4f:

    ichiru "I'm sure it's hard for him to be in two clubs at once!"
    ichiru "私は彼が一度に2つのクラブにいることは難しいと確信しています！"

# game/script.rpy:2083
translate japanese choice_6_C_5ee9bad0:

    toshu "Yeah, he must really like both music and baseball."
    toshu "ええ、彼は本当に音楽と野球の両方を好きでなければなりません。"

# game/script.rpy:2089
translate japanese choice_6_end_7642db96:

    toshu "In fact, I really like music."
    toshu "実際、私は本当に音楽が好きです。"

# game/script.rpy:2091
translate japanese choice_6_end_8b62719d:

    toshu "My mom always played the piano for me!"
    toshu "私の母はいつも私のためにピアノを演奏しました！"

# game/script.rpy:2093
translate japanese choice_6_end_73b7088f:

    ichiru "Oh, you never really told me about your parents yet."
    ichiru "ああ、あなたは本当にあなたの両親について私にまだ言ったことはありません。"

# game/script.rpy:2095
translate japanese choice_6_end_85a633d3:

    toshu "My parents? Well… I haven't seen them for a very long time."
    toshu "私の両親？まあ…私は非常に長い時間それらを見ていない。"

# game/script.rpy:2097
translate japanese choice_6_end_beb636ba:

    toshu "They're overseas, leaving me at our house with my aunt, sending me monthly allowance."
    toshu "彼らは海外です、私の叔母と私の家に私を残し、私に月額手当を送りました。"

# game/script.rpy:2098
translate japanese choice_6_end_db43997c:

    toshu "Though my aunt's pretty busy, she does take care of me a lot!"
    toshu "私の叔母はかなり忙しいですが、彼女は私を大事にしてくれます！"

# game/script.rpy:2099
translate japanese choice_6_end_2fc026fc:

    toshu "I think I was five when I last saw them."
    toshu "私は最後に彼らを見たときに私が5歳だったと思う。"

# game/script.rpy:2101
translate japanese choice_6_end_712f805e:

    toshu "I remember my mother used to play me songs in the piano when I was about to sleep."
    toshu "私は眠っているときに母がピアノで歌を演奏していたことを覚えています。"

# game/script.rpy:2102
translate japanese choice_6_end_40ae8d03:

    toshu "It's very relaxing, and I miss that feeling…"
    toshu "とてもリラックスしていて、その気持ちが恋しい…"

# game/script.rpy:2103
translate japanese choice_6_end_9ff27f41:

    ichiru "How about your dad?"
    ichiru "あなたのお父さんはどうですか？"

# game/script.rpy:2104
translate japanese choice_6_end_61c39d7a:

    toshu "My dad's abroad, with mom."
    toshu "私のお父さんは母親と一緒に海外にいる。"

# game/script.rpy:2106
translate japanese choice_6_end_82e8aadf:

    ichiru "I'm pretty sure it feels lonely without your parents around…"
    ichiru "私はあなたの両親がいなくても寂しいと感じていると確信しています…"

# game/script.rpy:2108
translate japanese choice_6_end_53c6ab56:

    toshu "No, not really! I understand why they have to be there."
    toshu "いいえ、本当は！彼らがなぜそこにいなければならないのか理解しています。"

# game/script.rpy:2110
translate japanese choice_6_end_3eb2f94f:

    ichiru "Looks like we both got problem with parents, huh?"
    ichiru "両親に問題があるように見えますか？"

# game/script.rpy:2112
translate japanese choice_6_end_6cbce851:

    toshu "Ehh? What do you mean Ichiru?"
    toshu "え？イチールって何？"

# game/script.rpy:2114
translate japanese choice_6_end_b53f100c:

    ichiru "My parents are worse. They're right here in the country but they're always busy with something too."
    ichiru "私の両親は悪いです。彼らはここにいるのですが、いつも何かで忙しいです。"

# game/script.rpy:2116
translate japanese choice_6_end_b5db175a:

    toshu "Oh…"
    toshu "ああ…"

# game/script.rpy:2118
translate japanese choice_6_end_b99664cb:

    ichiru "Nah, nevermind… it's no big deal."
    ichiru "ナー、それは大したことじゃない。"

# game/script.rpy:2120
translate japanese choice_6_end_4f6fe875:

    ichiru "Anyway, since there's no baseball practice today, there'd be nothing to do now."
    ichiru "とにかく今日は野球の練習がないので、今は何もしません。"

# game/script.rpy:2123
translate japanese choice_6_end_74ad47ca:

    ichiru "Are you excited about the fieldtrip Masaru told us about?"
    ichiru "Masaruが私たちに語ったフィールドトリップに興奮していますか？"

# game/script.rpy:2125
translate japanese choice_6_end_53928038:

    toshu "YES!! I'm totally excited for the field trip!!! I can't stop thinking how much fun we will have there!"
    toshu "はい！！私はフィールドトリップに全く興奮しています！私はそこにどれくらいの楽しみがあるか考えないでおくことができます！"

# game/script.rpy:2127
translate japanese choice_6_end_ab12e1c8:

    ichiru "It's still about a week ahead, don't get too excited."
    ichiru "まだ1週間先ですが、あまりにも興奮しないでください。"

# game/script.rpy:2129
translate japanese choice_6_end_38be2a06:

    toshu "I CAN'T HELP IT!"
    toshu "私はそれを助けることはできません！"

# game/script.rpy:2131
translate japanese choice_6_end_c0ffc1d6:

    ichiru "Aww~ Aren't you cute when you're excited?"
    ichiru "ああ〜あなたが興奮しているときにかわいいですか？"

# game/script.rpy:2133
translate japanese choice_6_end_5f7eae1e:

    ichiru "But for now, we should go home and get plenty of rest."
    ichiru "しかし今のところ、家に帰って十分な休息を取るべきです。"

# game/script.rpy:2135
translate japanese choice_6_end_5bc42484:

    ichiru "Don't forget to get enough sleep and study well, okay?"
    ichiru "十分な睡眠と勉強を忘れないでください。"

# game/script.rpy:2136
translate japanese choice_6_end_552352d6:

    ichiru "The exams are coming after the camping days!"
    ichiru "試験はキャンプの日の後に来ます！"

# game/script.rpy:2138
translate japanese choice_6_end_d54de157:

    toshu "Thank you, Ichiru!"
    toshu "イチールありがとう！"

# game/script.rpy:2139
translate japanese choice_6_end_3c1d9a89:

    ichiru "Okay then, see ya later!"
    ichiru "それでは、後で見てください！"

# game/script.rpy:2141
translate japanese choice_6_end_33ea56b6:

    toshu_t "I didn't realize Ichiru has problems with his family…"
    toshu_t "イチールが家族に問題があることは分かりませんでした…"

# game/script.rpy:2142
translate japanese choice_6_end_df0ce114:

    toshu_t "I wonder what is it exactly about?"
    toshu_t "それはどういうことなのだろう？"

# game/script.rpy:2143
translate japanese choice_6_end_6bc20c9a:

    toshu_t "He's still obviously not ready to tell it to me."
    toshu_t "彼はまだ明らかに私にそれを伝える準備ができていない。"

# game/script.rpy:2144
translate japanese choice_6_end_9452adfe:

    toshu_t "I guess I should just wait for him to share it with me when he's ready…"
    toshu_t "私は彼が準備ができたら私にそれを分かち合うのを待つべきだと思う…"

# game/script.rpy:2145
translate japanese choice_6_end_805deee5:

    toshu_t "Anyway…"
    toshu_t "とにかく…"

# game/script.rpy:2146
translate japanese choice_6_end_cf732661:

    toshu_t "I was way too excited for the entire week."
    toshu_t "私は一週間のうちにとても興奮していた。"

# game/script.rpy:2147
translate japanese choice_6_end_e5728f8f:

    toshu_t "There was nothing on my mind aside from that field trip…"
    toshu_t "そのフィールドトリップを除いて私の心には何もなかった…"

# game/script.rpy:2148
translate japanese choice_6_end_0e822b67:

    toshu_t "So excited… that…"
    toshu_t "とても興奮しています…その…"

# game/script.rpy:2149
translate japanese choice_6_end_7c4218dd:

    toshu_t "…I barely got any sleep."
    toshu_t "…私はほとんど眠れませんでした。"
