# game/script.rpy:5088
translate japanese choice_9_end_5645a6c9:

    toshu_t "Three weeks have passed since our camping trip."
    toshu_t "私たちのキャンプ旅行から3週間が経過しました。"

# game/script.rpy:5089
translate japanese choice_9_end_e686ab39:

    toshu_t "And since then, it looks like everything went out great!"
    toshu_t "それ以来、すべてのことがうまくいったように見えます！"

# game/script.rpy:5090
translate japanese choice_9_end_26b99019:

    toshu_t "We took our exams. And I am proud to say that I think I did well!"
    toshu_t "私たちは試験を受けました。そして、私はうまくやったと私は誇りに思っています！"

# game/script.rpy:5098
translate japanese choice_9_end_68d34c15:

    toshu_t "Ichiru and Masaru have been hanging out with me quite alot after the field trip!"
    toshu_t "イチルとマサルは、フィールドトリップの後に私とかなり一緒に遊んでいる！"

# game/script.rpy:5100
translate japanese choice_9_end_aae45cd9:

    toshu_t "But… the past week, they seemed a bit off…"
    toshu_t "しかし…先週、彼らはちょっと離れていたようでした…"

# game/script.rpy:5101
translate japanese choice_9_end_dc7d7ffb:

    toshu_t "And, distant for some reason…"
    toshu_t "そして、何らかの理由で遠くに…"

# game/script.rpy:5102
translate japanese choice_9_end_5cc12e93:

    toshu_t "With Ichiru skipping classes again, and Captain Masaru rarely attending baseball practice… I'm not sure…"
    toshu_t "Ichiruが再びクラスをスキップし、Masaru大尉がめったに野球の練習に通っていないと…私は確信していません…"

# game/script.rpy:5103
translate japanese choice_9_end_a89a4195:

    toshu_t "Maybe they're just both busy."
    toshu_t "たぶん彼らはどちらも忙しそうです。"

# game/script.rpy:5104
translate japanese choice_9_end_8050d0ba:

    toshu_t "Ichiru's family is a well known name and Masaru's little brother need him too!"
    toshu_t "イチールの家族はよく知られている名前で、マサルの弟も彼を必要としています！"

# game/script.rpy:5106
translate japanese choice_9_end_32ad73f2:

    toshu_t "Eh… what's with all the commotion today?"
    toshu_t "ええと…今日はどんな騒ぎがあるの？"

# game/script.rpy:5107
translate japanese choice_9_end_41143abe:

    toshu_t "People are gathering up in front of the bulletin board."
    toshu_t "人々は掲示板の前に集まっている。"

# game/script.rpy:5109
translate japanese choice_9_end_49ce1074:

    ichiru "YO!!~"
    ichiru "YO !!〜"

# game/script.rpy:5111
translate japanese choice_9_end_80ab5433:

    toshu "Ichiru! I haven't seen you for a while!"
    toshu "Ichiru！私はしばらくあなたを見ていない！"

# game/script.rpy:5113
translate japanese choice_9_end_15b7d668:

    toshu "You promised me you won't skip classes anymore!"
    toshu "あなたはもうクラスをスキップしないことを約束しました！"

# game/script.rpy:5115
translate japanese choice_9_end_e565fd27:

    toshu "Did anything happen?"
    toshu "何か起こったのですか？"

# game/script.rpy:5117
translate japanese choice_9_end_4bf1c100:

    ichiru "W-well… I'm really sorry about that."
    ichiru "まあ…それは本当に残念です。"

# game/script.rpy:5118
translate japanese choice_9_end_ba3e5732:

    ichiru "I knew I promised you I will attend class but…"
    ichiru "私は授業に出席することを約束したが、…"

# game/script.rpy:5120
translate japanese choice_9_end_dfba8bcf:

    ichiru "I just had some stuff to fix with my family."
    ichiru "私はちょうど私の家族と一緒に修正するいくつかのものを持っていた。"

# game/script.rpy:5122
translate japanese choice_9_end_b2f5e4fc:

    toshu "Ahhh… so I was right… you were indeed busy with your family."
    toshu "ああ…だから私は正しい…あなたは本当にあなたの家族と忙しかった。"

# game/script.rpy:5124
translate japanese choice_9_end_307b2017:

    ichiru "Forget about that! How about you?"
    ichiru "それを忘れる！あなたはどう？"

# game/script.rpy:5126
translate japanese choice_9_end_ca717138:

    toshu "Hmm. I'm fine as always."
    toshu "うーん。私はいつものように大丈夫です。"

# game/script.rpy:5128
translate japanese choice_9_end_71c984b1:

    toshu "But I have to admit… it sure was really quiet without you and Captain around…"
    toshu "しかし、私は認めなければなりません…あなたとキャプテンがいなくても本当に静かでした…"

# game/script.rpy:5130
translate japanese choice_9_end_10dec85d:

    ichiru "Masaru?"
    ichiru "Masaru?"

# game/script.rpy:5132
translate japanese choice_9_end_c17d4815:

    toshu "Yeah, I haven't heard of him much this past week."
    toshu "ええ、私はこの先週、彼にはあまり聞いていません。"

# game/script.rpy:5133
translate japanese choice_9_end_80962528:

    ichiru "Maybe he's busy with his other club, or with his little brother."
    ichiru "たぶん彼は彼の他のクラブ、または彼の弟と忙しそうです。"

# game/script.rpy:5135
translate japanese choice_9_end_417eb72e:

    toshu "Yeah, I thought of that too!"
    toshu "ええ、私はそれも考えました！"

# game/script.rpy:5137
translate japanese choice_9_end_8414c400:

    toshu "By the way, what's happening there over the bulletin board?"
    toshu "ところで、掲示板上で何が起こっているのですか？"

# game/script.rpy:5139
translate japanese choice_9_end_8f46d734:

    ichiru "Oh, they're probably looking at the exam results."
    ichiru "ああ、彼らはおそらく試験の結果を見ている。"

# game/script.rpy:5140
translate japanese choice_9_end_cf7d9d23:

    ichiru "I came to school today just to check out my scores."
    ichiru "私は今日私のスコアをチェックするために学校に来ました。"

# game/script.rpy:5142
translate japanese choice_9_end_12f06e09:

    toshu "Ahh! I wanna see mine too!"
    toshu "ああ！私も私の見たいです！"

# game/script.rpy:5144
translate japanese choice_9_end_eba1eb11:

    toshu "Wait, you came for school just for that?"
    toshu "待って、ちょうどそのために学校に来た？"

# game/script.rpy:5146
translate japanese choice_9_end_44619e82:

    ichiru "Yes, after that, I have to tell my parents about my grades."
    ichiru "はい、その後、私は私の成績について両親に言わなければなりません。"

# game/script.rpy:5148
translate japanese choice_9_end_a11d5b1e:

    toshu "Oh, I see…"
    toshu "ああなるほど…"

# game/script.rpy:5150
translate japanese choice_9_end_5d9d0e03:

    ichiru "Ohh! Isn't that Masaru?"
    ichiru "ああ！マサルって？"

# game/script.rpy:5152
translate japanese choice_9_end_f55aa6ce:

    ichiru "MASARUUU!!!"
    ichiru "MASARUUU!!!"

# game/script.rpy:5154
translate japanese choice_9_end_183091e9:

    masaru "Oh, hey you guys."
    masaru "ああ、お元気ですか。"

# game/script.rpy:5156
translate japanese choice_9_end_8fb0dc91:

    toshu "How are you, Captain?"
    toshu "さて、キャプテン？"

# game/script.rpy:5158
translate japanese choice_9_end_1166ffb6:

    toshu "Where have you been? I haven't seen you for a while."
    toshu "あなたはどこにいた？私はしばらくあなたを見ていない。"

# game/script.rpy:5160
translate japanese choice_9_end_a3e26768:

    masaru "Sorry I haven't attended much of our baseball practices lately."
    masaru "申し訳ありませんが、私は最近野球の練習の多くに参加していません。"

# game/script.rpy:5161
translate japanese choice_9_end_d60ae6a1:

    masaru "I was busy studying for exams."
    masaru "私は試験のために勉強していました。"

# game/script.rpy:5163
translate japanese choice_9_end_75279583:

    toshu "Really??!~ I'm sure your grades are super high!"
    toshu "本当に??！〜私はあなたの成績が最高だと確信しています！"

# game/script.rpy:5165
translate japanese choice_9_end_9a4a51d8:

    masaru "Well… you guys can see for yourselves."
    masaru "まあ…あなたたちはあなた自身のために見ることができます。"

# game/script.rpy:5167
translate japanese choice_9_end_d8aefd5d:

    ichiru "Let's check out our grades then!"
    ichiru "当時の成績をチェックしよう！"

# game/script.rpy:5175
translate japanese choice_9_end_79bd1233:

    toshu "Let's see…"
    toshu "どれどれ…"

# game/script.rpy:5176
translate japanese choice_9_end_503a79b5:

    toshu "I…ICHIRU??!!!"
    toshu "I…ICHIRU??!!!"

# game/script.rpy:5177
translate japanese choice_9_end_1ab4e75d:

    toshu "ICHIRU! Your grade is… PERFECT!"
    toshu "いちゆ！あなたの学年は…完璧！"

# game/script.rpy:5178
translate japanese choice_9_end_90a1642b:

    toshu "You're TOP 1 of the list!"
    toshu "あなたはリストのTOP 1です！"

# game/script.rpy:5179
translate japanese choice_9_end_dd6c04dc:

    ichiru "Hehe, thank you."
    ichiru "Hehe、ありがとう。"

# game/script.rpy:5180
translate japanese choice_9_end_ad9332d5:

    ichiru "Your grades are pretty high too, Toshu."
    ichiru "あなたの成績もかなり高いです。"

# game/script.rpy:5181
translate japanese choice_9_end_6e3b90a2:

    toshu "You guys told me to focus, so really, thanks to you guys!"
    toshu "あなたたちは集中するように私に言った、本当に皆さんのおかげで！"

# game/script.rpy:5182
translate japanese choice_9_end_b880b873:

    ichiru "Anyway, I… don't see your name Masaru."
    ichiru "とにかく、私はあなたの名前マサルを見ません。"

# game/script.rpy:5183
translate japanese choice_9_end_b7c6e0d2:

    toshu "Y-yeah… did they make a mistake or anything…?"
    toshu "Y-yeah …彼らは間違いや何かをしたのですか…？"

# game/script.rpy:5184
translate japanese choice_9_end_617ccdc1_1:

    # masaru "…"
    masaru "…"

# game/script.rpy:5190
translate japanese choice_9_end_87cd870b:

    masaru "Students who didn't pass the examination are not put on the list…"
    masaru "試験に合格しなかった学生はリストに載っていません…"

# game/script.rpy:5192
translate japanese choice_9_end_5ae831cc:

    toshu "E-EHH!!??"
    toshu "E-EHH !! ??"

# game/script.rpy:5194
translate japanese choice_9_end_8aa888ba:

    toshu "C-Captain!"
    toshu "C-キャプテン！"

# game/script.rpy:5195
translate japanese choice_9_end_b767fcff:

    toshu "I thought you were studying the past week? What happened?!"
    toshu "先週あなたが勉強していると思った？何が起こった？！"

# game/script.rpy:5197
translate japanese choice_9_end_9fc1c96e:

    masaru "I… I did… but--"
    masaru "私は…私はやった…しかし -"

# game/script.rpy:5199
translate japanese choice_9_end_0879de87:

    toshu "T-that can't be right! We must tell the faculty staff about this!"
    toshu "T-それは正しいことができない！私たちはこれについて教員のスタッフに伝えなければなりません！"

# game/script.rpy:5200
translate japanese choice_9_end_7ca2cbba:

    masaru "No, you got it all wrong, Toshu!"
    masaru "いいえ、あなたはそれがすべて間違っている、東武！"

# game/script.rpy:5202
translate japanese choice_9_end_666676a6:

    masaru "I'm such an idiot."
    masaru "私はそんなにばかです。"

# game/script.rpy:5204
translate japanese choice_9_end_26548537:

    ichiru "Masaru…"
    ichiru "Masaru…"

# game/script.rpy:5206
translate japanese choice_9_end_d6da2012:

    masaru "Don't worry about it, you guys!"
    masaru "それについて心配しないで、皆さん！"

# game/script.rpy:5208
translate japanese choice_9_end_fbf3ea45:

    masaru "We should celebrate, Ichiru got the highest score!"
    masaru "私たちは祝うべきです、Ichiruは最高の得点を得ました！"

# game/script.rpy:5209
translate japanese choice_9_end_de8380ee:

    masaru "No one can ever beat him in anything!"
    masaru "誰も彼を何とか打ちのめすことはできません！"

# game/script.rpy:5210
translate japanese choice_9_end_488eee8a:

    ichiru "B-but…"
    ichiru "B-しかし…"

# game/script.rpy:5212
translate japanese choice_9_end_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:5214
translate japanese choice_9_end_ff425948:

    masaru "Good job Ichiru!"
    masaru "Good job Ichiru!"

# game/script.rpy:5216
translate japanese choice_9_end_6b4083d6:

    ichiru "Ahh… thanks… I guess…?"
    ichiru "ああ…ありがとう…私は推測する…？"

# game/script.rpy:5240
translate japanese choice_12_A_2b5949ea:

    toshu "Captain? Are you okay?"
    toshu "キャプテン？大丈夫ですか？"

# game/script.rpy:5241
translate japanese choice_12_A_144d7b82:

    toshu "Please, cheer up!"
    toshu "さあ、応援してください！"

# game/script.rpy:5243
translate japanese choice_12_A_375d812f:

    ichiru "Yeah… m-maybe you can just catch up in the final exam--"
    ichiru "ええと…多分あなたは最終試験にちょうど追いつくことができます -"

# game/script.rpy:5245
translate japanese choice_12_A_0f537c65:

    masaru "It's not that…"
    masaru "そうではありませんが…"

# game/script.rpy:5246
translate japanese choice_12_A_560945f0:

    masaru "I…"
    masaru "私…"

# game/script.rpy:5251
translate japanese choice_12_B_2b5949ea:

    toshu "Captain? Are you okay?"
    toshu "キャプテン？大丈夫ですか？"

# game/script.rpy:5252
translate japanese choice_12_B_2be679bc:

    toshu "Please don't be sad anymore."
    toshu "もう悲しくならないでください。"

# game/script.rpy:5254
translate japanese choice_12_B_ebf9f77c:

    toshu "Besides, Ichiru grades are really high maybe we should go out together and celebrate!"
    toshu "それに、イチルの成績は本当に高いですね。一緒に出かけて祝うべきかもしれない！"

# game/script.rpy:5260
translate japanese choice_12_C_66fad93d:

    toshu "Yeah, Ichiru! Great job!"
    toshu "うん、Ichiru！よくやった！"

# game/script.rpy:5262
translate japanese choice_12_C_be449fc7:

    ichiru "Hahah! I'm so awesome right!"
    ichiru "ハハ！私はとても素晴らしいです！"

# game/script.rpy:5268
translate japanese choice_12_D_95b77aa1:

    toshu "Ichiru we should've helped Captain with his studies…"
    toshu "私たちはキャプテンの勉強を助けたはずです…"

# game/script.rpy:5270
translate japanese choice_12_D_22307837:

    ichiru "Masaru didn't approach me…"
    ichiru "マサルは私に近づかなかった…"

# game/script.rpy:5272
translate japanese choice_12_D_e620121c:

    ichiru "I would've gladly taught you if you asked me."
    ichiru "あなたが私に尋ねたなら、喜んで教えてくれたでしょう。"

# game/script.rpy:5274
translate japanese choice_12_D_5eed8ef5:

    toshu "I didn't know Captain was having trouble with his studies."
    toshu "キャプテンが勉強に困っていることは分かりませんでした。"

# game/script.rpy:5276
translate japanese choice_12_D_375d812f:

    ichiru "Yeah… m-maybe you can just catch up in the final exam--"
    ichiru "ええと…多分あなたは最終試験にちょうど追いつくことができます -"

# game/script.rpy:5278
translate japanese choice_12_D_0f537c65:

    masaru "It's not that…"
    masaru "そうではありませんが…"

# game/script.rpy:5279
translate japanese choice_12_D_560945f0:

    masaru "I…"
    masaru "私…"

# game/script.rpy:5290
translate japanese route_masaru_5_e83aa6f1:

    masaru "I always FAIL!"
    masaru "私はいつもFAIL！"

# game/script.rpy:5291
translate japanese route_masaru_5_4e213e3e:

    masaru "I can't do anything right!"
    masaru "私は何もできない！"

# game/script.rpy:5293
translate japanese route_masaru_5_3f6d1810:

    masaru "Every examination period, no matter how I try, I can't pass my exams…"
    masaru "すべての試験期間は、私がどのように試しても、私は私の試験に合格できません…"

# game/script.rpy:5294
translate japanese route_masaru_5_b390819d:

    masaru "My remarks are failed…"
    masaru "私の発言は失敗しました…"

# game/script.rpy:5295
translate japanese route_masaru_5_8899c7ce:

    masaru "If the school notices my standing, I won't know what to do…"
    masaru "学校が私の立場に気づいたら、私は何をすべきか分からない…"

# game/script.rpy:5297
translate japanese route_masaru_5_3c30e8fd:

    toshu "Ehh…? But Captain, it's not your fault!"
    toshu "エ…？でもキャプテン、それはあなたのせいじゃない！"

# game/script.rpy:5299
translate japanese route_masaru_5_18378f74:

    toshu "You did your best."
    toshu "あなたは最善を尽くしました。"

# game/script.rpy:5301
translate japanese route_masaru_5_fdacd64b:

    masaru "But I already did my best. Apparently it's not enough!"
    masaru "しかし、私はすでにベストを尽くしました。明らかにそれだけでは不十分です！"

# game/script.rpy:5303
translate japanese route_masaru_5_2239b6ea:

    masaru "If I fail, I will be--"
    masaru "私が失敗すれば、私は -"

# game/script.rpy:5304
translate japanese route_masaru_5_92a6a983:

    random "Big Brother!!"
    random "ビッグブラザー!!"

# game/script.rpy:5307
translate japanese route_masaru_5_f5cafe8c:

    masaru "S-Sohma!"
    masaru "S-Sohma！"

# game/script.rpy:5308
translate japanese route_masaru_5_30a16713:

    masaru "What happened to your face?!"
    masaru "あなたの顔には何が起こったのですか？"

# game/script.rpy:5310
translate japanese route_masaru_5_ee462dce:

    sohma "I-it's nothing, Big Brother! I j-just trip!"
    sohma "私は何もない、ビッグブラザー！私はちょうど旅行！"

# game/script.rpy:5312
translate japanese route_masaru_5_33b5e497:

    masaru "Trip? Look at you… you have bandages all over your face."
    masaru "旅行？あなたを見てください…あなたは包帯を全面に持っています。"

# game/script.rpy:5314
translate japanese route_masaru_5_5de777bb:

    masaru "And what are you doing here? Don't you have classes?"
    masaru "あなたはここで何をしていますか？クラスはありませんか？"

# game/script.rpy:5315
translate japanese route_masaru_5_d31dad9b:

    sohma "Teacher put us on recess, so I went straight to you."
    sohma "教師は私たちを休憩室に入れました。私はあなたにまっすぐ行きました。"

# game/script.rpy:5317
translate japanese route_masaru_5_24dfdff7:

    masaru "Ahh, where are my manners?"
    masaru "ああ、私のマナーはどこですか？"

# game/script.rpy:5318
translate japanese route_masaru_5_25285524:

    masaru "Guys, this is my little brother Sohma."
    masaru "みんな、これは私の弟、ソーマです。"

# game/script.rpy:5320
translate japanese route_masaru_5_c67c344b:

    sohma "Pleasure to meet you big-bro's friends!"
    sohma "ビッグブロムの友達にお会いできたら嬉しいです！"

# game/script.rpy:5322
translate japanese route_masaru_5_c3475c29:

    sohma "I know Ichiru already! Who's the new one?"
    sohma "私は既にイチルを知っている！新しい人は誰ですか？"

# game/script.rpy:5324
translate japanese route_masaru_5_d7b24acd:

    toshu "H-hi…"
    toshu "H-hi …"

# game/script.rpy:5326
translate japanese route_masaru_5_a0aa9c5a:

    ichiru "Hmmm…"
    ichiru "うーん…"

# game/script.rpy:5328
translate japanese route_masaru_5_4e9d785a:

    sohma "By the way Big Brother, you left your lunch at home."
    sohma "ところでビッグブラザー、あなたは自宅で昼食を食べた。"

# game/script.rpy:5330
translate japanese route_masaru_5_24d59ce5:

    masaru "Thanks Sohma."
    masaru "ありがとうSohma。"

# game/script.rpy:5332
translate japanese route_masaru_5_7ed13fac:

    masaru "You didn't have to go all that trouble just for this."
    masaru "あなたはこれだけの問題をすべて解決する必要はありませんでした。"

# game/script.rpy:5334
translate japanese route_masaru_5_4346d388:

    masaru "I'm really worried about your face… those wounds really look painful."
    masaru "私は本当にあなたの顔が心配です…その傷は本当に痛いです。"

# game/script.rpy:5336
translate japanese route_masaru_5_b0d24769:

    masaru "I'll take a closer look at them when we get home, alright?"
    masaru "家に帰ると、私は彼らをよく見ていきます。"

# game/script.rpy:5338
translate japanese route_masaru_5_5b13c360:

    sohma "It's really nothing, Big Brother!"
    sohma "それは本当に何もない、ビッグブラザー！"

# game/script.rpy:5341
translate japanese route_masaru_5_e0b2ac2a:

    ichiru "Sohma? Are you sure, you're okay?"
    ichiru "Sohma？本当ですか、あなたは大丈夫ですか？"

# game/script.rpy:5343
translate japanese route_masaru_5_3ddccc05:

    sohma "I… umm is there something wrong Big Brother's friend?"
    sohma "私は…ええ、ビッグブラザーの友達が何か間違っていますか？"

# game/script.rpy:5345
translate japanese route_masaru_5_9dfc7827:

    ichiru "You didn't trip, did you?"
    ichiru "あなたは旅行しませんでしたか？"

# game/script.rpy:5346
translate japanese route_masaru_5_fb416445:

    sohma "Wh- what do you mean?"
    sohma "あなたはどういう意味ですか？"

# game/script.rpy:5347
translate japanese route_masaru_5_767e711f:

    ichiru "If you really tripped, you would've gotten a cut instead of a bruise."
    ichiru "あなたが本当にうまくいけば、打撲の代わりに切れたでしょう。"

# game/script.rpy:5349
translate japanese route_masaru_5_b83fd4e1:

    ichiru "I don't see any blood from your bandages. What I see is purple skin."
    ichiru "私はあなたの包帯からの血液は見ません。私が見るものは紫色の肌です。"

# game/script.rpy:5351
translate japanese route_masaru_5_40aed596:

    # sohma "…"
    sohma "…"

# game/script.rpy:5353
translate japanese route_masaru_5_cf6933ae:

    masaru "Sohma? Is what Ichiru saying, true?"
    masaru "Sohma？ Ichiruが言っていることは、本当ですか？"

# game/script.rpy:5355
translate japanese route_masaru_5_3d333059:

    sohma "Y-yes…"
    sohma "Y-はい…"

# game/script.rpy:5357
translate japanese route_masaru_5_06623bf3:

    masaru "Sohma! What did I tell you about lying to me?!"
    masaru "Sohma！私はあなたに私に嘘をついていることを教えてくれましたか？"

# game/script.rpy:5359
translate japanese route_masaru_5_e3d8925c:

    sohma "B-but!"
    sohma "B-but！"

# game/script.rpy:5361
translate japanese route_masaru_5_30881b9e:

    sohma "Waahhh!!!"
    sohma "ワハ！"

# game/script.rpy:5362
translate japanese route_masaru_5_97dac364:

    sohma "I'm really sorry, Big Brother! My classmate punched me…!"
    sohma "私は本当に申し訳ありません、ビッグブラザー！私の同級生が私を殴った…！"

# game/script.rpy:5364
translate japanese route_masaru_5_a63126c3:

    masaru "A-again?"
    masaru "もう一度？"

# game/script.rpy:5366
translate japanese route_masaru_5_1adab4fc:

    masaru "Didn't we talk to your teacher about that already?"
    masaru "すでに先生に話していませんでしたか？"

# game/script.rpy:5368
translate japanese route_masaru_5_e64597ba:

    sohma "But, but, Big Brother! It's not his fault…!"
    sohma "しかし、しかし、ビッグブラザー！それは彼のせいじゃない…！"

# game/script.rpy:5369
translate japanese route_masaru_5_42213aa0:

    sohma "I was the reason why the group failed…"
    sohma "私はグループが失敗した理由だった…"

# game/script.rpy:5371
translate japanese route_masaru_5_f3940277:

    sohma "I really deserve what they did to--"
    sohma "彼らがやったことに本当に値する。"

# game/script.rpy:5373
translate japanese route_masaru_5_a8455b5f:

    masaru "SOHMA!"
    masaru "ソーマ！"

# game/script.rpy:5375
translate japanese route_masaru_5_ad1cadfb:

    sohma "Waaahh…!"
    sohma "Waaahh …！"

# game/script.rpy:5377
translate japanese route_masaru_5_14c7c979:

    masaru "How can we prove to dad we can live independently?!"
    masaru "私たちが独立して暮らすことができる父親に、どうして証明できますか？"

# game/script.rpy:5379
translate japanese route_masaru_5_09911727:

    masaru "You have to learn how to man up and stick up for yourself!"
    masaru "あなたは人を育て、自分のために頑張る方法を学ばなければなりません！"

# game/script.rpy:5380
translate japanese route_masaru_5_7f44dd3b:

    masaru "Your big bro won't always be there beside you and save you when things go wrong!"
    masaru "あなたの大きな仲間はいつもあなたのそばにいるわけではなく、物事がうまくいかないときにあなたを救うでしょう！"

# game/script.rpy:5382
translate japanese route_masaru_5_44b2f80c:

    ichiru "H-hey, Masaru, calm down a bit."
    ichiru "名前：ちょっと、大丈夫、少し落ち着いてください。"

# game/script.rpy:5384
translate japanese route_masaru_5_e36543db:

    sohma "I'm sorry, Big Brother…"
    sohma "すみません、ビッグブラザー…"

# game/script.rpy:5386
translate japanese route_masaru_5_0f7335ce:

    masaru "*sigh*"
    masaru "*一息*"

# game/script.rpy:5388
translate japanese route_masaru_5_0561bf4f:

    sohma "I need to go back to my class now…"
    sohma "私は今、私のクラスに戻る必要があります…"

# game/script.rpy:5390
translate japanese route_masaru_5_867b96fe:

    masaru "Alright, Sohma. Sorry that big bro scolded you."
    masaru "さようなら、Sohma。申し訳ありませんが、大きな兄はあなたを叱った。"

# game/script.rpy:5392
translate japanese route_masaru_5_c6030cad:

    masaru "And thank you for delivering me my lunch box."
    masaru "そして私の弁当箱を私に届けてくれてありがとう。"

# game/script.rpy:5394
translate japanese route_masaru_5_0b97b446:

    masaru "I love you Sohma, you know that, right?"
    masaru "私はあなたを愛していますSohma、あなたはそれを知っていますよね？"

# game/script.rpy:5396
translate japanese route_masaru_5_def1d357:

    masaru "If there's anyone in the world that I don't want to see getting hurt… it's you."
    masaru "世界に私が傷ついて見たくない人がいるなら…あなたです。"

# game/script.rpy:5397
translate japanese route_masaru_5_a9c53df2:

    sohma "O-okay… I'm sorry…"
    sohma "O  - 大丈夫…すみません…"

# game/script.rpy:5399
translate japanese route_masaru_5_ef5692ee:

    sohma "B-by the way, a Mr. Genji Tadano was looking for you…"
    sohma "ちなみに、田野源司さんがあなたを探していました…"

# game/script.rpy:5400
translate japanese route_masaru_5_0c36775f:

    sohma "He said he wants to see you immediately."
    sohma "彼はすぐにあなたに会いたいと言いました。"

# game/script.rpy:5402
translate japanese route_masaru_5_2c16694f:

    masaru "Please go ahead, Sohma. Thank you for telling me that."
    masaru "Sohma先に進んでください。私に言ってくれてありがとう。"

# game/script.rpy:5404
translate japanese route_masaru_5_710443b3:

    sohma "Okay, Big Brother."
    sohma "さて、ビッグブラザー。"

# game/script.rpy:5407
translate japanese route_masaru_5_3cceff47:

    toshu "Captain, I never saw you talk like that before…"
    toshu "キャプテン、私はあなたの前にそれを話すことはなかった…"

# game/script.rpy:5409
translate japanese route_masaru_5_6e0827ed:

    ichiru "You don't have to be that hard to Sohma."
    ichiru "あなたはSohmaにそれほど難しいことはありません。"

# game/script.rpy:5410
translate japanese route_masaru_5_59ac917a:

    ichiru "He's just a little kid."
    ichiru "彼はちょうど小さな子供です。"

# game/script.rpy:5412
translate japanese route_masaru_5_efb66ba0:

    masaru "It's the 11th time already…"
    masaru "すでに11回目です…"

# game/script.rpy:5413
translate japanese route_masaru_5_eb7cbb1f:

    masaru "He often comes home crying. He's such an easy prey for bullies."
    masaru "彼はしばしば家に泣いてきます。彼はいじめのような簡単な獲物です。"

# game/script.rpy:5415
translate japanese route_masaru_5_9a7f6fe1:

    toshu "T-that's terrible…"
    toshu "T-それはひどい…"

# game/script.rpy:5417
translate japanese route_masaru_5_388b4cb4:

    masaru "He often says to me that it's an accident."
    masaru "彼はしばしば事故だと私に言う。"

# game/script.rpy:5418
translate japanese route_masaru_5_6f8a2e91:

    masaru "The first seven ones, I did try to go and defend him."
    masaru "最初の7つのものは、私は彼を守りに行きました。"

# game/script.rpy:5419
translate japanese route_masaru_5_bc2f5274:

    masaru "But sadly, he's like offering himself to be bullied."
    masaru "しかし、悲しいことに、彼はいじめられて自分自身を提供するようなものです。"

# game/script.rpy:5420
translate japanese route_masaru_5_6d9d4c01:

    masaru "I'm trying my best with him, but my effort alone is not enough if he doesn't want to change too…"
    masaru "私は彼と一緒に最善を尽くしているが、彼が変わってほしくないなら、私の努力だけでは不十分だ。"

# game/script.rpy:5422
translate japanese route_masaru_5_b2a74ff6:

    masaru "… *sigh*"
    masaru "… *ひとり*"

# game/script.rpy:5423
translate japanese route_masaru_5_40b9317c:

    masaru "Anyway, I have an urgent meeting with Coach."
    masaru "とにかく、私はコーチとの緊急会議を持っています。"

# game/script.rpy:5425
translate japanese route_masaru_5_24edb067:

    masaru "I'll see you guys later. Sorry for the unnecessary issue."
    masaru "後で皆さんにお会いしましょう。不要な問題を申し訳ありません。"

# game/script.rpy:5427
translate japanese route_masaru_5_daa71165:

    toshu "Alright, Captain."
    toshu "さて、キャプテン。"

# game/script.rpy:5429
translate japanese route_masaru_5_ed65bc93:

    ichiru "Relax yourself a bit, Masaru!"
    ichiru "リラックスして、まる！"

# game/script.rpy:5431
translate japanese route_masaru_5_6c1f7331:

    masaru "Thanks. See ya."
    masaru "ありがとう。 yaを参照してください。"

# game/script.rpy:5434
translate japanese route_masaru_5_e8db6b05:

    toshu "I'm worried about Captain…"
    toshu "キャプテンが心配です…"

# game/script.rpy:5435
translate japanese route_masaru_5_98634226:

    toshu "I mean… his exams failed… and his problem with his little brother… I feel like it's too much."
    toshu "私は…彼の試験が失敗した…と彼の弟との問題…私はあまりにも多くのように感じる。"

# game/script.rpy:5436
translate japanese route_masaru_5_589e5fa9:

    ichiru "He'll be fine, Tofu!"
    ichiru "彼はうまくいくよ、Tofu！"

# game/script.rpy:5438
translate japanese route_masaru_5_f3423454:

    toshu "I'm just worried…"
    toshu "私は心配しています…"

# game/script.rpy:5439
translate japanese route_masaru_5_0c0729eb:

    ichiru "There, there! Don't be sad!"
    ichiru "よしよし！悲しまないでください！"

# game/script.rpy:5441
translate japanese route_masaru_5_ec9ae2de:

    ichiru "I know Masaru! He's tough!"
    ichiru "私は優を知っている！彼はタフです！"

# game/script.rpy:5443
translate japanese route_masaru_5_559a8671:

    ichiru "As his friends, our role is to cheer him up in his bad times, right?!"
    ichiru "彼の友人として、私たちの役割は彼の悪い時に彼を応援することです、右か？"

# game/script.rpy:5445
translate japanese route_masaru_5_8526f6ef:

    toshu "You're right…!"
    toshu "あなたが正しい…！"

# game/script.rpy:5447
translate japanese route_masaru_5_2cadfe1a:

    ichiru "Yep! That's what we're gonna do!"
    ichiru "うん！それが私たちがやることです！"

# game/script.rpy:5449
translate japanese route_masaru_5_ec44da64:

    ichiru "For now, let's head for class, Tofu!"
    ichiru "今のところ、クラス、Tofuに向かおう！"

# game/script.rpy:5451
translate japanese route_masaru_5_0cbf8155:

    ichiru "We can talk to him, after class, alright?!"
    ichiru "授業の後、彼と話すことができます、大丈夫ですか？"

# game/script.rpy:5453
translate japanese route_masaru_5_ecd3936d:

    toshu "O-okay!"
    toshu "O-okay!"

# game/script.rpy:5455
translate japanese route_masaru_5_2b062699:

    toshu_t "More and more, I find about Captain Masaru's personal life…"
    toshu_t "ますます、私はCaptain Masaruの個人的な生活について…"

# game/script.rpy:5456
translate japanese route_masaru_5_f702960c:

    toshu_t "I wish I could do something to help him."
    toshu_t "私は彼を助けるために何かできることを望む。"

# game/script.rpy:5465
translate japanese route_masaru_5_cb1dcc26:

    saki "Good morning, class!"
    saki "こんにちはみなさん！"

# game/script.rpy:5467
translate japanese route_masaru_5_540d68cd:

    saki "Before everything else, I want to announce that we won't have morning classes today."
    saki "それ以外のものの前に、今日は朝の授業を持たないことを発表したい。"

# game/script.rpy:5469
translate japanese route_masaru_5_c58ca9bb:

    ichiru "YOSHAAA!"
    ichiru "YOSHAAA！"

# game/script.rpy:5471
translate japanese route_masaru_5_21b9179a:

    toshu "You barely attended classes, now you're happy that we don't have one today?"
    toshu "あなたは授業にほとんど出席しませんでしたが、今日は幸せです。"

# game/script.rpy:5473
translate japanese route_masaru_5_bf2cee46:

    ichiru "OF COURSE!"
    ichiru "もちろん！"

# game/script.rpy:5475
translate japanese route_masaru_5_c372b49c:

    saki "The school faculty is going to have a meeting since winter break is coming."
    saki "冬休みが来ているので、学校の教員は会議を開く予定です。"

# game/script.rpy:5477
translate japanese route_masaru_5_025b256d:

    saki "Anyway, before I leave, as you have seen this morning."
    saki "とにかく、あなたが今朝見たように、私が出発する前に。"

# game/script.rpy:5478
translate japanese route_masaru_5_1c1f6d4a:

    saki "Your grades are posted in the bulletin board outside in the hallway."
    saki "あなたの成績は、廊下の掲示板に掲示されます。"

# game/script.rpy:5480
translate japanese route_masaru_5_ddf92248:

    saki "I am really happy to announce that none from this class failed the exams!"
    saki "私はこのクラスの誰も試験に合格しなかったことを発表して本当にうれしいです！"

# game/script.rpy:5481
translate japanese route_masaru_5_f337132a:

    saki "Also, I would like to congratulate Mr. Ichiru Yanai for becoming our school's top ranking student in the preliminary examination."
    saki "また、予備試験では、私たちの学校のトップクラスの学生になることを祝いました。"

# game/script.rpy:5482
translate japanese route_masaru_5_9f74bc5d:

    saki "That is really impressive Mr. Yanai."
    saki "それは本当に印象的な柳井さんです。"

# game/script.rpy:5484
translate japanese route_masaru_5_1c419aed:

    saki "I see Mr. Kanada really moulded you into a good student."
    saki "私は金田さんがあなたを良い学生に実際に作りました。"

# game/script.rpy:5486
translate japanese route_masaru_5_4a5272b6:

    toshu "Ahh…"
    toshu "ああ…"

# game/script.rpy:5488
translate japanese route_masaru_5_b4a53bd6:

    ichiru "Ha! But of course! Tofu is the reason why I go to class in the first place!"
    ichiru "ハ！しかし、もちろん！Tofuは私が最初に授業に行く理由です！"

# game/script.rpy:5490
translate japanese route_masaru_5_113e4f45:

    toshu "But the past week, you didn't attend a single day…"
    toshu "しかし、先週、あなたは一日に出席しなかった…"

# game/script.rpy:5491
translate japanese route_masaru_5_96be9ace:

    saki "Anyway, as I've said before, I will dismiss you early."
    saki "とにかく、私が前に言ったように、私はあなたを早く退けるでしょう。"

# game/script.rpy:5493
translate japanese route_masaru_5_2f68c6a7:

    saki "Have a nice day ahead. Goodbye class."
    saki "いい日を過ごしてください。さようなら。"

# game/script.rpy:5501
translate japanese route_masaru_5_27d91db3:

    ichiru "Tofu~ What's your plan for today?"
    ichiru "Tofu〜今日のあなたの計画は何ですか？"

# game/script.rpy:5502
translate japanese route_masaru_5_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:5504
translate japanese route_masaru_5_5c4a047d:

    ichiru "Umm… Tofu?"
    ichiru "うーん…Tofu？"

# game/script.rpy:5505
translate japanese route_masaru_5_16a8c9ea:

    ichiru "You're still worried about Masaru, aren't you?"
    ichiru "あなたはまだマサルについて心配していますか？"

# game/script.rpy:5507
translate japanese route_masaru_5_926c6244:

    toshu "Yeah…"
    toshu "うん…"

# game/script.rpy:5509
translate japanese route_masaru_5_dfa2f0ef:

    toshu "I really feel he's not okay back there."
    toshu "私は彼がそこに大丈夫ではないと本当に感じる。"

# game/script.rpy:5511
translate japanese route_masaru_5_697f3e0c:

    ichiru "I see…"
    ichiru "そうですか…"

# game/script.rpy:5513
translate japanese route_masaru_5_e7a7f2b3:

    toshu "Can we go to the field today?"
    toshu "今日、現場に行くことができますか？"

# game/script.rpy:5515
translate japanese route_masaru_5_6593ecd7:

    toshu "Sohma said Coach was looking for Captain."
    toshu "Sohmaはコーチがキャプテンを探していると言いました。"

# game/script.rpy:5516
translate japanese route_masaru_5_b61c9a97:

    toshu "Maybe Coach knows what else Captain's dealing with."
    toshu "たぶんコーチはキャプテンが何を扱っているのか分かっているかもしれない。"

# game/script.rpy:5518
translate japanese route_masaru_5_d1804b1f:

    ichiru "If that would remove your worry, then let's go."
    ichiru "それがあなたの心配を取り除くなら、行こう。"

# game/script.rpy:5527
translate japanese route_masaru_5_f3aa084e:

    genji "Oh! What brings you here boys?"
    genji "ああ！あなたはここに何をもたらしますか？"

# game/script.rpy:5529
translate japanese route_masaru_5_5bea99c4:

    toshu "Coach, did Captain go here?"
    toshu "コーチ、キャプテンはここに行きましたか？"

# game/script.rpy:5531
translate japanese route_masaru_5_3ef5500e:

    toshu "He wasn't looking okay before…"
    toshu "彼は以前は大丈夫だった…"

# game/script.rpy:5533
translate japanese route_masaru_5_cb661430:

    genji "Ahh… Masaru…"
    genji "ああ…マサル…"

# game/script.rpy:5534
translate japanese route_masaru_5_c4f9d6de:

    genji "Masaru's really in a bad situation."
    genji "マサルは本当に悪い状況です。"

# game/script.rpy:5536
translate japanese route_masaru_5_924d1b84:

    toshu "Ehh? What happened, Coach?"
    toshu "え？どうしたの？コーチ？"

# game/script.rpy:5538
translate japanese route_masaru_5_7e34822f:

    genji "Unfortunately, I have received a letter from our head master."
    genji "残念ながら、私は頭のマスターから手紙を受け取りました。"

# game/script.rpy:5540
translate japanese route_masaru_5_c5675d94:

    genji "He'll be kicked out from all of his clubs due to his failing grades."
    genji "彼は彼の失敗した成績のために彼のすべてのクラブから追い出されるだろう。"

# game/script.rpy:5541
translate japanese route_masaru_5_38972df2:

    genji "And if he gets kicked out of the baseball club, the scholarship comes with it."
    genji "彼が野球クラブから追い出されると、奨学金にはそれが付いてくる。"

# game/script.rpy:5542
translate japanese route_masaru_5_afa9cc9f:

    genji "Therefore, he might have to leave this school."
    genji "したがって、彼はこの学校を離れる必要があるかもしれません。"

# game/script.rpy:5544
translate japanese route_masaru_5_74e0d093:

    ichiru "T-that's harsh!"
    ichiru "T-それは厳しい！"

# game/script.rpy:5546
translate japanese route_masaru_5_cd3def2a:

    toshu "Captain…"
    toshu "キャプテン…"

# game/script.rpy:5548
translate japanese route_masaru_5_5f753999:

    genji "Honestly I feel really bad for him…"
    genji "正直、私は彼にとって本当に悪いと感じる…"

# game/script.rpy:5549
translate japanese route_masaru_5_b214892c:

    genji "He is like a son to me… so I tried to request the headmaster to give him a make-up exam."
    genji "彼は私の息子のようなものなので、ヘッドメーターにメイクアップ試験を依頼しました。"

# game/script.rpy:5551
translate japanese route_masaru_5_2cc1b394:

    genji "Fortunately, the headmaster accepted my request and told Masaru about it."
    genji "幸いにも、校長は私の要求を受け入れ、それについてMasaruに話しました。"

# game/script.rpy:5553
translate japanese route_masaru_5_0341eff9:

    genji "But he lost all of his self-confidence. He gave up on himself."
    genji "しかし、彼はすべての自信を失った。彼は自分自身をあきらめた。"

# game/script.rpy:5554
translate japanese route_masaru_5_3d4bb18a:

    genji "I feel sorry for the guy. I'm trying to help him as much as I can."
    genji "私はその男のために申し訳ありません。私はできる限り彼を助けようとしています。"

# game/script.rpy:5556
translate japanese route_masaru_5_4372cf4a:

    genji "But I think the people he needs the most right now, is his friends."
    genji "しかし、私は今彼が最も必要とする人は、彼の友人だと思います。"

# game/script.rpy:5557
translate japanese route_masaru_5_d6818ffb:

    toshu "Thanks for letting us know Coach."
    toshu "コーチを教えてくれてありがとう。"

# game/script.rpy:5559
translate japanese route_masaru_5_078bc548:

    ichiru "Ugh… poor Masaru…"
    ichiru "うーん…悪いマサル…"

# game/script.rpy:5561
translate japanese route_masaru_5_1536eb61:

    genji "The only thing we can do is support him."
    genji "私たちができるのは彼をサポートすることだけです。"

# game/script.rpy:5562
translate japanese route_masaru_5_48bacf42:

    genji "You guys won't be meeting as much too, because winter break is coming."
    genji "冬休みが来ているので、皆さんも会うことはありません。"

# game/script.rpy:5564
translate japanese route_masaru_5_c0fc4371:

    genji "I expect you guys to cheer him up!"
    genji "私はあなたたちが彼を応援してくれることを期待しています！"

# game/script.rpy:5566
translate japanese route_masaru_5_16e1c0b3:

    genji "I don't want my team play in the tournament with a frown on their faces!"
    genji "私は自分のチームが自分の顔にぼんやりとしたトーナメントでプレーすることを望んでいません！"

# game/script.rpy:5568
translate japanese route_masaru_5_7e935945:

    toshu "We will do our best, Coach!"
    toshu "私たちは最善を尽くします、コーチ！"

# game/script.rpy:5570
translate japanese route_masaru_5_7d4c4813:

    genji "Okay. I am leaving it all to you guys."
    genji "はい。私は皆さんにそれをすべて残しています。"

# game/script.rpy:5572
translate japanese route_masaru_5_0bba08af:

    toshu_t "Coach surely knows how to deal with serious situations like this."
    toshu_t "コーチは、このような深刻な状況にどう対処するかを確かに知っている。"

# game/script.rpy:5573
translate japanese route_masaru_5_8d9ca197:

    toshu_t "Captain was right about him, he does treat us as if we are his children."
    toshu_t "キャプテンは彼について正しい、彼は私たちが彼の子供のように私たちを扱っています。"

# game/script.rpy:5574
translate japanese route_masaru_5_b510f949:

    toshu_t "I feel like he's my second father too!"
    toshu_t "私は彼が私の第二の父親でもあるように感じる！"

# game/script.rpy:5576
translate japanese route_masaru_5_8a7dc53f:

    toshu_t "I never saw Masaru ever since that incident…"
    toshu_t "その事件以来、私は大正を見たことはありません…"

# game/script.rpy:5577
translate japanese route_masaru_5_dd5fba2c:

    toshu_t "He skipped all of our baseball practice sessions."
    toshu_t "彼はすべての野球練習をスキップしました。"

# game/script.rpy:5578
translate japanese route_masaru_5_07141f72:

    toshu_t "With that, winter break has begun… without having any news of my friends."
    toshu_t "それで、冬休みが始まった…私の友人の知らせがなくて。"

# game/script.rpy:5585
translate japanese route_ichiru_5_deca71de:

    masaru "You should've taught me, Ichiru."
    masaru "あなたは私に教えて、Ichiruする必要があります。"

# game/script.rpy:5587
translate japanese route_ichiru_5_0d0ba9b6:

    ichiru "You didn't ask for it."
    ichiru "あなたはそれを求めなかった。"

# game/script.rpy:5589
translate japanese route_ichiru_5_e620121c:

    ichiru "I would've gladly taught you if you asked me."
    ichiru "あなたが私に尋ねたなら、喜んで教えてくれたでしょう。"

# game/script.rpy:5591
translate japanese route_ichiru_5_5874edf2:

    masaru "You always get good remarks even though you don't give much effort…"
    masaru "あなたは多くの努力を惜しまなくても常に良い発言を得る…"

# game/script.rpy:5593
translate japanese route_ichiru_5_5eed8ef5:

    toshu "I didn't know Captain was having trouble with his studies."
    toshu "キャプテンが勉強に困っていることは分かりませんでした。"

# game/script.rpy:5595
translate japanese route_ichiru_5_d579213f:

    ichiru "But don't be sad Masaru, maybe I'll go treat you guys!"
    ichiru "しかし、悲しいことではありません。大丈夫、多分私はあなたたちを扱う行くよ！"

# game/script.rpy:5597
translate japanese route_ichiru_5_d53ea683:

    toshu "That sounds great!"
    toshu "それはいいです！"

# game/script.rpy:5599
translate japanese route_ichiru_5_6659493c:

    ichiru "If not for Toshu, I wouldn't get that score! Hehe!"
    ichiru "Toshuのためでなければ、私はそのスコアを得ることはなかった！ Hehe！"

# game/script.rpy:5601
translate japanese route_ichiru_5_b24d4485:

    toshu "R-really?"
    toshu "本当に？"

# game/script.rpy:5603
translate japanese route_ichiru_5_4b45e04d:

    toshu "Then I'm glad I was able to help… though I'm not sure where exactly I helped you with…"
    toshu "それから、私が助けてくれることをうれしく思っています…私はどこであなたを助けたのか分かりません…"

# game/script.rpy:5604
translate japanese route_ichiru_5_fcc6496b:

    random "Ichiru~~~~!"
    random "イチル~~~~！"

# game/script.rpy:5610
translate japanese route_ichiru_5_5be216eb:

    tomoka "I finally found my prince~"
    tomoka "ついに私の王子を見つけた〜"

# game/script.rpy:5614
translate japanese route_ichiru_5_12ece26b:

    ichiru "T-Tomoka??!"
    ichiru "T-Tomoka ??！"

# game/script.rpy:5616
translate japanese route_ichiru_5_bad4d2a6:

    tomoka "I've seen the bulletin board. It's no surprise that Ichiru's on top again!"
    tomoka "私は掲示板を見た。 Ichiruがもう一度上に来るのは驚きではありません！"

# game/script.rpy:5618
translate japanese route_ichiru_5_9c761cc6:

    tomoka "What's this about 'being glad to help' this trash was talking about?"
    tomoka "このゴミ箱が「助けてくれることを嬉しく思っている」というのは何ですか？"

# game/script.rpy:5620
translate japanese route_ichiru_5_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:5622
translate japanese route_ichiru_5_99a0353f:

    tomoka "Don't you know that this is not the first time my Ichiru has been on top?"
    tomoka "私が一番上にいたのはこれが初めてではないことは分かりませんか？"

# game/script.rpy:5623
translate japanese route_ichiru_5_b524c8cf:

    tomoka "In fact, my Ichiru is the most outstanding student for three years in a row!"
    tomoka "実際、私のIchiruは3年連続で最も優れた学生です！"

# game/script.rpy:5624
translate japanese route_ichiru_5_580c8cd9:

    tomoka "So Ichiru, what were you blabbering about 'If not for you, I wouldn't get that score'?"
    tomoka "だからイチール、あなたが「あなたのためでなければ、私はその得点を得られないだろう」と嘲笑していたのですか？"

# game/script.rpy:5626
translate japanese route_ichiru_5_419c0495:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:5628
translate japanese route_ichiru_5_e6633c53:

    tomoka "Well, you couldn't possibly fail anyway."
    tomoka "まあ、とにかく失敗することはありませんでした。"

# game/script.rpy:5630
translate japanese route_ichiru_5_4782c20d:

    tomoka "Because if you do, 'Uncle' will be disappointed."
    tomoka "あなたがそうするならば、「おじさん」は失望するだろうから。"

# game/script.rpy:5631
translate japanese route_ichiru_5_63d781e7:

    tomoka "Consequences are definitely not worth!"
    tomoka "結果は間違いなく価値がありません！"

# game/script.rpy:5633
translate japanese route_ichiru_5_5521864b:

    toshu "C-Captain, do something…"
    toshu "C  - キャプテン、何かをする…"

# game/script.rpy:5635
translate japanese route_ichiru_5_17d2c55f:

    masaru "Ehh…? Why me?"
    masaru "エ…？なんでわたし？"

# game/script.rpy:5636
translate japanese route_ichiru_5_3cf09849:

    masaru "Ahh… umm… excuse me, Miss Tomoka."
    masaru "ああ…ええと…実は、トモカさん。"

# game/script.rpy:5638
translate japanese route_ichiru_5_852e93ac:

    # tomoka "…"
    tomoka "…"

# game/script.rpy:5640
translate japanese route_ichiru_5_19fb0236:

    tomoka "HMPH! I see you are still hanging out with these peasants!"
    tomoka "HMPH！私はあなたがまだこれらの農民とぶらぶらしているのを見ます！"

# game/script.rpy:5641
translate japanese route_ichiru_5_bf94b739:

    tomoka "Such lowlives!"
    tomoka "そのような低利！"

# game/script.rpy:5643
translate japanese route_ichiru_5_3b752a6b:

    ichiru "They're my friends!"
    ichiru "彼らは私の友達です！"

# game/script.rpy:5645
translate japanese route_ichiru_5_f7f16887:

    tomoka "Friends?!!!"
    tomoka "友達ですか？"

# game/script.rpy:5647
translate japanese route_ichiru_5_07b220f1:

    masaru "Ichiru…"
    masaru "イチール…"

# game/script.rpy:5649
translate japanese route_ichiru_5_2ef79c78:

    tomoka "HOHOHOHOHO~!"
    tomoka "ホホロホロ〜！"

# game/script.rpy:5651
translate japanese route_ichiru_5_9cad2403:

    tomoka "Where we're going, you don't need any of these hoodlums!"
    tomoka "私たちが行っているところで、あなたはこれらのふしだらな女たちのどれも必要としません！"

# game/script.rpy:5653
translate japanese route_ichiru_5_71cceafb:

    ichiru "W-what do you mean…?"
    ichiru "W-何を意味するの？"

# game/script.rpy:5655
translate japanese route_ichiru_5_2c18bd79:

    tomoka "Did you forget the topic in the family meeting?"
    tomoka "家族会議の話題を忘れましたか？"

# game/script.rpy:5656
translate japanese route_ichiru_5_19bcf7eb:

    tomoka "Or you weren't listening?"
    tomoka "それとも、聞いていないのですか？"

# game/script.rpy:5658
translate japanese route_ichiru_5_34535d0f:

    ichiru "C'mon, tell me already!"
    ichiru "さあ、もう教えて！"

# game/script.rpy:5660
translate japanese route_ichiru_5_7f3c64db:

    tomoka "The Yanai and Saji family have agreed upon your school transfer!"
    tomoka "柳井さんと佐治さんの家族はあなたの学校への移籍に同意しました！"

# game/script.rpy:5662
translate japanese route_ichiru_5_e404c727:

    ichiru "!!!"
    ichiru "!!!"

# game/script.rpy:5664
translate japanese route_ichiru_5_9721ed1c:

    ichiru "I didn't agree to this!!!"
    ichiru "私はこれに同意しなかった！"

# game/script.rpy:5666
translate japanese route_ichiru_5_1a02a5fc:

    tomoka "Don't fret so much! You'll be with me! You don't need any of these so-called friends!"
    tomoka "そんなに心配しないで！あなたは私と一緒にいます！あなたはこれらのいわゆる友人のいずれかを必要としません！"

# game/script.rpy:5668
translate japanese route_ichiru_5_c016c726:

    tomoka "After you finish this year's final exam, you will continue your study with me… abroad!"
    tomoka "今年の最終試験が終わったら、あなたは私と一緒に勉強を続けます…海外！"

# game/script.rpy:5670
translate japanese route_ichiru_5_b30e5816:

    tomoka "Wah~ You and me, we can have that prestigious school all for ourselves!"
    tomoka "ワウ〜あなたと私、私たちは自分のためにその名門の学校を持つことができます！"

# game/script.rpy:5671
translate japanese route_ichiru_5_5015dfc9:

    tomoka "I finally can make you mine!"
    tomoka "私はついにあなたを私のものにすることができます！"

# game/script.rpy:5673
translate japanese route_ichiru_5_1df099a6:

    ichiru "T-this can't be…"
    ichiru "T-これはできません…"

# game/script.rpy:5675
translate japanese route_ichiru_5_b7c06c87:

    tomoka "My dear Ichiru, why aren't you as happy as I am?"
    tomoka "私の愛するIchiru、なぜ私は幸せではないのですか？"

# game/script.rpy:5676
translate japanese route_ichiru_5_f720e6a1:

    tomoka "We can finally go somewhere far much better!"
    tomoka "ついにどこかに行くことができます！"

# game/script.rpy:5678
translate japanese route_ichiru_5_06475f4f:

    tomoka "And get rid of these peasants you call friends!"
    tomoka "そしてあなたが友達と呼ぶこれらの農民を取り除く！"

# game/script.rpy:5679
translate japanese route_ichiru_5_d6e3521f:

    tomoka "They will stain your name."
    tomoka "彼らはあなたの名前を汚すでしょう。"

# game/script.rpy:5680
translate japanese route_ichiru_5_926b0f14:

    tomoka "Strike that--"
    tomoka "それを打つ -"

# game/script.rpy:5681
translate japanese route_ichiru_5_a3ee6d15:

    tomoka "They will stain 'OUR' name."
    tomoka "彼らは \'私たちの\'名前を汚すでしょう。"

# game/script.rpy:5683
translate japanese route_ichiru_5_db065f1a:

    tomoka "See you soon, my ICHIRUUUU~~!"
    tomoka "すぐに、私のICHIRUUUU ~~！"

# game/script.rpy:5685
translate japanese route_ichiru_5_d2c04c1f:

    tomoka "HOHOHOHO!!!"
    tomoka "HOHOHOHO !!!"

# game/script.rpy:5688
translate japanese route_ichiru_5_419c0495_1:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:5691
translate japanese route_ichiru_5_8e951ac7:

    toshu "Ichiru…"
    toshu "イチール…"

# game/script.rpy:5693
translate japanese route_ichiru_5_dff8fb39:

    ichiru "Damn it!!"
    ichiru "畜生！！"

# game/script.rpy:5694
translate japanese route_ichiru_5_7b3bec81:

    ichiru "I have to go to see my dad!"
    ichiru "私は私のお父さんを見に行かなければならない！"

# game/script.rpy:5698
translate japanese route_ichiru_5_0f7335ce:

    masaru "*sigh*"
    masaru "*一息*"

# game/script.rpy:5700
translate japanese route_ichiru_5_fc69851c:

    masaru "That Tomoka girl has always been troublesome…"
    masaru "そのトモカの女の子はいつも面倒だった…"

# game/script.rpy:5702
translate japanese route_ichiru_5_8e4c8636:

    toshu "We have got to do something, Captain!"
    toshu "私たちは何かをしなければならない、キャプテン！"

# game/script.rpy:5704
translate japanese route_ichiru_5_cd11bc67:

    masaru "I'm afraid, w-we really can't do anything…"
    masaru "私は本当に何もすることはできません。"

# game/script.rpy:5705
translate japanese route_ichiru_5_fa847996:

    masaru "It's Ichiru's personal problem…"
    masaru "イチルの個人的な問題です…"

# game/script.rpy:5707
translate japanese route_ichiru_5_c3dfd438:

    toshu "But…"
    toshu "しかし…"

# game/script.rpy:5708
translate japanese route_ichiru_5_88f2b8e1:

    masaru "It really can't be helped… for some reason, Ichiru is tied to Tomoka's family."
    masaru "それは本当に助けになることはできません…何らかの理由で、IchiruはTomokaの家族に結ばれています。"

# game/script.rpy:5710
translate japanese route_ichiru_5_619240b9:

    toshu "Why?"
    toshu "どうして？"

# game/script.rpy:5711
translate japanese route_ichiru_5_74a4932a:

    masaru "I'm not really sure… but you do know Ichiru is the son of this school's president, right?"
    masaru "私は本当に確信していない…しかし、あなたは知っているイチールは、この学校の大統領の息子、右ですか？"

# game/script.rpy:5712
translate japanese route_ichiru_5_364f49a2_1:

    # toshu "…"
    toshu "…"

# game/script.rpy:5714
translate japanese route_ichiru_5_3d29aea9:

    toshu "WHAT?!!"
    toshu "何？！！"

# game/script.rpy:5716
translate japanese route_ichiru_5_95b2c115:

    masaru "You never knew?"
    masaru "あなたは決して知らなかった？"

# game/script.rpy:5717
translate japanese route_ichiru_5_911e25e8:

    toshu "NO ONE EVER TOLD ME…!"
    toshu "私は今までに一度も…！"

# game/script.rpy:5719
translate japanese route_ichiru_5_29bff2b2:

    masaru "Haha!"
    masaru "ハハ！"

# game/script.rpy:5721
translate japanese route_ichiru_5_277fe23f:

    masaru "Anyway, I think Ichiru's dad is the one strongly connected to Tomoka's family."
    masaru "とにかく、Ichiruさんのお父さんは、トモカさんの家族と強くつながっていると思います。"

# game/script.rpy:5722
translate japanese route_ichiru_5_08f950c1:

    random "I'm sorry but I can't help but overhear your conversation."
    random "申し訳ありませんが、私はあなたの会話を耳にすることはできません。"

# game/script.rpy:5725
translate japanese route_ichiru_5_34ed5150:

    toshu "Ms. Saki?"
    toshu "咲さん？"

# game/script.rpy:5726
translate japanese route_ichiru_5_02f7b480:

    saki "Let's go inside the classroom!"
    saki "教室の中に入ってみましょう！"

# game/script.rpy:5735
translate japanese route_ichiru_5_b534fe63:

    saki "I didn't mean to eavesdrop or anything."
    saki "私は盗聴や何かを意味しませんでした。"

# game/script.rpy:5737
translate japanese route_ichiru_5_59ff7bd0:

    saki "But I happen to know a lot about the Yanai and Saji family!"
    saki "でも、私は柳井さんと佐治さんのことをよく知っています。"

# game/script.rpy:5739
translate japanese route_ichiru_5_85e48a4e:

    saki "I'll be happy to fill you in with the information."
    saki "私は情報をあなたに記入してうれしいです。"

# game/script.rpy:5741
translate japanese route_ichiru_5_3ad1d94f:

    toshu "Oh! Thank you Ms. Saki!"
    toshu "ああ！ありがとうございます咲さん！"

# game/script.rpy:5743
translate japanese route_ichiru_5_ce9a9db0:

    masaru "That is quite convenient."
    masaru "それは非常に便利です。"

# game/script.rpy:5745
translate japanese route_ichiru_5_a9e3c483:

    saki "I happen to respect both families."
    saki "私は両方の家族を尊敬することがあります。"

# game/script.rpy:5746
translate japanese route_ichiru_5_263814c3:

    saki "I am grateful to them since they gave me this job as a teacher."
    saki "彼らが私にこの仕事を教師として与えてくれたので、私は彼らに感謝しています。"

# game/script.rpy:5748
translate japanese route_ichiru_5_6192bd8d:

    saki "It's very hard to find a job these days you know!"
    saki "最近あなたが知っている仕事を見つけることは非常に難しいです！"

# game/script.rpy:5749
translate japanese route_ichiru_5_89a77ed1:

    saki "You have to do all kinds of job hunting in the news paper, internet and everywhere!"
    saki "ニュースペーパー、インターネット、あらゆるところであらゆる種類の就職活動をしなければなりません！"

# game/script.rpy:5751
translate japanese route_ichiru_5_c2a05549:

    saki "And You have to be very presentable when undergoing an interview too!"
    saki "インタビューを受けているときは、とてもプレゼンテーションが必要です！"

# game/script.rpy:5753
translate japanese route_ichiru_5_ff9830b4:

    saki "The interviews are usually the part I fail, I don't know why. That's why I'm really glad President Yanai took me in… PERSONALLY!"
    saki "インタビューは通常、私が失敗する部分です、私は理由を知らない。だから柳井会長が私を連れてきて本当にうれしいです。個人的に！"

# game/script.rpy:5756
translate japanese route_ichiru_5_5c7da4b0:

    toshu "Umm…"
    toshu "うーん…"

# game/script.rpy:5757
translate japanese route_ichiru_5_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:5759
translate japanese route_ichiru_5_d6772068:

    saki "The feeling after an interview is such a torture! You never know if you're in or not!"
    saki "面接後の気持ちはそんなに苦痛です！あなたが入居しているかどうかはわかりません！"

# game/script.rpy:5760
translate japanese route_ichiru_5_a13cd473:

    saki "And not just that! The job itself is really hard! You have to prepare lesson plans everyday, compute grades, make exams, check exams--"
    saki "それだけじゃない！仕事自体は本当に難しいです！毎日レッスンプランを準備し、成績を計算し、試験を受け、試験をチェックする必要があります。"

# game/script.rpy:5762
translate japanese route_ichiru_5_9abb46a0:

    masaru "Ahem… Ms. Saki."
    masaru "Ahem …さん。サキ"

# game/script.rpy:5764
translate japanese route_ichiru_5_e1e91a18:

    saki "Yes? What is it Mr. Nakahara?"
    saki "はい？中原さんって何？"

# game/script.rpy:5766
translate japanese route_ichiru_5_de47000b:

    masaru "I think you've gone way offtopic."
    masaru "私はあなたが遠隔地に行ったと思う。"

# game/script.rpy:5767
translate japanese route_ichiru_5_cf7d86ac:

    saki "Oh! I'm truly sorry!"
    saki "ああ！本当に申し訳ありません！"

# game/script.rpy:5769
translate japanese route_ichiru_5_bb227ade:

    saki "This is embarrassing! It was a bad habit of mine…!"
    saki "これは恥ずかしいです！それは私の悪い習慣だった…！"

# game/script.rpy:5771
translate japanese route_ichiru_5_3c89db13:

    saki "Ehem…"
    saki "元…"

# game/script.rpy:5773
translate japanese route_ichiru_5_62f51eda:

    saki "As I was saying, Ms. Tomoka is the daughter of the rich and successful Hideki Saji."
    saki "私が言っていたように、トモカさんは、豊かで成功した佐治秀樹の娘です。"

# game/script.rpy:5774
translate japanese route_ichiru_5_3b496f43:

    saki "Who happens to be the business partner of Keiji Yanai, Ichiru's Father, also president of Yakyusha Academy!"
    saki "伊達の父親、また八尾アカデミーの社長である柳井啓二のビジネスパートナーは誰ですか？"

# game/script.rpy:5775
translate japanese route_ichiru_5_38819a71:

    saki "At first, Mr. Yanai was not very successful. He didn't have the opportunity to shine because they didn't have any huge sponsors."
    saki "まず、柳井さんはあまり成功しませんでした。彼は巨大なスポンサーがいなかったので輝く機会はなかった。"

# game/script.rpy:5776
translate japanese route_ichiru_5_c4c10c49:

    saki "Mr. Saji, took notice of Mr. Yanai's Talents and sponsored him on his business plan."
    saki "佐々木氏は、柳井氏の「タレント」に気づいて、彼の事業計画を後援しました。"

# game/script.rpy:5777
translate japanese route_ichiru_5_1a32a2e2:

    saki "They worked well together and significantly acquired a lot of fortune in their very successful business."
    saki "彼らはうまく一緒に働いて、非常に成功したビジネスで多くの財産を獲得しました。"

# game/script.rpy:5778
translate japanese route_ichiru_5_8489037d:

    saki "Mr. Yanai is very grateful to Mr. Saji. For giving him the opportunity to propose his idea."
    saki "柳井さんは佐治さんに非常に感謝しています。彼に彼のアイデアを提案する機会を与えるため。"

# game/script.rpy:5779
translate japanese route_ichiru_5_d4f9f3a1:

    saki "Years passed and Mr. Saji had a daughter, which is Ms. Tomoka."
    saki "年を経て、佐治さんには娘がいました。"

# game/script.rpy:5780
translate japanese route_ichiru_5_5097433d:

    saki "Mr. Saji wanted to make both families closer to each other."
    saki "佐治さんは両家族を互いに近づけたいと思っていました。"

# game/script.rpy:5781
translate japanese route_ichiru_5_9864e84c:

    saki "And he had an idea of offering his beloved daughter' heart to Mr. Yanai's only son, heir to the Yanai Legacy, Ichiru."
    saki "そして、彼は柳井の唯一の息子、柳井遺産の一族、Ichiruに親愛なる娘の心を提供する考えがありました。"

# game/script.rpy:5784
translate japanese route_ichiru_5_6ce17760:

    masaru "W-wait… don't tell me…"
    masaru "W-待って…私に言わないで…"

# game/script.rpy:5785
translate japanese route_ichiru_5_27780291:

    saki "Yes, Ms. Tomoka is Ichiru's fiancée."
    saki "はい、TomokaさんはIchiruの婚約者です。"

# game/script.rpy:5787
translate japanese route_ichiru_5_f11624c4:

    toshu "WHAAAT…"
    toshu "ワイアット…"

# game/script.rpy:5788
translate japanese route_ichiru_5_6ee2b66d:

    toshu "This is way too much to process…!!"
    toshu "これはあまりにも処理する方法です…！"

# game/script.rpy:5790
translate japanese route_ichiru_5_5c3fa434:

    masaru "That means… judging on Ichiru's reaction… he's not in favour of the arranged marriage."
    masaru "つまり、イチールの反応を判断すると…結婚式に賛成ではない。"

# game/script.rpy:5792
translate japanese route_ichiru_5_b9ce40b6:

    masaru "He doesn't want to marry Tomoka."
    masaru "彼はトモカと結婚したくない。"

# game/script.rpy:5794
translate japanese route_ichiru_5_ac6f7729:

    toshu "Isn't Ichiru too young to be engaged to someone else?!"
    toshu "Ichiruは他の誰かに従事するにはあまりにも若いですか？"

# game/script.rpy:5796
translate japanese route_ichiru_5_5821b710:

    saki "President Yanai is a good man, but I heard he's extra strict and protective when it comes to family."
    saki "柳井会長はいい人ですが、家族になると彼は厳しく保護されていると聞きました。"

# game/script.rpy:5798
translate japanese route_ichiru_5_96630f83:

    masaru "I know that as well… that's why no one tries to mess with Ichiru."
    masaru "私もそれを知っています…だから誰もイチールを混乱させようとしていません。"

# game/script.rpy:5800
translate japanese route_ichiru_5_77da95a7:

    saki "And also, that's why he can skip classes as he pleases… Ichiru uses his dad's influence to do what he wants."
    saki "そして、それは彼が喜んでクラスをスキップすることができる理由です… Ichiruは彼が望むことを行うために彼の父の影響力を使用します。"

# game/script.rpy:5802
translate japanese route_ichiru_5_51f17a50:

    toshu "Now it all makes sense… the skipping… and why Ichiru doesn't have too many friends…"
    toshu "今それはすべて意味がある…スキップする…そしてなぜイチールはあまりにも多くの友人を持っていない…"

# game/script.rpy:5804
translate japanese route_ichiru_5_3d9773db:

    saki "But I believe Mr. Yanai is a good boy. He's just usually misunderstood. He's a genius with insurmountable potential."
    saki "でも、柳井さんはいい子だと思います。彼はちょうどよく誤解されている。彼は克服できないほどの可能性を秘めた天才だ。"

# game/script.rpy:5806
translate japanese route_ichiru_5_36b38d06:

    saki "And ever since he met Mr. Kanada…"
    saki "そして、彼は金田氏に会って以来…"

# game/script.rpy:5808
translate japanese route_ichiru_5_6590cae1:

    saki "He changed drastically! His smile became more sincere and he became happier!"
    saki "彼は劇的に変わった！彼の笑顔はより誠実になり、彼はもっと幸せになった！"

# game/script.rpy:5809
translate japanese route_ichiru_5_708d8fce:

    saki "I figured Ichiru is most comfortable when he's with his friends."
    saki "彼は友達と一緒にいち丸が一番快適だと思った。"

# game/script.rpy:5811
translate japanese route_ichiru_5_54c0b3ee:

    masaru "I see…"
    masaru "そうですか…"

# game/script.rpy:5813
translate japanese route_ichiru_5_7f148eb8:

    saki "Ah! I almost forgot!"
    saki "ああ！忘れそうだった！"

# game/script.rpy:5815
translate japanese route_ichiru_5_c372b49c:

    saki "The school faculty is going to have a meeting since winter break is coming."
    saki "冬休みが来ているので、学校の教員は会議を開く予定です。"

# game/script.rpy:5816
translate japanese route_ichiru_5_10ab5401:

    toshu "Then we won't be having class, Ms. Saki?"
    toshu "それから私たちはクラスを持たない、咲さん？"

# game/script.rpy:5817
translate japanese route_ichiru_5_20d66595:

    saki "Yes, yes, I got to go…!"
    saki "はい、はい、私は行く必要があります…！"

# game/script.rpy:5819
translate japanese route_ichiru_5_4d3b6444:

    masaru "She sure was in a rush."
    masaru "彼女は確かに急いでいた。"

# game/script.rpy:5821
translate japanese route_ichiru_5_fb05c77b:

    toshu "I still can't believe all these new things about Ichiru. I thought I knew him so well…"
    toshu "Ichiruについてのこれらの新しいことをすべて信じることはできません。私は彼をよく知っていると思った…"

# game/script.rpy:5823
translate japanese route_ichiru_5_4b7b6286:

    masaru "I guess he was hiding it to both of us…"
    masaru "私は彼が私たちにそれを隠していたと思う…"

# game/script.rpy:5825
translate japanese route_ichiru_5_696734b9:

    toshu "But, why?"
    toshu "しかし、なぜ？"

# game/script.rpy:5827
translate japanese route_ichiru_5_0e13fb20:

    masaru "He probably don't want us to worry about him…"
    masaru "彼はおそらく私たちが彼について心配することを望まない…"

# game/script.rpy:5829
translate japanese route_ichiru_5_4b2952b8:

    masaru "That's what you would most likely do too, right?"
    masaru "それはあなたがそうする可能性が最も高いでしょう、そうですか？"

# game/script.rpy:5831
translate japanese route_ichiru_5_359b4820:

    toshu "Yeah… I wish we could do something for Ichiru, as his friends."
    toshu "うん…私は、イチールのために何かできることを、彼の友人としてしたい。"

# game/script.rpy:5833
translate japanese route_ichiru_5_d7f62562:

    masaru "Ah how about, we go to Coach and ask for advice?"
    masaru "ああ、どうやって、コーチに行きアドバイスを求めますか？"

# game/script.rpy:5835
translate japanese route_ichiru_5_89eb8814:

    toshu "Good idea, Captain. Let's go!"
    toshu "良いアイデア、キャプテン。行こう！"

# game/script.rpy:5844
translate japanese route_ichiru_5_f3aa084e:

    genji "Oh! What brings you here boys?"
    genji "ああ！あなたはここに何をもたらしますか？"

# game/script.rpy:5846
translate japanese route_ichiru_5_d0462c38:

    toshu "Coach, did Ichiru go here?"
    toshu "コーチ、イッチルはここに行った？"

# game/script.rpy:5848
translate japanese route_ichiru_5_7d0b9356:

    genji "Ahh… I was about to tell you guys the news about him."
    genji "ああ…私は彼についてのニュースを皆さんに伝えようとしていました。"

# game/script.rpy:5850
translate japanese route_ichiru_5_924d1b84:

    toshu "Ehh? What happened, Coach?"
    toshu "え？どうしたの？コーチ？"

# game/script.rpy:5852
translate japanese route_ichiru_5_ac2b946f:

    genji "Unfortunately, I have received a note from President Yanai."
    genji "残念ながら、私は柳井社長からメモを受けました。"

# game/script.rpy:5854
translate japanese route_ichiru_5_5a5686f1:

    genji "It says, the baseball club is not a good influence to Ichiru."
    genji "それは、野球クラブはIchiruに良い影響ではないと言います。"

# game/script.rpy:5855
translate japanese route_ichiru_5_85006bdd:

    genji "With consecutive losses, President believes this club promotes laziness and irresponsibility."
    genji "継続的な損失で、大統領はこのクラブが怠惰と無責任を促進すると信じています。"

# game/script.rpy:5856
translate japanese route_ichiru_5_9ff72253:

    genji "He must be pertaining on how we tolerated Ichiru's class-skipping…"
    genji "彼は私がIchiruのクラスをスキップすることをどのように容認しているかに関係しているに違いない…"

# game/script.rpy:5858
translate japanese route_ichiru_5_8c426509:

    masaru "T-that's harsh!"
    masaru "T-それは厳しい！"

# game/script.rpy:5860
translate japanese route_ichiru_5_8e951ac7_1:

    toshu "Ichiru…"
    toshu "イチール…"

# game/script.rpy:5862
translate japanese route_ichiru_5_5f753999:

    genji "Honestly I feel really bad for him…"
    genji "正直、私は彼にとって本当に悪いと感じる…"

# game/script.rpy:5863
translate japanese route_ichiru_5_fdc0eafe:

    genji "He is like a son to me… so I promised the President that we will win this coming tournament."
    genji "彼は私の息子のようです…私は大統領にこの大会に勝つことを約束しました。"

# game/script.rpy:5865
translate japanese route_ichiru_5_5f23a935:

    genji "If we do win… Ichiru stays in the club."
    genji "私たちが勝つと…イチルはクラブにとどまります。"

# game/script.rpy:5867
translate japanese route_ichiru_5_1536eb61:

    genji "The only thing we can do is support him."
    genji "私たちができるのは彼をサポートすることだけです。"

# game/script.rpy:5868
translate japanese route_ichiru_5_48bacf42:

    genji "You guys won't be meeting as much too, because winter break is coming."
    genji "冬休みが来ているので、皆さんも会うことはありません。"

# game/script.rpy:5870
translate japanese route_ichiru_5_147697ee:

    genji "As his friends, I expect you guys to cheer him up!"
    genji "彼の友人として、私はあなたたちが彼を応援してくれることを期待しています！"

# game/script.rpy:5872
translate japanese route_ichiru_5_16e1c0b3:

    genji "I don't want my team play in the tournament with a frown on their faces!"
    genji "私は自分のチームが自分の顔にぼんやりとしたトーナメントでプレーすることを望んでいません！"

# game/script.rpy:5874
translate japanese route_ichiru_5_7e935945:

    toshu "We will do our best, Coach!"
    toshu "私たちは最善を尽くします、コーチ！"

# game/script.rpy:5876
translate japanese route_ichiru_5_b55ec730:

    toshu_t "I never saw Ichiru ever since that incident…"
    toshu_t "その事件以来、私はイチールを見たことがありません…"

# game/script.rpy:5877
translate japanese route_ichiru_5_dd5fba2c:

    toshu_t "He skipped all of our baseball practice sessions."
    toshu_t "彼はすべての野球練習をスキップしました。"

# game/script.rpy:5878
translate japanese route_ichiru_5_07141f72:

    toshu_t "With that, winter break has begun… without having any news of my friends."
    toshu_t "それで、冬休みが始まった…私の友人の知らせがなくて。"
