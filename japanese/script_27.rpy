# game/script.rpy:10774
translate japanese ending_ipe_mne_4760d0aa:

    toshu_t "The weekend has passed after that intense tournament."
    toshu_t "その激しいトーナメントの後、週末が過ぎました。"

# game/script.rpy:10775
translate japanese ending_ipe_mne_1aa1bffb:

    toshu_t "I want to know how my friends have been doing…"
    toshu_t "私の友達がどうしているのか知りたい"

# game/script.rpy:10783
translate japanese ending_ipe_mne_2d78b941:

    toshu "I… it's pretty quiet around…"
    toshu "私は…周りはかなり静かです…"

# game/script.rpy:10785
translate japanese ending_ipe_mne_b65efb71:

    toshu "It totally feels different without Ichiru or Masaru…"
    toshu "イチールやマサルなしで全く違う感じ…"

# game/script.rpy:10786
translate japanese ending_ipe_mne_548b7b6a:

    toshu "Where is everyone…?"
    toshu "みんなはどこですか…？"

# game/script.rpy:10788
translate japanese ending_ipe_mne_631ec800:

    toshu "Wait! Isn't that…!"
    toshu "待つ！それじゃない…！"

# game/script.rpy:10791
translate japanese ending_ipe_mne_1d80a47f:

    toshu "Masaru… what's wrong?"
    toshu "マサル…何が間違っているのですか？"

# game/script.rpy:10793
translate japanese ending_ipe_mne_d92c1356:

    toshu "Y-You look so down…"
    toshu "Y-あなたはとても下に見える…"

# game/script.rpy:10795
translate japanese ending_ipe_mne_fafe0ddb:

    masaru "I got the remedial exam results today…"
    masaru "今日私は矯正試験の結果を得ました…"

# game/script.rpy:10797
translate japanese ending_ipe_mne_cff0599b:

    masaru "I… I failed…again…"
    masaru "私は…私はもう一度失敗した…"

# game/script.rpy:10799
translate japanese ending_ipe_mne_8fafdfea:

    masaru "I have to leave this school…"
    masaru "私はこの学校を離れる必要があります…"

# game/script.rpy:10801
translate japanese ending_ipe_mne_29419cb4:

    toshu "W-wha…? Why?!"
    toshu "W-ワ？…？なぜ？！"

# game/script.rpy:10802
translate japanese ending_ipe_mne_dd01771e:

    masaru "My scholarship got completely terminated because I failed that exam…"
    masaru "私はその試験に合格しなかったので、私の奨学金は完全に終了しました…"

# game/script.rpy:10804
translate japanese ending_ipe_mne_841f8f36:

    masaru "I don't have enough resources to continue studying here…"
    masaru "私はここで勉強を続けるのに十分なリソースがありません…"

# game/script.rpy:10806
translate japanese ending_ipe_mne_cd04a07b:

    masaru "I really don't know what to do…"
    masaru "私は本当に何をすべきかわからない…"

# game/script.rpy:10808
translate japanese ending_ipe_mne_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:10810
translate japanese ending_ipe_mne_fa4a54a0:

    masaru "The music club didn't accept me back either…"
    masaru "音楽クラブは私を受け入れなかった…"

# game/script.rpy:10812
translate japanese ending_ipe_mne_1b1c820b:

    masaru "I will have to find a part time job first…"
    masaru "私はパートタイムの仕事を最初に見つけなければなりません…"

# game/script.rpy:10814
translate japanese ending_ipe_mne_a9a688b8:

    masaru "I want to save money as soon as I can to be able to finish my studies…"
    masaru "私は私の研究を終えることができるようになるとすぐにお金を節約したいです…"

# game/script.rpy:10816
translate japanese ending_ipe_mne_e65db101:

    masaru "Sorry, Toshu… I have to go…"
    masaru "Sorry, Toshu… I have to go…"

# game/script.rpy:10818
translate japanese ending_ipe_mne_50903839:

    toshu "Masaru…"
    toshu "Masaru…"

# game/script.rpy:10820
translate japanese ending_ipe_mne_f2e1c98b:

    toshu_t "I couldn't believe what Masaru said…"
    toshu_t "私はマサルが言ったことを信じられませんでした…"

# game/script.rpy:10821
translate japanese ending_ipe_mne_fc108e68:

    toshu_t "Masaru… my dear friend…"
    toshu_t "マサル…私の親愛なる友人…"

# game/script.rpy:10822
translate japanese ending_ipe_mne_5193ecc7:

    toshu_t "He worked so hard to stay in this school…"
    toshu_t "彼はこの学校にとどまるためにとても頑張った…"

# game/script.rpy:10823
translate japanese ending_ipe_mne_fcc63ec9:

    toshu_t "Was it not enough? Why does he still have to leave…?"
    toshu_t "十分ではありませんでしたか？なぜ彼はまだ放置しなければならないのですか？"

# game/script.rpy:10830
translate japanese ending_ipe_mne_b2b24239:

    ichiru "Oh, Toshu!"
    ichiru "Oh, Toshu!"

# game/script.rpy:10832
translate japanese ending_ipe_mne_ac5b77e5:

    toshu "Ichiru! Is it true that Masaru will leave?!"
    toshu "Ichiru！まるが離れるのは本当ですか？"

# game/script.rpy:10834
translate japanese ending_ipe_mne_e191807b:

    toshu "Please tell me that it isn't true!"
    toshu "それは本当ではないことを教えてください！"

# game/script.rpy:10836
translate japanese ending_ipe_mne_c9c26806:

    ichiru "I'm sorry, Toshu…"
    ichiru "I'm sorry, Toshu…"

# game/script.rpy:10837
translate japanese ending_ipe_mne_f2ffcb81:

    ichiru "But I'm afraid he's telling the truth."
    ichiru "しかし、私は彼が真実を伝えているのではないかと心配しています。"

# game/script.rpy:10839
translate japanese ending_ipe_mne_364f49a2_1:

    # toshu "…"
    toshu "…"

# game/script.rpy:10841
translate japanese ending_ipe_mne_10b9cb04:

    ichiru "There's really nothing we could've done…"
    ichiru "本当に何もできなかった…"

# game/script.rpy:10842
translate japanese ending_ipe_mne_d85bdeea:

    ichiru "We all saw how Masaru gave everything he could to pass those exams…"
    ichiru "私たちは皆、マサルがそれらの試験に合格するためにできることすべてを与えたのを見ました…"

# game/script.rpy:10844
translate japanese ending_ipe_mne_7504aeee:

    toshu "You're right, Ichiru… I'm sure nobody wanted this…"
    toshu "あなたは正しい、Ichiru…私は誰もこれを望んでいないと確信しています…"

# game/script.rpy:10846
translate japanese ending_ipe_mne_3590a0d6:

    ichiru "Please don't be sad anymore!"
    ichiru "もう悲しくならないでください！"

# game/script.rpy:10848
translate japanese ending_ipe_mne_a3034f1f:

    ichiru "I'll be always here for you!"
    ichiru "私はいつもここにいるよ！"

# game/script.rpy:10850
translate japanese ending_ipe_mne_8e951ac7:

    toshu "Ichiru…"
    toshu "イチール…"

# game/script.rpy:10852
translate japanese ending_ipe_mne_3b6311d7:

    ichiru "To be honest, I'm just as devastated as you…"
    ichiru "正直言って、私はあなたと同じように荒廃しています…"

# game/script.rpy:10853
translate japanese ending_ipe_mne_0b533203:

    ichiru "Masaru… He had it really rough…"
    ichiru "マサル…彼は本当に荒い…"

# game/script.rpy:10855
translate japanese ending_ipe_mne_982a1650:

    ichiru "In fact… his problems were way bigger than mine."
    ichiru "実際…彼の問題は私のものよりも大きかった。"

# game/script.rpy:10856
translate japanese ending_ipe_mne_b8b442ea:

    ichiru "I understand the fact that he needed to sacrifice some of the things he want for the things he need."
    ichiru "私は彼が必要とするもののために必要なもののいくつかを犠牲にする必要があったという事実を理解しています。"

# game/script.rpy:10858
translate japanese ending_ipe_mne_afa6b4a6:

    ichiru "I'm sure Masaru doesn't want us to be sad because of him."
    ichiru "マサルは私たちが彼のために悲しいことを望んでいないと確信しています。"

# game/script.rpy:10860
translate japanese ending_ipe_mne_14bf1357:

    ichiru "So let's try our best to cheer up!"
    ichiru "さあ、元気に応援しよう！"

# game/script.rpy:10861
translate japanese ending_ipe_mne_7b0a036e:

    ichiru "I'm sure we can all still hangout together! We'll find a way!"
    ichiru "私たちはまだ一緒にハングアウトすることができると確信しています！私たちは方法を見つけるでしょう！"

# game/script.rpy:10863
translate japanese ending_ipe_mne_a0f1960f:

    toshu_t "Ichiru's words were exactly the things I needed to hear!"
    toshu_t "イチールの言葉は、私が聞く必要があったこととまったく同じでした！"

# game/script.rpy:10864
translate japanese ending_ipe_mne_9aa41081:

    toshu_t "He sure knows what to say to cheer me up!"
    toshu_t "彼は私を応援するために何を言うべきかを知っている！"

# game/script.rpy:10865
translate japanese ending_ipe_mne_8c3cd1db:

    toshu_t "I am really looking forward to meeting Masaru again."
    toshu_t "私は本当にMasaruに再び会うのを楽しみにしています。"

# game/script.rpy:10866
translate japanese ending_ipe_mne_65876b86:

    toshu_t "I will totally cherish the time I have with Ichiru!"
    toshu_t "私はIchiruと一緒にいる時間を大切にします！"
