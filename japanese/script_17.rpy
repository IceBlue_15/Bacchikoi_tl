# game/script.rpy:9570
translate japanese ending_ibe_mbe_4760d0aa:

    toshu_t "The weekend has passed after that intense tournament."
    toshu_t "その激しいトーナメントの後、週末が過ぎました。"

# game/script.rpy:9571
translate japanese ending_ibe_mbe_1aa1bffb:

    toshu_t "I want to know how my friends have been doing…"
    toshu_t "私の友達がどうしているのか知りたい"

# game/script.rpy:9579
translate japanese ending_ibe_mbe_2d78b941:

    toshu "I… it's pretty quiet around…"
    toshu "私は…周りはかなり静かです…"

# game/script.rpy:9581
translate japanese ending_ibe_mbe_b65efb71:

    toshu "It totally feels different without Ichiru or Masaru…"
    toshu "イチールやマサルなしで全く違う感じ…"

# game/script.rpy:9582
translate japanese ending_ibe_mbe_548b7b6a:

    toshu "Where is everyone…?"
    toshu "みんなはどこですか…？"

# game/script.rpy:9584
translate japanese ending_ibe_mbe_adf24c14:

    random "HEY, YOU!"
    random "ねえ！"

# game/script.rpy:9586
translate japanese ending_ibe_mbe_f65923fa:

    tomoka "You're that twerp that Ichiru always mention about right?"
    tomoka "あなたはIchiruが常に正しいことを言及するtwerpですか？"

# game/script.rpy:9588
translate japanese ending_ibe_mbe_730389cc:

    toshu "Tomoka?"
    toshu "Tomoka?"

# game/script.rpy:9590
translate japanese ending_ibe_mbe_4d014980:

    tomoka "That's PRINCESS Saji to you!"
    tomoka "それはあなたに君臨するサジです！"

# game/script.rpy:9592
translate japanese ending_ibe_mbe_3a2718b4:

    toshu "W-what happened with Ichiru?"
    toshu "イチールに何が起こったの？"

# game/script.rpy:9594
translate japanese ending_ibe_mbe_22967a3d:

    tomoka "Oh, You didn't know?"
    tomoka "ああ、あなたは知らなかった？"

# game/script.rpy:9597
translate japanese ending_ibe_mbe_951ac901:

    tomoka "He already transferred away from this dump!"
    tomoka "彼はすでにこのダンプから移った！"

# game/script.rpy:9598
translate japanese ending_ibe_mbe_df59d8b3:

    tomoka "He's with me now~"
    tomoka "彼は今私と一緒だ〜"

# game/script.rpy:9600
translate japanese ending_ibe_mbe_686ed303:

    tomoka "HOHOHOHOHO~!!"
    tomoka "ホホロホロ〜!!"

# game/script.rpy:9602
translate japanese ending_ibe_mbe_9468668c:

    toshu "Wh-What!!??? What do you mean??!"
    toshu "Whーホワット!!どういう意味ですか？？！"

# game/script.rpy:9603
translate japanese ending_ibe_mbe_9f21ec60:

    toshu "I thought he's gonna--"
    toshu "私は彼が〜と思った。"

# game/script.rpy:9605
translate japanese ending_ibe_mbe_a0681fa2:

    tomoka "SHUT UP!"
    tomoka "黙れ！"

# game/script.rpy:9607
translate japanese ending_ibe_mbe_6ffac97d:

    tomoka "My dad couldn't care less about what happens in that shitty tournament!"
    tomoka "私のお父さんは、そのくそったトーナメントで何が起こるかについてはあまり気にすることができませんでした！"

# game/script.rpy:9608
translate japanese ending_ibe_mbe_6cd674c5:

    tomoka "He will do whatever as I please~"
    tomoka "彼は私が気に入っているように何でもするだろう〜"

# game/script.rpy:9610
translate japanese ending_ibe_mbe_451ee9a1:

    toshu "I… I don't believe you!"
    toshu "私は…あなたを信じていない！"

# game/script.rpy:9612
translate japanese ending_ibe_mbe_fe7da577:

    tomoka "Would you stop living in your make-believe worthless-world, you ignorant fool!"
    tomoka "あなたは無益な世界を作る、あなたは無知な愚か者を作ることをやめようと思いますか？"

# game/script.rpy:9614
translate japanese ending_ibe_mbe_acddb379:

    tomoka "Oh! Dear me, what am I doing?"
    tomoka "ああ！親愛なる私、私は何をしていますか？"

# game/script.rpy:9616
translate japanese ending_ibe_mbe_bcb65f18:

    tomoka "I was just filing my dear Ichiru's transfer papers and I stumbled upon this trash!"
    tomoka "私はちょうど私の親愛なるIchiruの転送用紙を提出していたと私はこのゴミにつまずいた！"

# game/script.rpy:9617
translate japanese ending_ibe_mbe_af0c4b35:

    tomoka "Hmph! I'm have other important things to do!"
    tomoka "Hmph！私は他にも重要なことをしています！"

# game/script.rpy:9619
translate japanese ending_ibe_mbe_cf008a72:

    tomoka "Good bye, TRASH!"
    tomoka "さようなら、TRASH！"

# game/script.rpy:9621
translate japanese ending_ibe_mbe_53ca8cac:

    tomoka "And by the way, I can't believe Ichiru called you friend! It makes me so sick!"
    tomoka "ところで、私はIchiruがあなたを友人と呼ぶとは信じられません！それは私がとても病気になります！"

# game/script.rpy:9623
translate japanese ending_ibe_mbe_f272b826:

    tomoka "HOHOHOHOOHOH~! WAHOHOHOHOH~!!!"
    tomoka "ホホロホアホ〜！ワハ！"

# game/script.rpy:9625
translate japanese ending_ibe_mbe_8d32682d:

    toshu_t "I didn't believe what Tomoka said. Not even one bit…"
    toshu_t "私はTomokaが言ったことを信じていませんでした。 1ビットでも…"

# game/script.rpy:9626
translate japanese ending_ibe_mbe_4fe85f52:

    toshu_t "Ichiru… my dear friend…"
    toshu_t "Ichiru …親愛なる友人…"

# game/script.rpy:9627
translate japanese ending_ibe_mbe_381aa84f:

    toshu_t "Would he really leave… without saying goodbye?"
    toshu_t "彼は本当にお別れを言わずに去るだろうか？"

# game/script.rpy:9635
translate japanese ending_ibe_mbe_8fbaf59f:

    genji "Oh, h…hello Toshu."
    genji "Oh, h…hello Toshu."

# game/script.rpy:9637
translate japanese ending_ibe_mbe_672bd564:

    toshu "C-Coach!!"
    toshu "Cコーチ!!"

# game/script.rpy:9638
translate japanese ending_ibe_mbe_1f91cb9d:

    toshu "Where is Ichiru?!"
    toshu "Ichiruはどこですか？"

# game/script.rpy:9640
translate japanese ending_ibe_mbe_729d2365:

    toshu "Please tell me that what Tomoka's saying is a lie!"
    toshu "トモカが言っていることは嘘だと教えてください！"

# game/script.rpy:9641
translate japanese ending_ibe_mbe_edf48b66:

    toshu "Is it true that Ichiru already left?!"
    toshu "Ichiruはすでに残っていることは本当ですか？"

# game/script.rpy:9643
translate japanese ending_ibe_mbe_75a12fcd:

    genji "I'm sorry, Toshu…"
    genji "I'm sorry, Toshu…"

# game/script.rpy:9644
translate japanese ending_ibe_mbe_8360cdc0:

    genji "But I'm afraid she's telling the truth."
    genji "しかし、彼女は真実を伝えているのではないかと心配しています。"

# game/script.rpy:9645
translate japanese ending_ibe_mbe_beedc66e:

    genji "What's worse is…"
    genji "悪いのは…"

# game/script.rpy:9647
translate japanese ending_ibe_mbe_c5af37f5:

    genji "Masaru… said that… he would also have to leave…"
    genji "マサルも言った…彼はまた出なければならないだろう…"

# game/script.rpy:9648
translate japanese ending_ibe_mbe_75c11ed7:

    genji "After this semester… he can't continue anymore…"
    genji "この学期の後…彼はもはや続けることができません…"

# game/script.rpy:9650
translate japanese ending_ibe_mbe_c09a1861:

    toshu "W-wha…? Why? Why Masaru too?!!"
    toshu "W-ワ？…？どうして？なぜマサルなの？"

# game/script.rpy:9652
translate japanese ending_ibe_mbe_6b8b8e68:

    genji "His scholarship got completely terminated after failing that remedial exam."
    genji "彼の奨学金は、その修復試験に失敗した後、完全に終了しました。"

# game/script.rpy:9653
translate japanese ending_ibe_mbe_fdefb178:

    genji "I don't have enough resources to continue helping him either…"
    genji "私は彼を助けるのに十分な資源がありません…"

# game/script.rpy:9655
translate japanese ending_ibe_mbe_fa1d52e7:

    genji "I really don't know what to do…"
    genji "私は本当に何をすべきかわからない…"

# game/script.rpy:9657
translate japanese ending_ibe_mbe_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:9659
translate japanese ending_ibe_mbe_1b2f5d13:

    genji "The music club didn't accept him anymore too…"
    genji "音楽クラブはもはや彼を受け入れなかった…"

# game/script.rpy:9660
translate japanese ending_ibe_mbe_f8bb2b87:

    genji "He… will have to find a part time job first…"
    genji "彼は…パートタイムの仕事を最初に見つけなければならないでしょう…"

# game/script.rpy:9661
translate japanese ending_ibe_mbe_00062083:

    genji "He told me he wants to save money to finish his studies…"
    genji "彼は自分の研究を終えるためにお金を節約したいと私に言った…"

# game/script.rpy:9663
translate japanese ending_ibe_mbe_e1e5086d:

    toshu "I…"
    toshu "私…"

# game/script.rpy:9667
translate japanese ending_ibe_mbe_e67d0f7e:

    # toshu "It's not fair!"
    toshu "そんなのおかしいよ！"

# game/script.rpy:9668
translate japanese ending_ibe_mbe_81cd8af3:

    toshu "Why both of them have to leave?!"
    toshu "なぜ彼らの両方を残す必要がありますか？"

# game/script.rpy:9669
translate japanese ending_ibe_mbe_c19dd638:

    genji "I'm really sorry, Toshu."
    genji "私は本当に申し訳ありません、Toshu。"

# game/script.rpy:9670
translate japanese ending_ibe_mbe_1061a731:

    toshu "Both of them left without a word…!"
    toshu "二人とも一言も言わずに…！"

# game/script.rpy:9671
translate japanese ending_ibe_mbe_03c5e25e:

    toshu "I'm such a useless friend!"
    toshu "私はそのような無駄な友人です！"

# game/script.rpy:9672
translate japanese ending_ibe_mbe_8dea0d28:

    toshu "I couldn't do anything to prevent this from happening!"
    toshu "私はこれを防ぐために何もできませんでした！"

# game/script.rpy:9674
translate japanese ending_ibe_mbe_553f0905:

    toshu_t "The tears from my eyes refused to stop from falling…"
    toshu_t "私の目から涙が落ちるのを止めなかった…"

# game/script.rpy:9675
translate japanese ending_ibe_mbe_ff490676:

    toshu_t "I couldn't describe the sadness I felt…"
    toshu_t "私が感じた悲しみを説明できませんでした…"

# game/script.rpy:9676
translate japanese ending_ibe_mbe_439f849b:

    toshu_t "I've always said to my friends…"
    toshu_t "私はいつも私の友達に言った…"

# game/script.rpy:9677
translate japanese ending_ibe_mbe_3086fc3a:

    toshu_t "To never give up even when things goes bad…"
    toshu_t "物事が悪くなっても決してあきらめないために…"

# game/script.rpy:9678
translate japanese ending_ibe_mbe_abb3df94:

    toshu_t "But it looks like… this time…"
    toshu_t "しかし、今のように見える…"

# game/script.rpy:9679
translate japanese ending_ibe_mbe_086b4b99:

    toshu_t "I am the one… who will break down…"
    toshu_t "私はその人です…誰が倒れるでしょう…"
