# game/script.rpy:7970
translate japanese after_minigame_6_b0dbfece:

    toshu "Fwah! What a great day!"
    toshu "フワ！なんていい日だ！"

# game/script.rpy:7972
translate japanese after_minigame_6_306cec70:

    # masaru "Toshu!"
    masaru "Toshu！"

# game/script.rpy:7974
translate japanese after_minigame_6_1b6aa744:

    # toshu "Oh! Hi, Masaru! You're here early!"
    toshu "あ、こんにちはMasaruさん！ 早いですね！"

# game/script.rpy:7976
translate japanese after_minigame_6_ec7d15bc:

    # masaru "You are not with Ichiru?"
    masaru "Ichiruと一緒じゃないのか？"

# game/script.rpy:7978
translate japanese after_minigame_6_7049dd6d:

    # toshu "Not at the moment. Why is it?"
    toshu "違いますけど、何でですか？"

# game/script.rpy:7980
translate japanese after_minigame_6_e78df5af:

    # masaru "Can I ask you a favor?"
    masaru "ちょっと頼み事してもいいか？"

# game/script.rpy:7982
translate japanese after_minigame_6_1742f70f:

    # toshu "Sure thing! What is it?"
    toshu "もちろんですよ！ 何でしょうか？"

# game/script.rpy:7983
translate japanese after_minigame_6_5ed38ba9:

    # masaru "Well…"
    masaru "その…"

# game/script.rpy:7985
translate japanese after_minigame_6_51f7a8ef:

    # masaru "My little brother is here."
    masaru "ここに俺の弟がいるんだが。"

# game/script.rpy:7987
translate japanese after_minigame_6_60a2cf0c:

    # sohma "H-hi, Big Brother's friend!"
    sohma "は…はじめまして、お兄ちゃんのお友達の方ですよね。"

# game/script.rpy:7989
translate japanese after_minigame_6_b59ad4ff:

    # masaru "I… I need you to look after him while I go meet the faculty."
    masaru "俺が試験を受けてる間、こいつのこと見といて欲しいんだ。"

# game/script.rpy:7990
translate japanese after_minigame_6_35629399:

    # masaru "I am so sorry, I suddenly asked you this favour!"
    masaru "いきなりこんなお願いごとして申し訳ない！"

# game/script.rpy:7992
translate japanese after_minigame_6_a352cfa5:

    masaru "B-but his teacher didn't attend his class. So I need someone to look after him…"
    masaru "彼の先生は彼のクラスに出席しなかった。だから私は彼を見守る人が必要です…"

# game/script.rpy:7993
translate japanese after_minigame_6_a5f84397:

    # masaru "I was hoping Ichiru would take care of him for me…"
    masaru "Ichiruにお願いしようかと思ってたんだが…"

# game/script.rpy:7995
translate japanese after_minigame_6_704b4838:

    # toshu "No problem Captain! I will look after Sohma!"
    toshu "大丈夫ですよキャプテン！ Sohmaくんの面倒は僕が見ます！"

# game/script.rpy:7996
translate japanese after_minigame_6_f7e9e508:

    # toshu "Please go ahead!"
    toshu "どうぞ行ってきて下さい！"

# game/script.rpy:7998
translate japanese after_minigame_6_2c011715:

    # masaru "Thank you, Toshu! I really owe you one!"
    masaru "ありがとう、Toshu！ これで一つ借りだな！"

# game/script.rpy:8001
translate japanese after_minigame_6_998590bf:

    # toshu "Hello, Sohma!"
    toshu "こんにちは、ソーマくん！"

# game/script.rpy:8003
translate japanese after_minigame_6_40aed596:

    # sohma "…"
    sohma "…"

# game/script.rpy:8005
translate japanese after_minigame_6_50a40def:

    # toshu "Haha! You don't have to be shy!"
    toshu "ハハハ！ 恥ずかしがらなくてもいいよ！"

# game/script.rpy:8007
translate japanese after_minigame_6_ed1797d9:

    # sohma "I-I'm sorry!"
    sohma "ご…ごめんなさい！"

# game/script.rpy:8009
translate japanese after_minigame_6_266e7ed3:

    # toshu "Eh? What are you apologizing for?"
    toshu "え？ 何で謝ってんの？"

# game/script.rpy:8011
translate japanese after_minigame_6_b4be9f12:

    # sohma "N-nothing…"
    sohma "な、何でもないです…"

# game/script.rpy:8013
translate japanese after_minigame_6_621f1ad2:

    # toshu "Let's go inside the classroom then?"
    toshu "教室の中に入ってようか？"

# game/script.rpy:8014
translate japanese after_minigame_6_ef78fc42:

    # sohma "O-okay…"
    sohma "は、はい…"

# game/script.rpy:8023
translate japanese after_minigame_6_ae0b52cd:

    # ichiru "Oh! Toshu!"
    ichiru "おお！ Toshuじゃねーか！"

# game/script.rpy:8025
translate japanese after_minigame_6_30dfafe9:

    # toshu "Wow! You're here so early!"
    toshu "うわー！ 今日は早いねえ！"

# game/script.rpy:8026
translate japanese after_minigame_6_d7304987:

    # toshu "Masaru was looking for you."
    toshu "Masaruさんが探してたよ。"

# game/script.rpy:8028
translate japanese after_minigame_6_a3682fd1:

    # ichiru "Who's that behind you?"
    ichiru "後ろにいるの誰？"

# game/script.rpy:8030
translate japanese after_minigame_6_8e63ffbc:

    # toshu "It's Masaru's little brother!"
    toshu "Masaruさんの弟だよ！"

# game/script.rpy:8032
translate japanese after_minigame_6_2165e701:

    # ichiru "Oh! It's Sohma!"
    ichiru "ああ！ Sohmaか！"

# game/script.rpy:8033
translate japanese after_minigame_6_05422b8c:

    # ichiru "Why are you with him, Toshu?"
    ichiru "Toshu、何でコイツと一緒なんだ？"

# game/script.rpy:8034
translate japanese after_minigame_6_7dc73d6d:

    toshu "Ah, Masaru asked me to keep an eye on him while he is having the exam."
    toshu "ああ、マサルは、試験を受けている間、私に彼に目を留めるように頼んだ。"

# game/script.rpy:8036
translate japanese after_minigame_6_882103d0:

    # ichiru "Oh, is that so?"
    ichiru "あー、そうなんだ？"

# game/script.rpy:8038
translate japanese after_minigame_6_af12ed29:

    # toshu "Masaru was supposed to ask you to take care of him, y'know?"
    toshu "MasaruさんはIchiruに世話をお願いしたかったみたいだけど、知ってる？"

# game/script.rpy:8040
translate japanese after_minigame_6_28a0cf08:

    # ichiru "Heya! What's up, Sohma!"
    ichiru "ヘイ！ 元気か、Sohma！"

# game/script.rpy:8042
translate japanese after_minigame_6_e6228012:

    # sohma "I'm fine. Thank you for being a good friend to my big brother!"
    sohma "はい、元気です。兄がいつもお世話になってます！"

# game/script.rpy:8044
translate japanese after_minigame_6_42d1e004:

    ichiru "Don't mention it! We always take good care of him!"
    ichiru "どういたしまして！私たちは常に彼の世話をする！"

# game/script.rpy:8045
translate japanese after_minigame_6_d908b464:

    toshu "More like the other way around, Ichiru."
    toshu "他の方法と同じように、Ichiru。"

# game/script.rpy:8047
translate japanese after_minigame_6_ec534a08:

    toshu "So, Sohma, what things do you always do with your big brother at home?"
    toshu "だから、ソマ、あなたはいつも自宅であなたの兄と何をしていますか？"

# game/script.rpy:8049
translate japanese after_minigame_6_bdae5509:

    sohma "N-nothing much… it's usually quiet around our house…"
    sohma "何もない…私たちの家の周りは普通静かです…"

# game/script.rpy:8050
translate japanese after_minigame_6_c06a1af1:

    sohma "But when dad comes home he always scolds us."
    sohma "しかし、お父さんが家に帰ると、彼はいつも私たちを叱る。"

# game/script.rpy:8052
translate japanese after_minigame_6_344323f6:

    sohma "Me and Big Brother usually lock ourselves up in our room by the time dad comes home…"
    sohma "私とビッグブラザーは、通常、お父さんが家に帰るまでに私たちの部屋に自分を閉じ込めます…"

# game/script.rpy:8054
translate japanese after_minigame_6_145d07cc:

    # toshu "Oh… that's pretty harsh…"
    toshu "ああ…それは大変だね…"

# game/script.rpy:8056
translate japanese after_minigame_6_5e7f829a:

    ichiru "Way to go for a topic, Toshu."
    ichiru "この話やめようぜ、Toshu。"

# game/script.rpy:8058
translate japanese after_minigame_6_cc42aba2:

    sohma "I-it's okay, sometimes when dad scolds me, Big Brother cheers me up!"
    sohma "私は大丈夫です、時にはお父さんが私を叱るとき、ビッグブラザーが私を鼓舞します！"

# game/script.rpy:8060
translate japanese after_minigame_6_87f85870:

    ichiru "What's with your dad anyways? He sounds like a nasty person."
    ichiru "とにかくあなたのお父さんはどうですか？彼は厄介な人のように聞こえる。"

# game/script.rpy:8062
translate japanese after_minigame_6_6b264a52:

    sohma "I… I don't know."
    sohma "私は…私は知らない。"

# game/script.rpy:8064
translate japanese after_minigame_6_91922aab:

    toshu "Ah, ah! Let's not talk about this sad stuff!"
    toshu "ああああ！この悲しいものについては話しません！"

# game/script.rpy:8066
translate japanese after_minigame_6_b980374b:

    ichiru "Yeah… I told you so."
    ichiru "うん…私はそう言った。"

# game/script.rpy:8068
translate japanese after_minigame_6_0e9939d9:

    # toshu "So, Sohma, what do you like to do for fun?"
    toshu "じゃあSohmaくん、何をするのが好きかな？"

# game/script.rpy:8070
translate japanese after_minigame_6_bd1d614b:

    # sohma "F-for fun?"
    sohma "す、好きなことですか？"

# game/script.rpy:8072
translate japanese after_minigame_6_655593dc:

    # ichiru "Yeah! Like hanging out with your friends or anything?"
    ichiru "ああ！ 友達とぶらぶらするのはどうだ？"

# game/script.rpy:8075
translate japanese after_minigame_6_bbbf7846:

    # sohma "I don't have any…"
    sohma "そういうのはちょっと…"

# game/script.rpy:8077
translate japanese after_minigame_6_054780c3:

    # toshu "Why is that?"
    toshu "何でだい？"

# game/script.rpy:8078
translate japanese after_minigame_6_2f603456:

    # sohma "I… am scared…"
    sohma "僕…怖いんです…"

# game/script.rpy:8080
translate japanese after_minigame_6_e0ad3037:

    # ichiru "E-eh? Scared of what?"
    ichiru "え？ 何が怖いっての？"

# game/script.rpy:8082
translate japanese after_minigame_6_acda19d5:

    sohma "Most of my classmates say I am a useless coward…"
    sohma "私のクラスメートのほとんどは、私が無用な臆病者だと言います…"

# game/script.rpy:8084
translate japanese after_minigame_6_35c4a9fd:

    # sohma "They often hit me… and push me around…"
    sohma "みんなはよく僕を叩いたり…押したりするんだ…"

# game/script.rpy:8085
translate japanese after_minigame_6_95f72d2a:

    # sohma "Nobody wants to be friends with me…"
    sohma "誰も僕と友達になんかなりたくないんだ…"

# game/script.rpy:8087
translate japanese after_minigame_6_bde053a4:

    sohma "So I think, what they are saying is probably true…"
    sohma "だから、彼らが言っていることは、おそらく真実だと思います…"

# game/script.rpy:8089
translate japanese after_minigame_6_98c958e2:

    ichiru "That's no good…"
    ichiru "それはまずいです…"

# game/script.rpy:8091
translate japanese after_minigame_6_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:8093
translate japanese after_minigame_6_ff05be9e:

    toshu "H-have you tried standing up for yourself?"
    toshu "あなた自身のために起立しようとしましたか？"

# game/script.rpy:8095
translate japanese after_minigame_6_099666aa:

    # sohma "There's no use…"
    sohma "そんなことしても無駄だよ…"

# game/script.rpy:8097
translate japanese after_minigame_6_a374e229:

    toshu "You can't be like that forever! You are not doing anything bad to them, so why should you suffer?"
    toshu "あなたはそれを永遠にすることはできません！あなたは彼らに悪いことをしていないので、なぜあなたは苦しんでいますか？"

# game/script.rpy:8099
translate japanese after_minigame_6_f27f205c:

    # sohma "I… can't… I'm weak…"
    sohma "そんなこと…できない…僕は弱いんだ…"

# game/script.rpy:8100
translate japanese after_minigame_6_4c369898:

    sohma "They're right about me being a wimp…"
    sohma "彼らは私が暴れているのは正しい…"

# game/script.rpy:8102
translate japanese after_minigame_6_1a86fefc:

    # toshu "No you are not."
    toshu "そんなことないよ。"

# game/script.rpy:8104
translate japanese after_minigame_6_a5bc0df6:

    # toshu "If you keep telling yourself that you are weak, you will really become like one."
    toshu "自分のこと弱いって言い続けてたら、本当にそうなっちゃうよ。"

# game/script.rpy:8105
translate japanese after_minigame_6_07e84905:

    # sohma "W-what should I do…?"
    sohma "じゃ、じゃあどうすれば…？"

# game/script.rpy:8107
translate japanese after_minigame_6_53b6ca9b:

    # toshu "Isn't it obvious, fight back!"
    toshu "当たり前のことさ、やり返すんだ！"

# game/script.rpy:8109
translate japanese after_minigame_6_d0beb068:

    # ichiru "T-Toshu…"
    ichiru "To、Toshu…"

# game/script.rpy:8111
translate japanese after_minigame_6_aa2c9efd:

    toshu "I'm not telling you to become violent. Because it will only lower you down to their level."
    toshu "私はあなたに暴力的になるように言わない。あなたのレベルまであなたを下げるだけなので。"

# game/script.rpy:8113
translate japanese after_minigame_6_dcd1db01:

    toshu "I'm just saying, no one deserves to be bullied!"
    toshu "私はちょうど、誰もいじめに値しないと言っている！"

# game/script.rpy:8114
translate japanese after_minigame_6_87d1e70f:

    toshu "You should fight for your right!"
    toshu "あなたはあなたのために戦うべきです！"

# game/script.rpy:8116
translate japanese after_minigame_6_f2b18e92:

    toshu "If you can't, tell someone who can!"
    toshu "できない場合は、誰かに教えてください！"

# game/script.rpy:8118
translate japanese after_minigame_6_06c1bd75:

    sohma "I-I did…!"
    sohma "私はそうでした…！"

# game/script.rpy:8119
translate japanese after_minigame_6_b766d814:

    sohma "Big Brother always came to the rescue!"
    sohma "ビッグブラザーはいつも救助に来た！"

# game/script.rpy:8120
translate japanese after_minigame_6_0f6b1d04:

    sohma "He defended me many times!"
    sohma "彼は何度も私を守った！"

# game/script.rpy:8122
translate japanese after_minigame_6_441c8e6a:

    sohma "But as it kept happening… Big Brother got mad to me whenever I told him about my classmates bullying me…"
    sohma "しかし、それが起こっているように…私を悩ましている私のクラスメートについて彼に言ったとき、ビッグブラザーは私に怒った…"

# game/script.rpy:8123
translate japanese after_minigame_6_bf665523:

    sohma "So I stopped telling it to him…"
    sohma "だから私は彼にそれを伝えるのをやめた。"

# game/script.rpy:8124
translate japanese after_minigame_6_c638b5dc:

    sohma "I will wait for Big Brother to defend me again…"
    sohma "私はビッグブラザーが私を再び守るのを待つ…"

# game/script.rpy:8126
translate japanese after_minigame_6_1064a9c9:

    toshu "That's wrong!"
    toshu "それは間違っている！"

# game/script.rpy:8128
translate japanese after_minigame_6_39052cc4:

    sohma "W-why…? He is my big brother…"
    sohma "W-なぜ…？彼は私の兄弟です…"

# game/script.rpy:8130
translate japanese after_minigame_6_17a18399:

    toshu "You can't expect him to always solve things for you!"
    toshu "あなたは彼がいつもあなたのために物事を解決するとは期待できません！"

# game/script.rpy:8131
translate japanese after_minigame_6_ede62a11:

    toshu "Your big brother has his own life!"
    toshu "あなたの兄は自分の人生を持っています！"

# game/script.rpy:8133
translate japanese after_minigame_6_b989a9e5:

    toshu "The thing here is, IT KEEPS HAPPENING!"
    toshu "ここのことは、IT KEEPS HAPPENING！"

# game/script.rpy:8134
translate japanese after_minigame_6_ad5ad651:

    toshu "When something bad happens over and over, you're definitely doing something wrong!"
    toshu "何か悪いことが何度も何度も繰り返されると、あなたは間違って何かをやっているのです！"

# game/script.rpy:8136
translate japanese after_minigame_6_62ecbfd1:

    sohma "I told you…! I didn't do anything wrong!!!"
    sohma "先ほども言いました…！私は間違って何もしなかった！"

# game/script.rpy:8138
translate japanese after_minigame_6_4320943b:

    # sohma "Waaaahhhh…!!!"
    sohma "うわあああぁぁぁぁ…！！！"

# game/script.rpy:8140
translate japanese after_minigame_6_e2a16369:

    toshu "Sohma, change is two-way!"
    toshu "Sohma、変更は双方向です！"

# game/script.rpy:8142
translate japanese after_minigame_6_3a450a08:

    toshu "There's always a time when you have to change something with yourself if you want things to be better!"
    toshu "物事をより良くしたい場合は、いつも自分で何かを変えなければならない時があります！"

# game/script.rpy:8144
translate japanese after_minigame_6_88993301:

    sohma "Waaahhh! Wahhh…!"
    sohma "ワーハイ！うーん…！"

# game/script.rpy:8146
translate japanese after_minigame_6_b5d2e253:

    # ichiru "Toshu… calm down… he's just a kid…"
    ichiru "Toshu…落ち着けよ…相手はただの子供だぞ…"

# game/script.rpy:8148
translate japanese after_minigame_6_364f49a2_1:

    # toshu "…"
    toshu "…"

# game/script.rpy:8150
translate japanese after_minigame_6_ccb0aedc:

    # toshu "I'm sorry, Sohma… I didn't mean to make you cry…"
    toshu "ゴメンね、Sohmaくん…泣かせるつもりじゃなかったんだ…"

# game/script.rpy:8152
translate japanese after_minigame_6_2575a9e9:

    sohma "B-but…*sniff* y-you're right…"
    sohma "B  - しかし… *スニフ* Y  - あなたは正しい…"

# game/script.rpy:8153
translate japanese after_minigame_6_7b0c0d35:

    sohma "All this time… I've been depending on Big Brother…"
    sohma "今度は…私はビッグブラザーに頼ってきました…"

# game/script.rpy:8155
translate japanese after_minigame_6_364f49a2_2:

    # toshu "…"
    toshu "…"

# game/script.rpy:8157
translate japanese after_minigame_6_646937cb:

    sohma "I didn't realize… I might have been burdening Big Brother all this time…"
    sohma "私は気づいていなかった…今度はBig Brotherに負担をかけていたかもしれない…"

# game/script.rpy:8158
translate japanese after_minigame_6_ab98e9a9:

    sohma "I… I have to stop being like this…"
    sohma "私は…このようなことをやめなければなりません…"

# game/script.rpy:8159
translate japanese after_minigame_6_cc188019:

    sohma "I don't want Big Brother to be hurt because of me…"
    sohma "私はビッグブラザーが私のために傷つけられたくない…"

# game/script.rpy:8161
translate japanese after_minigame_6_21a685d5:

    ichiru "H-hey, Sohma… it's alright…"
    ichiru "こんにちは、Sohma …それは大丈夫です…"

# game/script.rpy:8163
translate japanese after_minigame_6_7ab47b2f:

    sohma "It's my entire fault…"
    sohma "それは私の全部の欠点です…"

# game/script.rpy:8165
translate japanese after_minigame_6_4753edcb:

    toshu "There's no point into blaming yourself, Sohma…"
    toshu "Sohmaを責めることには何の意味もありません…"

# game/script.rpy:8166
translate japanese after_minigame_6_b8f027c1:

    toshu "No matter what someone says to you, you should not be ashamed of who you are or what you feel."
    toshu "誰かがあなたに何を言っても、あなたが誰であるか、あなたが感じることを恥じてはいけません。"

# game/script.rpy:8168
translate japanese after_minigame_6_f74b36da:

    toshu " Be proud of who you are."
    toshu "あなたが誰であるか自慢してください。"

# game/script.rpy:8170
translate japanese after_minigame_6_c9ef735c:

    toshu "There are many wonderful things about you."
    toshu "あなたには素晴らしいことがたくさんあります。"

# game/script.rpy:8171
translate japanese after_minigame_6_26bebdb7:

    toshu "Keep those in mind instead of those useless things you hear from those mean people."
    toshu "あなたがそれらの平均的な人々から聞くそれらの無駄なものの代わりにそれらを念頭に置いてください。"

# game/script.rpy:8173
translate japanese after_minigame_6_652b3511:

    toshu "Treasure those who really appreciate you!"
    toshu "あなたを本当に感謝する人たちを宝物！"

# game/script.rpy:8175
translate japanese after_minigame_6_53cd8411:

    sohma "Big Brother is always there for me…"
    sohma "ビッグブラザーはいつも私のためにそこにいます…"

# game/script.rpy:8177
translate japanese after_minigame_6_ae5faed4:

    sohma "I want to change… for Big Brother…"
    sohma "私はビッグブラザーのために…変えたい"

# game/script.rpy:8179
translate japanese after_minigame_6_c69b9a58:

    # toshu "That's the spirit, Sohma!"
    toshu "その意気だ、Sohmaくん！"

# game/script.rpy:8181
translate japanese after_minigame_6_25a6f6e3:

    toshu "See? You sound like a really strong man now!"
    toshu "見る？あなたは今本当に強い男のように聞こえる！"

# game/script.rpy:8183
translate japanese after_minigame_6_a311f2f4:

    sohma "Wahh~ Really?"
    sohma "ワー〜本当に？"

# game/script.rpy:8184
translate japanese after_minigame_6_c4fac3e1:

    sohma "Mr. Flaming Hair, thank you!"
    sohma "Mr. Flaming Hair、ありがとう！"

# game/script.rpy:8186
translate japanese after_minigame_6_64891395:

    toshu "Hngh! M-Mr. F-Flaming Hair?"
    toshu "Hngh！ Mさん。 F-炎の髪？"

# game/script.rpy:8188
translate japanese after_minigame_6_96d63611:

    ichiru "Wahaha! That nickname totally suits you!"
    ichiru "ワハハ！そのニックネームは完全にあなたに合っています！"

# game/script.rpy:8189
translate japanese after_minigame_6_979ab966:

    sohma "I will follow what you said!"
    sohma "私はあなたが言ったことに従います！"

# game/script.rpy:8190
translate japanese after_minigame_6_8f047548:

    sohma "Thank you very much!"
    sohma "どうもありがとうございました！"

# game/script.rpy:8192
translate japanese after_minigame_6_3d7c8cfe:

    toshu "I'm sorry about yelling at you!"
    toshu "私はあなたに叫んでついて申し訳ありません！"

# game/script.rpy:8194
translate japanese after_minigame_6_81fbba76:

    ichiru "At least, he looks super happy now!"
    ichiru "少なくとも、彼は今超幸せに見える！"

# game/script.rpy:8195
translate japanese after_minigame_6_2a0e373f:

    sohma "I-I really like you Mr. Flaming Hair!"
    sohma "私は本当にあなたがMr. Flaming Hairを気に入っています！"

# game/script.rpy:8197
translate japanese after_minigame_6_6804abd4:

    toshu "Hnn! Wh…wha!?"
    toshu "Hnn！あなた… 4人？"

# game/script.rpy:8199
translate japanese after_minigame_6_d5a28f6d:

    ichiru "HAHAHAHA!! Oh, you got him good, Toshu!"
    ichiru "ハハハッハッハ！！名前：ああ、あなたは彼を良い、東照！"

# game/script.rpy:8201
translate japanese after_minigame_6_e50fe3fb:

    # masaru "Hi guys!"
    masaru "よお、みんな！"

# game/script.rpy:8202
translate japanese after_minigame_6_a67cc9f5:

    # masaru "It looks pretty lively here!"
    masaru "何だか賑やかだな！"

# game/script.rpy:8204
translate japanese after_minigame_6_2438cf2b:

    # toshu "Oh, Welcome back, Masaru!"
    toshu "ああ、Masaruさんお帰りなさい！"

# game/script.rpy:8206
translate japanese after_minigame_6_e9f07ccf:

    sohma "Big Brother! You have such awesome friends!"
    sohma "ビッグブラザー！そんな素晴らしい友達がいる！"

# game/script.rpy:8208
translate japanese after_minigame_6_032a051f:

    masaru "E-eh? What's going on?"
    masaru "E-え？どうしたの？"

# game/script.rpy:8210
translate japanese after_minigame_6_c2cd82a6:

    sohma "Mr. Flaming Hair! Thank you very much! I feel so much better now!"
    sohma "Mr. Flaming Hair！どうもありがとうございました！今はずっといい気分だよ！"

# game/script.rpy:8211
translate japanese after_minigame_6_6dc5fecb:

    sohma "Big Brother! I will go back to my classroom now! I'm going to make friends!!!"
    sohma "ビッグブラザー！今私の教室に戻ってきます！私は友達を作るつもりです!!!"

# game/script.rpy:8213
translate japanese after_minigame_6_a72c59b4:

    masaru "O…okay?"
    masaru "O…okay?"

# game/script.rpy:8216
translate japanese after_minigame_6_9bb65ab5:

    masaru "Mr. Flaming Hair??? Hahaha!"
    masaru "Mr. Flaming Hair ???ははは！"

# game/script.rpy:8218
translate japanese after_minigame_6_ba6f668e:

    ichiru "I think your little brother has a crush on Toshu!"
    ichiru "私はあなたの弟が東照宮を圧倒していると思います！"

# game/script.rpy:8220
translate japanese after_minigame_6_b9ad43d0:

    toshu "H-he's not!"
    toshu "彼はいない！"

# game/script.rpy:8222
translate japanese after_minigame_6_2ef2eafb:

    masaru "What exactly happened here?"
    masaru "ここで何が起こったのですか？"

# game/script.rpy:8223
translate japanese after_minigame_6_fc5cee46:

    masaru "It's been a while since I saw him that lively."
    masaru "私は彼が活発に彼を見てからそれはしばらくしています。"

# game/script.rpy:8225
translate japanese after_minigame_6_e158ef24:

    toshu "I just gave him some advice on how to deal with his bullies."
    toshu "私はちょうど彼に彼のいじめに対処する方法のいくつかのアドバイスを与えた。"

# game/script.rpy:8227
translate japanese after_minigame_6_c86b7865:

    ichiru "Toshu does have the power to make people attached to him!"
    ichiru "東照宮には人々を繋ぐ力があります！"

# game/script.rpy:8229
translate japanese after_minigame_6_7b235d20:

    masaru "Sohma actually listened to your advice?"
    masaru "Sohmaは実際にあなたの助言を聞いた？"

# game/script.rpy:8231
translate japanese after_minigame_6_bba6cbdf:

    toshu "Well… he did cry…"
    toshu "まあ…彼は泣いた…"

# game/script.rpy:8233
translate japanese after_minigame_6_7254896c:

    masaru "Sounds like him… sorry about my brother… he can be really troublesome…"
    masaru "彼のような音…私の兄弟については残念です…彼は本当に面倒かもしれません…"

# game/script.rpy:8235
translate japanese after_minigame_6_73910d85:

    toshu "B-but! He took my words pretty well!"
    toshu "B-but！彼は私の言葉をかなりうまく取りました！"

# game/script.rpy:8236
translate japanese after_minigame_6_31d07aef:

    toshu "He said he wanted to change for real just for you!"
    toshu "彼はあなたのためだけに本物のために変えたいと言った！"

# game/script.rpy:8238
translate japanese after_minigame_6_79982ace:

    # masaru "R-really…?"
    masaru "ほ、本当か…？"

# game/script.rpy:8239
translate japanese after_minigame_6_49b0f52c:

    # masaru "Sohma said that…?"
    masaru "Sohmaがそんなことを…？"

# game/script.rpy:8241
translate japanese after_minigame_6_d10d86f2:

    toshu "Yes, he did! He really sound determined to change!"
    toshu "はい、彼はしました！彼は本当に変わると決心したよ！"

# game/script.rpy:8243
translate japanese after_minigame_6_622e256d:

    # masaru "Unbelievable!"
    masaru "信じられない！"

# game/script.rpy:8244
translate japanese after_minigame_6_dae51efc:

    masaru "You're the only person who was able to convince Sohma to change!"
    masaru "あなたはSohmaに変化を納得させることができた唯一の人です！"

# game/script.rpy:8245
translate japanese after_minigame_6_50eb35c6:

    masaru "I think you would really make a good father in the future!"
    masaru "私はあなたが本当に将来、良い父親を作るだろうと思う！"

# game/script.rpy:8247
translate japanese after_minigame_6_f637833b:

    toshu "W-whaaa…"
    toshu "インwhaaa …"

# game/script.rpy:8253
translate japanese after_minigame_6_08fd77c0:

    genji "Oh! There you guys are!"
    genji "ああ！あなたはそこにいる！"

# game/script.rpy:8255
translate japanese after_minigame_6_3230fd4e:

    genji "I was going to ask Masaru if he could relay to the players the news about the tournament."
    genji "私はMasaruに、トーナメントに関するニュースを選手たちに伝えることができるかどうか尋ねるつもりだった。"

# game/script.rpy:8257
translate japanese after_minigame_6_fbd319e3:

    genji "But since we're here, I might as well tell you guys."
    genji "しかし、私たちがここにいるので、皆さんにもお伝えします。"

# game/script.rpy:8259
translate japanese after_minigame_6_f413b1dc:

    ichiru "News?"
    ichiru "ニュース？"

# game/script.rpy:8261
translate japanese after_minigame_6_325b66aa:

    genji "Unfortunately, the tournament date was rescheduled…"
    genji "残念ながら、トーナメントの日程は変更されました…"

# game/script.rpy:8263
translate japanese after_minigame_6_bfa6e6eb:

    toshu "Wow! That's great news! We will have more time to practice!"
    toshu "うわー！それは素晴らしいニュースです！私たちは練習にもっと時間があります！"

# game/script.rpy:8265
translate japanese after_minigame_6_ceb4cfae:

    genji "No, Kanada, the tournament will be… THIS WEEK. Four days from now."
    genji "いいえ、カナダ、トーナメントは…今週です。今から4日。"

# game/script.rpy:8267
translate japanese after_minigame_6_07d15384:

    masaru "WHAT?! Seriously?!"
    masaru "何？！真剣に！"

# game/script.rpy:8269
translate japanese after_minigame_6_645f084d:

    genji "Yes… I was quite shocked of this news as well…"
    genji "はい…私はこのニュースにもかなりショックを受けました…"

# game/script.rpy:8271
translate japanese after_minigame_6_a9f9ddb4:

    masaru "Oh no… do you think we can win?"
    masaru "ああ…あなたが勝つことができると思いますか？"

# game/script.rpy:8272
translate japanese after_minigame_6_5418d775:

    masaru "Our practice session was cut by half!"
    masaru "私たちの練習セッションは半分になりました！"

# game/script.rpy:8274
translate japanese after_minigame_6_fd9c963c:

    genji "Yeah… that's what I was afraid of…"
    genji "ええ…それは私が恐れていたことです…"

# game/script.rpy:8276
translate japanese after_minigame_6_833715f6:

    ichiru "If that's really the case, we can't do anything about it other than train twice as hard!"
    ichiru "それが本当に事実なら、私たちは列車の2倍のこと以外は何もできません！"

# game/script.rpy:8278
translate japanese after_minigame_6_087d3ef8:

    masaru "You're right. There's no point on worrying about this now!"
    masaru "あなたが正しい。今これについて心配する必要はありません！"

# game/script.rpy:8280
translate japanese after_minigame_6_3d104339:

    masaru "I'm sure we can manage it somehow!"
    masaru "私は何とかそれを管理できると確信しています！"

# game/script.rpy:8282
translate japanese after_minigame_6_aca80fa3:

    toshu "Of course, we can! We are a team!"
    toshu "もちろん、我々はできます！私たちはチームです！"

# game/script.rpy:8284
translate japanese after_minigame_6_0119fbdb:

    genji "Let's give it all this week! We're gonna train nonstop!"
    genji "今週中にそれをすべて捧げましょう！私たちは直進するつもりです！"

# game/script.rpy:8285
translate japanese after_minigame_6_0c13f327:

    genji "Everyone! To the baseball field!"
    genji "みんな！野球場に！"

# game/script.rpy:8302
translate japanese after_minigame_7_bfdaa63b:

    toshu_t "We spent the week devotedly preparing for the tournament."
    toshu_t "我々は一週間を献身的にトーナメントの準備に費やした。"

# game/script.rpy:8303
translate japanese after_minigame_7_7c895440:

    toshu_t "I can't help to get the feeling…"
    toshu_t "私はその気持ちを手に入れることができません…"

# game/script.rpy:8304
translate japanese after_minigame_7_5dffa24a:

    toshu_t "I feel so afraid for some reason…"
    toshu_t "私は何らかの理由で恐れている…"

# game/script.rpy:8305
translate japanese after_minigame_7_72c03b5b:

    toshu_t "There's this heavy feeling in the air that just… won't go away."
    toshu_t "空気の中にこの重い気持ちがあります…ただ離れていないでしょう。"
