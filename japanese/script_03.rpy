# game/script.rpy:341
translate japanese start_d7948ebb:

    toshu_t "A week has passed since I have been introduced in the club."
    toshu_t "私はクラブに紹介されてから1週間が過ぎました。"

# game/script.rpy:342
translate japanese start_87d5227e:

    toshu_t "As Captain Masaru said, my first practice will start in about a week."
    toshu_t "Captain Masaruが言ったように、私の最初の練習は約1週間で始まります。"

# game/script.rpy:343
translate japanese start_247df5da:

    toshu_t "And today is that day!"
    toshu_t "今日はその日です！"

# game/script.rpy:344
translate japanese start_1a41dfca:

    # toshu_t "I can't wait for my first practice!"
    toshu_t "初練習、待ち切れないよ～！"

# game/script.rpy:352
translate japanese start_37d65bb1:

    toshu "Fuwahh! I can't wait to start my training session after today's class!"
    toshu "フワフ！今日の授業の後、私のトレーニングセッションを開始するのを待つことができません！"

# game/script.rpy:353
translate japanese start_2029e128:

    toshu "Everyone in the baseball club seem so nice~"
    toshu "野球部のみんながとてもいいよ〜"

# game/script.rpy:355
translate japanese start_992d537c:

    toshu "Especially, Captain Masaru! He's so cool!!"
    toshu "特に、大将大尉！彼はとても涼しい!!"

# game/script.rpy:357
translate japanese start_e1779c9a:

    toshu "And Ichiru too! He's very friendly! And very funny!"
    toshu "イチールも！彼はとてもフレンドリーです！そしてとても面白い！"

# game/script.rpy:358
translate japanese start_853aa5b3:

    narrator "*Someone pinches Toshu's cheek*"
    narrator "*誰かがToshuの頬をつまむ*"

# game/script.rpy:360
translate japanese start_b144c9bb:

    toshu "O-ow that hurts!"
    toshu "お義母は痛い！"

# game/script.rpy:363
translate japanese start_b84b6e25:

    ichiru "Who are you calling funny?"
    ichiru "あなたは誰を面白いと呼んでいますか"

# game/script.rpy:364
translate japanese start_2efe9b82:

    toshu "Ahh! Ichiru, stop pinching my cheeks!"
    toshu "ああ！イチル、私の頬をつまんで停止！"

# game/script.rpy:365
translate japanese start_d77c7898:

    ichiru "What are you daydreaming so early in the morning, Tofu?"
    ichiru "Tofu、朝早くも空腹ですか？"

# game/script.rpy:367
translate japanese start_ebfcb949:

    toshu "Oww… it's TOSHU!"
    toshu "ああ…それは東武です！"

# game/script.rpy:369
translate japanese start_66fb6351:

    toshu "And I'm about to go to class. How about you, Ichiru?"
    toshu "私はクラスに行くつもりです。あなたはどうですか、Ichiru？"

# game/script.rpy:371
translate japanese start_3ecc9845:

    ichiru "Class is boring!"
    ichiru "クラスは退屈です！"

# game/script.rpy:372
translate japanese start_55e6e0dd:

    toshu "Don't tell me you're going to skip class again?"
    toshu "授業をやり直すつもりはありませんか？"

# game/script.rpy:374
translate japanese start_b38f6816:

    ichiru "Well… since you're here I guess it won't hurt to attend at least this once."
    ichiru "まあ…あなたがここにいるので、少なくともこの一度は出席することに傷つくことはないと思います。"

# game/script.rpy:376
translate japanese start_8b3a5ca1:

    ichiru "Isn't it awesome that my teammate and best friend is my classmate too?!"
    ichiru "私のチームメイトと親友が私のクラスメートでもないのはすごいですか？"

# game/script.rpy:378
translate japanese start_aebba93c:

    toshu "B-best friend?"
    toshu "B-親友ですか？"

# game/script.rpy:381
translate japanese start_3ff6ff2f:

    ichiru "Let's go, you don't want to be late right, Tofu?"
    ichiru "行こう、遅刻したくない、Tofu？"

# game/script.rpy:389
translate japanese start_8b73818d:

    saki "Well, well, well, look who decided to show up after being gone for a week."
    saki "まあ、よく、よく、一週間行った後に現れることにした人を見てください。"

# game/script.rpy:391
translate japanese start_48274dc2:

    ichiru "Hahaha!! It can't be helped lady, I was sick!"
    ichiru "ははは！！助けられない女性、私は病気だった！"

# game/script.rpy:393
translate japanese start_8d826be2:

    saki "Lady?!"
    saki "レディ？！"

# game/script.rpy:394
translate japanese start_800dd9df:

    saki "That's rude!"
    saki "それは失礼です！"

# game/script.rpy:396
translate japanese start_e0dce4ec:

    toshu "But Ichiru you were--"
    toshu "しかし、あなたはイチールでした -"

# game/script.rpy:398
translate japanese start_ce25c3b3:

    ichiru "SHHHHHH!!"
    ichiru "SHHHHHH !!"

# game/script.rpy:400
translate japanese start_43c3895e:

    saki "*sigh* What got into that noggin' of yours and you've decided to go to class today?"
    saki "* sigh *あなたのことは何も分からず、今日あなたはクラスに行くことに決めましたか？"

# game/script.rpy:402
translate japanese start_2a715562:

    ichiru "Haha~! It's because Tofu is my classmate~"
    ichiru "ハハ〜！Tofuは私のクラスメートなので、"

# game/script.rpy:404
translate japanese start_cc22ccc0:

    saki "Tofu…?"
    saki "Tofu…？"

# game/script.rpy:405
translate japanese start_1883b0c0:

    saki "Who is Tofu?"
    saki "Tofuは誰ですか？"

# game/script.rpy:407
translate japanese start_fc33baa7:

    toshu "He meant Toshu, Ms. Saki…"
    toshu "He meant Toshu, Ms. Saki…"

# game/script.rpy:409
translate japanese start_4f09be91:

    ichiru "Tofu is one of our teammate in the baseball team!"
    ichiru "Tofuは野球チームのチームメイトの一人です！"

# game/script.rpy:410
translate japanese start_4f0765f0:

    ichiru "I have to accompany him since he's new!"
    ichiru "私は彼が新しいので、彼に同行しなければならない！"

# game/script.rpy:411
translate japanese start_ebb48c46:

    ichiru "Also, Tofu's my best friend!!"
    ichiru "また、Tofuの私の親友!!"

# game/script.rpy:413
translate japanese start_ee6d1470:

    toshu "Ahh Ichiru…"
    toshu "Ahh Ichiru…"

# game/script.rpy:415
translate japanese start_51428f1b:

    saki "It's nice to see that Mr. Kanada has been a good influence to you, Mr. Yanai."
    saki "金田さんがあなたに良い影響を与えてくれたことは素晴らしいことです。柳井さん。"

# game/script.rpy:416
translate japanese start_1e8f696a:

    saki "Mr. Kanada, please make sure he attends class daily okay?"
    saki "金田さん、毎日授業に出席してください。"

# game/script.rpy:417
translate japanese start_7e0e5ec4:

    saki "Also it's good that you made some friends as well."
    saki "また、友達を作ってもいいですね。"

# game/script.rpy:418
translate japanese start_b64bb72f:

    saki "I almost never see you get along with anyone here."
    saki "私はあなたがここに誰と一緒に会うのをほとんど見ない。"

# game/script.rpy:420
translate japanese start_a1bca4f7:

    ichiru "It's… none of your business…"
    ichiru "それは…あなたのビジネスのどれも…"

# game/script.rpy:422
translate japanese start_a10a9ffd:

    saki "*sigh* He never changes…"
    saki "*彼は決して変わらない"

# game/script.rpy:424
translate japanese start_174f98e5:

    toshu_t "Ichiru…"
    toshu_t "イチール…"

# game/script.rpy:426
translate japanese start_850f8cab:

    saki "Now take your seats, we'll start with the lesson."
    saki "座席に座って、レッスンを始めましょう。"

# game/script.rpy:428
translate japanese start_4cc127db:

    toshu_t "I didn't know what Ms. Saki meant about Ichiru not having any friends."
    toshu_t "私は咲さんが友達を持たないイチルについて何を意味するのか分かりませんでした。"

# game/script.rpy:429
translate japanese start_cd7e82a9:

    toshu_t "Maybe others don't know how to exactly deal with him."
    toshu_t "たぶん、他の人は彼とどうやって正確に対処するのか分かりません。"

# game/script.rpy:430
translate japanese start_0cd62288:

    toshu_t "Good things, it seems that I'm getting along with Ichiru just fine."
    toshu_t "良いこと、それはちょうどいいIchiruと一緒になっているようです。"

# game/script.rpy:431
translate japanese start_3bd33b47:

    toshu_t "And I'm really glad about that!"
    toshu_t "そして、私は本当にそれについて嬉しいです！"

# game/script.rpy:435
translate japanese start_23fe7b6e:

    saki "That concludes our lesson for today."
    saki "以上で今日のレッスンは終わりです。"

# game/script.rpy:436
translate japanese start_a4fdbb08:

    saki "Class dismissed."
    saki "クラスは却下されました。"

# game/script.rpy:438
translate japanese start_a6869dee:

    saki "Ichiru, go to class from now on okay?"
    saki "Ichiru、今からクラスに行くのは大丈夫ですか？"

# game/script.rpy:445
translate japanese start_2bb7612e:

    ichiru "I TOLD YOU CLASS IS BORING! How can you pay attention for so many hours Tofu?!"
    ichiru "私はあなたにクラスを賞賛しています！あなたは何時間もTofuに注意を払うことができますか？"

# game/script.rpy:447
translate japanese start_35c591c1:

    toshu "But you slept it all away…"
    toshu "しかし、あなたはそれをすべて眠った…"

# game/script.rpy:448
translate japanese start_6659874b:

    toshu "Also I can't believe you have been absent for almost 1 week."
    toshu "また、あなたが一週間近く不在だったとは信じられません。"

# game/script.rpy:450
translate japanese start_d8aadb28:

    toshu "How come Ms. Saki just let you off like that?"
    toshu "咲ちゃんはどうしてそんなことをしてくれるの？"

# game/script.rpy:452
translate japanese start_275e53c7:

    ichiru "It's because I'm awesome like that, hehe!"
    ichiru "それは私がそれのような素晴らしい、heheだからです！"

# game/script.rpy:454
translate japanese start_85ca07b9:

    toshu "Ichiru I'm very serious! Please don't neglect your studies…"
    toshu "Ichiruとても深刻です！あなたの研究を無視しないでください…"

# game/script.rpy:456
translate japanese start_32dc16f2:

    ichiru "I wouldn't worry about my studies. There's really no sense into it!"
    ichiru "私は私の研究について心配しません。それには意味がありません！"

# game/script.rpy:458
translate japanese start_83f332de:

    toshu "Errr… but--"
    toshu "Errr …しかし -"

# game/script.rpy:459
translate japanese start_5abf9b11:

    ichiru "No more questions! Let's attend today's baseball practice!"
    ichiru "もう質問はありません！今日の野球の練習に参加しよう！"

# game/script.rpy:460
translate japanese start_13da3a5d:

    toshu "Okay…"
    toshu "はい…"

# game/script.rpy:468
translate japanese start_8932a62c:

    narrator "*The two saw Masaru walking towards the field as well*"
    narrator "* 2人はまさるがフィールドに向かって歩くのを見た*"

# game/script.rpy:470
translate japanese start_ea3a2d08:

    ichiru "Masaru!!!"
    ichiru "Masaru!!!"

# game/script.rpy:472
translate japanese start_44fb745d:

    masaru "Ohh, hello."
    masaru "ああ、こんにちは。"

# game/script.rpy:473
translate japanese start_a3438dda:

    masaru "How was class?"
    masaru "授業はどうでしたか？"

# game/script.rpy:474
translate japanese start_13611212:

    masaru "I assumed that Ichiru finally went to class since both of you came together."
    masaru "私はあなたが一緒に来たので、最終的にIchiruが授業に行くと思った。"

# game/script.rpy:476
translate japanese start_64d0679d:

    ichiru "I did! And it was BORING AS HELL."
    ichiru "やった！そして、それは地獄のようでした。"

# game/script.rpy:478
translate japanese start_9fb5f84c:

    ichiru "Good thing Tofu was there to accompany me."
    ichiru "良いことにTofuは私に同行していた。"

# game/script.rpy:480
translate japanese start_21da5458:

    masaru "Now, now, don't underestimate going to class."
    masaru "今、今、クラスに行くことを過小評価しないでください。"

# game/script.rpy:481
translate japanese start_11c9baf9:

    masaru "It's great that you finally shown interest in attending classes. Looks like Toshu is a good influence to you."
    masaru "あなたが最終的にクラスに参加することに関心を示したことは素晴らしいことです。 Toshuはあなたに良い影響を与えるように見えます。"

# game/script.rpy:484
translate japanese start_773e906e:

    masaru "By the way, Coach contacted me saying that he's gonna meet us today."
    masaru "ところで、コーチは私に連絡して、彼が今日私たちと出会うと言った。"

# game/script.rpy:485
translate japanese start_e2b5b265:

    masaru "But he hasn't arrived yet, so we should probably get changed to our uniforms first."
    masaru "しかし、彼はまだ到着していないので、おそらく私たちはおそらく私たちの制服に変更する必要があります。"

# game/script.rpy:486
translate japanese start_74fdf2cb:

    masaru "I will go on ahead to the locker room."
    masaru "私はロッカールームに行くつもりです。"

# game/script.rpy:487
translate japanese start_f0e46316:

    masaru "Meet me there whenever you are ready."
    masaru "あなたが準備ができているときは、いつでも私に会いましょう。"

# game/script.rpy:490
translate japanese start_be1de279:

    ichiru "I guess we better change uniforms as well."
    ichiru "私は、ユニフォームも変えたいと思う。"

# game/script.rpy:491
translate japanese start_bea318a3:

    ichiru "Good thing I brought my uniform today! C'mon Tofu!!"
    ichiru "今日は私のユニフォームを持ってきました。さっそくTofu!!"

# game/script.rpy:493
translate japanese start_86bc222b:

    toshu "Eh? We need to provide our own uniform??"
    toshu "え？私たちは自分のユニフォームを提供する必要がありますか？"

# game/script.rpy:494
translate japanese start_111d0bc6:

    ichiru "Why of course!"
    ichiru "なぜ、もちろん！"

# game/script.rpy:495
translate japanese start_89f181b3:

    narrator "*Ichiru drags Toshu towards the locker room*"
    narrator "*イチールはToshuをロッカールームに引きずります*"

# game/script.rpy:497
translate japanese start_b8602d02:

    toshu "Whaa!! Wait! I don't ha--"
    toshu "わぁ!!待つ！私はいない -"

# game/script.rpy:504
translate japanese start_c21b61c2:

    toshu_t "Oh no… what should I do…?"
    toshu_t "ああ、…私は何をすべきか…？"

# game/script.rpy:505
translate japanese start_e1dc2c39:

    toshu_t "I didn't know that we have to provide uniform for ourselves."
    toshu_t "私は自分自身のために制服を用意しなければならないことを知らなかった。"

# game/script.rpy:506
translate japanese start_8e5a3c37:

    toshu_t "I don't have one yet…!"
    toshu_t "私はまだ持っていない…！"

# game/script.rpy:508
translate japanese start_0ef8325d:

    ichiru "What's wrong Tofu??"
    ichiru "何が間違っている？"

# game/script.rpy:510
translate japanese start_354b7dcd:

    toshu "Ermm… I don't have a baseball uniform."
    toshu "私は野球のユニフォームを持っていません。"

# game/script.rpy:512
translate japanese start_46511939:

    ichiru "What?? Didn't Masaru tell you before?"
    ichiru "何？？マサルが前に伝えなかったの？"

# game/script.rpy:514
translate japanese start_bdee6324:

    toshu "No… I don't remember Captain telling me to bring one."
    toshu "いいえ…私はキャプテンが私に持って来るように言っていることを覚えていません。"

# game/script.rpy:515
translate japanese start_2873f89b:

    ichiru "Oh I see…"
    ichiru "ああなるほど…"

# game/script.rpy:517
translate japanese start_7039e9ee:

    ichiru "But that's okay! The great Ichiru is here to the rescue~!"
    ichiru "しかしそれは大丈夫です！偉大なイチールがここに救助〜！"

# game/script.rpy:519
translate japanese start_7c02b5bc:

    ichiru "I have a spare here. It's my old one so it's a bit smaller, but I think it will fit you perfectly!"
    ichiru "私はここに余裕がある。それは私の古いものなので少し小さくなりますが、私はそれがあなたに完全に合うと思います！"

# game/script.rpy:521
translate japanese start_812289b4:

    masaru "Anything wrong here?"
    masaru "ここに何か間違っている？"

# game/script.rpy:524
translate japanese start_5067fac1:

    ichiru "Tofu didn't bring any uniform! Haha!"
    ichiru "Tofuは制服を持って来なかった！ハハ！"

# game/script.rpy:526
translate japanese start_1e76faeb:

    toshu "Ichiru, it's TOSHU!!"
    toshu "Ichiru, it's TOSHU!!"

# game/script.rpy:528
translate japanese start_8a60ba19:

    toshu "How many times do I have to tell you it's Tofu?!"
    toshu "何回Tofuって言ってもいい？"

# game/script.rpy:530
translate japanese start_1a82a5f8:

    toshu "I-I mean, TOSHU!!!"
    toshu "I-I mean, TOSHU!!!"

# game/script.rpy:532
translate japanese start_d22a24c3:

    ichiru "HAHAHAHA!!"
    ichiru "ハハハハ!!"

# game/script.rpy:533
translate japanese start_f2cc7580:

    ichiru "See? You even claimed it yourself!"
    ichiru "見る？あなたはあなた自身でそれを主張しました！"

# game/script.rpy:535
translate japanese start_ed5987f9:

    toshu "Waaaaah…"
    toshu "ワアアア…"

# game/script.rpy:537
translate japanese start_06e304d5:

    masaru "That's weird. I remember sending you a text message last week about it!"
    masaru "それは変だ。私はそれについて先週あなたにテキストメッセージを送ったことを覚えています！"

# game/script.rpy:539
translate japanese start_f2b6ec87:

    toshu "Captain… I don't have a cellphone…"
    toshu "キャプテン…私は携帯電話を持っていません…"

# game/script.rpy:541
translate japanese start_fa886dc7:

    masaru "Oh… I wonder who I sent that message to…?"
    masaru "ああ…私はそのメッセージを誰に送ったのだろうか？"

# game/script.rpy:542
translate japanese start_948f9a53:

    ichiru "Pffft! What's wrong with you Masaru haha!!"
    ichiru "パフフト！あなたは何が間違っている？"

# game/script.rpy:544
translate japanese start_ca32e96f:

    masaru "Oh well, I have a spare uniform here too. You can borrow it if you want, Toshu."
    masaru "ああ、私はここでもスペアのユニフォームを持っています。もしあなたが望めばそれを借りることができます、Toshu。"

# game/script.rpy:568
translate japanese choice_1_A_04f85d2d:

    toshu "It's okay Captain, I'll borrow Ichiru's."
    toshu "それは大丈夫ですキャプテン、私はIchiruを借りるでしょう。"

# game/script.rpy:570
translate japanese choice_1_A_1e82ecab:

    ichiru "Sorry Masaru! I asked him first, hehe!"
    ichiru "申し訳ありませんマサル！私は彼に最初に尋ねた、hehe！"

# game/script.rpy:572
translate japanese choice_1_A_f1daae60:

    masaru "Oh! Okay!"
    masaru "ああ！はい！"

# game/script.rpy:578
translate japanese choice_1_B_9ff11c46:

    toshu "Okay, Captain!"
    toshu "さて、キャプテン！"

# game/script.rpy:579
translate japanese choice_1_B_ff17eab4:

    toshu "I'll borrow yours."
    toshu "私はあなたのものを借ります。"

# game/script.rpy:581
translate japanese choice_1_B_ad9c1abe:

    ichiru "HEYYY!! Not faiirr!! I asked him first!"
    ichiru "HEYYY !!ファイヤー!!私は彼に最初に尋ねた！"

# game/script.rpy:583
translate japanese choice_1_B_1d9e6508:

    masaru "Well, he wants to borrow mine…"
    masaru "まあ、彼は私のものを借りたい…"

# game/script.rpy:585
translate japanese choice_1_B_d444e9e5:

    ichiru "Hmph!"
    ichiru "Hmph！"

# game/script.rpy:587
translate japanese choice_1_B_165ac135:

    masaru "Okay then, gear up! Let's meet outside when you are ready."
    masaru "さて、ギアアップ！準備ができたら外で出会いましょう。"

# game/script.rpy:588
translate japanese choice_1_B_7bf82da9:

    masaru "Coach is on his way here."
    masaru "コーチがここに来ている。"

# game/script.rpy:593
translate japanese choice_1_C_30dff728:

    # toshu "Can I play using my school uniform?"
    toshu "学校の制服で練習してもいいですか？"

# game/script.rpy:595
translate japanese choice_1_C_1c986fdd:

    # masaru "Seriously? You can't play with your school uniform, it's dangerous and it will get all dirty."
    masaru "本気かい？ 制服で練習は無理だよ。危ないし汚れちゃうよ。"

# game/script.rpy:597
translate japanese choice_1_C_3ebdb985:

    # ichiru "Tofuuu, I have a spare here, you can have it."
    ichiru "Tofuuu～、俺もう一着持ってるから貸してやるよ。"

# game/script.rpy:599
translate japanese choice_1_C_65ff3806:

    # toshu "Ohh! Right! I'll just borrow yours then."
    toshu "ああよかった！ じゃあ借りるね。"

# game/script.rpy:601
translate japanese choice_1_C_165ac135:

    # masaru "Okay then, gear up! Let's meet outside when you are ready."
    masaru "OK、それじゃ着替えて！ 準備ができたら外で集合だよ。"

# game/script.rpy:602
translate japanese choice_1_C_7bf82da9:

    # masaru "Coach is on his way here."
    masaru "コーチが向かってるからね。"

# game/script.rpy:606
translate japanese choice_1_D_8e06a947:

    # toshu "Can I play on the field naked?"
    toshu "裸で練習してもいいですか？"

# game/script.rpy:608
translate japanese choice_1_D_1f31b6e6:

    # ichiru "Me too! Me too!"
    ichiru "俺も！ 俺も！"

# game/script.rpy:610
translate japanese choice_1_D_701d55f3:

    # masaru "No way, you two!!!"
    masaru "いいわけないだろ、二人とも！！！"

# game/script.rpy:612
translate japanese choice_1_D_8f769708:

    # masaru "Here Toshu, you can have mine."
    masaru "はいToshu、俺のを貸してやるよ。"

# game/script.rpy:614
translate japanese choice_1_D_50117e98:

    # ichiru "Gosh, Masaru. Can't tell the difference between a joke and not?"
    ichiru "おいおい、Masaru。本気にすんなよ。"

# game/script.rpy:616
translate japanese choice_1_D_e47a3ca9:

    # masaru "Ah. Just get changed, and let's meet outside when you're ready."
    masaru "早く着替えろ、準備ができたら外で集合な。"

# game/script.rpy:617
translate japanese choice_1_D_7bf82da9:

    masaru "Coach is on his way here."
    masaru "コーチが向かってるからね。"

# game/script.rpy:628
translate japanese choice_1_end_222a7d4c:

    # random "Team assemble!!"
    random "全員集合！"

# game/script.rpy:630
translate japanese choice_1_end_efc4e9c7:

    # masaru "Yes, Coach!"
    masaru "はい、コーチ！"

# game/script.rpy:632
translate japanese choice_1_end_3d8685c5:

    ichiru "Fffft…"
    ichiru "Fffft …"

# game/script.rpy:633
translate japanese choice_1_end_f9a3d9ea:

    # random "I heard that there is a new recruit? Where is he?!"
    random "新入部員がいると聞いたが、どこにいる？！"

# game/script.rpy:634
translate japanese choice_1_end_1053d258:

    # toshu "I'm here! I'm here!"
    toshu "はいはい！ ここです！"

# game/script.rpy:637
translate japanese choice_1_end_fad2fa34:

    # random "Late on the first assembly? I don't want slowpokes in my team!!"
    random "初回の練習から遅刻か？ 俺のチームにのろまはいらんぞ！"

# game/script.rpy:638
translate japanese choice_1_end_5b82fc1d:

    # random "You're out! Scram!!"
    random "お前はいらん！ 出ていけ！"

# game/script.rpy:641
translate japanese choice_1_end_7fa8bb7e:

    random "The team would be better off without you."
    random "チームはあなたなしでより良いでしょう。"

# game/script.rpy:642
translate japanese choice_1_end_e83e2d63:

    random "You useless piece of--"
    random "あなたの役に立たない部分 -"

# game/script.rpy:644
translate japanese choice_1_end_c0c56fe8:

    # toshu "Nnnghh… *sniff*"
    toshu "ううう…(グスン)"

# game/script.rpy:646
translate japanese choice_1_end_dc3d5370:

    masaru "Cooaachh! Don't bully our new recruit…! You know how desperate this club is for new members…"
    masaru "Cooaachh！新しい人材をいじめられないで…！このクラブが新しいメンバーのためにいかに絶望的であるか知っています…"

# game/script.rpy:648
translate japanese choice_1_end_cab139d9:

    masaru "If he doesn't join us, you're going to regret it."
    masaru "もし彼が私たちに加わらなければ、あなたはそれを後悔するつもりです。"

# game/script.rpy:650
translate japanese choice_1_end_62c92ed3:

    ichiru "Hahaha!! GAYJI cut it out! Hahaha!"
    ichiru "ははは！！ゲイジはそれを切った！ははは！"

# game/script.rpy:653
translate japanese choice_1_end_b63e183c:

    ichiru "Tofu! That RETARDED Coach GAYJI is just acting tough!"
    ichiru "Tofu！あのリタイアド・コーチ・ゲイジはちょうどタフな演技だ！"

# game/script.rpy:655
translate japanese choice_1_end_d29b0eed:

    toshu "A-act…?"
    toshu "A-act …？"

# game/script.rpy:657
translate japanese choice_1_end_e78e8fa3:

    genji "That's COACH GENJI you little prick!"
    genji "それはコーチ・ジェンキーなんですか？"

# game/script.rpy:659
translate japanese choice_1_end_8a63a360:

    ichiru "Blehh!"
    ichiru "ブリー！"

# game/script.rpy:661
translate japanese choice_1_end_f620f638:

    genji "Give me a break Ichiru, I rarely get chances to show off."
    genji "私に一休みを与える、私はめったに誇示する機会を得ることはない。"

# game/script.rpy:662
translate japanese choice_1_end_b2a46f67:

    ichiru "Lame! Always picking on the new ones!"
    ichiru "ラメ！常に新しいものを選ぶ！"

# game/script.rpy:664
translate japanese choice_1_end_f4f1860d:

    toshu "…so I'm not out?"
    toshu "…私は外に出ていないのですか？"

# game/script.rpy:666
translate japanese choice_1_end_a69c4553:

    masaru "Of course not. You are a valuable new member."
    masaru "もちろん違います。あなたは貴重な新メンバーです。"

# game/script.rpy:667
translate japanese choice_1_end_f6c1661c:

    masaru "And we desperately need one."
    masaru "そして私たちは必死にそれを必要とします。"

# game/script.rpy:669
translate japanese choice_1_end_e52251be:

    # genji "Your name is Toshu Kanada right?"
    genji "お前の名前はToshu Kanadaだな？"

# game/script.rpy:671
translate japanese choice_1_end_627d5804:

    # toshu "Uhh… y-yes…"
    toshu "あ…は、はい…"

# game/script.rpy:673
translate japanese choice_1_end_680e0e3f:

    # genji "I CAN'T HEAR YOU!!!"
    genji "聞こえんぞ！！！"

# game/script.rpy:675
translate japanese choice_1_end_c4878f68:

    # toshu "Wahh!! YES COACH I AM TOSHU KANADA!"
    toshu "わわっ！！ はいコーチ、僕の名前はTOSHU KANADAです！"

# game/script.rpy:677
translate japanese choice_1_end_da5b3d73:

    # masaru "COACH!!! Stop it!"
    masaru "コーチ！！！ やめて下さい！"

# game/script.rpy:679
translate japanese choice_1_end_75a87496:

    # genji "Geez… why are you guys so being protective to him?"
    genji "もう…お前らなんでアイツの味方ばっかすんだよ？"

# game/script.rpy:681
translate japanese choice_1_end_19bb5767:

    # genji "Anyway, Kanada right?"
    genji "とにかく、いいかKanada？"

# game/script.rpy:683
translate japanese choice_1_end_32e154ae:

    # genji "My name is Genji Tadano. And I will be your baseball coach from now on."
    genji "私の名前はGenji Tadanoだ。今日から君の野球のコーチだ。"

# game/script.rpy:685
translate japanese choice_1_end_b6dee703:

    # genji "Welcome to the team!! HAHAHAHA!!!"
    genji "わがチームへようこそ！！ ハッハッハ！！！"

# game/script.rpy:687
translate japanese choice_1_end_26681064:

    # masaru "Sorry 'bout the Coach, Toshu, he's really like that."
    masaru "こんなコーチでゴメンな、Toshu。コーチはあんな人なんだ。"

# game/script.rpy:689
translate japanese choice_1_end_fd32af7c:

    # toshu "Ahhhh…"
    toshu "はあ…"

# game/script.rpy:691
translate japanese choice_1_end_eeebb112:

    # ichiru "Yeah, he is a good-for-nothing old geezer!"
    ichiru "そうだぜ、アイツはなんの取り柄もないアホなオッサンだ！"

# game/script.rpy:693
translate japanese choice_1_end_d82ba363:

    ichiru "Not to mention that he always brag about despite being a no-skilled talentless ha--"
    ichiru "言うまでもなく、彼はいつも熟練した才能のない人でもあるが、"

# game/script.rpy:695
translate japanese choice_1_end_8a06a319:

    # genji "So, Kanada, What is your preferred position?"
    genji "ところでKanada、希望のポジションはあるか？"

# game/script.rpy:697
translate japanese choice_1_end_880c7d23:

    # ichiru "Heeeyy! Don't interrupt me!"
    ichiru "おいおい！ 話の邪魔すんじゃねーよ！"

# game/script.rpy:699
translate japanese choice_1_end_d796bede:

    # toshu "Uhh… I was a pitcher back then, but I don't mind any position assigned to me."
    toshu "えーと…前は投手をやってましたが、どのポジションでも大丈夫です。"

# game/script.rpy:701
translate japanese choice_1_end_fc123bf4:

    # genji "Experienced in pitching eh?"
    genji "投手経験があるのか？"

# game/script.rpy:703
translate japanese choice_1_end_74c9b13f:

    # genji "Perfect timing! We really need a someone who can pitch…"
    genji "ちょうど良かった！ 投手ができるヤツが必要だったんだ…"

# game/script.rpy:705
translate japanese choice_1_end_d1cb8880:

    # genji "Our last pitcher, well… let's just say it didn't end well."
    genji "前のピッチャーは、まあ何というか…上手く行かなかったんだな。"

# game/script.rpy:706
translate japanese choice_1_end_d05d4c5c:

    # genji "He had an attitude problem and he disregarded everyone's advice for him. *sigh*"
    genji "アイツは素行に問題があって、誰のアドバイスも聞かなかったんだ。 (フゥ)"

# game/script.rpy:707
translate japanese choice_1_end_08c844fa:

    # genji "He didn't get along with everyone especially with Masaru, his catcher. That kid was so stubborn that he quit a week after joining."
    genji "彼は特に彼のキャッチャーであるMasaruと一緒にみんなとは付き合っていませんでした。その子供はとても頑固で、入社して1週間後に辞めました。"

# game/script.rpy:709
translate japanese choice_1_end_6a0ca113:

    genji "I HAVE TO MAKE SURE THAT YOU ARE NOT AN INSENSITIVE, UNGRATEFUL, BACKSTABBING QUITTER LIKE THOSE IDIOTS!!!"
    genji "私はあなたが恥ずかしがり屋のような、恥ずべきではない、元気なクスターではないことを確かめなければならない！"

# game/script.rpy:710
translate japanese choice_1_end_d241b385:

    # genji "You got that??!!"
    genji "分かるな？？！！"

# game/script.rpy:712
translate japanese choice_1_end_f874ad6a:

    # toshu "Y-Yes COACH!"
    toshu "は、はいコーチ！"

# game/script.rpy:714
translate japanese choice_1_end_61f6ea7b:

    # genji "Alright Toshu, show us what you can do!"
    genji "よし、お前の投球を見せてみろ！"

# game/script.rpy:716
translate japanese choice_1_end_bf26452d:

    # toshu "R-right now?"
    toshu "い、今ここで？"

# game/script.rpy:718
translate japanese choice_1_end_cb07f388:

    # masaru "Yeah, I'll catch it for you!"
    masaru "ああ、俺がキャッチャーやるよ！"

# game/script.rpy:720
translate japanese choice_1_end_1304d0db:

    # ichiru "AWESOME!!!"
    ichiru "スッゲェ！！！"

# game/script.rpy:722
translate japanese choice_1_end_5aa7eb8b:

    # masaru "Woahh!!"
    masaru "うわー！！"

# game/script.rpy:731
translate japanese choice_1_end_c3c09808:

    # toshu "I haven't played for a long time…"
    toshu "しばらく投げてなかったから…"

# game/script.rpy:733
translate japanese choice_1_end_734d0141:

    # toshu "I think my throw got rusty."
    toshu "肩がなまってる気がします。"

# game/script.rpy:735
translate japanese choice_1_end_e5850f2d:

    # genji "Hmm, surely you have the potential but…"
    genji "ふむ、確かに才能はあるようだが…"

# game/script.rpy:736
translate japanese choice_1_end_72313ff6:

    genji "You need more practice to improve your throw, add more velocity and accuracy, I'm not sure if you really are fit to join our club."
    genji "投球フォームを改善し、球速を上げてコントロールを良くするにはもっと練習が必要だな。私たちが本当にクラブに加わるのが適切かどうかはわかりません。"

# game/script.rpy:738
translate japanese choice_1_end_383dcfe7:

    # toshu "I hope I am qualified to be a member…"
    toshu "メンバーになれるといいんですが…"

# game/script.rpy:740
translate japanese choice_1_end_1e486c44:

    # masaru "Coach! Stop bullying him!"
    masaru "コーチ！ 意地悪言うの止めて下さい！"

# game/script.rpy:743
translate japanese choice_1_end_59a6c2a0:

    ichiru "Oh come on, GAYJI! We all saw how skilled he is with that single forkball pitch!"
    ichiru "ああ、やあ！私たちは皆、その単一のフォークピッチで彼がどれほど熟練しているかを見ました！"

# game/script.rpy:745
translate japanese choice_1_end_a0443cb8:

    masaru "Yeah Coach, it's been a while since someone talented like him joined our club."
    masaru "ええコーチ、それは彼のような才能のある誰かが私たちのクラブに加わって以来、しばらくしています。"

# game/script.rpy:747
translate japanese choice_1_end_003019e1:

    genji "Cut me some slack here! I'm trying to be cool!"
    genji "私はここにいくつかのたるみをカット！私はクールにしようとしている！"

# game/script.rpy:749
translate japanese choice_1_end_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:750
translate japanese choice_1_end_0640078c:

    # narrator "*Genji pats Toshu's back*"
    narrator "(GenjiがToshuの背中を叩く)"

# game/script.rpy:752
translate japanese choice_1_end_e3a0e8f1:

    # genji "HAHAHA!! I'm just kidding! You're pretty damn good!"
    genji "ハハハ！！ 冗談だよ！ 君はかなりいいぞ！"

# game/script.rpy:754
translate japanese choice_1_end_633c49ec:

    genji "You're exactly what we need, since I already signed up our team in the baseball tournament this coming season!"
    genji "今シーズンの野球大会で私たちのチームにすでにサインアップしているので、あなたはまさに私たちが必要としているものです！"

# game/script.rpy:755
translate japanese choice_1_end_223f3df6:

    genji "So I'm expecting you guys to do your best and train with all you got!"
    genji "だから私はあなたたちがあなたのベストを尽くし、あなたが持っているものすべてで訓練することを期待しています！"

# game/script.rpy:757
translate japanese choice_1_end_61f4cb8e:

    genji "Toshu here will be our trump card. He has exceptional skills that can help us win."
    genji "Toshuはここで私たちのトランプカードになります。彼は勝つのに役立つ素晴らしいスキルを持っています。"

# game/script.rpy:759
translate japanese choice_1_end_3090b66a:

    # genji "However, as I said a while ago…"
    genji "でもな、俺がさっき言ったように…"

# game/script.rpy:761
translate japanese choice_1_end_91fb7535:

    # genji "It's true that you still lack velocity and accuracy in your throw."
    genji "君の投球にスピードとコントロールが足りないのは本当だ。"

# game/script.rpy:762
translate japanese choice_1_end_437b3eaf:

    # genji "What you need now is more training and to learn to work together with the team!"
    genji "君に今必要なのは、たくさん練習してチームプレイを学ぶことだ！"

# game/script.rpy:764
translate japanese choice_1_end_324b16eb:

    # genji "Teamwork is very important in baseball."
    genji "野球ではチームワークがとても重要だ。"

# game/script.rpy:765
translate japanese choice_1_end_50c99027:

    # genji "Build up your relationship with your teammates and work together as one."
    genji "チームメイトとの絆を深めて、一丸となって練習に励んで欲しい。"

# game/script.rpy:767
translate japanese choice_1_end_30466414:

    # genji "Remember, there's no 'I' in 'TEAM'!"
    genji "忘れるなよ、「チーム」に「個」は必要ない！"

# game/script.rpy:768
translate japanese choice_1_end_5726e370:

    genji "It's your relationship with your teammates that build your confidence in playing!"
    genji "あなたのチームメートとの関係があなたのプレーに自信を深めます！"

# game/script.rpy:770
translate japanese choice_1_end_79d837cb:

    genji "You are a good pitcher, it pretty much sums up your potential in baseball."
    genji "あなたは良い投手です、野球のあなたの可能性をかなり集めています。"

# game/script.rpy:771
translate japanese choice_1_end_fcbf64fa:

    genji "But pitching alone is not enough."
    genji "しかし、投手だけでは十分ではありません。"

# game/script.rpy:772
translate japanese choice_1_end_86e2d094:

    genji "Judging from the strength and the way you throw, your bat handling skills needs some work."
    genji "強さと投げ方から判断すると、あなたのバットハンドリングスキルにはいくつかの作業が必要です。"

# game/script.rpy:774
translate japanese choice_1_end_fe89e524:

    genji "Ichiru here is our clean-up hitter, he can teach you a thing or two about batting."
    genji "ここで私たちのクリーンアップのヒッターは、彼はあなたに打撃について何かを教えることができます。"

# game/script.rpy:775
translate japanese choice_1_end_6efad5b2:

    genji "I am confident in your pitching skills. I will set Masaru as your official catcher."
    genji "私はあなたの投球スキルに自信があります。私はあなたを正式なキャッチャーとして設定します。"

# game/script.rpy:777
translate japanese choice_1_end_694d2736:

    genji "Today is your first day of training."
    genji "今日はあなたのトレーニング初日です。"

# game/script.rpy:778
translate japanese choice_1_end_552b2adc:

    genji "What do you prefer to train first?"
    genji "最初に鍛えたいものは何ですか？"

# game/script.rpy:805
translate japanese choice_2_A_467949f1:

    genji "Great! Your bat-handling skills will improve in no time!"
    genji "すばらしいです！あなたのバット処理スキルはすぐに向上します！"

# game/script.rpy:807
translate japanese choice_2_A_2c43df1f:

    # ichiru "Alright!!~ Great choice, Tofu!"
    ichiru "オッケ〜！ いい選択だぜ、Tofu！"

# game/script.rpy:808
translate japanese choice_2_A_8e5d0740:

    ichiru "You can count on me to teach you everything I know!!"
    ichiru "あなたは私が知っているすべてを教えるために私を期待することができます！"

# game/script.rpy:810
translate japanese choice_2_A_a009cd9e:

    # toshu "Thank you!! I'll do my best!"
    toshu "ありがとうございます！！ 一生懸命頑張ります！"

# game/script.rpy:812
translate japanese choice_2_A_cf42f182:

    # ichiru "This is gonna be so much fun!"
    ichiru "こいつは楽しくなりそうだぜ！"

# game/script.rpy:814
translate japanese choice_2_A_b8b3ad6b:

    masaru "Wow you guys get along really fast! Hahaha!"
    masaru "あなたは本当に速く一緒にうわー！ははは！"

# game/script.rpy:816
translate japanese choice_2_A_1c51f55f:

    masaru "We should start training before it gets dark."
    masaru "暗くなる前に訓練を始めるべきです。"

# game/script.rpy:818
translate japanese choice_2_A_2696d4c0:

    ichiru "C'mon Tofuuu~!"
    ichiru "さようなら〜！"

# game/script.rpy:819
translate japanese choice_2_A_f5c2fac9:

    narrator "*Ichiru drags Toshu away*"
    narrator "*Ichiru drags Toshu away*"

# game/script.rpy:821
translate japanese choice_2_A_35e47129:

    toshu "It's TOSHU!! You're doing this on purpose aren't you?!"
    toshu "それはTOSHUです！あなたはこれをやっているのですね。"

# game/script.rpy:836
translate japanese choice_2_B_6669d52d:

    genji "I don't recommend you to become a catcher, I am confident with Masaru's catching abilities."
    genji "私はキャッチャーになることをお勧めしません、私はマサルのキャッチ能力に自信を持っています。"

# game/script.rpy:837
translate japanese choice_2_B_a7336ff1:

    genji "Masaru is already the best catcher there is, and it'll be a shame if you train just to be a spare catcher."
    genji "Masaruはすでに最高のキャッチャーであり、あなたが予備のキャッチャーになるために鍛えれば残念です。"

# game/script.rpy:839
translate japanese choice_2_B_9d0f8569:

    masaru "Coach is right Toshu, why don't we practice your pitching instead? I can teach you some signs."
    masaru "コーチは正しいトーシュです、代わりにあなたの投球を練習してみませんか？私はいくつかの兆候を教えてくれる。"

# game/script.rpy:841
translate japanese choice_2_B_86f0c530:

    toshu "Ahh, Okay, I'm sorry Coach!"
    toshu "ああ、申し訳ありません、コーチです！"

# game/script.rpy:842
translate japanese choice_2_B_db4456fb:

    toshu "I'll be pitching instead!"
    toshu "私は代わりにピッチングするよ！"

# game/script.rpy:844
translate japanese choice_2_B_3d35084b:

    genji "Okay it's settled. Masaru, you will be Toshu's mentor, make sure to mould him into an excellent player!"
    genji "さて、それは解決されました。マサル、あなたはトシュのメンターになるでしょう、優れた選手に彼を形作ることを確認してください！"

# game/script.rpy:846
translate japanese choice_2_B_efc4e9c7:

    # masaru "Yes, Coach!"
    masaru "はい、コーチ！"

# game/script.rpy:848
translate japanese choice_2_B_0c5644c0:

    # masaru "Come on, Toshu. Let's get you trained."
    masaru "さあ、Toshu。特訓するぞ。"

# game/script.rpy:850
translate japanese choice_2_B_98727b26:

    # toshu "Yes!"
    toshu "はい！"

# game/script.rpy:852
translate japanese choice_2_B_e2d5b923:

    ichiru "Wait! I'll come with you guys!"
    ichiru "待つ！私はあなたたちと一緒に来るよ！"

# game/script.rpy:868
translate japanese choice_2_C_38529c8b:

    genji "Great! You have talent in your throwing skills!"
    genji "すばらしいです！あなたは投げ技に才能を持っています！"

# game/script.rpy:869
translate japanese choice_2_C_1acda892:

    genji "You just need to learn some of the signs Masaru gives to the pitcher and also add speed to your throw."
    genji "Masaruが投手に与える兆候のいくつかを学ぶだけでなく、あなたの投球にスピードを加える必要があります。"

# game/script.rpy:870
translate japanese choice_2_C_c6a896f6:

    genji "The pitcher and the catcher should have a special bond with each other. So I'll assign Masaru to become your mentor, okay?!"
    genji "投手とキャッチャーは、互いに特別な絆を持つべきです。名前：だから私はあなたの指導者になる、大丈夫を割り当てます！"

# game/script.rpy:872
translate japanese choice_2_C_c56c18ed:

    toshu "Yes Coach! I will do my best!"
    toshu "はいコーチ！最善をつくします！"

# game/script.rpy:874
translate japanese choice_2_C_ad703a3f:

    masaru "You can count on me, Toshu!"
    masaru "Toshu！"

# game/script.rpy:875
translate japanese choice_2_C_1428c5fa:

    narrator "*Masaru pats Toshu's head*"
    narrator "*Masaru pats Toshu's head*"

# game/script.rpy:877
translate japanese choice_2_C_3088f8db:

    toshu "Thank you Captain!"
    toshu "ありがとう、キャプテン！"

# game/script.rpy:879
translate japanese choice_2_C_33cbeacb:

    ichiru "Hey, hey! Let's go guys! It's getting late, we should start training now!!"
    ichiru "ねえ、ちょっと！みんなに行こうよ！遅くなっているので、今すぐトレーニングを始めましょう！"

# game/script.rpy:881
translate japanese choice_2_C_53dd753a:

    masaru "Ah, yeah. Come on Toshu!"
    masaru "ああ、そうです。 Toshuに来なさい！"

# game/script.rpy:883
translate japanese choice_2_C_5c4aa666:

    toshu "Yes, Captain!"
    toshu "はい、キャプテン！"

# game/script.rpy:898
translate japanese choice_2_D_93bc88f9:

    # genji "Wh-whaaa…??! REALLY?!"
    genji "は、はあぁ…？？！ 本気か？！"

# game/script.rpy:900
translate japanese choice_2_D_87c6b4f6:

    # genji "HAHAHAHA!!"
    genji "ハッハッハ！！"

# game/script.rpy:902
translate japanese choice_2_D_cbd389db:

    # ichiru "NO TOSHU! GAYJI doesn't know anything about baseball. He's practically useless when he plays it."
    ichiru "やめとけToshu！ GAYJIは野球のこと何も知らねーよ。一緒にプレイすれば分かるけどアイツ役立たずだぜ。"

# game/script.rpy:904
translate japanese choice_2_D_fd326c75:

    # genji "IT'S GENJI YOU ANNOYING BRAT!!!"
    genji "GAYJIじゃなくてGENJIだ、このクソガキ！！！"

# game/script.rpy:906
translate japanese choice_2_D_97346579:

    genji "Yeah sure I don't play anymore, but at least I know all the rules and I can examine a player's potential."
    genji "確かに私はもうプレーしないだろうが、少なくとも私はすべてのルールを知っていて、プレイヤーの可能性を調べることができる。"

# game/script.rpy:908
translate japanese choice_2_D_ec1276a8:

    masaru "It's because Coach Genji is also a physical education teacher. I heard he's also a preschool teacher. He must be really a busy person."
    masaru "Genji教授も体育教師であるからです。彼は就学前の先生でもあると聞きました。彼は本当に忙しい人でなければなりません。"

# game/script.rpy:910
translate japanese choice_2_D_fb06cd9c:

    ichiru "Hmph! The truth is he doesn't care about us at all!!"
    ichiru "Hmph！真実は彼が私たち全然気にしないことです！"

# game/script.rpy:912
translate japanese choice_2_D_880be09d:

    # genji "H-hey that is not true!! I LOVE each and every one of you like my own children, which of course I don't have."
    genji "お、おい。そんなことないぞ！ 俺は自分の受け持ってる生徒じゃなくても一人ひとり我が子のように愛してるぞ。"

# game/script.rpy:913
translate japanese choice_2_D_98c19286:

    genji "This is just my second year coaching baseball, spare me some consideration!"
    genji "これはちょうど私の2年目のコーチング野球です。"

# game/script.rpy:915
translate japanese choice_2_D_6abfa09b:

    ichiru "Pffft! With two years, I can master baseball even without this stupid Coach!"
    ichiru "パフフト！ 2年間で、私はこの愚かなコーチでなくても野球をマスターすることができます！"

# game/script.rpy:917
translate japanese choice_2_D_a9c4a5b2:

    genji "WHY YOU…!! YOU'RE NOTHING WITHOUT ME, YOU BRAT!"
    genji "あなたはなぜ…！あなたは私なしで何もしていません、あなたは殴ります！"

# game/script.rpy:919
translate japanese choice_2_D_7d396221:

    ichiru "Anyway Toshu, I won't let this creep teach you!"
    ichiru "とにかくToshu、私はこのクリープがあなたに教えさせません！"

# game/script.rpy:920
translate japanese choice_2_D_d18969c3:

    ichiru "I volunteer training with Toshu. Besides, he is my classmate, I can teach him while we're in class."
    ichiru "私はToshuと一緒にボランティアトレーニングをします。それに、彼は私のクラスメートです、私たちが授業中に私は彼に教えることができます。"

# game/script.rpy:922
translate japanese choice_2_D_9416991f:

    masaru "But, you're barely attending your classes…"
    masaru "しかし、あなたはほとんどクラスに出席していません…"

# game/script.rpy:924
translate japanese choice_2_D_99d4429a:

    ichiru "No way! With Toshu around, I will go attend my classes even if it's everyday~"
    ichiru "とんでもない！都心では毎日でもクラスに参加します〜"

# game/script.rpy:926
translate japanese choice_2_D_547e4ccc:

    genji "Fine! Do what you want. I'll assign Toshu with you then! What do you think Toshu?"
    genji "いいだろう！ やりたいようにやれ。Toshuの相手はお前に任せた！ それでいいか？ Toshu。"

# game/script.rpy:928
translate japanese choice_2_D_333ac031:

    # toshu "I don't mind at all, Coach!"
    toshu "僕は大丈夫です、コーチ！"

# game/script.rpy:930
translate japanese choice_2_D_19154674:

    # genji "Okay, it's settled then!"
    genji "OK、これで解決だ！"

# game/script.rpy:932
translate japanese choice_2_D_3cbd22dc:

    # masaru "Anyway, it's getting late, we should start practicing."
    masaru "もういい時間だ。そろそろ練習を始めよう。"

# game/script.rpy:934
translate japanese choice_2_D_dce4cf93:

    # ichiru "Let's go, Toshu!"
    ichiru "行こうぜ、Toshu！"

# game/script.rpy:949
translate japanese after_minigame_1_3cb79917:

    # narrator "2 hours later…"
    narrator "２時間後…"

# game/script.rpy:957
translate japanese after_minigame_1_024c5542:

    masaru "I'm gonna go take a quick shower. You should take one as well, Toshu."
    masaru "私は急いでシャワーを浴びるつもりです。あなたはToshuも同様に取るべきです。"

# game/script.rpy:959
translate japanese after_minigame_1_4f6973c3:

    # toshu "Okay!"
    toshu "はい！"

# game/script.rpy:962
translate japanese after_minigame_1_8891735c:

    # ichiru "Whew! I'm beat!!"
    ichiru "ひゃーっ！　疲れたー！"

# game/script.rpy:964
translate japanese after_minigame_1_24000068:

    # toshu "You must be really tired, Ichiru!"
    toshu "ホントに疲れたみたいだね、Ichiru！"

# game/script.rpy:966
translate japanese after_minigame_1_89ae3b63:

    # ichiru "Aww~ Tofu is worried about me. Hahaha!"
    ichiru "あ〜 Tofuが俺のこと心配してるよ。ハハハ！"

# game/script.rpy:968
translate japanese after_minigame_1_ebdaf686:

    ichiru "Well, I'm not just tired. I'm hungry…!! Aren't you too, Tofu?"
    ichiru "まあ、私は疲れているだけではありません。お腹が空きました…！！あなたも、Tofuですか？"

# game/script.rpy:970
translate japanese after_minigame_1_afd749aa:

    # toshu "It's Toshu…"
    toshu "Toshuだってのに…"

# game/script.rpy:972
translate japanese after_minigame_1_1c1b4003:

    # ichiru "Where's Masaru?"
    ichiru "Masaruはどこ？"

# game/script.rpy:974
translate japanese after_minigame_1_2e003193:

    # toshu "Oh, he said he's gonna go take a shower."
    toshu "ああ、シャワー浴びてくるって言ってたよ。"

# game/script.rpy:975
translate japanese after_minigame_1_81839c3d:

    # toshu "Won't you take a bath as well?"
    toshu "Ichiruはお風呂入らないの？"

# game/script.rpy:977
translate japanese after_minigame_1_c55f8698:

    # ichiru "Goooooshhh… I'm too lazy to take a bath. I'll just change my clothes!"
    ichiru "う～ん…風呂入るの面倒だな。着替えだけでいいや！"

# game/script.rpy:978
translate japanese after_minigame_1_8e85cf11:

    ichiru "I can just go take shower at home. It's much more comfortable than the showers here!"
    ichiru "私はちょうど自宅でシャワーを浴びることができます。ここのシャワーよりはるかに快適です！"

# game/script.rpy:980
translate japanese after_minigame_1_91450a91:

    # ichiru "How about you, Tofu?"
    ichiru "Tofu、お前はどうすんの？"

# game/script.rpy:1006
translate japanese choice_3_A_b94d3998:

    ichiru "Oh okay. It's easier to just change clothes, you know?"
    ichiru "ああ大丈夫。衣服を交換する方が簡単です。"

# game/script.rpy:1007
translate japanese choice_3_A_2facf9a9:

    toshu "Don't worry, I won't be long!"
    toshu "心配しないで、私は長くなることはありません！"

# game/script.rpy:1009
translate japanese choice_3_A_1325ff2c:

    # ichiru "Come back soon, okay?"
    ichiru "すぐ戻ってこいよ、いいな？"

# game/script.rpy:1011
translate japanese choice_3_A_4b17f8dc:

    # toshu "Sure!"
    toshu "分かったよ！"

# game/script.rpy:1014
translate japanese choice_3_A_3f852bc4:

    # toshu_t "Okay… so which cubicle is empty…?"
    toshu_t "うーんと、どれが空いてるかな？"

# game/script.rpy:1015
translate japanese choice_3_A_eb6b4f3b:

    # narrator "*Toshu opens a curtain*"
    narrator "(カーテンを開く)"

# game/script.rpy:1022
translate japanese choice_3_A_71ecb01a:

    # toshu "C-Captain?!"
    toshu "キャ、キャプテン？！"

# game/script.rpy:1023
translate japanese choice_3_A_40ffa724:

    # toshu_t "He's completely naked!!"
    toshu_t "真っ裸だ！"

# game/script.rpy:1025
translate japanese choice_3_A_f7ad8751:

    # masaru "Oh. Toshu."
    masaru "おお、Toshu。"

# game/script.rpy:1027
translate japanese choice_3_A_25060df1:

    # masaru "What's up? Do you need soap or anything?"
    masaru "どうした？ 石鹸でも忘れたのか？"

# game/script.rpy:1028
translate japanese choice_3_A_dec568f9:

    # toshu "…!!"
    toshu "…！！"

# game/script.rpy:1029
translate japanese choice_3_A_62fc485f:

    toshu_t "H-his body is so defined… almost perfect…"
    toshu_t "彼の体はこうして定義されています…ほぼ完璧です…"

# game/script.rpy:1031
translate japanese choice_3_A_8cceb6e1:

    # masaru "Are you alright, Toshu? Your face is all red."
    masaru "Toshu、大丈夫か？ 顔が真っ赤だぞ。"

# game/script.rpy:1032
translate japanese choice_3_A_df2336eb:

    # toshu "I-it's nothing, Captain!! I'm sorry!!"
    toshu "な、なんでもないです、キャプテン！ ごめんなさい！！"

# game/script.rpy:1033
translate japanese choice_3_A_4b0a1dc7:

    toshu "I was just looking for a cubicle to take a shower…"
    toshu "私はシャワーを浴びるために部屋を探していただけだった…"

# game/script.rpy:1035
translate japanese choice_3_A_35850fdf:

    masaru "I'm almost done. You can take a shower here."
    masaru "私はほとんど終わった。ここでシャワーを浴びることができます。"

# game/script.rpy:1036
translate japanese choice_3_A_fd87a75f:

    toshu "O-okay. I'm sorry again!"
    toshu "大丈夫もう一度申し訳ありません！"

# game/script.rpy:1038
translate japanese choice_3_A_8ec133ad:

    toshu_t "As the relaxing shower water flowed across my body… it made me think…"
    toshu_t "リラックスしたシャワーの水が私の体を流れるように…それは私に考えさせました…"

# game/script.rpy:1039
translate japanese choice_3_A_559d7bb1:

    toshu_t "The image of Captain Masaru being naked…"
    toshu_t "キャプテンマサルの裸のイメージ…"

# game/script.rpy:1040
translate japanese choice_3_A_d4b43f2f:

    toshu_t "It's weird… I felt something rushing inside me…"
    toshu_t "それは変だ…私は私の中で急ぐ何かを感じた…"

# game/script.rpy:1041
translate japanese choice_3_A_83bca322:

    toshu_t "I don't get it at all! I shouldn't think about this for now…"
    toshu_t "私はまったくそれを得ていない！今はこれについて考えるべきではない…"

# game/script.rpy:1049
translate japanese choice_3_A_8aa888ba:

    # toshu "C-Captain!"
    toshu "キャ、キャプテン！"

# game/script.rpy:1050
translate japanese choice_3_A_74b7f9e0:

    # toshu_t "He's wearing only his underwear!"
    toshu_t "キャプテンは下着姿で立ってる！"

# game/script.rpy:1052
translate japanese choice_3_A_996eecc2:

    # masaru "Ah, Toshu. I wanted to talk with you a bit."
    masaru "ああ、Toshu。 ちょっと話がしたくてね。"

# game/script.rpy:1055
translate japanese choice_3_A_c0ae1f20:

    masaru "Great job on today's practice by the way. You're really talented, Toshu!"
    masaru "途中で今日の練習に偉大な仕事。あなたは本当に才能豊かです。"

# game/script.rpy:1056
translate japanese choice_3_A_58e152cd:

    masaru "I knew the moment I caught that ball you have something special!"
    masaru "私はあなたが何か特別なボールをつかんだ瞬間を知っていた！"

# game/script.rpy:1057
translate japanese choice_3_A_2522c714:

    # toshu "B-but Coach said I need more training…"
    toshu "で、でもコーチはもっと練習が必要だって…"

# game/script.rpy:1059
translate japanese choice_3_A_30c92d63:

    masaru "Yeah, he did. And it's true! Potential alone is not enough, one needs to train it! Let's just say you're like an unpolished gem!"
    masaru "うん、彼はそうした。それは本当です！潜在的なものだけでは不十分です。それを訓練する必要があります！ただ、あなたが磨かれていない宝石のようだと言ってみましょう！"

# game/script.rpy:1061
translate japanese choice_3_A_45a2e309:

    toshu "Really…?"
    toshu "本当に…？"

# game/script.rpy:1064
translate japanese choice_3_A_0fd577df:

    masaru "Don't take the Coach too seriously. He just likes to joke around, and since you are the newest member he sees you as an easy prey for it."
    masaru "コーチをあまり真剣に受け入れないでください。彼はちょうど周りを冗談を言うのが好きで、あなたが新しいメンバーであるので、彼はあなたをそれのための簡単な獲物とみなします。"

# game/script.rpy:1066
translate japanese choice_3_A_e1ae3f1d:

    masaru "I remember last year we had a new recruit, Coach teased him so bad that he quit on the next day."
    masaru "私は昨年、新しい募集をしたことを覚えています。コーチは彼をひどく驚かせて、次の日に辞めました。"

# game/script.rpy:1068
translate japanese choice_3_A_9a37dc37:

    masaru "Coach really just values teamwork more than anything else. If one is not ready to improve for others sake, he threatens them just like that."
    masaru "コーチは本当にチームワークを重視しています。もし誰かのために改善の準備が整っていなければ、彼はそのようなものを脅かすでしょう。"

# game/script.rpy:1070
translate japanese choice_3_A_ab1f9ce1:

    toshu "Ahh, I see… I'll just try my best not to disappoint him then."
    toshu "ああ、私は…私は彼を失望させないように最善を尽くすつもりです。"

# game/script.rpy:1072
translate japanese choice_3_A_e4e34fea:

    masaru "That's the spirit Toshu! I'm sure you'll get along with Coach pretty soon!"
    masaru "それは精神Toshuです！私はかなりすぐにコーチと一緒になるだろうと確信しています！"

# game/script.rpy:1074
translate japanese choice_3_A_587025e1:

    toshu "Yes! I will!"
    toshu "はい！します！"

# game/script.rpy:1076
translate japanese choice_3_A_0b89679e:

    masaru "Ichiru is probably waiting for us, we should go."
    masaru "イチルはおそらく私たちを待っているでしょう、私たちは行くべきです。"

# game/script.rpy:1077
translate japanese choice_3_A_837af124:

    toshu "Alright!"
    toshu "よかった！"

# game/script.rpy:1079
translate japanese choice_3_A_3a6bcf75:

    toshu_t "Captain Masaru sound really responsible. He really is the perfect role model for a team captain."
    toshu_t "キャプテンマサルは本当に責任があります。彼は本当にチームキャプテンにとって完璧なロールモデルです。"

# game/script.rpy:1080
translate japanese choice_3_A_f6eb1908:

    toshu_t "Although, as much as I admired his selflessness, I wish he told me more about himself."
    toshu_t "私が彼の無私を賞賛したのと同じくらい、彼は自分自身についてもっと教えてくれることを願っています。"

# game/script.rpy:1088
translate japanese choice_3_A_8b6d909c:

    # ichiru "What took you two so long?! I'm starving here!!"
    ichiru "二人して何やってたんだよ？ 俺もう腹ペコだよ！"

# game/script.rpy:1089
translate japanese choice_3_A_111d709d:

    # ichiru "It's already dark outside!"
    ichiru "外はもう真っ暗だし！"

# game/script.rpy:1090
translate japanese choice_3_A_39e65469:

    # masaru "Sorry Ichiru. Come on, let's all go home."
    masaru "ゴメンな、Ichiru。さあ、家に帰ろう。"

# game/script.rpy:1091
translate japanese choice_3_A_b2761d44:

    # masaru "Please be safe on your way home okay?"
    masaru "気を付けて帰るんだぞ？"

# game/script.rpy:1092
translate japanese choice_3_A_3d42946f:

    # toshu "Take care guys!"
    toshu "みんな気を付けてね！"

# game/script.rpy:1097
translate japanese choice_3_B_572d2a29:

    # ichiru "What?? Are you out of your mind??"
    ichiru "ハァ？？ マジかよ？？"

# game/script.rpy:1098
translate japanese choice_3_B_6e16503c:

    ichiru "Plus, aren't you starving?! Save it for the next training!"
    ichiru "プラス、あなたは飢えていませんか？次のトレーニングのためにそれを保存してください！"

# game/script.rpy:1100
translate japanese choice_3_B_b4dd1a48:

    toshu "Ehh… but Coach said I need more training."
    toshu "ええ、コーチはもっと訓練が必要だと言った。"

# game/script.rpy:1102
translate japanese choice_3_B_42e236f2:

    ichiru "Yeah he did but that doesn't mean training the whole day without resting!"
    ichiru "うん、彼はやったけど、それは休んでいない一日中のトレーニングを意味するものではありません！"

# game/script.rpy:1103
translate japanese choice_3_B_40bcdd48:

    ichiru "That's just stupid!"
    ichiru "それはただのばかです！"

# game/script.rpy:1105
translate japanese choice_3_B_64796894:

    ichiru "Take a shower, get yourself cleaned."
    ichiru "シャワーを浴びて、自分をきれいにしなさい。"

# game/script.rpy:1107
translate japanese choice_3_B_b87a7bba:

    toshu "Oh, I'm sorry Ichiru!"
    toshu "ああ、すっごく残念だよ！"

# game/script.rpy:1108
translate japanese choice_3_B_fe928230:

    toshu "I'll go take a bath."
    toshu "お風呂に行くよ。"

# game/script.rpy:1110
translate japanese choice_3_B_b9ef2331:

    ichiru "Okay, I'll be waiting here."
    ichiru "さて、私はここで待っています。"

# game/script.rpy:1111
translate japanese choice_3_B_6909c04b:

    ichiru "Be quick, okay? I need to go home early. I'm very hungry."
    ichiru "すばやく、大丈夫ですか？早く家に帰る必要がある。私はとてもお腹がすいてます。"

# game/script.rpy:1113
translate japanese choice_3_B_94b3b1e9:

    toshu "Okay. I won't be long!"
    toshu "はい。私は長くはしません！"

# game/script.rpy:1116
translate japanese choice_3_B_39a23c2d:

    toshu_t "What was I thinking? Wanting to go outside and practice…"
    toshu_t "私が考えていたことは何でしょう？外に出て練習したい…"

# game/script.rpy:1117
translate japanese choice_3_B_a6126237:

    toshu_t "Despite Ichiru scolding me, I can totally feel him worrying about me at that moment."
    toshu_t "イチールが私を叱るのにもかかわらず、私は彼が私の心配を完全に感じることができます。"

# game/script.rpy:1118
translate japanese choice_3_B_8ca1c232:

    toshu_t "I should be more careful with what I say to not make him worry next time."
    toshu_t "私は次回に彼が心配しないように私が言うことにもっと注意する必要があります。"

# game/script.rpy:1128
translate japanese choice_3_B_bce2b15c:

    toshu "I'm back!"
    toshu "戻ってきました！"

# game/script.rpy:1129
translate japanese choice_3_B_8e03fbc3:

    ichiru "What took you so long Tofuuu? I'm so hungry I could eat ten beef bowls…"
    ichiru "あなたはとても長いTofuだったのですか？私は空腹で、私は10本の牛のボウルを食べることができました…"

# game/script.rpy:1131
translate japanese choice_3_B_29e1dea1:

    toshu "A-aaa…"
    toshu "アアアア"

# game/script.rpy:1133
translate japanese choice_3_B_8e696f6a:

    masaru "Let's all go home. I'm sure everyone's tired."
    masaru "すべて家に帰ろう。私は誰もが疲れていると確信しています。"

# game/script.rpy:1134
translate japanese choice_3_B_4f6973c3:

    toshu "Okay!"
    toshu "はい！"

# game/script.rpy:1141
translate japanese choice_3_C_2b244322:

    ichiru "Wow, didn't expect it is okay for you to just change clothes as well. Most of my teammates kept telling me it was a dirty habit of mine."
    ichiru "うわー、ちょうどあなたも服を交換するのは大丈夫だとは予想していませんでした。私のチームメイトのほとんどは、それが私の汚れた習慣であると私に伝え続けました。"

# game/script.rpy:1143
translate japanese choice_3_C_837ac9cc:

    toshu "Really? But we can always take a bath at home right?"
    toshu "本当に？でも、いつも家でお風呂にはいられますか？"

# game/script.rpy:1145
translate japanese choice_3_C_25164207:

    ichiru "Yeah! Exactly!"
    ichiru "うん！まったく！"

# game/script.rpy:1146
translate japanese choice_3_C_26fe0aaf:

    ichiru "I'd prefer to take a bath at home after a long and hard day of training."
    ichiru "私は長いと困難な訓練の後に家で風呂を取ることを好むだろう。"

# game/script.rpy:1148
translate japanese choice_3_C_b3b4027b:

    toshu "Yup! And you can go straight to bed and the feeling is very comfortable and relaxing!"
    toshu "うん！そして、あなたはまっすぐ寝ることができ、気分はとても快適でリラックスしています！"

# game/script.rpy:1150
translate japanese choice_3_C_204bef2f:

    ichiru "Hell yeah!"
    ichiru "地獄ええ！"

# game/script.rpy:1151
translate japanese choice_3_C_d9e7f84e:

    ichiru "For the first time, someone agreed with me with this!"
    ichiru "初めて、誰かが私に同意した！"

# game/script.rpy:1152
translate japanese choice_3_C_6eccf9e3:

    ichiru "Thanks Tofu!"
    ichiru "ありがとうTofu！"

# game/script.rpy:1154
translate japanese choice_3_C_99a94b4e:

    toshu "It's Toshu! T-O-S-H-U!"
    toshu "It's Toshu! T-O-S-H-U!"

# game/script.rpy:1155
translate japanese choice_3_C_700e6ff2:

    toshu "Is it really that hard to memorize?"
    toshu "それは本当にそれを覚えるのは難しいですか？"

# game/script.rpy:1156
translate japanese choice_3_C_95c730e4:

    ichiru "Baaah! Let's just get changed!"
    ichiru "バア！ちょっと変わってみましょう！"

# game/script.rpy:1158
translate japanese choice_3_C_1fa8db50:

    toshu_t "Without hesitation, Ichiru indeed stripped himself in front of me."
    toshu_t "躊躇せずに、イチルは確かに私の前で自分自身を剥がした。"

# game/script.rpy:1159
translate japanese choice_3_C_d0a0e943:

    toshu_t "I watched him take off his clothes piece by piece until…"
    toshu_t "私は彼が彼の服を一枚ずつ撤去するのを見ました…"

# game/script.rpy:1166
translate japanese choice_3_C_c5855387:

    ichiru "What are you staring at, Tofu?"
    ichiru "あなたは何を凝視していますか、Tofu？"

# game/script.rpy:1167
translate japanese choice_3_C_3280b794:

    toshu "Ehh! Wah I'm sorry!!"
    toshu "ええ！うわー、ごめんなさい！"

# game/script.rpy:1171
translate japanese choice_3_C_91bc565d:

    ichiru "Hahaha! And why are you blushing so hard?"
    ichiru "ははは！そしてなぜあなたはどんなに赤面していますか？"

# game/script.rpy:1172
translate japanese choice_3_C_cc08b96b:

    toshu_t "Why am I staring?! Hurry Toshu! Make a quick excuse!"
    toshu_t "なぜ私は見つめているのですか？急いでToshu！簡単な言い訳をしてください！"

# game/script.rpy:1173
translate japanese choice_3_C_19fc813f:

    toshu "Ohh I was just looking at your jockstrap! I thought it looked cool!"
    toshu "ああ、私はちょうどあなたのジョークストラップを見ていた！私はそれがクールだと思った！"

# game/script.rpy:1174
translate japanese choice_3_C_eae94ed6:

    toshu_t "AAAAHH! Out of everything, why did I say that?!"
    toshu_t "AAAAHH！すべてのうち、なぜ私はそれを言ったのですか？"

# game/script.rpy:1175
translate japanese choice_3_C_a20e25ba:

    ichiru "You like it?"
    ichiru "あなたはそれが好き？"

# game/script.rpy:1176
translate japanese choice_3_C_2d5cf229:

    toshu "Ahh! Yeah! It looks like it has a nice quality!"
    toshu "ああ！うん！それは素晴らしい品質を持っているように見えます！"

# game/script.rpy:1177
translate japanese choice_3_C_76f2fabb:

    toshu "Too bad, I didn't bring mine…"
    toshu "あまりにも、私は私のものを持ってこなかった…"

# game/script.rpy:1179
translate japanese choice_3_C_cfbf68fb:

    ichiru "Ehh…? Don't worry! You can wear yours next time, I'm sure it looks cool on you too~"
    ichiru "エ…？心配しないでください！あなたは次回はあなたが着ることができます、私はそれもあなたに涼しいと確信しています〜"

# game/script.rpy:1180
translate japanese choice_3_C_4557996d:

    toshu "You mean…?"
    toshu "もしかして…？"

# game/script.rpy:1181
translate japanese choice_3_C_89173709:

    ichiru "Why are we even talking about our undies?"
    ichiru "なぜ私たちはうそを話しているのですか？"

# game/script.rpy:1182
translate japanese choice_3_C_97330d7c:

    toshu "Ahh… I'm sorry I really didn't mean it."
    toshu "ああ…申し訳ありません、私は本当にそれを意味しませんでした。"

# game/script.rpy:1190
translate japanese choice_3_C_fe16fc2b:

    ichiru "By the way, Tofu. You did so well with today's training! You definitely impressed all of our teammates~!"
    ichiru "ところで、Tofu。あなたは今日のトレーニングでうまくやった！あなたは間違いなくすべてのチームメイトに感銘を受けました〜！"

# game/script.rpy:1192
translate japanese choice_3_C_b4fa697c:

    ichiru "I didn't think we'd be on the same level! Haha!!"
    ichiru "私は同じレベルにいるとは思わなかった！ハハ!!"

# game/script.rpy:1194
translate japanese choice_3_C_b24d4485:

    toshu "R-really?"
    toshu "本当に？"

# game/script.rpy:1195
translate japanese choice_3_C_962e0ec9:

    toshu "B-but Coach said that I need a lot more training…"
    toshu "B-しかし、コーチは、私はもっと多くのトレーニングが必要だと言った…"

# game/script.rpy:1197
translate japanese choice_3_C_51b128cb:

    ichiru "PFF!! Ignore that Gay Coach!"
    ichiru "PFF !!ゲイコーチを無視してください！"

# game/script.rpy:1199
translate japanese choice_3_C_9bdef1c6:

    ichiru "Hahaha!! He's just pretending to be awesome."
    ichiru "ははは！！彼はちょうどすごくふりをしています。"

# game/script.rpy:1201
translate japanese choice_3_C_36e13632:

    ichiru "All he does is just boss around during training."
    ichiru "彼がしているのは、トレーニング中のボスだけです。"

# game/script.rpy:1202
translate japanese choice_3_C_b155d3ec:

    ichiru "Nothing good is coming out from that mouth of his."
    ichiru "彼の口から何も良いものが出てこない。"

# game/script.rpy:1203
translate japanese choice_3_C_e79d2b48:

    ichiru "That old geezer!"
    ichiru "その古いオタク！"

# game/script.rpy:1205
translate japanese choice_3_C_5481874b:

    toshu "By the way, Ichiru, how come you can speak to Coach like that? Aren't you scared of him?"
    toshu "ところで、Ichiru、どうやってそんなコーチに話すことができますか？彼を恐れていないのですか？"

# game/script.rpy:1207
translate japanese choice_3_C_dcef1819:

    ichiru "No way! He might look like a super muscle man outside but he's really just a harmless and wimpy old man inside!"
    ichiru "とんでもない！彼は外のスーパーマッスルの男のように見えるかもしれませんが、彼は本当に無害で愚かな老人です！"

# game/script.rpy:1209
translate japanese choice_3_C_06d51d13:

    ichiru "Plus, it's okay, the staff here has connection with my parents."
    ichiru "プラス、それは大丈夫です、ここのスタッフは私の両親とつながっています。"

# game/script.rpy:1211
translate japanese choice_3_C_3a3f338f:

    toshu "Ehh? What do you mean?"
    toshu "え？どういう意味ですか？"

# game/script.rpy:1212
translate japanese choice_3_C_3afd84ae:

    masaru "Hey guys, I'm done!"
    masaru "ねえ、私はやった！"

# game/script.rpy:1214
translate japanese choice_3_C_ff3e355b:

    ichiru "Don't mind it, Masaru is here."
    ichiru "気にしないで、優はここにいます。"

# game/script.rpy:1216
translate japanese choice_3_C_be384caa:

    toshu_t "I didn't know what Ichiru meant when he said he had some sort of connection to the school staff."
    toshu_t "私は、学校の職員と何らかの関係があると言ったときに、Ichiruが何を意味するのか分からなかった。"

# game/script.rpy:1217
translate japanese choice_3_C_48b939a6:

    toshu_t "But I guess I really shouldn't mind about these trivial things."
    toshu_t "しかし、私は本当にこれらの些細なことについて気にするべきではないと思います。"

# game/script.rpy:1227
translate japanese choice_3_C_86f272e6:

    masaru "What were you guys talking about?"
    masaru "あなたは何を話していたのですか？"

# game/script.rpy:1229
translate japanese choice_3_C_66e1cfaf:

    ichiru "Nothing, nothing!"
    ichiru "何もない！"

# game/script.rpy:1230
translate japanese choice_3_C_5a06ff42:

    ichiru "C'mon! Let's go home I'm really hungry~"
    ichiru "さあ！家に帰ろう私は本当に空腹だ〜"

# game/script.rpy:1232
translate japanese choice_3_C_5d0487df:

    toshu "Yeah, it's really getting dark outside too."
    toshu "ええ、それは本当に暗くなってきています。"

# game/script.rpy:1234
translate japanese choice_3_C_15c0ec2c:

    masaru "You two take care on your way home okay?"
    masaru "あなたは二人の家に大丈夫ですか？"

# game/script.rpy:1236
translate japanese choice_3_C_714510d5:

    ichiru "Okay see you tomorrow guys!"
    ichiru "明日あなたに会いましょう！"

# game/script.rpy:1242
translate japanese choice_3_D_cee53033:

    ichiru "Wh-Whaa?? Really?!"
    ichiru "Wh  -  Whaa ??本当に？！"

# game/script.rpy:1244
translate japanese choice_3_D_f93ebc4f:

    ichiru "Hahaha!!"
    ichiru "ハハハ!!"

# game/script.rpy:1245
translate japanese choice_3_D_1ac4c05e:

    ichiru "I didn't know you're just as curious as me in that kind of stuff but SUUURE!!"
    ichiru "私はあなたが私のように好奇心をそそられていることは分かっていませんでしたが、SUUURE !!"

# game/script.rpy:1247
translate japanese choice_3_D_6387412c:

    ichiru "Let's see who has the bigger d--"
    ichiru "誰がより大きなdを持っているか見てみましょう -"

# game/script.rpy:1253
translate japanese choice_3_D_0c6bd51f:

    genji "WHAT ARE YOU TWO DOING?!"
    genji "あなたは何をしていますか？"

# game/script.rpy:1256
translate japanese choice_3_D_214b3576:

    toshu "!!!"
    toshu "!!!"

# game/script.rpy:1257
translate japanese choice_3_D_d9e9e572:

    ichiru "Oops."
    ichiru "おっとっと。"

# game/script.rpy:1259
translate japanese choice_3_D_5943f7d5:

    genji "You two get dressed up or else I'm going to kick you guys out."
    genji "あなたは二人でドレスアップするか、そうでなければ私はあなたたちを追い出すつもりです。"

# game/script.rpy:1261
translate japanese choice_3_D_1fc5db18:

    ichiru "PFFT!! Oh really, GAYJI?"
    ichiru "PFFT !!ああ本当に、ゲイ？"

# game/script.rpy:1263
translate japanese choice_3_D_e4825912:

    genji "HNGH! Now!"
    genji "HNGH！今すぐ！"

# game/script.rpy:1265
translate japanese choice_3_D_ad7840b3:

    ichiru "Fine, fine, we'll get dressed. C'mon Toshu!"
    ichiru "さて、罰金、私たちは服を着るでしょう。よろしく！"

# game/script.rpy:1267
translate japanese choice_3_D_af4085fb:

    toshu_t "I didn't know what came into my mind when I cracked that joke to Ichiru."
    toshu_t "私はその冗談をIchiruに割ったとき、何が私の心に入ったのか分からなかった。"

# game/script.rpy:1268
translate japanese choice_3_D_77456202:

    toshu_t "Didn't expect he would take it seriously."
    toshu_t "彼が真剣にそれを取るとは思わなかった。"

# game/script.rpy:1269
translate japanese choice_3_D_1da3b3d3:

    toshu_t "Ichiru can be really funny sometimes."
    toshu_t "イチールは本当に面白いことがあります。"

# game/script.rpy:1278
translate japanese choice_3_D_e521a6ba:

    masaru "I'm back. Sorry it took long."
    masaru "戻ってきました。申し訳ありません、それは長くかかりました。"

# game/script.rpy:1280
translate japanese choice_3_D_02bc8b0d:

    ichiru "It's okay! C'mon let's go hoomeee! I'm hungryyyyyy!"
    ichiru "いいんだよ！さようなら、行きましょう！私はhungryyyyyyだ！"

# game/script.rpy:1281
translate japanese choice_3_D_25512efa:

    toshu "Yeah it's getting dark too."
    toshu "うん、それも暗くなってきている。"

# game/script.rpy:1283
translate japanese choice_3_D_2e77a938:

    masaru "Take care on the way home okay?"
    masaru "帰り道には大丈夫ですか？"

# game/script.rpy:1284
translate japanese choice_3_D_bd82b3b9:

    masaru "See you guys tomorrow."
    masaru "明日お会いしましょう。"
