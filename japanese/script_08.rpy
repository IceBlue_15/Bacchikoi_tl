# game/script.rpy:3807
translate japanese choice_8_end_b46811e2:

    # ichiru "*poke poke*"
    ichiru "(ツンツン)"

# game/script.rpy:3809
translate japanese choice_8_end_8810b691:

    # ichiru "He looks really adorable when he is asleep…"
    ichiru "カワイイ寝顔だな…"

# game/script.rpy:3811
translate japanese choice_8_end_bd31d3ac:

    masaru "Haha, I bet he was drained from yesterday's activity."
    masaru "ハハ、彼は昨日の活動から逃げ出したと思う。"

# game/script.rpy:3813
translate japanese choice_8_end_266ef231:

    ichiru "Should we wake him up?"
    ichiru "私たちは彼を目覚めさせるべきでしょうか？"

# game/script.rpy:3815
translate japanese choice_8_end_662f58bb:

    masaru "Yeah, we have to. Coach ordered us to assemble outside."
    masaru "うん、私たちはする必要があります。コーチは私たちに外での組み立てを命じました。"

# game/script.rpy:3817
translate japanese choice_8_end_abba4e57:

    # ichiru "Haha, Okay!"
    ichiru "ハハ、オッケー！"

# game/script.rpy:3819
translate japanese choice_8_end_3c17de59:

    # ichiru "WAKE UP TOFU!!!!!!"
    ichiru "起きろー！！ TOFU！！！！"

# game/script.rpy:3821
translate japanese choice_8_end_c5f3fe3f:

    # toshu "AHH!!!!"
    toshu "わあっ！！！！"

# game/script.rpy:3823
translate japanese choice_8_end_daa88f49:

    # ichiru "Good morning Tofu~"
    ichiru "おはようTofu〜。"

# game/script.rpy:3825
translate japanese choice_8_end_4e7d3863:

    # toshu "Ichiru! You almost gave me a heart attack!"
    toshu "Ichiru！ 心臓が止まるかと思ったよ！"

# game/script.rpy:3827
translate japanese choice_8_end_c6b4d839:

    # masaru "Good morning Toshu."
    masaru "おはよう、Toshu。"

# game/script.rpy:3829
translate japanese choice_8_end_8fadf6f5:

    # toshu "Ah, Good morning too Captain."
    toshu "あ、おはようございます、キャプテン。"

# game/script.rpy:3831
translate japanese choice_8_end_fd1351eb:

    masaru "Please pack all your stuff."
    masaru "あなたのものはすべて梱包してください。"

# game/script.rpy:3833
translate japanese choice_8_end_01a2d73c:

    toshu "Ehh…? What's happening? Are we leaving?"
    toshu "エ…？何が起こっていますか？私たちは出かけるのですか？"

# game/script.rpy:3835
translate japanese choice_8_end_50e2ed8a:

    ichiru "I think so."
    ichiru "私はそう思う。"

# game/script.rpy:3836
translate japanese choice_8_end_cd10eaa5:

    masaru "Everyone is already assembled outside, Coach is going to make an announcement."
    masaru "誰もがすでに外に集められている、コーチは発表するつもりです。"

# game/script.rpy:3838
translate japanese choice_8_end_c1b0bbb4:

    # ichiru "C'mon, c'mon, get up!"
    ichiru "さあ、起きた起きた！"

# game/script.rpy:3840
translate japanese choice_8_end_7e272a98:

    # toshu "Y-yes…!"
    toshu "う、うん…！"

# game/script.rpy:3850
translate japanese choice_8_end_33bf467e:

    genji "Okay everyone! Our last day of baseball camp is reserved for a special training activity!"
    genji "大丈夫だよ！野球キャンプの最後の日は特別なトレーニング活動のために予約されています！"

# game/script.rpy:3851
translate japanese choice_8_end_cd72ae3e:

    # genji "I am going to need everyone to follow me."
    genji "全員俺の指示に従ってもらう。"

# game/script.rpy:3853
translate japanese choice_8_end_dae1e49c:

    # genji "NO QUESTIONS!"
    genji "質問は無しだ！"

# game/script.rpy:3855
translate japanese choice_8_end_30d98833:

    genji "So everyone, pack up all your things, disassemble your tents and we'll go there as soon as everyone is ready."
    genji "誰もがあなたのものを詰め、あなたのテントを分解し、みんなが準備ができるとすぐにそこに行くでしょう。"

# game/script.rpy:3858
translate japanese choice_8_end_fa544874:

    # ichiru "Ugh… what is that old man up to this time?"
    ichiru "うへぇ…あのオッサン今度は何考えてんだ？"

# game/script.rpy:3859
translate japanese choice_8_end_eb3876c5:

    ichiru "Can't we just use this last day of camping for fun?"
    ichiru "私たちは楽しむためにキャンプの最後の日を使うことはできませんか？"

# game/script.rpy:3861
translate japanese choice_8_end_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:3863
translate japanese choice_8_end_eaad6ef7:

    toshu "Captain Masaru…!"
    toshu "Masaruキャプテン…！"

# game/script.rpy:3865
translate japanese choice_8_end_1c020b2c:

    toshu "Don't worry, if it's another hiking activity, we'll never get lost as long as we're all together!"
    toshu "心配する必要はありません。ハイキングの他の活動であれば、一緒にいる限り、迷うことはありません。"

# game/script.rpy:3867
translate japanese choice_8_end_f48ee291:

    ichiru "As long as I'm here, you won't be lost!"
    ichiru "私がここにいる限り、あなたは失われません！"

# game/script.rpy:3869
translate japanese choice_8_end_6f67ef44:

    # genji "EVERYONE READY?"
    genji "みんな、準備はいいか？"

# game/script.rpy:3871
translate japanese choice_8_end_9f58be3d:

    ichiru "H-hey it's been just a minute since you asked us to pack!"
    ichiru "H-ちょっとパックしてくれたからちょっと待ってたよ！"

# game/script.rpy:3873
translate japanese choice_8_end_e71df910:

    ichiru "Annoying old man!"
    ichiru "迷惑な老人！"

# game/script.rpy:3875
translate japanese choice_8_end_073dd789:

    # genji "Then hurry up, or we'll leave you guys behind!"
    genji "じゃあ急げ。さもないと置いてくぞ！"

# game/script.rpy:3877
translate japanese choice_8_end_ea64c543:

    # toshu "Y-yes, Coach!"
    toshu "は…はい、コーチ！"

# game/script.rpy:3879
translate japanese choice_8_end_37089951:

    toshu_t "Rushing things to be packed, the entire team marched onwards back to the forest."
    toshu_t "詰め物を急いで、チーム全体が森に戻って行進しました。"

# game/script.rpy:3880
translate japanese choice_8_end_c8e2d9b1:

    toshu_t "We were led by Coach Genji. His excitement while holding that map is so obvious."
    toshu_t "Genji教授が率いる。その地図を持っている間の彼の興奮はとても分かります。"

# game/script.rpy:3881
translate japanese choice_8_end_63b32f5a:

    toshu_t "I wonder where he'll bring us to?"
    toshu_t "彼が私たちをどこに連れて行くのだろうか？"

# game/script.rpy:3891
translate japanese choice_8_end_54e3bb94:

    # genji "MARCH! MARCH!"
    genji "そーれ、１・２・３・４！"

# game/script.rpy:3893
translate japanese choice_8_end_f4b0d62d:

    # ichiru "Gayji is acting bossy. He must be up to something again, I can feel it!"
    ichiru "Gayjiが偉そうにしてんな。アイツまた何か企んでやがるぜ、俺には分かる！"

# game/script.rpy:3895
translate japanese choice_8_end_a30235de:

    masaru "Let's just wait what Coach has prepared for us today."
    masaru "コーチが今日私たちのために用意しているものを待ってみましょう。"

# game/script.rpy:3897
translate japanese choice_8_end_70bb3373:

    # ichiru "Even if we're going to climb a cliff or something?"
    ichiru "崖登りをする羽目になっても？"

# game/script.rpy:3899
translate japanese choice_8_end_617ccdc1_1:

    # masaru "…"
    masaru "…"

# game/script.rpy:3901
translate japanese choice_8_end_215c0a63:

    # masaru "I hope not…"
    masaru "それはイヤだな…"

# game/script.rpy:3903
translate japanese choice_8_end_659dd307:

    toshu "H-hey, Ichiru don't do that. Captain's already traumatized from yesterday."
    toshu "H-ちょっと、Ichiruはそれをしないでください。キャプテンはすでに昨日から怪我をしています。"

# game/script.rpy:3905
translate japanese choice_8_end_3cd012d5:

    ichiru "That won't happen again as long as you have me, the great Ichiru, survival specialist!"
    ichiru "あなたが私を持っている限り、それは再び起こりません、偉大なIchiru、生存の専門家！"

# game/script.rpy:3907
translate japanese choice_8_end_486befc5:

    masaru "Where did you learn how to survive in the wild in the first place?"
    masaru "最初は野生で生き残る方法をどこで学びましたか？"

# game/script.rpy:3908
translate japanese choice_8_end_9912d351:

    masaru "Did you get special classes on that, Ichiru?"
    masaru "あなたはそれに特別なクラスを得ましたか？"

# game/script.rpy:3910
translate japanese choice_8_end_5cdac59d:

    ichiru "Aside from my experience with my parents! It's all common sense!"
    ichiru "私の両親との経験を除いて！それはすべての常識です！"

# game/script.rpy:3913
translate japanese choice_8_end_ace27a59:

    # ichiru "There are lots of things you can use to tell which way you are going."
    ichiru "方角を知る方法ってのはたくさんあるんだ。"

# game/script.rpy:3914
translate japanese choice_8_end_a09eef7c:

    # ichiru "Us, being provided with a map and a compass, It was really childsplay."
    ichiru "俺たちは地図とコンパスを持ってたんだから、あんなもんガキの遊びだよ。"

# game/script.rpy:3916
translate japanese choice_8_end_301e911a:

    # masaru "Hah…"
    masaru "ハハ…"

# game/script.rpy:3918
translate japanese choice_8_end_9739a372:

    toshu "W-wow!! Ichiru is such a genius!"
    toshu "Wワウ！イチルはそんな天才です！"

# game/script.rpy:3920
translate japanese choice_8_end_91c9c52b:

    ichiru "I KNOW I KNOW!"
    ichiru "分かった分かった！"

# game/script.rpy:3922
translate japanese choice_8_end_ed983dea:

    masaru "I really never figured out how that compass works…"
    masaru "そのコンパスの仕組みを本当に理解したことはありません…"

# game/script.rpy:3923
translate japanese choice_8_end_0364ba6d:

    masaru "It kept pointing north… so I go north…"
    masaru "それは北を指し続けたので…私は北に行く…"

# game/script.rpy:3925
translate japanese choice_8_end_f0cfc0c6:

    ichiru "Ouch! That's not how it works, Masaru!"
    ichiru "おお！それはどのように動作、元よりはありません！"

# game/script.rpy:3927
translate japanese choice_8_end_1d28a518:

    # genji "YOU! YOU GUYS ARE SLOWING DOWN!!"
    genji "お前ら！ 遅れてるぞ！！"

# game/script.rpy:3929
translate japanese choice_8_end_660d4d3a:

    # masaru "Y-yes Coach, we're coming!"
    masaru "は、はいコーチ。すいません！"

# game/script.rpy:3931
translate japanese choice_8_end_6b4d0314:

    # genji "HURRY UP, WE'RE ALMOST THERE!!"
    genji "急げ、ゴールはすぐそこだぞ！！"

# game/script.rpy:3933
translate japanese choice_8_end_61e899d1:

    ichiru "I wonder what that old man is so worked up about."
    ichiru "私はその老人が何のために働いているのだろうかと思います。"

# game/script.rpy:3934
translate japanese choice_8_end_4739a0b9:

    masaru "Come on guys, we're really getting left behind."
    masaru "みんな、私たちは本当に残っている。"

# game/script.rpy:3936
translate japanese choice_8_end_161462ce:

    # genji "HERE WE ARE!"
    genji "着いたぞ！"

# game/script.rpy:3946
translate japanese choice_8_end_d83aec49:

    toshu "W-WOOOOOOWWWW!!!!"
    toshu "W-WOOOOOOWWWW !!!!"

# game/script.rpy:3947
translate japanese choice_8_end_154cf11a:

    # toshu "A natural hot spring!!"
    toshu "天然の温泉だ！！"

# game/script.rpy:3948
translate japanese choice_8_end_689c2e58:

    masaru "Wow, what a breathtaking view!"
    masaru "うわー、なんて驚くべき景色！"

# game/script.rpy:3949
translate japanese choice_8_end_71295cec:

    genji "I purposely set this day to give you guys a relaxing time for all your hard work!"
    genji "私は故意に、皆さんの努力のためにリラックスタイムを与えるためにこの日を設定しました！"

# game/script.rpy:3950
translate japanese choice_8_end_6353d03c:

    ichiru "A-amazing… my parents never brought me to someplace like this before!"
    ichiru "驚くべきことに…私の両親はこれまでに私をどこかに連れて来なかった！"

# game/script.rpy:3952
translate japanese choice_8_end_5bbef2df:

    ichiru "I guess you're not an annoying old man after all, Genji~"
    ichiru "結局あなたは迷惑な老人じゃないと思う、元祖〜"

# game/script.rpy:3953
translate japanese choice_8_end_6069d0e7:

    toshu "I'm so excited!!! But, I left my goggles at home…!"
    toshu "私はとても興奮しています！！！しかし、私は家にゴーグルを残しました…！"

# game/script.rpy:3954
translate japanese choice_8_end_0c870bf6:

    masaru "Ahh… me too…"
    masaru "ああ…私も…"

# game/script.rpy:3956
translate japanese choice_8_end_124d67d9:

    ichiru "Are you two for real? This is a hot spring!"
    ichiru "あなたは本当の2人ですか？これは温泉です！"

# game/script.rpy:3957
translate japanese choice_8_end_98608cfc:

    ichiru "The moment you put it on, it will be covered by steam and you won't see a thing."
    ichiru "あなたが置いた瞬間、それは蒸気で覆われ、あなたは何も見ません。"

# game/script.rpy:3959
translate japanese choice_8_end_4a5272b6:

    toshu "Ahh…"
    toshu "ああ…"

# game/script.rpy:3961
translate japanese choice_8_end_3fc3352c:

    masaru "I'm just glad it's not another survival test."
    masaru "私は別の生存テストではないことをうれしく思います。"

# game/script.rpy:3963
translate japanese choice_8_end_c9d9af0d:

    genji "HAHAHA! I won't let my precious team go through that again!"
    genji "ははは！私は貴重なチームが再びそれを通過することはありません！"

# game/script.rpy:3964
translate japanese choice_8_end_d9cda6d6:

    genji "You all deserve this! Take it as everybody's prize for yesterday!!!"
    genji "あなたはすべてこれに値する！昨日皆様の賞金として貰いましょう！"

# game/script.rpy:3966
translate japanese choice_8_end_b714a0a7:

    toshu "Then what are we waiting for?"
    toshu "それから私たちは何を待っているのですか？"

# game/script.rpy:3968
translate japanese choice_8_end_7e1caaa6:

    ichiru "Let's jump in!"
    ichiru "飛び込みましょう！"

# game/script.rpy:3969
translate japanese choice_8_end_41d88437:

    toshu "YEAH!"
    toshu "よ！"

# game/script.rpy:3971
translate japanese choice_8_end_60215f6f:

    masaru "Let's get changed firs--"
    masaru "変わったばかを手に入れよう"

# game/script.rpy:3980
translate japanese choice_8_end_fb03cd9c:

    toshu "YEAAAAHHHH!!!"
    toshu "YEAAAAHHHH !!!"

# game/script.rpy:3981
translate japanese choice_8_end_6541591e:

    ichiru "Weeee!!"
    ichiru "Weeee !!"

# game/script.rpy:3991
translate japanese choice_8_end_cd41f99f:

    toshu "Fuwaaahh~"
    toshu "フワアア〜"

# game/script.rpy:3993
translate japanese choice_8_end_c9d84375:

    masaru "This is so relaxing…"
    masaru "これはとてもリラックスしています…"

# game/script.rpy:3995
translate japanese choice_8_end_169f64d7:

    # ichiru "Neh, neh Tofu!"
    ichiru "ねえねえ、Tofu！"

# game/script.rpy:3996
translate japanese choice_8_end_b5ba6a0f:

    # narrator "*SPLASH*"
    narrator "(バシャッ)"

# game/script.rpy:3998
translate japanese choice_8_end_fec3c0e9:

    # toshu "Ahh!! It's hot!!"
    toshu "うわっ！ 熱い！！"

# game/script.rpy:4000
translate japanese choice_8_end_0779b3d9:

    # masaru "Hey Ichiru, be careful, the water's hot remember?"
    masaru "おいIchiru、気を付けろ。これは熱湯なんだぞ？"

# game/script.rpy:4002
translate japanese choice_8_end_0564b1ea:

    # ichiru "Oops~!"
    ichiru "おおっと〜！"

# game/script.rpy:4003
translate japanese choice_8_end_e999a1e1:

    # narrator "*SPLASH* *SPLASH*"
    narrator "(バシャバシャッ)"

# game/script.rpy:4005
translate japanese choice_8_end_617ccdc1_2:

    # masaru "…"
    masaru "…"

# game/script.rpy:4007
translate japanese choice_8_end_745d4b33:

    # ichiru "Hahaha! Your faces are all red!!"
    ichiru "ははは！ 顔が真っ赤だぞ！"

# game/script.rpy:4009
translate japanese choice_8_end_1d93d4b2:

    # toshu "And yours look very happy."
    toshu "Ichiruは楽しそうだね。"

# game/script.rpy:4011
translate japanese choice_8_end_6ac68472:

    # ichiru "You bet I am!"
    ichiru "もちろん！"

# game/script.rpy:4013
translate japanese choice_8_end_2fd854a9:

    ichiru "This is the kind of field trip I was expecting!"
    ichiru "これは、私が期待していたフィールドトリップのようなものです！"

# game/script.rpy:4015
translate japanese choice_8_end_874d2943:

    masaru "You are too energetic, Ichiru."
    masaru "あなたはあまりにも元気です。"

# game/script.rpy:4017
translate japanese choice_8_end_5a695fee:

    # masaru "Why don't you try to sit down, relax, and enjoy the water…"
    masaru "ゆっくり座ってリラックスして、お湯を堪能したらどうだ…"

# game/script.rpy:4019
translate japanese choice_8_end_cbbc5fa2:

    # ichiru "BOOORING!"
    ichiru "そんなのつまんねーよ！"

# game/script.rpy:4021
translate japanese choice_8_end_79da3641:

    # masaru "Hngh…"
    masaru "ぐぬぬ…"

# game/script.rpy:4023
translate japanese choice_8_end_dd825868:

    # toshu_t "Everyone seem to be having fun!"
    toshu_t "みんな楽しそうにしてる！"

# game/script.rpy:4025
translate japanese choice_8_end_4ec37ee2:

    toshu_t "And… the fact that everyone is half naked…"
    toshu_t "そして…皆が半分裸であるという事実…"

# game/script.rpy:4026
translate japanese choice_8_end_24bc6cf1:

    toshu_t "It makes me feel--"
    toshu_t "それは私に気づかせる。"

# game/script.rpy:4028
translate japanese choice_8_end_efa494e6:

    # toshu_t "Hnghh! Stupid! Stupid!"
    toshu_t "うわあぁ！ バカ！ バカ！"

# game/script.rpy:4029
translate japanese choice_8_end_9b4e2b82:

    # toshu_t "I shouldn't be thinking this way!"
    toshu_t "そんなこと考えちゃダメだ！"

# game/script.rpy:4031
translate japanese choice_8_end_be040f75:

    # masaru "Hmm? What's wrong Toshu?"
    masaru "ん？ Toshu、どうかしたのか？"

# game/script.rpy:4033
translate japanese choice_8_end_4b394744:

    # ichiru "You sound awfully quiet, is there something wrong?"
    ichiru "やたら静かだけど、どうかしたのか？"

# game/script.rpy:4035
translate japanese choice_8_end_eea8c7ac:

    toshu_t "Crap! I've been talking to myself too much!"
    toshu_t "うんざり！私はあまりにも自分自身に話している！"

# game/script.rpy:4036
translate japanese choice_8_end_f3120623:

    # toshu_t "Quick, I gotta say something!"
    toshu_t "早く、何か言わなきゃ！"

# game/script.rpy:4038
translate japanese choice_8_end_abb9dc95:

    # toshu_t "I can't just say that I was thinking something inappropriate…!"
    toshu_t "エッチなこと考えてたなんて言えないよ…！"

# game/script.rpy:4061
translate japanese choice_9_A_7d445d6a:

    # masaru "Eh?"
    masaru "え？"

# game/script.rpy:4063
translate japanese choice_9_A_2943f9ca:

    # masaru "Do I look that old?"
    masaru "俺ってそんなに老けて見えるかな？"

# game/script.rpy:4065
translate japanese choice_9_A_80acdb9e:

    # toshu "N-no! You got it all wrong!"
    toshu "い、いいえ！ そういう意味じゃなくて！"

# game/script.rpy:4066
translate japanese choice_9_A_b1f46263:

    toshu "I meant that your body looks so defined… with all those muscles…"
    toshu "私は、あなたの体がそうしたように定義されていることを意味しています。"

# game/script.rpy:4068
translate japanese choice_9_A_7fca7fa7:

    # ichiru "I have muscles too!"
    ichiru "俺にも筋肉あるぜ！"

# game/script.rpy:4070
translate japanese choice_9_A_05ac1cab:

    toshu "But Captain Masaru's muscles are so big, look at his arms!"
    toshu "しかし、キャプテンマサルの筋肉はとても大きいので、彼の腕を見てください！"

# game/script.rpy:4072
translate japanese choice_9_A_4cae32f9:

    ichiru "Well yeah, I have to admit, his arms are lot bigger than mine…"
    ichiru "まあ、私は彼の腕が私よりもはるかに大きいことを認めなければならない…"

# game/script.rpy:4074
translate japanese choice_9_A_7bd9728b:

    ichiru "How come?! All you do is just catch a ball!"
    ichiru "どうして？！あなたがしているのはちょうどボールをキャッチすることです！"

# game/script.rpy:4075
translate japanese choice_9_A_9b6467ff:

    ichiru "I'm the clean-up hitter! Why my arms aren't that big?!"
    ichiru "私はクリーンアップのヒッターです！なぜ私の腕はそれほど大きくないのですか？"

# game/script.rpy:4077
translate japanese choice_9_A_79f36977:

    # masaru "Well, I do workout at home…"
    masaru "まあ、俺は家で筋トレしてるからな…"

# game/script.rpy:4078
translate japanese choice_9_A_cfd5c00d:

    masaru "If you want to have it, you should work out as well."
    masaru "あなたがそれを持っていたいなら、あなたも同様に取り組むべきです。"

# game/script.rpy:4080
translate japanese choice_9_A_81754035:

    ichiru "But I work out a lot with baseball too!"
    ichiru "でも、私は野球でもたくさん仕事をしています！"

# game/script.rpy:4082
translate japanese choice_9_A_ab574d8e:

    masaru "Maybe it's because you eat too much junk food Ichiru."
    masaru "あなたがジャンクフードイチールをあまりにも多く食べるからかもしれない。"

# game/script.rpy:4083
translate japanese choice_9_A_db26dc5b:

    masaru "Actually, you basically eat a lot."
    masaru "実際、あなたは基本的にたくさん食べています。"

# game/script.rpy:4085
translate japanese choice_9_A_b9e000a7:

    ichiru "Ugghhh… I suddenly feel fat…"
    ichiru "Ugghhh …私は突然、太った感じ…"

# game/script.rpy:4087
translate japanese choice_9_A_bb249500:

    toshu "Captain Masaru is having trouble scrubbing his back!"
    toshu "キャプテン・マサルが背中を擦れて問題を抱えている！"

# game/script.rpy:4088
translate japanese choice_9_A_891cf304:

    toshu "Should… I offer to help?"
    toshu "すべき…私は助けることを申し出ますか？"

# game/script.rpy:4090
translate japanese choice_9_A_c1dcf4d1:

    toshu "This is my chance to make it up to him!"
    toshu "これは彼にそれを作るチャンスです！"

# game/script.rpy:4091
translate japanese choice_9_A_63b3f4fd:

    toshu "Uhhmm… Captain?"
    toshu "Uhhmm …キャプテン？"

# game/script.rpy:4093
translate japanese choice_9_A_24640736:

    masaru "Yes, Toshu?"
    masaru "Yes, Toshu?"

# game/script.rpy:4095
translate japanese choice_9_A_8d77a850:

    toshu "Y-you look like you are having trouble!"
    toshu "あなたは問題を抱えているように見える！"

# game/script.rpy:4097
translate japanese choice_9_A_a648fe70:

    toshu "I… could scrub your back for you if you want!"
    toshu "私は…あなたが欲しいならば、あなたのためにあなたの背中をこすりつけることができます！"

# game/script.rpy:4099
translate japanese choice_9_A_3fc31a69:

    masaru "Oh, Please do!"
    masaru "ああ、してください！"

# game/script.rpy:4101
translate japanese choice_9_A_726aad47:

    ichiru "H-hey what about me?!"
    ichiru "名前：H-ねえ私はどうですか？"

# game/script.rpy:4107
translate japanese choice_9_B_43b177b2:

    ichiru "Huh? Where did that come from?"
    ichiru "ハァッ？それはどこから来ましたか？"

# game/script.rpy:4109
translate japanese choice_9_B_6f013bef:

    toshu "Ahh, well."
    toshu "ああ、そうですね。"

# game/script.rpy:4110
translate japanese choice_9_B_d23eef9f:

    toshu "I've always thought your hairstyle looks really cool."
    toshu "私はいつもあなたの髪型が本当にクールだと思った。"

# game/script.rpy:4112
translate japanese choice_9_B_2c32bf20:

    toshu "Even now that it's wet, it's still pointing up! Haha!"
    toshu "今でも濡れている、それはまだ指摘している！ハハ！"

# game/script.rpy:4114
translate japanese choice_9_B_2c70507f:

    ichiru "Tofu, you're making me blush!"
    ichiru "Tofu、あなたは私を赤面させている！"

# game/script.rpy:4116
translate japanese choice_9_B_7b12377f:

    masaru "That came out of nowhere, Toshu."
    masaru "それはどこからも出なかった。"

# game/script.rpy:4118
translate japanese choice_9_B_f5cdbf46:

    toshu "Ahh… sorry, I was spaced out 'cause the water is just really so relaxing."
    toshu "ああ…申し訳ありません、私は水がちょうど本当にとてもリラックスしているので、間隔を置いていました。"

# game/script.rpy:4120
translate japanese choice_9_B_953ae917:

    ichiru "Haha! Isn't Tofu cute?"
    ichiru "ハハ！Tofuはかわいいですか？"

# game/script.rpy:4122
translate japanese choice_9_B_b81a876a:

    ichiru "Oh! If you like my hair, maybe you can use my shampoo!"
    ichiru "ああ！あなたが私の髪が好きなら、多分あなたは私のシャンプーを使うことができます！"

# game/script.rpy:4124
translate japanese choice_9_B_ca0b1180:

    ichiru "Good thing I brought it."
    ichiru "私はそれを持ってきました。"

# game/script.rpy:4125
translate japanese choice_9_B_4f7b869b:

    ichiru "Sit down Tofu~"
    ichiru "座ってTofu〜"

# game/script.rpy:4127
translate japanese choice_9_B_ed3b04a6:

    ichiru "Here, let me wash your hair for you."
    ichiru "ここで、私はあなたのためにあなたの髪を洗ってみましょう。"

# game/script.rpy:4129
translate japanese choice_9_B_376dc6b0:

    toshu "Yes! Thank you Ichiru!"
    toshu "はい！イチールありがとう！"

# game/script.rpy:4135
translate japanese choice_9_C_7d445d6a:

    masaru "Eh?"
    masaru "え？"

# game/script.rpy:4137
translate japanese choice_9_C_a00b1066:

    toshu "Ahh I mean, when I grow up I'd like to have a body like yours!"
    toshu "ああ、私が成長すると、私はあなたのような体をしたいと思います！"

# game/script.rpy:4138
translate japanese choice_9_C_70a31ab6:

    toshu "Captain's body looks so defined… with all those muscles…"
    toshu "キャプテンの体はこのように定義されているように見えます。"

# game/script.rpy:4140
translate japanese choice_9_C_7fca7fa7:

    ichiru "I have muscles too!"
    ichiru "私も筋肉がある！"

# game/script.rpy:4142
translate japanese choice_9_C_05ac1cab:

    toshu "But Captain Masaru's muscles are so big, look at his arms!"
    toshu "しかし、キャプテンマサルの筋肉はとても大きいので、彼の腕を見てください！"

# game/script.rpy:4144
translate japanese choice_9_C_4cae32f9:

    ichiru "Well yeah, I have to admit, his arms are lot bigger than mine…"
    ichiru "まあ、私は彼の腕が私よりもはるかに大きいことを認めなければならない…"

# game/script.rpy:4146
translate japanese choice_9_C_7bd9728b:

    ichiru "How come?! All you do is just catch a ball!"
    ichiru "どうして？！あなたがしているのはちょうどボールをキャッチすることです！"

# game/script.rpy:4147
translate japanese choice_9_C_9b6467ff:

    ichiru "I'm the clean-up hitter! Why my arms aren't that big?!"
    ichiru "私はクリーンアップのヒッターです！なぜ私の腕はそれほど大きくないのですか？"

# game/script.rpy:4149
translate japanese choice_9_C_efe9283e:

    masaru "Ahh…"
    masaru "ああ…"

# game/script.rpy:4151
translate japanese choice_9_C_6ae92516:

    toshu "Ichiru and I are totally different…"
    toshu "イチルと私は全く違う…"

# game/script.rpy:4153
translate japanese choice_9_C_5f99ed12:

    ichiru "W-whoa, look at his back too!"
    ichiru "W-ウワ、彼の背中も見て！"

# game/script.rpy:4155
translate japanese choice_9_C_8d7f7259:

    toshu "Waah, there are muscles on the back too?"
    toshu "ああ、背中にも筋肉がありますか？"

# game/script.rpy:4157
translate japanese choice_9_C_1744be39:

    ichiru "How come I don't have those?!"
    ichiru "どうして私はそれらを持っていないのですか？"

# game/script.rpy:4159
translate japanese choice_9_C_31f0f05f:

    ichiru "Hmph!!"
    ichiru "Hmph !!"

# game/script.rpy:4161
translate japanese choice_9_C_79f36977:

    masaru "Well, I do workout at home…"
    masaru "まあ、私は自宅でエクササイズをしています…"

# game/script.rpy:4162
translate japanese choice_9_C_cfd5c00d:

    masaru "If you want to have it, you should work out as well."
    masaru "あなたがそれを持っていたいなら、あなたも同様に取り組むべきです。"

# game/script.rpy:4164
translate japanese choice_9_C_81754035:

    ichiru "But I work out a lot with baseball too!"
    ichiru "でも、私は野球でもたくさん仕事をしています！"

# game/script.rpy:4166
translate japanese choice_9_C_ab574d8e:

    masaru "Maybe it's because you eat too much junk food Ichiru."
    masaru "あなたがジャンクフードイチールをあまりにも多く食べるからかもしれない。"

# game/script.rpy:4167
translate japanese choice_9_C_db26dc5b:

    masaru "Actually, you basically eat a lot."
    masaru "実際、あなたは基本的にたくさん食べています。"

# game/script.rpy:4169
translate japanese choice_9_C_419c0495:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:4171
translate japanese choice_9_C_bd9918e3:

    ichiru "I'm not fat! Am I, Tofu?"
    ichiru "私は太ってない！私は、Tofuですか？"

# game/script.rpy:4173
translate japanese choice_9_C_4eeaf883:

    toshu "Umm… no?"
    toshu "うーん…いいえ？"

# game/script.rpy:4175
translate japanese choice_9_C_b9e000a7:

    ichiru "Ugghhh… I suddenly feel fat…"
    ichiru "Ugghhh …私は突然、太った感じ…"

# game/script.rpy:4177
translate japanese choice_9_C_b62b135d:

    toshu_t "Captain Masaru is having trouble scrubbing his back!"
    toshu_t "キャプテン・マサルが背中を擦れて問題を抱えている！"

# game/script.rpy:4178
translate japanese choice_9_C_4b87f4e2:

    toshu_t "Should… I offer to help?"
    toshu_t "すべき…私は助けることを申し出ますか？"

# game/script.rpy:4180
translate japanese choice_9_C_64f6425a:

    toshu_t "This is my chance to make it up to him!"
    toshu_t "これは彼にそれを作るチャンスです！"

# game/script.rpy:4181
translate japanese choice_9_C_63b3f4fd:

    toshu "Uhhmm… Captain?"
    toshu "Uhhmm …キャプテン？"

# game/script.rpy:4183
translate japanese choice_9_C_24640736:

    masaru "Yes, Toshu?"
    masaru "Yes, Toshu?"

# game/script.rpy:4185
translate japanese choice_9_C_5b62738e:

    toshu "You look like you are having trouble!"
    toshu "あなたは困っているように見えます！"

# game/script.rpy:4187
translate japanese choice_9_C_13624939:

    toshu "I… could scrub your back for you, if you want!"
    toshu "私は…あなたが欲しいのなら、あなたのためにあなたの背中を擦ることができます！"

# game/script.rpy:4189
translate japanese choice_9_C_3fc31a69:

    masaru "Oh, Please do!"
    masaru "ああ、してください！"

# game/script.rpy:4191
translate japanese choice_9_C_726aad47:

    ichiru "H-hey what about me?!"
    ichiru "名前：H-ねえ私はどうですか？"

# game/script.rpy:4196
translate japanese choice_9_D_43b177b2:

    ichiru "Huh? Where did that come from?"
    ichiru "ハァッ？それはどこから来ましたか？"

# game/script.rpy:4198
translate japanese choice_9_D_7b12377f:

    masaru "That came out of nowhere, Toshu."
    masaru "それはどこからも出なかった。"

# game/script.rpy:4200
translate japanese choice_9_D_4a5272b6:

    toshu "Ahh…"
    toshu "ああ…"

# game/script.rpy:4202
translate japanese choice_9_D_567c1414:

    ichiru "Hey! Hey! I just got an idea!"
    ichiru "ねえ！ねえ！私はちょっと考えました！"

# game/script.rpy:4204
translate japanese choice_9_D_4f7b869b:

    ichiru "Sit down Tofu~"
    ichiru "座ってTofu〜"

# game/script.rpy:4206
translate japanese choice_9_D_8140a627:

    ichiru "Here, Let me wash your hair for you."
    ichiru "ここで、私はあなたのためにあなたの髪を洗ってみましょう。"

# game/script.rpy:4208
translate japanese choice_9_D_73baef76:

    toshu "T-thank you, Ichiru!"
    toshu "T-ありがとう、Ichiru！"

# game/script.rpy:4217
translate japanese route_masaru_2_3b148d42:

    toshu "W-woww! It's a lot different than I thought!"
    toshu "Wワウ！それは私が思ったよりもはるかに異なっています！"

# game/script.rpy:4219
translate japanese route_masaru_2_89be4c38:

    masaru "What do you mean?"
    masaru "どういう意味ですか？"

# game/script.rpy:4221
translate japanese route_masaru_2_1ea6ea33:

    toshu "I thought your back would be as rough as rock, but it's actually smooth!"
    toshu "私はあなたの背中が岩のように荒いと思っていましたが、実際には滑らかです！"

# game/script.rpy:4223
translate japanese route_masaru_2_e5b4dc98:

    toshu "Even with all these muscles."
    toshu "これらのすべての筋肉でさえ。"

# game/script.rpy:4225
translate japanese route_masaru_2_319b94b4:

    # narrator "*scrub scrub*"
    narrator "(ゴシゴシ)"

# game/script.rpy:4227
translate japanese route_masaru_2_1a725357:

    # masaru "Hnn…"
    masaru "ん…"

# game/script.rpy:4229
translate japanese route_masaru_2_7ba27bcf:

    # toshu "Did Captain just moan?"
    toshu "痛かったですか？"

# game/script.rpy:4231
translate japanese route_masaru_2_72947667:

    # masaru "That feels great, Toshu."
    masaru "気持ちいいよ、Toshu。"

# game/script.rpy:4233
translate japanese route_masaru_2_c642ac63:

    toshu "Wahh! He actually likes it!"
    toshu "ワー！彼は実際にそれを好きです！"

# game/script.rpy:4235
translate japanese route_masaru_2_6ae7ea50:

    # masaru "This reminds when how Sohma scrubs my back."
    masaru "こうしてるとSohmaが背中を流してくれたのを思い出すな。"

# game/script.rpy:4237
translate japanese route_masaru_2_9ec0e181:

    # toshu "Sohma?"
    toshu "Sohma？"

# game/script.rpy:4239
translate japanese route_masaru_2_5ac73ae1:

    # masaru "Y-yeah, almost every day."
    masaru "ああ、ほとんど毎日。"

# game/script.rpy:4240
translate japanese route_masaru_2_36224582:

    masaru "We take baths together before going to school."
    masaru "私たちは学校に行く前にお風呂を一緒に取る。"

# game/script.rpy:4242
translate japanese route_masaru_2_ef1698a0:

    toshu_t "Woww! I kinda wish I was his little brother!"
    toshu_t "うわー！私はちょっと彼の弟だったらいい！"

# game/script.rpy:4243
translate japanese route_masaru_2_310c26f8:

    # toshu_t "Bathing together with him every day…"
    toshu_t "毎日一緒にお風呂に入るだなんて…"

# game/script.rpy:4245
translate japanese route_masaru_2_84dc5d84:

    # toshu_t "Must be fun!"
    toshu_t "羨ましい！"

# game/script.rpy:4247
translate japanese route_masaru_2_9699294b:

    toshu_t "Technically, I'm kinda bathing together with Captain right now!"
    toshu_t "技術的に、私は今キャプテンと一緒にちょっと入浴している！"

# game/script.rpy:4249
translate japanese route_masaru_2_f6ae7ff2:

    toshu_t "Ah!~ Captain Masaru is really sexy…!"
    toshu_t "ああ〜キャプテン大将は本当にセクシーです…！"

# game/script.rpy:4256
translate japanese route_masaru_2_c035961a:

    # masaru "Hm?"
    masaru "ん？"

# game/script.rpy:4258
translate japanese route_masaru_2_a7a3c1bd:

    # masaru "Something's poking my back…"
    masaru "何かが背中に当たってるぞ…"

# game/script.rpy:4260
translate japanese route_masaru_2_567e5f6e:

    # toshu "Oh no! I didn't notice how aroused I am!"
    toshu "ヤバい！ 勃ってるのに気付かなかった！"

# game/script.rpy:4262
translate japanese route_masaru_2_2c61dfc9:

    # toshu "S-sorry Captain I didn't mean to--"
    toshu "す、すいませんキャプテン。そういうつもりじゃ…"

# game/script.rpy:4264
translate japanese route_masaru_2_38784891:

    masaru "Haha! It's okay."
    masaru "ハハ！いいんだよ。"

# game/script.rpy:4266
translate japanese route_masaru_2_e441ac96:

    ichiru "BAAAH!"
    ichiru "BAAAH！"

# game/script.rpy:4267
translate japanese route_masaru_2_f9132a6c:

    ichiru "I want my turn!!! Toshu, scrub my back too!--"
    ichiru "私は私の回りが欲しい！ Toshu、あまりにも私の後ろをスクラブ！ -"

# game/script.rpy:4270
translate japanese route_masaru_2_0c4e1c3d:

    genji "HEEEY~~"
    genji "HEEEY ~~"

# game/script.rpy:4272
translate japanese route_masaru_2_f2219e96:

    genji "Whaddaya think about the hot springs here? Pretty neat huh?"
    genji "Whaddayaはここで温泉を考えますか？かなりきちんとしたね？"

# game/script.rpy:4274
translate japanese route_masaru_2_78fd936c:

    masaru "Yeah it is, Coach! It's very relaxing!"
    masaru "そうです、コーチ！とてもリラックスしています！"

# game/script.rpy:4276
translate japanese route_masaru_2_152ebebc:

    ichiru "Mind your own business GENJI!"
    ichiru "自分のビジネスに気をつけろ！"

# game/script.rpy:4278
translate japanese route_masaru_2_39e691c3:

    ichiru "And what the heck are you wearing?!"
    ichiru "そしてあなたは何を着ているのですか？"

# game/script.rpy:4280
translate japanese route_masaru_2_88311b3a:

    ichiru "HAHAHAHA!!!"
    ichiru "ハハハハ!!!"

# game/script.rpy:4282
translate japanese route_masaru_2_13b12c99:

    genji "Wah… why you little!"
    genji "ワウ…なぜあなたは少し！"

# game/script.rpy:4284
translate japanese route_masaru_2_6c5f3df1:

    # genji "It's called a fundoshi!"
    genji "これはフンドシというものだ！"

# game/script.rpy:4285
translate japanese route_masaru_2_ea7c2e04:

    genji "Wearing this raises my pride as a man!"
    genji "これを身に着けて、男として私の誇りを高める！"

# game/script.rpy:4287
translate japanese route_masaru_2_943b9ff1:

    genji "And I am proud to show off my manhood!"
    genji "私は私の男児を誇示することを誇りに思っています！"

# game/script.rpy:4288
translate japanese route_masaru_2_978565f0:

    genji "hahaha! You jealous?"
    genji "ははは！あなたは嫉妬？"

# game/script.rpy:4290
translate japanese route_masaru_2_951417e9:

    ichiru "Pff!! Oh my god, you really ARE the one and only GAYJI."
    ichiru "Pff !!ああ、私の神様、あなたは本当に唯一のゲイジです。"

# game/script.rpy:4291
translate japanese route_masaru_2_34618271:

    ichiru "Why would I be jealous of your 'Little Manhood'?"
    ichiru "なぜあなたの「少年少女」に嫉妬を感じるのですか？"

# game/script.rpy:4293
translate japanese route_masaru_2_fb4ebddf:

    genji "WHAT?! Mine's not little at all!"
    genji "何？！私のことは少しではありません！"

# game/script.rpy:4295
translate japanese route_masaru_2_60ca4f0e:

    ichiru "You mad, Gayji?"
    ichiru "あなたは怒っている、ゲイジ？"

# game/script.rpy:4297
translate japanese route_masaru_2_bac3c535:

    genji "Do you want me to prove it?"
    genji "私はそれを証明してもらいたいですか？"

# game/script.rpy:4299
translate japanese route_masaru_2_085d7268:

    genji "Why are you so mean to me… have I ever done anything bad to you?"
    genji "なぜあなたは私にそんなに意味があるのですか…私はあなたに悪いことをしたことがありますか？"

# game/script.rpy:4301
translate japanese route_masaru_2_ce154117:

    masaru "Give it up, Coach. You know Ichiru's just fooling around."
    masaru "あきらめる、コーチ。あなたはイチールがちょっとばかげているのを知っています。"

# game/script.rpy:4302
translate japanese route_masaru_2_eec84d2c:

    # masaru "I think that's how he shows his affection…?"
    masaru "一種の愛情表現だと思いますよ？"

# game/script.rpy:4304
translate japanese route_masaru_2_44a57afb:

    # ichiru "Definitely, not!"
    ichiru "んなこたねーよ！"

# game/script.rpy:4305
translate japanese route_masaru_2_fd6beb9d:

    # ichiru "In fact, I want to go somewhere else where I can't see this exhibitionist!"
    ichiru "てか、この露出狂が見えないところに行きてーよ！"

# game/script.rpy:4307
translate japanese route_masaru_2_1efa5227:

    # ichiru "C'mon Toshu!"
    ichiru "行こうぜ、Toshu！"

# game/script.rpy:4331
translate japanese choice_10_A_f6271e1f:

    ichiru "Hmph! Why won't you scrub my back too?"
    ichiru "Hmph！どうして私の背中もこすり洗いしないのですか？"

# game/script.rpy:4333
translate japanese choice_10_A_feba7ae9:

    ichiru "Okay, I guess I'll stay here and wait for my turn then!"
    ichiru "さて、私はここに滞在し、私の番を待つだろうと思う！"

# game/script.rpy:4335
translate japanese choice_10_A_d444e9e5:

    ichiru "Hmph!"
    ichiru "Hmph！"

# game/script.rpy:4340
translate japanese choice_10_B_9c779bef:

    ichiru "Ehh? Okay."
    ichiru "え？はい。"

# game/script.rpy:4342
translate japanese choice_10_B_d144bd7a:

    ichiru "As long as Toshu's here. I'll stay!"
    ichiru "Toshuがここにいる限り。私は滞在するつもりです！"

# game/script.rpy:4344
translate japanese choice_10_B_3504a43e:

    ichiru "If it weren't for that eyesore of a Coach!"
    ichiru "それがコーチの目の前でなかったら！"

# game/script.rpy:4354
translate japanese choice_10_C_2b6414e3:

    ichiru "He'll be fine!"
    ichiru "彼は元気でしょう！"

# game/script.rpy:4355
translate japanese choice_10_C_afcf654e:

    ichiru "He can handle himself!"
    ichiru "彼は自分自身を扱うことができます！"

# game/script.rpy:4356
translate japanese choice_10_C_bcacf943:

    toshu "But where are we going?"
    toshu "しかしどこへ行くの？"

# game/script.rpy:4357
translate japanese choice_10_C_e239cf7e:

    ichiru "I saw an awesome spot over there where we can have the place for ourselves!"
    ichiru "私はそこに私達が自分のために場所を持つことができる素晴らしいスポットを見ました！"

# game/script.rpy:4358
translate japanese choice_10_C_8e56d060:

    toshu "R-Really?"
    toshu "R-本当ですか？"

# game/script.rpy:4359
translate japanese choice_10_C_c70aa7f9:

    ichiru "Yup! Just follow me!"
    ichiru "うん！私についてきて！"

# game/script.rpy:4371
translate japanese choice_10_D_6ad584c6:

    toshu "Ohh… okay Ichiru."
    toshu "Ohh… okay Ichiru."

# game/script.rpy:4372
translate japanese choice_10_D_bcacf943:

    toshu "But where are we going?"
    toshu "しかしどこへ行くの？"

# game/script.rpy:4373
translate japanese choice_10_D_83018442:

    ichiru "I saw an awesome spot over there where that annoying old geezer won't see us!"
    ichiru "私はその厄介な古いオモチャが私たちを見ていないところに素晴らしいスポットを見ました！"

# game/script.rpy:4374
translate japanese choice_10_D_8e56d060:

    toshu "R-Really?"
    toshu "R-本当ですか？"

# game/script.rpy:4375
translate japanese choice_10_D_c70aa7f9:

    ichiru "Yup! Just follow me!"
    ichiru "うん！私についてきて！"

# game/script.rpy:4386
translate japanese route_ichiru_2_2d2e5a32:

    ichiru "You have a nice hair as well, Tofu!"
    ichiru "髪もいいですね、Tofu！"

# game/script.rpy:4388
translate japanese route_ichiru_2_50742a7e:

    toshu "No way, my hair is always a mess."
    toshu "私の髪はいつも混乱している。"

# game/script.rpy:4390
translate japanese route_ichiru_2_fbb5ca50:

    ichiru "I like it that way! It's short but puffy at the same time."
    ichiru "それが好き！それは同時に短くてふかふかです。"

# game/script.rpy:4392
translate japanese route_ichiru_2_fba941e1:

    toshu "Fuwaah~ It feels nice."
    toshu "Fuwaah〜いい感じです。"

# game/script.rpy:4393
translate japanese route_ichiru_2_5de3bde2:

    toshu "No one has ever shampooed my hair before."
    toshu "誰も前に私の髪をシャンプーしたことはありません。"

# game/script.rpy:4395
translate japanese route_ichiru_2_0ac4204d:

    ichiru "Really?"
    ichiru "本当に？"

# game/script.rpy:4397
translate japanese route_ichiru_2_71a26dd4:

    ichiru "My mom used to do this for me, I really like it!"
    ichiru "私のお母さんは私のためにこれをやっていましたが、本当に好きです！"

# game/script.rpy:4399
translate japanese route_ichiru_2_12dd3154:

    toshu "I like it too haha!"
    toshu "私はあまりにもハハが好きです！"

# game/script.rpy:4400
translate japanese route_ichiru_2_a0cde081:

    ichiru "You're so cute, TOFUUUU! How can you be so adorable!"
    ichiru "あなたはとてもかわいいです、TOFUUUU！どのようにあなたはとても愛らしいことができますか？"

# game/script.rpy:4402
translate japanese route_ichiru_2_319b94b4:

    narrator "*scrub scrub*"
    narrator "*スクラブスクラブ*"

# game/script.rpy:4404
translate japanese route_ichiru_2_4b7e28ab:

    toshu_t "Is it just me or Ichiru's treating me special compared to others?"
    toshu_t "それはちょうど私かIchiruは他の人に比べて特別な私の扱いですか？"

# game/script.rpy:4406
translate japanese route_ichiru_2_066f2b2d:

    toshu_t "Either way, I'm just glad we're really good friends."
    toshu_t "いずれにしても、私たちは本当に良い友達だとうれしいです。"

# game/script.rpy:4415
translate japanese route_ichiru_2_055a106c:

    masaru "Looks like you two are having fun."
    masaru "あなたは2人が楽しんでいるように見えます。"

# game/script.rpy:4416
translate japanese route_ichiru_2_62472355:

    ichiru "Of course we are~"
    ichiru "もちろん私たちは〜です"

# game/script.rpy:4417
translate japanese route_ichiru_2_2315e8a5:

    ichiru "Right, Toshu?"
    ichiru "Right, Toshu?"

# game/script.rpy:4419
translate japanese route_ichiru_2_98727b26:

    toshu "Yes!"
    toshu "はい！"

# game/script.rpy:4421
translate japanese route_ichiru_2_f712c486:

    masaru "Toshu is the only person I know that you've been extra nice to, Ichiru."
    masaru "Toshuは私が知っている唯一の人です。"

# game/script.rpy:4423
translate japanese route_ichiru_2_80d0a758:

    ichiru "Of course!"
    ichiru "もちろん！"

# game/script.rpy:4425
translate japanese route_ichiru_2_4b1e1aa9:

    ichiru "Who would be mean to such an adorable person?"
    ichiru "誰がそのような愛らしい人に意味がありますか？"

# game/script.rpy:4428
translate japanese route_ichiru_2_0c4e1c3d:

    genji "HEEEY~~"
    genji "HEEEY ~~"

# game/script.rpy:4430
translate japanese route_ichiru_2_f2219e96:

    genji "Whaddaya think about the hot springs here? Pretty neat huh?"
    genji "Whaddayaはここで温泉を考えますか？かなりきちんとしたね？"

# game/script.rpy:4432
translate japanese route_ichiru_2_78fd936c:

    masaru "Yeah it is, Coach! It's very relaxing!"
    masaru "そうです、コーチ！とてもリラックスしています！"

# game/script.rpy:4434
translate japanese route_ichiru_2_152ebebc:

    ichiru "Mind your own business GENJI!"
    ichiru "自分のビジネスに気をつけろ！"

# game/script.rpy:4436
translate japanese route_ichiru_2_39e691c3:

    ichiru "And what the heck are you wearing?!"
    ichiru "そしてあなたは何を着ているのですか？"

# game/script.rpy:4438
translate japanese route_ichiru_2_88311b3a:

    ichiru "HAHAHAHA!!!"
    ichiru "ハハハハ!!!"

# game/script.rpy:4440
translate japanese route_ichiru_2_13b12c99:

    genji "Wah… why you little!"
    genji "ワウ…なぜあなたは少し！"

# game/script.rpy:4442
translate japanese route_ichiru_2_6c5f3df1:

    genji "It's called a fundoshi!"
    genji "それは、資金援助と呼ばれています！"

# game/script.rpy:4443
translate japanese route_ichiru_2_ea7c2e04:

    genji "Wearing this raises my pride as a man!"
    genji "これを身に着けて、男として私の誇りを高める！"

# game/script.rpy:4445
translate japanese route_ichiru_2_943b9ff1:

    genji "And I am proud to show off my manhood!"
    genji "私は私の男児を誇示することを誇りに思っています！"

# game/script.rpy:4446
translate japanese route_ichiru_2_978565f0:

    genji "hahaha! You jealous?"
    genji "ははは！あなたは嫉妬？"

# game/script.rpy:4448
translate japanese route_ichiru_2_951417e9:

    ichiru "Pff!! Oh my god, you really ARE the one and only GAYJI."
    ichiru "Pff !!ああ、私の神様、あなたは本当に唯一のゲイジです。"

# game/script.rpy:4449
translate japanese route_ichiru_2_34618271:

    ichiru "Why would I be jealous of your 'Little Manhood'?"
    ichiru "なぜあなたの「少年少女」に嫉妬を感じるのですか？"

# game/script.rpy:4451
translate japanese route_ichiru_2_fb4ebddf:

    genji "WHAT?! Mine's not little at all!"
    genji "何？！私のことは少しではありません！"

# game/script.rpy:4453
translate japanese route_ichiru_2_60ca4f0e:

    ichiru "You mad, Gayji?"
    ichiru "あなたは怒っている、ゲイジ？"

# game/script.rpy:4455
translate japanese route_ichiru_2_bac3c535:

    genji "Do you want me to prove it?"
    genji "私はそれを証明してもらいたいですか？"

# game/script.rpy:4457
translate japanese route_ichiru_2_085d7268:

    genji "Why are you so mean to me… have I ever done anything bad to you?"
    genji "なぜあなたは私にそんなに意味があるのですか…私はあなたに悪いことをしたことがありますか？"

# game/script.rpy:4459
translate japanese route_ichiru_2_ce154117:

    masaru "Give it up, Coach. You know Ichiru's just fooling around."
    masaru "あきらめる、コーチ。あなたはイチールがちょっとばかげているのを知っています。"

# game/script.rpy:4460
translate japanese route_ichiru_2_eec84d2c:

    masaru "I think that's how he shows his affection…?"
    masaru "私は彼が彼の愛情を示す方法だと思う？"

# game/script.rpy:4462
translate japanese route_ichiru_2_44a57afb:

    ichiru "Definitely, not!"
    ichiru "絶対にありません！"

# game/script.rpy:4463
translate japanese route_ichiru_2_fd6beb9d:

    ichiru "In fact, I want to go somewhere else where I can't see this exhibitionist!"
    ichiru "実際、私はこの展覧会家が見えないどこかに行きたいです！"

# game/script.rpy:4465
translate japanese route_ichiru_2_1efa5227:

    ichiru "C'mon Toshu!"
    ichiru "C'mon Toshu!"

# game/script.rpy:4489
translate japanese choice_11_A_2715b03d:

    ichiru "Ughh… fine…"
    ichiru "うーん…うーん…"

# game/script.rpy:4490
translate japanese choice_11_A_337345fb:

    ichiru "Okay, I guess, I'll stay here and wait then."
    ichiru "さて、私はここに滞在して待っています。"

# game/script.rpy:4492
translate japanese choice_11_A_c384bf25:

    ichiru "Hmph! Stupid Genji!"
    ichiru "Hmph！愚かなGenji！"

# game/script.rpy:4497
translate japanese choice_11_B_b45f4991:

    ichiru "Ehh… well… okay!"
    ichiru "ええ…まあ…大丈夫！"

# game/script.rpy:4499
translate japanese choice_11_B_bfdd9ad9:

    ichiru "I'll stay where Toshu stays!"
    ichiru "私はToshuが残るところにとどまります！"

# game/script.rpy:4501
translate japanese choice_11_B_3504a43e:

    ichiru "If it weren't for that eyesore of a Coach!"
    ichiru "それがコーチの目の前でなかったら！"

# game/script.rpy:4511
translate japanese choice_11_C_2b6414e3:

    ichiru "He'll be fine!"
    ichiru "彼は元気でしょう！"

# game/script.rpy:4512
translate japanese choice_11_C_afcf654e:

    ichiru "He can handle himself!"
    ichiru "彼は自分自身を扱うことができます！"

# game/script.rpy:4513
translate japanese choice_11_C_bcacf943:

    toshu "But where are we going?"
    toshu "しかしどこへ行くの？"

# game/script.rpy:4514
translate japanese choice_11_C_d538cc73:

    ichiru "A I saw an awesome spot over there where we can have the place for ourselves!"
    ichiru "AIは私たちが自分のためにその場所を持つことができる素晴らしい場所を見ました！"

# game/script.rpy:4515
translate japanese choice_11_C_8e56d060:

    toshu "R-Really?"
    toshu "R-本当ですか？"

# game/script.rpy:4516
translate japanese choice_11_C_c70aa7f9:

    ichiru "Yup! Just follow me!"
    ichiru "うん！私についてきて！"

# game/script.rpy:4528
translate japanese choice_11_D_6ad584c6:

    toshu "Ohh… okay Ichiru."
    toshu "Ohh… okay Ichiru."

# game/script.rpy:4529
translate japanese choice_11_D_bcacf943:

    toshu "But where are we going?"
    toshu "しかしどこへ行くの？"

# game/script.rpy:4530
translate japanese choice_11_D_83018442:

    ichiru "I saw an awesome spot over there where that annoying old geezer won't see us!"
    ichiru "私はその厄介な古いオモチャが私たちを見ていないところに素晴らしいスポットを見ました！"

# game/script.rpy:4531
translate japanese choice_11_D_8e56d060:

    toshu "R-Really?"
    toshu "R-本当ですか？"

# game/script.rpy:4532
translate japanese choice_11_D_c70aa7f9:

    ichiru "Yup! Just follow me!"
    ichiru "うん！私についてきて！"

# game/script.rpy:4540
translate japanese route_masaru_3_c37df7e9:

    masaru "Ichiru, calm down, we're here to relax."
    masaru "イチール、落ち着いて、ここにはリラックスしています。"

# game/script.rpy:4542
translate japanese route_masaru_3_fa1e12de:

    toshu "Why do you hate Coach that much, Ichiru?"
    toshu "あなたはコーチをそんなに嫌うのはなぜですか？Ichiru？"

# game/script.rpy:4544
translate japanese route_masaru_3_4a42c39c:

    genji "Yeah… what did I ever do to you?!"
    genji "うん…私はあなたに何をしたのですか？"

# game/script.rpy:4546
translate japanese route_masaru_3_f68f6875:

    genji "Can't you be nice to me just once?"
    genji "あなたは一度私に素敵なことができませんか？"

# game/script.rpy:4548
translate japanese route_masaru_3_518ed9bf:

    ichiru "J-just leave me alone, stupid Genji…"
    ichiru "Jちょうど私を、愚かなGenjiのままに…"

# game/script.rpy:4550
translate japanese route_masaru_3_17fdce3d:

    masaru "Ichiru… I thought you respect Coach more?"
    masaru "イチル…コーチをもっと尊敬すると思った？"

# game/script.rpy:4552
translate japanese route_masaru_3_903c3a83:

    genji "Yeahh…"
    genji "うん…"

# game/script.rpy:4554
translate japanese route_masaru_3_067c9744:

    genji "Oh, I know! C'mere Ichiru!"
    genji "ああ、わかる！ IchiruさんC\'mere！"

# game/script.rpy:4556
translate japanese route_masaru_3_d6d9448b:

    genji "Maybe you need some quality time with me."
    genji "たぶんあなたは私とある程度の質の高い時間が必要です。"

# game/script.rpy:4565
translate japanese route_masaru_3_a3016df9:

    ichiru "H-HEY!! PUT ME DOWN YOU PERVERTED COACH!"
    ichiru "こんにちは！あなたはコーチを過ぎてしまった！"

# game/script.rpy:4566
translate japanese route_masaru_3_d792f2cb:

    ichiru "Where are you gonna take me?!"
    ichiru "どこで私を連れて行くの？"

# game/script.rpy:4567
translate japanese route_masaru_3_c1fc7f1f:

    genji "C'mon! Give me a chance to prove that I'm not what you say I am!"
    genji "さあ！私にあなたが言うことではないことを証明するチャンスを与えてください！"

# game/script.rpy:4576
translate japanese route_masaru_3_ea9c1d50:

    toshu "I'm worried about Ichiru."
    toshu "Ichiruが心配です。"

# game/script.rpy:4577
translate japanese route_masaru_3_3e37aa47:

    toshu "He sounds pretty mad."
    toshu "彼はかなり怒っているようだ。"

# game/script.rpy:4579
translate japanese route_masaru_3_34873a23:

    masaru "I'm more worried about Coach, though."
    masaru "私はコーチについてもっと心配しています。"

# game/script.rpy:4581
translate japanese route_masaru_3_f9cfde47:

    masaru "Ichiru is extra mean to Coach for some reason."
    masaru "イチールは何らかの理由でコーチに特別な意味がある。"

# game/script.rpy:4582
translate japanese route_masaru_3_1f5824bc:

    masaru "Good thing Coach is such a good sport."
    masaru "良いことコーチはとても良いスポーツです。"

# game/script.rpy:4584
translate japanese route_masaru_3_6f5cf0cb:

    toshu "Ahh… Captain…"
    toshu "ああ…キャプテン…"

# game/script.rpy:4586
translate japanese route_masaru_3_8d772cef:

    masaru "Yeah?"
    masaru "うん？"

# game/script.rpy:4588
translate japanese route_masaru_3_d4999f56:

    toshu "Thanks for letting me scrub you back a while ago…!"
    toshu "しばらく前に私をあなたに洗い流してくれてありがとう！"

# game/script.rpy:4590
translate japanese route_masaru_3_60aef151:

    masaru "I should be the one thanking you!"
    masaru "私はあなたに感謝しているはずです！"

# game/script.rpy:4592
translate japanese route_masaru_3_346636b4:

    masaru "It felt really great!"
    masaru "それは本当に素晴らしい感じでした！"

# game/script.rpy:4594
translate japanese route_masaru_3_7e64c344:

    toshu "I-it's no problem, Captain!"
    toshu "私はそれは問題ではない、キャプテン！"

# game/script.rpy:4599
translate japanese route_masaru_3_22721b83:

    masaru "You are really adorable, you know…"
    masaru "あなたは本当に愛らしい、あなたが知っている…"

# game/script.rpy:4601
translate japanese route_masaru_3_7906a0db:

    masaru "…and I noticed you were aroused before."
    masaru "…そして、私はあなたが以前に興奮していたことに気づいた。"

# game/script.rpy:4603
translate japanese route_masaru_3_94a258b4:

    masaru "That was really cute."
    masaru "それは本当にかわいかったです。"

# game/script.rpy:4605
translate japanese route_masaru_3_fc343b46:

    masaru "It's enough to… well…"
    masaru "それは十分です…まあ…"

# game/script.rpy:4607
translate japanese route_masaru_3_419097e0:

    masaru "Let's just say that, maybe I should repay the pleasure you gave me."
    masaru "あなたが私に与えた喜びを返すべきかもしれないと言ってみましょう。"

# game/script.rpy:4609
translate japanese route_masaru_3_0a750bbb:

    toshu "W-what do you mean Captain?"
    toshu "キャプテンって何？"

# game/script.rpy:4618
translate japanese route_masaru_3_7f054211:

    toshu "And… there they go…"
    toshu "そして…彼らはそこに行く…"

# game/script.rpy:4619
translate japanese route_masaru_3_f2406ccd:

    masaru "You and Ichiru get along pretty well."
    masaru "あなたとイチルはかなりうまくやって来ます。"

# game/script.rpy:4620
translate japanese route_masaru_3_56e67668:

    toshu "Yes, Captain! Ichiru is very nice and funny!"
    toshu "はい、キャプテン！イチルはとても素敵で面白いです！"

# game/script.rpy:4622
translate japanese route_masaru_3_aea0e007:

    masaru "Umm… Toshu…"
    masaru "Umm… Toshu…"

# game/script.rpy:4623
translate japanese route_masaru_3_53f02544:

    masaru "Since we are alone…"
    masaru "我々は一人でいるので…"

# game/script.rpy:4624
translate japanese route_masaru_3_2b7e3ba9:

    masaru "I wanted to tell you something."
    masaru "私はあなたに何かを伝えたいと思った。"

# game/script.rpy:4626
translate japanese route_masaru_3_a653cf16:

    toshu "Yes, Captain?"
    toshu "はい、キャプテン？"

# game/script.rpy:4628
translate japanese route_masaru_3_6f073f08:

    masaru "I'm really glad to have met you!"
    masaru "私はあなたに会って本当にうれしいです！"

# game/script.rpy:4629
translate japanese route_masaru_3_e833cc2d:

    masaru "The moment I caught that ball for you, I felt we'd be really great friends!"
    masaru "私はあなたのためにボールをキャッチした瞬間、私は本当に素晴らしい友達だと感じました！"

# game/script.rpy:4631
translate japanese route_masaru_3_db2f096b:

    toshu "Ohh… why suddenly tell that Captain?"
    toshu "オハイ…なぜ突然キャプテン？"

# game/script.rpy:4633
translate japanese route_masaru_3_4b836b8d:

    masaru "I had the feeling of wanting to protect you…"
    masaru "私はあなたを守る気持ちがあった…"

# game/script.rpy:4634
translate japanese route_masaru_3_e8f0f79a:

    masaru "But I've disappointed you with how I failed that hiking activity…"
    masaru "しかし、私はそのハイキング活動に失敗したことにあなたを失望させました…"

# game/script.rpy:4636
translate japanese route_masaru_3_5e11d48b:

    toshu "Captain, How many times do we need to tell you to not worry about that?"
    toshu "キャプテン、心配しないように何回言わなければならないのですか？"

# game/script.rpy:4637
translate japanese route_masaru_3_6feb2476:

    toshu "Everyone has their own strengths and weaknesses."
    toshu "誰もが自分の強みと弱みを持っています。"

# game/script.rpy:4642
translate japanese route_masaru_3_b97f4f30:

    masaru "You are really admirable, you know…"
    masaru "あなたは本当に素晴らしいですね、あなたは知っています…"

# game/script.rpy:4644
translate japanese route_masaru_3_fc343b46_1:

    masaru "It's enough to… well…"
    masaru "それは十分です…まあ…"

# game/script.rpy:4646
translate japanese route_masaru_3_0a750bbb_1:

    toshu "W-what do you mean Captain?"
    toshu "キャプテンって何？"

# game/script.rpy:4658
translate japanese route_ichiru_3_a59fbbaa:

    ichiru "Gosh, that Genji is really annoying."
    ichiru "まあ、Genjiは本当に迷惑だ。"

# game/script.rpy:4659
translate japanese route_ichiru_3_d3584530:

    ichiru "He's ruining the mood!"
    ichiru "彼は気分を台無しにしている！"

# game/script.rpy:4661
translate japanese route_ichiru_3_d444e9e5:

    ichiru "Hmph!"
    ichiru "Hmph！"

# game/script.rpy:4663
translate japanese route_ichiru_3_455abc70:

    toshu "I don't think he is, Ichiru."
    toshu "私は彼が、Ichiruとは思わない。"

# game/script.rpy:4665
translate japanese route_ichiru_3_314d8752:

    toshu "By the way, is it really okay to leave Captain Masaru alone there?"
    toshu "ちなみに、マサール大尉をそこに一人でお迎えするのは本当に大丈夫ですか？"

# game/script.rpy:4669
translate japanese route_ichiru_3_419c0495:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:4671
translate japanese route_ichiru_3_8bf3a2d8:

    toshu "Eh? Did I say something wrong?"
    toshu "え？私は間違ったことを言ったのですか？"

# game/script.rpy:4673
translate japanese route_ichiru_3_f4bd7a77:

    ichiru "Why so worried about him?"
    ichiru "なぜ彼について心配したのですか？"

# game/script.rpy:4674
translate japanese route_ichiru_3_5ae51a67:

    ichiru "Genji is with him. He won't feel left out!"
    ichiru "Genjiは彼と一緒です。彼は脱いだと感じません！"

# game/script.rpy:4676
translate japanese route_ichiru_3_3ef6ed0a:

    ichiru "Can't we have time together? Just two of us?"
    ichiru "一緒に時間を過ごすことはできないのですか？私たちの二人だけ？"

# game/script.rpy:4678
translate japanese route_ichiru_3_d13cc1cf:

    toshu "Ohh… if you say so."
    toshu "ああ…あなたがそう言ったら。"

# game/script.rpy:4680
translate japanese route_ichiru_3_d240cdec:

    ichiru "You…"
    ichiru "君は…"

# game/script.rpy:4681
translate japanese route_ichiru_3_6d5d65b6:

    ichiru "You looked so happy, scrubbing Masaru's back."
    ichiru "あなたはとても幸せそうに見えます。"

# game/script.rpy:4683
translate japanese route_ichiru_3_cf469003:

    toshu "Ahh that's…"
    toshu "ああ、それは…"

# game/script.rpy:4685
translate japanese route_ichiru_3_29151af0:

    toshu "I'll scrub your back right now if you want!"
    toshu "私はあなたが欲しいならば、今あなたの背中をこすります！"

# game/script.rpy:4687
translate japanese route_ichiru_3_44b1eaad:

    ichiru "T-that's not what I mean…"
    ichiru "T-それは私が意味するものではありません…"

# game/script.rpy:4688
translate japanese route_ichiru_3_ba87d44e:

    ichiru "Seeing you having fun with Masaru…"
    ichiru "あなたがMasaruと楽しんでいるのを見て…"

# game/script.rpy:4690
translate japanese route_ichiru_3_f851a147:

    ichiru "It made me so jealous."
    ichiru "それは私をとても嫉妬にさせた。"

# game/script.rpy:4692
translate japanese route_ichiru_3_1c9b985b:

    ichiru "I sound so selfish, don't I?"
    ichiru "私はそんなに利己的に聞こえる、そうではない？"

# game/script.rpy:4694
translate japanese route_ichiru_3_f318b797:

    toshu "N-no!"
    toshu "いいえ！"

# game/script.rpy:4696
translate japanese route_ichiru_3_7bccde20:

    ichiru "J-just…! Don't make me feel jealous when he's around."
    ichiru "Jちょうど…！彼が周りにいるとき私が嫉妬を感じさせないでください。"

# game/script.rpy:4697
translate japanese route_ichiru_3_8bedc838:

    ichiru "I'm sorry if I am suddenly saying this to you now…"
    ichiru "私は突然あなたにこれを今言っていると申し訳ありません…"

# game/script.rpy:4699
translate japanese route_ichiru_3_37ef545c:

    ichiru "I- I just wanted to let you know how much I enjoy being with you!"
    ichiru "私はちょうど私があなたと一緒にいることをどれだけ楽しんでいるかを伝えたいと思っていました！"

# game/script.rpy:4704
translate japanese route_ichiru_3_b93f89ec:

    ichiru "So much that… I want to…"
    ichiru "そんなに… …私はしたい…"

# game/script.rpy:4709
translate japanese route_ichiru_3_216da5f0:

    ichiru "Yeah of course. He can handle himself, he's already old."
    ichiru "ええ、もちろん。彼は自分自身を扱うことができる、彼はすでに古いです。"

# game/script.rpy:4711
translate japanese route_ichiru_3_fa7955ef:

    ichiru "ANYWAY!!~~ TOFUUU!!~"
    ichiru "いつも!! ~~ TOFUUU !!〜"

# game/script.rpy:4713
translate japanese route_ichiru_3_40ba4168:

    toshu "!!?"
    toshu "!!？"

# game/script.rpy:4715
translate japanese route_ichiru_3_007c35ee:

    ichiru "I'm really happy you let me wash your hair like that!"
    ichiru "私はあなたが私のようなあなたの髪を洗わせて本当にうれしいです！"

# game/script.rpy:4717
translate japanese route_ichiru_3_b3c68a05:

    toshu "Me too! Thank you so much for doing that! It felt really nice!"
    toshu "私も！それをしてくれてありがとう！それは本当に素晴らしかった！"

# game/script.rpy:4719
translate japanese route_ichiru_3_2ab8792d:

    ichiru "It's… it's nothing…!"
    ichiru "それは…何もない…！"

# game/script.rpy:4720
translate japanese route_ichiru_3_68bb687d:

    ichiru "Toshu's really cute after all!"
    ichiru "結局トーシュは本当にかわいい！"

# game/script.rpy:4722
translate japanese route_ichiru_3_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:4724
translate japanese route_ichiru_3_8bedc838_1:

    ichiru "I'm sorry if I am suddenly saying this to you now…"
    ichiru "私は突然あなたにこれを今言っていると申し訳ありません…"

# game/script.rpy:4726
translate japanese route_ichiru_3_ae56539b:

    ichiru "I-I just wanted to let you know how much I enjoy being with you!"
    ichiru "私はどれくらい楽しんでいるのか教えてくれました。"

# game/script.rpy:4731
translate japanese route_ichiru_3_b93f89ec_1:

    ichiru "So much that… I want to…"
    ichiru "そんなに… …私はしたい…"

# game/script.rpy:4739
translate japanese route_masaru_4_0f8d9c20:

    toshu_t "Those words kept repeating into my head…"
    toshu_t "それらの言葉は私の頭の中に繰り返され続けた…"

# game/script.rpy:4740
translate japanese route_masaru_4_41a32b10:

    toshu_t "It was something I really thought would exist in my fantasies."
    toshu_t "私の幻想に本当に存在すると思っていたものでした。"

# game/script.rpy:4741
translate japanese route_masaru_4_7e4cc8cf:

    toshu_t "I ended up agreeing to what Captain has suggested us to do."
    toshu_t "キャプテンが私たちに提案したことに同意した。"

# game/script.rpy:4746
translate japanese route_masaru_4_11f00a99:

    toshu "Hnn… C-Captain!"
    toshu "Hnn … C-キャプテン！"

# game/script.rpy:4748
translate japanese route_masaru_4_c77633be:

    # masaru "Relax, Toshu."
    masaru "力を抜いて、Toshu。"

# game/script.rpy:4749
translate japanese route_masaru_4_ec5f909c:

    # toshu "O-okay…"
    toshu "は、はい…"

# game/script.rpy:4751
translate japanese route_masaru_4_806ee063:

    # toshu "Hnn… I-it feels good, Captain…"
    toshu "んん…キャプテン、き…気持ちいい…"

# game/script.rpy:4752
translate japanese route_masaru_4_d80ef79a:

    # toshu "C-Captain! I can't hold it anymore!"
    toshu "キャ、キャプテン！ 僕もう我慢できないです！"

# game/script.rpy:4754
translate japanese route_masaru_4_98775339:

    # toshu "A-Ahh!!"
    toshu "あ、ああっ！！"

# game/script.rpy:4755
translate japanese route_masaru_4_15d3a941:

    # masaru "Hnn!"
    masaru "んんっ！"

# game/script.rpy:4757
translate japanese route_masaru_4_81b536eb:

    # toshu "Haa-haa…"
    toshu "はぁ、はぁ…"

# game/script.rpy:4758
translate japanese route_masaru_4_514483f0:

    # masaru "Nnnh…"
    masaru "んん…"

# game/script.rpy:4760
translate japanese route_masaru_4_62ae63f6:

    # toshu "I really felt good Captain…"
    toshu "すごく気持ちよかったです、キャプテン…"

# game/script.rpy:4761
translate japanese route_masaru_4_cef53903:

    # masaru "Yeah… me too…"
    masaru "ああ、俺もだ…"

# game/script.rpy:4763
translate japanese route_masaru_4_0eb32704:

    toshu_t "Never in my mind crossed that I would do something like that with Captain Masaru."
    toshu_t "私の心の中で、大佐大佐と同じことをすることは決してありませんでした。"

# game/script.rpy:4764
translate japanese route_masaru_4_e449339b:

    toshu_t "I think… what I'm feeling… is not so simple after all…"
    toshu_t "私は思う…私が感じていることは…結局のところ単純ではない…"

# game/script.rpy:4765
translate japanese route_masaru_4_f9a6dfeb:

    toshu_t "I'm so happy I was able to do that with Captain."
    toshu_t "私はキャプテンと一緒にそれをすることができたことにとても満足しています。"

# game/script.rpy:4766
translate japanese route_masaru_4_00896d11:

    toshu_t "I need to get to know more about him!"
    toshu_t "私は彼についてもっと知ることが必要です！"

# game/script.rpy:4777
translate japanese route_masaru_4_nosex_e95c253f:

    ichiru "HEYY!! Guys!!"
    ichiru "HEYY !!みんな！"

# game/script.rpy:4779
translate japanese route_masaru_4_nosex_484ed306:

    masaru "Oh, Ichiru."
    masaru "Oh, Ichiru."

# game/script.rpy:4781
translate japanese route_masaru_4_nosex_bb5cdb1a:

    toshu "Hi Ichiru, What happened with you and Coach?"
    toshu "こんにちは、あなたとコーチはどうでしたか？"

# game/script.rpy:4783
translate japanese route_masaru_4_nosex_e33e9690:

    ichiru "I ditched that perverted old geezer!"
    ichiru "私はその変態した古いオタクを捨てた！"

# game/script.rpy:4785
translate japanese route_masaru_4_nosex_bc02811b:

    ichiru "Glad I was able to escape from him!"
    ichiru "うれしい私は彼から逃れることができた！"

# game/script.rpy:4787
translate japanese route_masaru_4_nosex_b3108a87:

    ichiru "That STUPID Coach is so annoying!"
    ichiru "その愚かなコーチはとても迷惑です！"

# game/script.rpy:4789
translate japanese route_masaru_4_nosex_5ca6870f:

    toshu "At least you're back here!"
    toshu "少なくともあなたはここに戻ります！"

# game/script.rpy:4791
translate japanese route_masaru_4_nosex_5332e063:

    ichiru "Anyway~ Toshu! Where's that back scrubbing you promised?!"
    ichiru "とにかく〜Toshu！あなたが約束した背中がどこにあるのですか？"

# game/script.rpy:4793
translate japanese route_masaru_4_nosex_9dd71bb8:

    genji "HEY! ICHIRU!"
    genji "HEY! ICHIRU!"

# game/script.rpy:4795
translate japanese route_masaru_4_nosex_875f23a3:

    ichiru "HNGH!! YOU OLD PRICK! STOP FOLLOWING ME!!"
    ichiru "HNGH !!あなたは古いPRICK！付いて来ないでください！！"

# game/script.rpy:4797
translate japanese route_masaru_4_nosex_d05d97d3:

    genji "Haiizzz…"
    genji "Haiizzz …"

# game/script.rpy:4799
translate japanese route_masaru_4_nosex_61f82d00:

    genji "I think we overstayed here at the hot springs. We should start packing."
    genji "私はここで温泉で過ごしたと思う。私たちは梱包を開始するべきです。"

# game/script.rpy:4801
translate japanese route_masaru_4_nosex_cb16b663:

    masaru "Ah… yeah I didn't notice its almost sundown."
    masaru "ああ…ええ、私はそのほとんどの日没に気付かなかった。"

# game/script.rpy:4803
translate japanese route_masaru_4_nosex_7b768304:

    ichiru "Whattt!!! But I haven't gotten my back scrubbing from Toshu!!!"
    ichiru "Whattt !!!しかし、私はToshuから私の背中のスクラブを得ていない！"

# game/script.rpy:4805
translate japanese route_masaru_4_nosex_041bce8b:

    ichiru "It's all your fault, you stupid Coach!"
    ichiru "それはあなたのすべての欠陥です、あなたは愚かなコーチです！"

# game/script.rpy:4807
translate japanese route_masaru_4_nosex_6608fcda:

    toshu "I-ichiru, we should really get prepared."
    toshu "私は、私たちは本当に準備する必要があります。"

# game/script.rpy:4809
translate japanese route_masaru_4_nosex_bd840406:

    toshu "We can do it some other time."
    toshu "我々は別の時間にそれをすることができます。"

# game/script.rpy:4810
translate japanese route_masaru_4_nosex_75e511ab:

    ichiru "There won't be another time…"
    ichiru "別の時間はありません…"

# game/script.rpy:4812
translate japanese route_masaru_4_nosex_600db367:

    toshu "Aww…"
    toshu "ああ…"

# game/script.rpy:4814
translate japanese route_masaru_4_nosex_a2d75d5f:

    ichiru "Fine! Next time, promise me you'll give me a backscrub, okay?!"
    ichiru "ファイン！次回は、あなたが私にバックスクラブを与えることを約束します、大丈夫ですか？"

# game/script.rpy:4816
translate japanese route_masaru_4_nosex_a0ee5e1e:

    masaru "Ehh…"
    masaru "ええ…"

# game/script.rpy:4826
translate japanese route_masaru_4_nosex_e95c253f_1:

    ichiru "HEYY!! Guys!!"
    ichiru "HEYY !!みんな！"

# game/script.rpy:4828
translate japanese route_masaru_4_nosex_484ed306_1:

    masaru "Oh, Ichiru."
    masaru "Oh, Ichiru."

# game/script.rpy:4830
translate japanese route_masaru_4_nosex_bb5cdb1a_1:

    toshu "Hi Ichiru, What happened with you and Coach?"
    toshu "こんにちは、あなたとコーチはどうでしたか？"

# game/script.rpy:4832
translate japanese route_masaru_4_nosex_e33e9690_1:

    ichiru "I ditched that perverted old geezer!"
    ichiru "私はその変態した古いオタクを捨てた！"

# game/script.rpy:4834
translate japanese route_masaru_4_nosex_bc02811b_1:

    ichiru "Glad I was able to escape from him!"
    ichiru "うれしい私は彼から逃れることができた！"

# game/script.rpy:4836
translate japanese route_masaru_4_nosex_b3108a87_1:

    ichiru "That STUPID Coach is so annoying!"
    ichiru "その愚かなコーチはとても迷惑です！"

# game/script.rpy:4838
translate japanese route_masaru_4_nosex_5ca6870f_1:

    toshu "At least you're back here!"
    toshu "少なくともあなたはここに戻ります！"

# game/script.rpy:4840
translate japanese route_masaru_4_nosex_4e555d70:

    ichiru "Anyway~ Toshu! Let me continue washing your hair!"
    ichiru "とにかく〜Toshu！あなたの髪を洗ってみましょう！"

# game/script.rpy:4842
translate japanese route_masaru_4_nosex_9dd71bb8_1:

    genji "HEY! ICHIRU!"
    genji "HEY! ICHIRU!"

# game/script.rpy:4844
translate japanese route_masaru_4_nosex_875f23a3_1:

    ichiru "HNGH!! YOU OLD PRICK! STOP FOLLOWING ME!!"
    ichiru "HNGH !!あなたは古いPRICK！付いて来ないでください！！"

# game/script.rpy:4846
translate japanese route_masaru_4_nosex_d05d97d3_1:

    genji "Haiizzz…"
    genji "Haiizzz …"

# game/script.rpy:4848
translate japanese route_masaru_4_nosex_61f82d00_1:

    genji "I think we overstayed here at the hot springs. We should start packing."
    genji "私はここで温泉で過ごしたと思う。私たちは梱包を開始するべきです。"

# game/script.rpy:4850
translate japanese route_masaru_4_nosex_cb16b663_1:

    masaru "Ah… yeah I didn't notice its almost sundown."
    masaru "ああ…ええ、私はそのほとんどの日没に気付かなかった。"

# game/script.rpy:4852
translate japanese route_masaru_4_nosex_a2b86b60:

    ichiru "Whattt!!! But I haven't finished bathing Tofu…"
    ichiru "Whattt !!!しかし、私はTofuの入浴を終えていません…"

# game/script.rpy:4854
translate japanese route_masaru_4_nosex_041bce8b_1:

    ichiru "It's all your fault, you stupid Coach!"
    ichiru "それはあなたのすべての欠陥です、あなたは愚かなコーチです！"

# game/script.rpy:4856
translate japanese route_masaru_4_nosex_6608fcda_1:

    toshu "I-ichiru, we should really get prepared."
    toshu "私は、私たちは本当に準備する必要があります。"

# game/script.rpy:4858
translate japanese route_masaru_4_nosex_bd840406_1:

    toshu "We can do it some other time."
    toshu "我々は別の時間にそれをすることができます。"

# game/script.rpy:4860
translate japanese route_masaru_4_nosex_0fedfab8:

    ichiru "Fine! Next time, we'll take a bath together, okay?!"
    ichiru "ファイン！次回は一緒にお風呂に行くよ、大丈夫？"

# game/script.rpy:4862
translate japanese route_masaru_4_nosex_a0ee5e1e_1:

    masaru "Ehh…"
    masaru "ええ…"

# game/script.rpy:4870
translate japanese route_ichiru_4_a24fa69e:

    toshu_t "Ichiru's words kept repeating in my head…"
    toshu_t "イチルの言葉が頭の中で繰り返され続けた…"

# game/script.rpy:4871
translate japanese route_ichiru_4_0031ddb0:

    toshu_t "I never thought he would say those."
    toshu_t "私は彼がそれらを言うとは思わなかった。"

# game/script.rpy:4872
translate japanese route_ichiru_4_a9aa056d:

    toshu_t "I felt really warm inside…"
    toshu_t "私は本当に暖かい…"

# game/script.rpy:4873
translate japanese route_ichiru_4_9db0c3d0:

    toshu_t "But now, things are getting quite out of hand…"
    toshu_t "しかし、今、物事はかなり手を抜いています…"

# game/script.rpy:4878
translate japanese route_ichiru_4_a5e0d47f:

    toshu "Wahh…! Ichiru! What if someone sees us?!"
    toshu "ワハ！Ichiru！もし誰かが私たちを見たら？"

# game/script.rpy:4879
translate japanese route_ichiru_4_e6157167:

    ichiru "It's okay… no one will see us, I'm pretty sure this is a blind spot."
    ichiru "それは大丈夫です…誰も私たちを見ることはありません、私はこれが盲目であると確信しています。"

# game/script.rpy:4881
translate japanese route_ichiru_4_67ebd979:

    toshu "T-that's so embarrassing Ichiru…!"
    toshu "名前：T-それはイッチルので恥ずかしいです…！"

# game/script.rpy:4882
translate japanese route_ichiru_4_becf0b4f:

    ichiru "It's okay Tofu! Let's just enjoy this!"
    ichiru "Tofuは大丈夫です！これを楽しんでみましょう！"

# game/script.rpy:4883
translate japanese route_ichiru_4_2d5c1418:

    ichiru "Hehe, your dick is so cute~"
    ichiru "Hehe、あなたのディックはとてもかわいいです〜"

# game/script.rpy:4884
translate japanese route_ichiru_4_351f15b9:

    ichiru "Don't mind if I dig in!"
    ichiru "私が掘り下げれば気にしないで！"

# game/script.rpy:4885
translate japanese route_ichiru_4_19b78d8c:

    ichiru "You too! Please!"
    ichiru "君も！お願いします！"

# game/script.rpy:4886
translate japanese route_ichiru_4_4a5272b6:

    toshu "Ahh…"
    toshu "ああ…"

# game/script.rpy:4888
translate japanese route_ichiru_4_7f602749:

    toshu "Hnn…"
    toshu "Hnn …"

# game/script.rpy:4889
translate japanese route_ichiru_4_0221a1a5:

    ichiru "Mmhh…"
    ichiru "うーん…"

# game/script.rpy:4891
translate japanese route_ichiru_4_ecf01997:

    ichiru "Mnghh!!!"
    ichiru "Mnghh !!!"

# game/script.rpy:4892
translate japanese route_ichiru_4_9c4766c0:

    toshu "Mmmhh!!"
    toshu "うーん!!"

# game/script.rpy:4893
translate japanese route_ichiru_4_0dc7d871:

    toshu_t "Ichiru's tongue is moving so fast…!"
    toshu_t "イチルの舌がとても速く動いている…！"

# game/script.rpy:4894
translate japanese route_ichiru_4_af5c1abd:

    toshu_t "His hips are thrusting forward too…!"
    toshu_t "彼の腰も前方に突っ込んでいる…！"

# game/script.rpy:4895
translate japanese route_ichiru_4_67aa2672:

    ichiru "MMMHHH…!"
    ichiru "MMMHHH …！"

# game/script.rpy:4897
translate japanese route_ichiru_4_20497d0b:

    toshu "Ukhh!!"
    toshu "うーん！"

# game/script.rpy:4898
translate japanese route_ichiru_4_001185d8:

    ichiru "Unghh…!"
    ichiru "うん…！"

# game/script.rpy:4900
translate japanese route_ichiru_4_63127dab:

    toshu "Humm…"
    toshu "うーん…"

# game/script.rpy:4901
translate japanese route_ichiru_4_0221a1a5_1:

    ichiru "Mmhh…"
    ichiru "うーん…"

# game/script.rpy:4902
translate japanese route_ichiru_4_c7ffc7b3:

    toshu_t "Ichiru's cum… this the first time I tasted something like this…"
    toshu_t "Ichiruの兼…これは私がこのようなものを味わった初めての…"

# game/script.rpy:4903
translate japanese route_ichiru_4_1108a322:

    toshu_t "It's so warm and thick… it just kept sliding down my throat…"
    toshu_t "それはとても暖かくて厚いです…それはちょうど私の喉を滑り続けていました…"

# game/script.rpy:4904
translate japanese route_ichiru_4_70c021fc:

    toshu_t "I can feel Ichiru slurping mine too…"
    toshu_t "私はイッチルが私の酒を飲むのを感じることができます…"

# game/script.rpy:4906
translate japanese route_ichiru_4_4a5272b6_1:

    toshu "Ahh…"
    toshu "ああ…"

# game/script.rpy:4907
translate japanese route_ichiru_4_c55743a8:

    ichiru "Haaa… Haa…"
    ichiru "Haaa … Haa …"

# game/script.rpy:4908
translate japanese route_ichiru_4_6ec19620:

    ichiru "It's really yummy."
    ichiru "本当においしいです。"

# game/script.rpy:4910
translate japanese route_ichiru_4_8145cf8d:

    toshu_t "After that incident with Ichiru."
    toshu_t "その事件の後、Ichiru。"

# game/script.rpy:4911
translate japanese route_ichiru_4_e449339b:

    toshu_t "I think… what I'm feeling… is not so simple after all…"
    toshu_t "私は思う…私が感じていることは…結局のところ単純ではない…"

# game/script.rpy:4912
translate japanese route_ichiru_4_4a09ba73:

    toshu_t "I'm so happy I was able to do that with Ichiru."
    toshu_t "Ichiruでそれをすることができたことはとても嬉しいです。"

# game/script.rpy:4913
translate japanese route_ichiru_4_00896d11:

    toshu_t "I need to get to know more about him!"
    toshu_t "私は彼についてもっと知ることが必要です！"

# game/script.rpy:4924
translate japanese route_ichiru_4_nosex_6fab1e72:

    genji "Hey, you two! There you are!"
    genji "ねえ、あなた二人！あなたはそこにいる！"

# game/script.rpy:4926
translate japanese route_ichiru_4_nosex_69cd537f:

    masaru "Where have you guys been?"
    masaru "あなたはどこにいましたか？"

# game/script.rpy:4928
translate japanese route_ichiru_4_nosex_49843bdd:

    ichiru "Oh! We're just doing, you know, stuff…"
    ichiru "ああ！私たちはちょうどやっている、あなた知っている、もの…"

# game/script.rpy:4930
translate japanese route_ichiru_4_nosex_70969e97:

    genji "And what kind of stuff exactly?"
    genji "正確にはどんなもの？"

# game/script.rpy:4932
translate japanese route_ichiru_4_nosex_609ce71b:

    ichiru "IT'S NONE OF YOUR BUSINESS, PERVERT!"
    ichiru "あなたのビジネスはまったく変わっていません！"

# game/script.rpy:4933
translate japanese route_ichiru_4_nosex_2c6c6ccc:

    ichiru "That piece of towel between your legs is really burning my eyes!!"
    ichiru "あなたの足の間のタオルの部分が本当に私の目を燃やしている!!"

# game/script.rpy:4935
translate japanese route_ichiru_4_nosex_3a00c793:

    genji "I told you! It's a fundoshi! There's no problem wearing this! It's just like our athletic jockstrap!"
    genji "先ほども言いました！それは、原則です！これを着ても問題ありません！それは私たちの運動競技場のようなものです！"

# game/script.rpy:4937
translate japanese route_ichiru_4_nosex_42ab01c5:

    ichiru "No way that's a jockstrap! Look at that thing! It's almost see-through! I can see your… YOUR ARGHHH!"
    ichiru "それはジョークストラップじゃない！そのことを見てください！それはほぼシースルーです！私はあなたを見ることができます…あなたのARGHHH！"

# game/script.rpy:4939
translate japanese route_ichiru_4_nosex_68154a29:

    genji "W-whaaa…! Do you have to point that out?!"
    genji "W-whaaa …！あなたはそれを指摘する必要がありますか？"

# game/script.rpy:4941
translate japanese route_ichiru_4_nosex_08147391:

    masaru "Here they go again…"
    masaru "ここで彼らは再び行く…"

# game/script.rpy:4943
translate japanese route_ichiru_4_nosex_2be2bfdb:

    genji "Sheesh! Why are you so worked up about it?"
    genji "Sheesh！なぜあなたはそれについてどう扱われているのですか？"

# game/script.rpy:4945
translate japanese route_ichiru_4_nosex_9e8e7132:

    genji "*cough* We need to go soon! It's already sundown."
    genji "*咳*私たちはすぐに行く必要があります！それはすでに夕暮れです。"

# game/script.rpy:4947
translate japanese route_ichiru_4_nosex_294add2d:

    genji "Pack your things and get dressed! We'll go as soon as everyone is ready."
    genji "あなたのものを詰めて服を着てください！誰もが準備ができるとすぐに私たちは行くでしょう。"

# game/script.rpy:4949
translate japanese route_ichiru_4_nosex_4565edf1:

    masaru "Yeah, they said the buses are waiting for us. I'll get changed."
    masaru "ええ、彼らはバスが私たちを待っていると言いました。私は変更されます。"

# game/script.rpy:4954
translate japanese route_ichiru_4_nosex_6ff7b5ac:

    ichiru "Phew! That was a close one!"
    ichiru "ピー！それは近いものでした！"

# game/script.rpy:4956
translate japanese route_ichiru_4_nosex_a6aa4eef:

    toshu "Y-yeah…"
    toshu "Y-yeah …"

# game/script.rpy:4958
translate japanese route_ichiru_4_nosex_c89f9669:

    ichiru "It's our little secret, okay?!"
    ichiru "それは私たちの小さな秘密です、大丈夫ですか？"

# game/script.rpy:4960
translate japanese route_ichiru_4_nosex_5c23fb1e:

    toshu "Yup!"
    toshu "うん！"

# game/script.rpy:4975
translate japanese choice_9_end_ccc9757c:

    genji "ATTENTION EVERYONE!"
    genji "誰にでも気をつけろ！"

# game/script.rpy:4977
translate japanese choice_9_end_44382c90:

    genji "I'm officially announcing the end of our 3-day camp! We've managed to have a great time together haven't we?!"
    genji "私は正式に3日間のキャンプの終了を発表しています！私たちは一緒に楽しい時間を過ごすことができましたか？"

# game/script.rpy:4979
translate japanese choice_9_end_2ee35508:

    genji "Seeing you guys improve your SKILLS on the first day."
    genji "最初の日にあなたのスキルを向上させるのが見えます。"

# game/script.rpy:4980
translate japanese choice_9_end_ec912982:

    genji "And improve your Teamwork on the second day."
    genji "2日目にチームワークを改善しましょう。"

# game/script.rpy:4982
translate japanese choice_9_end_c231ede9:

    genji "Let's not forget how we all had fun today on our last day!"
    genji "私たちが最後の日に今日どのように楽しく過ごしたかを忘れてはいけません！"

# game/script.rpy:4984
translate japanese choice_9_end_fdb1d59f:

    genji "Really makes me feel like I did a good job as your coach."
    genji "本当に私はあなたのコーチとしていい仕事をしたように感じます。"

# game/script.rpy:4985
translate japanese choice_9_end_c5d99476:

    genji "So I would like to congratulate you guys!"
    genji "だから皆さんおめでとうございます！"

# game/script.rpy:4987
translate japanese choice_9_end_d34152c5:

    genji "We'll keep practicing and make our way to the top! GOT THAT?!"
    genji "私たちは練習を続け、トップへ行く！わかった？！"

# game/script.rpy:4989
translate japanese choice_9_end_d2fcf211:

    masaru "Thank you Coach!"
    masaru "コーチありがとう！"

# game/script.rpy:4991
translate japanese choice_9_end_32a0f4d7:

    toshu "Thank you Coach!!"
    toshu "コーチありがとう！"

# game/script.rpy:4993
translate japanese choice_9_end_f15e4141:

    ichiru "Thanks… I guess."
    ichiru "ありがとう…私は推測する。"

# game/script.rpy:4995
translate japanese choice_9_end_72efb7c2:

    genji "Before anyone forgets, when we go home, be sure to study alright?"
    genji "誰かが忘れる前に、私たちが家に帰るとき、必ず勉強してください？"

# game/script.rpy:4996
translate japanese choice_9_end_ae0f81ec:

    genji "School exam period will be 2 weeks from now."
    genji "学校の試験期間は今から2週間です。"

# game/script.rpy:4997
translate japanese choice_9_end_742e79c2:

    genji "So make sure you keep your grades up okay?"
    genji "だからグレードアップを大丈夫にしておいてください。"

# game/script.rpy:4999
translate japanese choice_9_end_b4d784c3:

    genji "So I bid you all Good luck! In both the Tournament and your Exams!"
    genji "だから私はあなたにすべて幸運を請求する！トーナメントとあなたの試験の両方で！"

# game/script.rpy:5000
translate japanese choice_9_end_95dbca42:

    genji "Let's go home, team!"
    genji "家に帰ろう、チーム！"

# game/script.rpy:5001
translate japanese choice_9_end_464f58d8:

    toshu "Yes, Coach!"
    toshu "はい、コーチ！"

# game/script.rpy:5003
translate japanese choice_9_end_933bf5a8:

    toshu_t "I really had fun these past three days!"
    toshu_t "私はこれらの過去3日間本当に楽しかった！"

# game/script.rpy:5004
translate japanese choice_9_end_85ebd924:

    toshu_t "It was an experience that I will always treasure in my memory!"
    toshu_t "私はいつも私の記憶の中で宝をする経験でした！"

# game/script.rpy:5005
translate japanese choice_9_end_b941edec:

    toshu_t "Coach's words made me more motivated than ever!"
    toshu_t "コーチの言葉はこれまで以上に私をよりモチベーションにしました！"

# game/script.rpy:5010
translate japanese choice_9_end_37fa48c4:

    ichiru "Fwahhh~ I'm so beat!"
    ichiru "Fwahhh〜私はとてもビートです！"

# game/script.rpy:5012
translate japanese choice_9_end_4ef4da6c:

    masaru "Yeah, me too. At least we all had fun."
    masaru "うん、私も。少なくとも私たちはすべて楽しく過ごしました。"

# game/script.rpy:5014
translate japanese choice_9_end_9ebeaea6:

    ichiru "You bet!"
    ichiru "あなたは賭ける！"

# game/script.rpy:5015
translate japanese choice_9_end_b6d1c001:

    ichiru "I never expected that this camping trip would be enjoyable!"
    ichiru "私はこのキャンプ旅行が楽しいとは決して考えなかった！"

# game/script.rpy:5017
translate japanese choice_9_end_28f06f5b:

    ichiru "Plus, I got to spend a lot of time with Tofu!"
    ichiru "加えて、私はTofuと一緒に多くの時間を過ごさなければなりません！"

# game/script.rpy:5019
translate japanese choice_9_end_7d877e32:

    masaru "We all spent a lot of time together."
    masaru "私たちは皆、一緒に多くの時間を過ごしました。"

# game/script.rpy:5021
translate japanese choice_9_end_b3db2e5a:

    ichiru "Not to mention you got lost in the woods!!"
    ichiru "あなたは森で迷子になったことは言うまでもありません!!"

# game/script.rpy:5023
translate japanese choice_9_end_f52251ed:

    toshu "I-Ichiru!!"
    toshu "I-Ichiru!!"

# game/script.rpy:5025
translate japanese choice_9_end_07b220f1:

    masaru "Ichiru…"
    masaru "イチール…"

# game/script.rpy:5027
translate japanese choice_9_end_72701224:

    ichiru "I'm just kidding Masaru!"
    ichiru "私はまさるを冗談だよ！"

# game/script.rpy:5028
translate japanese choice_9_end_c55d3687:

    ichiru "Hahaha!"
    ichiru "ハハハ！"

# game/script.rpy:5030
translate japanese choice_9_end_e3c01f67:

    masaru "You guys are the best!"
    masaru "君たちは最高です！"

# game/script.rpy:5032
translate japanese choice_9_end_0eb57cfe:

    masaru "I wish we can all be together for a long time."
    masaru "私たちは皆、長い間一緒にいたいと思っています。"

# game/script.rpy:5033
translate japanese choice_9_end_4f034664:

    masaru "I've really gotten close to you guys."
    masaru "私はあなた達に本当に近づいてきました。"

# game/script.rpy:5035
translate japanese choice_9_end_caf55d8d:

    masaru "My problems disappear when we're all together."
    masaru "私たちが一緒にいると私の問題は消えます。"

# game/script.rpy:5036
translate japanese choice_9_end_af42f417:

    masaru "We just keep laughing and laughing."
    masaru "私たちはただ笑って笑っています。"

# game/script.rpy:5038
translate japanese choice_9_end_fb1a5c81:

    ichiru "Me too! I forgot all about mine as well!"
    ichiru "私も！私も私のことを忘れてしまった！"

# game/script.rpy:5040
translate japanese choice_9_end_11a94f5e:

    ichiru "That GENJI really made a great job I guess!"
    ichiru "GENJIが本当に素晴らしい仕事をしたと思うよ！"

# game/script.rpy:5041
translate japanese choice_9_end_09265b31:

    ichiru "I wish you two were my brothers!"
    ichiru "私はあなたの2人が私の兄弟だったらいい！"

# game/script.rpy:5043
translate japanese choice_9_end_30382dd7:

    toshu_t "Masaru's problems? And Ichiru too?"
    toshu_t "マサルの問題？イチールも？"

# game/script.rpy:5044
translate japanese choice_9_end_1375b755:

    toshu_t "Is there something bothering them lately?"
    toshu_t "最近彼らに迷惑をかける何かがありますか？"

# game/script.rpy:5046
translate japanese choice_9_end_4e46953d:

    ichiru "Oh! By the way, Tofu~ Coach said the exam will be on 2 weeks from now."
    ichiru "ああ！ちなみに、Tofu〜コーチは今から2週間後に試験が行われると言いました。"

# game/script.rpy:5047
translate japanese choice_9_end_7631536a:

    ichiru "Any plans of studying?"
    ichiru "勉強の計画はありますか？"

# game/script.rpy:5049
translate japanese choice_9_end_ce77df9b:

    toshu "Ahh… actually… not yet."
    toshu "ああ…実際…まだ。"

# game/script.rpy:5050
translate japanese choice_9_end_d040a000:

    toshu "I didn't have the time to study since I was so hyped up about this trip."
    toshu "私はこの旅行についてとても誇示されて以来、勉強する時間はなかった。"

# game/script.rpy:5052
translate japanese choice_9_end_f957a36d:

    ichiru "Hmm don't let your grades go down okay?"
    ichiru "うーん、あなたの成績が下がることはありませんか？"

# game/script.rpy:5054
translate japanese choice_9_end_3116d2fd:

    ichiru "If our grades go down, we won't be able to continue to join any club, including baseball."
    ichiru "成績が落ちれば、野球を含め、どんなクラブにも参加することはできません。"

# game/script.rpy:5056
translate japanese choice_9_end_cfea1d1d:

    toshu "I think you should be more worried."
    toshu "私はあなたがもっと心配するべきだと思う。"

# game/script.rpy:5057
translate japanese choice_9_end_3b4c2266:

    toshu "You've been missing out all the lessons cause of your skipping."
    toshu "あなたはスキップしたすべてのレッスンを逃してしまった。"

# game/script.rpy:5059
translate japanese choice_9_end_98871c2c:

    masaru "Ichiru! Didn't I tell you not to skip class???"
    masaru "Ichiru！私はクラスをスキップしないようにあなたに言わなかったのですか？"

# game/script.rpy:5061
translate japanese choice_9_end_2a35c361:

    ichiru "E-Eh!! But!"
    ichiru "E-Eh !!しかし！"

# game/script.rpy:5063
translate japanese choice_9_end_11189528:

    ichiru "I'm not doing it anymore!"
    ichiru "私はもうやっていない！"

# game/script.rpy:5064
translate japanese choice_9_end_039a1a23:

    ichiru "Ever since you told me not to!"
    ichiru "それ以来、あなたは私に言わなかったから！"

# game/script.rpy:5066
translate japanese choice_9_end_c428cd4f:

    masaru "That's good. Studying can be difficult sometimes."
    masaru "それは良い。勉強は時々難しいことがあります。"

# game/script.rpy:5068
translate japanese choice_9_end_7aaebc9c:

    masaru "Let's rest. We have school to catch up tomorrow."
    masaru "安心しましょう。私たちは明日追いつくための学校を持っています。"

# game/script.rpy:5070
translate japanese choice_9_end_ce4a2d33:

    ichiru "Yeah… I'm out of energy."
    ichiru "うん…私はエネルギーが足りない。"

# game/script.rpy:5072
translate japanese choice_9_end_fca2ebc5:

    toshu "I'm tired too. Let's rest!"
    toshu "私も疲れています。残りましょう！"

# game/script.rpy:5074
translate japanese choice_9_end_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:5076
translate japanese choice_9_end_90834d07:

    masaru "I'm sorry guys…"
    masaru "ごめんなさい…"
