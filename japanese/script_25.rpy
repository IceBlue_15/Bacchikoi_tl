# game/script.rpy:10518
translate japanese ending_ine_mpe_1657c646:

    # toshu "*sigh* …"
    toshu "はぁ……。"

# game/script.rpy:10520
translate japanese ending_ine_mpe_d1caa465:

    # toshu "Ichiru didn't contact ever since he left school…"
    toshu "Ichiruが学校を去ってからずっと連絡が無い……。"

# game/script.rpy:10521
translate japanese ending_ine_mpe_606f6703:

    # toshu "I'm really worried about him…"
    toshu "本当に心配だよ……。"

# game/script.rpy:10523
translate japanese ending_ine_mpe_0d562fd8:

    # toshu "Will he really leave without properly saying goodbye…?"
    toshu "お別れの挨拶も無しにいなくなっちゃうのかな……？"

# game/script.rpy:10524
translate japanese ending_ine_mpe_f933c8ab:

    # toshu "I should stop thinking this way!"
    toshu "こんなこと考えてちゃダメだ！"

# game/script.rpy:10526
translate japanese ending_ine_mpe_a4d59c91:

    # toshu "Masaru said I shouldn't think too much about this!"
    toshu "Masaruさんもあまり考え過ぎるなって言ってたし！"

# game/script.rpy:10529
translate japanese ending_ine_mpe_e9b5b288:

    # toshu "A-auntie?"
    toshu "お、叔母さん？"

# game/script.rpy:10531
translate japanese ending_ine_mpe_44a3b751:

    # toshu "I'm coming!!"
    toshu "今行きます！！"

# game/script.rpy:10534
translate japanese ending_ine_mpe_37b877c9:

    # toshu "Auntie, I didn't know you will go home toda--"
    toshu "叔母さん、今日戻ってくるなんて知らなかっ──"

# game/script.rpy:10535
translate japanese ending_ine_mpe_025e069b:

    # random "Who are you calling auntie?"
    random "誰が叔母さんだって？"

# game/script.rpy:10536
translate japanese ending_ine_mpe_ce5e66e0:

    # random "Did you already forget about me, Tofu?!"
    random "俺のことを忘れちまったのか？　Tofu。"

# game/script.rpy:10537
translate japanese ending_ine_mpe_bba4677f:

    # toshu "I…ICHIRU!"
    toshu "I……　ICHIRU！"

# game/script.rpy:10545
translate japanese ending_ine_mpe_1b4d0f0f:

    # ichiru "Hi Tofu! I wanted to visit you!"
    ichiru "久し振りだな、Tofu！　会いたかったぞ！"

# game/script.rpy:10547
translate japanese ending_ine_mpe_13520145:

    # ichiru "I haven't seen or talked with you for a long time."
    ichiru "話しをするのもしばらく振りだな。"

# game/script.rpy:10549
translate japanese ending_ine_mpe_6dccb94e:

    # ichiru "I really missed you!"
    ichiru "本当に寂しかったぞ！"

# game/script.rpy:10551
translate japanese ending_ine_mpe_29fbb4b9:

    # toshu "I missed you too."
    toshu "僕も……　会いたかった……。"

# game/script.rpy:10553
translate japanese ending_ine_mpe_3ebf9843:

    # ichiru "Toshu… I'm really sorry about last time…"
    ichiru "Toshu……　前回のことは本当にゴメンな……。"

# game/script.rpy:10556
translate japanese ending_ine_mpe_0d639d2f:

    ichiru "My dad convinced me to follow his plans for now."
    ichiru "私のお父さんは今私の計画に従うように私に納得させました。"

# game/script.rpy:10557
translate japanese ending_ine_mpe_86a058ee:

    ichiru "He promised me he will give me the freedom with my decisions after I finish my studies."
    ichiru "彼は、私が勉強を終えた後、彼が私の意思決定で私に自由を与えることを約束しました。"

# game/script.rpy:10559
translate japanese ending_ine_mpe_f856550b:

    # ichiru "Believe me… I really broke down after that…"
    ichiru "信じてくれ……　あれ以来、俺は本当に壊れちまってたんだ……。"

# game/script.rpy:10560
translate japanese ending_ine_mpe_915a1af5:

    # ichiru "I needed time to calm myself down…"
    ichiru "俺にはじっくり考える時間が必要だったんだ……。"

# game/script.rpy:10562
translate japanese ending_ine_mpe_364f49a2_1:

    # toshu "…"
    toshu "……。"

# game/script.rpy:10564
translate japanese ending_ine_mpe_4dce49ef:

    # ichiru "You know that I never really wanted to leave, right?"
    ichiru "俺が本心では去りたくないって分かってるだろ？"

# game/script.rpy:10566
translate japanese ending_ine_mpe_68e422fd:

    # ichiru "I would never abandon such a special friend!"
    ichiru "俺は特別な友達を見捨てるようなことはしない！"

# game/script.rpy:10570
translate japanese ending_ine_mpe_cabff13e:

    # ichiru "You're the one who taught me to never give up!"
    ichiru "お前が俺に諦めないことを教えてくれたんだ！"

# game/script.rpy:10572
translate japanese ending_ine_mpe_8e042313:

    # ichiru "You always listen to my problems!"
    ichiru "お前はいつも俺の抱えてる問題に親身に耳を傾けてくれた！"

# game/script.rpy:10574
translate japanese ending_ine_mpe_f6f44d4b:

    # ichiru "And you taught me the value of friendship!"
    ichiru "そして俺に友情とは何かを教えてくれたんだ！"

# game/script.rpy:10575
translate japanese ending_ine_mpe_3c36bca6:

    # ichiru "You made my life so much better, Toshu!"
    ichiru "Toshu、お前と出会って俺の人生は変わったんだ。"

# game/script.rpy:10577
translate japanese ending_ine_mpe_fcd83c3e:

    # ichiru "I'll make a promise to you!"
    ichiru "俺は約束するよ！"

# game/script.rpy:10579
translate japanese ending_ine_mpe_2bfd7743:

    # ichiru "That even we're going on separate ways, we will still be the best of friends!"
    ichiru "俺達は別々の道を行くけど、これからもずっと最高の友達だってな！"

# game/script.rpy:10581
translate japanese ending_ine_mpe_3105c0f7:

    # toshu "I-Ichiru…"
    toshu "I……　Ichiru……。"

# game/script.rpy:10583
translate japanese ending_ine_mpe_6433351f:

    # ichiru "Goodbyes are not forever…"
    ichiru "別れは永遠じゃない……。"

# game/script.rpy:10584
translate japanese ending_ine_mpe_0b9a4306:

    # ichiru "Goodbyes are not the end…"
    ichiru "別れは終わりじゃない……。"

# game/script.rpy:10586
translate japanese ending_ine_mpe_4a64ed78:

    # ichiru "It simply mean… I will miss you!"
    ichiru "ただちょっと……　寂しくなるけどな！"

# game/script.rpy:10587
translate japanese ending_ine_mpe_dc397d6c:

    # ichiru "We will meet again!"
    ichiru "またきっといつか会おうぜ！"

# game/script.rpy:10589
translate japanese ending_ine_mpe_be12d1d5:

    # toshu_t "In the end…"
    toshu_t "最終的には……"

# game/script.rpy:10590
translate japanese ending_ine_mpe_566de304:

    # toshu_t "The pain I felt from saying goodbye to Ichiru was nothing compared…"
    toshu_t "Ichiruにさよならを言って感じた痛みは……"

# game/script.rpy:10591
translate japanese ending_ine_mpe_f0f24371:

    # toshu_t "…from the happiness I felt from the friends I've made."
    toshu_t "……Ichiruたちと出会って得られた幸福に比べれば何でもないものだった。"
