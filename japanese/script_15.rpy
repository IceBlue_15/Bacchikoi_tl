# game/script.rpy:8965
translate japanese after_minigame_8_b0dbfece:

    toshu "Fwah! What a great day!"
    toshu "フワ！なんていい日だ！"

# game/script.rpy:8967
translate japanese after_minigame_8_46ffbf08:

    ichiru "Toshu!"
    ichiru "Toshu!"

# game/script.rpy:8969
translate japanese after_minigame_8_ebf246ce:

    toshu "Oh, Ichiru! Good morning!"
    toshu "ああ、Ichiru！おはようございます！"

# game/script.rpy:8970
translate japanese after_minigame_8_7ca063a6:

    toshu "I'm really impressed!"
    toshu "私は本当に感銘を受けました！"

# game/script.rpy:8972
translate japanese after_minigame_8_efd072a5:

    toshu "You're attending class more often now! And you're earlier than me!"
    toshu "あなたは今より頻繁にクラスに参加しています！あなたは私よりも早いです！"

# game/script.rpy:8974
translate japanese after_minigame_8_3df3d737:

    ichiru "Of course! I will go to school every day just to see you!"
    ichiru "もちろん！私はあなたに会うために毎日学校に行きます！"

# game/script.rpy:8976
translate japanese after_minigame_8_c5b7435f:

    toshu "So, what are your plans for today besides going to practice after class?"
    toshu "では、今日のあなたの計画は、授業後に練習する以外に何ですか？"

# game/script.rpy:8978
translate japanese after_minigame_8_6d3c0d6c:

    ichiru "Hmm, I really have no plans for today… Maybe we can meet Masaru later and ask him how his exam went?"
    ichiru "うーん、今日は本当に計画がありません…後でマサルと会い、試験がどのようになったのか聞いてみてください。"

# game/script.rpy:8980
translate japanese after_minigame_8_63273dd4:

    toshu "Yeah, you are right we should check up on hi--"
    toshu "ええ、あなたが正しいことを私たちはhi上で調べるべきです -"

# game/script.rpy:8982
translate japanese after_minigame_8_84ab7380:

    random "DON'T MAKE ME REPEAT MYSELF YOU STUPID BITCH!"
    random "あなたは愚かなことを繰り返すことはできません！"

# game/script.rpy:8984
translate japanese after_minigame_8_612568d9:

    ichiru "Huh? What's with all the ruckus outside?"
    ichiru "ハァッ？外の騒動はどういったものですか？"

# game/script.rpy:8986
translate japanese after_minigame_8_8a861984:

    toshu "Sounds like there's a fight?"
    toshu "そこには戦いがあるように聞こえる？"

# game/script.rpy:8988
translate japanese after_minigame_8_5c39fc8f:

    ichiru "C'mon let's check it out!"
    ichiru "さあ、それをチェックしよう！"

# game/script.rpy:8996
translate japanese after_minigame_8_f9d9301a:

    saki "Honestly, I do not know! Please forgive me Princess Saji! I just arrived!"
    saki "正直、私は知らない！私を許してくださいプリンセス匙！ちょうど着きました！"

# game/script.rpy:8997
translate japanese after_minigame_8_4d3aeebc:

    tomoka "You useless piece of garbage!"
    tomoka "あなたは役に立たないゴミの一片！"

# game/script.rpy:8999
translate japanese after_minigame_8_17fc02ec:

    tomoka "What kind of teacher are you?! You don't know where you're students are?!"
    tomoka "あなたはどんな先生ですか？あなたはどこにいるのか分かりません。"

# game/script.rpy:9001
translate japanese after_minigame_8_da8faf19:

    tomoka "I'm ASKING you once again! Where is ICHIRU!!!!"
    tomoka "私はもう一度あなたに尋ねています！ ICHIRUはどこですか？"

# game/script.rpy:9004
translate japanese after_minigame_8_70362388:

    ichiru "S-SHIT!"
    ichiru "S-SHIT！"

# game/script.rpy:9006
translate japanese after_minigame_8_7c13a6e2:

    tomoka "Ooh!~ there you are my, Ichiru!"
    tomoka "ああ！〜そこに私は、Ichiruです！"

# game/script.rpy:9007
translate japanese after_minigame_8_6fd13b11:

    tomoka "Did you hear the terrible news from daddy?!"
    tomoka "あなたはパパから恐ろしいニュースを聞いたのですか？"

# game/script.rpy:9009
translate japanese after_minigame_8_90503ab0:

    tomoka "He said, your father is requesting to reconsider our marriage??"
    tomoka "彼は言った、あなたの父は私たちの結婚を再考することを要求している？"

# game/script.rpy:9010
translate japanese after_minigame_8_c08e928d:

    tomoka "Did I hear that right?"
    tomoka "私はそれを聞いたのですか？"

# game/script.rpy:9012
translate japanese after_minigame_8_ecd6a769:

    tomoka "I came here to confirm It myself~"
    tomoka "私はそれを確認するためにここに来た自分自身〜"

# game/script.rpy:9014
translate japanese after_minigame_8_0b0c9161:

    tomoka "Is that true?"
    tomoka "本当？"

# game/script.rpy:9016
translate japanese after_minigame_8_e8b3dff9:

    ichiru "W-well…"
    ichiru "うーん…"

# game/script.rpy:9018
translate japanese after_minigame_8_98dfd527:

    tomoka "Oh! And before you answer. Let me remind you that your dad is indebted to my father."
    tomoka "ああ！そしてあなたが答える前に。あなたのお父さんが私の父親に借りていることを思い出させてください。"

# game/script.rpy:9020
translate japanese after_minigame_8_ccaa365b:

    tomoka "No matter what he says, if I do not like it, he will not do it."
    tomoka "彼が何を言っても、私がそれを気に入らなければ、彼はそれをしません。"

# game/script.rpy:9021
translate japanese after_minigame_8_06a74f30:

    tomoka "And also, I heard that if you win the tournament, he will reconsider?"
    tomoka "そして、あなたがトーナメントに勝つと、彼は再考するだろうと聞きましたか？"

# game/script.rpy:9022
translate japanese after_minigame_8_dd37ea95:

    tomoka "You know how much of a baseball fan my dad is right? Heh. Getting to his good sides."
    tomoka "私のお父さんがどれくらいの野球ファンを知っていますか？ Heh。彼の良い面へ行く。"

# game/script.rpy:9024
translate japanese after_minigame_8_d201df65:

    tomoka "Sneaky, sneaky Ichiru!"
    tomoka "卑劣な、卑劣なIchiru！"

# game/script.rpy:9025
translate japanese after_minigame_8_6dec5f27:

    tomoka "Well, let's count how many wins this school had anyway…"
    tomoka "さて、この学校が何とか勝利を数えてみましょう…"

# game/script.rpy:9026
translate japanese after_minigame_8_522076fa:

    tomoka "Hhmm… let's see… OH! That's right!"
    tomoka "うーん…見てみましょう…オハイオ州！そのとおり！"

# game/script.rpy:9028
translate japanese after_minigame_8_e899f68a:

    tomoka "ZERO! HOHOHO!!!"
    tomoka "ZERO! HOHOHO!!!"

# game/script.rpy:9030
translate japanese after_minigame_8_c73252a5:

    tomoka "Why don't you just give it up and go away with me~ I can give you everything you want!"
    tomoka "どうしてあなたはそれをあきらめて私と一緒に去ってはいけないのですか？あなたが望むものすべてをあなたに与えることができます！"

# game/script.rpy:9032
translate japanese after_minigame_8_23a76ecd:

    tomoka "We love each other, don't we, Ichiru~?"
    tomoka "私たちはお互いを愛している、私たちはいちル〜ない？"

# game/script.rpy:9034
translate japanese after_minigame_8_419c0495:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:9036
translate japanese after_minigame_8_e1f57127:

    tomoka "WELL??!!! SPEAK UP ICHIRU!!!"
    tomoka "いいですか??スピークアップICHIRU !!!"

# game/script.rpy:9037
translate japanese after_minigame_8_e3ecaae9:

    ichiru "I… I…"
    ichiru "私は…私は…"

# game/script.rpy:9038
translate japanese after_minigame_8_a4e391f0:

    tomoka "I can't hear you!"
    tomoka "聞こえません！"

# game/script.rpy:9040
translate japanese after_minigame_8_d3c1c98b:

    tomoka "IF YOU WANT TO SPEAK, SPEAK CLEARLY SO THAT I WILL HE--"
    tomoka "あなたが話したいのであれば、私が彼になるだろうということをはっきりと話す -"

# game/script.rpy:9042
translate japanese after_minigame_8_a56a8ca7:

    toshu "THAT'S ENOUGH!!"
    toshu "十分です！"

# game/script.rpy:9044
translate japanese after_minigame_8_63a3adbe:

    tomoka "Excuse me??"
    tomoka "すみません？？"

# game/script.rpy:9045
translate japanese after_minigame_8_fa5bd692:

    tomoka "Why is this trash talkin--"
    tomoka "なぜこのゴミは話しているのですか？"

# game/script.rpy:9047
translate japanese after_minigame_8_d05154fa:

    toshu "Ms. Saji with all due respect!"
    toshu "敬意をもって佐治さんに！"

# game/script.rpy:9049
translate japanese after_minigame_8_9d87085a:

    toshu "You are being rude and mean!"
    toshu "あなたは失礼で、意味があります！"

# game/script.rpy:9050
translate japanese after_minigame_8_5fb8b4f7:

    toshu "Not just to Ichiru!"
    toshu "イチールだけじゃない！"

# game/script.rpy:9051
translate japanese after_minigame_8_cc95cf0c:

    toshu "But to everyone around you!"
    toshu "しかし、あなたの周りのみんなに！"

# game/script.rpy:9052
translate japanese after_minigame_8_a34ff4ec:

    toshu "Don't you imagine how painful your words are to the people you speak to?!"
    toshu "あなたが話している人々にあなたの言葉がどれほど痛いのか想像はしませんか？"

# game/script.rpy:9054
translate japanese after_minigame_8_fa72bfb0:

    tomoka "WHY YOU LITTLE!"
    tomoka "あなたはなぜですか？"

# game/script.rpy:9055
translate japanese after_minigame_8_5d0f04cb:

    tomoka "Do you not know who you are speaking to?!"
    tomoka "あなたが誰と話しているのかわからないのですか？"

# game/script.rpy:9057
translate japanese after_minigame_8_7298041d:

    toshu "I am fully aware of who you are and what you are capable of!"
    toshu "私はあなたが誰であり、あなたができることを十分に認識しています！"

# game/script.rpy:9059
translate japanese after_minigame_8_16197dd5:

    toshu "But if you want respect from other people…"
    toshu "しかし、あなたが他の人からの尊敬を望むなら…"

# game/script.rpy:9061
translate japanese after_minigame_8_800ce534:

    toshu "BE THE FIRST TO SHOW IT!"
    toshu "それを最初に見せてください！"

# game/script.rpy:9063
translate japanese after_minigame_8_d0beb068:

    ichiru "T-Toshu…"
    ichiru "T-Toshu…"

# game/script.rpy:9065
translate japanese after_minigame_8_12899842:

    toshu "You don't know how many times Ichiru has been hurt because of you!"
    toshu "イチールが何回あなたのために傷ついているのか分かりません！"

# game/script.rpy:9067
translate japanese after_minigame_8_6b4c5fdf:

    toshu "You monster!"
    toshu "あなたはモンスターです！"

# game/script.rpy:9069
translate japanese after_minigame_8_4589817f:

    tomoka "*GASP*"
    tomoka "* GASP *"

# game/script.rpy:9071
translate japanese after_minigame_8_e69292df:

    tomoka "F-Father will hear about this!!!"
    tomoka "F-父はこれについて聞いてくれるでしょう！"

# game/script.rpy:9074
translate japanese after_minigame_8_419c0495_1:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:9076
translate japanese after_minigame_8_627d27e4:

    # saki "…"
    saki "…"

# game/script.rpy:9078
translate japanese after_minigame_8_2238ce60:

    toshu "Hmph! Good riddance!"
    toshu "Hmph！いい厄介払い！"

# game/script.rpy:9079
translate japanese after_minigame_8_5df61c1d:

    saki "M-Mr. Kanada…"
    saki "Mさん。カナディア…"

# game/script.rpy:9080
translate japanese after_minigame_8_6701d95b:

    ichiru "There hasn't been anyone who talked back to Tomoka like that…"
    ichiru "トモカに言った人はいません…"

# game/script.rpy:9082
translate japanese after_minigame_8_661c6e2e:

    toshu "Well, she was mean! I had to do something!"
    toshu "まあ、彼女は意味があった！私は何かをしなければならなかった！"

# game/script.rpy:9083
translate japanese after_minigame_8_1f2ef388:

    random "That's the bravest thing I ever saw, Kanada!"
    random "それは私が今までに見た勇敢なものです、カナディアン！"

# game/script.rpy:9085
translate japanese after_minigame_8_f8b7f03d:

    genji "I never expected you to have an iron fist like that!"
    genji "私はあなたがそれのような鉄の拳を持っていることを期待したことはありません！"

# game/script.rpy:9088
translate japanese after_minigame_8_2d5eb91d:

    toshu "C-Coach!"
    toshu "Cコーチ！"

# game/script.rpy:9090
translate japanese after_minigame_8_6df4a51a:

    saki "M-Mr. Tadano!"
    saki "Mさん。タダノ！"

# game/script.rpy:9091
translate japanese after_minigame_8_8da0749a:

    saki "H…How long have you been there?"
    saki "H …どこにいたの？"

# game/script.rpy:9093
translate japanese after_minigame_8_25b4c3fe:

    genji "I was walking down the hallway when suddenly I heard that girl yelling."
    genji "私は突然その女の子が叫んでいると聞いて廊下を歩いていた。"

# game/script.rpy:9094
translate japanese after_minigame_8_d4851b96:

    genji "She was pretty harsh on you, Saki!"
    genji "彼女はあなたにかなり厳しい、さき！"

# game/script.rpy:9096
translate japanese after_minigame_8_2451a520:

    saki "U… hnn… w-whn… a-ah…"
    saki "U … hnn … w-whn … a-ah …"

# game/script.rpy:9097
translate japanese after_minigame_8_5a72ae29:

    genji "Are you alright?"
    genji "だいじょうぶですか？"

# game/script.rpy:9099
translate japanese after_minigame_8_80756f09:

    saki "Ahh! Yes! Thank you Mr. Tadano!"
    saki "ああ！はい！ありがとうございました！"

# game/script.rpy:9101
translate japanese after_minigame_8_7916a92f:

    genji "What's wrong? Your face is all red."
    genji "どうしましたか？あなたの顔はすべて赤いです。"

# game/script.rpy:9102
translate japanese after_minigame_8_2003c96f:

    genji "You have a fever or something?"
    genji "発熱がありますか？"

# game/script.rpy:9104
translate japanese after_minigame_8_06c44b07:

    saki "N-no n-no! I'm okay!"
    saki "N-no n-no！私は大丈夫ですよ！"

# game/script.rpy:9105
translate japanese after_minigame_8_ce058f25:

    saki "T-thanks for everything I have to leave bye!"
    saki "私が残す必要があるすべてのためのT-ありがとう！"

# game/script.rpy:9107
translate japanese after_minigame_8_f9c96739:

    genji "Well, that was weird…"
    genji "まあ、それは変だった…"

# game/script.rpy:9109
translate japanese after_minigame_8_a5805a7b:

    genji "Anyway, hahaha! You really kicked that princess ass, didn't you toshu?"
    genji "とにかく、ハハハ！あなたは本当にその王女のお尻を蹴った、あなたはない？"

# game/script.rpy:9110
translate japanese after_minigame_8_043272a4:

    genji "Gosh it's such an eyesore to see her walking around in the school wearing that gown and bringing that unecessary parasol!"
    genji "あのガウンを着た学校で歩いているのを見て、その不要なパラソルを持って来るのは、まあまあ！"

# game/script.rpy:9112
translate japanese after_minigame_8_30b123e7:

    ichiru "H-how can you guys relax? We will really be in trouble because of this!"
    ichiru "あなたはどうやってリラックスできますか？私たちは本当にこのために困っています！"

# game/script.rpy:9113
translate japanese after_minigame_8_09d17982:

    ichiru "Tomoka's dad will surely do whatever his daughter wishes!"
    ichiru "トモカさんのお父さんは、娘の望むところは絶対にやるよ！"

# game/script.rpy:9115
translate japanese after_minigame_8_8038ea28:

    ichiru "Toshu got involved in my own personal problem. I'm so sorry I dragged you into this…"
    ichiru "Toshuは私自身の個人的な問題に関わってきました。大変申し訳ございませんが、"

# game/script.rpy:9117
translate japanese after_minigame_8_c20c596f:

    toshu "N-no, Ichiru! Your problem is my problem too!"
    toshu "いいえ、Ichiru！あなたの問題も私の問題です！"

# game/script.rpy:9118
translate japanese after_minigame_8_056f4deb:

    toshu "And besides, I'm the one who jumped in!"
    toshu "それに、私は飛び込んだ人です！"

# game/script.rpy:9120
translate japanese after_minigame_8_bc2a310b:

    toshu "I will do everything it takes for me to see your smile!"
    toshu "私はあなたの笑顔を見るために必要なすべてをやります！"

# game/script.rpy:9122
translate japanese after_minigame_8_118763b1:

    ichiru "Toshu…"
    ichiru "Toshu…"

# game/script.rpy:9124
translate japanese after_minigame_8_50495b32:

    genji "Aww… isn't that cute?"
    genji "ああ…それはかわいいですか？"

# game/script.rpy:9126
translate japanese after_minigame_8_042f94ef:

    masaru "Hey guys!"
    masaru "ねえ、みんな！"

# game/script.rpy:9128
translate japanese after_minigame_8_3a058d90:

    masaru "What are you guys doing all huddled up here in the hallway?"
    masaru "あなたは皆、ここで廊下で集まって何をしていますか？"

# game/script.rpy:9130
translate japanese after_minigame_8_42bd144f:

    toshu "Oh! Hi, Masaru!"
    toshu "ああ！こんにちは、元帥！"

# game/script.rpy:9131
translate japanese after_minigame_8_316b5c72:

    toshu "We were just talking!"
    toshu "私たちはただ話していただけです"

# game/script.rpy:9132
translate japanese after_minigame_8_9084f80d:

    toshu "How was your exam?"
    toshu "あなたの試験はどうでしたか？"

# game/script.rpy:9134
translate japanese after_minigame_8_3242ab90:

    masaru "Ah, I think it went just fine."
    masaru "ああ、私はそれがうまくいったと思う。"

# game/script.rpy:9136
translate japanese after_minigame_8_2b293a5c:

    ichiru "That's a relief…"
    ichiru "それは救済だ…"

# game/script.rpy:9138
translate japanese after_minigame_8_911ab1a9:

    masaru "Hmm? You look troubled, Ichiru."
    masaru "うーん？あなたは悩んでいる、Ichiru。"

# game/script.rpy:9139
translate japanese after_minigame_8_dd9cadac:

    masaru "Did something happen?"
    masaru "なんかあったの？"

# game/script.rpy:9141
translate japanese after_minigame_8_b86066f1:

    toshu "We had a little conflict with Tomoka a while ago…"
    toshu "しばらく前にトモカとちょっとぶつかりました…"

# game/script.rpy:9143
translate japanese after_minigame_8_32a6de0a:

    masaru "Oh, her again…"
    masaru "ああ、もう一度…"

# game/script.rpy:9145
translate japanese after_minigame_8_d404af8c:

    genji "OH YEAH! I almost forgot! I came here to look for you, Masaru!"
    genji "そうそう！忘れそうだった！私はあなたを探しにここに来た、元！"

# game/script.rpy:9146
translate japanese after_minigame_8_320c0f76:

    genji "I was going to ask you if you could relay to the players the news about the tournament."
    genji "トーナメントに関するニュースを選手たちに伝えられるかどうか聞いてみました。"

# game/script.rpy:9148
translate japanese after_minigame_8_fbd319e3:

    genji "But since we're here, I might as well tell you guys."
    genji "しかし、私たちがここにいるので、皆さんにもお伝えします。"

# game/script.rpy:9150
translate japanese after_minigame_8_f413b1dc:

    ichiru "News?"
    ichiru "ニュース？"

# game/script.rpy:9152
translate japanese after_minigame_8_325b66aa:

    genji "Unfortunately, the tournament date was rescheduled…"
    genji "残念ながら、トーナメントの日程は変更されました…"

# game/script.rpy:9154
translate japanese after_minigame_8_bfa6e6eb:

    toshu "Wow! That's great news! We will have more time to practice!"
    toshu "うわー！それは素晴らしいニュースです！私たちは練習にもっと時間があります！"

# game/script.rpy:9156
translate japanese after_minigame_8_ceb4cfae:

    genji "No, Kanada, the tournament will be… THIS WEEK. Four days from now."
    genji "いいえ、カナダ、トーナメントは…今週です。今から4日。"

# game/script.rpy:9158
translate japanese after_minigame_8_07d15384:

    masaru "WHAT?! Seriously?!"
    masaru "何？！真剣に！"

# game/script.rpy:9160
translate japanese after_minigame_8_645f084d:

    genji "Yes… I was quite shocked of this news as well…"
    genji "はい…私はこのニュースにもかなりショックを受けました…"

# game/script.rpy:9162
translate japanese after_minigame_8_a9f9ddb4:

    masaru "Oh no… do you think we can win?"
    masaru "ああ…あなたが勝つことができると思いますか？"

# game/script.rpy:9163
translate japanese after_minigame_8_5418d775:

    masaru "Our practice session was cut by half!"
    masaru "私たちの練習セッションは半分になりました！"

# game/script.rpy:9165
translate japanese after_minigame_8_fd9c963c:

    genji "Yeah… that's what I was afraid of…"
    genji "ええ…それは私が恐れていたことです…"

# game/script.rpy:9167
translate japanese after_minigame_8_990b8ae9:

    ichiru "W…we…"
    ichiru "W …私たち…"

# game/script.rpy:9169
translate japanese after_minigame_8_52cd0b38:

    genji "Hmm?"
    genji "うーん？"

# game/script.rpy:9171
translate japanese after_minigame_8_0cabd302:

    ichiru "We WILL WIN!"
    ichiru "我々は勝つ！"

# game/script.rpy:9172
translate japanese after_minigame_8_c9912a26:

    ichiru "We will definitely WIN!"
    ichiru "我々は間違いなく勝つ！"

# game/script.rpy:9173
translate japanese after_minigame_8_da0f7b97:

    ichiru "I won't be able to forgive myself if we don't win this!"
    ichiru "私たちがこれに勝たなければ、私は自分自身を許すことができません！"

# game/script.rpy:9174
translate japanese after_minigame_8_3bbe97ee:

    ichiru "I must put my words into action!"
    ichiru "私は行動に私の言葉を置く必要があります！"

# game/script.rpy:9176
translate japanese after_minigame_8_e583c927:

    ichiru "I'll show Tomoka!"
    ichiru "私はトモカを見せてあげるよ！"

# game/script.rpy:9178
translate japanese after_minigame_8_67193777:

    ichiru "I'll show them all!"
    ichiru "私はそれらをすべて表示します！"

# game/script.rpy:9180
translate japanese after_minigame_8_31127ce5:

    genji "That's the spirit Ichiru!"
    genji "それは霊のイチルです！"

# game/script.rpy:9182
translate japanese after_minigame_8_087d3ef8:

    masaru "You're right. There's no point on worrying about this now!"
    masaru "あなたが正しい。今これについて心配する必要はありません！"

# game/script.rpy:9184
translate japanese after_minigame_8_3d104339:

    masaru "I'm sure we can manage it somehow!"
    masaru "私は何とかそれを管理できると確信しています！"

# game/script.rpy:9186
translate japanese after_minigame_8_aca80fa3:

    toshu "Of course, we can! We are a team!"
    toshu "もちろん、我々はできます！私たちはチームです！"

# game/script.rpy:9188
translate japanese after_minigame_8_105c63d9:

    genji "Let's give it all this week! We're gonna train extra hard!"
    genji "今週中にそれをすべて捧げましょう！我々は余分な訓練をするつもりです！"

# game/script.rpy:9189
translate japanese after_minigame_8_0c13f327:

    genji "Everyone! To the baseball field!"
    genji "みんな！野球場に！"

# game/script.rpy:9206
translate japanese after_minigame_9_bfdaa63b:

    toshu_t "We spent the week devotedly preparing for the tournament."
    toshu_t "我々は一週間を献身的にトーナメントの準備に費やした。"

# game/script.rpy:9207
translate japanese after_minigame_9_69fa3184:

    toshu_t "For some reason, I'm pretty confident we can win!"
    toshu_t "何らかの理由で私は勝つことができると確信しています！"

# game/script.rpy:9208
translate japanese after_minigame_9_8a2d5476:

    toshu_t "Maybe it's because I'm seeing Ichiru wanting to win so bad…"
    toshu_t "多分私はイチールが勝つことを望んでいるのを見ているからかもしれない。"

# game/script.rpy:9209
translate japanese after_minigame_9_b7a06039:

    toshu_t "It gives me a gush of confidence!"
    toshu_t "それは私に自信を与えてくれます！"
