# game/script.rpy:8316
translate japanese after_minigame_7_b9b4ff58:

    toshu_t "Today's the big day!"
    toshu_t "今日は大きな一日！"

# game/script.rpy:8317
translate japanese after_minigame_7_73bc502c:

    toshu_t "Winning or losing, having my friends by my side is what matters the most!"
    toshu_t "勝つか失った、私の側で私の友人を持っていることが最も重要なことです！"

# game/script.rpy:8318
translate japanese after_minigame_7_dad8d35c:

    toshu_t "We've been through a lot to get where we are right now…"
    toshu_t "私たちは現在、どこにいるのかをたくさん見てきました…"

# game/script.rpy:8319
translate japanese after_minigame_7_b6150ed7:

    toshu_t "I'll definitely do my best and make everyone proud!"
    toshu_t "私は間違いなく全力を尽くし、誰もが誇りに思うよ！"

# game/script.rpy:8320
translate japanese after_minigame_7_1acd9a3b:

    toshu_t "Everything I experienced up to this point."
    toshu_t "今まで私が経験したすべて。"

# game/script.rpy:8321
translate japanese after_minigame_7_9184c936:

    toshu_t "For all the friends I've met!"
    toshu_t "私が会ったすべての友達のために！"

# game/script.rpy:8322
translate japanese after_minigame_7_433e3caa:

    toshu_t "And for the hard work we've done these past months!"
    toshu_t "そして、私たちは過去数ヶ月にわたって頑張ってきました！"

# game/script.rpy:8323
translate japanese after_minigame_7_9ba40378:

    toshu_t "It's finally going to pay off!"
    toshu_t "ついに払うつもりです！"

# game/script.rpy:8329
translate japanese after_minigame_7_0cd11cfb:

    # ichiru "Yoshaaaa!!! You guys, ready?!"
    ichiru "よっしゃあ！！！ みんな、準備はいいか！"

# game/script.rpy:8330
translate japanese after_minigame_7_3632e6c9:

    genji "This is it guys! This is what we trained hard for since last year!"
    genji "これはみんな！これは私たちが昨年から懸命に訓練したものです！"

# game/script.rpy:8331
translate japanese after_minigame_7_40283430:

    masaru "We really came a long way, haven't we?"
    masaru "私たちは本当に長い道のりを歩いたのですか？"

# game/script.rpy:8332
translate japanese after_minigame_7_510ab3cd:

    ichiru "Yeah! That's why we really all have to do our best!"
    ichiru "うん！だから私たちは本当に全力を尽くさなければなりません！"

# game/script.rpy:8333
translate japanese after_minigame_7_11d711c0:

    genji "Kanada! As our pitcher, we're really counting on you! Hahaha!"
    genji "カナディアン！私たちの投手として、私たちは本当にあなたを信じています！ははは！"

# game/script.rpy:8334
translate japanese after_minigame_7_464f58d8:

    toshu "Yes, Coach!"
    toshu "はい、コーチ！"

# game/script.rpy:8335
translate japanese after_minigame_7_0d30d1bd:

    masaru "So, is everyone ready?"
    masaru "だから皆は準備ができていますか？"

# game/script.rpy:8336
translate japanese after_minigame_7_ebe7d725:

    toshu "Ready!"
    toshu "準備ができました！"

# game/script.rpy:8337
translate japanese after_minigame_7_dbe9ff73:

    ichiru "YAKYUSHA TEAM, LET'S WIN THIS!!!"
    ichiru "YAKYUSHA TEAM、これで勝て！"

# game/script.rpy:8341
translate japanese after_minigame_7_561bde9d:

    # narrator "Aaaand… play ball!"
    narrator "プレイボール！"

# game/script.rpy:8342
translate japanese after_minigame_7_276d6762:

    toshu "Okay! I have to focus!"
    toshu "はい！私は集中しなければならない！"

# game/script.rpy:8343
translate japanese after_minigame_7_57071e4c:

    toshu "Don't let the enemy intimidate you!"
    toshu "敵があなたを脅かさないようにしてください！"

# game/script.rpy:8344
translate japanese after_minigame_7_3c70a868:

    toshu "I just need to follow Masaru's Signs and I will be fine!"
    toshu "私はMasaruの看板に従うだけでいいです。"

# game/script.rpy:8346
translate japanese after_minigame_7_448bef42:

    narrator "STRIKE ONE!"
    narrator "ストライクワン！"

# game/script.rpy:8347
translate japanese after_minigame_7_9311733f:

    toshu "Hnn… okay! So far so good."
    toshu "Hnn …大丈夫です！ここまでは順調ですね。"

# game/script.rpy:8349
translate japanese after_minigame_7_63618df8:

    narrator "STRIKE TWO!"
    narrator "ストライクツー！"

# game/script.rpy:8350
translate japanese after_minigame_7_f7fd8ee9:

    masaru "Nice pitch, Toshu!"
    masaru "Nice pitch, Toshu!"

# game/script.rpy:8351
translate japanese after_minigame_7_66481751:

    toshu "One last strike and he's out!"
    toshu "1つの最後のストライキと彼は出ている！"

# game/script.rpy:8352
translate japanese after_minigame_7_62636e16:

    toshu "Okay! I got this!"
    toshu "はい！私はこれを得た！"

# game/script.rpy:8354
translate japanese after_minigame_7_942ba2d9:

    narrator "STRIKE THREE! YOU'RE OUT!!"
    narrator "ストライクスリー！あなたは外にいる！！"

# game/script.rpy:8355
translate japanese after_minigame_7_80c62844:

    toshu "One OUT!"
    toshu "1アウト！"

# game/script.rpy:8356
translate japanese after_minigame_7_0b779aaa:

    ichiru "Great job guys!"
    ichiru "素晴らしい職人！"

# game/script.rpy:8358
translate japanese after_minigame_7_04ed0587:

    toshu_t "I thought our team was doing well…"
    toshu_t "私はチームがうまくやっていると思った…"

# game/script.rpy:8359
translate japanese after_minigame_7_37b1436f:

    toshu_t "I didn't expect that our enemy would be really good…!"
    toshu_t "私は敵が本当に良いとは思わなかった…！"

# game/script.rpy:8363
translate japanese after_minigame_7_08b965ca:

    toshu_t "There I was, staring at the scoreboard…feeling hopeless…"
    toshu_t "そこに私は、スコアボードを見つめていた…絶望感…"

# game/script.rpy:8364
translate japanese after_minigame_7_d2ffa736:

    toshu_t "Even a miracle might not be enough to win this game…"
    toshu_t "奇跡でさえこのゲームに勝つには不十分かもしれません…"

# game/script.rpy:8365
translate japanese after_minigame_7_7a453334:

    toshu_t "I… bet… everyone is disappointed…"
    toshu_t "私は…ベット…みんなが失望している…"

# game/script.rpy:8366
translate japanese after_minigame_7_4768524c:

    toshu_t "Please! Hang in there guys!"
    toshu_t "お願いします！そこにハングアップ！"

# game/script.rpy:8368
translate japanese after_minigame_7_4fc8adf5:

    toshu_t "As I made another pitch for Masaru to catch…"
    toshu_t "マサルがキャッチするために別のピッチを作ったとき…"

# game/script.rpy:8369
translate japanese after_minigame_7_a781e69c:

    toshu_t "The enemy batter once again, hit my throw…"
    toshu_t "もう一度敵の打者が私の投げを打つ…"

# game/script.rpy:8370
translate japanese after_minigame_7_843a2c46:

    toshu_t "The whole team was feeling devastated as the enemies ran around the diamond…"
    toshu_t "ダイヤモンドの周りに敵が走ったので、チーム全体が荒廃しているように感じました…"

# game/script.rpy:8371
translate japanese after_minigame_7_0d13e29d:

    toshu_t "And when that batter took a dive down to the mound… I knew something awful was going to happen!"
    toshu_t "そしてそのバッターがマウンドにダイビングをしたとき…私はひどい何かが起こることを知っていた！"

# game/script.rpy:8375
translate japanese after_minigame_7_4e4c156c:

    genji "M-Masaru!"
    genji "マサール！"

# game/script.rpy:8376
translate japanese after_minigame_7_0599f309:

    ichiru "Masaru…!"
    ichiru "Masaru…!"

# game/script.rpy:8377
translate japanese after_minigame_7_bfaef78e:

    toshu "Masaru!!!"
    toshu "Masaru!!!"

# game/script.rpy:8378
translate japanese after_minigame_7_8296dbc3:

    narrator "Uh oh, folks! Looks like Yakyusha's Catcher and team captain Masaru Nakahara has been injured!"
    narrator "ああ、皆さん！ヤクシュサのキャッチャーのように見え、チームキャプテンの中原勝がけがをしています！"

# game/script.rpy:8379
translate japanese after_minigame_7_0961f3b6:

    genji "Call off the game! Someone is injured!"
    genji "ゲームを呼び出す！誰かが負傷している！"

# game/script.rpy:8388
translate japanese after_minigame_7_8b03c852:

    genji "Masaru! Are you alright?!"
    genji "マサル！だいじょうぶですか？！"

# game/script.rpy:8390
translate japanese after_minigame_7_73958687:

    masaru "It's okay Coach. It just a sprai-hnn!"
    masaru "それは大丈夫コーチです。それはちょうどスピンhnn！"

# game/script.rpy:8392
translate japanese after_minigame_7_8825a75f:

    genji "That's no good!"
    genji "それはまずいです！"

# game/script.rpy:8393
translate japanese after_minigame_7_6e08d414:

    toshu "M-Masaru…! Your… hand…"
    toshu "M-Masaru …！あなたの手…"

# game/script.rpy:8395
translate japanese after_minigame_7_0fdaf6a7:

    masaru "I… I'M SO SORRY!!!"
    masaru "私は…あまりにも悲しいです！"

# game/script.rpy:8396
translate japanese after_minigame_7_71f09498:

    masaru "I'M SORRY I LET THE TEAM DOWN!"
    masaru "私は悲しいです、私はチームを下にしましょう！"

# game/script.rpy:8397
translate japanese after_minigame_7_ae9f37d0:

    masaru "W… We lost because of me!"
    masaru "W …私は私のために失った！"

# game/script.rpy:8399
translate japanese after_minigame_7_e23024e1:

    ichiru "It's not your fault, Masaru…!"
    ichiru "あなたのせいじゃない、元…！"

# game/script.rpy:8401
translate japanese after_minigame_7_4c06f9bc:

    genji "Ichiru! Hurry! Call a medic!"
    genji "Ichiru！急いで！医者に電話してください！"

# game/script.rpy:8402
translate japanese after_minigame_7_ad662140:

    masaru "No, Coach!"
    masaru "いいえ、コーチ！"

# game/script.rpy:8412
translate japanese after_minigame_7_50903839:

    toshu "Masaru…"
    toshu "Masaru…"

# game/script.rpy:8413
translate japanese after_minigame_7_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:8436
translate japanese choice_18_A_84cee7e7:

    toshu_t "As I entered the locker room, I heard Masaru's voice… then saw him breaking down…"
    toshu_t "ロッカールームに入ると、私はマサルの声が聞こえた。"

# game/script.rpy:8438
translate japanese choice_18_A_579e9787:

    masaru "*sniff* *sniff*"
    masaru "*スニフ* *スニフ*"

# game/script.rpy:8440
translate japanese choice_18_A_50903839:

    toshu "Masaru…"
    toshu "Masaru…"

# game/script.rpy:8441
translate japanese choice_18_A_31347b5d:

    masaru "M-my hand…"
    masaru "M-私の手…"

# game/script.rpy:8442
translate japanese choice_18_A_ecd2fa9b:

    toshu "Your wrist… your fingers…"
    toshu "あなたの手首…あなたの指先…"

# game/script.rpy:8444
translate japanese choice_18_A_6ae41a30:

    toshu "They're broken…"
    toshu "彼らは壊れている…"

# game/script.rpy:8446
translate japanese choice_18_A_9ee874da:

    masaru "Not only I let you guys down…"
    masaru "だけでなく、私はあなたがダウンさせる…"

# game/script.rpy:8447
translate japanese choice_18_A_7524db99:

    masaru " But the only living proof of my mom too…"
    masaru "しかし、私のお母さんの唯一の生き証拠も…"

# game/script.rpy:8449
translate japanese choice_18_A_6466c893:

    masaru "I go to the music club just to honor my mother's gift to me…"
    masaru "私は私の母の贈り物に敬意を表して音楽クラブに行く…"

# game/script.rpy:8451
translate japanese choice_18_A_922bee39:

    toshu "Y-you mean…"
    toshu "あなたは…"

# game/script.rpy:8453
translate japanese choice_18_A_6fe99bbb:

    toshu "Your piano was sold to the music club?!"
    toshu "あなたのピアノは音楽クラブに売られましたか？"

# game/script.rpy:8455
translate japanese choice_18_A_6a3b4d8a:

    masaru "Being evicted from the club… and now… how can I even play the piano?!"
    masaru "クラブから追い出されて…今、ピアノを弾く方法は？"

# game/script.rpy:8457
translate japanese choice_18_A_dc793b6a:

    toshu_t "I knew there's no other possible thing to say to ease Masaru's pain…"
    toshu_t "まるの痛みを和らげるために言えることはほかにありません…"

# game/script.rpy:8458
translate japanese choice_18_A_2f100f4d:

    toshu "Give me your hand…"
    toshu "あなたの手をくれ…"

# game/script.rpy:8460
translate japanese choice_18_A_42ff0966:

    masaru "W-what are you doing…?"
    masaru "あなたは何をしていますか？"

# game/script.rpy:8462
translate japanese choice_18_A_1d615bb5:

    toshu_t "I held Masaru's hand and wrapped his injury to heal."
    toshu_t "私はマサルの手を握り、怪我を包んで癒した。"

# game/script.rpy:8464
translate japanese choice_18_A_c7d29a33:

    toshu "I've seen this kind of wound from my friend before…"
    toshu "私は友人からこの種の傷を見たことがあります…"

# game/script.rpy:8465
translate japanese choice_18_A_e8604d40:

    toshu "It will heal… I promise…"
    toshu "それは癒すだろう…私は約束…"

# game/script.rpy:8467
translate japanese choice_18_A_33504c5d:

    # masaru "Toshu…"
    masaru "Toshu…"

# game/script.rpy:8474
translate japanese choice_18_A_2696c50f:

    # masaru "…!!!"
    masaru "…！！！"

# game/script.rpy:8476
translate japanese choice_18_A_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:8478
translate japanese choice_18_A_41455c4b:

    toshu "Please do not give up!"
    toshu "あきらめないでください！"

# game/script.rpy:8486
translate japanese choice_18_A_1b558ae0:

    toshu "This is not the Masaru I know!"
    toshu "これは私が知っているマサルじゃない！"

# game/script.rpy:8488
translate japanese choice_18_A_4edd671a:

    toshu "The Captain Masaru that I know always bears a smile no matter what!"
    toshu "私が知っているキャプテンマサルは、いつも笑顔を浮かべています！"

# game/script.rpy:8489
translate japanese choice_18_A_ebe186bf:

    toshu "Even through the harshest situations!"
    toshu "最も厳しい状況でさえ！"

# game/script.rpy:8491
translate japanese choice_18_A_e350335d:

    masaru "But the game…"
    masaru "しかし、ゲーム…"

# game/script.rpy:8492
translate japanese choice_18_A_eadd4c44:

    masaru "Everyone trained for it so hard… and because of me we lost just like that…"
    masaru "誰もがとても頑張ったように訓練してくれました。"

# game/script.rpy:8494
translate japanese choice_18_A_ac9ea4e7:

    toshu "It is not your fault that we lost the game!"
    toshu "私たちがゲームを失ったのはあなたのせいではありません！"

# game/script.rpy:8495
translate japanese choice_18_A_fae8ffe5:

    toshu "We lost because we lacked training!"
    toshu "私たちは訓練が足りなかったので失った！"

# game/script.rpy:8497
translate japanese choice_18_A_d37e5d13:

    toshu "Please do not blame yourself!"
    toshu "あなた自身を責めないでください！"

# game/script.rpy:8498
translate japanese choice_18_A_c114b94a:

    toshu "Because whenever you are sad!"
    toshu "あなたが悲しいときはいつでも！"

# game/script.rpy:8500
translate japanese choice_18_A_e9542c4c:

    toshu "I… I can not stop myself from shedding tears as well!"
    toshu "私は…私も涙を流すのを止めることはできません！"

# game/script.rpy:8501
translate japanese choice_18_A_65d959de:

    toshu "I LOVE YOU CAPTAIN MASARU!"
    toshu "私はあなたを魅了します！"

# game/script.rpy:8502
translate japanese choice_18_A_a9e984c7:

    toshu "You mean the world to me!"
    toshu "貴方は私にとって大事な人です！"

# game/script.rpy:8504
translate japanese choice_18_A_4fbf2280:

    # masaru "T-Toshu…!"
    masaru "To、Toshu…！"

# game/script.rpy:8506
translate japanese choice_18_A_f1256079:

    toshu_t "I was out of my mind…"
    toshu_t "私は心配していませんでした…"

# game/script.rpy:8507
translate japanese choice_18_A_5d2f6bb2:

    toshu_t "I didn't know what can make Masaru feel better…"
    toshu_t "私は元帥が気分を良くすることができるか分からなかった…"

# game/script.rpy:8508
translate japanese choice_18_A_177acaea:

    toshu_t "I need to make him believe that he is special!"
    toshu_t "私は彼が彼が特別だと信じさせる必要があります！"

# game/script.rpy:8509
translate japanese choice_18_A_77fb9698:

    toshu_t "Masaru! I want to be with you forever…!"
    toshu_t "マサル！永遠にあなたと一緒にいたい…！"

# game/script.rpy:8516
translate japanese choice_18_A_33504c5d_1:

    # masaru "Toshu…"
    masaru "Toshu…"

# game/script.rpy:8517
translate japanese choice_18_A_4f403ff9:

    toshu "I want to always see your smile, Masaru…"
    toshu "私はいつもあなたの笑顔を見たい、マサル…"

# game/script.rpy:8518
translate japanese choice_18_A_83911d0b:

    toshu "Let me take your pain away!"
    toshu "私はあなたの苦痛を取り除きましょう！"

# game/script.rpy:8520
translate japanese choice_18_A_ea5ca1b0:

    toshu "Huwahh…"
    toshu "フワフ…"

# game/script.rpy:8521
translate japanese choice_18_A_6f2fd984:

    masaru "Ahhh… Toshu…"
    masaru "Ahhh… Toshu…"

# game/script.rpy:8523
translate japanese choice_18_A_28ed7208:

    toshu "Masaru…!"
    toshu "Masaru…!"

# game/script.rpy:8524
translate japanese choice_18_A_5b692410:

    masaru "Toshu, I'm gonna--!"
    masaru "Toshu, I'm gonna--!"

# game/script.rpy:8526
translate japanese choice_18_A_2b362307:

    toshu "Nghh!"
    toshu "Nghh！"

# game/script.rpy:8527
translate japanese choice_18_A_daea2397:

    masaru "Aaahh!!!"
    masaru "ああ！"

# game/script.rpy:8529
translate japanese choice_18_A_842c4fb3:

    toshu "Haa… are you alright, Masaru?"
    toshu "あなたは大丈夫ですか？"

# game/script.rpy:8530
translate japanese choice_18_A_fd8a9f68:

    masaru "Haaa… haa…"
    masaru "Haaa … haa …"

# game/script.rpy:8531
translate japanese choice_18_A_77eb7e10:

    toshu "Does your hand hurt?"
    toshu "あなたの手は傷ついていますか？"

# game/script.rpy:8532
translate japanese choice_18_A_553ee8b9:

    masaru "No… I'm fine…"
    masaru "いいえ、大丈夫です…"

# game/script.rpy:8533
translate japanese choice_18_A_77f10d88:

    toshu "Don't move it too much."
    toshu "あまり動かさないでください。"

# game/script.rpy:8534
translate japanese choice_18_A_070084dd:

    masaru "Toshu, I want to go inside you again…"
    masaru "Toshu、私はあなたの中にもう一度行きたいです…"

# game/script.rpy:8536
translate japanese choice_18_A_57775018:

    toshu "Hngh!"
    toshu "Hngh！"

# game/script.rpy:8537
translate japanese choice_18_A_a7816741:

    masaru "I was never really wrong about you…"
    masaru "私は本当にあなたについて間違っていたことはありません…"

# game/script.rpy:8538
translate japanese choice_18_A_5060802b:

    masaru "I knew you're the one who could make my everyday bright…"
    masaru "私はあなたが私の毎日の明るいを作ることができる人であることを知っていた…"

# game/script.rpy:8540
translate japanese choice_18_A_1eec8803:

    toshu "M-Masaru…!"
    toshu "M-Masaru …！"

# game/script.rpy:8541
translate japanese choice_18_A_f80cb5ed:

    masaru "You're my new reason why I want to keep smiling!"
    masaru "あなたは私が笑顔を維持したい私の新しい理由です！"

# game/script.rpy:8542
translate japanese choice_18_A_be481c6d:

    toshu "I'm coming…!"
    toshu "今行ってる…！"

# game/script.rpy:8543
translate japanese choice_18_A_caf146a3:

    masaru "I really… really… love you!"
    masaru "私は本当に、本当にあなたを愛して！"

# game/script.rpy:8545
translate japanese choice_18_A_793c850a:

    toshu "Nghh!!"
    toshu "Nghh !!"

# game/script.rpy:8546
translate japanese choice_18_A_ebd61d31:

    masaru "Wahh!!!"
    masaru "ワハ！"

# game/script.rpy:8548
translate japanese choice_18_A_b5e7a3ce:

    masaru "You're really the best, Toshu…"
    masaru "あなたは本当に最高です、Toshu …"

# game/script.rpy:8549
translate japanese choice_18_A_fadbab7d:

    masaru "I love you…"
    masaru "わたしは、あなたを愛しています…"

# game/script.rpy:8550
translate japanese choice_18_A_e1e5086d:

    toshu "I…"
    toshu "私…"

# game/script.rpy:8551
translate japanese choice_18_A_5ed17f7e:

    toshu "I love you too, Masaru…"
    toshu "私もあなたも大好きです…"

# game/script.rpy:8553
translate japanese choice_18_A_ca545bcb:

    toshu_t "This pleasure isn't something new for me…"
    toshu_t "この喜びは私にとって新しいものではありません…"

# game/script.rpy:8554
translate japanese choice_18_A_f3f3d3dd:

    toshu_t "Being with Masaru is the only thing I wanted the most after all…"
    toshu_t "まると一緒にいることが、私が一番欲しかった唯一のものです…"

# game/script.rpy:8555
translate japanese choice_18_A_9b59d0cd:

    toshu_t "I'm sure he will recover soon."
    toshu_t "私は彼がすぐに回復すると確信しています。"

# game/script.rpy:8566
translate japanese choice_18_B_84cee7e7:

    toshu_t "As I entered the locker room, I heard Masaru's voice… then saw him breaking down…"
    toshu_t "ロッカールームに入ると、私はマサルの声が聞こえた。"

# game/script.rpy:8568
translate japanese choice_18_B_579e9787:

    masaru "*sniff* *sniff*"
    masaru "*スニフ* *スニフ*"

# game/script.rpy:8570
translate japanese choice_18_B_50903839:

    toshu "Masaru…"
    toshu "Masaru…"

# game/script.rpy:8571
translate japanese choice_18_B_31347b5d:

    masaru "M-my hand…"
    masaru "M-私の手…"

# game/script.rpy:8572
translate japanese choice_18_B_ecd2fa9b:

    toshu "Your wrist… your fingers…"
    toshu "あなたの手首…あなたの指先…"

# game/script.rpy:8574
translate japanese choice_18_B_6ae41a30:

    toshu "They're broken…"
    toshu "彼らは壊れている…"

# game/script.rpy:8576
translate japanese choice_18_B_9ee874da:

    masaru "Not only I let you guys down…"
    masaru "だけでなく、私はあなたがダウンさせる…"

# game/script.rpy:8577
translate japanese choice_18_B_7524db99:

    masaru " But the only living proof of my mom too…"
    masaru "しかし、私のお母さんの唯一の生き証拠も…"

# game/script.rpy:8579
translate japanese choice_18_B_6466c893:

    masaru "I go to the music club just to honor my mother's gift to me…"
    masaru "私は私の母の贈り物に敬意を表して音楽クラブに行く…"

# game/script.rpy:8581
translate japanese choice_18_B_922bee39:

    toshu "Y-you mean…"
    toshu "あなたは…"

# game/script.rpy:8583
translate japanese choice_18_B_6fe99bbb:

    toshu "Your piano was sold to the music club?!"
    toshu "あなたのピアノは音楽クラブに売られましたか？"

# game/script.rpy:8585
translate japanese choice_18_B_6a3b4d8a:

    masaru "Being evicted from the club… and now… how can I even play the piano?!"
    masaru "クラブから追い出されて…今、ピアノを弾く方法は？"

# game/script.rpy:8587
translate japanese choice_18_B_dc793b6a:

    toshu_t "I knew there's no other possible thing to say to ease Masaru's pain…"
    toshu_t "まるの痛みを和らげるために言えることはほかにありません…"

# game/script.rpy:8588
translate japanese choice_18_B_2f100f4d:

    toshu "Give me your hand…"
    toshu "あなたの手をくれ…"

# game/script.rpy:8590
translate japanese choice_18_B_42ff0966:

    masaru "W-what are you doing…?"
    masaru "あなたは何をしていますか？"

# game/script.rpy:8592
translate japanese choice_18_B_1d615bb5:

    toshu_t "I held Masaru's hand and wrapped his injury to heal."
    toshu_t "私はマサルの手を握り、怪我を包んで癒した。"

# game/script.rpy:8594
translate japanese choice_18_B_c7d29a33:

    toshu "I've seen this kind of wound from my friend before…"
    toshu "私は友人からこの種の傷を見たことがあります…"

# game/script.rpy:8595
translate japanese choice_18_B_e8604d40:

    toshu "It will heal… I promise…"
    toshu "それは癒すだろう…私は約束…"

# game/script.rpy:8597
translate japanese choice_18_B_33504c5d:

    masaru "Toshu…"
    masaru "Toshu…"

# game/script.rpy:8600
translate japanese choice_18_B_1b558ae0:

    toshu "This is not the Masaru I know!"
    toshu "これは私が知っているマサルじゃない！"

# game/script.rpy:8602
translate japanese choice_18_B_4edd671a:

    toshu "The Captain Masaru that I know always bears a smile no matter what!"
    toshu "私が知っているキャプテンマサルは、いつも笑顔を浮かべています！"

# game/script.rpy:8603
translate japanese choice_18_B_ebe186bf:

    toshu "Even through the harshest situations!"
    toshu "最も厳しい状況でさえ！"

# game/script.rpy:8605
translate japanese choice_18_B_e350335d:

    masaru "But the game…"
    masaru "しかし、ゲーム…"

# game/script.rpy:8606
translate japanese choice_18_B_eadd4c44:

    masaru "Everyone trained for it so hard… and because of me we lost just like that…"
    masaru "誰もがとても頑張ったように訓練してくれました。"

# game/script.rpy:8608
translate japanese choice_18_B_ac9ea4e7:

    toshu "It is not your fault that we lost the game!"
    toshu "私たちがゲームを失ったのはあなたのせいではありません！"

# game/script.rpy:8609
translate japanese choice_18_B_fae8ffe5:

    toshu "We lost because we lacked training!"
    toshu "私たちは訓練が足りなかったので失った！"

# game/script.rpy:8611
translate japanese choice_18_B_d37e5d13:

    toshu "Please do not blame yourself!"
    toshu "あなた自身を責めないでください！"

# game/script.rpy:8613
translate japanese choice_18_B_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:8618
translate japanese choice_18_B_0bad06bd:

    ichiru "Masaru, are you alright?!"
    ichiru "大丈夫、大丈夫ですか？"

# game/script.rpy:8620
translate japanese choice_18_B_532ce23c:

    toshu "He will be fine, Ichiru. I put first aid for now."
    toshu "彼はうまくいくだろう、Ichiru。今は応急処置をしました。"

# game/script.rpy:8622
translate japanese choice_18_B_95938fb4:

    ichiru "You made us so worried!"
    ichiru "あなたは私たちをとても心配させました！"

# game/script.rpy:8624
translate japanese choice_18_B_e564a245:

    genji "We better get you to the nearest hospital!"
    genji "私たちは最寄りの病院にお任せください！"

# game/script.rpy:8625
translate japanese choice_18_B_ae8c5612:

    genji "Let's get that injury healed by doctors!"
    genji "医者が怪我を負わせよう！"

# game/script.rpy:8627
translate japanese choice_18_B_14795222:

    masaru "I'm sorry…"
    masaru "ごめんなさい…"

# game/script.rpy:8629
translate japanese choice_18_B_189c460d:

    genji "Oh come on, Masaru! We all know this is nobody's fault!"
    genji "名前：ああ、マサル！私たちは皆、これが誰のせいではないことを知っています！"

# game/script.rpy:8631
translate japanese choice_18_B_a4cb1c95:

    ichiru "Genji! I'll bring him to the hospital now!"
    ichiru "Genji物語！今すぐ彼を病院に連れて行くよ！"

# game/script.rpy:8636
translate japanese choice_18_B_fe5c57ab:

    genji "I hope Masaru passed his exam…"
    genji "マサルが試験に合格したらいいな…"

# game/script.rpy:8638
translate japanese choice_18_B_23239484:

    # toshu "Coach…?"
    toshu "コーチ…？"

# game/script.rpy:8639
translate japanese choice_18_B_5fad2ab1:

    genji "The last thing he'd hate himself for is if he failed that remedial…"
    genji "彼がその矯正に失敗した場合、彼が自分自身を憎む最後のことは…"

# game/script.rpy:8640
translate japanese choice_18_B_5f45ae37:

    genji "He might quit baseball… or worse even the music club…"
    genji "彼は野球を辞めるかもしれないし…悪いことに音楽クラブも…"

# game/script.rpy:8642
translate japanese choice_18_B_5748c75b:

    toshu "Coach! Don't say that!"
    toshu "コーチ！それを言わないで！"

# game/script.rpy:8643
translate japanese choice_18_B_30937353:

    genji "I just really feel so bad… I can't help but think negative…"
    genji "私は本当にとても悪いと感じています…私は助けることはできませんが否定的だと思う…"

# game/script.rpy:8644
translate japanese choice_18_B_31d52218:

    genji "Let's just hope for Masaru's quick recovery!"
    genji "マサルの速い回復を望みましょう！"

# game/script.rpy:8646
translate japanese choice_18_B_55b04593:

    toshu_t "The thought of Masaru leaving baseball and music sent chills to my spine…"
    toshu_t "野球と音楽を残していた元禄の考えは、私の背骨に悪寒を送りました…"

# game/script.rpy:8647
translate japanese choice_18_B_216b54a0:

    toshu_t "I won't let today's loss affect the future!"
    toshu_t "私は今日の損失が未来に影響を与えることはありません！"

# game/script.rpy:8648
translate japanese choice_18_B_e35f9758:

    toshu_t "I'm sure Masaru will recover soon!"
    toshu_t "私はまさるがすぐに回復すると確信しています！"

# game/script.rpy:8661
translate japanese route_ichiru_7_6c447d7f:

    toshu "Ichiru! What are you doing here?"
    toshu "Ichiru！あなたはここで何をしているの？"

# game/script.rpy:8662
translate japanese route_ichiru_7_5fe8daed:

    ichiru "Hehe! Isn't it obvious?"
    ichiru "Hehe！それは明白ではありませんか？"

# game/script.rpy:8664
translate japanese route_ichiru_7_c97f95eb:

    ichiru "I am ditching practice! What else do I look like I'm doing?"
    ichiru "私は練習を断っている！私は何をしているのですか？"

# game/script.rpy:8666
translate japanese route_ichiru_7_d56ab578:

    toshu "Ichiru! I thought you're already taking what we say seriously!"
    toshu "Ichiru！私はあなたがすでに真剣に言っていることを取っていると思った！"

# game/script.rpy:8667
translate japanese route_ichiru_7_36dee4c7:

    toshu "You heard Coach right?"
    toshu "コーチの話は聞いたことがありますか？"

# game/script.rpy:8668
translate japanese route_ichiru_7_efbec2df:

    toshu "We only have less than a week to train!"
    toshu "我々は訓練するのに一週間もかからない！"

# game/script.rpy:8670
translate japanese route_ichiru_7_2a22e1cb:

    ichiru "I know, I know!"
    ichiru "分かった分かった！"

# game/script.rpy:8671
translate japanese route_ichiru_7_1e7ffa36:

    ichiru "But before we go to training…"
    ichiru "しかし、私たちがトレーニングに行く前に…"

# game/script.rpy:8673
translate japanese route_ichiru_7_de3eac4f:

    ichiru "I have something important to tell you!"
    ichiru "私はあなたに伝えることが重要な何かを持っています！"

# game/script.rpy:8676
translate japanese route_ichiru_7_7bdaffa0:

    ichiru "I finally made an argument with my dad!"
    ichiru "私はついに父と議論をしました！"

# game/script.rpy:8678
translate japanese route_ichiru_7_7f6c64c2:

    ichiru "I told him about me having my own rights for myself!"
    ichiru "私は自分のために自分自身の権利を持っていることについて私に彼に言った！"

# game/script.rpy:8680
translate japanese route_ichiru_7_eaa7b37e:

    ichiru "He did listen to me!"
    ichiru "彼は私に耳を傾けた！"

# game/script.rpy:8682
translate japanese route_ichiru_7_0f0e98b8:

    toshu "R-really?! What did he say?"
    toshu "本当に？彼が何を言ったの？"

# game/script.rpy:8684
translate japanese route_ichiru_7_8160cd2b:

    ichiru "He said I only have to prove myself to him that I'm responsible for my own decisions…"
    ichiru "彼は、私が自分の判断を担当していることを自分に証明しなければならないと言いました…"

# game/script.rpy:8686
translate japanese route_ichiru_7_0b92d206:

    ichiru "If I can show him that, he will negotiate that marriage for me!"
    ichiru "私が彼にそれを示すことができれば、彼は私のためにその結婚を交渉するでしょう！"

# game/script.rpy:8688
translate japanese route_ichiru_7_3d234247:

    toshu "T-That's really great news Ichiru!"
    toshu "T-本当にすばらしいニュースだよ！"

# game/script.rpy:8690
translate japanese route_ichiru_7_7bed9a16:

    ichiru "I really told him how I love being here with my friends and the baseball club!"
    ichiru "私は本当に私の友人や野球クラブとここにいることを愛する方法を彼に言った！"

# game/script.rpy:8692
translate japanese route_ichiru_7_9aa95010:

    ichiru "He was so shocked when he heard I have friends."
    ichiru "彼は友達がいると聞いてとてもショックを受けました。"

# game/script.rpy:8693
translate japanese route_ichiru_7_3d8572aa:

    ichiru "He didn't believe me at first."
    ichiru "彼は当初私を信じていませんでした。"

# game/script.rpy:8695
translate japanese route_ichiru_7_90fc0e33:

    ichiru "But I explained to him how my friends changed me to a better person!"
    ichiru "しかし、私は友人が私をより良い人間に変えてくれたことを彼に説明しました！"

# game/script.rpy:8697
translate japanese route_ichiru_7_b24d4485:

    toshu "R-really?"
    toshu "本当に？"

# game/script.rpy:8699
translate japanese route_ichiru_7_a30229b9:

    ichiru "And he said, for me to prove myself!"
    ichiru "そして、彼は私が自分自身を証明するために言った！"

# game/script.rpy:8700
translate japanese route_ichiru_7_ff6adb42:

    ichiru "So he offered me a deal!"
    ichiru "だから彼は私に契約を申し出た！"

# game/script.rpy:8702
translate japanese route_ichiru_7_014e4543:

    ichiru "If I win the tournament and keep my grades up at the same time, he might reconsider my transfer!"
    ichiru "私がトーナメントに勝ち、同時にグレードアップを続けると、彼は私の移籍を再考するかもしれません！"

# game/script.rpy:8704
translate japanese route_ichiru_7_2b3bb826:

    toshu "That's really awesome!"
    toshu "それは本当に素晴らしいです！"

# game/script.rpy:8706
translate japanese route_ichiru_7_1343cf43:

    ichiru "I know right, Toshu!"
    ichiru "私は右、Toshuを知っている！"

# game/script.rpy:8711
translate japanese route_ichiru_7_42dc2017:

    ichiru "I can finally stay here with everyone! Especially with you!"
    ichiru "私は最終的にみんなとここに泊まることができます！特にあなたと！"

# game/script.rpy:8712
translate japanese route_ichiru_7_ce785eda:

    toshu "I'm so happy for you, Ichiru!"
    toshu "私はあなたにとても満足しています、Ichiru！"

# game/script.rpy:8713
translate japanese route_ichiru_7_77e523f4:

    ichiru "But… first…"
    ichiru "でもまず…"

# game/script.rpy:8715
translate japanese route_ichiru_7_d64c5b72:

    toshu "I-Ichiru!!!"
    toshu "I-Ichiru!!!"

# game/script.rpy:8716
translate japanese route_ichiru_7_d8196759:

    toshu_t "I was so shocked."
    toshu_t "私はとてもショックを受けました。"

# game/script.rpy:8717
translate japanese route_ichiru_7_e703e906:

    toshu_t "Ichiru was so overwhelmned with joy…"
    toshu_t "イチルは喜びで圧倒されました…"

# game/script.rpy:8718
translate japanese route_ichiru_7_c89268cc:

    toshu_t "He couldn't contain his excitement…"
    toshu_t "彼は興奮を含んでいなかった…"

# game/script.rpy:8719
translate japanese route_ichiru_7_04d064af:

    toshu_t "He pinned me down to the floor…"
    toshu_t "彼は床に私をピンで留めた…"

# game/script.rpy:8720
translate japanese route_ichiru_7_0295f8b0:

    toshu_t "…licked my neck and then bit my ear…"
    toshu_t "…私の首を舐めて、耳を噛んで…"

# game/script.rpy:8725
translate japanese route_ichiru_7_80468139:

    toshu "Ichiru!!!"
    toshu "イチル！"

# game/script.rpy:8726
translate japanese route_ichiru_7_ea36267f:

    toshu "W-we can't do this now!"
    toshu "名前：私たちは今これを行うことはできません！"

# game/script.rpy:8727
translate japanese route_ichiru_7_113c9ebe:

    toshu "What if someone comes in?"
    toshu "誰かが来たらどうしますか？"

# game/script.rpy:8728
translate japanese route_ichiru_7_d524d0f0:

    ichiru "Don't worry! I locked all the doors!"
    ichiru "心配しないでください！私はすべてのドアをロック！"

# game/script.rpy:8730
translate japanese route_ichiru_7_7c480bc0:

    toshu "But still…!"
    toshu "それでも…！"

# game/script.rpy:8731
translate japanese route_ichiru_7_24552593:

    ichiru "Just relax!"
    ichiru "ただリラックス！"

# game/script.rpy:8732
translate japanese route_ichiru_7_066bfcd7:

    ichiru "I will go in now!"
    ichiru "私は今行くつもりです！"

# game/script.rpy:8734
translate japanese route_ichiru_7_12f36263:

    toshu "Uwahh…!"
    toshu "うわー…！"

# game/script.rpy:8735
translate japanese route_ichiru_7_fc5cc731:

    toshu "I-Ichiru!"
    toshu "I-Ichiru!"

# game/script.rpy:8736
translate japanese route_ichiru_7_5096e64f:

    ichiru "Hehe!"
    ichiru "Hehe！"

# game/script.rpy:8738
translate japanese route_ichiru_7_e8a01799:

    toshu "Wahh! S-slow down…!"
    toshu "ワー！ S  - スローダウン！"

# game/script.rpy:8739
translate japanese route_ichiru_7_9979652e:

    ichiru "I-I don't want to…! You're so tight!"
    ichiru "私はしたくない…！あなたはとてもタイトです！"

# game/script.rpy:8740
translate japanese route_ichiru_7_bcb69b75:

    toshu "I'm gonna--"
    toshu "するつもり -"

# game/script.rpy:8742
translate japanese route_ichiru_7_3e30cba9:

    ichiru "Ahh…!"
    ichiru "ああ…！"

# game/script.rpy:8743
translate japanese route_ichiru_7_d53d8991:

    toshu "Hnn!"
    toshu "Hnn！"

# game/script.rpy:8745
translate japanese route_ichiru_7_6bcb742d:

    toshu "Wahh…"
    toshu "ワフ…"

# game/script.rpy:8746
translate japanese route_ichiru_7_09446d3a:

    ichiru "Wow, you came all over your face!"
    ichiru "うわー、あなたはあなたの顔の上に来ました！"

# game/script.rpy:8747
translate japanese route_ichiru_7_1b116ae1:

    ichiru "You look so cute as always!"
    ichiru "あなたはいつものようにとてもかわいく見えます！"

# game/script.rpy:8748
translate japanese route_ichiru_7_a10a7050:

    toshu "Let's get cleaned up… someone might--"
    toshu "きれいにしましょう…誰かが -"

# game/script.rpy:8749
translate japanese route_ichiru_7_8a2354ae:

    ichiru "I'm not done yet! I'm gonna go in again!"
    ichiru "まだ終えていません！私は再び入るつもりです！"

# game/script.rpy:8751
translate japanese route_ichiru_7_fd6ca27a:

    toshu "E-eh?!"
    toshu "E-え？"

# game/script.rpy:8752
translate japanese route_ichiru_7_52672071:

    ichiru "Aaah… it's so slippery inside!"
    ichiru "ああ…それはとても滑りやすい！"

# game/script.rpy:8754
translate japanese route_ichiru_7_f59ccad1:

    toshu "Ichiru if you keep this up I'm gonna--"
    toshu "Ichiruこれを保つなら、私は行くつもりだ -"

# game/script.rpy:8756
translate japanese route_ichiru_7_540afdb6:

    toshu "Ahhh!"
    toshu "ああ！"

# game/script.rpy:8761
translate japanese route_ichiru_7_b1b027b9:

    toshu "Ukh!!"
    toshu "えー！"

# game/script.rpy:8762
translate japanese route_ichiru_7_24b49115:

    ichiru "Ahh…"
    ichiru "ああ…"

# game/script.rpy:8764
translate japanese route_ichiru_7_3e316246:

    ichiru "Whew… you came straight into your mouth…"
    ichiru "あなたはあなたの口にまっすぐに入ってきました…"

# game/script.rpy:8765
translate japanese route_ichiru_7_e06915d5:

    toshu "Haa… it tastes so weird…"
    toshu "ハア…それは奇妙な味が…"

# game/script.rpy:8766
translate japanese route_ichiru_7_faf2efb7:

    ichiru "Let's clean up now! We don't want anyone to see us!"
    ichiru "今すぐクリーンアップしよう！誰も私たちを見ることを望んでいません！"

# game/script.rpy:8774
translate japanese route_ichiru_7_f492d684:

    toshu "That was close…"
    toshu "ぎりぎりでした…"

# game/script.rpy:8776
translate japanese route_ichiru_7_f367913d:

    ichiru "Hahaha! How does your own cum taste like?"
    ichiru "ははは！どのように自分の兼味が好きですか？"

# game/script.rpy:8778
translate japanese route_ichiru_7_120af381:

    toshu "Eww! The taste won't get off my throat…"
    toshu "Eww！味は私の喉から降りません…"

# game/script.rpy:8781
translate japanese route_ichiru_7_fc7232c2:

    ichiru "Owiee!!"
    ichiru "オーエーエ!!"

# game/script.rpy:8783
translate japanese route_ichiru_7_5e23ff77:

    toshu "That's for suddenly pinning me to the ground!"
    toshu "それは突然私を地面に固定するためのものです！"

# game/script.rpy:8785
translate japanese route_ichiru_7_b9caba34:

    toshu "Are you out of your mind? Someone could have seen us!"
    toshu "あなたは正気ですか？誰かが私たちを見たことがありました！"

# game/script.rpy:8787
translate japanese route_ichiru_7_c2b1639e:

    ichiru "Hnn! But you liked it, right, Toshu?"
    ichiru "Hnn！しかし、あなたはそれを好きだった、右、Toshu？"

# game/script.rpy:8788
translate japanese route_ichiru_7_57a799a7:

    ichiru "You came two times!"
    ichiru "あなたは2回来ました！"

# game/script.rpy:8790
translate japanese route_ichiru_7_2c6cb286:

    toshu "Stupid Ichiru!"
    toshu "愚かなIchiru！"

# game/script.rpy:8792
translate japanese route_ichiru_7_0e56695d:

    ichiru "I got too excited because I will be staying here with you!"
    ichiru "私はあなたと一緒にここに滞在するので、あまりにも興奮した！"

# game/script.rpy:8794
translate japanese route_ichiru_7_7875e9e2:

    toshu "Don't make any hasty conclusions!"
    toshu "急いで結論を出してはいけません！"

# game/script.rpy:8795
translate japanese route_ichiru_7_cdfe7816:

    toshu "We haven't even won the competition yet!"
    toshu "我々はまだ競争に勝たなかった！"

# game/script.rpy:8797
translate japanese route_ichiru_7_06e4740b:

    ichiru "Don't worry about it!"
    ichiru "ご心配なく！"

# game/script.rpy:8798
translate japanese route_ichiru_7_bb68014a:

    ichiru "If I get my head in the game, there's no way we would lose!"
    ichiru "私がゲームで頭を上げれば、失うことはない！"

# game/script.rpy:8800
translate japanese route_ichiru_7_02c316a0:

    toshu "*sigh*"
    toshu "*一息*"

# game/script.rpy:8801
translate japanese route_ichiru_7_c9bcaf66:

    toshu "Don't ever do--"
    toshu "しないでください -"

# game/script.rpy:8804
translate japanese route_ichiru_7_f869e93c:

    random "ICHIRU!!!"
    random "ICHIRU !!!"

# game/script.rpy:8806
translate japanese route_ichiru_7_9080267b:

    ichiru "Crap!"
    ichiru "うんざり！"

# game/script.rpy:8812
translate japanese route_ichiru_7_8f49164d:

    genji "THERE YOU ARE, YOU LITTLE RUNT!"
    genji "そこにはあなたがいない！"

# game/script.rpy:8814
translate japanese route_ichiru_7_5f1e5d93:

    genji "How dare you ditch practice when the tournament is just weeks away!"
    genji "トーナメントが数週間離れた頃、あなたはどんなに練習をしますか？"

# game/script.rpy:8815
translate japanese route_ichiru_7_2008c3ca:

    genji "I'm going to give you a beating!"
    genji "私はあなたに打撃を与えるつもりです！"

# game/script.rpy:8817
translate japanese route_ichiru_7_22bd816e:

    genji "AND YOU TOO, KANADA!"
    genji "あなたも、カナディア！"

# game/script.rpy:8819
translate japanese route_ichiru_7_c2c469ea:

    toshu "Wahh!!"
    toshu "ワー！"

# game/script.rpy:8821
translate japanese route_ichiru_7_c259d3e7:

    genji "I expected much from you!! I never thought you will be ditching with Ichiru!"
    genji "私はあなたから多くを期待していました！私はあなたがIchiruと逃げることは決して考えなかった！"

# game/script.rpy:8823
translate japanese route_ichiru_7_9c1ef23a:

    genji "You two will have to practice until your body can't move anymore!!"
    genji "あなたの体はもう動けなくなるまで、2人は練習をしなければなりません!!"

# game/script.rpy:8825
translate japanese route_ichiru_7_7319a71d:

    ichiru "Whaaaattt…"
    ichiru "Whaaaattt …"

# game/script.rpy:8827
translate japanese route_ichiru_7_1d1b0963:

    genji "No complaining!"
    genji "不平を言うな！"

# game/script.rpy:8829
translate japanese route_ichiru_7_0702723c:

    genji "Now scram! Go to the field and change into your uniforms!"
    genji "今スクラム！フィールドに移動し、あなたのユニフォームに変更！"

# game/script.rpy:8839
translate japanese route_ichiru_7_69cd537f:

    masaru "Where have you guys been?"
    masaru "あなたはどこにいましたか？"

# game/script.rpy:8841
translate japanese route_ichiru_7_520a7721:

    masaru "Coach was looking all over the place for you guys."
    masaru "コーチはあなたのために場所のいたるところを見ていた。"

# game/script.rpy:8843
translate japanese route_ichiru_7_50ab8bc2:

    ichiru "We were just studying."
    ichiru "私たちはただ勉強していました。"

# game/script.rpy:8845
translate japanese route_ichiru_7_a91fb387:

    toshu "W-wha…"
    toshu "イン義和…"

# game/script.rpy:8847
translate japanese route_ichiru_7_08c22ec1:

    ichiru "Shh!"
    ichiru "シャー！"

# game/script.rpy:8849
translate japanese route_ichiru_7_5b69e8ae:

    genji "I caught these two hooligans ditching practice!"
    genji "私はこれらの2つのフーリガンが練習を断つのを捕まえた！"

# game/script.rpy:8851
translate japanese route_ichiru_7_3b7e91c3:

    masaru "Ichiru, when will you ever learn? You even dragged Toshu with you."
    masaru "イチル、いつまでに学びますか？あなたはあなたと一緒にToshuを引っ張った。"

# game/script.rpy:8852
translate japanese route_ichiru_7_94ac8ca5:

    masaru "*sigh* You guys should really worry about what's to come…"
    masaru "*一息*あなたは本当に何が来るのか心配すべきです…"

# game/script.rpy:8854
translate japanese route_ichiru_7_f057e635:

    masaru "Finals is around the corner and our tournament is just days away…"
    masaru "決勝戦が終わり、私たちのトーナメントは数日離れたところにあります…"

# game/script.rpy:8856
translate japanese route_ichiru_7_96e497f8:

    toshu "I'm very sorry, Masaru! I will never do it again!"
    toshu "大変申し訳ありません、大変！私は再びそれをやりません！"

# game/script.rpy:8858
translate japanese route_ichiru_7_d6f198a0:

    masaru "Don't worry… I understand, it's Coach Genji you should be worried about."
    masaru "心配しないで…分かります、それはあなたが心配する必要がありますGenjiコーチです。"

# game/script.rpy:8859
translate japanese route_ichiru_7_4680e975:

    genji "Yeah! Tsk! Even for Masaru who's studying hard is now here today with us for practice!"
    genji "うん！ Tsk！勉強している元郎が今日も今日は練習のためにここにいる！"

# game/script.rpy:8861
translate japanese route_ichiru_7_b4dddc1c:

    toshu "Ah! That's right! I heard from Ms. Saki she was helping you study."
    toshu "ああ！そのとおり！私は咲さんからあなたが勉強を手伝っていると聞きました。"

# game/script.rpy:8863
translate japanese route_ichiru_7_9fe3b45d:

    masaru "Yes, I finished my tutorial session quite early, so I went here to spend time for practice."
    masaru "はい、私は非常に早い時間にチュートリアルセッションを終了したので、私は練習のために時間を過ごすためにここに行きました。"

# game/script.rpy:8865
translate japanese route_ichiru_7_cd7a66f7:

    masaru "It was really nice of her to tutor me even though it was her birthday."
    masaru "たとえそれが彼女の誕生日だったとしても、私を教えるのは本当に素敵でした。"

# game/script.rpy:8867
translate japanese route_ichiru_7_7fb55df0:

    genji "I-it was her birthday!??"
    genji "私は彼女の誕生日だった！"

# game/script.rpy:8869
translate japanese route_ichiru_7_d708f64c:

    genji "Eh… so that's why she was acting weird all morning."
    genji "ええと…そういうわけで、彼女は午前中に変な演技をしていたのです。"

# game/script.rpy:8871
translate japanese route_ichiru_7_47f1b41f:

    toshu "Eh? Coach, I thought you remembered it?"
    toshu "え？コーチ、私はあなたがそれを思いついたと思った？"

# game/script.rpy:8872
translate japanese route_ichiru_7_9553a0d1:

    toshu "Ms. Saki even told me you gave her a box of chocolate."
    toshu "サキさんはチョコレートの箱を彼女に贈ったと言ってくれました。"

# game/script.rpy:8874
translate japanese route_ichiru_7_0708ef7e:

    genji "She was acting weird at the faculty lounge today…"
    genji "彼女は今日教室で奇妙な演技をしていました…"

# game/script.rpy:8875
translate japanese route_ichiru_7_623ba34f:

    genji "She suddenly got all clingy and kept asking me what the date it is today."
    genji "彼女は突然すべてがぎこちなくなり、今日の日時を尋ねてきた。"

# game/script.rpy:8876
translate japanese route_ichiru_7_07d3ec95:

    genji "I was pretty confused with what was going on with her."
    genji "私は彼女と何が起こっているのかかなり混乱していた。"

# game/script.rpy:8878
translate japanese route_ichiru_7_f4061f10:

    genji "So I thought she was just hungry, then I gave her the leftover chocolates I brought with me."
    genji "だから私は彼女がちょうど空腹であると思った、私は私に持ってきた残りのチョコレートを彼女に贈った。"

# game/script.rpy:8880
translate japanese route_ichiru_7_3a3de118:

    ichiru "Wow… talk about heartless."
    ichiru "うわー…無情について話す。"

# game/script.rpy:8882
translate japanese route_ichiru_7_373cac34:

    genji "ANYWAY!"
    genji "いつでも！"

# game/script.rpy:8884
translate japanese route_ichiru_7_fecca712:

    genji "Ichiru! Toshu! Get into your gear, we are wasting daylight!"
    genji "Ichiru！ Toshu！あなたのギアに乗って、私たちは昼光を浪費しています！"

# game/script.rpy:8885
translate japanese route_ichiru_7_ecf36632:

    genji "I've already wasted enough time from finding you both!"
    genji "私はすでにあなたの両方を見つけるのに十分な時間を無駄にしました！"

# game/script.rpy:8887
translate japanese route_ichiru_7_8994667e:

    genji "I'm totally going to beat you guys to a pulp!"
    genji "私は完全にパルプにあなたを打つつもりだ！"

# game/script.rpy:8904
translate japanese after_minigame_8_35f16171:

    toshu_t "Everything went pretty awesome!"
    toshu_t "すべてはかなり素晴らしい行った！"

# game/script.rpy:8905
translate japanese after_minigame_8_bc8f944b:

    toshu_t "With that intense practice, I can see the improvement in everyone's skill."
    toshu_t "その激しい練習で、私は全員のスキルの向上を見ることができます。"

# game/script.rpy:8906
translate japanese after_minigame_8_e64d360f:

    toshu_t "Especially Ichiru! He is really hyped up today!"
    toshu_t "特にIchiru！彼は今日本当に誇張されている！"

# game/script.rpy:8916
translate japanese after_minigame_8_5e91a7d2:

    toshu "Wahh… I'm so tired…"
    toshu "うーん…疲れている…"

# game/script.rpy:8917
translate japanese after_minigame_8_cef53903:

    masaru "Yeah… me too…"
    masaru "うん、私も…"

# game/script.rpy:8918
translate japanese after_minigame_8_4b4d7e5c:

    ichiru "You guys are tired? Pfft! I could do this all day!"
    ichiru "あなたたちは疲れていますか？ Pfft！私は一日中これをすることができました！"

# game/script.rpy:8920
translate japanese after_minigame_8_ee1ee725:

    masaru "Woah, Ichiru! What's with the sudden burst of energy?"
    masaru "うわー、Ichiru！突然のエネルギーの爆発はどうですか？"

# game/script.rpy:8922
translate japanese after_minigame_8_5f9ebe97:

    ichiru "I'm going to win that tournament for Toshu!"
    ichiru "私はToshuのためのそのトーナメントに勝つつもりです！"

# game/script.rpy:8923
translate japanese after_minigame_8_e7e2841f:

    ichiru "C'mon GENJI! Let's practice some more!"
    ichiru "ガンジー！もう少し練習しましょう！"

# game/script.rpy:8925
translate japanese after_minigame_8_26d129b1:

    genji "I've never seen you this motivated to practice."
    genji "私はあなたがこれを練習する意欲を見たことはありません。"

# game/script.rpy:8926
translate japanese after_minigame_8_51edc81e:

    genji "Toshu, what exactly did you do to him to make him so motivated?"
    genji "Toshu、あなたは彼をそんなに動機づけさせるために彼に何をしたのですか？"

# game/script.rpy:8928
translate japanese after_minigame_8_1349c49f:

    ichiru "What? I don't get you, Genji!"
    ichiru "何？私はあなたを取得していません、Genji！"

# game/script.rpy:8929
translate japanese after_minigame_8_8ab95cf6:

    ichiru "I laze around, you complain! Now that I'm working hard and you are complaining again!"
    ichiru "私は眠い、あなたは文句を言う！今私は懸命に働いていて、もう一度不平を言っている！"

# game/script.rpy:8931
translate japanese after_minigame_8_f50ad7a7:

    genji "I'm not complaining! I'm actually very happy you are motivated!"
    genji "私は不平を言っていません！私は実際にあなたが動機付けされていることをとてもうれしく思います！"

# game/script.rpy:8933
translate japanese after_minigame_8_d444e9e5:

    ichiru "Hmph!"
    ichiru "Hmph！"

# game/script.rpy:8935
translate japanese after_minigame_8_c41d4843:

    genji "OKAY! Let's call it a day!"
    genji "はい！それでは、一日を呼び出してみましょう！"

# game/script.rpy:8937
translate japanese after_minigame_8_e7373fc9:

    genji "Masaru, your remedial exam is within this week, right? I hope Saki taught you everything you need to know!"
    genji "マサル、あなたの修復試験は今週中ですよね？サキはあなたに知っておくべきことを教えてくれることを願っています！"

# game/script.rpy:8939
translate japanese after_minigame_8_8345f7fa:

    genji "I wish you good luck!"
    genji "がんばって！"

# game/script.rpy:8941
translate japanese after_minigame_8_71e1f573:

    toshu "Good luck, Masaru!"
    toshu "幸運、大丈夫！"

# game/script.rpy:8943
translate japanese after_minigame_8_df4591db:

    ichiru "Don't let us down Masaru!"
    ichiru "私達を元羅させてはいけません！"

# game/script.rpy:8945
translate japanese after_minigame_8_16f0c78c:

    masaru "Okay! Thank you, guys!"
    masaru "はい！君たちありがとう！"

# game/script.rpy:8947
translate japanese after_minigame_8_94fae5d6:

    toshu_t "Masaru had his exam on the last day of the week"
    toshu_t "マサルは週の最後の日に彼の試験を受けた"

# game/script.rpy:8948
translate japanese after_minigame_8_0d51bcc7:

    toshu_t "The week has passed and we've been extra busy for training for the tournament."
    toshu_t "週が終わり、トーナメントのトレーニングに余裕がありました。"

# game/script.rpy:8949
translate japanese after_minigame_8_3d4b40cf:

    toshu_t "The first days of training were really intense, but we've gotten used to it eventually!"
    toshu_t "トレーニングの最初の日は本当に激しかったですが、最終的にはそれに慣れました！"

# game/script.rpy:8950
translate japanese after_minigame_8_4ae88387:

    toshu_t "Today I got myself well-rested! I'm ready!"
    toshu_t "今日私は自分自身をよく休んだ！準備できました！"
