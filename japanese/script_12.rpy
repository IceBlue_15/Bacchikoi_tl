# game/script.rpy:7446
translate japanese choice_15_end_59783e94:

    # toshu "Wah!! Ms. Saki, I'm sorry I'm late!"
    toshu "ウワー！ Saki先生、遅れてすみません！"

# game/script.rpy:7448
translate japanese choice_15_end_16b01e00:

    # saki "Ah! Mr. Kanada!"
    saki "ああ！ Kanadaくん！"

# game/script.rpy:7450
translate japanese choice_15_end_96d7fb72:

    toshu "I overslept…! Yesterday's baseball practice really drained me out…"
    toshu "寝坊した…！昨日の野球の練習は本当に私を流出させました…"

# game/script.rpy:7451
translate japanese choice_15_end_8b6c54f0:

    saki "Weren't you informed?"
    saki "あなたは知らなかったか？"

# game/script.rpy:7453
translate japanese choice_15_end_3081ab4b:

    # toshu "Eh?"
    toshu "え？"

# game/script.rpy:7454
translate japanese choice_15_end_9bd74eca:

    # saki "I cancelled our class for today."
    saki "今日の私の授業はお休みよ。"

# game/script.rpy:7456
translate japanese choice_15_end_3919e185:

    # saki "Why you ask? Because today is my birthday!"
    saki "何でかって？ それは私の誕生日だからです！"

# game/script.rpy:7458
translate japanese choice_15_end_1da66466:

    # toshu "W-wow! Happy birthday Ms. Saki!"
    toshu "うわぁ！ お誕生日おめでとうございます、Saki先生！"

# game/script.rpy:7460
translate japanese choice_15_end_b14b5e4c:

    toshu_t "Haiiizz… I rushed to school for no reason…"
    toshu_t "ハイイゼス…私は何の理由もなく学校に駆けつけた…"

# game/script.rpy:7461
translate japanese choice_15_end_e62288a5:

    # saki "Why thank you, Mr. Kanada!"
    saki "あら、ありがとうKanadaくん！"

# game/script.rpy:7462
translate japanese choice_15_end_ed88d50e:

    # saki "The principal said I can have this day for free!"
    saki "校長がお休みをくれたの！"

# game/script.rpy:7463
translate japanese choice_15_end_bafb9ebd:

    # saki "But I can't leave my precious students alone, so I still went to school!"
    saki "でも、大事な生徒たちを放って置けないから学校に来たってわけ！"

# game/script.rpy:7465
translate japanese choice_15_end_90a04840:

    toshu "That's Great Ms. Saki--"
    toshu "それは偉大な咲さんです"

# game/script.rpy:7466
translate japanese choice_15_end_6646d6b5:

    saki "Ah!~ my precious students! A lot of them gave me presents!"
    saki "ああ！〜私の貴重な学生！彼らの多くが私にプレゼントをくれました！"

# game/script.rpy:7467
translate japanese choice_15_end_f5e01e1b:

    # saki "I'm so glad they remembered my birthday!"
    saki "みんなが私の誕生日を覚えてくれててとっても嬉しいわ！"

# game/script.rpy:7468
translate japanese choice_15_end_057348d4:

    saki "Guess who else remembered my birthday!"
    saki "私の誕生日を誰が思い出したのでしょうか？"

# game/script.rpy:7470
translate japanese choice_15_end_86104a35:

    # toshu "Um--"
    toshu "えーと…"

# game/script.rpy:7471
translate japanese choice_15_end_c6c9f363:

    # saki "Mr. Tadano!"
    saki "Tadanoさんよ！"

# game/script.rpy:7473
translate japanese choice_15_end_44e1a5d4:

    saki "I can't believe Mr. Genji Tadano remembered my birthday!"
    saki "忠野Genjiが私の誕生日を思い出したとは思えない！"

# game/script.rpy:7474
translate japanese choice_15_end_3d9a40c7:

    saki "And not only that! He gave me a box of chocolate too!"
    saki "それだけではない！彼は私にチョコレートの箱もくれました！"

# game/script.rpy:7475
translate japanese choice_15_end_6185ae90:

    # saki "Wahh~!"
    saki "キャ〜！"

# game/script.rpy:7476
translate japanese choice_15_end_0f59a300:

    # saki "Mr. Kanada! What do you think?"
    saki "Kanadaくん！ どう思う？"

# game/script.rpy:7478
translate japanese choice_15_end_0255e7cc:

    # saki "Will me and Mr. Tadano…"
    saki "私とTadanoさんが将来…"

# game/script.rpy:7480
translate japanese choice_15_end_58df54bc:

    # saki "Wahh! I can't say it out loud~"
    saki "キャー！ そんなこと口に出せないわ〜"

# game/script.rpy:7482
translate japanese choice_15_end_e528f8da:

    # toshu "Well, Ms. Saki--"
    toshu "えーと、Saki先生…"

# game/script.rpy:7483
translate japanese choice_15_end_64102ac5:

    # saki "I should really give him something special in return as well!"
    saki "何かスペシャルなものをお返ししなくっちゃ！"

# game/script.rpy:7484
translate japanese choice_15_end_500a4f20:

    # saki "What do you think Mr. Kanada?"
    saki "どうしましょ？ Kanadaくん。"

# game/script.rpy:7485
translate japanese choice_15_end_6fe19a8c:

    # saki "What is Mr. Tadano's type? What does he like?"
    saki "Tadanoさんのタイプってどんな人かしら？ 何が好きなの？"

# game/script.rpy:7487
translate japanese choice_15_end_7fff7a9a:

    # toshu "Ms. Saki!"
    toshu "Saki先生！"

# game/script.rpy:7488
translate japanese choice_15_end_cdb68f0a:

    # toshu "P-please calm down!"
    toshu "お、落ち着いてください！"

# game/script.rpy:7489
translate japanese choice_15_end_a92b6be1:

    # toshu "I can't keep up!"
    toshu "話しについて行けません！"

# game/script.rpy:7491
translate japanese choice_15_end_ae801483:

    # saki "Oh dear! It seems like I got carried away again!"
    saki "あらまあ！ また我を忘れちゃったわ！"

# game/script.rpy:7492
translate japanese choice_15_end_964eee2d:

    # saki "I'm terribly sorry, Mr. Kanada!"
    saki "ホントにごめんなさいね、Kanadaくん！"

# game/script.rpy:7493
translate japanese choice_15_end_3c89db13:

    # saki "Ehem…"
    saki "えっと…"

# game/script.rpy:7494
translate japanese choice_15_end_87e4d61e:

    # saki "Mr. Yanai already went to the baseball field with Mr. Tadano!"
    saki "YanaiくんはTadanoさんと一緒にもう野球場に行ったわよ！"

# game/script.rpy:7496
translate japanese choice_15_end_e0fc2e04:

    saki "Heehee~! Mr. Tadano~"
    saki "Heehee〜！氏只野〜"

# game/script.rpy:7498
translate japanese choice_15_end_0c32d605:

    saki "I actually came from the library to get some books for Mr. Nakahara!"
    saki "実際に図書館から来て、中原さんの本を手に入れました！"

# game/script.rpy:7500
translate japanese choice_15_end_48af1b76:

    # toshu "Ohh…! So Masaru's inside our classroom!"
    toshu "ああ…！ じゃあMasaruさんは教室にいるんですね！"

# game/script.rpy:7501
translate japanese choice_15_end_864f4ecb:

    saki "Mr. Tadano asked me to be his tutor with some subjects!"
    saki "忠野先生は私にいくつかの科目を教えるように教えてくれました。"

# game/script.rpy:7503
translate japanese choice_15_end_c5793434:

    toshu_t "Ah… so Coach Genji… basically bribed Ms. Saki…?"
    toshu_t "ああ…だから元祖コーチ…基本的に咲さんに賄賂を…？"

# game/script.rpy:7504
translate japanese choice_15_end_02398a33:

    # saki "You could join us if you want!"
    saki "もしよかったら一緒に来てもいいのよ！"

# game/script.rpy:7506
translate japanese choice_15_end_ebfa6c18:

    # toshu "Oh, I see Ms. Saki! Thank you very much for the information!"
    toshu "ああ、わかりましたSaki先生！ 教えてくれてありがとうございます！"

# game/script.rpy:7507
translate japanese choice_15_end_61695adc:

    saki "You are most welcome Mr. Kanada!"
    saki "あなたは金田氏を大歓迎しています！"

# game/script.rpy:7508
translate japanese choice_15_end_89e5faed:

    saki "I will take my leave then!"
    saki "私はそれから私の休暇を取る！"

# game/script.rpy:7511
translate japanese choice_15_end_5d3abac2:

    # toshu "Whew… Ms. Saki sure talks A LOT."
    toshu "うへぇ… Saki先生はホントによく喋るな。"

# game/script.rpy:7512
translate japanese choice_15_end_9d784664:

    # toshu "I wonder where I should go next…"
    toshu "どこに行ったらいいだろう…"

# game/script.rpy:7538
translate japanese choice_16_A_99d891fc:

    toshu "I should go to the baseball field and practice with Ichiru and Coach Genji."
    toshu "私は野球場に行き、イチールとGenjiと一緒に練習しなければなりません。"

# game/script.rpy:7539
translate japanese choice_16_A_7f943521:

    toshu "Besides…"
    toshu "しかも…"

# game/script.rpy:7540
translate japanese choice_16_A_7d076f66:

    toshu "The tournament is near! I must practice with the team!"
    toshu "トーナメントが近くに！私はチームと一緒に練習する必要があります！"

# game/script.rpy:7542
translate japanese choice_16_A_b7ad1442:

    toshu "As the pitcher, everyone's counting on me!"
    toshu "投手として、誰もが私を信じている！"

# game/script.rpy:7544
translate japanese choice_16_A_5e1d006c:

    random "Psst! Tofu! Over here!"
    random "Psst！Tofu！ここに！"

# game/script.rpy:7545
translate japanese choice_16_A_f59895d6:

    toshu "I-Ichiru!?"
    toshu "I-Ichiru!?"

# game/script.rpy:7547
translate japanese choice_16_A_90d10aad:

    toshu "What are you doi--"
    toshu "あなたは何ですか？"

# game/script.rpy:7553
translate japanese choice_16_B_387f7fb4:

    toshu "I want to visit Captain Masaru in the classroom!"
    toshu "私は教室でマサル大尉を訪ねたい！"

# game/script.rpy:7555
translate japanese choice_16_B_0a4c1aea:

    toshu "He must be working so hard to pass his exam!"
    toshu "彼は試験に合格するためにとても熱心に働いているに違いない！"

# game/script.rpy:7556
translate japanese choice_16_B_40b9f482:

    toshu "The least I can do is to visit him."
    toshu "私ができることは、彼を訪問することです。"

# game/script.rpy:7558
translate japanese choice_16_B_8cd82407:

    toshu "And besides, Ms. Saki invited me to join them!"
    toshu "それに、咲さんが私に招待されました！"

# game/script.rpy:7564
translate japanese choice_16_C_7d076f66:

    toshu "The tournament is near! I must practice with the team!"
    toshu "トーナメントが近くに！私はチームと一緒に練習する必要があります！"

# game/script.rpy:7565
translate japanese choice_16_C_3a1004c5:

    toshu "I don't want to disappoint my teammates! Especially Coach Genji and Ichiru!"
    toshu "私はチームメートを失望させたくありません！特にGenjiとIchiru先生！"

# game/script.rpy:7566
translate japanese choice_16_C_c413d501:

    toshu "I need to win the tournament!"
    toshu "私はトーナメントに勝つ必要があります！"

# game/script.rpy:7568
translate japanese choice_16_C_b7ad1442:

    toshu "As the pitcher, everyone's counting on me!"
    toshu "投手として、誰もが私を信じている！"

# game/script.rpy:7570
translate japanese choice_16_C_5e1d006c:

    random "Psst! Tofu! Over here!"
    random "Psst！Tofu！ここに！"

# game/script.rpy:7571
translate japanese choice_16_C_f59895d6:

    toshu "I-Ichiru!?"
    toshu "I-Ichiru!?"

# game/script.rpy:7573
translate japanese choice_16_C_90d10aad:

    toshu "What are you doi--"
    toshu "あなたは何ですか？"

# game/script.rpy:7579
translate japanese choice_16_D_54b24b7d:

    toshu "I should take this chance to study harder for the final exam!"
    toshu "私は最後の試験のためにもっと勉強するこの機会を取るべきです！"

# game/script.rpy:7580
translate japanese choice_16_D_b556ba89:

    toshu "Ms. Saki and Masaru are in the classroom…"
    toshu "サキとマサルさんは教室にいます…"

# game/script.rpy:7582
translate japanese choice_16_D_110e19a8:

    toshu "Masaru must be working so hard to pass his exam!"
    toshu "マサルは、彼の試験に合格するためにとても大変努力しなければなりません！"

# game/script.rpy:7583
translate japanese choice_16_D_40b9f482:

    toshu "The least I can do is to visit him."
    toshu "私ができることは、彼を訪問することです。"

# game/script.rpy:7585
translate japanese choice_16_D_f9f3f99c:

    toshu "Maybe I should join them!"
    toshu "多分私はそれらに参加する必要があります！"

# game/script.rpy:7597
translate japanese route_masaru_7_636af217:

    masaru "Oh! Toshu!"
    masaru "Oh! Toshu!"

# game/script.rpy:7598
translate japanese route_masaru_7_3f0f9ba4:

    masaru "What's up? I thought you have practice today with Ichiru and Coach?"
    masaru "どうしたの？イチールとコーチと一緒に今日練習したと思った？"

# game/script.rpy:7600
translate japanese route_masaru_7_d31abc7c:

    toshu "Yes I do! But I wanted to visit you and see how you are doing with your studies!"
    toshu "ええ、やりますよ！しかし、私はあなたを訪問して、あなたの学業をどうやってやっているのかを見たいと思いました！"

# game/script.rpy:7601
translate japanese route_masaru_7_b2c81dbc:

    toshu "I met Ms. Saki outside. She invited me to join in as well!"
    toshu "私は外の咲さんに会った。彼女は私にも参加するように私を招待しました！"

# game/script.rpy:7603
translate japanese route_masaru_7_b171198f:

    toshu "By the way, where is she?"
    toshu "ところで、彼女はどこですか？"

# game/script.rpy:7605
translate japanese route_masaru_7_b7aee009:

    masaru "Ah. She just delivered these books from the library, and she said she forgot some of it so she went back… again."
    masaru "ああ彼女はちょうど図書館からこれらの本を配達しました。そして、彼女はそれをいくつか忘れてしまったと言いました。"

# game/script.rpy:7607
translate japanese route_masaru_7_23f82cec:

    masaru "Thank you for visiting me though!"
    masaru "私を訪問してくれてありがとう！"

# game/script.rpy:7608
translate japanese route_masaru_7_6fa02650:

    # masaru "It regenerated all of my motivation, haha!"
    masaru "またやる気が出てきたよ、ハハハ。"

# game/script.rpy:7610
translate japanese route_masaru_7_0e053e41:

    # toshu "It's my pleasure, Masaru!"
    toshu "そう言ってもらえると嬉しいです、Masaruさん。"

# game/script.rpy:7611
translate japanese route_masaru_7_bb7b296a:

    # toshu "Do you mind if I join you?"
    toshu "ここにいてもいいですか？"

# game/script.rpy:7613
translate japanese route_masaru_7_12f3050a:

    # masaru "Please have a seat, you can sit beside me."
    masaru "こっちに来て隣に座りなよ。"

# game/script.rpy:7615
translate japanese route_masaru_7_4f6973c3:

    # toshu "Okay!"
    toshu "はい！"

# game/script.rpy:7616
translate japanese route_masaru_7_e358f53d:

    # toshu "So, what are you studying?"
    toshu "で、今何を勉強してるんですか？"

# game/script.rpy:7618
translate japanese route_masaru_7_919726df:

    # masaru "Mathematics… it's very hard…"
    masaru "数学…めっちゃ難しい…"

# game/script.rpy:7620
translate japanese route_masaru_7_5e3aaaf4:

    # toshu "Ahh… that subject can indeed be difficult sometimes…"
    toshu "ああ…確かに数学ってよく分かんなくなるときがありますよね…"

# game/script.rpy:7622
translate japanese route_masaru_7_26060b43:

    toshu "But if you listen very closely to your teacher and do some sample exercise, you will get the hang of it!"
    toshu "しかし、あなたが先生に非常に耳を傾けて、いくつかのサンプル演習をすれば、あなたはそれを掛けるでしょう！"

# game/script.rpy:7624
translate japanese route_masaru_7_53ca73e8:

    # masaru "Hnn… it's so confusing with all the letters and numbers, and stuff…"
    masaru "はあ… 文字やら数字やらで混乱するよ…"

# game/script.rpy:7625
translate japanese route_masaru_7_30a69484:

    # masaru "I thought math was just numbers…"
    masaru "数学って数字だけだと思ってたよ…"

# game/script.rpy:7627
translate japanese route_masaru_7_643d8926:

    toshu "You can do it, Masaru! As long as you keep doing your best, I'm sure you'll get it!"
    toshu "あなたはそれを行うことができます、元！あなたが最善を尽くしている限り、私はあなたがそれを得ることを確信しています！"

# game/script.rpy:7629
translate japanese route_masaru_7_29d24f5e:

    masaru "Thanks! I won't let you down!"
    masaru "ありがとう！私はあなたを失望させません！"

# game/script.rpy:7630
translate japanese route_masaru_7_4c2e7e21:

    masaru "I promise, I will do everything I can to pass this exam!"
    masaru "私は約束します、私はこの試験に合格するために私ができること全てをやります！"

# game/script.rpy:7632
translate japanese route_masaru_7_ffe43923:

    toshu "Ah! So that means you'll be able to go back to your music club!"
    toshu "ああ！つまり、あなたはあなたの音楽クラブに戻ることができます！"

# game/script.rpy:7634
translate japanese route_masaru_7_1f14485e:

    masaru "Hopefully they will accept me back…"
    masaru "うまくいけば彼らは私を受け入れるだろう…"

# game/script.rpy:7635
translate japanese route_masaru_7_86983776:

    masaru "I won't be able to play anymore if they don't."
    masaru "彼らがいなくても私はもう遊べないでしょう。"

# game/script.rpy:7637
translate japanese route_masaru_7_8eb69698:

    toshu "Ehh? What do you mean? Can't you play at home?"
    toshu "え？どういう意味ですか？家で遊ぶことはできませんか？"

# game/script.rpy:7638
translate japanese route_masaru_7_78f53326:

    toshu "I thought your mom gave you a piano when you were young and she taught you using that."
    toshu "私は、あなたのお母さんが、あなたが若い時にあなたにピアノをくれたと思って、あなたはそれを使って教えてくれました。"

# game/script.rpy:7640
translate japanese route_masaru_7_5a79e331:

    masaru "Yes, she did, she gave that piano as a present when I was seven years old…"
    masaru "はい、彼女は、私が7歳のときに、ピアノをプレゼントとして与えました…"

# game/script.rpy:7641
translate japanese route_masaru_7_d1d12898:

    masaru "Aside from the knowledge in music she passed down to me…"
    masaru "音楽の知識とは別に、彼女は私に渡った…"

# game/script.rpy:7643
translate japanese route_masaru_7_4e4b64e7:

    masaru "That piano was the only memento my mother left for me."
    masaru "そのピアノは私の母が私のために残した唯一の記念品でした。"

# game/script.rpy:7645
translate japanese route_masaru_7_7f25dfd1:

    masaru "But unfortunately, when she got sick… we were in a financial crisis."
    masaru "しかし、残念なことに、彼女が病気になったとき…私たちは金融危機にあった。"

# game/script.rpy:7646
translate japanese route_masaru_7_cc82dc9f:

    masaru "My dad does not know anymore where to get the money for her medical expense…"
    masaru "私のお父さんは、医療費のためにお金をどこで手に入れるかわからない…"

# game/script.rpy:7648
translate japanese route_masaru_7_f99e6bc4:

    masaru "He thought it would be a good idea to sell the piano for additional cash…"
    masaru "彼は追加の現金のためにピアノを売るのは良い考えだと思った…"

# game/script.rpy:7649
translate japanese route_masaru_7_1b8a1015:

    masaru "Well, it did cover the expense… but not for long."
    masaru "まあ、それは費用をカバーしました…しかし、長くはありません。"

# game/script.rpy:7651
translate japanese route_masaru_7_ecabc18f:

    masaru "She died eventually…"
    masaru "彼女は結局亡くなりました…"

# game/script.rpy:7653
translate japanese route_masaru_7_02e53259:

    # toshu "That's… terrible…"
    toshu "それは…可哀そうに…"

# game/script.rpy:7655
translate japanese route_masaru_7_4806ae47:

    # toshu "I'm sorry to make you bring that up…"
    toshu "そんな話をさせてしまってすいません…"

# game/script.rpy:7657
translate japanese route_masaru_7_60899d28:

    masaru "It's okay… at least now someone is here to listen to me…"
    masaru "それは大丈夫です…少なくとも今は誰かが私の声を聞くためにここにいる…"

# game/script.rpy:7659
translate japanese route_masaru_7_5a18a8dd:

    # masaru "It's all in the past. I should cherish what I have today!"
    masaru "それはみんな昔の話しだ。今日を大事にしなきゃ！"

# game/script.rpy:7661
translate japanese route_masaru_7_852d66a3:

    # toshu_t "The quality that I really like about Masaru."
    toshu_t "だから僕はMasaruさんが大好きなんだ。"

# game/script.rpy:7662
translate japanese route_masaru_7_6dc7c266:

    # toshu_t "Is that, no matter how much hardships he has gone through…"
    toshu_t "過去にどんなつらいことがあったとしても…"

# game/script.rpy:7663
translate japanese route_masaru_7_09189e64:

    # toshu_t "He can still show that smile…"
    toshu_t "それでも笑顔を絶やさない…"

# game/script.rpy:7664
translate japanese route_masaru_7_881d8d14:

    # toshu_t "The smile that looks so cheerful and pure."
    toshu_t "Masaruさんの笑顔は明るく純粋だ。"

# game/script.rpy:7666
translate japanese route_masaru_7_33504c5d:

    # masaru "Toshu…"
    masaru "Toshu…"

# game/script.rpy:7667
translate japanese route_masaru_7_adf0894a:

    # masaru "Can… I ask you something?"
    masaru "ちょっと…聞いてもいいか？"

# game/script.rpy:7669
translate japanese route_masaru_7_15aa0825:

    # toshu "What is it, Masaru…?"
    toshu "何ですか？ Masaruさん…"

# game/script.rpy:7672
translate japanese route_masaru_7_c833a8c2:

    # masaru "What do you think about me?"
    masaru "俺のことどんな風に思ってる？"

# game/script.rpy:7674
translate japanese route_masaru_7_c9672609:

    # toshu "Haa? Where did that question come from?"
    toshu "ええっ？ 何でそんなこと聞くんですか？"

# game/script.rpy:7676
translate japanese route_masaru_7_f4786062:

    # masaru "I was worried that you might look down on me now…"
    masaru "俺は軽蔑されてるんじゃないかと心配だったんだ…"

# game/script.rpy:7677
translate japanese route_masaru_7_5911dd89:

    # masaru "I mean, well…"
    masaru "つまり、その…"

# game/script.rpy:7679
translate japanese route_masaru_7_476b8948:

    # masaru "I failed my exams… I get lost so easily… I don't know how to do simple camping things… and I am not even that talented at baseball--"
    masaru "俺は試験に失敗したし…すぐ道に迷うし…簡単なキャンプの知識もないし…野球の才能もないし…"

# game/script.rpy:7681
translate japanese route_masaru_7_0f695117:

    # toshu "Stupid Masaru!"
    toshu "バカなこと言わないで下さい、Masaruさん！"

# game/script.rpy:7683
translate japanese route_masaru_7_f425b10a:

    # toshu "If I was looking down on you, I wouldn't even bother to come here to cheer you up!"
    toshu "軽蔑なんかしてたら、わざわざここに励ましに来たりしませんよ！"

# game/script.rpy:7685
translate japanese route_masaru_7_994c4f3c:

    # toshu "And don't count what you do not have…"
    toshu "自分にないものばかりに目を向けないで下さい…"

# game/script.rpy:7686
translate japanese route_masaru_7_a0ff9520:

    toshu "Look at the good things that you achieved! The blessings that made you who you are!"
    toshu "あなたが達成した良いものを見てください！あなたの人生を祝福してくれました！"

# game/script.rpy:7688
translate japanese route_masaru_7_dd2dd5da:

    toshu "I always have fun talking with you! You always know what to say to cheer me up."
    toshu "私はいつもあなたと話すのが楽しみです！あなたはいつも私を応援する言葉を知っています。"

# game/script.rpy:7690
translate japanese route_masaru_7_b593b39b:

    toshu "Even though you have tons of problems. I rarely saw you sad. You just smile it away like it was nothing."
    toshu "あなたにはたくさんの問題がありますが。私はめったにあなたを悲しんだ。それは何もなかったようにあなたはただそれを笑顔にします。"

# game/script.rpy:7692
translate japanese route_masaru_7_fda149e1:

    toshu "And you are very talented too you know!"
    toshu "そして、あなたはあまりにも才能があります！"

# game/script.rpy:7693
translate japanese route_masaru_7_2c8f1087:

    toshu "I never saw anyone who can play the piano that well!"
    toshu "私はピアノをよく演奏できる人は見たことがありません！"

# game/script.rpy:7695
translate japanese route_masaru_7_4f624b4a:

    # masaru "Haha! You think so, Toshu?"
    masaru "ハハハ！ そんな風に思ってくれてたんだ、Toshu。"

# game/script.rpy:7697
translate japanese route_masaru_7_b3b2dd7c:

    # toshu "I know so!"
    toshu "そうですよ！"

# game/script.rpy:7699
translate japanese route_masaru_7_cb2fb3db:

    # masaru "Thank you…"
    masaru "ありがとう…"

# game/script.rpy:7704
translate japanese route_masaru_7_a7573951:

    masaru "You know…"
    masaru "ええと…"

# game/script.rpy:7705
translate japanese route_masaru_7_38465abe:

    masaru "You are the only person who made me feel this special…"
    masaru "あなたは私をこの特別な気分にさせた唯一の人です…"

# game/script.rpy:7706
translate japanese route_masaru_7_e8970c45:

    masaru "I'm really happy to have met you…"
    masaru "私はあなたに会って本当に嬉しいです…"

# game/script.rpy:7708
translate japanese route_masaru_7_d85d4c85:

    masaru "Heyy…"
    masaru "Heyy …"

# game/script.rpy:7710
translate japanese route_masaru_7_81ce3e50:

    masaru "Can I request something?"
    masaru "何かリクエストできますか？"

# game/script.rpy:7712
translate japanese route_masaru_7_1eec8803:

    toshu "M-Masaru…!"
    toshu "M-Masaru …！"

# game/script.rpy:7713
translate japanese route_masaru_7_d8196759:

    toshu_t "I was so shocked."
    toshu_t "私はとてもショックを受けました。"

# game/script.rpy:7714
translate japanese route_masaru_7_8c7e6ede:

    toshu_t "Masaru was obviously heating up…"
    toshu_t "マサルは明らかに熱くなっていた…"

# game/script.rpy:7715
translate japanese route_masaru_7_137a0359:

    toshu_t "He suddenly lifted me by my legs…"
    toshu_t "彼は突然私の足で私を持ち上げた。"

# game/script.rpy:7716
translate japanese route_masaru_7_e82b4e01:

    toshu_t "…kissed my neck and licked my ear…"
    toshu_t "…私の首にキスして、私の耳をなめる…"

# game/script.rpy:7721
translate japanese route_masaru_7_772c562a:

    toshu "Wahh!! Captain!"
    toshu "ワー！キャプテン！"

# game/script.rpy:7722
translate japanese route_masaru_7_ea36267f:

    toshu "W-we can't do this now!"
    toshu "名前：私たちは今これを行うことはできません！"

# game/script.rpy:7723
translate japanese route_masaru_7_ceadc617:

    toshu "What if Ms. Saki comes back?!"
    toshu "咲さんが帰ってきたら？"

# game/script.rpy:7724
translate japanese route_masaru_7_b507ec2e:

    toshu "A-and… don't we have to study…?"
    toshu "A-and …私たちは勉強しなくていいのですか？"

# game/script.rpy:7725
translate japanese route_masaru_7_0965ff21:

    masaru "I promise, I'll be quick…"
    masaru "私は約束する、私は速くなるだろう…"

# game/script.rpy:7727
translate japanese route_masaru_7_90a74b56:

    toshu "Hnn…!"
    toshu "Hnn …！"

# game/script.rpy:7728
translate japanese route_masaru_7_92d50b44:

    toshu_t "Masaru's dick is so big… I can feel it rubbing against my hole…"
    toshu_t "マサルの巨根はとても大きいです…私は私の穴にこすって感じることができます…"

# game/script.rpy:7729
translate japanese route_masaru_7_c89422e9:

    masaru "Just relax!"
    masaru "ただリラックス！"

# game/script.rpy:7730
translate japanese route_masaru_7_1752a4ff:

    masaru "I will go in now…!"
    masaru "私は今行くつもりです…！"

# game/script.rpy:7732
translate japanese route_masaru_7_6fa23f82:

    toshu "Mnnnhh!!"
    toshu "Mnnnhh !!"

# game/script.rpy:7733
translate japanese route_masaru_7_4447d23a:

    masaru "See? It's in."
    masaru "見る？入った。"

# game/script.rpy:7734
translate japanese route_masaru_7_08940a3a:

    toshu "M-Masaru's so… big…"
    toshu "M-Masaruはそう大…"

# game/script.rpy:7735
translate japanese route_masaru_7_fece8f01:

    masaru "I'll start moving now, Toshu…"
    masaru "私は今、Toshu、移動を開始します…"

# game/script.rpy:7737
translate japanese route_masaru_7_793c850a:

    toshu "Nghh!!"
    toshu "Nghh !!"

# game/script.rpy:7738
translate japanese route_masaru_7_eb727108:

    masaru "Haa…!"
    masaru "ああ…！"

# game/script.rpy:7739
translate japanese route_masaru_7_420b6da2:

    toshu_t "Masaru's totally different when it comes with this kind of stuff…"
    toshu_t "マサルはこの種のものになると全く違う…"

# game/script.rpy:7740
translate japanese route_masaru_7_621d3eff:

    toshu_t "He's so vigorous."
    toshu_t "彼はとても活発です。"

# game/script.rpy:7741
translate japanese route_masaru_7_69634731:

    masaru "D-does it hurt…?"
    masaru "D-それは痛いです…？"

# game/script.rpy:7742
translate japanese route_masaru_7_4af5afe5:

    toshu "N-no, Masaru…"
    toshu "N-no, Masaru…"

# game/script.rpy:7743
translate japanese route_masaru_7_bc1760ae:

    masaru "I'm gonna--"
    masaru "するつもり -"

# game/script.rpy:7745
translate japanese route_masaru_7_e319c071:

    masaru "Erghh!!"
    masaru "Erghh !!"

# game/script.rpy:7746
translate japanese route_masaru_7_95595919:

    toshu "Wahh…!"
    toshu "ワハ！"

# game/script.rpy:7747
translate japanese route_masaru_7_c05ebdab:

    toshu_t "I totally felt the strong gush of hot liquid inside me."
    toshu_t "私は完全に私の中の熱い液体の強い噴出を感じました。"

# game/script.rpy:7749
translate japanese route_masaru_7_6afcf1be:

    toshu "Haaa…"
    toshu "Haaa …"

# game/script.rpy:7750
translate japanese route_masaru_7_06df3127:

    masaru "I… I'm sorry I came inside you…"
    masaru "私は…あなたの中に入ってすみません…"

# game/script.rpy:7751
translate japanese route_masaru_7_d2c9708f:

    masaru "You look so cute as always!"
    masaru "あなたはいつものようにとてもかわいく見えます！"

# game/script.rpy:7752
translate japanese route_masaru_7_07b1145c:

    toshu "M-Masaru… more… please!"
    toshu "M-Masaru …もっと…してください！"

# game/script.rpy:7753
translate japanese route_masaru_7_609f06de:

    masaru "A-are you sure?"
    masaru "A-本当ですか？"

# game/script.rpy:7754
translate japanese route_masaru_7_d7f299c0:

    toshu "Yes… please!"
    toshu "はい、お願いします！"

# game/script.rpy:7755
translate japanese route_masaru_7_2d4e5e69:

    masaru "Alright!"
    masaru "よかった！"

# game/script.rpy:7757
translate japanese route_masaru_7_d53d8991:

    toshu "Hnn!"
    toshu "Hnn！"

# game/script.rpy:7758
translate japanese route_masaru_7_a82f1c87:

    masaru "Ugh… are you sure you're okay…? It's totally slippery inside…!"
    masaru "あなたは大丈夫ですか？それは内部に完全に滑りやすい…！"

# game/script.rpy:7759
translate japanese route_masaru_7_19a95d17:

    toshu "Y-yes… don't worry…"
    toshu "Y-yes …心配しないで…"

# game/script.rpy:7761
translate japanese route_masaru_7_83c86f2b:

    masaru "You're really sexy, Toshu…!"
    masaru "あなたは本当にセクシーです、Toshu…！"

# game/script.rpy:7762
translate japanese route_masaru_7_b4cad85d:

    toshu "Masaru…! I'm going to come…!"
    toshu "マサル…！私は来るつもりだ…！"

# game/script.rpy:7764
translate japanese route_masaru_7_a5dfb105:

    toshu "Nghhh!!!"
    toshu "Nghhh !!!"

# game/script.rpy:7765
translate japanese route_masaru_7_b7f040b3:

    masaru "Aaahh…!"
    masaru "ああ…！"

# game/script.rpy:7767
translate japanese route_masaru_7_14e60831:

    masaru "Haa… I can feel you're so full inside…"
    masaru "ハア…私はあなたがとても満腹だと感じることができます…"

# game/script.rpy:7768
translate japanese route_masaru_7_9624cab8:

    toshu "Mnn… it's dripping…"
    toshu "Mnn …落ちている…"

# game/script.rpy:7769
translate japanese route_masaru_7_23dcbc6a:

    toshu "Masaru came a lot…"
    toshu "まさるがいっぱい来た…"

# game/script.rpy:7770
translate japanese route_masaru_7_30b6bf24:

    masaru "I'll take it out slowly…"
    masaru "私はそれをゆっくりと取り出します…"

# game/script.rpy:7772
translate japanese route_masaru_7_c5fde213:

    toshu "Mnghh…"
    toshu "Mnghh …"

# game/script.rpy:7773
translate japanese route_masaru_7_d4907fd1:

    masaru "There we go…"
    masaru "そこに行く…"

# game/script.rpy:7774
translate japanese route_masaru_7_d1dfe0a4:

    toshu "It felt awesome, Masaru…"
    toshu "それは素晴らしい、マサルを感じた…"

# game/script.rpy:7775
translate japanese route_masaru_7_b0c67b9e:

    masaru "Let's clean up then? We have to go back to studying…"
    masaru "それからクリーンアップしましょうか？私たちは勉強に戻る必要があります…"

# game/script.rpy:7785
translate japanese route_masaru_7_ddc0a1db:

    saki "Oh I'm so sorry! It took me so long to come back!"
    saki "ああ、大変申し訳ありません！私は戻ってくるのにずっと時間がかかった！"

# game/script.rpy:7786
translate japanese route_masaru_7_1108a9dc:

    # saki "The book I was looking for was too hard to find!"
    saki "探してた本は見つからなかったわ！"

# game/script.rpy:7788
translate japanese route_masaru_7_4f778a64:

    # saki "Oh, Mr. Kanada. What a pleasant surprise seeing you here!"
    saki "あら、Kanadaくん。ここにいるなんてビックリしちゃった！"

# game/script.rpy:7790
translate japanese route_masaru_7_0b89df2d:

    # toshu "Hello Ms. Saki!"
    toshu "こんにちは、Saki先生！"

# game/script.rpy:7791
translate japanese route_masaru_7_94f69ec2:

    # toshu "I hope you don't mind if I stay to help Masaru with his studies!"
    toshu "ここでMasaruさんの勉強の手伝いをしててもいいですか？"

# game/script.rpy:7793
translate japanese route_masaru_7_bcad723a:

    # saki "Oh! You are most welcome to join!"
    saki "ああ！ もちろん大歓迎よ！"

# game/script.rpy:7794
translate japanese route_masaru_7_28a9b84b:

    # random "OH HELL NO HE'S NOT!"
    random "この俺が許さねえぞ！"

# game/script.rpy:7797
translate japanese route_masaru_7_6df4a51a:

    saki "M-Mr. Tadano!"
    saki "Ta、Tadanoさん！"

# game/script.rpy:7800
translate japanese route_masaru_7_8f49164d:

    # genji "THERE YOU ARE, YOU LITTLE RUNT!"
    genji "ここにいたのか、チビすけが！"

# game/script.rpy:7802
translate japanese route_masaru_7_5f1e5d93:

    # genji "How dare you ditch practice when the tournament is just weeks away!"
    genji "数週間後に大会があるってぇのに、よくもまあサボれるもんだ！"

# game/script.rpy:7803
translate japanese route_masaru_7_2008c3ca:

    # genji "I'm going to give you a beating!"
    genji "罰を与えねぇといけないみたいだな！"

# game/script.rpy:7805
translate japanese route_masaru_7_c2c469ea:

    # toshu "Wahh!!"
    toshu "うわぁ！！"

# game/script.rpy:7807
translate japanese route_masaru_7_4dd2c5d9:

    # genji "I expected much from you!! I never thought you will be ditching like Ichiru did!"
    genji "お前には期待してたのに！ まさかIchiruみたいにサボるとは思ってもみなかったぞ！"

# game/script.rpy:7809
translate japanese route_masaru_7_1299136f:

    # genji "You will have to practice until your body can't move anymore!!"
    genji "足腰立たなくなるまでシゴいてやるぞ！！"

# game/script.rpy:7811
translate japanese route_masaru_7_7b4ae5de:

    # toshu "Whaa…"
    toshu "あわわ…"

# game/script.rpy:7813
translate japanese route_masaru_7_accb71b4:

    # genji "Now don't use that pitiful face again on me!"
    genji "金輪際、その情けないツラするんじゃねぇぞ！"

# game/script.rpy:7815
translate japanese route_masaru_7_99cb4e70:

    # genji "Scram! Go to the field and change into your uniforms!"
    genji "走れ！ 練習場に行ってユニフォームに着替えろ！"

# game/script.rpy:7817
translate japanese route_masaru_7_6522d0ad:

    # masaru "Coach! Stop bullying Toshu!"
    masaru "コーチ！ Toshuをいじめるのは止めて下さい！"

# game/script.rpy:7819
translate japanese route_masaru_7_cd1ebd0d:

    # genji "I'm not bullying him! I'm just lecturing him since he is ditching."
    genji "誰もいじめたりなんかしてねぇ！ サボってる奴に教えてやってるんだ。"

# game/script.rpy:7821
translate japanese route_masaru_7_9d58b068:

    # genji "Saki, may I borrow Masaru too for a moment?"
    genji "Saki先生、ちょっとMasaruをお借りしてもいいですか？"

# game/script.rpy:7823
translate japanese route_masaru_7_0bd0a941:

    # saki "Ahh…"
    saki "はわわ…"

# game/script.rpy:7824
translate japanese route_masaru_7_a0141fb1:

    # genji "Saki…?"
    genji "Saki先生…？"

# game/script.rpy:7826
translate japanese route_masaru_7_4f8ded22:

    # saki "Wah! Yes yes you may have me! I MEAN HIM!"
    saki "わわっ！ は、はいどうぞ私を連れてって下さい！ じゃなくて彼を！"

# game/script.rpy:7827
translate japanese route_masaru_7_14284fa6:

    # saki "M-Mr. Nakahara, GO WITH Mr. Tadano!"
    saki "Na、Nakaharaくん、Tadanoさんに付いて行きなさい！"

# game/script.rpy:7829
translate japanese route_masaru_7_1f96dbe0:

    # masaru "Why do you need me Coach?"
    masaru "コーチ、どうして私が行かなくちゃいけないんですか？"

# game/script.rpy:7831
translate japanese route_masaru_7_ff47657d:

    # genji "We need a catcher of course!"
    genji "もちろんキャッチャーがいるからだ！"

# game/script.rpy:7833
translate japanese route_masaru_7_2b729e41:

    # genji "What would we do without you?!"
    genji "お前なしでどうしろと？"

# game/script.rpy:7835
translate japanese route_masaru_7_3623f98c:

    genji "I'll make sure Saki here will give you a special tutorial service after practice!"
    genji "私はここで咲さんが練習後に特別なチュートリアルサービスを提供することを確かめます！"

# game/script.rpy:7837
translate japanese route_masaru_7_9146c13e:

    # genji "Right, Saki?!"
    genji "ですね？ Saki先生。"

# game/script.rpy:7839
translate japanese route_masaru_7_43227ff3:

    # saki "OF COURSE!"
    saki "も、もちろんですわ！"

# game/script.rpy:7841
translate japanese route_masaru_7_69f7e253:

    # masaru "Alright, Coach! C'mon, Toshu! Let's go to the field!"
    masaru "わかりました、コーチ！ さあToshu、練習場に行くぞ！"

# game/script.rpy:7843
translate japanese route_masaru_7_081ff38f:

    genji "Hngh… Masaru you are spoiling him too much!"
    genji "Hngh …マサルはあまりにも彼を台無しにしている！"

# game/script.rpy:7853
translate japanese route_masaru_7_1c58e628:

    # ichiru "Hi, Toshu! Hi, Masaru!"
    ichiru "やあToshu、Masaru！"

# game/script.rpy:7855
translate japanese route_masaru_7_59817792:

    # ichiru "GENJI was looking all over the place for you, Toshu!"
    ichiru "Genjiがお前のこと探しまわってたぞ、Toshu！"

# game/script.rpy:7857
translate japanese route_masaru_7_690c4b44:

    toshu "I was joining Masaru and Ms. Saki with their study."
    toshu "私はMasaruとMs. Sakiに彼らの研究に加わりました。"

# game/script.rpy:7859
translate japanese route_masaru_7_f25aeb8a:

    ichiru "Oh! That's right! Masaru has a remedial exam to pass!"
    ichiru "ああ！そのとおり！マサルに合格するための修復試験があります！"

# game/script.rpy:7861
translate japanese route_masaru_7_2aefaf8b:

    masaru "You didn't have to say that."
    masaru "あなたはそれを言う必要はありませんでした。"

# game/script.rpy:7863
translate japanese route_masaru_7_c3f600d2:

    genji "Masaru was the one studying."
    genji "マサルは勉強していた人だった。"

# game/script.rpy:7865
translate japanese route_masaru_7_a2be6da9:

    genji "I caught Toshu here, ditching practice!"
    genji "私はここで戸州を捕まえ、練習を辞めました！"

# game/script.rpy:7867
translate japanese route_masaru_7_e19ad486:

    ichiru "What?! Toshu?! Really?"
    ichiru "何？！ Toshu ?!本当に？"

# game/script.rpy:7869
translate japanese route_masaru_7_3cf6fc63:

    ichiru "I thought you told me to not skip work?"
    ichiru "私はあなたが仕事を飛ばさないように私に言ったと思った？"

# game/script.rpy:7871
translate japanese route_masaru_7_dcb88f74:

    masaru "He was just worried about how was I doing. Please don't be hard on him!"
    masaru "彼はちょうど私がやっていることを心配していた。彼には大変なことはしないでください！"

# game/script.rpy:7873
translate japanese route_masaru_7_c628b10b:

    masaru "My remedial exam is around the corner and our tournament is just two weeks away…"
    masaru "私の修復試験はまさに隅々にあり、私たちのトーナメントはわずか2週間離れています…"

# game/script.rpy:7875
translate japanese route_masaru_7_7bbeb962:

    masaru "It was really nice of Ms. Saki to tutor me even though it was her birthday."
    masaru "それは彼女の誕生日だったにもかかわらず私を教えるために咲さんの本当に良かったです。"

# game/script.rpy:7877
translate japanese route_masaru_7_b370df54:

    genji "I-It was her birthday!??"
    genji "私は彼女の誕生日だった！"

# game/script.rpy:7879
translate japanese route_masaru_7_d708f64c:

    genji "Eh… so that's why she was acting weird all morning."
    genji "ええと…そういうわけで、彼女は午前中に変な演技をしていたのです。"

# game/script.rpy:7881
translate japanese route_masaru_7_47f1b41f:

    toshu "Eh? Coach, I thought you remembered it?"
    toshu "え？コーチ、私はあなたがそれを思いついたと思った？"

# game/script.rpy:7883
translate japanese route_masaru_7_0708ef7e:

    genji "She was acting weird at the faculty lounge today…"
    genji "彼女は今日教室で奇妙な演技をしていました…"

# game/script.rpy:7884
translate japanese route_masaru_7_623ba34f:

    genji "She suddenly got all clingy and kept asking me what the date it is today."
    genji "彼女は突然すべてがぎこちなくなり、今日の日時を尋ねてきた。"

# game/script.rpy:7885
translate japanese route_masaru_7_07d3ec95:

    genji "I was pretty confused with what was going on with her."
    genji "私は彼女と何が起こっているのかかなり混乱していた。"

# game/script.rpy:7887
translate japanese route_masaru_7_f4061f10:

    genji "So I thought she was just hungry, then I gave her the leftover chocolates I brought with me."
    genji "だから私は彼女がちょうど空腹であると思った、私は私に持ってきた残りのチョコレートを彼女に贈った。"

# game/script.rpy:7889
translate japanese route_masaru_7_3a3de118:

    ichiru "Wow… talk about heartless."
    ichiru "うわー…無情について話す。"

# game/script.rpy:7891
translate japanese route_masaru_7_373cac34:

    # genji "ANYWAY!"
    genji "そんなことはどうでもいい！"

# game/script.rpy:7893
translate japanese route_masaru_7_f4de4642:

    genji "Masaru! Toshu! Get into your gear, we are wasting daylight!"
    genji "Masaru！ Toshu！ お前らは調子に乗って、私たちは昼光を浪費しています！"

# game/script.rpy:7894
translate japanese route_masaru_7_c83aa04d:

    genji "I've already wasted enough time from finding you, Toshu!"
    genji "私はすでに、あなたを見つけることから十分な時間を無駄にしてしまった！"

# game/script.rpy:7896
translate japanese route_masaru_7_8994667e:

    # genji "I'm totally going to beat you guys to a pulp!"
    genji "ボロボロになるまで罰を与えてやるからな！"

# game/script.rpy:7913
translate japanese after_minigame_6_0ea0af0f:

    toshu_t "Despite Coach Genji's strict training program… today's practice felt a bit off…"
    toshu_t "コーチGenjiの厳しいトレーニングプログラムにもかかわらず…今日の練習はちょっと離れていました…"

# game/script.rpy:7914
translate japanese after_minigame_6_ee1c524d:

    toshu_t "Why do I feel like we are not good enough?"
    toshu_t "なぜ私たちは十分ではないように感じるのですか？"

# game/script.rpy:7915
translate japanese after_minigame_6_ddd3dd44:

    toshu_t "But I'm sure everyone is doing their best!"
    toshu_t "しかし、私は誰もが最善を尽くすと確信しています！"

# game/script.rpy:7916
translate japanese after_minigame_6_c276f407:

    toshu_t "That's what matters!"
    toshu_t "それは重要なことです！"

# game/script.rpy:7926
translate japanese after_minigame_6_5e91a7d2:

    toshu "Wahh… I'm so tired…"
    toshu "うーん…疲れている…"

# game/script.rpy:7927
translate japanese after_minigame_6_cef53903:

    masaru "Yeah… me too…"
    masaru "うん、私も…"

# game/script.rpy:7929
translate japanese after_minigame_6_2cafc70f:

    masaru "But… I feel we're not in perfect sync today…"
    masaru "しかし…私は今日、完全に同期していないと感じています…"

# game/script.rpy:7930
translate japanese after_minigame_6_834b9a8b:

    masaru "We need to practice more if we want to win the tournament."
    masaru "トーナメントに勝つためにはもっと練習が必要です。"

# game/script.rpy:7932
translate japanese after_minigame_6_7599def1:

    ichiru "We need to win that tournament or else Masaru is doomed!"
    ichiru "私たちはその大会に勝つ必要がある、そうでなければ勝が死んでいる！"

# game/script.rpy:7934
translate japanese after_minigame_6_c32b6499:

    ichiru "I know it's not like me to say this but…"
    ichiru "私はこれを言うのが私のようなものではないことを知っているが…"

# game/script.rpy:7935
translate japanese after_minigame_6_14f72ac4:

    ichiru "C'mon GENJI let's practice some more!"
    ichiru "さあ、GENJIはもう少し練習しましょう！"

# game/script.rpy:7937
translate japanese after_minigame_6_22e07ddb:

    genji "I appreciate you two's determination but we already worked extra hard today."
    genji "私はあなたに2つの決意をいただきありがとうございます。"

# game/script.rpy:7938
translate japanese after_minigame_6_43103dbd:

    genji "I don't want you guys to be overly fatigued."
    genji "私はあなたに過度に疲れさせたくありません。"

# game/script.rpy:7940
translate japanese after_minigame_6_4a561d79:

    # genji "Let's call it a day and continue our practice tomorrow!"
    genji "今日はこれくらいにして続きは明日にしよう！"

# game/script.rpy:7942
translate japanese after_minigame_6_b6fee877:

    genji "Masaru, your remedial exam is within this week, right? I hope Saki will teach you everything you need to know!"
    genji "マサル、あなたの修復試験は今週中ですよね？私はあなたが知る必要があるすべてを教えてくれることを祈っています！"

# game/script.rpy:7944
translate japanese after_minigame_6_8345f7fa:

    # genji "I wish you good luck!"
    genji "頑張れ！"

# game/script.rpy:7946
translate japanese after_minigame_6_71e1f573:

    # toshu "Good luck, Masaru!"
    toshu "頑張って下さい、Masaruさん！"

# game/script.rpy:7948
translate japanese after_minigame_6_df4591db:

    # ichiru "Don't let us down Masaru!"
    ichiru "ガッカリさせんなよ、Masaru！"

# game/script.rpy:7950
translate japanese after_minigame_6_16f0c78c:

    # masaru "Okay! Thank you, guys!"
    masaru "ああ！ みんなありがとう！"

# game/script.rpy:7952
translate japanese after_minigame_6_e866a125:

    toshu_t "Masaru had his exam on the last day of the week."
    toshu_t "Masaruは週の最後の日に彼の試験を受けました。"

# game/script.rpy:7953
translate japanese after_minigame_6_0d51bcc7:

    toshu_t "The week has passed and we've been extra busy for training for the tournament."
    toshu_t "週が終わり、トーナメントのトレーニングに余裕がありました。"

# game/script.rpy:7954
translate japanese after_minigame_6_3d4b40cf:

    toshu_t "The first days of training were really intense, but we've gotten used to it eventually!"
    toshu_t "トレーニングの最初の日は本当に激しかったですが、最終的にはそれに慣れました！"

# game/script.rpy:7955
translate japanese after_minigame_6_4ae88387:

    toshu_t "Today I got myself well-rested! I'm ready!"
    toshu_t "今日私は自分自身をよく休んだ！準備できました！"
