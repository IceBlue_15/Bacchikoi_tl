# game/script.rpy:1302
translate japanese choice_3_end_9b07b20d:

    # toshu_t "Hmm… I wonder where is Ichiru today…? He skipped class again…"
    toshu_t "うーん…Ichiruはどこに行ったんだろう…？ また授業をサボってるよ…"

# game/script.rpy:1304
translate japanese choice_3_end_5655d746:

    # toshu_t "His grades are gonna drop if he keeps on doing this."
    toshu_t "こんなことしてたら成績が落ちちゃうよ。"

# game/script.rpy:1305
translate japanese choice_3_end_04b03ead:

    toshu_t "And I thought he's going to keep his word when he said he will attend classes if I'm there as well."
    toshu_t "そして、もし私がそこにいるならば、彼が授業に出席すると言ったときに彼は彼の言葉を守るつもりだと思った。"

# game/script.rpy:1307
translate japanese choice_3_end_41419365:

    # toshu_t "I better stop thinking about it for now and focus with Ms. Saki's lecture."
    toshu_t "こんなこと考えるのは止めて、Saki先生の授業に集中しよう。"

# game/script.rpy:1311
translate japanese choice_3_end_df86e39d:

    # random "WHERE IS MY ICHIRU?!!"
    random "私のIchiruはどこかしら？"

# game/script.rpy:1313
translate japanese choice_3_end_05338153:

    # toshu "…?!"
    toshu "…？！"

# game/script.rpy:1315
translate japanese choice_3_end_d8b019d3:

    # saki "I'm sorry? Who are you? You can't just barge into my class screaming like that."
    saki "失礼ですがどなたかしら？ 授業に割り込んでそんな風に叫んじゃダメですよ。"

# game/script.rpy:1317
translate japanese choice_3_end_5f35fdc7:

    # random "Who am I?? WHO AM I?!!"
    random "どなた？？ どなたかしらですって？！！"

# game/script.rpy:1318
translate japanese choice_3_end_04dc5d8e:

    # tomoka "You peasant!! Why I am none other than Tomoka Saji. How dare you not know my name?"
    tomoka "平民が！ 私はTomoka Saji以外の何者でもありませんわ？ 私の名前を知らないだなんて！"

# game/script.rpy:1320
translate japanese choice_3_end_83d755cb:

    tomoka "I came from the RICH Saji family who OWNS this city including this school."
    tomoka "私はこの学校を含めてこの街を知り合っているRICH Saji家から来ました。"

# game/script.rpy:1322
translate japanese choice_3_end_0e417e16:

    # saki "T-To… Tomoka!!? Tomoka Saji?"
    saki "T、To…Tomoka！！？ Tomoka Sajiですって？"

# game/script.rpy:1323
translate japanese choice_3_end_1154706d:

    # saki "I'm so sorry! I didn't recognize you Princess Tomoka!"
    saki "申し訳ありません！ あなた様がプリンセスTomokaとは存じ上げませんでした！"

# game/script.rpy:1325
translate japanese choice_3_end_f2d2949a:

    tomoka "Fix those glasses of yours so that you can know who you are talking with, you ignorant fool!"
    tomoka "彼らの眼鏡を修正して、あなたが誰と話しているのか知ることができます。あなたは無知な愚か者です！"

# game/script.rpy:1327
translate japanese choice_3_end_45a5c257:

    # tomoka "Anyway! Where is my Ichiru?"
    tomoka "とにかく！ 私のIchiruはどこ？"

# game/script.rpy:1328
translate japanese choice_3_end_10ccc221:

    # tomoka "I didn't see him in the SAJI-YANAI family meeting! He's making me worried sick."
    tomoka "SAJI家とYANAI家の会合で彼を見かけませんでしたの！ 心配でたまりませんわ。"

# game/script.rpy:1330
translate japanese choice_3_end_09a62a38:

    tomoka "Ahh~ Why is this terrible world always severs our red string of fate~ Ahh~ Ichiru~"
    tomoka "ああ〜なぜこのひどい世界は、私たちの運命の赤い文字列を常に断っているのですか〜Ahh〜Ichiru〜"

# game/script.rpy:1332
translate japanese choice_3_end_3fc55105:

    saki "I'm sorry Princess, but he didn't go to class again today. He has been skipping his classes lately…"
    saki "すみません、プリンセスですが、今日は再び授業に出ませんでした。彼は最近、彼のクラスをスキップしています…"

# game/script.rpy:1334
translate japanese choice_3_end_fee81b99:

    # saki "You can ask Mr. Kanada, he is Mr. Yanai's best friend."
    saki "Yanaiくんの親友のKanadaくんに聞いてみたらどうでしょう？"

# game/script.rpy:1336
translate japanese choice_3_end_510c3d73:

    # tomoka "BEST FRIEND?!!!"
    tomoka "親友ですって？！！！"

# game/script.rpy:1338
translate japanese choice_3_end_1e3bf663:

    # toshu "M-Ms. Saki!"
    toshu "Sa、Saki先生！"

# game/script.rpy:1339
translate japanese choice_3_end_cbc966d6:

    # saki "He is in the same club as Mr. Yanai."
    saki "彼はYanaiくんのチームメイトです。"

# game/script.rpy:1341
translate japanese choice_3_end_4d371532:

    # tomoka "Huhhh?? Why is my Ichiru hanging out with a lowlife like him?"
    tomoka "フフフ？ どうして私のIchiruは彼のような下層民と一緒にいるのかしら？"

# game/script.rpy:1343
translate japanese choice_3_end_8629e68c:

    # toshu "…l-lowlife?"
    toshu "…か、下層民？"

# game/script.rpy:1345
translate japanese choice_3_end_29ddce81:

    # tomoka "So?? Speak up, you commoner!"
    tomoka "で？？ 平民よ、早く話しなさいな！"

# game/script.rpy:1347
translate japanese choice_3_end_721394f4:

    toshu "W-wait I'm really confused… why are you bringing an umbrella…?"
    toshu "私は本当に混乱しています…なぜあなたは傘を持ってきていますか？"

# game/script.rpy:1349
translate japanese choice_3_end_ed88621a:

    tomoka "U-UMBRELLA?! For your information, this is DIAMOND-EMBEDDED-VICTORIAN-PARASOL!"
    tomoka "U-UMBRELLA ?!あなたの情報は、DIAMOND-EMBEDDED-VICTORIAN-PARASOLです！"

# game/script.rpy:1350
translate japanese choice_3_end_af66e3fd:

    tomoka "What an illiterate! You sully the name of this school!"
    tomoka "どのような文盲！あなたはこの学校の名前を傷つけます！"

# game/script.rpy:1352
translate japanese choice_3_end_e43f0830:

    toshu_t "That didn't answer my question…"
    toshu_t "それは私の質問に答えなかった…"

# game/script.rpy:1354
translate japanese choice_3_end_f9bdfda5:

    toshu "I'm sorry lady but I haven't seen him today as well…"
    toshu "申し訳ありませんが、私は今日も彼を見たことがありません…"

# game/script.rpy:1356
translate japanese choice_3_end_bd126b99:

    tomoka "THAT'S PRINCESS TOMOKA TO YOU. Scavenger!"
    tomoka "あなたに優しい土方です。スカベンジャー！"

# game/script.rpy:1358
translate japanese choice_3_end_52724a91:

    tomoka "Oh how dreadful! My beloved Ichiru has gone missing again!"
    tomoka "ああ、恐ろしい！私の最愛のIchiruがもう一度行方不明になった！"

# game/script.rpy:1359
translate japanese choice_3_end_cdcb026c:

    tomoka "Hnn~~ I just can't imagine what ill-dreaded fate has he befallen upon to!"
    tomoka "Hnn ~~私は恐ろしい運命に襲われたことを想像することはできません！"

# game/script.rpy:1361
translate japanese choice_3_end_0858f909:

    saki "Princess, he might just be roaming around the hallways…?"
    saki "プリンセス、彼はちょうど廊下の周りをローミングしているかもしれない…？"

# game/script.rpy:1363
translate japanese choice_3_end_956b0830:

    tomoka "Ughhh~~ You lowly creatures are wasting my precious time!"
    tomoka "ええと~~あなたは卑劣な生き物が私の貴重な時間を無駄にしています！"

# game/script.rpy:1365
translate japanese choice_3_end_59f72839:

    # tomoka "Wait for me my beloved prince!!!"
    tomoka "待ってらして、我が最愛の王子様！！！"

# game/script.rpy:1369
translate japanese choice_3_end_627d27e4:

    # saki "…"
    saki "…"

# game/script.rpy:1371
translate japanese choice_3_end_a0723414:

    # toshu "Who was that weird lady, Ms. Saki?"
    toshu "あの変な女の人は誰なんですか？ Saki先生。"

# game/script.rpy:1373
translate japanese choice_3_end_9850beab:

    # saki "Shh! Don't say anything about her!"
    saki "シーッ！ あの方について喋ってはいけません！"

# game/script.rpy:1375
translate japanese choice_3_end_3f385abb:

    saki "She's Princess Tomoka Saji… she is the only daughter from the Saji monarch, and one of the richest families in this country…"
    saki "彼女はプリンセス・トモカ・サジです。彼女はサジ君主の唯一の娘で、この国で最も豊かな家族のひとりです…"

# game/script.rpy:1377
translate japanese choice_3_end_c12de697:

    # toshu "Whoa…"
    toshu "はぁ…"

# game/script.rpy:1379
translate japanese choice_3_end_f671bb40:

    # toshu "But why is she looking for Ichiru?"
    toshu "でも、なんであの人はIchiruを探しているんですか？"

# game/script.rpy:1381
translate japanese choice_3_end_4cc59a1e:

    saki "It's a long story. There seems to be quite an affair between the two families."
    saki "長い話だけれども。 2人の家族の間にはかなりの関係があるようです。"

# game/script.rpy:1382
translate japanese choice_3_end_162e4217:

    toshu "Ichiru's family knows such royalty?"
    toshu "イチルの家族はそのようなロイヤリティを知っていますか？"

# game/script.rpy:1384
translate japanese choice_3_end_de0f03e4:

    # saki "Eh? Don't you know the Yanai family is--"
    saki "え？ あなたはYanai家を知らないの？"

# game/script.rpy:1386
translate japanese choice_3_end_9a593b44:

    saki "Ohh… I'm sorry Mr. Kanada, but I have to go to the principal to report about what happened."
    saki "ああ…金田さんには申し訳ありませんが、何が起こったのかを報告するためには元に行く必要があります。"

# game/script.rpy:1387
translate japanese choice_3_end_6574d01a:

    saki "That's it for today everyone. You may all go home."
    saki "それは今日の皆のためです。あなたはすべて家に帰るかもしれません。"

# game/script.rpy:1389
translate japanese choice_3_end_9ab0df7f:

    toshu_t "What a day… I should go to the field before it gets late."
    toshu_t "何日…遅くなる前に野原に行くべきです。"

# game/script.rpy:1390
translate japanese choice_3_end_1b65dd92:

    toshu_t "It was quite a shock though…"
    toshu_t "しかし、それはかなりショックでした…"

# game/script.rpy:1391
translate japanese choice_3_end_5253123c:

    toshu_t "Why would a princess be looking for Ichiru?"
    toshu_t "なぜプリンセスがイチールを探しているのですか？"

# game/script.rpy:1392
translate japanese choice_3_end_7e62c358:

    toshu_t "Even saying something like, 'My Ichiru'."
    toshu_t "「マイイチル」のようなものを言っても。"

# game/script.rpy:1393
translate japanese choice_3_end_8b8e18e5:

    toshu_t "From that alone, I can assume that she is Ichiru's girlfriend."
    toshu_t "それだけで、彼女はIchiruのガールフレンドだと推測できます。"

# game/script.rpy:1401
translate japanese choice_3_end_ae3e9f85:

    genji "TOSHU!! YOU ARE LATE AGAIN!"
    genji "TOSHU !!あなたはまた遅刻した！"

# game/script.rpy:1403
translate japanese choice_3_end_d6700c9c:

    # toshu "B-but Coach! I-it's still five more minutes before practice begins…"
    toshu "で、でもコーチ！ まだ練習開始５分前ですよ…"

# game/script.rpy:1405
translate japanese choice_3_end_cb3dce22:

    # genji "I don't care! For me you are still late!"
    genji "関係ない！ 私が遅いと言ったら遅いんだ！"

# game/script.rpy:1407
translate japanese choice_3_end_6bcb742d:

    # toshu "Wahh…"
    toshu "うう…"

# game/script.rpy:1409
translate japanese choice_3_end_c996c38a:

    # masaru "Coach…! Stop it!"
    masaru "コーチ…！ やめてください！"

# game/script.rpy:1411
translate japanese choice_3_end_5da5a410:

    # genji "You're late too 'Captain'!"
    genji "お前も遅刻だぞ、\'キャプテン\'！"

# game/script.rpy:1413
translate japanese choice_3_end_83d54b23:

    genji "And what's this? Are you defending our newbie again?"
    genji "それは何ですか？もう初心者を守っていますか？"

# game/script.rpy:1414
translate japanese choice_3_end_7b46bf4c:

    genji "I can smell something in the air~"
    genji "私は空気中の何かをにおいすることができます〜"

# game/script.rpy:1416
translate japanese choice_3_end_a840d2e2:

    # masaru "Wh-what?"
    masaru "な、何だと？"

# game/script.rpy:1418
translate japanese choice_3_end_e2f7e2fa:

    # genji "Ehem! Where is Ichiru??"
    genji "ええい！ Ichiruはどこだ？"

# game/script.rpy:1419
translate japanese choice_3_end_12bd85ff:

    genji "It's awfully quiet without him around."
    genji "彼がいなくてもひどく静かです。"

# game/script.rpy:1421
translate japanese choice_3_end_a6686a7e:

    # ichiru "I'm here I'm here!"
    ichiru "ここだよ、俺はここにいるよ！"

# game/script.rpy:1423
translate japanese choice_3_end_61595dfe:

    genji "Well! Aren't you early today?"
    genji "まあ！今日は早い？"

# game/script.rpy:1425
translate japanese choice_3_end_bc775ce5:

    genji "Good boy, Good boy!! Hahaha!!"
    genji "グッドボーイ、グッドボーイ!!ははは！！"

# game/script.rpy:1427
translate japanese choice_3_end_81a48a5c:

    masaru "Ehh? How come you treat Ichiru differently?"
    masaru "え？イチールをどうやって扱いますか？"

# game/script.rpy:1429
translate japanese choice_3_end_3c605cbb:

    genji "HAHAHA!!"
    genji "ハハハ!!"

# game/script.rpy:1431
translate japanese choice_3_end_9568926d:

    genji "Trust me I don't want to get bullied again by him…"
    genji "私を信じて、私は彼によってもういじめられたくない…"

# game/script.rpy:1434
translate japanese choice_3_end_54c0b3ee:

    masaru "I see…"
    masaru "そうですか…"

# game/script.rpy:1436
translate japanese choice_3_end_6de280a8:

    # toshu "Ichiru, where have you been?"
    toshu "Ichiru、今までどこにいたの？"

# game/script.rpy:1437
translate japanese choice_3_end_0baf31c9:

    # toshu "You skipped class again today…"
    toshu "今日も授業をサボって…"

# game/script.rpy:1439
translate japanese choice_3_end_951b4e71:

    # ichiru "SHHHHH!!!"
    ichiru "シーーーッ！！！"

# game/script.rpy:1441
translate japanese choice_3_end_b36a368b:

    # masaru "You skipped class again??! I told you to get rid of that habit of yours."
    masaru "またサボったのか？ その悪い癖直すように言ったろ。"

# game/script.rpy:1443
translate japanese choice_3_end_b1c3ab27:

    # ichiru "I know, I know! But this time, it's cause Tomoka was there!"
    ichiru "分かってるよ！ でも今日はTomokaがいたんだ！"

# game/script.rpy:1445
translate japanese choice_3_end_af70b49b:

    # masaru "Oh… no wonder…"
    masaru "ああ…じゃあしょうがないか…"

# game/script.rpy:1447
translate japanese choice_3_end_def3c217:

    # toshu "Tomoka…? Tomoka Saji??"
    toshu "Tomoka…？ Tomoka Sajiのこと？？"

# game/script.rpy:1449
translate japanese choice_3_end_19d126b0:

    # ichiru "You know her, Toshu?"
    ichiru "Toshu、アイツのこと知ってんの？"

# game/script.rpy:1451
translate japanese choice_3_end_8013f29e:

    toshu "No but, she barged into class today. She was definitely looking for you."
    toshu "ううん、でも彼女は今日クラスに入った。彼女は間違いなくあなたを探していた。"

# game/script.rpy:1453
translate japanese choice_3_end_92fb118c:

    toshu "She was very mean. She even scolded me and Ms. Saki."
    toshu "彼女は非常に意味がありました。彼女は私と咲さんを叱った。"

# game/script.rpy:1455
translate japanese choice_3_end_ae409271:

    ichiru "SEE?! I'm so glad I didn't go to class today. Who knows what trouble she will bring to me again?!"
    ichiru "だろ？！ 今日は授業に出なくてよかったぜ。彼女が再び私にもたらすトラブルを知っていますか？"

# game/script.rpy:1457
translate japanese choice_3_end_c1f51154:

    masaru "That is so like her… *sigh*"
    masaru "それは彼女のように… *一息*"

# game/script.rpy:1459
translate japanese choice_3_end_0f5fa807:

    # toshu "Who is she exactly??"
    toshu "あの女の人は何者なんですか？"

# game/script.rpy:1461
translate japanese choice_3_end_bc2d3e5f:

    # masaru "Tomoka is Ichiru's--"
    masaru "TomokaはIchiruの…"

# game/script.rpy:1466
translate japanese choice_3_end_9c8d54c8:

    ichiru "SHHH!!! You don't have to tell him 'THAT'!"
    ichiru "SHHH !!!あなたは彼にそれを伝える必要はありません！"

# game/script.rpy:1467
translate japanese choice_3_end_0f5d1c65:

    masaru "H-hey…!"
    masaru "H-ねえ…！"

# game/script.rpy:1469
translate japanese choice_3_end_4477a0bf:

    masaru "Well everyone knows about it! Why not tell Toshu?"
    masaru "まあ誰もそれについて知っている！ Toshuに言ってみませんか？"

# game/script.rpy:1471
translate japanese choice_3_end_55bc448b:

    ichiru "Want me to tell him your 'secret' too?!"
    ichiru "彼にあなたの「秘密」も教えて欲しいですか？"

# game/script.rpy:1473
translate japanese choice_3_end_71e26a1c:

    masaru "H-hey! That's not the topic here!"
    masaru "H-ねえ！それはここの話題ではない！"

# game/script.rpy:1475
translate japanese choice_3_end_8366c2d5:

    ichiru "Tofu is special! And he don't need to know it…"
    ichiru "Tofuは特別です！そして、彼はそれを知る必要はありません…"

# game/script.rpy:1476
translate japanese choice_3_end_648267b6:

    ichiru "I will tell him some other time."
    ichiru "私は彼に別の時を伝えます。"

# game/script.rpy:1477
translate japanese choice_3_end_c778ae34:

    toshu "…?"
    toshu "…？"

# game/script.rpy:1479
translate japanese choice_3_end_a070bf81:

    masaru "I'm sorry Toshu, Ichiru doesn't want to tell…"
    masaru "すっごく残念だよ、イチルは伝えたくない…"

# game/script.rpy:1481
translate japanese choice_3_end_241d81d5:

    toshu "Huh?? Why?"
    toshu "ハァッ??どうして？"

# game/script.rpy:1483
translate japanese choice_3_end_d1b55647:

    ichiru "Don't worry Tofu! It's not important at all!"
    ichiru "Tofuを心配しないで！それはまったく重要ではありません！"

# game/script.rpy:1485
translate japanese choice_3_end_678621d4:

    toshu "It's okay Ichiru. If it's a private matter I won't force you to tell me."
    toshu "それは大丈夫です。それが私的なものなら、私はあなたに私に言ってもらうつもりはない。"

# game/script.rpy:1487
translate japanese choice_3_end_e9031698:

    ichiru "Aww! You are so understanding, Tofuuu!"
    ichiru "ああ！あなたはとても理解しています、Tofu！"

# game/script.rpy:1489
translate japanese choice_3_end_c0f68438:

    genji "Enough chitchat, you guys should start practicing. We're wasting daylight!"
    genji "あなたは練習を始めるべきです。私たちは昼光を無駄にしています！"

# game/script.rpy:1491
translate japanese choice_3_end_fa0b63db:

    toshu_t "Captain Masaru and Ichiru were obviously hiding something from me…"
    toshu_t "キャプテンマサルとイチルは明らかに私から何かを隠していた…"

# game/script.rpy:1492
translate japanese choice_3_end_dcf48a48:

    toshu_t "Like any normal person, it made me curious."
    toshu_t "どんな普通の人と同じように、私は好奇心をそそられました。"

# game/script.rpy:1494
translate japanese choice_3_end_99931963:

    toshu_t "But for now, I should focus on getting better with baseball!"
    toshu_t "しかし、今のところ、私は野球でより良くなることに集中すべきです！"

# game/script.rpy:1517
translate japanese after_minigame_2_600d7cfe:

    masaru "Mannn… Coach was extra strict today…"
    masaru "マン監督はコーチの今日の厳しい監督だった…"

# game/script.rpy:1519
translate japanese after_minigame_2_ecb0ec93:

    ichiru "I'M SOOOOOOO HUNGRYYYY!!!"
    ichiru "私はSOOOOOOO HUNGRYYYYです！"

# game/script.rpy:1521
translate japanese after_minigame_2_b9f43e73:

    toshu "I guess it's because of the coming tournament?"
    toshu "来るべきトーナメントのためだと思いますか？"

# game/script.rpy:1523
translate japanese after_minigame_2_5826a2b6:

    masaru "Most probably."
    masaru "おそらく。"

# game/script.rpy:1525
translate japanese after_minigame_2_e2cff724:

    ichiru "But that perverted Coach had to leave us during training because of his 'other priorities'."
    ichiru "しかし、その変態したコーチは、彼の「他の優先事項」のためにトレーニング中に私たちを去らなければならなかった。"

# game/script.rpy:1526
translate japanese after_minigame_2_d16f8073:

    ichiru "Probably gonna stalk a lady. That perverted old geezer!"
    ichiru "おそらく、女性を奪うつもりです。その変態した古いオタク！"

# game/script.rpy:1527
translate japanese after_minigame_2_cbace00f:

    toshu "I don't think Coach is like that."
    toshu "私はコーチがそうではないと思う。"

# game/script.rpy:1529
translate japanese after_minigame_2_08d34b8b:

    ichiru "Ah guys! I know, I know~ !!"
    ichiru "ああ、みんな！私は知っている、私は知っている〜！"

# game/script.rpy:1531
translate japanese after_minigame_2_e4391bce:

    ichiru "To pay off our hard work today, how about I treat you guys out for dinner~??"
    ichiru "今日の私たちの努力を払うために、どうやって私はあなたたちを夕食のために食べるのですか？"

# game/script.rpy:1533
translate japanese after_minigame_2_cd5a1efe:

    masaru "I don't know… I'm pretty tired. I want to get some rest too…"
    masaru "私は知らない…私はかなり疲れている。私はあまりにも安らかにしたいです…"

# game/script.rpy:1534
translate japanese after_minigame_2_e972050d:

    masaru "My little brother is waiting for me at home."
    masaru "私の弟は自宅で私を待っています。"

# game/script.rpy:1536
translate japanese after_minigame_2_ffc7cb9c:

    toshu "Oh! You have a little brother, Captain?"
    toshu "ああ！あなたは弟がいる、キャプテン？"

# game/script.rpy:1537
translate japanese after_minigame_2_90634948:

    masaru "Yes, he's with my dad."
    masaru "はい、彼は私のお父さんと一緒です。"

# game/script.rpy:1539
translate japanese after_minigame_2_8304a1a9:

    masaru "He said he can take care of himself but I can't help to worry."
    masaru "彼は自分の世話をすることができると言いましたが、私は心配することはできません。"

# game/script.rpy:1541
translate japanese after_minigame_2_cbb21738:

    toshu "Eh? I thought he's with your dad? I'm sure he'll be fine!"
    toshu "え？彼はあなたのお父さんと一緒だと思った？彼は大丈夫だろうと確信しています！"

# game/script.rpy:1543
translate japanese after_minigame_2_50f114b1:

    masaru "I don't want to rely on him…"
    masaru "私は彼に頼りたくはありません…"

# game/script.rpy:1544
translate japanese after_minigame_2_7616d487:

    masaru "Can't we let it slide today?"
    masaru "今日は滑らせることができないのですか？"

# game/script.rpy:1546
translate japanese after_minigame_2_70ae2f11:

    ichiru "Aww… that sucks!"
    ichiru "ああ…それは吸う！"

# game/script.rpy:1548
translate japanese after_minigame_2_c80ea0e9:

    ichiru "I guess me and Tofu will go out together then~ only us two~ without you~"
    ichiru "私は私とTofuが一緒に出かけるだろうと思う〜あなただけの2〜"

# game/script.rpy:1550
translate japanese after_minigame_2_9829e3b8:

    toshu "Ehhh?"
    toshu "Ehhh？"

# game/script.rpy:1552
translate japanese after_minigame_2_1c00e90a:

    ichiru "What do you think Tofu?"
    ichiru "Tofuはどう思いますか？"

# game/script.rpy:1575
translate japanese choice_4_A_55449e75:

    ichiru "Ohh…"
    ichiru "ああ…"

# game/script.rpy:1577
translate japanese choice_4_A_50ab3cad:

    masaru "Why don't we call it a day? I'm sure we're all pretty tired. You can treat us some other time Ichiru."
    masaru "なぜ私たちはそれを1日と呼びませんか？私たちはかなり疲れていると確信しています。他の時に私たちを治療することができます。"

# game/script.rpy:1579
translate japanese choice_4_A_15f84709:

    ichiru "Hmph!!! Fine!"
    ichiru "Hmph !!!ファイン！"

# game/script.rpy:1581
translate japanese choice_4_A_83ef885b:

    ichiru "I'll eat alone then!"
    ichiru "私は一人で食べるよ！"

# game/script.rpy:1583
translate japanese choice_4_A_1dfaef18:

    masaru "He'll be fine Toshu. He just need a good meal."
    masaru "彼は素晴らしいToshuになるでしょう。彼は良い食事が必要です。"

# game/script.rpy:1585
translate japanese choice_4_A_4800614d:

    toshu "If you say so, Captain."
    toshu "もしそうなら、キャプテン。"

# game/script.rpy:1587
translate japanese choice_4_A_a4eedd97:

    masaru "Let's go home, okay?"
    masaru "家に帰ろう、いい？"

# game/script.rpy:1589
translate japanese choice_4_A_9c096386:

    toshu_t "I kinda regret declining to Ichiru's offer."
    toshu_t "Ichiruの申し出に落ち込んでしまいました。"

# game/script.rpy:1590
translate japanese choice_4_A_9b83e1da:

    toshu_t "But as much as I want to join him, my body really says no."
    toshu_t "しかし、私が彼に加わりたいと思うほど、私の体は本当にいいえと言います。"

# game/script.rpy:1591
translate japanese choice_4_A_8ee63873:

    toshu_t "I will probably just apologize about it to him."
    toshu_t "私はたぶん彼にそれについて謝罪します。"

# game/script.rpy:1598
translate japanese choice_4_B_672b9c96:

    ichiru "Yoshaaa~!"
    ichiru "春~~！"

# game/script.rpy:1600
translate japanese choice_4_B_1d7c4197:

    ichiru "Me and Tofu are gonna have a date!!"
    ichiru "私とTofuにはデートがあります!!"

# game/script.rpy:1602
translate japanese choice_4_B_c28e8673:

    toshu "D-Date…?!"
    toshu "D-Date … ?!"

# game/script.rpy:1604
translate japanese choice_4_B_dcc57ee9:

    masaru "Uhh… maybe it's a bad idea that I leave you two alone."
    masaru "うーん…私はあなたを2人だけ残しておくのは悪い考えです。"

# game/script.rpy:1606
translate japanese choice_4_B_4be9d772:

    masaru "I'm coming along."
    masaru "私は一緒に来ている。"

# game/script.rpy:1608
translate japanese choice_4_B_8c6ccbf1:

    ichiru "Psshh! I thought you're tired and worried 'bout your lil' bro?!"
    ichiru "Psshh！私はあなたが疲れていると思っていて、あなたのlilの仲間を心配していますか？"

# game/script.rpy:1610
translate japanese choice_4_B_8e235ad3:

    masaru "W-well…"
    masaru "うーん…"

# game/script.rpy:1612
translate japanese choice_4_B_932d32df:

    masaru "I'm just afraid to leave you two walking alone in the streets. It will be dangerous out at night."
    masaru "私はちょうど通りに一人で歩いてあなたを残すことを恐れている。夜は危険です。"

# game/script.rpy:1614
translate japanese choice_4_B_109cbe6c:

    ichiru "Sure, whatever Masaru~"
    ichiru "確かに、どんなことでも〜"

# game/script.rpy:1616
translate japanese choice_4_B_ba731365:

    ichiru "Anyway, I know this awesome restaurant! You guys should try it too!"
    ichiru "とにかく、私はこの素晴らしいレストランを知っている！皆さんも試してみてください！"

# game/script.rpy:1623
translate japanese choice_4_C_eba7e96d:

    masaru "You really want me to go with you guys that bad?"
    masaru "あなたは本当に私があなたと一緒に行くことを望んで悪い人？"

# game/script.rpy:1625
translate japanese choice_4_C_c6498073:

    masaru "Okay then, I guess it can't be helped, I'm gonna go as well."
    masaru "さて、それは助けられないと思う、私も行くつもりです。"

# game/script.rpy:1626
translate japanese choice_4_C_26ae8bea:

    toshu "Wait… what about your little brother?"
    toshu "待って…あなたの弟はどう？"

# game/script.rpy:1627
translate japanese choice_4_C_cef07f98:

    masaru "I'll just contact him through phone. Don't worry."
    masaru "私は電話で連絡します。心配しないでください。"

# game/script.rpy:1629
translate japanese choice_4_C_a3e9be73:

    ichiru "Psssh… Masaru is just jealous if he leaves us together for our date!"
    ichiru "Psssh … Masaruは彼が私達の日のために私達を一緒に残すならば、ただ嫉妬深いです！"

# game/script.rpy:1631
translate japanese choice_4_C_64b80808:

    toshu "D-date…?!"
    toshu "D-date … ?!"

# game/script.rpy:1633
translate japanese choice_4_C_932d32df:

    masaru "I'm just afraid to leave you two walking alone in the streets. It will be dangerous out at night."
    masaru "私はちょうど通りに一人で歩いてあなたを残すことを恐れている。夜は危険です。"

# game/script.rpy:1634
translate japanese choice_4_C_3088f8db:

    toshu "Thank you Captain!"
    toshu "ありがとう、キャプテン！"

# game/script.rpy:1635
translate japanese choice_4_C_ba731365:

    ichiru "Anyway, I know this awesome restaurant! You guys should try it too!"
    ichiru "とにかく、私はこの素晴らしいレストランを知っている！皆さんも試してみてください！"

# game/script.rpy:1644
translate japanese choice_4_end_7358011b:

    ichiru "Fuwahh~ I just love the smell here! Feels like heaven!!"
    ichiru "Fuwahh〜私はここで匂いが大好き！天国のような感じ！！"

# game/script.rpy:1646
translate japanese choice_4_end_670b5052:

    masaru "It definitely smells like fried food."
    masaru "揚げ物のような香りです。"

# game/script.rpy:1648
translate japanese choice_4_end_4dc6aff3:

    toshu "I've never been to this restaurant before…"
    toshu "私は前にこのレストランに行ったことがない…"

# game/script.rpy:1650
translate japanese choice_4_end_6148367b:

    toshu "But I heard that food here is expensive…"
    toshu "しかし、ここの食べ物は高価だと聞きました…"

# game/script.rpy:1652
translate japanese choice_4_end_9e1ab907:

    ichiru "I always come here to eat when I skip cl--"
    ichiru "私はclをスキップするときにいつもここに来る。"

# game/script.rpy:1653
translate japanese choice_4_end_e595113a:

    ichiru "Errr… when I have free time!"
    ichiru "Errr …私は自由な時間があるとき！"

# game/script.rpy:1655
translate japanese choice_4_end_3f3bbb0a:

    ichiru "Order up guys!! Its my treat today~ You don't have to worry about a single cent!"
    ichiru "注文する人！その今日の私の治療〜あなたは一セントについて心配する必要はありません！"

# game/script.rpy:1656
translate japanese choice_4_end_bbe1ed93:

    narrator "*Ichiru points on the menu above*"
    narrator "*上記メニューのイチールポイント*"

# game/script.rpy:1659
translate japanese choice_4_end_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:1660
translate japanese choice_4_end_0b344fa5:

    toshu "…is this really a restaurant?"
    toshu "…これは本当にレストランですか？"

# game/script.rpy:1662
translate japanese choice_4_end_814ec7d0:

    ichiru "Ehh? What's wrong guys?"
    ichiru "え？何が間違っている？"

# game/script.rpy:1664
translate japanese choice_4_end_07b220f1:

    masaru "Ichiru…"
    masaru "イチール…"

# game/script.rpy:1666
translate japanese choice_4_end_09aaaa11:

    toshu "THESE ARE SOOO EXPENSIIVEE!!!"
    toshu "これらはSOOO EXPENSIIVEEです！"

# game/script.rpy:1667
translate japanese choice_4_end_e6ba9c1e:

    ichiru "Ehh???"
    ichiru "ええ？"

# game/script.rpy:1668
translate japanese choice_4_end_30f8cbf8:

    toshu "Hnghh!! This is like ten times my daily allowance for a single meal."
    toshu "Hnghh !!これは1回の食事の1日当たりの10倍のようなものです。"

# game/script.rpy:1669
translate japanese choice_4_end_1d693c68:

    toshu "They're all junk food too…"
    toshu "彼らもすべてのジャンクフードです…"

# game/script.rpy:1671
translate japanese choice_4_end_c070ea6e:

    masaru "It makes me guilty to eat these by just looking at the prices."
    masaru "それは価格を見るだけでこれらを食べることを私に罪悪感を与えます。"

# game/script.rpy:1673
translate japanese choice_4_end_bd27749a:

    ichiru "OH C'MON GUYS! I told you it's my treat! C'mon order as much as you want!"
    ichiru "OH C\'MONみんな！私はそれが私の治療だと言った！あなたが望むだけ注文してください！"

# game/script.rpy:1675
translate japanese choice_4_end_1d52dc0b:

    toshu "B-but--"
    toshu "B-しかし -"

# game/script.rpy:1677
translate japanese choice_4_end_9d935aa2:

    ichiru "NO BUTS! Just try it! It'll be fun!"
    ichiru "いいえ！やってみなよ！楽しくなるよ！"

# game/script.rpy:1679
translate japanese choice_4_end_aef08805:

    masaru "I… I guess I'll have the cheapest one… how about the uh…"
    masaru "私は…私は一番安いものを持っていると思います…どうですか？"

# game/script.rpy:1681
translate japanese choice_4_end_875e93ba:

    masaru "Fried… Flappy Bird???"
    masaru "揚げ…フラッフィーバード？"

# game/script.rpy:1683
translate japanese choice_4_end_e31a161f:

    ichiru "C'mon Masaru! Are you kidding me? You just ordered a kiddie meal."
    ichiru "さようなら！私をからかってるの？ちょうど子供の食事を注文しました。"

# game/script.rpy:1685
translate japanese choice_4_end_5b2aea3c:

    toshu "Pfft--"
    toshu "Pfft--"

# game/script.rpy:1687
translate japanese choice_4_end_0453a286:

    masaru "Oh."
    masaru "ああ。"

# game/script.rpy:1689
translate japanese choice_4_end_bb8794e0:

    ichiru "Do you like anything in particular, Masaru?"
    ichiru "あなたは特に何か好きですか？"

# game/script.rpy:1691
translate japanese choice_4_end_b4bdfc9b:

    masaru "Well… I like sweets."
    masaru "まあ…私はお菓子が好きです。"

# game/script.rpy:1693
translate japanese choice_4_end_fb6db4f0:

    ichiru "Ohh!! You should try their Candy Crush Shake, Big Banana Split and their bestselling Creamy Creampie!"
    ichiru "ああ！キャンディクラッシュシェイク、ビッグバナナスプリット、ベストセラークリーミークリームパイを試してみてください！"

# game/script.rpy:1695
translate japanese choice_4_end_18c494d0:

    masaru "…o-kay?"
    masaru "… o-to？"

# game/script.rpy:1697
translate japanese choice_4_end_bb6214cc:

    ichiru "It's settled then! I'll order those three for you!"
    ichiru "それはそれから落ち着いた！私はそれらの3つをあなたのために注文します！"

# game/script.rpy:1699
translate japanese choice_4_end_d57dd225:

    masaru "Wait…!"
    masaru "待つ…！"

# game/script.rpy:1701
translate japanese choice_4_end_acd8c878:

    toshu "Neh neh, Ichiru, I seriously don't know what I'm going to order. They are all named so weird…"
    toshu "Neh neh、Ichiru、私は真剣に私が注文しようとしているか分からない。彼らはすべて奇妙な名前が付けられています…"

# game/script.rpy:1703
translate japanese choice_4_end_a54c7d20:

    toshu "Can you suggest me anything too?"
    toshu "あなたは私に何かを提案することはできますか？"

# game/script.rpy:1705
translate japanese choice_4_end_3cd3a2f0:

    ichiru "I really like any food with meat in it. If I were you, I'd order their famous Beefcake Bowl!"
    ichiru "私は本当にその中に肉のある食べ物が好きです。もし私があなただったら、私は彼らの有名なビーフケーキボウルを注文するでしょう！"

# game/script.rpy:1706
translate japanese choice_4_end_309e5268:

    ichiru "Do you like anything in particular?"
    ichiru "特に何か好きですか？"

# game/script.rpy:1730
translate japanese choice_5_A_520013e4:

    ichiru "You have the same taste as Masaru?"
    ichiru "あなたはまると同じ味がありますか？"

# game/script.rpy:1732
translate japanese choice_5_A_fe60a367:

    masaru "Is that so, Toshu? Then why don't we get the same order?"
    masaru "Toshu？それでは同じ注文をしないのはなぜですか？"

# game/script.rpy:1734
translate japanese choice_5_A_bc75c2c9:

    toshu "Yes, Please!"
    toshu "はい、お願いします！"

# game/script.rpy:1736
translate japanese choice_5_A_bc000340:

    ichiru "You sure you guys don't want anything meaty??"
    ichiru "あなたは確かに肉が欲しいとは思わない？"

# game/script.rpy:1738
translate japanese choice_5_A_bcbd7ac2:

    masaru "I'm not a huge fan of savory food."
    masaru "私は風味豊かな料理の巨大なファンではない。"

# game/script.rpy:1740
translate japanese choice_5_A_b8f5649b:

    toshu "Ah well, I like meat but right now I'm not craving for it."
    toshu "ああ、私は肉が好きですが、今私はそれを渇望していません。"

# game/script.rpy:1742
translate japanese choice_5_A_6d8ad1a7:

    ichiru "Okay~ I'll call the waiter then!"
    ichiru "さて、私はウェイターに電話しましょう！"

# game/script.rpy:1744
translate japanese choice_5_A_2fb5cc99:

    masaru "I think we should order food over the counter…? This is a fastfood diner after all…?"
    masaru "私はカウンターで食べ物を注文すべきだと思います…？結局、これはファストフードの食堂です…？"

# game/script.rpy:1746
translate japanese choice_5_A_cb1097ea:

    ichiru "Oh don't sweat it! They already know me here! I'm their best customer after all!"
    ichiru "ああ、それを汗をかいてはいけない！彼らはすでに私をここに知っている！私は結局彼らの最高の顧客です！"

# game/script.rpy:1748
translate japanese choice_5_A_3f6d55c0:

    ichiru "Waiter!!~"
    ichiru "ウェイター!!〜"

# game/script.rpy:1750
translate japanese choice_5_A_8bd22de4:

    ichiru "Two orders each of the Candy Crush Shake, Big Banana Split and Creamy Creampie!"
    ichiru "キャンディクラッシュシェイク、ビッグバナナスプリット、クリーミークリームパイの2つのオーダー！"

# game/script.rpy:1752
translate japanese choice_5_A_6b2e5a44:

    ichiru "As for me, I'll have 2 Beefcake Bowls, 3 pieces of Angry-Angry Birdie, and the Super-Special-Awesome-Deluxe All-Meat Combo!"
    ichiru "私にとっては、Beefcakeボウル2個、Angry-angry Birdie 3個、Super-Special-Awesome-Deluxe All-Meat Comboを用意します！"

# game/script.rpy:1754
translate japanese choice_5_A_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:1756
translate japanese choice_5_A_070c501e:

    masaru "…haha."
    masaru "…笑。"

# game/script.rpy:1758
translate japanese choice_5_A_4e93b517:

    ichiru "Oh, I almost forgot, additional 2 orders of Tentacle Prawn and 3 pitchers of Watermelon Juice please!~"
    ichiru "ああ、私はほとんど触れていない、触手のエビの追加2注文とスイカジュースの3投げてください！〜"

# game/script.rpy:1764
translate japanese choice_5_B_92f2e5df:

    ichiru "OH COOL! We have the same taste!"
    ichiru "かっこいい、イケてる！私たちは同じ味があります！"

# game/script.rpy:1765
translate japanese choice_5_B_561614ff:

    ichiru "Okay, you should try the Beefcake Bowl! It's sooo DELICIOUS!!"
    ichiru "さて、ビーフケーキボウルを試してみてください！それはすっごくおいしい!!"

# game/script.rpy:1767
translate japanese choice_5_B_d1286464:

    toshu "O-okay…!"
    toshu "O-okay…!"

# game/script.rpy:1769
translate japanese choice_5_B_f9d09cf4:

    masaru "It pleases me to see Ichiru get excited over food."
    masaru "イチルが食べ物に興奮するのを見て喜ぶ。"

# game/script.rpy:1771
translate japanese choice_5_B_e2c0b006:

    ichiru "Heyy! What does that mean?!"
    ichiru "やあ！どういう意味ですか？！"

# game/script.rpy:1773
translate japanese choice_5_B_a058b41e:

    masaru "N-Nothing."
    masaru "N  - 何もない。"

# game/script.rpy:1775
translate japanese choice_5_B_6d8ad1a7:

    ichiru "Okay~ I'll call the waiter then!"
    ichiru "さて、私はウェイターに電話しましょう！"

# game/script.rpy:1777
translate japanese choice_5_B_2fb5cc99:

    masaru "I think we should order food over the counter…? This is a fastfood diner after all…?"
    masaru "私はカウンターで食べ物を注文すべきだと思います…？結局、これはファストフードの食堂です…？"

# game/script.rpy:1779
translate japanese choice_5_B_cb1097ea:

    ichiru "Oh don't sweat it! They already know me here! I'm their best customer after all!"
    ichiru "ああ、それを汗をかいてはいけない！彼らはすでに私をここに知っている！私は結局彼らの最高の顧客です！"

# game/script.rpy:1781
translate japanese choice_5_B_76530937:

    ichiru "I will order now~! Waiter!!!"
    ichiru "私は今〜を注文します！ウェイター！！！"

# game/script.rpy:1783
translate japanese choice_5_B_7391be27:

    ichiru "Give me one of each Candy Crush Shake, Big Banana Split and Creamy Creampie!"
    ichiru "キャンディクラッシュシェイク、ビッグバナナスプリット、クリーミークリームパイの1つを教えてください！"

# game/script.rpy:1784
translate japanese choice_5_B_a62f12fb:

    masaru "That's a lot…"
    masaru "それは多いです…"

# game/script.rpy:1785
translate japanese choice_5_B_2f457299:

    ichiru "And 1 Regular Beefcake Bowl for Tofu~"
    ichiru "そしてTofuのための1本のビーフケーキボウル〜"

# game/script.rpy:1787
translate japanese choice_5_B_e2a6b233:

    ichiru "As for me, I'll have 2 Large Beefcake bowl, 3 pieces of Angry-Angry Birdie, and the Super-Special-Awesome-Deluxe All-Meat Combo!"
    ichiru "私にとって、私は大型ビーフケーキボウル2個、怒っている怒っているバーディー3個、スーパースペシャル・アイドル・デラックスオールミートコンボを用意します！"

# game/script.rpy:1789
translate japanese choice_5_B_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:1791
translate japanese choice_5_B_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:1793
translate japanese choice_5_B_4e93b517:

    ichiru "Oh, I almost forgot, additional 2 orders of Tentacle Prawn and 3 pitchers of Watermelon Juice please!~"
    ichiru "ああ、私はほとんど触れていない、触手のエビの追加2注文とスイカジュースの3投げてください！〜"

# game/script.rpy:1798
translate japanese choice_5_C_0cfe496d:

    ichiru "You've made the right decision!"
    ichiru "あなたは正しい決断を下しました！"

# game/script.rpy:1800
translate japanese choice_5_C_eda8073b:

    masaru "Prepare yourself Toshu…"
    masaru "あなた自身を準備する…"

# game/script.rpy:1802
translate japanese choice_5_C_50eefb58:

    toshu "Ehh?"
    toshu "え？"

# game/script.rpy:1804
translate japanese choice_5_C_8248f8cb:

    ichiru "Waiter!!!"
    ichiru "ウェイター！！！"

# game/script.rpy:1806
translate japanese choice_5_C_7e85460d:

    masaru "Whoa, they have waiters in this place? I thought this was a fastfood diner?"
    masaru "おい、彼らはこの場所にウェイターを持っていますか？私はこれがファストフードの食堂だと思った？"

# game/script.rpy:1808
translate japanese choice_5_C_cb1097ea:

    ichiru "Oh don't sweat it! They already know me here! I'm their best customer after all!"
    ichiru "ああ、それを汗をかいてはいけない！彼らはすでに私をここに知っている！私は結局彼らの最高の顧客です！"

# game/script.rpy:1810
translate japanese choice_5_C_812f735f:

    ichiru "Give me 1 Candy Crush Shake, Big Banana Split and Creamy Creampie for Masaru!"
    ichiru "マッサルのためのキャンディクラッシュシェイク、ビッグバナナスプリット、クリーミークリームパイを1つ与えてください！"

# game/script.rpy:1812
translate japanese choice_5_C_0f5d1c65:

    masaru "H-hey…!"
    masaru "H-ねえ…！"

# game/script.rpy:1814
translate japanese choice_5_C_fc4ab7e1:

    ichiru "And for Tofu~, 1 Regular BeefCake Bowl, 2 Saucy meatballs, 1 Juicy Tofu! Put some extra rice on it as well!"
    ichiru "Tofu〜、1本の普通牛ケーキボウル、2種類のミートボール、1個のジューシーなTofu！それに余分な米を入れて！"

# game/script.rpy:1816
translate japanese choice_5_C_47387f61:

    toshu "Ichiru!! I can't finish that!"
    toshu "Ichiru!!私はそれを完了することはできません！"

# game/script.rpy:1817
translate japanese choice_5_C_e2a6b233:

    ichiru "As for me, I'll have 2 Large Beefcake bowl, 3 pieces of Angry-Angry Birdie, and the Super-Special-Awesome-Deluxe All-Meat Combo!"
    ichiru "私にとって、私は大型ビーフケーキボウル2個、怒っている怒っているバーディー3個、スーパースペシャル・アイドル・デラックスオールミートコンボを用意します！"

# game/script.rpy:1819
translate japanese choice_5_C_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:1821
translate japanese choice_5_C_0aece5cb:

    masaru "…I told you."
    masaru "…先ほども言いました。"

# game/script.rpy:1823
translate japanese choice_5_C_6b20d994:

    toshu "I'm gonna die…"
    toshu "私は死ぬんだ…"

# game/script.rpy:1828
translate japanese choice_5_D_359f2781:

    masaru "Ehh? Okay…?"
    masaru "え？はい…？"

# game/script.rpy:1830
translate japanese choice_5_D_ea4a2279:

    masaru "How about you order some sweets too?"
    masaru "お菓子を注文するのはいかがですか？"

# game/script.rpy:1832
translate japanese choice_5_D_4ccb7f5a:

    toshu "Anything you think is safe to eat, Captain."
    toshu "あなたが思うものは食べても大丈夫です、キャプテン。"

# game/script.rpy:1834
translate japanese choice_5_D_16ce2cdf:

    ichiru "Don't worry!! They're all good! Trust me!"
    ichiru "心配しないでください！！彼らはすべて良いです！私を信じて！"

# game/script.rpy:1836
translate japanese choice_5_D_e498de7d:

    masaru "Let's just get the same order shall we?"
    masaru "私たちは同じ順序を取っていきましょうか？"

# game/script.rpy:1838
translate japanese choice_5_D_d451ff46:

    ichiru "It's settled then! You get the same choice as Masaru!"
    ichiru "それはそれから落ち着いた！あなたはまると同じ選択をする！"

# game/script.rpy:1839
translate japanese choice_5_D_8248f8cb:

    ichiru "Waiter!!!"
    ichiru "ウェイター！！！"

# game/script.rpy:1841
translate japanese choice_5_D_7e85460d:

    masaru "Whoa, they have waiters in this place? I thought this was a fastfood diner?"
    masaru "おい、彼らはこの場所にウェイターを持っていますか？私はこれがファストフードの食堂だと思った？"

# game/script.rpy:1843
translate japanese choice_5_D_cb1097ea:

    ichiru "Oh don't sweat it! They already know me here! I'm their best customer after all!"
    ichiru "ああ、それを汗をかいてはいけない！彼らはすでに私をここに知っている！私は結局彼らの最高の顧客です！"

# game/script.rpy:1845
translate japanese choice_5_D_fa158a2c:

    ichiru "I'll have 2 of each of the Candy Crush Shake, Big Banana Split and Creamy Creampie!"
    ichiru "キャンディクラッシュシェイク、ビッグバナナスプリット、クリーミークリームパイの2種類があります！"

# game/script.rpy:1847
translate japanese choice_5_D_8bd22de4:

    ichiru "Two orders each of the Candy Crush Shake, Big Banana Split and Creamy Creampie!"
    ichiru "キャンディクラッシュシェイク、ビッグバナナスプリット、クリーミークリームパイの2つのオーダー！"

# game/script.rpy:1849
translate japanese choice_5_D_6b2e5a44:

    ichiru "As for me, I'll have 2 Beefcake Bowls, 3 pieces of Angry-Angry Birdie, and the Super-Special-Awesome-Deluxe All-Meat Combo!"
    ichiru "私にとっては、Beefcakeボウル2個、Angry-angry Birdie 3個、Super-Special-Awesome-Deluxe All-Meat Comboを用意します！"

# game/script.rpy:1851
translate japanese choice_5_D_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:1853
translate japanese choice_5_D_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:1858
translate japanese choice_5_end_5a50ac21:

    toshu_t "It was really generous of Ichiru to treat us for dinner!"
    toshu_t "夕食のために私たちを扱うのは本当に寛大でした！"

# game/script.rpy:1859
translate japanese choice_5_end_34d9ab3b:

    toshu_t "But somehow I don't feel very comfortable…"
    toshu_t "しかし、どういうわけか私は非常に快適に感じません…"

# game/script.rpy:1860
translate japanese choice_5_end_5797e0f3:

    toshu_t "He spent one-month worth of my allowance in one night."
    toshu_t "彼は一晩中私の手当を1ヶ月分費やした。"

# game/script.rpy:1861
translate japanese choice_5_end_505a4de8:

    toshu_t "Maybe he comes from a rich family???"
    toshu_t "たぶん彼は豊かな家族から来ていますか？"

# game/script.rpy:1862
translate japanese choice_5_end_6369c26e:

    toshu_t "Nah, what matters is I think I've gotten closer with Ichiru and Captain! Seeing Ichiru that excited over food is really interesting!"
    toshu_t "ナー、大事なのは、イチールとキャプテンに近づいたと思うよ！食べ物に興奮しているIchiruを見ているのは本当に面白い！"

# game/script.rpy:1872
translate japanese choice_5_end_82ba43c3:

    ichiru "Fwahhh!!~~~ I'm so full~!! Now I want to sleep…!"
    ichiru "Fwahhh !! ~~~私はとても満員です〜!!今私は眠りたい！"

# game/script.rpy:1873
translate japanese choice_5_end_12f99ad1:

    masaru "Don't sleep right away, you'll upset your tummy, plus it will get you fat."
    masaru "すぐに寝ないでください、あなたはあなたのおなかを怒らせます、そして、それはあなたに脂肪を与えるでしょう。"

# game/script.rpy:1875
translate japanese choice_5_end_c03d8f50:

    ichiru "Pff. I'm not gonna get fat."
    ichiru "Pff。私は脂肪になるつもりはない。"

# game/script.rpy:1877
translate japanese choice_5_end_ad959a93:

    toshu "Thank you for the treat Ichiru!"
    toshu "イチール扱いありがとうございます！"

# game/script.rpy:1879
translate japanese choice_5_end_c7241b41:

    masaru "Yeah, thanks a lot! I feel bad that you spent a lot though…"
    masaru "ええ、ありがとう！私はあなたが多くを過ごしたことが悪いと感じる…"

# game/script.rpy:1881
translate japanese choice_5_end_a5532fdb:

    ichiru "Aww, shucks! That's no big deal! You guys are my friends!"
    ichiru "ああ、シャック！それは大したことではありません！あなたたちは私の友達です！"

# game/script.rpy:1884
translate japanese choice_5_end_faba592f:

    # masaru "Ah, wait a second guys."
    masaru "あ、ちょっとゴメンね。"

# game/script.rpy:1886
translate japanese choice_5_end_7bb45c61:

    # masaru "Hello?"
    masaru "もしもし？"

# game/script.rpy:1887
translate japanese choice_5_end_e9aa5f27:

    # random_c "Big Brother where are you? It's already late."
    random_c "お兄ちゃんどこにいるの？ もうこんな時間だよ。"

# game/script.rpy:1889
translate japanese choice_5_end_2bed8552:

    # masaru "Ahh… I'm sorry… I went out with my friends."
    masaru "ああ…ゴメン…友達と出かけてたんだ。"

# game/script.rpy:1890
translate japanese choice_5_end_9a7e5039:

    # random_c "Ahh, it's okay Big Bro, I was just worried where you are, that's all."
    random_c "ううん、大丈夫だよお兄ちゃん。どこにいるのか気になっただけ。"

# game/script.rpy:1891
translate japanese choice_5_end_f57f08e2:

    # random_c "Please come home quick, dad left me again."
    random_c "早く帰ってきて。お父さんまた出て行っちゃった。"

# game/script.rpy:1892
translate japanese choice_5_end_8f98c830:

    # random_c "I don't wanna be alone... I don't know how to cook rice."
    random_c "独りはイヤだよ…ボクご飯の作り方分かんない。"

# game/script.rpy:1894
translate japanese choice_5_end_aac5292b:

    # masaru "Ahh. I'm sorry, stay there and just wait for big-bro to come home okay?"
    masaru "ああ。ゴメンね、お兄ちゃんが帰って来るのをおとなしく待ってるんだよ。"

# game/script.rpy:1895
translate japanese choice_5_end_ad7755b5:

    # random_c "Okay Big Brother! I'll be a good kid!"
    random_c "うん、お兄ちゃん！ ボクいい子にしてるよ！"

# game/script.rpy:1896
translate japanese choice_5_end_a06de80a:

    # masaru "See ya."
    masaru "うん、わかった。"

# game/script.rpy:1899
translate japanese choice_5_end_be2d8e15:

    # ichiru "Was that Sohma?"
    ichiru "電話の相手ってSohma？"

# game/script.rpy:1901
translate japanese choice_5_end_694784fb:

    # toshu "Sohma??"
    toshu "Sohma？？"

# game/script.rpy:1904
translate japanese choice_5_end_1c2f2784:

    # masaru "Yeah. Sohma's my little brother."
    masaru "ああ。 Sohmaは俺の弟なんだ。"

# game/script.rpy:1905
translate japanese choice_5_end_bcb800bd:

    # masaru "I'm sorry guys but I have to go ahead."
    masaru "みんなゴメンね。俺、先に帰らなきゃ。"

# game/script.rpy:1906
translate japanese choice_5_end_e2eb912f:

    masaru "I can't leave him alone, I need to go home quick."
    masaru "私は彼を一人のままにすることはできません、私は早く家に帰る必要があります。"

# game/script.rpy:1908
translate japanese choice_5_end_e4ec6734:

    toshu "Alone? I thought your dad was at home?"
    toshu "一人？あなたのお父さんは家にいたと思った？"

# game/script.rpy:1910
translate japanese choice_5_end_1e492741:

    masaru "Ah… well…"
    masaru "まぁ…"

# game/script.rpy:1912
translate japanese choice_5_end_09dd6296:

    masaru "You could say that he's the type who's quite fond of drinking…"
    masaru "あなたは彼が飲むのがかなり好きなタイプだと言うことができます…"

# game/script.rpy:1913
translate japanese choice_5_end_c2dbabde:

    masaru "And he's usually not around either."
    masaru "そして、彼はたいていその周りにいません。"

# game/script.rpy:1915
translate japanese choice_5_end_e643f91d:

    toshu "Oh I see… that's too bad…"
    toshu "ああ、それは…あまりにも悪い"

# game/script.rpy:1917
translate japanese choice_5_end_164501f9:

    masaru "I'm sorry but I really have to go."
    masaru "申し訳ありませんが、実際に行く必要があります。"

# game/script.rpy:1919
translate japanese choice_5_end_41f217d9:

    ichiru "It's okay Masaru! I was planning on going home either!"
    ichiru "大丈夫だよ！私はいずれかの家に行くことを計画していた！"

# game/script.rpy:1920
translate japanese choice_5_end_27fa15da:

    ichiru "Let's call it a night?"
    ichiru "それを夜と呼ぼうか？"

# game/script.rpy:1922
translate japanese choice_5_end_9ca3293d:

    toshu "Sure thing!"
    toshu "確実なこと！"

# game/script.rpy:1923
translate japanese choice_5_end_af75dffe:

    ichiru "We'll see each other tomorrow anyway!"
    ichiru "とにかく明日お互いに会うよ！"

# game/script.rpy:1925
translate japanese choice_5_end_be5536f0:

    masaru "See you guys, and take care!"
    masaru "あなたたちに会い、気をつけてください！"

# game/script.rpy:1927
translate japanese choice_5_end_9e9feffd:

    toshu "Okay Captain! Please be careful on your way home!"
    toshu "さて、キャプテン！家に帰る途中で気をつけてください！"

# game/script.rpy:1929
translate japanese choice_5_end_03725bab:

    # toshu_t "Captain Masaru's father… is a drunkard?"
    toshu_t "Masaruキャプテンのお父さんって…飲んだくれなの？"

# game/script.rpy:1930
translate japanese choice_5_end_b8202807:

    # toshu_t "Suddenly, I'm getting worried about Captain."
    toshu_t "僕は急にキャプテンのことが心配になった。"

# game/script.rpy:1931
translate japanese choice_5_end_aa4555f6:

    # toshu_t "Maybe I shouldn't stick my nose to other's business."
    toshu_t "人の家の事情に首を突っ込むのはきっとよくないことだと思う。"

# game/script.rpy:1932
translate japanese choice_5_end_1703095b:

    # toshu_t "At least today was really fun!"
    toshu_t "ともかく今日は本当に楽しかった！"

# game/script.rpy:1933
translate japanese choice_5_end_b1ec3200:

    toshu_t "I get to know more about my friends! I look forward of growing closer to them each day!"
    toshu_t "私は友人についてもっと知ることになる！私は毎日彼らに近づくことを楽しみにしています！"
