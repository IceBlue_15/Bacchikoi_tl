# game/script.rpy:5895
translate japanese choice_12_end_8722dd01:

    toshu "Today is my birthday…"
    toshu "今日は私の誕生日です…"

# game/script.rpy:5897
translate japanese choice_12_end_260b3d25:

    toshu "Since there's no school recently, it's been pretty quiet around…"
    toshu "最近学校がないので、周りはかなり静かです…"

# game/script.rpy:5898
translate japanese choice_12_end_556da879:

    toshu "Well, I'm pretty used to being alone in this house anyway."
    toshu "まあ、とにかく私はこの家に一人でいることにかなり慣れています。"

# game/script.rpy:5900
translate japanese choice_12_end_e0c1a3ff:

    toshu "Auntie is gone again… I really thought she will stay for at least my birthday."
    toshu "Auntieはもう一度消えた…私は本当に彼女が少なくとも私の誕生日のためにとどまると思った。"

# game/script.rpy:5901
translate japanese choice_12_end_683efae7:

    toshu "She did leave me a cellphone as a birthday gift…"
    toshu "彼女は私に誕生日プレゼントとして携帯電話を残しました…"

# game/script.rpy:5903
translate japanese choice_12_end_3bde5a0a:

    toshu "Sigh…"
    toshu "一口…"

# game/script.rpy:5905
translate japanese choice_12_end_bbec6d20:

    toshu "I usually don't get bored and play all day with my trusted computer."
    toshu "私は通常、飽きずに信頼できるコンピュータで一日中遊ぶことはありません。"

# game/script.rpy:5906
translate japanese choice_12_end_4845a3a8:

    toshu "But ever since I've been friends with Ichiru and Captain Masaru."
    toshu "しかし、以来、私はIchiruと大将と友人だった。"

# game/script.rpy:5908
translate japanese choice_12_end_a0849f45:

    toshu "I always feel I need to see them at least once a day."
    toshu "私はいつも少なくとも一日に一度それを見る必要があると感じています。"

# game/script.rpy:5910
translate japanese choice_12_end_3f477bd1:

    toshu "I miss them so bad…"
    toshu "私はそれらがとても悪いことを逃す…"

# game/script.rpy:5912
translate japanese choice_12_end_24286587:

    toshu "Hey! I just had an idea!"
    toshu "ねえ！私はちょうどアイデアがあった！"

# game/script.rpy:5913
translate japanese choice_12_end_b67a9868:

    toshu "It's my birthday today… I could invite them over!"
    toshu "今日私の誕生日です…私はそれらを招待することができます！"

# game/script.rpy:5915
translate japanese choice_12_end_c69bed9f:

    toshu "I have the whole house today for myself anyways!"
    toshu "私は今日、自分のために家全体をとにかく持っています！"

# game/script.rpy:5917
translate japanese choice_12_end_3f17aaa9:

    toshu "Yeah, I guess I'll do that!"
    toshu "ええ、私はそれをやろうと思う！"

# game/script.rpy:5919
translate japanese choice_12_end_f95dd1a5:

    toshu "Should I give them a call?"
    toshu "私は彼らに電話をするべきですか？"

# game/script.rpy:5942
translate japanese choice_13_A_47c1fd69:

    toshu "On second thought… maybe they're busy."
    toshu "二番目の考えで…多分彼らは忙しいです。"

# game/script.rpy:5943
translate japanese choice_13_A_0a577981:

    toshu "I shouldn't bother them…"
    toshu "私はそれらを気にするべきではない…"

# game/script.rpy:5945
translate japanese choice_13_A_be8245cb:

    toshu "Especially Ichiru… he must be busy with his family…"
    toshu "特にイチル…彼は家族と一緒に忙しくしなければならない…"

# game/script.rpy:5947
translate japanese choice_13_A_099efa8f:

    toshu "Sigh… I guess I will just go out today and treat myself maybe."
    toshu "一言…私は今日出て、多分自分自身を扱うだろうと思う。"

# game/script.rpy:5948
translate japanese choice_13_A_519ac4c8:

    toshu "There's a nearby restaurant that I really like…"
    toshu "私が本当に好きなレストランが近くにあります…"

# game/script.rpy:5949
translate japanese choice_13_A_a0adb3d7:

    toshu "I will just probably go th--"
    toshu "私はおそらく行くだろう -"

# game/script.rpy:5952
translate japanese choice_13_A_cbb0058c:

    toshu "Eh?? I wonder who that might be?"
    toshu "え？私はそれが誰かもしれないのだろうか？"

# game/script.rpy:5953
translate japanese choice_13_A_7e2b1c24:

    toshu "I thought this phone is brand new… how could someone know my number?"
    toshu "私はこの携帯電話がまったく新しいと思った…誰かが私の番号を知ることができた？"

# game/script.rpy:5958
translate japanese choice_13_B_16347151:

    toshu "But… Masaru's probably spending time with his little brother…"
    toshu "しかし…マサルはおそらく弟と一緒に過ごしています…"

# game/script.rpy:5960
translate japanese choice_13_B_3660a773:

    toshu "Maybe it's a bad idea…"
    toshu "多分それは悪い考えです…"

# game/script.rpy:5962
translate japanese choice_13_B_099efa8f:

    toshu "Sigh… I guess I will just go out today and treat myself maybe."
    toshu "一言…私は今日出て、多分自分自身を扱うだろうと思う。"

# game/script.rpy:5963
translate japanese choice_13_B_519ac4c8:

    toshu "There's a nearby restaurant that I really like…"
    toshu "私が本当に好きなレストランが近くにあります…"

# game/script.rpy:5964
translate japanese choice_13_B_a0adb3d7:

    toshu "I will just probably go th--"
    toshu "私はおそらく行くだろう -"

# game/script.rpy:5967
translate japanese choice_13_B_cbb0058c:

    toshu "Eh?? I wonder who that might be?"
    toshu "え？私はそれが誰かもしれないのだろうか？"

# game/script.rpy:5968
translate japanese choice_13_B_7e2b1c24:

    toshu "I thought this phone is brand new… how could someone know my number?"
    toshu "私はこの携帯電話がまったく新しいと思った…誰かが私の番号を知ることができた？"

# game/script.rpy:5974
translate japanese choice_13_C_18b7640d:

    toshu "Yeah I will call Ichiru! He'll come for sure!"
    toshu "うん、私はIchiruを呼び出します！彼は確かに来るよ！"

# game/script.rpy:5975
translate japanese choice_13_C_5d8a117d:

    toshu "He'll be surprised that I now have a phone!"
    toshu "彼は私が今電話を持っていることに驚くでしょう！"

# game/script.rpy:5977
translate japanese choice_13_C_d8c13dee:

    toshu "Now… what was Ichiru's number again…?"
    toshu "さて…Ichiruの番は何だったの？"

# game/script.rpy:5979
translate japanese choice_13_C_fb913b49:

    toshu "Oh, I remember I wrote it on my notebook the other day!"
    toshu "先日、ノートに書いたことを覚えています！"

# game/script.rpy:5980
translate japanese choice_13_C_8021c9c8:

    toshu "Let me just go ge--"
    toshu "私はちょうどゲットに行きましょう -"

# game/script.rpy:5983
translate japanese choice_13_C_cbb0058c:

    toshu "Eh?? I wonder who that might be?"
    toshu "え？私はそれが誰かもしれないのだろうか？"

# game/script.rpy:5984
translate japanese choice_13_C_7e2b1c24:

    toshu "I thought this phone is brand new… how could someone know my number?"
    toshu "私はこの携帯電話がまったく新しいと思った…誰かが私の番号を知ることができた？"

# game/script.rpy:5990
translate japanese choice_13_D_756732d8:

    toshu "I'm guessing Captain's free today! I'll give him a call then!"
    toshu "私は今日キャプテンの無料を推測している！私は彼に電話してあげるよ！"

# game/script.rpy:5991
translate japanese choice_13_D_5d8a117d:

    toshu "He'll be surprised that I now have a phone!"
    toshu "彼は私が今電話を持っていることに驚くでしょう！"

# game/script.rpy:5993
translate japanese choice_13_D_801fd499:

    toshu "Now… what was Captains's number again…?"
    toshu "今、キャプテンズの番号は何だったの？"

# game/script.rpy:5995
translate japanese choice_13_D_fb913b49:

    toshu "Oh, I remember I wrote it on my notebook the other day!"
    toshu "先日、ノートに書いたことを覚えています！"

# game/script.rpy:5996
translate japanese choice_13_D_8021c9c8:

    toshu "Let me just go ge--"
    toshu "私はちょうどゲットに行きましょう -"

# game/script.rpy:5999
translate japanese choice_13_D_cbb0058c:

    toshu "Eh?? I wonder who that might be?"
    toshu "え？私はそれが誰かもしれないのだろうか？"

# game/script.rpy:6000
translate japanese choice_13_D_7e2b1c24:

    toshu "I thought this phone is brand new… how could someone know my number?"
    toshu "私はこの携帯電話がまったく新しいと思った…誰かが私の番号を知ることができた？"

# game/script.rpy:6007
translate japanese route_masaru_6_a2c5bd2d:

    toshu "Hello?"
    toshu "こんにちは？"

# game/script.rpy:6008
translate japanese route_masaru_6_23ae1b0f:

    random_c "Hello, Toshu!"
    random_c "Hello, Toshu!"

# game/script.rpy:6010
translate japanese route_masaru_6_8aa888ba:

    toshu "C-Captain!"
    toshu "C-キャプテン！"

# game/script.rpy:6011
translate japanese route_masaru_6_dc2b061a:

    masaru_c "Yeah, It's me."
    masaru_c "うん、それは私です。"

# game/script.rpy:6013
translate japanese route_masaru_6_4ca750c4:

    toshu "It's been a while!"
    toshu "それはしばらくしている！"

# game/script.rpy:6015
translate japanese route_masaru_6_8952eb41:

    toshu "How are you doing?"
    toshu "お元気ですか？"

# game/script.rpy:6016
translate japanese route_masaru_6_e2898696:

    masaru_c "I'm doing great!"
    masaru_c "私は素晴らしいことをやっている！"

# game/script.rpy:6017
translate japanese route_masaru_6_6044a831:

    masaru_c "I missed you so much!"
    masaru_c "私はそんなにあなたを逃した！"

# game/script.rpy:6019
translate japanese route_masaru_6_b988d5ba:

    toshu "Me too! I've been missing you guys so bad!!"
    toshu "私も！私はあなたがとても悪いことを逃してきた！"

# game/script.rpy:6020
translate japanese route_masaru_6_bc888c38:

    toshu "It's such a coincidence that you called, Captain!"
    toshu "あなたが呼ばれたような偶然です、キャプテン！"

# game/script.rpy:6021
translate japanese route_masaru_6_29450dc6:

    masaru_c "Oh! About that, I called to say..."
    masaru_c "ああ！それについて、私は言いました…"

# game/script.rpy:6022
translate japanese route_masaru_6_0d0794ab:

    masaru_c "HAPPY BIRTHDAY!"
    masaru_c "お誕生日おめでとうございます！"

# game/script.rpy:6024
translate japanese route_masaru_6_ea3cc2e4:

    toshu "Awww! Thank you very much!!"
    toshu "Awww！どうもありがとうございました！！"

# game/script.rpy:6026
translate japanese route_masaru_6_56e28bda:

    toshu "Actually, I-I was really wondering if you can come over to my place today!"
    toshu "実際、私は今日あなたが私の場所に来ることができるかどうか本当に不思議でした！"

# game/script.rpy:6027
translate japanese route_masaru_6_6fddc502:

    toshu "I mean, I really don't have any plans and I have the house to myself anyway!"
    toshu "私は本当に計画がありませんし、とにかく自分自身に家を持っています！"

# game/script.rpy:6028
translate japanese route_masaru_6_ba13c2a7:

    masaru_c "Oh! I was thinking the same thing!"
    masaru_c "ああ！私は同じことを考えていた！"

# game/script.rpy:6030
translate japanese route_masaru_6_a370e8e4:

    toshu "R-really?!"
    toshu "本当に？"

# game/script.rpy:6031
translate japanese route_masaru_6_b69500a3:

    masaru_c "Of course I would gladly go there to your place."
    masaru_c "もちろん私はあなたのところに喜んでそこに行きます。"

# game/script.rpy:6032
translate japanese route_masaru_6_6c483ce0:

    masaru_c "I'll be there in an hour!"
    masaru_c "私は1時間でそこにいます！"

# game/script.rpy:6034
translate japanese route_masaru_6_644b9b23:

    toshu "Okay, Captain! I'll be waiting!"
    toshu "さて、キャプテン！待っています！"

# game/script.rpy:6037
translate japanese route_masaru_6_16483356:

    toshu "Wahh! Captain Masaru's coming over!!"
    toshu "ワー！ Captain Masaruがやって来る！"

# game/script.rpy:6038
translate japanese route_masaru_6_8a37cf3c:

    toshu "I should make something for him!"
    toshu "私は彼のために何かを作るべきです！"

# game/script.rpy:6040
translate japanese route_masaru_6_60c250f3:

    toshu "As I recall, Captain likes sweets!"
    toshu "私が思い出すように、キャプテンはお菓子が好きです！"

# game/script.rpy:6046
translate japanese route_masaru_6_bbc3957e:

    toshu "Wahh! I can just imagine how will he react if he taste my cooking."
    toshu "ワー！彼が私の料理を味わったら、どうやって反応するのだろうと想像することができます。"

# game/script.rpy:6047
translate japanese route_masaru_6_21397a6b:

    toshu "'Wow, Toshu, this taste so great!'"
    toshu "「うわー、Toshu、この味がすごい！」"

# game/script.rpy:6048
translate japanese route_masaru_6_30b07b95:

    toshu "'You would be an awesome husband someday'"
    toshu "「あなたはいつか素晴らしい夫になるだろう」"

# game/script.rpy:6049
translate japanese route_masaru_6_8c7fe92e:

    toshu "Wahh! What am I daydreaming about?"
    toshu "ワー！私は何について空想しているのですか？"

# game/script.rpy:6050
translate japanese route_masaru_6_04dcf7bc:

    toshu "I should focus on preparing before Captain gets here!"
    toshu "キャプテンがここに来る前に準備に集中すべきだ！"

# game/script.rpy:6051
translate japanese route_masaru_6_3cc01977:

    toshu "I have to clean my room first!"
    toshu "私はまず部屋をきれいにする必要があります！"

# game/script.rpy:6059
translate japanese route_masaru_6_fa334a08:

    toshu "Waaahh… I'm not sure if my room is tidy enough!"
    toshu "Waaahh …私の部屋がきれいだとは分かりません！"

# game/script.rpy:6061
translate japanese route_masaru_6_b0c43476:

    toshu "This spot needs some sweeping!"
    toshu "この場所はいくつかの掃除が必要です！"

# game/script.rpy:6063
translate japanese route_masaru_6_0c233feb:

    toshu "Ack! My furnitures are so dusty…!"
    toshu "ああ！私の家具はほこりが多い…！"

# game/script.rpy:6065
translate japanese route_masaru_6_914a83d9:

    toshu "Ahhhh!!! I have to change my bedsheet!"
    toshu "ああ！私は私のベッドシートを変更する必要があります！"

# game/script.rpy:6067
translate japanese route_masaru_6_dfb0708c:

    toshu "WHAT SHOULD I DO FIRST?!"
    toshu "私はまず何をすべきですか？"

# game/script.rpy:6069
translate japanese route_masaru_6_c87dd7dc:

    toshu_t "I got myself so busy tidying up my room…"
    toshu_t "私は自分の部屋を整理するのに忙しかった…"

# game/script.rpy:6070
translate japanese route_masaru_6_61d421de:

    toshu_t "I totally forgot about the thing I'm cooking…"
    toshu_t "私は料理していることを完全に忘れていました…"

# game/script.rpy:6077
translate japanese route_masaru_6_07d7ec95:

    toshu "W-what is that smell?"
    toshu "名前：W-何がその匂いですか？"

# game/script.rpy:6079
translate japanese route_masaru_6_19e05052:

    toshu "Wah!!!!! MY OVEN!!!"
    toshu "ワ！!!!!!私のオーブン!!!"

# game/script.rpy:6081
translate japanese route_masaru_6_17414822:

    toshu "Hngh! This is no good! Masaru won't be able to eat this!"
    toshu "Hngh！これは良いことではありません！マサルはこれを食べることができません！"

# game/script.rpy:6083
translate japanese route_masaru_6_0cb3c84e:

    toshu "Hold on…"
    toshu "つかまっている…"

# game/script.rpy:6085
translate japanese route_masaru_6_0ace4289:

    toshu "Its already been 1 hour and a half…"
    toshu "その1時間半前に既に…"

# game/script.rpy:6086
translate japanese route_masaru_6_92043e90:

    toshu "Where's Capt--"
    toshu "キャプテンはどこですか？"

# game/script.rpy:6089
translate japanese route_masaru_6_8a817301:

    toshu "H-hello?"
    toshu "H-こんにちは？"

# game/script.rpy:6090
translate japanese route_masaru_6_25e33213:

    masaru_c "Uhm... Toshu... I need help..."
    masaru_c "Uhm … Toshu …助けが必要です…"

# game/script.rpy:6091
translate japanese route_masaru_6_de1300fc:

    masaru_c "I... think I'm lost."
    masaru_c "私は…私が失われていると思う。"

# game/script.rpy:6092
translate japanese route_masaru_6_c777af95:

    masaru_c "I ended up in a place called 'FCK'... I think it's the same branch as the one Ichiru always goes to."
    masaru_c "私は「FCK」という場所で終わった。私は、イチルがいつも行くものと同じ枝だと思う。"

# game/script.rpy:6094
translate japanese route_masaru_6_cfe53476:

    toshu "C-Captain! That's so far from here!"
    toshu "C-キャプテン！それはここから遠い！"

# game/script.rpy:6096
translate japanese route_masaru_6_057a863d:

    toshu "How did you manage to get there?"
    toshu "どのようにそこに着くことができましたか？"

# game/script.rpy:6097
translate japanese route_masaru_6_4c3dba75:

    masaru_c "I'm sorry! I... I think I went the opposite direction..."
    masaru_c "ごめんなさい！私は反対の方向に行ったと思う…"

# game/script.rpy:6099
translate japanese route_masaru_6_2b17be75:

    toshu "I'll fetch you there, Captain! Just stay there, okay?!"
    toshu "私はそこにあなたを連れて行こう、キャプテン！そこにとどまるよ、大丈夫？"

# game/script.rpy:6102
translate japanese route_masaru_6_df1ec6bf:

    toshu_t "Captain Masaru has really no sense of direction…"
    toshu_t "キャプテンマサルは本当に方向感がありません…"

# game/script.rpy:6103
translate japanese route_masaru_6_11a85e12:

    toshu_t "Between him and Ichiru, his house is actually the closest to mine."
    toshu_t "彼とIchiruの間で、彼の家は実際に私に最も近い。"

# game/script.rpy:6104
translate japanese route_masaru_6_7a2d97be:

    toshu_t "And yet he got lost again."
    toshu_t "そして、彼は再び失われました。"

# game/script.rpy:6111
translate japanese route_masaru_6_002d3e8c:

    toshu "Captain!"
    toshu "キャプテン！"

# game/script.rpy:6113
translate japanese route_masaru_6_71c1d31e:

    masaru "I'm sorry Toshu!"
    masaru "I'm sorry Toshu!"

# game/script.rpy:6115
translate japanese route_masaru_6_6a47b252:

    toshu "I-it's okay, Captain! I forgot that you're not good with directions…"
    toshu "私は大丈夫です、キャプテン！あなたが道案内がうまくいかないことを忘れていた…"

# game/script.rpy:6117
translate japanese route_masaru_6_b6bfc99e:

    masaru "*sigh* How embarrassing. I should've been the one going to you…"
    masaru "*一口*どのように恥ずかしい。私はあなたに行くべきだったはずだ…"

# game/script.rpy:6118
translate japanese route_masaru_6_dedc6252:

    masaru "It's your birthday and you went for the trouble to fetch me…"
    masaru "それはあなたの誕生日です。"

# game/script.rpy:6120
translate japanese route_masaru_6_7d9ec75b:

    toshu "I told you, it's fine Captain! Don't worry about that!"
    toshu "私はあなたに言った、それは大丈夫ですキャプテン！それを心配しないで！"

# game/script.rpy:6122
translate japanese route_masaru_6_cfd8f5a7:

    masaru "I-if you say so. I'm really sorry again."
    masaru "私はあなたがそう言えば。私は本当に再び申し訳なく思っています。"

# game/script.rpy:6124
translate japanese route_masaru_6_afe3a85a:

    toshu "W-wait… I just realized…"
    toshu "私はちょっと気付いた。"

# game/script.rpy:6125
translate japanese route_masaru_6_9ba44cef:

    toshu "How did you get my phone number too…? This phone is brand new."
    toshu "どうやって電話番号も手に入れたの？この携帯電話は新品です。"

# game/script.rpy:6126
translate japanese route_masaru_6_eafe2578:

    masaru "Did you like our gift to you?"
    masaru "あなたに私たちの贈り物が好きでしたか？"

# game/script.rpy:6128
translate japanese route_masaru_6_b20d20a2:

    toshu "Wh-what?"
    toshu "何？"

# game/script.rpy:6129
translate japanese route_masaru_6_503c93c5:

    masaru "That's right! We bought that phone for you! Me, Ichiru and Coach Genji!"
    masaru "そのとおり！私たちはあなたのためにその電話を買った！私、Ichiru、元祖コーチ！"

# game/script.rpy:6131
translate japanese route_masaru_6_7b4ae5de:

    toshu "Whaa…"
    toshu "わぁ…"

# game/script.rpy:6133
translate japanese route_masaru_6_87e981eb:

    ichiru "We sent it to your aunt before winter break. Surprised?"
    ichiru "私たちはあなたの叔母に冬休みの前にそれを送った。驚いた？"

# game/script.rpy:6135
translate japanese route_masaru_6_b78e1631:

    toshu "T-thank you…!"
    toshu "T-ありがとう！"

# game/script.rpy:6138
translate japanese route_masaru_6_b7413c99:

    masaru "There, there, Toshu! Don't cry! You're really special, don't forget that!"
    masaru "そこに、東照！泣かないで！あなたは本当に特別です、それを忘れないでください！"

# game/script.rpy:6140
translate japanese route_masaru_6_9e8398fa:

    masaru "Ah! Since we're here after all!"
    masaru "ああ！私たちは結局ここにいるから！"

# game/script.rpy:6141
translate japanese route_masaru_6_b6e4b771:

    masaru "How about I treat you for lunch?"
    masaru "私はあなたを昼食にどうですか？"

# game/script.rpy:6143
translate japanese route_masaru_6_e98c7912:

    toshu "N-no, Captain…!"
    toshu "いいえ、キャプテン…！"

# game/script.rpy:6144
translate japanese route_masaru_6_a3c8e226:

    toshu "It's expensive here…!!"
    toshu "ここは高価です… !!"

# game/script.rpy:6145
translate japanese route_masaru_6_2aca9558:

    masaru "I insist! It's your special day after all!"
    masaru "私は主張する！結局あなたの特別な日です！"

# game/script.rpy:6147
translate japanese route_masaru_6_551c0350:

    toshu "Actually, I was preparing something for us to eat at home…"
    toshu "実際、私は家で食べるものを準備していました…"

# game/script.rpy:6148
translate japanese route_masaru_6_94b05b1a:

    toshu "But it failed…"
    toshu "しかし、それは失敗した…"

# game/script.rpy:6150
translate japanese route_masaru_6_cde38b1f:

    masaru "Aww…"
    masaru "ああ…"

# game/script.rpy:6152
translate japanese route_masaru_6_b8ea4c26:

    masaru "But at least now you have no choice but to accept my treat!"
    masaru "しかし、少なくとも今あなたは私の治療を受け入れるしかありません！"

# game/script.rpy:6154
translate japanese route_masaru_6_52f61a98:

    toshu "O-okay Captain! I accept then!"
    toshu "大丈夫キャプテン！私はそれを受け入れる！"

# game/script.rpy:6156
translate japanese route_masaru_6_93a4d04f:

    masaru "Great!"
    masaru "すばらしいです！"

# game/script.rpy:6158
translate japanese route_masaru_6_a0ef4100:

    toshu "B-by the way, where's Ichiru?"
    toshu "名前：B-ところで、Ichiruはどこですか？"

# game/script.rpy:6159
translate japanese route_masaru_6_ec4cc76c:

    toshu "I wanted to invite him too."
    toshu "私も彼を招待したいと思っていました。"

# game/script.rpy:6161
translate japanese route_masaru_6_43223320:

    masaru "Ah… speaking of him. I did call him first to come with me to visit you for your birthday."
    masaru "ああ…彼に話す。あなたの誕生日のために私を訪ねて来るように彼に最初に電話しました。"

# game/script.rpy:6162
translate japanese route_masaru_6_f4a4debc:

    masaru "He said he couldn't come because of Tomoka."
    masaru "彼はトモカのために来ることができないと言った。"

# game/script.rpy:6163
translate japanese route_masaru_6_dbcf0813:

    masaru "Something about, putting him in a leash so that he won't escape?"
    masaru "何かについては、彼が脱出しないようにひもで入れて？"

# game/script.rpy:6165
translate japanese route_masaru_6_bd28b85b:

    masaru "Talk about overly attached girlfriends…"
    masaru "過度についたガールフレンドについて話す…"

# game/script.rpy:6166
translate japanese route_masaru_6_c6cfe860:

    masaru "Though I'm pretty sure, it's one of those family meetings."
    masaru "私はかなり確信していますが、それはその家族会議の1つです。"

# game/script.rpy:6168
translate japanese route_masaru_6_c7d15700:

    toshu "Aaww… poor Ichiru!"
    toshu "Aaww …悪いIchiru！"

# game/script.rpy:6170
translate japanese route_masaru_6_6c4fb6d5:

    masaru "You and Ichiru seem to get along really well."
    masaru "あなたとIchiruは本当にうまくやっているようです。"

# game/script.rpy:6172
translate japanese route_masaru_6_b4ead4bf:

    toshu "Yes, Ichiru is very friendly!"
    toshu "はい、Ichiruはとてもフレンドリーです！"

# game/script.rpy:6174
translate japanese route_masaru_6_37c8363d:

    masaru "Honestly, you are the first person I know who got close to Ichiru that much."
    masaru "正直なところ、一番近い人がいちごに近づいたのは、私が知っている最初の人です。"

# game/script.rpy:6176
translate japanese route_masaru_6_b24d4485:

    # toshu "R-really?"
    toshu "ホ、ホントですか？"

# game/script.rpy:6178
translate japanese route_masaru_6_a4a9460f:

    masaru "Yup! And he cares about you very much!"
    masaru "うん！そして彼はあなたのことをとても気にしています！"

# game/script.rpy:6180
translate japanese route_masaru_6_7eb55b60:

    toshu "Ahh… I see…!"
    toshu "ええ、わかりました…！"

# game/script.rpy:6182
translate japanese route_masaru_6_de30e7d5:

    # masaru "Ah! You must be hungry now!"
    masaru "あっ！ お腹空いてるだろ！"

# game/script.rpy:6184
translate japanese route_masaru_6_5b730c00:

    # masaru "Let's make an order shall we?"
    masaru "注文しようぜ！"

# game/script.rpy:6186
translate japanese route_masaru_6_93f91efb:

    # toshu_t "Captain Masaru is no doubt the kindest person I've ever met."
    toshu_t "Masaruキャプテンは今まで会った中で一番親切な人だ。"

# game/script.rpy:6187
translate japanese route_masaru_6_eae4128f:

    # toshu_t "He really took the time to go for my birthday, even though I know he has many things going on with his family."
    toshu_t "家庭の事情があるはずなのに、僕の誕生日のためにわざわざ時間を割いてくれた。"

# game/script.rpy:6188
translate japanese route_masaru_6_635c9f9c:

    # toshu_t "He even treats me for lunch. I feel guilty because I know he's not as wealthy as Ichiru."
    toshu_t "キャプテンは食事まで奢ってくれた。キャプテンがIchiruほど裕福でないことは知っているので、申し訳なさを感じた。"

# game/script.rpy:6189
translate japanese route_masaru_6_55a15a11:

    toshu_t "But what really makes me admire him is his thoughtfulness."
    toshu_t "しかし、本当に私が彼を賞賛するのは、彼の思慮深いことです。"

# game/script.rpy:6190
translate japanese route_masaru_6_72574c5e:

    toshu_t "He still has the time to think about what others feel."
    toshu_t "彼はまだ他人が感じていることを考える時間があります。"

# game/script.rpy:6191
translate japanese route_masaru_6_db58db98:

    # toshu_t "Captain Masaru is selfless."
    toshu_t "Masaruキャプテンは私心のない人だ。"

# game/script.rpy:6199
translate japanese route_masaru_6_425b2dcc:

    # toshu "Captain! Thanks for treating me out today!"
    toshu "キャプテン！ 今日はご馳走様でした！"

# game/script.rpy:6201
translate japanese route_masaru_6_f4a8fd13:

    # masaru "Haha. No problem."
    masaru "ハハハ。 どういたしまして。"

# game/script.rpy:6203
translate japanese route_masaru_6_1adca960:

    # masaru "Really nice place you got here."
    masaru "素敵な部屋だね。"

# game/script.rpy:6205
translate japanese route_masaru_6_baff923d:

    # toshu "T-thank you for really coming Captain!"
    toshu "キャ、キャプテン！ ホントに来てくれてありがとうございます！"

# game/script.rpy:6206
translate japanese route_masaru_6_68df63fc:

    # toshu "Please make yourself at home!"
    toshu "どうぞくつろいでください！"

# game/script.rpy:6207
translate japanese route_masaru_6_c27968b5:

    # masaru "By the way, may I ask something?"
    masaru "ところで、ちょっと聞いてもいいかな？"

# game/script.rpy:6209
translate japanese route_masaru_6_079a2429:

    # toshu "What is it, Captain?"
    toshu "何ですか？ キャプテン。"

# game/script.rpy:6211
translate japanese route_masaru_6_3edf207b:

    # masaru "I can't help but notice, is that a piano covered by a blanket?"
    masaru "ちょっと気になったんだけど、その毛布が掛かってるのはピアノかな？"

# game/script.rpy:6213
translate japanese route_masaru_6_31974de1:

    # toshu "Ah! Yes it is!"
    toshu "あ、はい。そうです！"

# game/script.rpy:6214
translate japanese route_masaru_6_55588ed1:

    # toshu "Actually, it belongs to my mom."
    toshu "ホントはお母さんのものなんですけどね。"

# game/script.rpy:6216
translate japanese route_masaru_6_782134f1:

    # toshu "She used to play me songs when I was about to sleep."
    toshu "僕が寝るときによくピアノを弾いてくれました。"

# game/script.rpy:6217
translate japanese route_masaru_6_a038fce3:

    # toshu "That's why it was placed beside my bed."
    toshu "だから僕のベッドの横に置いてあるんです。"

# game/script.rpy:6219
translate japanese route_masaru_6_d025a2ec:

    # masaru "Wow! How sweet!"
    masaru "うわあ！ 素敵な話だね！"

# game/script.rpy:6221
translate japanese route_masaru_6_c1b02aab:

    # toshu "Ah! I just remembered! You play the piano, right, Captain?"
    toshu "あっ、思い出しました！ キャプテンってピアノ弾けるんですよね？"

# game/script.rpy:6223
translate japanese route_masaru_6_cda0c964:

    # masaru "Yes I do!"
    masaru "ああ、そうだよ！"

# game/script.rpy:6225
translate japanese route_masaru_6_220f6c47:

    # toshu "I want to hear you play!"
    toshu "キャプテンのピアノ、聴いてみたいな！"

# game/script.rpy:6226
translate japanese route_masaru_6_d19dd3a0:

    # toshu "Here, let me remove the blanket."
    toshu "毛布を除けますね。"

# game/script.rpy:6229
translate japanese route_masaru_6_a328fba0:

    # masaru "Beautiful piano."
    masaru "綺麗なピアノだね。"

# game/script.rpy:6230
translate japanese route_masaru_6_e2dffc37:

    # masaru "Let me play for you."
    masaru "君のために弾くよ。"

# game/script.rpy:6237
translate japanese route_masaru_6_f0ce18e7:

    # toshu_t "I just stood there… speechless…"
    toshu_t "僕は呆然と立ち尽くしてしまった…言葉が出ない…"

# game/script.rpy:6239
translate japanese route_masaru_6_6d185e4d:

    toshu_t "I can't believe… of all piano pieces… Masaru plays me that one special piece."
    toshu_t "私は信じられない…すべてのピアノ作品… Masaruは私にその1つの特別な作品を演奏する。"

# game/script.rpy:6241
translate japanese route_masaru_6_548ae17c:

    toshu_t "I was immediately brought to tears…"
    toshu_t "私はすぐに涙を流しました…"

# game/script.rpy:6249
translate japanese route_masaru_6_b33a3644:

    # toshu "That… song…"
    toshu "その…曲…"

# game/script.rpy:6251
translate japanese route_masaru_6_b2ff6d36:

    # masaru "E-eh? What's wrong, Toshu?"
    masaru "え？ Toshu、どうかしたの？"

# game/script.rpy:6252
translate japanese route_masaru_6_97fe7583:

    # toshu "It is the same song that my mom plays when I sleep…"
    toshu "僕が眠るときにお母さんが弾いてくれた曲です…"

# game/script.rpy:6253
translate japanese route_masaru_6_dfebce21:

    # toshu "It makes me recall the time she was always with me…"
    toshu "その曲を聴くとお母さんがいつも一緒にいてくれた頃を思い出します…"

# game/script.rpy:6255
translate japanese route_masaru_6_219e168a:

    # masaru "W-what a coincidence…"
    masaru "何という偶然…"

# game/script.rpy:6256
translate japanese route_masaru_6_1c7badba:

    # masaru "This is the first song my mom taught me."
    masaru "これは俺の母さんが教えてくれた初めての曲なんだ。"

# game/script.rpy:6258
translate japanese route_masaru_6_a393547e:

    # masaru "It serves as her memento."
    masaru "母さんの形見みたいなもんだな。"

# game/script.rpy:6260
translate japanese route_masaru_6_3ed41c62:

    toshu_t "I didn't have any idea how painful it is to lose someone… I can feel the weight from Captain's words…"
    toshu_t "私は誰かを失うことがどれほど苦痛であるか考えていなかった…キャプテンの言葉から体重を感じることができる…"

# game/script.rpy:6262
translate japanese route_masaru_6_59084021:

    # toshu "C-Captain…"
    toshu "キャ、キャプテン…"

# game/script.rpy:6264
translate japanese route_masaru_6_db3d6ad7:

    masaru "I will never forget how much she cared for our family…"
    masaru "彼女がどれほど家族の世話をしたのか決して忘れないだろう…"

# game/script.rpy:6266
translate japanese route_masaru_6_20300077:

    masaru "She taught me how not to give up even when we are going through hardships."
    masaru "彼女は私が苦難を経験していても、あきらめない方法を教えてくれました。"

# game/script.rpy:6267
translate japanese route_masaru_6_6c406942:

    masaru "When she was terribly sick, she always smiled…"
    masaru "彼女がひどく病気になったとき、彼女はいつも微笑んだ…"

# game/script.rpy:6269
translate japanese route_masaru_6_36eee7ba:

    masaru "Even until her last moments… she smiled…"
    masaru "彼女の最後の瞬間まで…彼女は微笑んだ…"

# game/script.rpy:6271
translate japanese route_masaru_6_cd2b681b:

    toshu_t "I can now somewhat understand where Captain Masaru got his positivity…"
    toshu_t "私はMasaru船長がポジティブになったところを幾分理解できます…"

# game/script.rpy:6273
translate japanese route_masaru_6_920124a7:

    masaru "But after she passed away… the foundation of our family broke to pieces…"
    masaru "しかし、彼女が亡くなった後、家族の基盤が壊れてしまった…"

# game/script.rpy:6275
translate japanese route_masaru_6_637c4308:

    masaru "Our family really tried to afford her medication… until we went completely bankrupt…"
    masaru "私たちの家族は本当に彼女の薬を買うことを試みました…私たちが完全に破産するまで…"

# game/script.rpy:6276
translate japanese route_masaru_6_712432e1:

    masaru "We made so many debts…"
    masaru "私たちはたくさんの借金をしました…"

# game/script.rpy:6278
translate japanese route_masaru_6_0387ab22:

    masaru "But despite all our efforts, mom still passed away. It was really a terminal illness…"
    masaru "しかし、すべての努力にもかかわらず、ママはまだ亡くなりました。それは本当に終末期病でした…"

# game/script.rpy:6279
translate japanese route_masaru_6_614ba7e0:

    masaru "Dad could not accept her death…"
    masaru "お父さんは彼女の死を受け入れることができなかった…"

# game/script.rpy:6280
translate japanese route_masaru_6_44169f8e:

    masaru "He kept blaming himself for not having more money to pay off mom's medication…"
    masaru "彼は、お母さんの薬を払うためにもっとお金を持っていないという理由で自分を責め続けていました…"

# game/script.rpy:6282
translate japanese route_masaru_6_1d11bec9:

    masaru "So… he destroyed himself as well…"
    masaru "だから…彼は自分自身も破壊した…"

# game/script.rpy:6284
translate japanese route_masaru_6_dcfb9467:

    toshu "As much as I wanted to try to comfort Captain, no words would come out of my mouth…"
    toshu "キャプテンを慰めようと思っていたほど、私の口から言葉が出てこない…"

# game/script.rpy:6286
translate japanese route_masaru_6_b30b9b79:

    masaru "Sohma was still too young to be able to remember mom."
    masaru "Sohmaはまだ母親を思い出すことができないほど若すぎました。"

# game/script.rpy:6287
translate japanese route_masaru_6_0a488977:

    masaru "That's why he is what you see today…"
    masaru "だから彼は今日あなたが見るものです…"

# game/script.rpy:6289
translate japanese route_masaru_6_c4f760ef:

    masaru "He had lived life without a mom…"
    masaru "彼はお母さんなしで人生を生きていた…"

# game/script.rpy:6290
translate japanese route_masaru_6_3c80a4e1:

    masaru "And almost without a dad…"
    masaru "そして、お父さんがいなくても…"

# game/script.rpy:6292
translate japanese route_masaru_6_45020ef9:

    masaru "I can understand why he is always crying…"
    masaru "なぜ彼はいつも泣いているのか理解できます…"

# game/script.rpy:6294
translate japanese route_masaru_6_b938c813:

    toshu "C-Captain… *sniff*"
    toshu "C  - キャプテン… *スニフ*"

# game/script.rpy:6296
translate japanese route_masaru_6_78bb9b3f:

    masaru "Ahh!"
    masaru "ああ！"

# game/script.rpy:6298
translate japanese route_masaru_6_7beec952:

    masaru "I'm sorry! It's your birthday! I'm supposed to make you happy not make you cry…!"
    masaru "ごめんなさい！あなたの誕生日です！私はあなたを泣かせないように幸せにするはずです…！"

# game/script.rpy:6299
translate japanese route_masaru_6_eb2784f6:

    toshu "I-I'm really sorry Captain Masaru…!"
    toshu "私は本当に大佐大佐を…申し訳ありません！"

# game/script.rpy:6300
translate japanese route_masaru_6_f66567d7:

    toshu "All this time I thought I understood you… I never knew you were carrying all that pain inside you…"
    toshu "私はあなたを理解していたと思っていました。"

# game/script.rpy:6301
translate japanese route_masaru_6_c5081420:

    toshu "I have no right to be called your friend…"
    toshu "私はあなたの友人と呼ばれる権利がありません…"

# game/script.rpy:6302
translate japanese route_masaru_6_7f438e8c:

    toshu "I can't even make you happy…"
    toshu "私はあなたを幸せにすることさえできません…"

# game/script.rpy:6304
translate japanese route_masaru_6_617ccdc1:

    # masaru "…"
    masaru "…"

# game/script.rpy:6306
translate japanese route_masaru_6_a4783f77:

    masaru "You're wrong, Toshu!"
    masaru "あなたは間違っています。"

# game/script.rpy:6308
translate japanese route_masaru_6_e451cc42:

    toshu_t "Captain's tone changed completely."
    toshu_t "キャプテンのトーンは完全に変わった。"

# game/script.rpy:6309
translate japanese route_masaru_6_bbcf6387:

    toshu_t "I felt my heart just skipped a beat."
    toshu_t "私の心はちょうどビートをスキップしたと感じました。"

# game/script.rpy:6311
translate japanese route_masaru_6_9ded591f:

    masaru "Right now… you are the only person that I can trust completely…"
    masaru "今は…あなたは私が完全に信頼できる唯一の人です…"

# game/script.rpy:6313
translate japanese route_masaru_6_ef27ce10:

    masaru "Every time I am with you… I am filled with joy… and all of my problems disappear!"
    masaru "私はあなたと一緒にいるたびに…私は喜びで満たされています…そして、私の問題はすべて消えてしまいます！"

# game/script.rpy:6318
translate japanese route_masaru_6_f074c5bc:

    masaru "I like you a lot, Toshu!"
    masaru "I like you a lot, Toshu!"

# game/script.rpy:6320
translate japanese route_masaru_6_aa18772a:

    toshu_t "It turns out… I'm not the only who's heart was beating fast."
    toshu_t "それは分かった…私は心が拍手していた唯一の人ではない。"

# game/script.rpy:6321
translate japanese route_masaru_6_7b46408c:

    toshu_t "The feeling I felt towards Captain Masaru… it was mutual."
    toshu_t "私がマサル大尉に感じた気持ち…それは相互だった。"

# game/script.rpy:6322
translate japanese route_masaru_6_1d33cc70:

    toshu_t "I never had this strong feeling towards someone else before in my life…"
    toshu_t "私は人生の前に他の誰かに向かってこの強い感情を覚えたことはありません…"

# game/script.rpy:6325
translate japanese route_masaru_6_c58bf6fe:

    toshu "C-Captain…!"
    toshu "C-キャプテン…！"

# game/script.rpy:6330
translate japanese route_masaru_6_88a32a9f:

    masaru "I'm sorry Toshu…!"
    masaru "I'm sorry Toshu…!"

# game/script.rpy:6331
translate japanese route_masaru_6_9c727e77:

    masaru "I can't hold back what I am feeling anymore!"
    masaru "私はもう私が感じていることを抑えることができません！"

# game/script.rpy:6332
translate japanese route_masaru_6_c1754956:

    toshu "I like Captain too…"
    toshu "キャプテンも大好きです…"

# game/script.rpy:6334
translate japanese route_masaru_6_c7192a40:

    masaru "R-really?!"
    masaru "本当に？"

# game/script.rpy:6335
translate japanese route_masaru_6_4044872e:

    masaru "I'm really glad… that you feel the same way too!"
    masaru "私は本当にうれしいよ…あなたも同じように感じる！"

# game/script.rpy:6336
translate japanese route_masaru_6_4511205f:

    toshu "Hnnh…"
    toshu "Hnnh …"

# game/script.rpy:6338
translate japanese route_masaru_6_73d2c349:

    toshu "Ahh!"
    toshu "ああ！"

# game/script.rpy:6339
translate japanese route_masaru_6_9296fa21:

    masaru "Relax Toshu. I want you to feel good…"
    masaru "Relax Toshu。私はあなたが良い気分にしてほしい…"

# game/script.rpy:6341
translate japanese route_masaru_6_d210fb3f:

    # toshu "C-Captain…! I'm gonna…"
    toshu "キャ、キャプテン…！ 僕もう…"

# game/script.rpy:6343
translate japanese route_masaru_6_760f0b0f:

    # toshu "U-uwahh!"
    toshu "う、うわあぁ！"

# game/script.rpy:6345
translate japanese route_masaru_6_fff6e882:

    # masaru "Are you alright?"
    masaru "大丈夫？"

# game/script.rpy:6346
translate japanese route_masaru_6_aceee90a:

    # toshu "I-I can still go on!"
    toshu "ぼ、僕まだできます！"

# game/script.rpy:6347
translate japanese route_masaru_6_f7ebeafd:

    # masaru "Let's get you ready…!"
    masaru "じゃ、準備しよっか…！"

# game/script.rpy:6348
translate japanese route_masaru_6_2a616c78:

    # toshu "E-eh?"
    toshu "え、えぇ？"

# game/script.rpy:6350
translate japanese route_masaru_6_5649d7ad:

    # toshu "Wah!"
    toshu "うわっ！"

# game/script.rpy:6352
translate japanese route_masaru_6_4a5272b6:

    # toshu "Ahh…"
    toshu "ああ…"

# game/script.rpy:6353
translate japanese route_masaru_6_ec387fbf:

    # masaru "Its really slippery down here…"
    masaru "ここ、ぬるぬるだよ…"

# game/script.rpy:6355
translate japanese route_masaru_6_d07aa9d1:

    # toshu "Hnghh…!"
    toshu "ンンッ…！"

# game/script.rpy:6357
translate japanese route_masaru_6_04a8f9ad:

    # toshu "Haa… haa…"
    toshu "ハア…ハァ…"

# game/script.rpy:6358
translate japanese route_masaru_6_f0fc9e8e:

    # masaru "You are truly adorable…"
    masaru "ホントに可愛いね…"

# game/script.rpy:6360
translate japanese route_masaru_6_8aee13b4:

    toshu_t "Things were really getting out of hand between me and Captain Masaru…"
    toshu_t "私とCaptain Masaruの間には、本当に間違っていました…"

# game/script.rpy:6361
translate japanese route_masaru_6_4520540c:

    # masaru "Take off your clothes."
    masaru "服を脱いで。"

# game/script.rpy:6362
translate japanese route_masaru_6_6fd56a4b:

    # toshu_t "Without any second thoughts, I did as Captain said."
    toshu_t "僕はただただキャプテンが言うようにした。"

# game/script.rpy:6363
translate japanese route_masaru_6_fc19951b:

    toshu_t "Watching him take off his shirt, he then put himself on top of me…"
    toshu_t "彼がシャツを脱ぐのを見て、彼は私の上に身を置いた。"

# game/script.rpy:6364
translate japanese route_masaru_6_62da7022:

    toshu_t "We're definitely both in heat… I'm not sure what I've gotten myself into."
    toshu_t "私たちは間違いなく熱の両方です…私は自分自身に何を得たか分かりません。"

# game/script.rpy:6369
translate japanese route_masaru_6_d41d3d67:

    masaru "A-are you ready, Toshu?"
    masaru "A-are you ready, Toshu?"

# game/script.rpy:6370
translate japanese route_masaru_6_06b54fec:

    toshu "Are we going to…"
    toshu "私たちは行くつもりです…"

# game/script.rpy:6371
translate japanese route_masaru_6_42f45b55:

    # masaru "Yeah… please endure it a bit…"
    masaru "ん…ちょっとガマンしてね…"

# game/script.rpy:6372
translate japanese route_masaru_6_b0d70326:

    # masaru "I'm gonna go in now…"
    masaru "入れるよ…"

# game/script.rpy:6374
translate japanese route_masaru_6_7b8a5c32:

    # toshu "Ahhh…!"
    toshu "ああ…！"

# game/script.rpy:6375
translate japanese route_masaru_6_88cc99b3:

    # masaru "Mmmhh…"
    masaru "んん…"

# game/script.rpy:6376
translate japanese route_masaru_6_59084021_1:

    # toshu "C-Captain…"
    toshu "キャ、キャプテン…"

# game/script.rpy:6377
translate japanese route_masaru_6_1f5fdc56:

    # masaru "You're so tight, Toshu…"
    masaru "Toshu、すごくよく締まるよ…"

# game/script.rpy:6379
translate japanese route_masaru_6_56a8a922:

    # toshu "Nghhh…!!"
    toshu "ンンッ…！！"

# game/script.rpy:6380
translate japanese route_masaru_6_40cba201:

    # masaru "This is unbelievable…"
    masaru "すごい…"

# game/script.rpy:6381
translate japanese route_masaru_6_078ac9c8:

    # toshu_t "I can't believe this…! Captain Masaru's completely inside me!"
    toshu_t "信じられない…！ Masaruキャプテンが僕の中に入ってる！"

# game/script.rpy:6383
translate japanese route_masaru_6_aa5abefd:

    # masaru "Toshu…!"
    masaru "Toshu…！"

# game/script.rpy:6384
translate japanese route_masaru_6_10e43dd9:

    toshu "Erghh…!"
    toshu "Erghh …！"

# game/script.rpy:6385
translate japanese route_masaru_6_b99c4661:

    toshu_t "Despite Captain Masaru's manly exterior, he was pretty gentle and passionate with his every thrust."
    toshu_t "マサール大尉の男らしい外見にもかかわらず、彼は彼のあらゆる勢いでとても優しく情熱的でした。"

# game/script.rpy:6386
translate japanese route_masaru_6_66927ee9:

    # toshu "Captain…! I'm gonna--"
    toshu "キャプテン…！ 俺イッちゃ…"

# game/script.rpy:6388
translate japanese route_masaru_6_78bb9b3f_1:

    # masaru "Ahh!"
    masaru "ああ！"

# game/script.rpy:6389
translate japanese route_masaru_6_3a2283d8:

    # toshu "Haaa…!!"
    toshu "はあぁ…！！"

# game/script.rpy:6390
translate japanese route_masaru_6_4d9cbe4a:

    toshu_t "Captain's release made me tremble. I can totally feel the liquid gushing inside…"
    toshu_t "キャプテンのリリースは私を震わせさせた。私は完全に内部に噴出している液体を感じることができます…"

# game/script.rpy:6392
translate japanese route_masaru_6_7de9154b:

    # masaru "Haa… did I hurt you…?"
    masaru "はぁ…痛くなかった？"

# game/script.rpy:6393
translate japanese route_masaru_6_a2223056:

    # toshu "N-no, Captain…"
    toshu "う…ううん、キャプテン…"

# game/script.rpy:6395
translate japanese route_masaru_6_e6af334c:

    toshu_t "As Captain pulled out his member, his warm breath dropped over my cheeks."
    toshu_t "キャプテンがメンバーを引き抜くと、彼の暖かい息が私の頬の上に落ちた。"

# game/script.rpy:6396
translate japanese route_masaru_6_aab51c25:

    toshu_t "That image of Captain was so priceless…"
    toshu_t "キャプテンのそのイメージはとても貴重だった…"

# game/script.rpy:6397
translate japanese route_masaru_6_3d86d395:

    masaru "T-this is the first time I did something like this…"
    masaru "T-これが私がこのようなことをしたのは初めてです…"

# game/script.rpy:6398
translate japanese route_masaru_6_f3486080:

    toshu "Me too… Captain…"
    toshu "私も…キャプテン…"

# game/script.rpy:6400
translate japanese route_masaru_6_e1503030:

    toshu_t "It's true… it was the first time someone went inside me…"
    toshu_t "それは本当です…誰かが私の中に入ったのは初めてでした…"

# game/script.rpy:6401
translate japanese route_masaru_6_c2455b15:

    toshu_t "Right after everything that had happened…"
    toshu_t "起こったすべての直後…"

# game/script.rpy:6402
translate japanese route_masaru_6_250dc61e:

    toshu_t "I was left speechless…"
    toshu_t "私は言葉が残っていた…"

# game/script.rpy:6403
translate japanese route_masaru_6_77e2e6e4:

    toshu_t "I can't believe Masaru was carrying all that weight on his shoulders all this time."
    toshu_t "私は、大丈夫が今度は彼の肩にすべての体重を運んでいたとは思えません。"

# game/script.rpy:6404
translate japanese route_masaru_6_f04cfa9c:

    toshu_t "I'm really glad I was able to make him release all of it."
    toshu_t "私は本当に彼がそのすべてを解放できるようになったことをうれしく思っています。"

# game/script.rpy:6412
translate japanese route_masaru_6_b47c2e99:

    masaru "Sorry for bursting out like that all of a sudden…"
    masaru "突然そのように爆発して申し訳ありません…"

# game/script.rpy:6414
translate japanese route_masaru_6_63090420:

    toshu "Don't worry about it Captain!"
    toshu "それについて心配しないでください。キャプテン！"

# game/script.rpy:6416
translate japanese route_masaru_6_9098a925:

    toshu "I'm glad you told me what was bothering you."
    toshu "あなたを悩ましていたことを教えてくれてうれしいです。"

# game/script.rpy:6418
translate japanese route_masaru_6_b7a5fe70:

    toshu "It made me feel like I am a person you trust."
    toshu "それは私があなたが信頼する人であるように感じました。"

# game/script.rpy:6419
translate japanese route_masaru_6_1f5ee747:

    toshu "Please cheer up okay?"
    toshu "元気で元気？"

# game/script.rpy:6421
translate japanese route_masaru_6_7bd58569:

    masaru "Thank you. It feels really relieving to have all my stress released."
    masaru "ありがとうございました。すべてのストレスが解放されることは本当に気になります。"

# game/script.rpy:6423
translate japanese route_masaru_6_cf30c22f:

    masaru "I have a new reason to smile now."
    masaru "私は今、笑顔になる新しい理由があります。"

# game/script.rpy:6427
translate japanese route_masaru_6_5ea855b7:

    toshu "Oh!"
    toshu "ああ！"

# game/script.rpy:6428
translate japanese route_masaru_6_d17cbd93:

    ichiru_c "Hello, Tofu!"
    ichiru_c "こんにちは、Tofu！"

# game/script.rpy:6430
translate japanese route_masaru_6_fa4e9a95:

    toshu "I…Ichiru!"
    toshu "I…Ichiru!"

# game/script.rpy:6431
translate japanese route_masaru_6_cd62c2cb:

    ichiru_c "I wanted to greet you 'HAPPY HAPPY BIRTHDAY'!"
    ichiru_c "私はあなたにハッピーハッピーバースデーを迎えたい！"

# game/script.rpy:6432
translate japanese route_masaru_6_5e2b7529:

    ichiru_c "I see you're using our gift for you! I hope you liked it!"
    ichiru_c "私はあなたのために私たちの贈り物を使用して参照してください！私はあなたがそれを好きであることを望む"

# game/script.rpy:6434
translate japanese route_masaru_6_4c836b0b:

    toshu "Yes! Thank you so much!"
    toshu "はい！どうもありがとうございます！"

# game/script.rpy:6435
translate japanese route_masaru_6_14ba06a5:

    ichiru_c "I'm very sorry I couldn't go... Tomoka won't let me go out today..."
    ichiru_c "私は行くことができなかったのは大変残念です…トモカは私を今日外出させません…"

# game/script.rpy:6436
translate japanese route_masaru_6_5f23e1ed:

    ichiru_c "Not to mention being stuck in this stupid and boring family meeting...!"
    ichiru_c "この愚かで退屈な家族会議にこだわっていることは言うまでもありません…！"

# game/script.rpy:6438
translate japanese route_masaru_6_10ab98bc:

    toshu "It's okay Ichiru. I understand!"
    toshu "それは大丈夫です。わかりました！"

# game/script.rpy:6439
translate japanese route_masaru_6_e178c639:

    ichiru_c "That's a relief... I thought you were angry at me."
    ichiru_c "それは救済だ…私はあなたが私に怒っていると思った。"

# game/script.rpy:6440
translate japanese route_masaru_6_1ef0ebb7:

    ichiru_c "Anyway, I have to go. Tomoka's yelling again."
    ichiru_c "とにかく、私は行かなければならない。トモカは再び叫ぶ。"

# game/script.rpy:6442
translate japanese route_masaru_6_f2fac1c0:

    toshu "Okay, Ichiru! Please take care there!"
    toshu "さて、Ichiru！そこに注意してください！"

# game/script.rpy:6445
translate japanese route_masaru_6_35b9887e:

    masaru "I'm glad he was able to steal time to call you! I thought he's not gonna make it!"
    masaru "彼はあなたに電話する時間を盗むことができてうれしいです！私は彼がそれを作るつもりはないと思った！"

# game/script.rpy:6447
translate japanese route_masaru_6_4630106e:

    toshu "Y-yeah… I feel kinda guilty…"
    toshu "名前：Y-ええ…私はちょっと有罪を感じる…"

# game/script.rpy:6449
translate japanese route_masaru_6_1b27174b:

    masaru "Don't be! Just appreciate it! We all want you to have a great time!"
    masaru "ありません！ただそれを感謝します！私たちは皆、素晴らしい時間を過ごしたいです！"

# game/script.rpy:6451
translate japanese route_masaru_6_a94ffb17:

    toshu "Thanks to you, Captain, my birthday was really a BLAST!"
    toshu "あなたのおかげで、キャプテン、私の誕生日は本当にBLASTでした！"

# game/script.rpy:6453
translate japanese route_masaru_6_682853df:

    masaru "Well, I should be heading home now… it's getting late."
    masaru "さて、私は今帰るべきです…遅くなっています。"

# game/script.rpy:6454
translate japanese route_masaru_6_3890c387:

    toshu "Ah… right, Captain!"
    toshu "ああ…そうです、キャプテン！"

# game/script.rpy:6456
translate japanese route_masaru_6_95f4a1e4:

    masaru "I noticed you've been calling me Captain all this time."
    masaru "私はあなたがこの時間に私をキャプテンと呼んでいることに気づいた。"

# game/script.rpy:6458
translate japanese route_masaru_6_e2c2bc66:

    masaru "You can just call me by my name, you know?"
    masaru "あなたは私の名前で私に電話することができます。"

# game/script.rpy:6459
translate japanese route_masaru_6_1c2bd3a7:

    toshu "Ohh…"
    toshu "ああ…"

# game/script.rpy:6460
translate japanese route_masaru_6_0a265e04:

    masaru "Let me get dressed."
    masaru "私は服を着せさせてください。"

# game/script.rpy:6464
translate japanese route_masaru_6_763b44cb:

    toshu "Fwahh… Captain Masaru's really sexy…"
    toshu "Fwahh …キャプテンマサルは本当にセクシーです…"

# game/script.rpy:6466
translate japanese route_masaru_6_b6330177:

    masaru "There…"
    masaru "そこ…"

# game/script.rpy:6468
translate japanese route_masaru_6_bb427f3a:

    masaru "I hope you really had a great birthday today."
    masaru "私は今日あなたが本当に素晴らしい誕生日をお祈りしています。"

# game/script.rpy:6470
translate japanese route_masaru_6_cb4cbd9d:

    masaru "I have to go now, okay?"
    masaru "今は行かなければなりません。"

# game/script.rpy:6472
translate japanese route_masaru_6_3b3776e0:

    masaru "Goodbye Toshu!"
    masaru "Goodbye Toshu!"

# game/script.rpy:6474
translate japanese route_masaru_6_302ad6e9:

    toshu "Goodbye… Masaru! Please take care!"
    toshu "さようなら…マサル！気をつけてください！"

# game/script.rpy:6476
translate japanese route_masaru_6_691a5b3c:

    masaru "See you again!"
    masaru "またね！"

# game/script.rpy:6478
translate japanese route_masaru_6_107634d5:

    toshu_t "For Captain to let me call him by his name…"
    toshu_t "キャプテンが私の名前で彼に電話するように…"

# game/script.rpy:6479
translate japanese route_masaru_6_30da3e9d:

    toshu_t "It felt really special!"
    toshu_t "それは本当に特別な感じでした！"

# game/script.rpy:6480
translate japanese route_masaru_6_8ef2090b:

    toshu_t "I feel like our relationship is much more than what I expected!"
    toshu_t "私の関係は私が期待していたものよりはるかに多いように感じます！"

# game/script.rpy:6487
translate japanese route_ichiru_6_a2c5bd2d:

    toshu "Hello?"
    toshu "こんにちは？"

# game/script.rpy:6488
translate japanese route_ichiru_6_41ec3295:

    random "Tofu!"
    random "Tofu！"

# game/script.rpy:6490
translate japanese route_ichiru_6_76f72096:

    toshu "I-Ichiru?!"
    toshu "I-Ichiru?!"

# game/script.rpy:6491
translate japanese route_ichiru_6_20f1fa96:

    ichiru "Yup!"
    ichiru "うん！"

# game/script.rpy:6493
translate japanese route_ichiru_6_224bfb62:

    toshu "Hello, Ichiru!"
    toshu "Hello, Ichiru!"

# game/script.rpy:6494
translate japanese route_ichiru_6_4ca750c4:

    toshu "It's been a while!"
    toshu "それはしばらくしている！"

# game/script.rpy:6496
translate japanese route_ichiru_6_8952eb41:

    toshu "How are you doing?"
    toshu "お元気ですか？"

# game/script.rpy:6497
translate japanese route_ichiru_6_02cff764:

    ichiru "I'm doing great!"
    ichiru "私は素晴らしいことをやっている！"

# game/script.rpy:6498
translate japanese route_ichiru_6_128d2b6a:

    ichiru "I missed you so much!"
    ichiru "私はそんなにあなたを逃した！"

# game/script.rpy:6500
translate japanese route_ichiru_6_b988d5ba:

    toshu "Me too! I've been missing you guys so bad!!"
    toshu "私も！私はあなたがとても悪いことを逃してきた！"

# game/script.rpy:6501
translate japanese route_ichiru_6_e83996ee:

    toshu "Ichiru, are you free today?"
    toshu "イチル、今日は無料ですか？"

# game/script.rpy:6502
translate japanese route_ichiru_6_85a2e5f8:

    toshu "I-I was wondering if you can--"
    toshu "私はあなたができるかどうか疑問に思っていました -"

# game/script.rpy:6503
translate japanese route_ichiru_6_4790bd64:

    ichiru "Enough talking! There's something for you at your doorstep!"
    ichiru "十分な話！あなたの玄関先には何かがあります！"

# game/script.rpy:6505
translate japanese route_ichiru_6_32a31c30:

    toshu "E-Ehh? What do you mean Ichiru?"
    toshu "E-Ehh？イチールって何？"

# game/script.rpy:6511
translate japanese route_ichiru_6_ec9d3d3c:

    toshu "Ichiru!!"
    toshu "Ichiru!!"

# game/script.rpy:6512
translate japanese route_ichiru_6_b261dd96:

    ichiru "HAPPY BIRTHDAY TOFU!!!"
    ichiru "ハッピーバースデーTofu!!!"

# game/script.rpy:6513
translate japanese route_ichiru_6_5a93879a:

    toshu "Ichiru… I…"
    toshu "Ichiru… I…"

# game/script.rpy:6521
translate japanese route_ichiru_6_f9e4aa52:

    toshu "*sniff*"
    toshu "*スニフ*"

# game/script.rpy:6523
translate japanese route_ichiru_6_0927a01b:

    toshu "Thank you Ichiru…!"
    toshu "イチールありがとう…！"

# game/script.rpy:6525
translate japanese route_ichiru_6_57cca8cc:

    ichiru "Awww! Don't cry now, Tofu!"
    ichiru "Awww！泣いてはいけない、Tofu！"

# game/script.rpy:6527
translate japanese route_ichiru_6_1183f5ab:

    toshu "How did you know it was my birthday today?!"
    toshu "あなたが今日私の誕生日だったことをどうやって知ったのですか？"

# game/script.rpy:6529
translate japanese route_ichiru_6_60cf5e67:

    ichiru "Of course! I know everything about you!"
    ichiru "もちろん！私はあなたについてすべてを知っている！"

# game/script.rpy:6531
translate japanese route_ichiru_6_19029a45:

    toshu "W-wait… how did you get my phone number too…? This phone is brand new."
    toshu "待って…私の電話番号もどうやって得たの？この携帯電話は新品です。"

# game/script.rpy:6532
translate japanese route_ichiru_6_43161cd8:

    ichiru "Yup! Brand new as in, COACH, ME and MASARU bought that for you as a present!"
    ichiru "うん！ COACH、MEとMASARUがあなたのためにプレゼントとして買ったように、まったく新しい！"

# game/script.rpy:6534
translate japanese route_ichiru_6_7b4ae5de:

    toshu "Whaa…"
    toshu "わぁ…"

# game/script.rpy:6536
translate japanese route_ichiru_6_43b7d02b:

    ichiru "We sent it to your aunt before winter break. A shocker, ain't it?!"
    ichiru "私たちはあなたの叔母に冬休みの前にそれを送った。ショッカー、そうじゃないの？"

# game/script.rpy:6538
translate japanese route_ichiru_6_b78e1631:

    toshu "T-thank you…!"
    toshu "T-ありがとう！"

# game/script.rpy:6540
translate japanese route_ichiru_6_325c07bf:

    ichiru "Anything to make you happy, Tofu! You're really special, don't forget that!"
    ichiru "あなたを幸せにする何か、Tofu！あなたは本当に特別です、それを忘れないでください！"

# game/script.rpy:6542
translate japanese route_ichiru_6_c52ff1aa:

    toshu "B-but aren't you busy with your family?"
    toshu "B-しかしあなたの家族と一緒に忙しいですか？"

# game/script.rpy:6544
translate japanese route_ichiru_6_8ca47ced:

    ichiru "Yeah… dad has been scheduling family meetings one after another."
    ichiru "ええと、お父さんは家族会議を次々と予定しています。"

# game/script.rpy:6546
translate japanese route_ichiru_6_9360d5ef:

    ichiru "I got tired of it. So I escaped!"
    ichiru "私はそれに疲れました。だから私は脱出した！"

# game/script.rpy:6548
translate japanese route_ichiru_6_dbfd19bc:

    ichiru "And besides, it's your birthday, I wouldn't miss it for the world!"
    ichiru "そして、それはあなたの誕生日です、私は世界のためにそれを逃すことはありません！"

# game/script.rpy:6550
translate japanese route_ichiru_6_3671faac:

    toshu "I'm so touched… I really feel special…!"
    toshu "私はとても感動しています…私は本当に特別な気がします…！"

# game/script.rpy:6552
translate japanese route_ichiru_6_61ddad8f:

    ichiru "That's what a birthday supposed to feel like!"
    ichiru "それは誕生日のように感じるはずです！"

# game/script.rpy:6554
translate japanese route_ichiru_6_6348e77a:

    ichiru "Anyway, nice place you got here!"
    ichiru "とにかくここにきた素敵な場所！"

# game/script.rpy:6556
translate japanese route_ichiru_6_32e09851:

    ichiru "…and you look as cute as always!"
    ichiru "…あなたはいつものようにかわいく見えます！"

# game/script.rpy:6558
translate japanese route_ichiru_6_4a5272b6:

    toshu "Ahh…"
    toshu "ああ…"

# game/script.rpy:6560
translate japanese route_ichiru_6_a1adcdfd:

    toshu "I haven't cleaned yet! I'm sorry if it's messy!"
    toshu "私はまだきれいにしていない！それが面倒であればすみません！"

# game/script.rpy:6562
translate japanese route_ichiru_6_e232bb43:

    toshu "I wasn't expecting anyone would come, to be honest!"
    toshu "正直なところ誰も来るとは思っていませんでした！"

# game/script.rpy:6563
translate japanese route_ichiru_6_c894def7:

    toshu "A-anyway, let me get some plates! Let's eat the cake you brought!"
    toshu "A-とにかく、いくつかのプレートを手に入れよう！あなたが持ってきたケーキを食べましょう！"

# game/script.rpy:6565
translate japanese route_ichiru_6_372df359:

    toshu_t "Ichiru… came for my birthday…"
    toshu_t "イチル…私の誕生日に来た…"

# game/script.rpy:6566
translate japanese route_ichiru_6_181c5985:

    toshu_t "That's so thoughtful of him!"
    toshu_t "それは彼のことをとても思慮深いものにしています！"

# game/script.rpy:6567
translate japanese route_ichiru_6_09674fc5:

    toshu_t "I feel so special…"
    toshu_t "私はとても特別な気がします…"

# game/script.rpy:6568
translate japanese route_ichiru_6_31a84661:

    toshu_t "W-what's wrong with me…?"
    toshu_t "名前：W-何が私の間違っている…？"

# game/script.rpy:6569
translate japanese route_ichiru_6_cf93f73d:

    toshu_t "My heart is beating so fast…"
    toshu_t "私の心はとても速く鼓動しています…"

# game/script.rpy:6577
translate japanese route_ichiru_6_aebaee87:

    toshu "By the way Ichiru, where's Captain?"
    toshu "ちなみにIchiru、キャプテンはどこですか？"

# game/script.rpy:6578
translate japanese route_ichiru_6_dfd41c2f:

    toshu "I wanted to invite him here too."
    toshu "彼もここに招待したいと思っていました。"

# game/script.rpy:6580
translate japanese route_ichiru_6_9d5ebbd7:

    ichiru "Oh, he said he can't go. He's celebrating an occasion with his brother."
    ichiru "ああ、彼は行くことができないと言った。彼は弟と一緒に行事を祝っている。"

# game/script.rpy:6582
translate japanese route_ichiru_6_ba4d8024:

    ichiru "He told me to tell you Happy Birthday!"
    ichiru "彼はあなたにハッピーバースデーを教えてくれた！"

# game/script.rpy:6584
translate japanese route_ichiru_6_2df8890c:

    toshu "Ah I see! I hope he's having a great time too!"
    toshu "ああ、なるほど！彼は素晴らしい時間を過ごしていることを願っています！"

# game/script.rpy:6589
translate japanese route_ichiru_6_a0aa9c5a:

    ichiru "Hmmm…"
    ichiru "うーん…"

# game/script.rpy:6591
translate japanese route_ichiru_6_6b969b09:

    toshu "What is it Ichiru?"
    toshu "それは何ですか？"

# game/script.rpy:6592
translate japanese route_ichiru_6_4dc8bcb0:

    ichiru "You always worry about Masaru."
    ichiru "あなたはいつもマサルについて心配しています。"

# game/script.rpy:6594
translate japanese route_ichiru_6_4d645b43:

    ichiru "You like him don't you?"
    ichiru "彼はあなたが好きじゃない？"

# game/script.rpy:6596
translate japanese route_ichiru_6_6414c872:

    toshu "Wh-what? Ichiru what are you talking about?"
    toshu "何？ Ichiru何を話しているのですか？"

# game/script.rpy:6598
translate japanese route_ichiru_6_9e0f74aa:

    toshu "It's not like that! We are all friends, right?"
    toshu "それはそうではありません！私たちは皆友達ですよね？"

# game/script.rpy:6600
translate japanese route_ichiru_6_eef3cd22:

    ichiru "Hahaha!! Of course I'm just kidding!"
    ichiru "ははは！！もちろん私は冗談だよ！"

# game/script.rpy:6602
translate japanese route_ichiru_6_4a5272b6_1:

    toshu "Ahh…"
    toshu "ああ…"

# game/script.rpy:6604
translate japanese route_ichiru_6_2f4ad796:

    ichiru "U-umm… c-can I ask you something?"
    ichiru "うーん、私に何か聞いてもらえますか？"

# game/script.rpy:6605
translate japanese route_ichiru_6_4a6015df:

    toshu "Yes…?"
    toshu "はい…？"

# game/script.rpy:6607
translate japanese route_ichiru_6_fb2ff947:

    ichiru "W-what d-do you think of me…?"
    ichiru "あなたは私のことをどう思いますか？"

# game/script.rpy:6609
translate japanese route_ichiru_6_9f3c5e21:

    toshu "What do I think of you?"
    toshu "あなたはどう思いますか？"

# game/script.rpy:6611
translate japanese route_ichiru_6_65991390:

    toshu "H-how should I put this… let's see…"
    toshu "私はどうやってこれを置くべきですか…見てみましょう…"

# game/script.rpy:6613
translate japanese route_ichiru_6_c7a941ad:

    toshu "Well… Ichiru, you are very funny! You always make me laugh and there was never a dull moment with you."
    toshu "名前：まあ…Ichiru、あなたは非常に面白いです！あなたはいつも私を笑わせるようにして、決してあなたと鈍い瞬間はなかった。"

# game/script.rpy:6615
translate japanese route_ichiru_6_b25dffd8:

    toshu "You're also very nice to me!"
    toshu "あなたは私にもとても素敵です！"

# game/script.rpy:6617
translate japanese route_ichiru_6_a41fc6d6:

    toshu "I'm so grateful to have a friend like you."
    toshu "私はあなたのような友達がいることにとても感謝しています。"

# game/script.rpy:6619
translate japanese route_ichiru_6_c88630c8:

    ichiru "Y-you see me as a friend only?"
    ichiru "あなたは私を友人としてのみ見ますか？"

# game/script.rpy:6621
translate japanese route_ichiru_6_4a612db4:

    toshu "N-no! It's… well…"
    toshu "いいえ！まあいいんじゃない…"

# game/script.rpy:6623
translate japanese route_ichiru_6_7ad64394:

    toshu "Definitely more than that actually…!"
    toshu "確かにそれよりも実際には…！"

# game/script.rpy:6625
translate japanese route_ichiru_6_701a2703:

    ichiru "R-really?"
    ichiru "本当に？"

# game/script.rpy:6627
translate japanese route_ichiru_6_fc968d86:

    ichiru "Because! I really like you Toshu!"
    ichiru "だから！私は本当にあなたが好きです！"

# game/script.rpy:6628
translate japanese route_ichiru_6_bca5d95d:

    ichiru "I really like you a lot!"
    ichiru "私は本当にあなたが大好き！"

# game/script.rpy:6629
translate japanese route_ichiru_6_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:6631
translate japanese route_ichiru_6_ad726920:

    toshu_t "I was speechless."
    toshu_t "私は言葉がありませんでした。"

# game/script.rpy:6632
translate japanese route_ichiru_6_aa18772a:

    toshu_t "It turns out… I'm not the only who's heart was beating fast."
    toshu_t "それは分かった…私は心が拍手していた唯一の人ではない。"

# game/script.rpy:6633
translate japanese route_ichiru_6_70665bb7:

    toshu_t "The feeling I felt towards Ichiru… it was mutual."
    toshu_t "イチールに向かって感じた気持ち…それは相互だった。"

# game/script.rpy:6634
translate japanese route_ichiru_6_1d33cc70:

    toshu_t "I never had this strong feeling towards someone else before in my life…"
    toshu_t "私は人生の前に他の誰かに向かってこの強い感情を覚えたことはありません…"

# game/script.rpy:6639
translate japanese route_ichiru_6_959c9ae8:

    toshu "W-wah! Ichiru!"
    toshu "で、ワウ！ Ichiru！"

# game/script.rpy:6640
translate japanese route_ichiru_6_d3e8e70a:

    toshu "What are you doing?"
    toshu "何してるの？"

# game/script.rpy:6641
translate japanese route_ichiru_6_24552593:

    ichiru "Just relax!"
    ichiru "ただリラックス！"

# game/script.rpy:6642
translate japanese route_ichiru_6_853ac4c3:

    ichiru "I want to make you feel good!"
    ichiru "私はあなたが気分が良くなるようにしたい！"

# game/script.rpy:6643
translate japanese route_ichiru_6_a81973d5:

    toshu "W-wait! That's dirty!"
    toshu "W待って！それは汚いです！"

# game/script.rpy:6644
translate japanese route_ichiru_6_0f916af7:

    ichiru "I don't care!"
    ichiru "私は気にしない！"

# game/script.rpy:6646
translate japanese route_ichiru_6_2d28d8e8:

    toshu "Ahnn~"
    toshu "アン〜"

# game/script.rpy:6647
translate japanese route_ichiru_6_8fbc1d43:

    toshu "I-it tickles Ichiru!"
    toshu "私はイチールをくすぐる！"

# game/script.rpy:6648
translate japanese route_ichiru_6_e162c40e:

    toshu_t "I wasn't sure at that point what Ichiru was doing…"
    toshu_t "Ichiruがやっていたことは、その時点では分かりませんでした…"

# game/script.rpy:6649
translate japanese route_ichiru_6_46098c2c:

    toshu_t "But it definitely felt good."
    toshu_t "しかし、それは間違いなく良い感じでした。"

# game/script.rpy:6651
translate japanese route_ichiru_6_e9708f49:

    toshu "Ahh! It feels… good…"
    toshu "ああ！それは…良いことを感じる…"

# game/script.rpy:6653
translate japanese route_ichiru_6_73d2c349:

    toshu "Ahh!"
    toshu "ああ！"

# game/script.rpy:6654
translate japanese route_ichiru_6_7e126611:

    ichiru "Hnn~"
    ichiru "Hnn〜"

# game/script.rpy:6656
translate japanese route_ichiru_6_114cbebc:

    toshu "Haa…"
    toshu "はぁ…"

# game/script.rpy:6657
translate japanese route_ichiru_6_03bff98d:

    toshu "This is so embarrassing…"
    toshu "これはとても恥ずかしいです…"

# game/script.rpy:6658
translate japanese route_ichiru_6_3d55b41f:

    ichiru "You are so cute, Tofu…"
    ichiru "あなたはとてもかわいい、Tofuです…"

# game/script.rpy:6660
translate japanese route_ichiru_6_8f488db4:

    ichiru "It's so slippery and wet!"
    ichiru "とても滑りやすく濡れています！"

# game/script.rpy:6661
translate japanese route_ichiru_6_d6d32b2f:

    toshu_t "I feel very heated with what Ichiru did… he sounds like he's planning to do something else…"
    toshu_t "私はイチールがやったことで非常に熱く感じられます…彼は彼が何か他のことを計画しているように聞こえます…"

# game/script.rpy:6662
translate japanese route_ichiru_6_8d13c6c0:

    ichiru "I think you are ready!"
    ichiru "私はあなたが準備ができていると思います！"

# game/script.rpy:6669
translate japanese route_ichiru_6_1e3e51b5:

    toshu "R-ready for what…?"
    toshu "何のための準備ができて…？"

# game/script.rpy:6671
translate japanese route_ichiru_6_883932d2:

    ichiru "It's a surprise!"
    ichiru "驚きです！"

# game/script.rpy:6673
translate japanese route_ichiru_6_1c2bd3a7:

    toshu "Ohh…"
    toshu "ああ…"

# game/script.rpy:6675
translate japanese route_ichiru_6_b064fdf7:

    ichiru "C'mon, take off your clothes!"
    ichiru "さあ、あなたの服を脱ぐ！"

# game/script.rpy:6677
translate japanese route_ichiru_6_7bbf1e89:

    toshu "O-okay."
    toshu "O-okay."

# game/script.rpy:6681
translate japanese route_ichiru_6_57d631cb:

    ichiru "You look so cute when you are fully naked!"
    ichiru "あなたは完全に裸であるときあなたはとてもかわいく見えます！"

# game/script.rpy:6683
translate japanese route_ichiru_6_fa5971f8:

    toshu "I'm so embarrassed…"
    toshu "私はとても恥ずかしいです…"

# game/script.rpy:6685
translate japanese route_ichiru_6_a30613b0:

    ichiru "You don't have to be! There's only you and me here anyway!"
    ichiru "あなたはする必要はありません！とにかくここにはあなたと私だけしかいません！"

# game/script.rpy:6686
translate japanese route_ichiru_6_a29aa0b8:

    ichiru "I'll take off mine too!"
    ichiru "私も私のものを離れるでしょう！"

# game/script.rpy:6688
translate japanese route_ichiru_6_abfa584a:

    ichiru "There we go!"
    ichiru "そこに行く！"

# game/script.rpy:6690
translate japanese route_ichiru_6_2a014e8a:

    toshu "Y-you look really sexy, Ichiru!"
    toshu "あなたは本当にセクシーに見える、Ichiru！"

# game/script.rpy:6692
translate japanese route_ichiru_6_76b54647:

    ichiru "Naughty Toshu…"
    ichiru "Naughty Toshu…"

# game/script.rpy:6694
translate japanese route_ichiru_6_96a8a249:

    ichiru "Let's make this fast! I'm really excited!"
    ichiru "これを速くしよう！私は本当に興奮しています！"

# game/script.rpy:6696
translate japanese route_ichiru_6_e3f93e3c:

    toshu_t "Things were really getting out of hand between me and Ichiru…"
    toshu_t "私とイチルとの間には物事が本当に邪魔になりました…"

# game/script.rpy:6697
translate japanese route_ichiru_6_62da7022:

    toshu_t "We're definitely both in heat… I'm not sure what I've gotten myself into."
    toshu_t "私たちは間違いなく熱の両方です…私は自分自身に何を得たか分かりません。"

# game/script.rpy:6701
translate japanese route_ichiru_6_ef86fd43:

    toshu "Ahh…!"
    toshu "ああ…！"

# game/script.rpy:6702
translate japanese route_ichiru_6_9197d109:

    toshu "A-are you okay Ichiru?"
    toshu "A-are you okay Ichiru?"

# game/script.rpy:6703
translate japanese route_ichiru_6_a79a24dd:

    toshu "Am I heavy…?"
    toshu "私は重いです…？"

# game/script.rpy:6704
translate japanese route_ichiru_6_5fa3c9b2:

    ichiru "No not at all! Don't worry about that!"
    ichiru "いいえ、全くありません！それを心配しないで！"

# game/script.rpy:6705
translate japanese route_ichiru_6_24552593_1:

    ichiru "Just relax!"
    ichiru "ただリラックス！"

# game/script.rpy:6706
translate japanese route_ichiru_6_1aa37fc9:

    ichiru "I'm gonna go in now!"
    ichiru "私は今行くつもりだ！"

# game/script.rpy:6707
translate japanese route_ichiru_6_11720901:

    toshu "Are we really gonna--"
    toshu "私たちは本当に -"

# game/script.rpy:6709
translate japanese route_ichiru_6_634ba741:

    toshu "Hnnn!"
    toshu "Hnnn！"

# game/script.rpy:6710
translate japanese route_ichiru_6_b73984f7:

    ichiru "Does it hurt Tofu?"
    ichiru "Tofuを傷つけるのですか？"

# game/script.rpy:6711
translate japanese route_ichiru_6_5c11ec62:

    toshu "Y-yeah…!"
    toshu "Y-yeah …！"

# game/script.rpy:6712
translate japanese route_ichiru_6_f452bb52:

    toshu "I-it feels weird!"
    toshu "私は変だと感じる！"

# game/script.rpy:6713
translate japanese route_ichiru_6_658065bf:

    ichiru "Just endure it a bit…"
    ichiru "ちょっと待って…"

# game/script.rpy:6715
translate japanese route_ichiru_6_9ad5f5b5:

    ichiru "Wow it's so damn slippery inside!"
    ichiru "うわー、それは本当に内部の滑りやすいです！"

# game/script.rpy:6716
translate japanese route_ichiru_6_d8a9c442:

    toshu "Ahhh!!"
    toshu "ああ！"

# game/script.rpy:6717
translate japanese route_ichiru_6_6e466f8d:

    toshu_t "It's… starting to feel really good…"
    toshu_t "それは…本当に良い気分になり始めている…"

# game/script.rpy:6718
translate japanese route_ichiru_6_99b62dcc:

    toshu_t "Ichiru is moving so robustly… at this rate I'm going to--"
    toshu_t "Ichiruはとても頑丈に動いています…このレートで私は行くつもりです -"

# game/script.rpy:6720
translate japanese route_ichiru_6_57775018:

    toshu "Hngh!"
    toshu "Hngh！"

# game/script.rpy:6721
translate japanese route_ichiru_6_68682d2c:

    ichiru "Ahh!"
    ichiru "ああ！"

# game/script.rpy:6722
translate japanese route_ichiru_6_e9faa507:

    toshu_t "I totally felt Ichiru's release gush strong inside me."
    toshu_t "私はIchiruの解放が、私の中で強烈に強く感じられた。"

# game/script.rpy:6724
translate japanese route_ichiru_6_797a15c8:

    ichiru "That was fast! Don't you think so Tofu?"
    ichiru "それは速かった！あなたはTofuだと思いませんか？"

# game/script.rpy:6725
translate japanese route_ichiru_6_a6aa4eef:

    toshu "Y-yeah…"
    toshu "Y-yeah …"

# game/script.rpy:6726
translate japanese route_ichiru_6_62a99bb2:

    ichiru "I want more…"
    ichiru "私がもっと欲しい…"

# game/script.rpy:6727
translate japanese route_ichiru_6_206bc507:

    ichiru "I want to go inside you again!"
    ichiru "私は再びあなたの中に行きたい！"

# game/script.rpy:6728
translate japanese route_ichiru_6_b8c2a7ff:

    toshu "W-wha…?"
    toshu "イン義和…？"

# game/script.rpy:6730
translate japanese route_ichiru_6_3cdb01bd:

    toshu "Mhnnhh!!!"
    toshu "Mhnnhh !!!"

# game/script.rpy:6731
translate japanese route_ichiru_6_77d9abff:

    toshu_t "The sensation is stirring me up inside!"
    toshu_t "感覚が私の中を揺らしている！"

# game/script.rpy:6732
translate japanese route_ichiru_6_fe097c19:

    toshu_t "Each of Ichiru's vigorous thrusts hit a pleasurable spot."
    toshu_t "イチールの激しい突撃のそれぞれが楽しい場所を襲った。"

# game/script.rpy:6734
translate japanese route_ichiru_6_98b59e73:

    ichiru "Hmnnnhh…! This feels awesome, Tofu…!"
    ichiru "Hmnnnhh …！これは素晴らしい、Tofuを感じる…！"

# game/script.rpy:6735
translate japanese route_ichiru_6_d53d8991:

    toshu "Hnn!"
    toshu "Hnn！"

# game/script.rpy:6736
translate japanese route_ichiru_6_9919c69e:

    ichiru "I-I'm gonna cum!"
    ichiru "私は兼ねるつもりです！"

# game/script.rpy:6737
translate japanese route_ichiru_6_58f0e434:

    toshu "M-me too…!"
    toshu "M-meも…！"

# game/script.rpy:6739
translate japanese route_ichiru_6_95100ce3:

    toshu "Haaa!"
    toshu "Haaa！"

# game/script.rpy:6740
translate japanese route_ichiru_6_9fde4cff:

    toshu_t "I can feel Ichiru's cum overflowing my entry…"
    toshu_t "イチールのザーメンが私のエントリーをあふれさせているのを感じることができます…"

# game/script.rpy:6741
translate japanese route_ichiru_6_d3b470fe:

    ichiru "Ahh!!"
    ichiru "ああ！"

# game/script.rpy:6743
translate japanese route_ichiru_6_4caa77f5:

    ichiru "Ahh… Toshu's really sexy!!"
    ichiru "ああ… Toshuは本当にセクシーだ！"

# game/script.rpy:6744
translate japanese route_ichiru_6_e274ab3a:

    toshu_t "I feel almost numb from too much pleasure."
    toshu_t "私はあまりにも多くの喜びから麻痺する気がします。"

# game/script.rpy:6745
translate japanese route_ichiru_6_e538a66d:

    toshu "It's so hot inside… Ichiru… t-take it out…"
    toshu "それは内部がとても暑いです…イチール…それを取る…"

# game/script.rpy:6746
translate japanese route_ichiru_6_dd20c5e5:

    ichiru "Alrighty~"
    ichiru "よろしく〜"

# game/script.rpy:6748
translate japanese route_ichiru_6_b61aebe0:

    ichiru "Yeah…!"
    ichiru "うん…！"

# game/script.rpy:6749
translate japanese route_ichiru_6_a37b4df9:

    ichiru "Just look at all these cum…!"
    ichiru "これらのすべてのcumを見て…！"

# game/script.rpy:6750
translate japanese route_ichiru_6_32892abb:

    toshu "T-this is the first time I did something like this…"
    toshu "T-これが私がこのようなことをしたのは初めてです…"

# game/script.rpy:6751
translate japanese route_ichiru_6_95b4673e:

    ichiru "Me too, Tofu!"
    ichiru "私も、Tofu！"

# game/script.rpy:6752
translate japanese route_ichiru_6_d6fde322:

    ichiru "I'm glad you're my first!"
    ichiru "あなたが私の最初のことをうれしく思います！"

# game/script.rpy:6754
translate japanese route_ichiru_6_b4411fcc:

    toshu_t "I can't believe I did such thing with someone…"
    toshu_t "私は誰かとそのようなことをしたとは信じられません…"

# game/script.rpy:6755
translate japanese route_ichiru_6_922b8907:

    toshu_t "But the fact that it's Ichiru, I didn't mind at all."
    toshu_t "しかし、それがIchiruということは、私はまったく気にしませんでした。"

# game/script.rpy:6756
translate japanese route_ichiru_6_250dc61e:

    toshu_t "I was left speechless…"
    toshu_t "私は言葉が残っていた…"

# game/script.rpy:6757
translate japanese route_ichiru_6_61d19849:

    toshu_t "It just felt really good…"
    toshu_t "それはちょうど本当に良い感じ…"

# game/script.rpy:6758
translate japanese route_ichiru_6_4b2d30e7:

    toshu_t "No words came out from my mouth."
    toshu_t "私の口から言葉は出てこなかった。"

# game/script.rpy:6759
translate japanese route_ichiru_6_524efd75:

    toshu_t "Feeling ecstatic from my first experience… Ichiru suddenly broke the silence…"
    toshu_t "私の最初の経験から恍惚を感じる…一瞬、突然沈黙を壊した…"

# game/script.rpy:6760
translate japanese route_ichiru_6_118763b1:

    ichiru "Toshu…"
    ichiru "Toshu…"

# game/script.rpy:6761
translate japanese route_ichiru_6_fffe180a:

    ichiru "Can you hug me…?"
    ichiru "あなたは私を抱きしめることができますか？"

# game/script.rpy:6762
translate japanese route_ichiru_6_f8d0784d:

    toshu_t "Hearing those words from Ichiru…"
    toshu_t "Ichiruからそれらの言葉を聞く…"

# game/script.rpy:6763
translate japanese route_ichiru_6_70725164:

    toshu_t "It felt sincere…"
    toshu_t "それは誠実に感じた…"

# game/script.rpy:6764
translate japanese route_ichiru_6_83c9cff7:

    toshu_t "And at the same time… depressing."
    toshu_t "そして同時に…うつ病。"

# game/script.rpy:6765
translate japanese route_ichiru_6_400653dc:

    toshu_t "My short moment of happiness suddenly crushed down when Ichiru left those words…"
    toshu_t "イチルがその言葉を去ったとき、私の短い幸福の瞬間が突然打ち砕かれました…"

# game/script.rpy:6770
translate japanese route_ichiru_6_118763b1_1:

    ichiru "Toshu…"
    ichiru "Toshu…"

# game/script.rpy:6772
translate japanese route_ichiru_6_9197d109_1:

    toshu "A-are you okay Ichiru?"
    toshu "A-are you okay Ichiru?"

# game/script.rpy:6773
translate japanese route_ichiru_6_41168484:

    ichiru "To be honest Tofu… I'm not…"
    ichiru "正直言ってTofu…私は…"

# game/script.rpy:6775
translate japanese route_ichiru_6_7e8863bb:

    ichiru "I like you…"
    ichiru "君の事が好きです…"

# game/script.rpy:6776
translate japanese route_ichiru_6_bb158072:

    ichiru "But I am not allowed to…"
    ichiru "しかし、私は許可されていません…"

# game/script.rpy:6778
translate japanese route_ichiru_6_7b82ad8a:

    toshu "Ahh?"
    toshu "ああ？"

# game/script.rpy:6780
translate japanese route_ichiru_6_4b2eda27:

    ichiru "They… my family… have already agreed with my transfer…"
    ichiru "彼ら…私の家族は…私の移籍にすでに同意している…"

# game/script.rpy:6781
translate japanese route_ichiru_6_f26eff49:

    ichiru "And… I am going to marry Tomoka."
    ichiru "そして…私はトモカと結婚するつもりです。"

# game/script.rpy:6783
translate japanese route_ichiru_6_cef0263c:

    ichiru "Sniff sniff…"
    ichiru "スニッフスニッフ…"

# game/script.rpy:6785
translate japanese route_ichiru_6_3105c0f7:

    toshu "I-Ichiru…"
    toshu "I-Ichiru…"

# game/script.rpy:6787
translate japanese route_ichiru_6_ccd1ec0d:

    ichiru "IT'S JUST NOT FAIR!"
    ichiru "それは公正ではない！"

# game/script.rpy:6788
translate japanese route_ichiru_6_91e0da17:

    ichiru "Why can't I make my own decisions for myself?!"
    ichiru "なぜ自分で自分の意思決定をすることができないのですか？"

# game/script.rpy:6789
translate japanese route_ichiru_6_d580d0c0:

    ichiru "I've always followed what dad wanted for me…!"
    ichiru "私はいつも私のために父が望んでいたものに従ってきました…！"

# game/script.rpy:6790
translate japanese route_ichiru_6_20088562:

    ichiru "The more I follow him, the higher his expectations get!"
    ichiru "私が彼に続くほど、彼の期待は高くなる！"

# game/script.rpy:6791
translate japanese route_ichiru_6_86aa908d:

    ichiru "Now he's asking me to marry someone I don't like…!"
    ichiru "今、彼は私に好きでない人と結婚するように求めています…！"

# game/script.rpy:6793
translate japanese route_ichiru_6_9716610a:

    ichiru "I… I want to be with someone who I really love…"
    ichiru "私は…私が本当に好きな人と一緒にいたい"

# game/script.rpy:6795
translate japanese route_ichiru_6_9c3d0517:

    toshu "C-can't you talk to this about your parents?"
    toshu "あなたの両親についてこれについて話すことはできないのですか？"

# game/script.rpy:6796
translate japanese route_ichiru_6_2d584f2e:

    toshu "It's your life… you should have the right to decide for yourself."
    toshu "それはあなたの人生です…あなたは自分のために決定する権利を持っているべきです。"

# game/script.rpy:6798
translate japanese route_ichiru_6_8ab56c14:

    ichiru "I tried… Tofu…"
    ichiru "私は試みた…Tofu…"

# game/script.rpy:6800
translate japanese route_ichiru_6_ea48b48e:

    ichiru "Unfortunately I can't do anything about it."
    ichiru "残念ながら私はそれについて何もできません。"

# game/script.rpy:6802
translate japanese route_ichiru_6_d3739030:

    ichiru "My dad is indebted to Tomoka's Father…"
    ichiru "私のお父さんはTomokaの父親に借りている…"

# game/script.rpy:6803
translate japanese route_ichiru_6_16a373f6:

    ichiru "My dad has to follow everything that man says…"
    ichiru "私のお父さんは、男が言うことすべてを追わなければならない…"

# game/script.rpy:6804
translate japanese route_ichiru_6_489be2d2:

    ichiru "And what he says is… for her daughter to marry me."
    ichiru "彼の言うことは…娘が私と結婚することです。"

# game/script.rpy:6805
translate japanese route_ichiru_6_b465a265:

    ichiru "Tomoka and I had been friends since we were little…"
    ichiru "トモカと私は友人でした。"

# game/script.rpy:6806
translate japanese route_ichiru_6_6497746f:

    ichiru "I always stood up for her."
    ichiru "私はいつも彼女のために立ち上がった。"

# game/script.rpy:6807
translate japanese route_ichiru_6_93cdabf1:

    ichiru "That is probably why she has feelings for me…"
    ichiru "それはおそらく彼女が私の気持ちを持っている理由です…"

# game/script.rpy:6808
translate japanese route_ichiru_6_9999fd6e:

    ichiru "But Tomoka changed… the richer her family became, the more blinded she got…"
    ichiru "しかし、トモカは変わった。家族が豊かになればなるほど、彼女はもっと盲目になった。"

# game/script.rpy:6809
translate japanese route_ichiru_6_b206596e:

    ichiru "She wasn't mean or conceited at all when we were young."
    ichiru "私たちが若い時、彼女は意地悪でも幻想的でもありませんでした。"

# game/script.rpy:6810
translate japanese route_ichiru_6_cd2e57b3:

    ichiru "And because of that, I started to dislike her."
    ichiru "そしてそのために、私は彼女を嫌うようになりました。"

# game/script.rpy:6811
translate japanese route_ichiru_6_23d5f609:

    ichiru "Tomoka's father will do everything his beloved daughter says."
    ichiru "トモカの父親は、彼の最愛の娘が言うすべてのことをするでしょう。"

# game/script.rpy:6812
translate japanese route_ichiru_6_01e48c60:

    ichiru "And will give anything she asks for…"
    ichiru "彼女が求めていることは何でも与える…"

# game/script.rpy:6814
translate japanese route_ichiru_6_730fe9f2:

    ichiru "Including… me."
    ichiru "私を含む…"

# game/script.rpy:6816
translate japanese route_ichiru_6_f7cedad6:

    toshu "That is terrible Ichiru…"
    toshu "それは恐ろしいイチルです…"

# game/script.rpy:6818
translate japanese route_ichiru_6_ae81368e:

    ichiru "I know!"
    ichiru "知っている！"

# game/script.rpy:6819
translate japanese route_ichiru_6_c0818bcb:

    ichiru "That's why I am so frustrated!"
    ichiru "だから私はとてもイライラしている！"

# game/script.rpy:6820
translate japanese route_ichiru_6_66b2de8e:

    ichiru "I can't control my life!"
    ichiru "私は私の人生を制御することはできません！"

# game/script.rpy:6822
translate japanese route_ichiru_6_55a4394f:

    ichiru "What should I do, Toshu?!"
    ichiru "私はどうすればいいですか？"

# game/script.rpy:6824
translate japanese route_ichiru_6_db29de63:

    toshu "Ichiru! Don't let it make you fall!"
    toshu "Ichiru！それはあなたが落ちるようにしないでください！"

# game/script.rpy:6826
translate japanese route_ichiru_6_9a042d38:

    toshu "No matter what, I will never leave by your side!"
    toshu "どんなことでも、私はあなたのそばを離れません！"

# game/script.rpy:6828
translate japanese route_ichiru_6_dd4bc8a7:

    toshu "I want to make you happy the way you always do for me!"
    toshu "私はあなたがいつも私のためにやっているようにあなたを幸せにしたい！"

# game/script.rpy:6830
translate japanese route_ichiru_6_0398a9b3:

    toshu "I will do whatever it takes to see that very cheerful smile of yours again!"
    toshu "私はあなたのものの非常に陽気な笑顔を見るために必要なものは何でもします！"

# game/script.rpy:6832
translate japanese route_ichiru_6_1453f31e:

    ichiru "Promise me… please stay by my side always…"
    ichiru "私を約束してください…常に私の側にとどまってください…"

# game/script.rpy:6834
translate japanese route_ichiru_6_58890840:

    toshu "I promise!"
    toshu "約束します！"

# game/script.rpy:6836
translate japanese route_ichiru_6_8159b481:

    toshu_t "I knew from the very beginning that Ichiru was always keeping something from us…"
    toshu_t "私は最初からイチルがいつも何かを私たちから守っていることを知っていました…"

# game/script.rpy:6837
translate japanese route_ichiru_6_e346bb3a:

    toshu_t "But I never thought he burdened all those problems to himself all this time."
    toshu_t "しかし、私は彼がこのすべての問題を今度は自分自身に負担させたとは思っていませんでした。"

# game/script.rpy:6838
translate japanese route_ichiru_6_e130a1eb:

    toshu_t "I can't believe Ichiru was carrying all that weight on his shoulders…"
    toshu_t "Ichiruが肩にその重さを持っているとは信じられない…"

# game/script.rpy:6839
translate japanese route_ichiru_6_ac5e2ca7:

    toshu_t "Ichiru… never felt sincere love from others…"
    toshu_t "イチル…他人からの誠実な愛を感じなかった…"

# game/script.rpy:6840
translate japanese route_ichiru_6_94755a07:

    toshu_t "It must be hard for him…"
    toshu_t "彼にとっては難しいことです…"

# game/script.rpy:6841
translate japanese route_ichiru_6_4c12538f:

    toshu_t "That feeling of… desperation…"
    toshu_t "その絶望感…"

# game/script.rpy:6842
translate japanese route_ichiru_6_8b0a0791:

    toshu_t "I can't imagine how much he had endured already…"
    toshu_t "彼がどれくらい耐えていたのか想像もできません…"

# game/script.rpy:6850
translate japanese route_ichiru_6_0bc84817:

    ichiru "Sorry for bursting out like that all of a sudden…"
    ichiru "突然そのように爆発して申し訳ありません…"

# game/script.rpy:6852
translate japanese route_ichiru_6_0d0ea866:

    toshu "It's alright, I totally understand!"
    toshu "それは大丈夫、私は完全に理解する！"

# game/script.rpy:6853
translate japanese route_ichiru_6_9098a925:

    toshu "I'm glad you told me what was bothering you."
    toshu "あなたを悩ましていたことを教えてくれてうれしいです。"

# game/script.rpy:6855
translate japanese route_ichiru_6_b7a5fe70:

    toshu "It made me feel like I am a person you trust."
    toshu "それは私があなたが信頼する人であるように感じました。"

# game/script.rpy:6856
translate japanese route_ichiru_6_7c778906:

    toshu "Don't cry anymore, okay?"
    toshu "もう泣かないでよ、大丈夫？"

# game/script.rpy:6858
translate japanese route_ichiru_6_a0fb599e:

    ichiru "Okay, Tofu!"
    ichiru "さて、Tofu！"

# game/script.rpy:6860
translate japanese route_ichiru_6_2bb701eb:

    ichiru "What should we do now?"
    ichiru "何をするべきだろう？"

# game/script.rpy:6862
translate japanese route_ichiru_6_b036ea98:

    toshu "I got video games."
    toshu "私はビデオゲームを持っています。"

# game/script.rpy:6864
translate japanese route_ichiru_6_ef759a25:

    ichiru "Cool! I wanna play with you, Tofu!"
    ichiru "クール！私はあなたと一緒にプレーしたい、Tofu！"

# game/script.rpy:6866
translate japanese route_ichiru_6_3dc39a5d:

    toshu "I don't think you can beat me though."
    toshu "私はあなたが私を打つことができるとは思わない。"

# game/script.rpy:6868
translate japanese route_ichiru_6_293ae555:

    ichiru "Oh really, you're telling that to me?"
    ichiru "ああ、あなたは私にそれを伝えていますか？"

# game/script.rpy:6870
translate japanese route_ichiru_6_e83abb50:

    ichiru "TO ICHIRU THE ALL-AROUND GENIUS?!"
    ichiru "すべての近所のジェニウスをICHIRUに！"

# game/script.rpy:6872
translate japanese route_ichiru_6_e72f1428:

    toshu "Then bring it on!"
    toshu "その後、それをもたらす！"

# game/script.rpy:6874
translate japanese route_ichiru_6_7b81310b:

    toshu_t "With Ichiru finally calming down, we spent the rest of the day playing all my video games!"
    toshu_t "ついにIchiruが落ち着いて、私たちは今日の残りの部分をすべて私のビデオゲームで過ごしました！"

# game/script.rpy:6875
translate japanese route_ichiru_6_63dd2b15:

    toshu_t "I usually play alone… that's why I never knew that playing with someone else is twice as great!"
    toshu_t "私はたいてい1人で遊んでいます…だからこそ、他の人と遊ぶことが2倍になることは決して知りませんでした！"

# game/script.rpy:6876
translate japanese route_ichiru_6_060465db:

    toshu_t "Especially if it's Ichiru!"
    toshu_t "特にイチルなら！"

# game/script.rpy:6877
translate japanese route_ichiru_6_acc25f48:

    toshu_t "We really had a fun day together!"
    toshu_t "一緒に楽しい一日を過ごしました！"

# game/script.rpy:6878
translate japanese route_ichiru_6_e6369912:

    toshu_t "I'm glad I was able to release Ichiru's frustration and replace it with happiness!"
    toshu_t "イチールの欲求不満を解き放ち、それを幸せに置き換えることができてうれしいです！"

# game/script.rpy:6879
translate japanese route_ichiru_6_af3b4da4:

    toshu_t "This is by far… my best birthday ever!!!"
    toshu_t "これはずっと…私の最高の誕生日です！"

# game/script.rpy:6887
translate japanese route_ichiru_6_fac83362:

    toshu "I warned you."
    toshu "私はあなたに警告した。"

# game/script.rpy:6888
translate japanese route_ichiru_6_2a01012b:

    ichiru "NO WAY…"
    ichiru "とんでもない…"

# game/script.rpy:6890
translate japanese route_ichiru_6_da5dd5ff:

    ichiru "I don't accept this!"
    ichiru "私はこれを受け入れない！"

# game/script.rpy:6892
translate japanese route_ichiru_6_5a5b1c43:

    ichiru "How can I be beaten… Twenty times…"
    ichiru "どのように私は殴られることができます… 20回…"

# game/script.rpy:6894
translate japanese route_ichiru_6_90aa55d9:

    ichiru "…IN A ROW?!"
    ichiru "… IN AOW ?!"

# game/script.rpy:6895
translate japanese route_ichiru_6_ad870671:

    toshu "You may be a genius. But gaming is totally a different world!"
    toshu "あなたは天才かもしれません。しかし、ゲームはまったく別の世界です！"

# game/script.rpy:6897
translate japanese route_ichiru_6_39f9cfe3:

    ichiru "For the first time, I'm accepting defeat. You totally pawned me."
    ichiru "初めて、私は敗北を受け入れています。あなたは私を完全に傷つけました。"

# game/script.rpy:6899
translate japanese route_ichiru_6_eaa15b88:

    toshu "Hahaha!"
    toshu "ハハハ！"

# game/script.rpy:6901
translate japanese route_ichiru_6_b48adab4:

    ichiru "Haha!"
    ichiru "ハハ！"

# game/script.rpy:6905
translate japanese route_ichiru_6_a2c5bd2d_1:

    toshu "Hello?"
    toshu "こんにちは？"

# game/script.rpy:6906
translate japanese route_ichiru_6_23ae1b0f:

    random_c "Hello, Toshu!"
    random_c "Hello, Toshu!"

# game/script.rpy:6908
translate japanese route_ichiru_6_002d3e8c:

    toshu "Captain!"
    toshu "キャプテン！"

# game/script.rpy:6909
translate japanese route_ichiru_6_119fee9c:

    masaru_c "I just wanted to greet you a very HAPPY BIRTHDAY before it's too late!"
    masaru_c "私はちょうどそれが遅すぎる前に非常に幸せな誕生日を迎えたいと思った！"

# game/script.rpy:6910
translate japanese route_ichiru_6_491b59ae:

    masaru_c "I see you're using our gift! I hope you liked it!"
    masaru_c "私はあなたが私たちの贈り物を使用して参照してください私はあなたがそれを好きであることを望む"

# game/script.rpy:6912
translate japanese route_ichiru_6_b0b20ffb:

    toshu "Yes, I do, Captain! Thank you so much!"
    toshu "はい、私は、キャプテン！どうもありがとうございます！"

# game/script.rpy:6913
translate japanese route_ichiru_6_cc8be3b7:

    masaru_c "I'm very sorry, I couldn't go... things here are really hectic..."
    masaru_c "私は非常に残念です、私は行くことができなかった…ここに物事は本当に忙しいです…"

# game/script.rpy:6915
translate japanese route_ichiru_6_b39d5497:

    toshu "It's okay Captain. I understand!"
    toshu "大丈夫です。キャプテン。わかりました！"

# game/script.rpy:6916
translate japanese route_ichiru_6_56dea1b0:

    masaru_c "That's a relief..."
    masaru_c "それは救済だ…"

# game/script.rpy:6917
translate japanese route_ichiru_6_753c3ddb:

    masaru_c "I thought you were mad."
    masaru_c "私はあなたが怒っていると思った。"

# game/script.rpy:6918
translate japanese route_ichiru_6_be682dd8:

    masaru_c "Anyway, I have to go. My brother's crying again! Dad's really making a mess...!"
    masaru_c "とにかく、私は行かなければならない。私の弟はもう一度泣いている！お父さんは本当に混乱している…！"

# game/script.rpy:6920
translate japanese route_ichiru_6_91c1ca96:

    toshu "Okay, Captain! Please take care there!"
    toshu "さて、キャプテン！そこに注意してください！"

# game/script.rpy:6923
translate japanese route_ichiru_6_9fbb047a:

    ichiru "I'm glad he was able to steal time to call you! I thought he's not gonna make it!"
    ichiru "彼はあなたに電話する時間を盗むことができてうれしいです！私は彼がそれを作るつもりはないと思った！"

# game/script.rpy:6925
translate japanese route_ichiru_6_4630106e:

    toshu "Y-yeah… I feel kinda guilty…"
    toshu "名前：Y-ええ…私はちょっと有罪を感じる…"

# game/script.rpy:6927
translate japanese route_ichiru_6_60ebdc34:

    ichiru "Don't be! Just appreciate it! We all want you to have a great time!"
    ichiru "ありません！ただそれを感謝します！私たちは皆、素晴らしい時間を過ごしたいです！"

# game/script.rpy:6929
translate japanese route_ichiru_6_a31a81bb:

    toshu "Thanks to you, Ichiru, my birthday was really a BLAST!"
    toshu "あなたのおかげで、Ichiru、私の誕生日は本当にBLASTでした！"

# game/script.rpy:6931
translate japanese route_ichiru_6_f67d2772:

    ichiru "Well, I should be heading home now… it's getting late."
    ichiru "さて、私は今帰るべきです…遅くなっています。"

# game/script.rpy:6932
translate japanese route_ichiru_6_f40b4ccd:

    ichiru "They must be panicking there that I went missing again."
    ichiru "彼らは私が再び行方不明になったときにパニックになっているはずです。"

# game/script.rpy:6934
translate japanese route_ichiru_6_608a6150:

    toshu "Eh? You didn't tell them you were leaving?"
    toshu "え？あなたは彼らが離れると言っていませんでしたか？"

# game/script.rpy:6936
translate japanese route_ichiru_6_9c00077f:

    ichiru "I told youuuu… I just escaped!"
    ichiru "私はyouuuuに言った…私はただ逃げた！"

# game/script.rpy:6938
translate japanese route_ichiru_6_10cd8c09:

    toshu "Ohh… then they must be really worried."
    toshu "ああ…本当に心配しなければならない。"

# game/script.rpy:6940
translate japanese route_ichiru_6_3a99aa27:

    ichiru "Yeah… probably…"
    ichiru "うん…おそらく…"

# game/script.rpy:6941
translate japanese route_ichiru_6_138455ed:

    ichiru "I'm like a caged bird there."
    ichiru "私はそこに檻に入れられた鳥のようだ。"

# game/script.rpy:6942
translate japanese route_ichiru_6_ff6d125f:

    ichiru "Every move I make is under surveillance."
    ichiru "私が作るすべての動きは監視下にあります。"

# game/script.rpy:6943
translate japanese route_ichiru_6_78f41164:

    ichiru "Tomoka probably released a search team right about now."
    ichiru "トモカはおそらく今すぐ調査チームをリリースしました。"

# game/script.rpy:6945
translate japanese route_ichiru_6_58275680:

    toshu "W-wahh! That's terrible! I'm so sorry caused you a lot of trouble!"
    toshu "W-ワ！それはひどい！大変申し訳ございませんが、多くのトラブルが発生しました。"

# game/script.rpy:6947
translate japanese route_ichiru_6_ba2d588a:

    ichiru "Pfff! It's not your fault."
    ichiru "Pfff！これはあなたの責任ではないです。"

# game/script.rpy:6949
translate japanese route_ichiru_6_5c2defe4:

    ichiru "Don't worry I'm already used to this."
    ichiru "私はすでにこれに慣れていることを心配しないでください。"

# game/script.rpy:6950
translate japanese route_ichiru_6_fab8f669:

    ichiru "Well! See you again!"
    ichiru "まあ！またね！"

# game/script.rpy:6951
translate japanese route_ichiru_6_766b01b9:

    ichiru "I will pass by your house every time I have the chance!"
    ichiru "私はチャンスがあるたびにあなたの家を通り過ぎます！"

# game/script.rpy:6953
translate japanese route_ichiru_6_839092f8:

    ichiru "Goodbye Toshu!"
    ichiru "Goodbye Toshu!"

# game/script.rpy:6956
translate japanese route_ichiru_6_fc707a30:

    toshu "T-Toshu!?"
    toshu "T-Toshu!?"

# game/script.rpy:6958
translate japanese route_ichiru_6_729dad95:

    toshu_t "Ichiru finally called me by my name!"
    toshu_t "Ichiruはついに私の名前で私を呼びました！"

# game/script.rpy:6959
translate japanese route_ichiru_6_30da3e9d:

    toshu_t "It felt really special!"
    toshu_t "それは本当に特別な感じでした！"

# game/script.rpy:6960
translate japanese route_ichiru_6_8ef2090b:

    toshu_t "I feel like our relationship is much more than what I expected!"
    toshu_t "私の関係は私が期待していたものよりはるかに多いように感じます！"
