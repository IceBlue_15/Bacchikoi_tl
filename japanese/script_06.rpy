# game/script.rpy:2165
translate japanese choice_6_end_558204dd:

    ichiru "Yoshaaa!!~ My first field trip with Tofu!!"
    ichiru "Yoshaaa !!〜Tofuとの初めてのフィールドトリップ!!"

# game/script.rpy:2167
translate japanese choice_6_end_d1082bae:

    masaru "Don't get too excited now. We're gonna go there for practice, not for fun and games."
    masaru "今はあまり興奮しないでください。私たちは練習のためにそこに行くつもりです。楽しみとゲームのためではありません。"

# game/script.rpy:2169
translate japanese choice_6_end_3f823733:

    ichiru "Wow Masaru, since when have you become such a killjoy?"
    ichiru "ワウ・マサル、いつこんな殺し屋になったの？"

# game/script.rpy:2171
translate japanese choice_6_end_f6f275e8:

    masaru "Ah, speaking of Toshu, I haven't seen him today."
    masaru "ああ、Toshuの話、今日は彼を見たことがない。"

# game/script.rpy:2173
translate japanese choice_6_end_41e54f5d:

    ichiru "Yeah me too."
    ichiru "うん、私も。"

# game/script.rpy:2175
translate japanese choice_6_end_db8e4b3e:

    genji "Is everyone here yet? Let's see…"
    genji "誰もまだここにいるの？どれどれ…"

# game/script.rpy:2177
translate japanese choice_6_end_5e4e82bd:

    genji "Where is Toshu?!"
    genji "Toshuはどこですか？"

# game/script.rpy:2180
translate japanese choice_6_end_f30dc2c4:

    masaru "I haven't seen him yet."
    masaru "私はまだ彼を見ていない。"

# game/script.rpy:2182
translate japanese choice_6_end_654d1e55:

    ichiru "I didn't see him either. He was bragging all day yesterday that he was so excited for the field trip."
    ichiru "私も彼に会いませんでした。彼は昨日、野外旅行のためにとても興奮していたことを自慢していました。"

# game/script.rpy:2184
translate japanese choice_6_end_cf0c7d09:

    ichiru "Maybe he's still asleep?"
    ichiru "多分彼はまだ眠っているのだろうか？"

# game/script.rpy:2186
translate japanese choice_6_end_f829f0fc:

    masaru "I hope not."
    masaru "私は望んでいない。"

# game/script.rpy:2188
translate japanese choice_6_end_b30341d2:

    genji "If he doesn't come here soon, we will leave witho--"
    genji "もしすぐにここに来なければ、"

# game/script.rpy:2190
translate japanese choice_6_end_85f41767:

    toshu "I'M HERE COACH!! I'M SO SORRY I'M LATE!! I WOKE UP LATE!!"
    toshu "私はここコーチです！私はそんなに哀しいよ、私は遅刻だ!!遅く起きました！！"

# game/script.rpy:2192
translate japanese choice_6_end_89f6649f:

    ichiru "Wow, I was right. He did oversleep."
    ichiru "うわー、私は正しかった。彼は過ごした。"

# game/script.rpy:2194
translate japanese choice_6_end_7b20a994:

    masaru "Poor thing, couldn't contain the excitement."
    masaru "貧しいもの、興奮を含むことはできませんでした。"

# game/script.rpy:2196
translate japanese choice_6_end_94c2d645:

    genji "TOO LATE Kanada!! We've already set up and we are gonna go without you."
    genji "あまりにも遅いカナダ！我々はすでにセットアップしており、あなたなしで行くつもりです。"

# game/script.rpy:2198
translate japanese choice_6_end_3c605cbb:

    genji "HAHAHA!!"
    genji "ハハハ!!"

# game/script.rpy:2200
translate japanese choice_6_end_dbb34649:

    ichiru "There he goes again…"
    ichiru "そこに彼は再び行く…"

# game/script.rpy:2202
translate japanese choice_6_end_c6b5c3d3:

    toshu "NOOO!!! Nooo!! Please Coach let me join!!! I wanna go to the field trip!!"
    toshu "NOOO !!!いいえ！コーチは私に参加させてください！フィールドトリップに行きたい!!"

# game/script.rpy:2204
translate japanese choice_6_end_f9e4aa52:

    toshu "*sniff*"
    toshu "*スニフ*"

# game/script.rpy:2206
translate japanese choice_6_end_0935f54d:

    masaru "COACH!! DON'T BULLY TOSHU!!!"
    masaru "コーチ！！ TOSHUを怒らせないでください!!!"

# game/script.rpy:2208
translate japanese choice_6_end_297ca8b1:

    genji "HAHAHAH!! You silly boy! Of course we're not going to leave you behind."
    genji "ハハハ!!あなたは愚かな男の子！もちろん、私たちはあなたを後に残すつもりはありません。"

# game/script.rpy:2210
translate japanese choice_6_end_c756e1b1:

    genji "You're lucky that our Captain here is such an over-protective man!"
    genji "あなたは私たちのキャプテンがそんなに保護的な男であることは幸いです！"

# game/script.rpy:2212
translate japanese choice_6_end_236635b3:

    toshu "*sniff sniff*"
    toshu "*スニッフスニフ*"

# game/script.rpy:2213
translate japanese choice_6_end_96008c68:

    genji "Such a crybaby!"
    genji "そんな泣き虫！"

# game/script.rpy:2215
translate japanese choice_6_end_86a86267:

    ichiru "There, there, Tofu…"
    ichiru "そこに、そこに、Tofu…"

# game/script.rpy:2217
translate japanese choice_6_end_734c9991:

    ichiru "Just ignore that GAYJI for being such a troll!"
    ichiru "そのようなトロルであるために、そのGAYJIを無視してください！"

# game/script.rpy:2219
translate japanese choice_6_end_36f00a27:

    genji "What did you say you little brat?!"
    genji "何があなたの小さなかわい子と言いましたか？"

# game/script.rpy:2221
translate japanese choice_6_end_9c5362be:

    ichiru "I said GAYJI, GAYJIII!!"
    ichiru "私はゲイジ、ゲイだと言った!!"

# game/script.rpy:2223
translate japanese choice_6_end_b9d64579:

    genji "WHY YOU LITTLE--!!"
    genji "あなたはなぜほら〜!!"

# game/script.rpy:2226
translate japanese choice_6_end_8d005fbb:

    masaru "See, Toshu? You'll get used to Coach eventually!"
    masaru "Toshu？あなたは最終的にコーチに慣れます！"

# game/script.rpy:2228
translate japanese choice_6_end_4b178606:

    genji "Ehem… let's get going, we have a long day ahead!"
    genji "エヘム…行きましょう、私たちは長い一日を前にしています！"

# game/script.rpy:2230
translate japanese choice_6_end_464f58d8:

    toshu "Yes, Coach!"
    toshu "はい、コーチ！"

# game/script.rpy:2232
translate japanese choice_6_end_9f5f5906:

    genji "I hope you're ready for a lengthy road trip!"
    genji "あなたは長い旅行の準備ができていることを願っています！"

# game/script.rpy:2234
translate japanese choice_6_end_d9f5fae9:

    toshu_t "Coach said it's gonna be a long way to reach the place… I wonder who should I seat beside to…?"
    toshu_t "コーチは、場所に行くのは遠い道のりだろうと言った…私は誰の隣に座るべきだろう…？"

# game/script.rpy:2254
translate japanese choice_7_A_f3c4cfcf:

    # toshu "Uhm, Coach? Can I sit beside you?"
    toshu "あの…コーチ、隣に座ってもいいですか？"

# game/script.rpy:2256
translate japanese choice_7_A_faef001e:

    # genji "Ohh~ Why sure! I didn't expect you'd tell me that!"
    genji "おお〜もちろんだ！ そんなこと言ってくれるとは思わなかった！"

# game/script.rpy:2258
translate japanese choice_7_A_aa385858:

    # ichiru "NO!!! TOFU!"
    ichiru "やめとけ！！！ TOFU！"

# game/script.rpy:2260
translate japanese choice_7_A_6cf3982a:

    ichiru "GAYJI is dangerous! He will just bully and annoy you to death!"
    ichiru "ゲイジは危険です！彼はちょうどあなたをいじめにし、あなたを悩ますでしょう！"

# game/script.rpy:2262
translate japanese choice_7_A_1adce339:

    genji "Now I'm the bully?"
    genji "今私はいじめっ子ですか？"

# game/script.rpy:2263
translate japanese choice_7_A_7bccd5d2:

    # ichiru "Just leave him be!"
    ichiru "Toshuから離れろ！"

# game/script.rpy:2265
translate japanese choice_7_A_3a206f2d:

    # ichiru "Besides, it is a three-seated bus!"
    ichiru "あと、このバスは３列シートだから！"

# game/script.rpy:2266
translate japanese choice_7_A_71903e0a:

    # ichiru "Me and Masaru already reserved you a seat!"
    ichiru "俺とMasaruがお前の席を取っといてやったよ！"

# game/script.rpy:2268
translate japanese choice_7_A_5aa8828a:

    # genji "OI!! Kanada wants to sit beside me, not you guys!"
    genji "おい！！ Kanadaはお前らじゃなくて俺の隣に座りたいんだよ！"

# game/script.rpy:2270
translate japanese choice_7_A_58cf498a:

    # ichiru "In your dreams, GAYJI."
    ichiru "勝手に妄想してろ、GAYJI。"

# game/script.rpy:2272
translate japanese choice_7_A_f6f41a2f:

    # ichiru "Come on, Tofu, before GAYJI's stupidity infects you!"
    ichiru "来いよ、Tofu。GAYJIのアホがうつっちまうぜ！"

# game/script.rpy:2274
translate japanese choice_7_A_8682543a:

    # toshu "Ahh… okay…"
    toshu "うん…わかった…"

# game/script.rpy:2275
translate japanese choice_7_A_14f145d3:

    # toshu "I'm sorry, Coach!"
    toshu "すいません、コーチ！"

# game/script.rpy:2277
translate japanese choice_7_A_4c84001c:

    # toshu "I didn't know they already reserved me a seat."
    toshu "既に席が取ってあることを知りませんでした。"

# game/script.rpy:2279
translate japanese choice_7_A_90b6d5d5:

    # ichiru "C'mon, let's go!"
    ichiru "さあ、行こうぜ！"

# game/script.rpy:2282
translate japanese choice_7_A_67afb707:

    # toshu "I wonder where will we go today?"
    toshu "これからどこへ行くんだろう？"

# game/script.rpy:2284
translate japanese choice_7_A_e8f6729f:

    # masaru "Coach said he will take us somewhere we can train."
    masaru "練習ができるような場所へ行くってコーチが言ってたよ。"

# game/script.rpy:2286
translate japanese choice_7_A_d58c259c:

    # ichiru "Why go far? We have our own place to train in our school."
    ichiru "なんでわざわざ遠くに行かなきゃなんねーんだ？ 学校に練習場があるじゃん。"

# game/script.rpy:2288
translate japanese choice_7_A_3e8a6d0d:

    # masaru "Remember he said something about camping as well?"
    masaru "コーチがキャンプについて言ってたこと覚えてるだろ？"

# game/script.rpy:2290
translate japanese choice_7_A_deae8298:

    # masaru "Maybe he just wants to spend time with us too. Some sort of teambuilding?"
    masaru "たぶんコーチは俺たちと過ごしたいんじゃないかな。チームビルディングってやつ？"

# game/script.rpy:2292
translate japanese choice_7_A_94f50c5a:

    ichiru "Pff… I bet he has dirty schemes again."
    ichiru "パフ…私は彼が再び汚れたスキームを持っていると思う。"

# game/script.rpy:2294
translate japanese choice_7_A_34ad09a1:

    ichiru "We'll never know. Imma keep my eyes peeled."
    ichiru "私たちは決して知りません。 Immaは私の目を剥がしています。"

# game/script.rpy:2296
translate japanese choice_7_A_7a558718:

    masaru "Haha! You've always been pretty cautious about what Coach is up to."
    masaru "ハハ！あなたはいつもコーチが何をしているかについてかなり慎重だった。"

# game/script.rpy:2298
translate japanese choice_7_A_e53fdb03:

    # ichiru "To be honest, I am pretty excited too!"
    ichiru "正直俺もかなり興奮してるぜ！"

# game/script.rpy:2300
translate japanese choice_7_A_45d9c210:

    # ichiru "Because Toshu is with us in this field trip!"
    ichiru "この練習合宿でToshuと一緒だからな！"

# game/script.rpy:2302
translate japanese choice_7_A_11bf05c7:

    # ichiru "It's gonna be fun!"
    ichiru "楽しくなるぞ！"

# game/script.rpy:2304
translate japanese choice_7_A_ee54dc9c:

    # masaru "Yeah."
    masaru "そうだな。"

# game/script.rpy:2306
translate japanese choice_7_A_30fa612e:

    masaru "We have a long trip ahead you guys should probably rest."
    masaru "我々はあなたがおそらく休むべきである長い旅を先にしています。"

# game/script.rpy:2308
translate japanese choice_7_A_50ad0521:

    # toshu_t "Nothing much happened… during our road trip."
    toshu_t "道中は何も起こらなかった。"

# game/script.rpy:2309
translate japanese choice_7_A_0ffeaeeb:

    # toshu_t "Because the moment I sat on the seat, I felt so drowsy and eventually fallen asleep."
    toshu_t "座席に座ったとたん僕はとても眠くなり、ずっと寝ていたからだ。"

# game/script.rpy:2310
translate japanese choice_7_A_5ef4c2ad:

    # toshu_t "I got too excited yesterday and lacked sleep!"
    toshu_t "夕べ興奮し過ぎて、睡眠不足だったんだ！"

# game/script.rpy:2318
translate japanese choice_7_B_4b0daf14:

    # toshu "Ichiru. Can I sit beside you?"
    toshu "Ichiru。隣に座っていい？"

# game/script.rpy:2320
translate japanese choice_7_B_3bd7d03f:

    # ichiru "Sure thing!!~"
    ichiru "もちろんだぜ〜！！"

# game/script.rpy:2322
translate japanese choice_7_B_1fd50de9:

    ichiru "I'm so happy you asked me that hahaha!!"
    ichiru "私はハハハッハーに頼んでくれてとても嬉しいよ！"

# game/script.rpy:2324
translate japanese choice_7_B_6a493d34:

    ichiru "But, this is a three-seater bus, and me and Masaru kinda agreed that you sit between us."
    ichiru "しかし、これは3人乗りのバスで、私とマサルは、あなたが私たちの間に座ることに同意しました。"

# game/script.rpy:2326
translate japanese choice_7_B_d1cd5f48:

    ichiru "Don't worry you'd still be sitting beside me!"
    ichiru "まだ私のそばに座っているのを心配しないで！"

# game/script.rpy:2328
translate japanese choice_7_B_ccb3facb:

    ichiru "Only difference is Masaru is there to bother us!"
    ichiru "違いがあるのは私たちを邪魔するだけです！"

# game/script.rpy:2330
translate japanese choice_7_B_1520ded4:

    masaru "I'm right here, Ichiru."
    masaru "私はここにいる、Ichiru。"

# game/script.rpy:2332
translate japanese choice_7_B_90b6d5d5:

    ichiru "C'mon, let's go!"
    ichiru "さあ、行こうよ！"

# game/script.rpy:2335
translate japanese choice_7_B_67afb707:

    toshu "I wonder where will we go today?"
    toshu "私は今日どこに行くのだろうか？"

# game/script.rpy:2337
translate japanese choice_7_B_e8f6729f:

    masaru "Coach said he will take us somewhere we can train."
    masaru "コーチは、私たちが練習できる場所に私たちを連れて行くと言いました。"

# game/script.rpy:2339
translate japanese choice_7_B_d58c259c:

    ichiru "Why go far? We have our own place to train in our school."
    ichiru "なぜ遠くに行くの？私たちは私たちの学校で訓練する私たち自身の場所を持っています。"

# game/script.rpy:2341
translate japanese choice_7_B_3e8a6d0d:

    masaru "Remember he said something about camping as well?"
    masaru "彼はキャンプについても何か言ったことを覚えている？"

# game/script.rpy:2343
translate japanese choice_7_B_deae8298:

    masaru "Maybe he just wants to spend time with us too. Some sort of teambuilding?"
    masaru "たぶん彼は私たちと時間を過ごしたいと思っています。何らかのチームビルディング？"

# game/script.rpy:2345
translate japanese choice_7_B_94f50c5a:

    ichiru "Pff… I bet he has dirty schemes again."
    ichiru "パフ…私は彼が再び汚れたスキームを持っていると思う。"

# game/script.rpy:2347
translate japanese choice_7_B_34ad09a1:

    ichiru "We'll never know. Imma keep my eyes peeled."
    ichiru "私たちは決して知りません。 Immaは私の目を剥がしています。"

# game/script.rpy:2349
translate japanese choice_7_B_7a558718:

    masaru "Haha! You've always been pretty cautious about what Coach is up to."
    masaru "ハハ！あなたはいつもコーチが何をしているかについてかなり慎重だった。"

# game/script.rpy:2351
translate japanese choice_7_B_e53fdb03:

    ichiru "To be honest, I am pretty excited too!"
    ichiru "正直言って、私もかなり興奮しています！"

# game/script.rpy:2353
translate japanese choice_7_B_45d9c210:

    ichiru "Because Toshu is with us in this field trip!"
    ichiru "Toshuはこのフィールドトリップで私たちと一緒だから！"

# game/script.rpy:2355
translate japanese choice_7_B_11bf05c7:

    ichiru "It's gonna be fun!"
    ichiru "それは楽しみになるよ！"

# game/script.rpy:2357
translate japanese choice_7_B_ee54dc9c:

    masaru "Yeah."
    masaru "うん。"

# game/script.rpy:2359
translate japanese choice_7_B_30fa612e:

    masaru "We have a long trip ahead you guys should probably rest."
    masaru "我々はあなたがおそらく休むべきである長い旅を先にしています。"

# game/script.rpy:2361
translate japanese choice_7_B_a6f93757:

    toshu_t "It was really fun getting to sit beside my friends."
    toshu_t "私の友達のそばに座っているのは本当に面白かったです。"

# game/script.rpy:2362
translate japanese choice_7_B_95ba6713:

    toshu_t "Now I am more excited about this field trip!"
    toshu_t "今私はこのフィールドトリップにもっと興奮しています！"

# game/script.rpy:2368
translate japanese choice_7_B_3a25ab0f:

    narrator "Several minutes later…"
    narrator "数分後…"

# game/script.rpy:2371
translate japanese choice_7_B_4a1b7b28:

    ichiru "Ohh, it seems like Masaru already fell asleep."
    ichiru "ああ、マサルはすでに眠っていたようだ。"

# game/script.rpy:2372
translate japanese choice_7_B_d0df4372:

    toshu "Wow, Captain slept so fast."
    toshu "うわー、キャプテンはとても早く寝ました。"

# game/script.rpy:2373
translate japanese choice_7_B_a6915107:

    ichiru "I miss going on long trips like this."
    ichiru "私はこのような長い旅行に行くのを忘れる。"

# game/script.rpy:2375
translate japanese choice_7_B_d09319cd:

    ichiru "I remember my father used to bring me to faraway places so that I can explore the world."
    ichiru "私は父が遠く離れた場所に私を連れて来て、私が世界を探検することができることを覚えています。"

# game/script.rpy:2377
translate japanese choice_7_B_1b550989:

    toshu "Wow… you already went to other countries?"
    toshu "うわー…あなたはすでに他の国に行きましたか？"

# game/script.rpy:2379
translate japanese choice_7_B_fc45d886:

    ichiru "Yep!"
    ichiru "うん！"

# game/script.rpy:2380
translate japanese choice_7_B_c7eea4ee:

    toshu "Wow~ I've never been to any other country before. How lucky!"
    toshu "うわー〜私はかつて他の国に行ったことがない。なんてラッキーなんだ！"

# game/script.rpy:2382
translate japanese choice_7_B_2235a68e:

    toshu "Ohh! Ichiru, I just remembered, I heard that your family has connections with the president?"
    toshu "ああ！ Ichiru、私はちょうど思い出した、あなたの家族は大統領との接続があると聞いた？"

# game/script.rpy:2384
translate japanese choice_7_B_7dc23933:

    toshu "I… I mean well, Ms. Saki said that Tomoka is the president's daughter."
    toshu "私は…元気ですが、沙木さんはトモカは大統領の娘だと言いました。"

# game/script.rpy:2385
translate japanese choice_7_B_0888912e:

    toshu "Knowing she was looking for you like that, it must've been important…"
    toshu "彼女があなたのことを好んでいたことを知っていれば、それは重要だったはずです…"

# game/script.rpy:2387
translate japanese choice_7_B_7a4f5494:

    toshu "Why was Tomoka Looking for you?"
    toshu "トモカはなぜあなたをお探しですか？"

# game/script.rpy:2389
translate japanese choice_7_B_18800ffa:

    ichiru "Well… it's really not interesting as a topic, but…"
    ichiru "まあ、それは本当に面白い話題ではありませんが…"

# game/script.rpy:2391
translate japanese choice_7_B_b543957f:

    ichiru "I guess it's about time I tell you about my family. Since you told me yours, yeah?"
    ichiru "私は私の家族について話す時が来たと思います。あなたがあなたに言ったので、そう？"

# game/script.rpy:2392
translate japanese choice_7_B_58f1adca:

    ichiru "My dad is famous for being a very successful businessman."
    ichiru "私のお父さんは非常に成功したビジネスマンとして有名です。"

# game/script.rpy:2393
translate japanese choice_7_B_9d1f457c:

    ichiru "The city mayor noticed my father's achievement and so he decided to offer him to be his business partner or something…"
    ichiru "市長は私の父の業績に気づいたので、彼をビジネスパートナーや何かにすることを決めました…"

# game/script.rpy:2394
translate japanese choice_7_B_206309ab:

    ichiru "I don't know what exactly they do, and I really don't care."
    ichiru "私は正確に彼らが何をするのか分からず、私は本当に気にしません。"

# game/script.rpy:2396
translate japanese choice_7_B_6935f7df:

    ichiru "Because of that, they were able to contribute significantly to this city's economy…"
    ichiru "そのため、彼らはこの都市の経済に大きく貢献することができました…"

# game/script.rpy:2397
translate japanese choice_7_B_3c44d8fe:

    ichiru "At least, that's how my mom told me."
    ichiru "少なくとも、それは私のお母さんが私に言った方法です。"

# game/script.rpy:2398
translate japanese choice_7_B_13eeaeb9:

    ichiru "I don't know much about my dad anymore since he's always away working."
    ichiru "彼はいつも離れているので、私の父についてもう分かりません。"

# game/script.rpy:2400
translate japanese choice_7_B_2e16e360:

    toshu "Whoa… so you really are from a rich family!"
    toshu "あなたは本当に豊かな家族のものです！"

# game/script.rpy:2402
translate japanese choice_7_B_ab61997a:

    ichiru "Ahh, well… money has never been a problem in our family."
    ichiru "ああ、まあ…お金は私たちの家族の中で一度も問題にはなりませんでした。"

# game/script.rpy:2404
translate japanese choice_7_B_1e569fa8:

    ichiru "I remember my dad came home and wanted to check up on me."
    ichiru "私は父が家に帰ってきて私をチェックしたかったことを覚えています。"

# game/script.rpy:2405
translate japanese choice_7_B_a7342670:

    ichiru "He said he wanted a father-to-son bonding time…"
    ichiru "彼は父から息子への結合時間を望んでいると言いました…"

# game/script.rpy:2407
translate japanese choice_7_B_8c43d0e7:

    ichiru "So he sent me to school in a limousine!"
    ichiru "だから彼は私をリムジンで学校に送りました！"

# game/script.rpy:2408
translate japanese choice_7_B_b54670de:

    ichiru "It was really embarrassing. Even Masaru teased me about it."
    ichiru "それは本当に恥ずかしいことでした。マサルもそれについて私を驚かせました。"

# game/script.rpy:2409
translate japanese choice_7_B_cc39db85:

    toshu "A-amazing…!!"
    toshu "素晴らしい… … !!"

# game/script.rpy:2411
translate japanese choice_7_B_f37717f0:

    ichiru "But really… sometimes being rich has its bad sides too… because of it, the other students were too scared and irritated to be friends with me."
    ichiru "しかし、実際には…時には金持ちになることも悪い面があります…それゆえ、他の学生はあまりにも怖かったし、私と友達になることを苛立ちました。"

# game/script.rpy:2412
translate japanese choice_7_B_a73d109a:

    ichiru "And they say I have some attitude problems too."
    ichiru "そして彼らは私にも態度の問題があると言います。"

# game/script.rpy:2413
translate japanese choice_7_B_491b4d10:

    toshu "Ehh??"
    toshu "Ehh？"

# game/script.rpy:2415
translate japanese choice_7_B_56277bfe:

    ichiru "Don't mind it! It's really no big deal!"
    ichiru "気にしないで！それは本当に大したことではありません！"

# game/script.rpy:2416
translate japanese choice_7_B_2b380cf5:

    ichiru "Maybe you should rest a bit!~ You haven't got enough sleep, right?!"
    ichiru "たぶんあなたはちょっと休まなければなりません！〜あなたは十分な睡眠を取れていません、そうですか？"

# game/script.rpy:2418
translate japanese choice_7_B_9dee90ca:

    toshu "B-but Ichiru, I want to hear more about your story…"
    toshu "B-しかしIchiru、あなたの話をもっと聞きたい"

# game/script.rpy:2420
translate japanese choice_7_B_2f1d51d8:

    ichiru "I'm sure it would be boring!"
    ichiru "私はそれが退屈だろうと確信しています！"

# game/script.rpy:2421
translate japanese choice_7_B_e9e928ff:

    ichiru "Besides, I'm quite sleepy as well."
    ichiru "それに、私もかなり眠いです。"

# game/script.rpy:2423
translate japanese choice_7_B_d4caecfd:

    toshu "Okay then…"
    toshu "じゃあオーケー…"

# game/script.rpy:2425
translate japanese choice_7_B_c3b5cbbe:

    toshu "Thanks for sharing it with me though!"
    toshu "私と一緒にそれを共有してくれてありがとう！"

# game/script.rpy:2427
translate japanese choice_7_B_730518cf:

    toshu_t "I'm glad Ichiru shared something about himself this time."
    toshu_t "今回はIchiruが自分自身について何かを共有してくれてうれしいです。"

# game/script.rpy:2428
translate japanese choice_7_B_bf6d9c23:

    toshu_t "It makes me feel bad that somehow he feels neglected by his family despite being wealthy."
    toshu_t "裕福であるにもかかわらず彼の家族によって何とか無視されていると感じることは、私には気分が悪くなります。"

# game/script.rpy:2429
translate japanese choice_7_B_86ade097:

    toshu_t "I can't help but get more curious when he tries to change the topic."
    toshu_t "私は彼がトピックを変更しようとすると、より興味が持てるように助けることはできません。"

# game/script.rpy:2430
translate japanese choice_7_B_c8ea4c98:

    toshu_t "But for now, I at least got a chance to know him more!"
    toshu_t "しかし、今のところ、私は少なくとも彼をもっと知る機会を得ました！"

# game/script.rpy:2436
translate japanese choice_7_C_0203eb6f:

    toshu "Can I sit beside you, Captain Masaru?"
    toshu "私はあなたのそばに座ってもいいですか？"

# game/script.rpy:2438
translate japanese choice_7_C_a1793b99:

    masaru "Ohh, of course! I'm glad you asked me that."
    masaru "ああ、もちろん！あなたは私にそれを聞いてうれしいです。"

# game/script.rpy:2440
translate japanese choice_7_C_e1531855:

    masaru "But, this is a three-seater bus, and me and Ichiru already agreed that you should sit with us."
    masaru "しかし、これは3人乗りのバスで、私とイッチルはすでにあなたが私たちと一緒に座るべきだということに同意しました。"

# game/script.rpy:2441
translate japanese choice_7_C_b9818349:

    masaru "You'll still be sitting beside me though."
    masaru "あなたはまだ私のそばに座っているでしょう。"

# game/script.rpy:2443
translate japanese choice_7_C_41f313af:

    masaru "Don't worry. It's more fun when we are all together!"
    masaru "心配しないでください。私たちが一緒にいると楽しいです！"

# game/script.rpy:2444
translate japanese choice_7_C_a3804b53:

    toshu "That is so much better, Captain!"
    toshu "それはずっと優れている、キャプテン！"

# game/script.rpy:2446
translate japanese choice_7_C_28199e3f:

    masaru "So, let's go?"
    masaru "じゃ、行こう？"

# game/script.rpy:2447
translate japanese choice_7_C_98727b26:

    toshu "Yes!"
    toshu "はい！"

# game/script.rpy:2452
translate japanese choice_7_C_67afb707:

    toshu "I wonder where will we go today?"
    toshu "私は今日どこに行くのだろうか？"

# game/script.rpy:2454
translate japanese choice_7_C_e8f6729f:

    masaru "Coach said he will take us somewhere we can train."
    masaru "コーチは、私たちが練習できる場所に私たちを連れて行くと言いました。"

# game/script.rpy:2456
translate japanese choice_7_C_d58c259c:

    ichiru "Why go far? We have our own place to train in our school."
    ichiru "なぜ遠くに行くの？私たちは私たちの学校で訓練する私たち自身の場所を持っています。"

# game/script.rpy:2458
translate japanese choice_7_C_3e8a6d0d:

    masaru "Remember he said something about camping as well?"
    masaru "彼はキャンプについても何か言ったことを覚えている？"

# game/script.rpy:2460
translate japanese choice_7_C_deae8298:

    masaru "Maybe he just wants to spend time with us too. Some sort of teambuilding?"
    masaru "たぶん彼は私たちと時間を過ごしたいと思っています。何らかのチームビルディング？"

# game/script.rpy:2462
translate japanese choice_7_C_94f50c5a:

    ichiru "Pff… I bet he has dirty schemes again."
    ichiru "パフ…私は彼が再び汚れたスキームを持っていると思う。"

# game/script.rpy:2464
translate japanese choice_7_C_34ad09a1:

    ichiru "We'll never know. Imma keep my eyes peeled."
    ichiru "私たちは決して知りません。 Immaは私の目を剥がしています。"

# game/script.rpy:2466
translate japanese choice_7_C_7a558718:

    masaru "Haha! You've always been pretty cautious about what Coach is up to."
    masaru "ハハ！あなたはいつもコーチが何をしているかについてかなり慎重だった。"

# game/script.rpy:2468
translate japanese choice_7_C_e53fdb03:

    ichiru "To be honest, I am pretty excited too!"
    ichiru "正直言って、私もかなり興奮しています！"

# game/script.rpy:2470
translate japanese choice_7_C_45d9c210:

    ichiru "Because Toshu is with us in this field trip!"
    ichiru "Toshuはこのフィールドトリップで私たちと一緒だから！"

# game/script.rpy:2472
translate japanese choice_7_C_11bf05c7:

    ichiru "It's gonna be fun!"
    ichiru "それは楽しみになるよ！"

# game/script.rpy:2474
translate japanese choice_7_C_ee54dc9c:

    masaru "Yeah."
    masaru "うん。"

# game/script.rpy:2476
translate japanese choice_7_C_30fa612e:

    masaru "We have a long trip ahead you guys should probably rest."
    masaru "我々はあなたがおそらく休むべきである長い旅を先にしています。"

# game/script.rpy:2478
translate japanese choice_7_C_a6f93757:

    toshu_t "It was really fun getting to sit beside my friends."
    toshu_t "私の友達のそばに座っているのは本当に面白かったです。"

# game/script.rpy:2479
translate japanese choice_7_C_95ba6713:

    toshu_t "Now I am more excited about this field trip!"
    toshu_t "今私はこのフィールドトリップにもっと興奮しています！"

# game/script.rpy:2483
translate japanese choice_7_C_3a25ab0f:

    narrator "Several minutes later…"
    narrator "数分後…"

# game/script.rpy:2488
translate japanese choice_7_C_715e7f8f:

    masaru "Ichiru fell asleep, that's new. He was just so active a while ago."
    masaru "Ichiruは眠りに落ちました、それは新しいものです。彼はしばらく前にとても活発だった。"

# game/script.rpy:2489
translate japanese choice_7_C_abb141de:

    toshu "Y-yeah. Haha!"
    toshu "Y-yeahハハ！"

# game/script.rpy:2492
translate japanese choice_7_C_d0b227b3:

    toshu "Captain Masaru?"
    toshu "キャプテンマサル？"

# game/script.rpy:2493
translate japanese choice_7_C_24640736:

    masaru "Yes, Toshu?"
    masaru "Yes, Toshu?"

# game/script.rpy:2495
translate japanese choice_7_C_e661ba10:

    toshu "I heard from Ichiru that you're also a music club member?"
    toshu "あなたは音楽クラブのメンバーでもあることをIchiruから聞いたことがありますか？"

# game/script.rpy:2496
translate japanese choice_7_C_e0f70870:

    masaru "E-eh?! He told you…?"
    masaru "E-え？彼はあなたに言った…？"

# game/script.rpy:2497
translate japanese choice_7_C_412b25fd:

    toshu "Don't panic, Captain!"
    toshu "慌ててはいけない、キャプテン！"

# game/script.rpy:2498
translate japanese choice_7_C_fb2654a1:

    masaru "Oh no…"
    masaru "あらいやだ…"

# game/script.rpy:2500
translate japanese choice_7_C_a1ee0704:

    toshu "D-don't worry! I really think it's so cool and awesome!"
    toshu "D-心配しないで！私は本当にそれがとてもクールで素晴らしいと思います！"

# game/script.rpy:2501
translate japanese choice_7_C_255fdfc9:

    masaru "Cool…?"
    masaru "クール…？"

# game/script.rpy:2502
translate japanese choice_7_C_5c4aa666:

    toshu "Yes, Captain!"
    toshu "はい、キャプテン！"

# game/script.rpy:2503
translate japanese choice_7_C_2c9ac39f:

    toshu "It' super cool! I love music!"
    toshu "それは素晴らしいです！私は音楽が好きです！"

# game/script.rpy:2505
translate japanese choice_7_C_9c07a7d6:

    toshu "I'm curious though, what instrument do you play?"
    toshu "私は興味がありますが、どの楽器を演奏していますか？"

# game/script.rpy:2506
translate japanese choice_7_C_e2b7a6ab:

    masaru "I play the piano…"
    masaru "私はピアノを弾きます…"

# game/script.rpy:2507
translate japanese choice_7_C_68268c12:

    toshu "Wow that's amazing! How did you learn to play it?"
    toshu "わおそれは驚きだ！あなたはそれをどのようにして学びましたか？"

# game/script.rpy:2509
translate japanese choice_7_C_355522ab:

    masaru "I loved music ever since I was a kid…"
    masaru "私は子供のころから音楽を愛していました…"

# game/script.rpy:2510
translate japanese choice_7_C_7c3761f8:

    masaru "Ever since my mom bought me a piano for my birthday, I kept playing."
    masaru "母が私の誕生日にピアノを買って以来、私は遊んでいました。"

# game/script.rpy:2511
translate japanese choice_7_C_79d804d9:

    masaru "She used to play the piano as well, and she was the one who taught me how to play."
    masaru "彼女はピアノを演奏していましたが、演奏方法を教えてくれました。"

# game/script.rpy:2512
translate japanese choice_7_C_5536b221:

    toshu "Whoa! What a coincidence! My mother plays the piano too!"
    toshu "ウワ！なんて偶然！私の母もピアノを弾く！"

# game/script.rpy:2513
translate japanese choice_7_C_a7af6e96:

    masaru "Wow! She does too? That's great!"
    masaru "うわー！彼女もやってる？それは素晴らしいことです！"

# game/script.rpy:2514
translate japanese choice_7_C_77e9ac54:

    toshu "I remember you mentioned your dad is not usually at home…"
    toshu "あなたのお父さんが家にいないと言ったことを覚えています…"

# game/script.rpy:2515
translate japanese choice_7_C_b659100b:

    toshu "What about your mom? Where is she?"
    toshu "あなたのお母さんはどうですか？彼女はどこ？"

# game/script.rpy:2517
translate japanese choice_7_C_a7d64157:

    masaru "…she passed away from a terminal illness."
    masaru "…彼女は終末期の病気から亡くなりました。"

# game/script.rpy:2518
translate japanese choice_7_C_6f038ad8:

    masaru "My family couldn't afford her maintaining medication…"
    masaru "私の家族は薬を維持することができませんでした…"

# game/script.rpy:2519
translate japanese choice_7_C_6036626a:

    masaru "We even fell bankrupt due to it…"
    masaru "私たちはそれによって破産しました…"

# game/script.rpy:2521
translate japanese choice_7_C_c6519965:

    toshu "I'm so sorry to hear that."
    toshu "それを聞いてとても残念です。"

# game/script.rpy:2523
translate japanese choice_7_C_b1f83d61:

    masaru "It's okay Toshu, it's all in the past!"
    masaru "それは大丈夫トーシュ、それはすべての過去です！"

# game/script.rpy:2525
translate japanese choice_7_C_dcda0fff:

    masaru "Though I swore to myself that I would keep playing the piano… for her!"
    masaru "私は自分のためにピアノを弾き続けると誓ったが…彼女のために！"

# game/script.rpy:2526
translate japanese choice_7_C_500239ea:

    masaru "It's the only memento I have left for my mom."
    masaru "それは私が母に残した唯一の記念品です。"

# game/script.rpy:2528
translate japanese choice_7_C_578715d3:

    masaru "Ah… don't worry much about it, Toshu!"
    masaru "ああ…それほど心配しないで、Toshu！"

# game/script.rpy:2530
translate japanese choice_7_C_53af7238:

    masaru "Maybe you should rest a bit! You need more sleep. you barely had any time to sleep yesterday."
    masaru "たぶんあなたは少し休むべきです！あなたはもっと眠る必要があります。あなたは昨日寝る時間がほとんどありませんでした。"

# game/script.rpy:2532
translate japanese choice_7_C_c229179d:

    toshu "B-but, Captain! I would like to hear more of your story."
    toshu "B-しかし、キャプテン！あなたの話をもっと聞きたいです。"

# game/script.rpy:2534
translate japanese choice_7_C_b10d4cb8:

    masaru "I… it's not really an interesting topic. I don't want you feeling sad just because of my past."
    masaru "私は本当に興味深い話題ではない。私はあなたが私の過去のために悲しいと感じることを望んでいません。"

# game/script.rpy:2536
translate japanese choice_7_C_873b5090:

    masaru "I'll tell you some other time! For now, let's get some rest, okay?"
    masaru "私は別の時を教えてあげるよ！今のところ、ちょっと休みましょう、よろしいですか？"

# game/script.rpy:2538
translate japanese choice_7_C_d8554755:

    toshu "Okay, Captain."
    toshu "さて、キャプテン。"

# game/script.rpy:2540
translate japanese choice_7_C_b89a0042:

    toshu_t "It was really sad to learn about Captain Masaru's mom…"
    toshu_t "マサル大佐のお母さんについて学ぶのは本当に悲しかった…"

# game/script.rpy:2541
translate japanese choice_7_C_ae5ee679:

    toshu_t "I wish he told me more… there's something in his eyes that made the air heavy."
    toshu_t "彼は私にもっと教えて欲しいと思っています…空気を重くした何かが彼の目にあります。"

# game/script.rpy:2542
translate japanese choice_7_C_f58ddabb:

    toshu_t "I can also feel from his smiles that he's really not okay…"
    toshu_t "私は彼が本当に大丈夫ではないという笑顔からも感じることができます…"

# game/script.rpy:2543
translate japanese choice_7_C_8963bc60:

    toshu_t "But I'm happy enough that he opened up to me like that. I'm getting to know him better."
    toshu_t "しかし、私は彼が私のようにそれを開いたように十分満足しています。私は彼をよく知るようになっています。"

# game/script.rpy:2548
translate japanese choice_7_end_9794d27a:

    # genji "Wake up! We're here now."
    genji "起きろ！ 着いたぞ。"

# game/script.rpy:2557
translate japanese choice_7_end_6abd3edf:

    # masaru "Wow the view is gorgeous~"
    masaru "うわー、いい眺めだな〜。"

# game/script.rpy:2558
translate japanese choice_7_end_c86dbc59:

    # ichiru "Yeah! Super awesome!"
    ichiru "ああ！ 超スゲぇな！"

# game/script.rpy:2560
translate japanese choice_7_end_33ed6ba1:

    # ichiru "The smell of fresh air! I missed this!"
    ichiru "新鮮な空気の香り！ これだよこれ！"

# game/script.rpy:2561
translate japanese choice_7_end_80f23af4:

    # toshu "Yeahh! It's so cool!"
    toshu "うわぁ～！ すっごいなー！"

# game/script.rpy:2562
translate japanese choice_7_end_ed73e4dd:

    toshu "I wanna go camping in the woods, explore and collect stuff!!"
    toshu "森の中でキャンプして、探検して宝探ししたい！！"

# game/script.rpy:2564
translate japanese choice_7_end_e11d9ada:

    # toshu "Its gonna be so much fun!~"
    toshu "楽しいだろうな〜！"

# game/script.rpy:2566
translate japanese choice_7_end_0c0c2cfd:

    masaru "Hahaha slow down there Toshu, we can go exploring and have fun after we train."
    masaru "ハハハはそこでToshuを減速させて、私たちは練習してみることができます。"

# game/script.rpy:2568
translate japanese choice_7_end_7fefad98:

    toshu "Yes! I'll be patient!"
    toshu "はい！私は忍耐強くなる！"

# game/script.rpy:2573
translate japanese choice_7_end_e9b87e6a:

    # genji "Attention, everyone!"
    genji "みんな注目！"

# game/script.rpy:2575
translate japanese choice_7_end_d440da5f:

    # genji "Okay then, I will explain what we are gonna do here."
    genji "よしじゃあ俺たちがここで何をするのか説明するぞ。"

# game/script.rpy:2576
translate japanese choice_7_end_e097f853:

    # genji "We're here today at the mountain campsite!"
    genji "俺たちは今、山のキャンプ場にいる！"

# game/script.rpy:2577
translate japanese choice_7_end_3337f425:

    # genji "We will be staying here for three days and two nights for only one reason!"
    genji "俺たちはここでただ１つの目的のために２泊３日の練習合宿を行う！"

# game/script.rpy:2579
translate japanese choice_7_end_b546d820:

    genji "With upcoming tournament nearing, everyone will have to work their asses off, and train!"
    genji "次のトーナメントに近づくと、誰もがお尻をオフにしてトレーニングしなければなりません！"

# game/script.rpy:2581
translate japanese choice_7_end_d2a64bad:

    genji "Today, since we are at the base of the mountain, we will train in the nearby baseball field that provided to us!"
    genji "今日、私たちは山の基地にいるので、私たちに提供された近くの野球場でトレーニングを行います！"

# game/script.rpy:2583
translate japanese choice_7_end_ad2e223a:

    # genji "WE WILL TRAIN NONSTOP UNTIL THE SUN SETS!"
    genji "日没までノンストップで特訓だ！"

# game/script.rpy:2585
translate japanese choice_7_end_19852401:

    genji "I better see progress from you guys or else I won't let you sleep tonight."
    genji "私はあなた方からの進歩を見ている方が良いでしょう。さもなくば、私はあなたに今夜寝らせません。"

# game/script.rpy:2587
translate japanese choice_7_end_06a8159e:

    # genji "Especially you, Toshu Kanada!"
    genji "特にお前だ、Toshu Kanada！"

# game/script.rpy:2589
translate japanese choice_7_end_24254782:

    genji "Since you're our trump card in the tournament, you better work on your chemistry with your teammates."
    genji "あなたはトーナメントでのトランプカードなので、チームメートとのケミストリーをより良くすることができます。"

# game/script.rpy:2591
translate japanese choice_7_end_22857d3e:

    genji "I don't want to see you fooling around here, you have a lot to learn!"
    genji "私はあなたがここをだましているのを見たくない、学ぶべきことがたくさんある！"

# game/script.rpy:2593
translate japanese choice_7_end_38ccef92:

    # genji "YOU GOT THAT?!!"
    genji "わかったか？！！"

# game/script.rpy:2595
translate japanese choice_7_end_03ca335c:

    # toshu "Y-YES COACH!!!"
    toshu "は…はい、コーチ！！！"

# game/script.rpy:2596
translate japanese choice_7_end_1d61d1e2:

    genji "GOOD! Now prepare your stuff and set up your baseball gear, we don't want to waste daylight."
    genji "よし！ 今あなたのものを準備し、あなたの野球装置を設定し、我々は昼光を浪費したくない。"

# game/script.rpy:2597
translate japanese choice_7_end_673d5c78:

    # genji "Okay team! Let's go!"
    genji "OK、みんな！ 行くぞ！"

# game/script.rpy:2622
translate japanese after_minigame_3_6e28b8f4:

    # genji "Great session!"
    genji "よくやったぞ！"

# game/script.rpy:2624
translate japanese after_minigame_3_5788293e:

    # toshu "Thank you, Coach!"
    toshu "ありがとうございます、コーチ！"

# game/script.rpy:2626
translate japanese after_minigame_3_c2bc295e:

    ichiru "This is weird… Gayji's actually training 'with' us!"
    ichiru "これは奇妙です…ゲイジは実際に私たちと一緒にトレーニング！"

# game/script.rpy:2628
translate japanese after_minigame_3_9b856ad3:

    masaru "Well, truth is, he's always been coaching us. He's just usually delegating the tasks down to me."
    masaru "まあ、真実です、彼はいつも私たちを指導してきました。彼はたいてい仕事を私に委ねています。"

# game/script.rpy:2630
translate japanese after_minigame_3_39916820:

    genji "That's right! HAHAHA!"
    genji "そのとおり！ははは！"

# game/script.rpy:2631
translate japanese after_minigame_3_a5844b54:

    genji "Are you now impressed with my knowledge of baseball?!"
    genji "あなたは今野球の知識に感心していますか？"

# game/script.rpy:2633
translate japanese after_minigame_3_31414dd3:

    # ichiru "Nope. Not one bit!"
    ichiru "全然。これっぽっちも！"

# game/script.rpy:2637
translate japanese after_minigame_3_7a99e175:

    # genji "WHAT?!?!"
    genji "何だと？！？！"

# game/script.rpy:2639
translate japanese after_minigame_3_1111c4f9:

    # masaru "H-hey… you two, calm down…"
    masaru "ね、ねえ…二人とも落ち着いて…"

# game/script.rpy:2641
translate japanese after_minigame_3_4a8d71a2:

    genji "That's it! You guys are going to do two sessions today!!"
    genji "それでおしまい！あなたたちは今日2つのセッションをするつもりです!!"

# game/script.rpy:2643
translate japanese after_minigame_3_b988ba1e:

    # toshu "Waah…"
    toshu "うわぁ…"

# game/script.rpy:2645
translate japanese after_minigame_3_bb1a67fb:

    # ichiru "Pssh! I don't care! Make it twenty more if you want! I'll surely hit all balls you throw at me!"
    ichiru "へんっ！ 俺は構わねえぜ！ 10でも20でもやってやらぁ！ お前が投げるボール、全部打ってやるよ！"

# game/script.rpy:2646
translate japanese after_minigame_3_927f3eb8:

    ichiru "Maybe this is how you treat your poor preschool students! How unlucky of them!!"
    ichiru "たぶんこれは貧しい就学前の生徒をどう扱うかです！どのようにそれらの不運！"

# game/script.rpy:2648
translate japanese after_minigame_3_dad83552:

    genji "WHY YOU…!"
    genji "あなたはなぜ…！"

# game/script.rpy:2650
translate japanese after_minigame_3_75778a2a:

    toshu "C-Captain! Are they fighting for real?"
    toshu "C-キャプテン！彼らは本当に戦っていますか？"

# game/script.rpy:2652
translate japanese after_minigame_3_b59ad9a6:

    masaru "For the first time… I'm not really sure…"
    masaru "初めて…本当にわかりません…"

# game/script.rpy:2654
translate japanese after_minigame_3_3fde35f0:

    genji "Teamwork is everything! I will not tolerate your arrogance, Ichiru!"
    genji "チームワークはすべてです！私はあなたの傲慢、Ichiruを容認しません！"

# game/script.rpy:2657
translate japanese after_minigame_3_58f8eb5d:

    genji "If you don't know how to cooperate and respect me as your Coach! YOU MIGHT AS WELL QUIT!"
    genji "あなたがコーチとして私を協力し、尊敬する方法を知らないなら！あなたはうまくいくかもしれません！"

# game/script.rpy:2659
translate japanese after_minigame_3_e0e2ad6e:

    genji "We do one more session!! Get ready!!!"
    genji "我々はもう一度セッションを行う!!準備する！"

# game/script.rpy:2678
translate japanese after_minigame_4_2c879c03:

    toshu_t "It was a pretty exhausting day…"
    toshu_t "かなり疲れた日だった…"

# game/script.rpy:2679
translate japanese after_minigame_4_51ecc5cb:

    toshu_t "The training was extra intense. I can totally feel Coach Genji and Ichiru's conflict."
    toshu_t "トレーニングは非常に激しかったです。Genji教授とIchiruの葛藤を完全に感じることができます。"

# game/script.rpy:2680
translate japanese after_minigame_4_dd395432:

    toshu_t "I hope things will turn out okay…"
    toshu_t "私は物事が大丈夫になることを願って…"

# game/script.rpy:2690
translate japanese after_minigame_4_9a4de406:

    # toshu "Huff huff…"
    toshu "ハァハァ…"

# game/script.rpy:2691
translate japanese after_minigame_4_1418f483:

    # masaru "Whew…"
    masaru "ふぅ…"

# game/script.rpy:2693
translate japanese after_minigame_4_6e0ea4f7:

    # genji "Great job, everyone!"
    genji "みんな、よくやったぞ！"

# game/script.rpy:2694
translate japanese after_minigame_4_6d27b105:

    genji "I'm so proud of you guys!"
    genji "私はあなたたちのことをとても誇りに思います！"

# game/script.rpy:2696
translate japanese after_minigame_4_c388b4dd:

    # toshu "Ehh? What's wrong Ichiru?"
    toshu "あれ？ Ichiru、どうかしたの？"

# game/script.rpy:2698
translate japanese after_minigame_4_d9c98c0b:

    # masaru "Did you hurt yourself?"
    masaru "どっか痛めたか？"

# game/script.rpy:2700
translate japanese after_minigame_4_7268a29b:

    # genji "Huh?"
    genji "ン？"

# game/script.rpy:2701
translate japanese after_minigame_4_419c0495:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:2702
translate japanese after_minigame_4_0c26df0f:

    # genji "You alright, boy?"
    genji "お前、大丈夫か？"

# game/script.rpy:2704
translate japanese after_minigame_4_d78baa59:

    # ichiru "*sniff*"
    ichiru "(グスン)"

# game/script.rpy:2706
translate japanese after_minigame_4_84187bae:

    genji "Ah, shucks…"
    genji "ああ、シャックス…"

# game/script.rpy:2707
translate japanese after_minigame_4_1d88dd56:

    genji "I'll be fired for sure, if your dad finds out about this."
    genji "Ichiruの父親がこれを知ったら、俺はクビになるだろうな。"

# game/script.rpy:2709
translate japanese after_minigame_4_a447f8ae:

    # ichiru "S-stupid…"
    ichiru "そ、そんなバカな…"

# game/script.rpy:2711
translate japanese after_minigame_4_ee1b9edd:

    genji "C'mon! You're a grown up man, aren't you? It's just a little feedback from your Coach!"
    genji "さあ！あなたは大人なんだよね？あなたのコーチからのちょっとしたフィードバックです！"

# game/script.rpy:2713
translate japanese after_minigame_4_6176d9be:

    # ichiru "*sniff* Y-yes… Coach Genji… *sniff*"
    ichiru "(グスン) は、はい…Genjiコーチ… (グスン)"

# game/script.rpy:2715
translate japanese after_minigame_4_319007a2:

    # genji "SHIT SHIT SHIT! Don't cry!!!"
    genji "おいおい、泣くなよ！！！"

# game/script.rpy:2716
translate japanese after_minigame_4_62c41594:

    ichiru "I'm not crying… I don't want to quit…"
    ichiru "私は泣いていない…私はやめたくない…"

# game/script.rpy:2718
translate japanese after_minigame_4_cc6ad07d:

    genji "Oh maaan…"
    genji "ああ待って…"

# game/script.rpy:2721
translate japanese after_minigame_4_592c3e9e:

    genji "Okay, okay, let's get you changed alright? I won't say that anymore, I promise!"
    genji "さて、あなたは大丈夫に変えましょうか？もう言わないよ、約束するよ！"

# game/script.rpy:2723
translate japanese after_minigame_4_59d686e3:

    genji "That's it for today. Masaru, Toshu, you two take a bath as well!"
    genji "それは今日のそれです。勝、俊、お風呂にも二人乗るよ！"

# game/script.rpy:2724
translate japanese after_minigame_4_70af56d6:

    genji "We'll be right back."
    genji "私たちはすぐに戻ってきます。"

# game/script.rpy:2726
translate japanese after_minigame_4_923a3de4:

    # toshu "Yes, Coach!!"
    toshu "はい、コーチ！！"

# game/script.rpy:2728
translate japanese after_minigame_4_22613720:

    # masaru "Yes, Coach…!"
    masaru "はい、コーチ…！"

# game/script.rpy:2734
translate japanese after_minigame_4_1292e41b:

    # toshu "What happened just now?"
    toshu "何があったんだろう？"

# game/script.rpy:2736
translate japanese after_minigame_4_621bdb60:

    masaru "That's the first time I see them both heated like that…"
    masaru "それは私がそれらのように熱くなるのを見るのは初めてです…"

# game/script.rpy:2737
translate japanese after_minigame_4_d3710962:

    # toshu "Is Ichiru going to be alright? He looks like he was about to cry…"
    toshu "Ichiruは大丈夫かな？ 泣きそうになってたけど…"

# game/script.rpy:2739
translate japanese after_minigame_4_160ac547:

    masaru "I'm sure he'll be fine. Coach's gonna take care of him."
    masaru "彼は大丈夫だろうと確信しています。コーチが彼を世話するつもりだ。"

# game/script.rpy:2740
translate japanese after_minigame_4_4e8c46aa:

    toshu "Does Coach and Ichiru… dislike each other?"
    toshu "コーチとイッチルはお互いが嫌いですか？"

# game/script.rpy:2742
translate japanese after_minigame_4_39b0a50d:

    # masaru "No! Not at all!"
    masaru "いいや！ そんなことないよ！"

# game/script.rpy:2744
translate japanese after_minigame_4_7db8983f:

    # masaru "Coach Genji is like our second father! Though he's not really old to be called a father, Haha!"
    masaru "Genjiコーチはもう一人の父親のような存在なんだ！ 実際、父親と呼ばれるような歳じゃないけどね、ハハハ！"

# game/script.rpy:2745
translate japanese after_minigame_4_9f7f0618:

    masaru "Ichiru's father is always busy with work, he practically have no time for Ichiru."
    masaru "イチルの父はいつも仕事で忙しいですが、実際にイチールのための時間はありません。"

# game/script.rpy:2746
translate japanese after_minigame_4_6166cdbf:

    masaru "Coach, being a father figure to us, Ichiru pours all of his frustrations towards him instead."
    masaru "コーチは、私たちの父親の姿であり、イチールは代わりに彼のすべての不満を彼に向けています。"

# game/script.rpy:2747
translate japanese after_minigame_4_0288aeae:

    masaru "Of course, Coach is fully aware of this and understands Ichiru's situation."
    masaru "もちろん、コーチはこれを完全に認識しており、イチールの状況を理解しています。"

# game/script.rpy:2748
translate japanese after_minigame_4_45415123:

    toshu "Oh… I see…"
    toshu "ああなるほど…"

# game/script.rpy:2750
translate japanese after_minigame_4_b5c50806:

    masaru "Even though Ichiru don't attend much of his classes, he really enjoys his time in school."
    masaru "イチールは授業の多くには出席しませんが、学校では本当に楽しんでいます。"

# game/script.rpy:2751
translate japanese after_minigame_4_4774e8c6:

    masaru "Especially when he's with the team, including Coach Genji's company!"
    masaru "特にコーチ元祖の会社を含め、チームにいるときは！"

# game/script.rpy:2753
translate japanese after_minigame_4_263968c5:

    toshu "But Ichiru seem to always tease and make fun of Coach."
    toshu "しかし、イチルはいつもコーチをやっつけて楽しませるようです。"

# game/script.rpy:2755
translate japanese after_minigame_4_0a58dbd4:

    masaru "Is it safe to say that… it's Ichiru's way of affection???"
    masaru "それを言うのは安全なのでしょうか…それはイチールの愛情の方法ですか？"

# game/script.rpy:2757
translate japanese after_minigame_4_3323eac2:

    toshu "Then, that is sure some unique way to show fondness to someone."
    toshu "そうすれば、誰かに好意を示すための独特の方法が得られるはずです。"

# game/script.rpy:2758
translate japanese after_minigame_4_bc46e814:

    toshu "I'm relieved."
    toshu "安心した。"

# game/script.rpy:2760
translate japanese after_minigame_4_e58907c8:

    masaru "Yes, you should be."
    masaru "はい、そうでしょう。"

# game/script.rpy:2761
translate japanese after_minigame_4_29f4dc2f:

    masaru "We have to clean up before they come back. Let's go?"
    masaru "彼らが戻ってくる前に掃除をしなければなりません。行こう？"

# game/script.rpy:2763
translate japanese after_minigame_4_5ae9270e:

    # toshu "Yes Captain!"
    toshu "はい、キャプテン！"

# game/script.rpy:2771
translate japanese after_minigame_4_29c47d26:

    # masaru "I'm so beat… I wonder where we will stay tonight…"
    masaru "疲れた…今夜はどこに泊まればいいんだろうか…"

# game/script.rpy:2773
translate japanese after_minigame_4_e2e21a61:

    # toshu "I don't see any nearby buildings. We must be setting up a camp right here!"
    toshu "近くに建物は無いですし、ここでキャンプするしかないですよ！"

# game/script.rpy:2775
translate japanese after_minigame_4_96c3c4cb:

    # masaru "Where do you get such energy Toshu?"
    masaru "Toshu、なんでそんなに元気なんだ？"

# game/script.rpy:2777
translate japanese after_minigame_4_e5860817:

    # toshu "Aren't you excited, Captain?!"
    toshu "キャプテン、ワクワクしませんか？"

# game/script.rpy:2779
translate japanese after_minigame_4_53200d99:

    # masaru "I don't like camping, it's troublesome."
    masaru "俺はキャンプがあまり好きじゃない、面倒なだけだ。"

# game/script.rpy:2782
translate japanese after_minigame_4_6d893c11:

    # genji "Attention everyone!"
    genji "みんな注意！"

# game/script.rpy:2785
translate japanese after_minigame_4_214b3576:

    # toshu "!!!"
    toshu "！！！"

# game/script.rpy:2786
translate japanese after_minigame_4_57744ae4:

    # toshu "Ichiru's smiling as if nothing happened!"
    toshu "Ichiruは何事もなかったかのように笑ってる！"

# game/script.rpy:2788
translate japanese after_minigame_4_fc925fb4:

    # masaru "I told you he'll be fine."
    masaru "大丈夫だって言ったろ。"

# game/script.rpy:2790
translate japanese after_minigame_4_1dbafd98:

    genji "Here are the tents we'll use for sleeping!"
    genji "ここには寝るのに使うテントがあります！"

# game/script.rpy:2792
translate japanese after_minigame_4_ca2aed81:

    genji "Since our school didn't give us enough budget for a rest house, they gave us these tents instead!"
    genji "私たちの学校は休憩所に十分な予算を与えてくれなかったので、代わりに私たちにこのテントをくれました！"

# game/script.rpy:2794
translate japanese after_minigame_4_fc7a20cc:

    genji "Hahaha! Isn't that cool?!"
    genji "ははは！それはクールではないですか？"

# game/script.rpy:2796
translate japanese after_minigame_4_decf1250:

    toshu "WOOOOOOWWWW!!!!"
    toshu "WOOOOOOWWWW !!!!"

# game/script.rpy:2797
translate japanese after_minigame_4_7f3802fb:

    genji "And besides, it's a new experience for you to be camping out, right?"
    genji "そして、それはキャンプアウトのための新しい経験ですね。"

# game/script.rpy:2799
translate japanese after_minigame_4_c3bbb66d:

    ichiru "I wish Genji told me about it before, so I can ask my dad to provide for us."
    ichiru "Genjiが以前に教えてくれたら、私のお父さんに私たちに提供してもらうことができます。"

# game/script.rpy:2801
translate japanese after_minigame_4_85fa4250:

    genji "Grab a tent and set up! One tent should fit three people in."
    genji "テントをつかんでセットアップしてください！ 1つのテントは3人の人に合うべきです。"

# game/script.rpy:2802
translate japanese after_minigame_4_10e34ea4:

    genji "I'll see you guys tomorrow! Go get plenty of rest!"
    genji "明日お会いしましょう！行って、たくさんの休息を取ってください！"

# game/script.rpy:2806
translate japanese after_minigame_4_4478ada6:

    ichiru "I'll take one for me, Masaru and Toshu~!"
    ichiru "私は私のために1つを取る、勝と俊夫〜！"

# game/script.rpy:2808
translate japanese after_minigame_4_b21ecfe5:

    toshu "AWESOME!!!"
    toshu "驚くばかり！！！"

# game/script.rpy:2809
translate japanese after_minigame_4_c67c8c63:

    toshu "We're really gonna camp out! This is so exciting!"
    toshu "私たちは本当にキャンプをするつもりです！これはとてもエキサイティングです！"

# game/script.rpy:2811
translate japanese after_minigame_4_360aa20a:

    ichiru "Cool! C'mon Toshu help me set up the tent!"
    ichiru "クール！さようなら私はテントを設定するのを手伝ってください！"

# game/script.rpy:2812
translate japanese after_minigame_4_b4c313bc:

    toshu "Okaaaay! Let's go, Captain!"
    toshu "Okaaaay！行こう、キャプテン！"

# game/script.rpy:2814
translate japanese after_minigame_4_d5427de1:

    masaru "S-sure."
    masaru "確かに"

# game/script.rpy:2817
translate japanese after_minigame_4_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:2819
translate japanese after_minigame_4_419c0495_1:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:2822
translate japanese after_minigame_4_4f125e45:

    # ichiru "Pffft… HAHAHA!"
    ichiru "プッ…ハハハ！"

# game/script.rpy:2823
translate japanese after_minigame_4_afd64135:

    # ichiru "What are you doing Masaru?!"
    ichiru "Masaru、何やってんだよ？"

# game/script.rpy:2825
translate japanese after_minigame_4_0e3ffad2:

    # masaru "Hey I'm doing my best here!"
    masaru "これでも一生懸命やってんだよ！"

# game/script.rpy:2827
translate japanese after_minigame_4_0a96a972:

    # toshu "It seems Captain doesn't know how to set up a tent?"
    toshu "キャプテンはテントの張り方を知らないみたいですね？"

# game/script.rpy:2828
translate japanese after_minigame_4_c39229b2:

    # ichiru "Hahaha! HOW LAME MASARU!!"
    ichiru "ハハハ！ Masaruだっせぇ！"

# game/script.rpy:2830
translate japanese after_minigame_4_75c02d07:

    # masaru "A-a little help…?"
    masaru "ちょ、ちょっとは手伝ってくれよ…"

# game/script.rpy:2832
translate japanese after_minigame_4_23145908:

    toshu "Ahhh… this thing goes here. It's really simple, Captain."
    toshu "ああ…これはここに行く。本当にシンプルです、キャプテン。"

# game/script.rpy:2834
translate japanese after_minigame_4_15e17b5e:

    masaru "I'm sorry Toshu but camping is not really my thing."
    masaru "申し訳ありませんが、キャンプは本当に私のことではありません。"

# game/script.rpy:2835
translate japanese after_minigame_4_b2d8e64b:

    masaru "Plus I don't have any experience of it before…"
    masaru "プラス私は前にそれの経験を持っていない…"

# game/script.rpy:2837
translate japanese after_minigame_4_4e5918f5:

    toshu "It's okay Captain, everyone has a first time for everything!"
    toshu "それは大丈夫ですキャプテン、誰もがすべてのために初めてです！"

# game/script.rpy:2838
translate japanese after_minigame_4_5d36ec8a:

    masaru "You must think I'm so lame now…"
    masaru "あなたは今私がとても痩せていると思っているはずです…"

# game/script.rpy:2840
translate japanese after_minigame_4_4cf534f5:

    toshu "No Captain! Not at all! I still think you are very AWESOME!"
    toshu "いいえ、キャプテン！どういたしまして！私はまだあなたがとても凄いと思う！"

# game/script.rpy:2842
translate japanese after_minigame_4_8d68a7bc:

    ichiru "Heeeey! How come you never compliment me Tofu?!"
    ichiru "おい！どのように私はTofuを褒めることはありませんか？"

# game/script.rpy:2843
translate japanese after_minigame_4_0a99f484:

    ichiru "Hmph, while you guys were talking, I already finished setting up the tent!"
    ichiru "あなたが話している間に、私はすでにテントの設定を終えました！"

# game/script.rpy:2845
translate japanese after_minigame_4_c9b22630:

    masaru "W-whoa…"
    masaru "W-whoa …"

# game/script.rpy:2847
translate japanese after_minigame_4_efebb126:

    toshu "That was less than a minute…!"
    toshu "それは1分足らずだった…！"

# game/script.rpy:2849
translate japanese after_minigame_4_e1b62e54:

    # toshu "THAT WAS SO PRO, ICHIRU!"
    toshu "Ichiru、プロじゃん！"

# game/script.rpy:2851
translate japanese after_minigame_4_323cf0d7:

    # ichiru "I know right? Let's go inside!"
    ichiru "だろ？ 中に入ろうぜ！"

# game/script.rpy:2860
translate japanese after_minigame_4_0a202725:

    toshu "Woowww!! This is so cool, Ichiru!"
    toshu "うわー!!これはすごくすごい、Ichiru！"

# game/script.rpy:2861
translate japanese after_minigame_4_0ab71e4e:

    ichiru "Yep! I like the fact that it's not spacious. We can cuddle together like brothers!"
    ichiru "うん！私はそれが広々としていないという事実が好きです。私たちは兄弟のように一緒に抱き合うことができます！"

# game/script.rpy:2862
translate japanese after_minigame_4_02c122ee:

    masaru "This is really impressive, Ichiru."
    masaru "これは本当に印象的な、Ichiru。"

# game/script.rpy:2864
translate japanese after_minigame_4_89059735:

    ichiru "*yaaawwwn*"
    ichiru "* yaaawwwn *"

# game/script.rpy:2865
translate japanese after_minigame_4_e5c51e8e:

    ichiru "Today was extra tiresome…"
    ichiru "今日は余計に面倒だった…"

# game/script.rpy:2867
translate japanese after_minigame_4_4e848e4a:

    masaru "I guess we better sleep now, we need to keep our energy up for tomorrow."
    masaru "私たちは今よりよく眠り、明日のためにエネルギーを蓄えておく必要があります。"

# game/script.rpy:2869
translate japanese after_minigame_4_bc7673d6:

    ichiru "Yeah… so it's gonna be like this for three days?"
    ichiru "うん… 3日間このようになるの？"

# game/script.rpy:2871
translate japanese after_minigame_4_ea95961c:

    toshu "I like it! Don't you guys think this is fun?!"
    toshu "私はそれが好きです！あなたたちはこれが楽しいと思いませんか？"

# game/script.rpy:2873
translate japanese after_minigame_4_0f9eea12:

    ichiru "Now don't get too excited again, you don't want another set of eye bags tomorrow, yeah?"
    ichiru "今やもう一度興奮しないでください、明日の別のセットのアイバッグを望んでいない、そうですか？"

# game/script.rpy:2875
translate japanese after_minigame_4_fc60792c:

    masaru "So can we call it a night?"
    masaru "だから私たちはそれを夜と呼ぶことができますか？"

# game/script.rpy:2877
translate japanese after_minigame_4_3a1349f2:

    # ichiru "Good night Tofu~"
    ichiru "おやすみTofu〜。"

# game/script.rpy:2879
translate japanese after_minigame_4_868273a9:

    # toshu "Goodnight guys."
    toshu "みんなおやすみ。"

# game/script.rpy:2881
translate japanese after_minigame_4_fd6404fb:

    toshu_t "I fell asleep as soon as I closed my eyes. I guess I really had a long and exhausting day."
    toshu_t "私は目を閉じてすぐに眠りに落ちました。私は本当に長くて疲れた日だったと思う。"

# game/script.rpy:2882
translate japanese after_minigame_4_d9607e44:

    toshu_t "It was nice to sleep beside Ichiru and Masaru, I felt closer to my friends."
    toshu_t "イチルとマサルのそばで寝るのはうれしかったし、私は友達に近づいた。"

# game/script.rpy:2889
translate japanese after_minigame_4_bac40f20:

    # random "Take it off."
    random "さあ、脱いで。"

# game/script.rpy:2890
translate japanese after_minigame_4_d7de735e:

    # toshu "What are we going to do?"
    toshu "何をするの？"

# game/script.rpy:2891
translate japanese after_minigame_4_406953e3:

    # random "Just let me do all the work~"
    random "全部俺に任せてくれればいいから…"

# game/script.rpy:2892
translate japanese after_minigame_4_9d981df6:

    # toshu "Ahhh, not there…!"
    toshu "ああっ、そこはダメ…！"

# game/script.rpy:2893
translate japanese after_minigame_4_ff337c21:

    # random "Relax, you'll feel great eventually."
    random "力を抜いて。すぐに気持ちよくなるから。"

# game/script.rpy:2894
translate japanese after_minigame_4_7b8a5c32:

    # toshu "Ahhh…!"
    toshu "ああ…！"

# game/script.rpy:2900
translate japanese after_minigame_4_629dbb33:

    # toshu "Wah!!"
    toshu "うわっ！！"

# game/script.rpy:2901
translate japanese after_minigame_4_6badc1c6:

    # toshu_t "Oh. It's just a dream…"
    toshu_t "ああ。夢か…"

# game/script.rpy:2903
translate japanese after_minigame_4_52dfc17e:

    # toshu_t "How weird… I can't remember who it was…"
    toshu_t "変な夢…あれが誰だったのか思い出せない…"

# game/script.rpy:2905
translate japanese after_minigame_4_ee1cc90b:

    # toshu_t "Ahh… I feel so weird…"
    toshu_t "ああ…変な感じだ…"

# game/script.rpy:2907
translate japanese after_minigame_4_f04ab6e7:

    # toshu_t "Oh no… I'm… erect."
    toshu_t "え、ウソ…僕…勃ってる。"

# game/script.rpy:2908
translate japanese after_minigame_4_6cabfb3a:

    # toshu_t "Looks like Captain Masaru and Ichiru are still asleep."
    toshu_t "MasaruキャプテンとIchiruはまだ眠ってるみたい。"

# game/script.rpy:2909
translate japanese after_minigame_4_c3fe8825:

    # toshu_t "I wonder if I should…"
    toshu_t "どうしよう…"

# game/script.rpy:2911
translate japanese after_minigame_4_e9ef6663:

    # toshu_t "That dream…"
    toshu_t "さっきの夢…"

# game/script.rpy:2912
translate japanese after_minigame_4_702d4d11:

    toshu_t "The moment I had that dream…"
    toshu_t "私はその夢があった瞬間…"

# game/script.rpy:2913
translate japanese after_minigame_4_d6420f47:

    toshu_t "I knew that I already developed a feeling…"
    toshu_t "私はすでに気持ちが発達していることを知っていました…"

# game/script.rpy:2915
translate japanese after_minigame_4_2c9a0198:

    # toshu "Mnnn… I can't sleep like this."
    toshu "むう…こんなんじゃ眠れないよ。"

# game/script.rpy:2916
translate japanese after_minigame_4_c434a424:

    # toshu "I hope they don't notice."
    toshu "みんなに気付かれませんように。"

# game/script.rpy:2917
translate japanese after_minigame_4_8749aa45:

    # toshu "I have to do this really quick…"
    toshu "早く処理しちゃわないと…"

# game/script.rpy:2926
translate japanese after_minigame_4_751404a4:

    # toshu "Ahhh…"
    toshu "ああっ…"

# game/script.rpy:2927
translate japanese after_minigame_4_0f67ae1f:

    # toshu "That felt great."
    toshu "気持ち良かった。"

# game/script.rpy:2928
translate japanese after_minigame_4_accff51a:

    # toshu "I better clean up before anyone catches me…"
    toshu "誰か気付かれる前に掃除しないと…"

# game/script.rpy:2930
translate japanese after_minigame_4_cf8df572:

    toshu_t "I don't know what had gotten into me…"
    toshu_t "私は何が私の中に入ったのかわからない…"

# game/script.rpy:2931
translate japanese after_minigame_4_59fdb9ee:

    # toshu_t "Why did I do that…? Especially when Ichiru and Captain is just beside me…"
    toshu_t "何であんなことしちゃったんだろう？ Ichiruとキャプテンが横にいるってのに…"

# game/script.rpy:2932
translate japanese after_minigame_4_339d8459:

    toshu_t "Good thing they didn't wake up… or else it would have been so embarrassing."
    toshu_t "彼らが目を覚まさなかったことは良いことです。そうでなければ、とても恥ずかしいでしょう。"

# game/script.rpy:2933
translate japanese after_minigame_4_3ce1deba:

    # toshu_t "I should go back to sleep…"
    toshu_t "もうひと眠りしなきゃ…"
