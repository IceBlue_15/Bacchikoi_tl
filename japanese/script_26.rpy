# game/script.rpy:10602
translate japanese ending_ine_mpe_8026a9ab:

    toshu_t "It's been several weeks since Ichiru's transfer."
    toshu_t "Ichiruの移籍から数週間が経ちました。"

# game/script.rpy:10603
translate japanese ending_ine_mpe_72be11b9:

    toshu_t "Masaru stood always by my side. We've been hanging out more than usual!"
    toshu_t "マサルはいつも私の側に立っていた。私たちはいつも以上にぶらぶらしていました！"

# game/script.rpy:10611
translate japanese ending_ine_mpe_de2af54b:

    toshu "Fuwaahh… It's weekend again… I'm so bored…"
    toshu "Fuwaahh …もう週末だよ…私はとても退屈だ。"

# game/script.rpy:10612
translate japanese ending_ine_mpe_1f4978a2:

    toshu "I don't have anything to do…"
    toshu "私は何もする必要はありません…"

# game/script.rpy:10614
translate japanese ending_ine_mpe_bc436732:

    toshu "I wish Masaru is with me everyday. It has really been fun hanging out together with him!"
    toshu "私は大丸が毎日私と一緒にいられることを願っています。彼と一緒に遊ぶのは本当に面白かったです！"

# game/script.rpy:10617
translate japanese ending_ine_mpe_1e44abec:

    toshu "Hmm? I thought auntie went to work today?"
    toshu "うーん？今日はおばさんが仕事に行くと思った？"

# game/script.rpy:10619
translate japanese ending_ine_mpe_eda2bd2f:

    masaru "Hi there, Toshu!"
    masaru "Hi there, Toshu!"

# game/script.rpy:10621
translate japanese ending_ine_mpe_f5b99a19:

    toshu "M-Masaru!"
    toshu "マサール！"

# game/script.rpy:10623
translate japanese ending_ine_mpe_119e746b:

    masaru "I know we were just hanging out yesterday…"
    masaru "私たちが昨日立ち去っていたことは分かっています…"

# game/script.rpy:10625
translate japanese ending_ine_mpe_6b2d05b8:

    masaru "But I already missed you, I couldn't help but go here."
    masaru "しかし、私はすでにあなたを見逃しました、私は助けることができなかったが、ここに行く。"

# game/script.rpy:10627
translate japanese ending_ine_mpe_ffe12305:

    toshu "T-That's so sweet, Masaru!"
    toshu "名前：T-それはとても甘い、元大！"

# game/script.rpy:10629
translate japanese ending_ine_mpe_0f420d0c:

    toshu "In fact… I was thinking about you a while ago…"
    toshu "実際に…私はずっと前にあなたについて考えていた…"

# game/script.rpy:10631
translate japanese ending_ine_mpe_c1168ccf:

    masaru "R-Really?"
    masaru "R-本当ですか？"

# game/script.rpy:10633
translate japanese ending_ine_mpe_aac4823e:

    toshu "Y-yes! I really love Masaru's company!"
    toshu "Y-はい！私は本当にマサルの会社が大好き！"

# game/script.rpy:10635
translate japanese ending_ine_mpe_f916d35e:

    toshu "By the way, how's your arm?"
    toshu "ところで、あなたの腕はどうですか？"

# game/script.rpy:10637
translate japanese ending_ine_mpe_15bea4fd:

    masaru "It's been healing very well!"
    masaru "それはとてもうまく治っています！"

# game/script.rpy:10638
translate japanese ending_ine_mpe_a9facc95:

    masaru "I can play the piano again, but not at full yet."
    masaru "私はピアノをもう一度弾くことができるが、まだ完全ではない。"

# game/script.rpy:10640
translate japanese ending_ine_mpe_643f828b:

    toshu "Oh, I wish it would heal soon! I really miss you playing the piano…!"
    toshu "ああ、私はそれがすぐに治癒したい！私はあなたがピアノを弾くのを本当に忘れています…！"

# game/script.rpy:10642
translate japanese ending_ine_mpe_1a3dfcf5:

    masaru "Thank you, Toshu!"
    masaru "Thank you, Toshu!"

# game/script.rpy:10644
translate japanese ending_ine_mpe_a2bfa5e6:

    masaru "A-Actually, I came over to tell you something…"
    masaru "A：実際に、私はあなたに何かを伝えるために来た…"

# game/script.rpy:10646
translate japanese ending_ine_mpe_538a2d3c:

    masaru "I guess this is the right time to say it."
    masaru "私はこれがそれを言う正しい時だと思う。"

# game/script.rpy:10648
translate japanese ending_ine_mpe_73f0caf2:

    toshu "W-what is it, Masaru?"
    toshu "名前：W-何ですか、優ですか？"

# game/script.rpy:10650
translate japanese ending_ine_mpe_4814dd79:

    masaru "I've been thinking about what you told to me way back from the tournament…"
    masaru "私はあなたがトーナメントから帰ってきた方法について考えてきました…"

# game/script.rpy:10652
translate japanese ending_ine_mpe_4a5272b6:

    toshu "Ahh…"
    toshu "ああ…"

# game/script.rpy:10655
translate japanese ending_ine_mpe_0a01e5d1:

    masaru "Yeah… you said 'I love you' to me."
    masaru "ええ…あなたは私を愛しています。"

# game/script.rpy:10657
translate japanese ending_ine_mpe_efcd6231:

    toshu "W-wah!"
    toshu "Wワ！"

# game/script.rpy:10660
translate japanese ending_ine_mpe_b9ed0dfb:

    masaru "Well, I never got the chance to properly respond…"
    masaru "まあ、私は適切に対応するチャンスを得ていません…"

# game/script.rpy:10661
translate japanese ending_ine_mpe_fa38f7e0:

    masaru "I wanted to say that…"
    masaru "私はそれを言いたかった…"

# game/script.rpy:10663
translate japanese ending_ine_mpe_2b372ce5:

    masaru "I love you too, Toshu."
    masaru "私もあなたも大好きです。"

# game/script.rpy:10665
translate japanese ending_ine_mpe_42858303:

    toshu "M-MASARU!!!"
    toshu "M-MASARU !!!"

# game/script.rpy:10667
translate japanese ending_ine_mpe_a4c7167b:

    masaru "I really do love you! I don't know what to do with my everyday without you."
    masaru "私は本当にあなたを愛しています！私はあなたなしで私の毎日と何をすべきかわかりません。"

# game/script.rpy:10669
translate japanese ending_ine_mpe_560945f0:

    masaru "I…"
    masaru "私…"

# game/script.rpy:10671
translate japanese ending_ine_mpe_e6ab3332:

    masaru "I want to prove it to you…!"
    masaru "私はあなたにそれを証明したい…！"

# game/script.rpy:10673
translate japanese ending_ine_mpe_0bec896b:

    toshu "P-prove?"
    toshu "P-証明？"

# game/script.rpy:10675
translate japanese ending_ine_mpe_666b06a5:

    toshu_t "Once again, I suddenly found myself under Masaru's gentle touch…"
    toshu_t "もう一度、私は突然Masaruの優しいタッチの下に自分自身を見つけた…"

# game/script.rpy:10676
translate japanese ending_ine_mpe_7b41a993:

    toshu_t "He took off his clothes and then mine…"
    toshu_t "彼は服を脱いだ後、私の服を着た。"

# game/script.rpy:10677
translate japanese ending_ine_mpe_a25a83e3:

    toshu_t "He put me on top of him… and held my hand."
    toshu_t "彼は私を彼の上に置き、私の手を握った。"

# game/script.rpy:10678
translate japanese ending_ine_mpe_1c6bb0c9:

    toshu_t "After a sweet kiss… I knew what was coming next."
    toshu_t "甘いキスの後…私は次に来るものを知っていた。"

# game/script.rpy:10683
translate japanese ending_ine_mpe_e65213e8:

    toshu "M-Masaru…"
    toshu "M-Masaru …"

# game/script.rpy:10684
translate japanese ending_ine_mpe_102e0675:

    toshu "I'm embarrassed…"
    toshu "私は恥ずかしい…"

# game/script.rpy:10685
translate japanese ending_ine_mpe_0286927f:

    masaru "We've done this plenty of times, Toshu."
    masaru "私たちはこれをたくさんやってきました。"

# game/script.rpy:10686
translate japanese ending_ine_mpe_1fd2d02f:

    toshu_t "He's right… we've done it secretly so many times before…"
    toshu_t "彼はそうです…私たちはこれまでずっと秘密裏にそれをしてきました…"

# game/script.rpy:10687
translate japanese ending_ine_mpe_237447cf:

    toshu_t "It reminds me of all the great times we have spent together…"
    toshu_t "それは一緒に過ごしたすばらしい時代を私に思い出させます…"

# game/script.rpy:10688
translate japanese ending_ine_mpe_5ef1ea98:

    masaru "Just relax…"
    masaru "ただリラックス…"

# game/script.rpy:10689
translate japanese ending_ine_mpe_75ea5f88:

    masaru "I'll be gentle…"
    masaru "私は優しいよ…"

# game/script.rpy:10691
translate japanese ending_ine_mpe_ac572b06:

    toshu "Waahh…"
    toshu "ワハ…"

# game/script.rpy:10692
translate japanese ending_ine_mpe_a4009deb:

    masaru "Does it hurt, Toshu?"
    masaru "Toshu？それは痛いですか？"

# game/script.rpy:10693
translate japanese ending_ine_mpe_76d8e01d:

    toshu "Y-yeah… just a bit…"
    toshu "Y-yeah …ちょっと…"

# game/script.rpy:10694
translate japanese ending_ine_mpe_132ff2f8:

    toshu_t "I tried to recollect in my mind the feeling before when Masaru entered me…"
    toshu_t "マサルが私に入った前の気分を思い出してみました…"

# game/script.rpy:10695
translate japanese ending_ine_mpe_691a3c02:

    toshu_t "To be doing this with such a handsome and perfect guy like him… I feel really special!"
    toshu_t "彼のようなハンサムで完璧な男とこれをやっている…私は本当に特別な気がする！"

# game/script.rpy:10696
translate japanese ending_ine_mpe_cf32bcbc:

    masaru "You're really hot inside…"
    masaru "あなたは本当に暑いです…"

# game/script.rpy:10698
translate japanese ending_ine_mpe_d4f541c2:

    toshu "Nghh…!"
    toshu "Nghh …！"

# game/script.rpy:10699
translate japanese ending_ine_mpe_44837626:

    masaru "Ahh… so tight…"
    masaru "ああ…そんなにタイトで…"

# game/script.rpy:10700
translate japanese ending_ine_mpe_e65213e8_1:

    toshu "M-Masaru…"
    toshu "M-Masaru …"

# game/script.rpy:10702
translate japanese ending_ine_mpe_f113fd16:

    toshu_t "I can feel Masaru's chest breathing heavily… his heart is almost pounding out of his chest."
    toshu_t "私は心の胸が大声で呼吸を感じることができます…彼の心は彼の胸の中でほとんど叩かれています。"

# game/script.rpy:10703
translate japanese ending_ine_mpe_33504c5d:

    masaru "Toshu…"
    masaru "Toshu…"

# game/script.rpy:10704
translate japanese ending_ine_mpe_7b0f1da9:

    toshu "I'm going to come…!"
    toshu "私は来るつもりだ…！"

# game/script.rpy:10706
translate japanese ending_ine_mpe_587c0c24:

    toshu "Wahh!"
    toshu "ワー！"

# game/script.rpy:10707
translate japanese ending_ine_mpe_88741280:

    toshu_t "I could not expect less from Masaru's abundance. He filled me plenty inside that it instantly made me cum as well."
    toshu_t "私は元羅の豊かさからあまり期待できませんでした。彼は私の中にたくさんのものを詰め込み、即座に私を兼ねさせました。"

# game/script.rpy:10708
translate japanese ending_ine_mpe_15d3a941:

    masaru "Hnn!"
    masaru "Hnn！"

# game/script.rpy:10710
translate japanese ending_ine_mpe_751404a4:

    toshu "Ahhh…"
    toshu "ああ…"

# game/script.rpy:10711
translate japanese ending_ine_mpe_0f5930f5:

    masaru "Haa… that felt really great…"
    masaru "ハア…それは本当に素晴らしい感じ…"

# game/script.rpy:10712
translate japanese ending_ine_mpe_84cffc3f:

    masaru "Of all the times we did this… I can finally do it with you without anything to worry about…"
    masaru "いつも私たちはこれをやった…私は最終的に心配することなくあなたとそれをすることができます…"

# game/script.rpy:10713
translate japanese ending_ine_mpe_3317fb34:

    toshu "Y-yeah Masaru… I've been waiting for this moment…"
    toshu "Y-yeah Masaru …私はこの瞬間を待っていました…"

# game/script.rpy:10714
translate japanese ending_ine_mpe_2197d09b:

    toshu_t "Even after releasing so much, Masaru's still standing up…!"
    toshu_t "あまりにも多くをリリースした後でさえ、元帥はまだ立っている…！"

# game/script.rpy:10715
translate japanese ending_ine_mpe_0c086752:

    masaru "I really want to show you how much I love you…!"
    masaru "私は本当にあなたがどれくらいあなたを愛しているかを見せています…！"

# game/script.rpy:10717
translate japanese ending_ine_mpe_4def7aba:

    toshu "A-aah! W-wait!"
    toshu "ああ！ W待って！"

# game/script.rpy:10718
translate japanese ending_ine_mpe_cfcd647c:

    masaru "Nnhh…"
    masaru "Nnhh …"

# game/script.rpy:10719
translate japanese ending_ine_mpe_971340bd:

    masaru "You're hard again… I'm glad you're feeling good…"
    masaru "あなたは再び大変です…あなたが気分がいいとうれしいです…"

# game/script.rpy:10720
translate japanese ending_ine_mpe_6ebe351b:

    toshu "M-Masaru… slow down…!"
    toshu "Mマサル…ゆっくり…！"

# game/script.rpy:10722
translate japanese ending_ine_mpe_4666e774:

    masaru "I really love you, Toshu!"
    masaru "私は本当にあなたを愛しています！"

# game/script.rpy:10723
translate japanese ending_ine_mpe_d5083c20:

    toshu "Uwaaah!!!"
    toshu "ウワア！"

# game/script.rpy:10724
translate japanese ending_ine_mpe_e600a118:

    toshu_t "As Masaru kept thrusting with all his strength, his voice was the contrast of it."
    toshu_t "Masaruは力をつけて追い続けるので、彼の声はそれのコントラストでした。"

# game/script.rpy:10725
translate japanese ending_ine_mpe_29f235d4:

    toshu_t "His moans are full of sincerity and affection…"
    toshu_t "彼の嘆きは誠実さと愛情でいっぱいです…"

# game/script.rpy:10726
translate japanese ending_ine_mpe_59411e4c:

    toshu_t "He kept whispering to my ear…"
    toshu_t "彼は私の耳にささやき続けた…"

# game/script.rpy:10727
translate japanese ending_ine_mpe_b7eb257a:

    masaru "Toshu… I love you…"
    masaru "Toshu… I love you…"

# game/script.rpy:10729
translate japanese ending_ine_mpe_406ec721:

    toshu "Masaru…! I love you too!!!"
    toshu "マサル…！私もあなたを愛してます！！！"

# game/script.rpy:10730
translate japanese ending_ine_mpe_cdc3abda:

    masaru "Nghh!!!"
    masaru "Nghh !!!"

# game/script.rpy:10731
translate japanese ending_ine_mpe_bbd0b2be:

    toshu_t "By that moment, Masaru pounded my entry in a frenzied pace."
    toshu_t "その瞬間、マサルは熱狂的なペースでエントリーを叩きました。"

# game/script.rpy:10732
translate japanese ending_ine_mpe_bdaaa6d5:

    toshu_t "He clenched my hand with even more force, and grasped my leg as if his arm was not injured at all."
    toshu_t "彼は私の手をさらに強く握り締め、腕がまったく負傷していないかのように脚をつかんだ。"

# game/script.rpy:10733
translate japanese ending_ine_mpe_deb7da0f:

    toshu_t "His member felt like it was growing even bigger inside me… he's about to reach his limit."
    toshu_t "彼のメンバーは、私の中でさらに大きく成長しているように感じました…彼は限界に近づいています。"

# game/script.rpy:10734
translate japanese ending_ine_mpe_077a6a58:

    masaru "I'm coming…!!!"
    masaru "今行ってる…！！！"

# game/script.rpy:10736
translate japanese ending_ine_mpe_d89e1aa8:

    toshu "Masaru…!!"
    toshu "Masaru…!!"

# game/script.rpy:10737
translate japanese ending_ine_mpe_c2eaff51:

    masaru "Toshu!!!"
    masaru "Toshu!!!"

# game/script.rpy:10739
translate japanese ending_ine_mpe_4c416665:

    toshu "Aaah… that felt really great…"
    toshu "ああ…それは本当に素晴らしい感じ…"

# game/script.rpy:10740
translate japanese ending_ine_mpe_f6dcd9ce:

    masaru "I'm glad you enjoyed it that much."
    masaru "あなたはそれを楽しんでうれしいです。"

# game/script.rpy:10742
translate japanese ending_ine_mpe_ecd62e22:

    toshu "Masaru is really the best!"
    toshu "マサルは本当に最高です！"

# game/script.rpy:10743
translate japanese ending_ine_mpe_84f2d336:

    masaru "I love you, Toshu!"
    masaru "I love you, Toshu!"

# game/script.rpy:10744
translate japanese ending_ine_mpe_da8b218c:

    toshu "I love you too, Masaru!"
    toshu "あなたも大好き！"

# game/script.rpy:10748
translate japanese ending_ine_mpe_a8a93c12:

    toshu_t "Masaru has officially become my boyfriend after that!"
    toshu_t "マサルはその後正式に私のボーイフレンドになった！"

# game/script.rpy:10749
translate japanese ending_ine_mpe_97e84569:

    toshu_t "My transfer to Yakyusha brought me to making such great friends!"
    toshu_t "ヤクシュサへの私の移転は私をこのような偉大な友人にしてくれました！"

# game/script.rpy:10750
translate japanese ending_ine_mpe_ce5a33af:

    toshu_t "But the biggest thing I'm grateful for…"
    toshu_t "しかし、私が感謝している最大のこと…"

# game/script.rpy:10751
translate japanese ending_ine_mpe_2c2040a8:

    toshu_t "…is meeting the special person who struck my heart!"
    toshu_t "…私の心を打つ特別な人に会うのです！"
