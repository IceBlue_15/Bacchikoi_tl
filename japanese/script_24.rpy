# game/script.rpy:10400
translate japanese ending_ine_mpe_4760d0aa:

    toshu_t "The weekend has passed after that intense tournament."
    toshu_t "その激しいトーナメントの後、週末が過ぎました。"

# game/script.rpy:10401
translate japanese ending_ine_mpe_1aa1bffb:

    toshu_t "I want to know how my friends have been doing…"
    toshu_t "私の友達がどうしているのか知りたい"

# game/script.rpy:10409
translate japanese ending_ine_mpe_2d78b941:

    toshu "I… it's pretty quiet around…"
    toshu "私は…周りはかなり静かです…"

# game/script.rpy:10411
translate japanese ending_ine_mpe_b65efb71:

    toshu "It totally feels different without Ichiru or Masaru…"
    toshu "イチールやマサルなしで全く違う感じ…"

# game/script.rpy:10412
translate japanese ending_ine_mpe_548b7b6a:

    toshu "Where is everyone…?"
    toshu "みんなはどこですか…？"

# game/script.rpy:10414
translate japanese ending_ine_mpe_631ec800:

    toshu "Wait! Isn't that…!"
    toshu "待つ！それじゃない…！"

# game/script.rpy:10416
translate japanese ending_ine_mpe_419c0495:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:10419
translate japanese ending_ine_mpe_fc5cc731:

    toshu "I-Ichiru!"
    toshu "I-Ichiru!"

# game/script.rpy:10421
translate japanese ending_ine_mpe_d0beb068:

    ichiru "T-Toshu…"
    ichiru "T-Toshu…"

# game/script.rpy:10422
translate japanese ending_ine_mpe_2d21d20b:

    toshu "W-what's wrong, Ichiru?"
    toshu "名前：W何が間違っている、Ichiru？"

# game/script.rpy:10424
translate japanese ending_ine_mpe_d78baa59:

    ichiru "*sniff*"
    ichiru "*スニフ*"

# game/script.rpy:10426
translate japanese ending_ine_mpe_36fbd142:

    toshu "Why are you crying?!"
    toshu "なぜ泣いているのですか？"

# game/script.rpy:10427
translate japanese ending_ine_mpe_5539909d:

    toshu "W-what happened?"
    toshu "W-何が起こったの？"

# game/script.rpy:10429
translate japanese ending_ine_mpe_00ec623d:

    ichiru "I'm sorry, TOSHU!"
    ichiru "I'm sorry, TOSHU!"

# game/script.rpy:10430
translate japanese ending_ine_mpe_e54a10f9:

    ichiru "I failed you!"
    ichiru "私はあなたに失敗しました！"

# game/script.rpy:10432
translate japanese ending_ine_mpe_c5577f0c:

    ichiru "M-my transfer… dad's going to continue it…"
    ichiru "私の移籍…父親はそれを続けるつもりです…"

# game/script.rpy:10433
translate japanese ending_ine_mpe_71a46452:

    ichiru "Whether I like it or not…"
    ichiru "私が好きかどうかは…"

# game/script.rpy:10435
translate japanese ending_ine_mpe_f6d56761:

    toshu "Wh-What?! What do you mean?!"
    toshu "ホワ - 何？どういう意味ですか？！"

# game/script.rpy:10437
translate japanese ending_ine_mpe_3f3d31ae:

    toshu "I thought if your dad--"
    toshu "あなたのお父さんが"

# game/script.rpy:10439
translate japanese ending_ine_mpe_ae81368e:

    ichiru "I know!"
    ichiru "知っている！"

# game/script.rpy:10440
translate japanese ending_ine_mpe_ae2e208e:

    ichiru "Tomoka's dad rejected my dad's request for reconsideration…"
    ichiru "トモカさんのお父さんが私のお父さんの再審査請求を拒否しました…"

# game/script.rpy:10441
translate japanese ending_ine_mpe_b583d29e:

    ichiru "He threathened my dad by removing him from business partnership if he complained more…"
    ichiru "彼はもっと不平を言うなら、ビジネスパートナーシップから彼を取り除くことによって、私の父を脅かす…"

# game/script.rpy:10443
translate japanese ending_ine_mpe_4506cb60:

    toshu "I… I c-can't believe this…"
    toshu "私は…信じられない"

# game/script.rpy:10445
translate japanese ending_ine_mpe_018b031b:

    ichiru "I am as devastated as you, Toshu!"
    ichiru "私はあなたと同じくらい徹底している！"

# game/script.rpy:10446
translate japanese ending_ine_mpe_b7f26f9f:

    ichiru "I never wanted to leave you guys…"
    ichiru "私はあなたたちを去ることは決してなかった…"

# game/script.rpy:10447
translate japanese ending_ine_mpe_6a6e2efd:

    ichiru "I'm so sorry…"
    ichiru "ごめんなさい…"

# game/script.rpy:10449
translate japanese ending_ine_mpe_06f86a82:

    ichiru "I… have to go…!"
    ichiru "私が行かなければならない…！"

# game/script.rpy:10451
translate japanese ending_ine_mpe_57e39c08:

    toshu_t "I couldn't believe what Ichiru said…"
    toshu_t "Ichiruが言ったことを信じられませんでした…"

# game/script.rpy:10452
translate japanese ending_ine_mpe_4fe85f52:

    toshu_t "Ichiru… my dear friend…"
    toshu_t "Ichiru …親愛なる友人…"

# game/script.rpy:10453
translate japanese ending_ine_mpe_a9fb501a:

    toshu_t "We've been through a lot…"
    toshu_t "私たちはずっと経験してきました…"

# game/script.rpy:10454
translate japanese ending_ine_mpe_bf8b3264:

    toshu_t "Why does he still have to leave…?"
    toshu_t "なぜ彼はまだ放置しなければならないのですか？"

# game/script.rpy:10461
translate japanese ending_ine_mpe_aa5abefd:

    masaru "Toshu…!"
    masaru "Toshu…!"

# game/script.rpy:10463
translate japanese ending_ine_mpe_5be77f88:

    toshu "Masaru! Is it true that Ichiru will leave?!"
    toshu "マサル！ Ichiruが残すのは本当ですか？"

# game/script.rpy:10465
translate japanese ending_ine_mpe_e191807b:

    toshu "Please tell me that it isn't true!"
    toshu "それは本当ではないことを教えてください！"

# game/script.rpy:10467
translate japanese ending_ine_mpe_be0c99b0:

    masaru "I'm sorry, Toshu…"
    masaru "I'm sorry, Toshu…"

# game/script.rpy:10468
translate japanese ending_ine_mpe_787bb622:

    masaru "But I'm afraid he's telling the truth."
    masaru "しかし、私は彼が真実を伝えているのではないかと心配しています。"

# game/script.rpy:10470
translate japanese ending_ine_mpe_364f49a2:

    # toshu "…"
    toshu "…"

# game/script.rpy:10472
translate japanese ending_ine_mpe_5bbe9679:

    masaru "There's really nothing we could've done…"
    masaru "本当に何もできなかった…"

# game/script.rpy:10473
translate japanese ending_ine_mpe_c7f13db6:

    masaru "I'm pretty sure Ichiru tried his best to argue with his dad about it…"
    masaru "Ichiruが父親と議論するために最善を尽くすと確信しています…"

# game/script.rpy:10475
translate japanese ending_ine_mpe_3b8ccfab:

    toshu "You're right, Masaru… I'm sure nobody wanted this…"
    toshu "あなたは大丈夫だ、まる…誰もこれを望んでいないと確信している…"

# game/script.rpy:10477
translate japanese ending_ine_mpe_5bf3649e:

    masaru "Please don't be sad anymore!"
    masaru "もう悲しくならないでください！"

# game/script.rpy:10479
translate japanese ending_ine_mpe_ffbb2b47:

    masaru "I'll be always here for you!"
    masaru "私はいつもここにいるよ！"

# game/script.rpy:10481
translate japanese ending_ine_mpe_50903839:

    toshu "Masaru…"
    toshu "Masaru…"

# game/script.rpy:10483
translate japanese ending_ine_mpe_b6fab263:

    masaru "Hey, I actually aced my remedial exam! Thanks to you and Ichiru!"
    masaru "ちょっと、私は実際に私の修復試験に挑戦しました！あなたとIchiruのおかげで！"

# game/script.rpy:10485
translate japanese ending_ine_mpe_d96021e9:

    toshu "W-whaa! Really?!"
    toshu "W-ワア！本当に？！"

# game/script.rpy:10487
translate japanese ending_ine_mpe_e531d7af:

    masaru "I'm happy to say that I'm really getting back on track!"
    masaru "私は本当に戻ってきていると言ってうれしいです！"

# game/script.rpy:10489
translate japanese ending_ine_mpe_282e2748:

    masaru "And thanks to Coach Genji's support, he negotiated with the music club to accept me back!"
    masaru "そしてGenji長の支援のおかげで、彼は私を受け入れるために音楽クラブと交渉しました！"

# game/script.rpy:10491
translate japanese ending_ine_mpe_57227e95:

    masaru "I can finally play that piano again!"
    masaru "ついにピアノを弾くことができます！"

# game/script.rpy:10493
translate japanese ending_ine_mpe_53994499:

    toshu "That's really great news, Masaru!!!"
    toshu "本当に素晴らしいニュースです。"

# game/script.rpy:10495
translate japanese ending_ine_mpe_2e80f0e5:

    masaru "I'm sure Ichiru doesn't want us to be sad…"
    masaru "Ichiruは私たちが悲しいことを望んでいないと確信しています…"

# game/script.rpy:10497
translate japanese ending_ine_mpe_e5f4f23c:

    masaru "So let's try our best to cheer up!"
    masaru "さあ、元気に応援しよう！"

# game/script.rpy:10498
translate japanese ending_ine_mpe_f5b48e53:

    masaru "I'm sure we can all still hangout together even if he transfers!"
    masaru "彼が移籍しても、私たちはいつも一緒にハングアウトすることができると確信しています！"

# game/script.rpy:10500
translate japanese ending_ine_mpe_3c1a467c:

    toshu_t "Masaru's words were exactly the things I needed to hear!"
    toshu_t "マサルの言葉は、私が聴く必要があったものでした！"

# game/script.rpy:10501
translate japanese ending_ine_mpe_9aa41081:

    toshu_t "He sure knows what to say to cheer me up!"
    toshu_t "彼は私を応援するために何を言うべきかを知っている！"

# game/script.rpy:10502
translate japanese ending_ine_mpe_bb5034d6:

    toshu_t "I am really looking forward to meeting Ichiru soon."
    toshu_t "まもなくIchiruに会うことを本当に楽しみにしています。"

# game/script.rpy:10503
translate japanese ending_ine_mpe_bce8330c:

    toshu_t "I will totally cherish the time I have with Masaru!"
    toshu_t "私はまると一緒にいる時間を大切にします！"
