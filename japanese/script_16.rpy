# game/script.rpy:9220
translate japanese after_minigame_9_b9b4ff58:

    toshu_t "Today's the big day!"
    toshu_t "今日は大きな一日！"

# game/script.rpy:9221
translate japanese after_minigame_9_73bc502c:

    toshu_t "Winning or losing, having my friends by my side is what matters the most!"
    toshu_t "勝つか失った、私の側で私の友人を持っていることが最も重要なことです！"

# game/script.rpy:9222
translate japanese after_minigame_9_dad8d35c:

    toshu_t "We've been through a lot to get where we are right now…"
    toshu_t "私たちは現在、どこにいるのかをたくさん見てきました…"

# game/script.rpy:9223
translate japanese after_minigame_9_b6150ed7:

    toshu_t "I'll definitely do my best and make everyone proud!"
    toshu_t "私は間違いなく全力を尽くし、誰もが誇りに思うよ！"

# game/script.rpy:9224
translate japanese after_minigame_9_1acd9a3b:

    toshu_t "Everything I experienced up to this point."
    toshu_t "今まで私が経験したすべて。"

# game/script.rpy:9225
translate japanese after_minigame_9_9184c936:

    toshu_t "For all the friends I've met!"
    toshu_t "私が会ったすべての友達のために！"

# game/script.rpy:9226
translate japanese after_minigame_9_433e3caa:

    toshu_t "And for the hard work we've done these past months!"
    toshu_t "そして、私たちは過去数ヶ月にわたって頑張ってきました！"

# game/script.rpy:9227
translate japanese after_minigame_9_9ba40378:

    toshu_t "It's finally going to pay off!"
    toshu_t "ついに払うつもりです！"

# game/script.rpy:9233
translate japanese after_minigame_9_0cd11cfb:

    ichiru "Yoshaaaa!!! You guys, ready?!"
    ichiru "Yoshaaaa !!!あなたたち、準備！"

# game/script.rpy:9234
translate japanese after_minigame_9_3632e6c9:

    genji "This is it guys! This is what we trained hard for since last year!"
    genji "これはみんな！これは私たちが昨年から懸命に訓練したものです！"

# game/script.rpy:9235
translate japanese after_minigame_9_40283430:

    masaru "We really came a long way, haven't we?"
    masaru "私たちは本当に長い道のりを歩いたのですか？"

# game/script.rpy:9236
translate japanese after_minigame_9_510ab3cd:

    ichiru "Yeah! That's why we really all have to do our best!"
    ichiru "うん！だから私たちは本当に全力を尽くさなければなりません！"

# game/script.rpy:9237
translate japanese after_minigame_9_11d711c0:

    genji "Kanada! As our pitcher, we're really counting on you! Hahaha!"
    genji "カナディアン！私たちの投手として、私たちは本当にあなたを信じています！ははは！"

# game/script.rpy:9238
translate japanese after_minigame_9_464f58d8:

    toshu "Yes, Coach!"
    toshu "はい、コーチ！"

# game/script.rpy:9239
translate japanese after_minigame_9_0d30d1bd:

    masaru "So, is everyone ready?"
    masaru "だから皆は準備ができていますか？"

# game/script.rpy:9240
translate japanese after_minigame_9_ebe7d725:

    toshu "Ready!"
    toshu "準備ができました！"

# game/script.rpy:9241
translate japanese after_minigame_9_dbe9ff73:

    ichiru "YAKYUSHA TEAM, LET'S WIN THIS!!!"
    ichiru "YAKYUSHA TEAM、これで勝て！"

# game/script.rpy:9245
translate japanese after_minigame_9_561bde9d:

    narrator "Aaaand… play ball!"
    narrator "Aaaand …プレーボール！"

# game/script.rpy:9246
translate japanese after_minigame_9_276d6762:

    toshu "Okay! I have to focus!"
    toshu "はい！私は集中しなければならない！"

# game/script.rpy:9247
translate japanese after_minigame_9_57071e4c:

    toshu "Don't let the enemy intimidate you!"
    toshu "敵があなたを脅かさないようにしてください！"

# game/script.rpy:9248
translate japanese after_minigame_9_3c70a868:

    toshu "I just need to follow Masaru's Signs and I will be fine!"
    toshu "私はMasaruの看板に従うだけでいいです。"

# game/script.rpy:9250
translate japanese after_minigame_9_448bef42:

    narrator "STRIKE ONE!"
    narrator "ストライクワン！"

# game/script.rpy:9251
translate japanese after_minigame_9_9311733f:

    toshu "Hnn… okay! So far so good."
    toshu "Hnn …大丈夫です！ここまでは順調ですね。"

# game/script.rpy:9253
translate japanese after_minigame_9_63618df8:

    narrator "STRIKE TWO!"
    narrator "ストライクツー！"

# game/script.rpy:9254
translate japanese after_minigame_9_f7fd8ee9:

    masaru "Nice pitch, Toshu!"
    masaru "Nice pitch, Toshu!"

# game/script.rpy:9255
translate japanese after_minigame_9_66481751:

    toshu "One last strike and he's out!"
    toshu "1つの最後のストライキと彼は出ている！"

# game/script.rpy:9256
translate japanese after_minigame_9_62636e16:

    toshu "Okay! I got this!"
    toshu "はい！私はこれを得た！"

# game/script.rpy:9258
translate japanese after_minigame_9_942ba2d9:

    narrator "STRIKE THREE! YOU'RE OUT!!"
    narrator "ストライクスリー！あなたは外にいる！！"

# game/script.rpy:9259
translate japanese after_minigame_9_80c62844:

    toshu "One OUT!"
    toshu "1アウト！"

# game/script.rpy:9260
translate japanese after_minigame_9_0b779aaa:

    ichiru "Great job guys!"
    ichiru "素晴らしい職人！"

# game/script.rpy:9262
translate japanese after_minigame_9_e2d01c37:

    toshu_t "This was such a close fight."
    toshu_t "これはとても緊密な戦いだった。"

# game/script.rpy:9263
translate japanese after_minigame_9_186c7669:

    toshu_t "I didn't expect that our enemy would be as good as us!"
    toshu_t "私は敵が私たちと同じくらい良いとは思っていませんでした！"

# game/script.rpy:9267
translate japanese after_minigame_9_d0e66c8e:

    toshu_t "We ended up having a tie score at the final parts of the game…"
    toshu_t "我々はゲームの最後の部分で同点スコアを持つことに終わった…"

# game/script.rpy:9268
translate japanese after_minigame_9_186c7669_1:

    toshu_t "I didn't expect that our enemy would be as good as us!"
    toshu_t "私は敵が私たちと同じくらい良いとは思っていませんでした！"

# game/script.rpy:9269
translate japanese after_minigame_9_a72e0950:

    toshu_t "We're so close to victory…!"
    toshu_t "私たちは勝利にとても近い…！"

# game/script.rpy:9270
translate japanese after_minigame_9_fd809ecd:

    toshu_t "It's Ichiru's turn."
    toshu_t "Ichiruの番です。"

# game/script.rpy:9271
translate japanese after_minigame_9_b023edae:

    toshu_t "Please! Score for us Ichiru!"
    toshu_t "お願いします！私たちのために得点！"

# game/script.rpy:9273
translate japanese after_minigame_9_bebce3fb:

    toshu_t "As Ichiru walked to the mound getting ready to swing the bat…"
    toshu_t "Ichiruがバットを振る準備をしているマウンドに歩いていくと…"

# game/script.rpy:9274
translate japanese after_minigame_9_69c1ff0f:

    toshu_t "I could see in his eyes how serious he was."
    toshu_t "私は彼の目で彼がどれほど深刻であるかを見ることができました。"

# game/script.rpy:9275
translate japanese after_minigame_9_ff8188ff:

    toshu_t "For the very first time, Ichiru's eyes were full of determination and passion."
    toshu_t "初めて、Ichiruの目は決意と情熱に満ちていた。"

# game/script.rpy:9276
translate japanese after_minigame_9_e6c7cf47:

    toshu_t "As the enemy pitcher threw the ball… what happened next was expected from our cleanup hitter!"
    toshu_t "敵の投手がボールを投げたとき、次に起こったことはクリーンアップのヒッターから期待されていた！"

# game/script.rpy:9281
translate japanese after_minigame_9_ac68460c:

    narrator "HOOOMEEE RUUNN!!!"
    narrator "HOOOMEEE RUUNN !!!"

# game/script.rpy:9283
translate japanese after_minigame_9_0cf361ec:

    narrator "What a nice way to end a very close fight folks!"
    narrator "非常に近い戦いの人々を終わらせるためにどのような良い方法！"

# game/script.rpy:9284
translate japanese after_minigame_9_fd3d5734:

    narrator "The win goes to Yakyusha Academy!"
    narrator "ヤクシュサアカデミーに勝つ！"

# game/script.rpy:9294
translate japanese after_minigame_9_1ea02d7d:

    ichiru "Did you guys see that?!"
    ichiru "あなたたちはそれを見ましたか？"

# game/script.rpy:9295
translate japanese after_minigame_9_dc460085:

    toshu "I… I can't believe it! We won!"
    toshu "私は…信じられない！我々は勝った！"

# game/script.rpy:9296
translate japanese after_minigame_9_62ace142:

    genji "This is officially Yakyusha's first tournament win!"
    genji "これは公式には八百科初の大会優勝です！"

# game/script.rpy:9297
translate japanese after_minigame_9_fb05c2a9:

    genji "I am so proud of my boys!!!"
    genji "私は私の男の子をとても誇りに思います！"

# game/script.rpy:9298
translate japanese after_minigame_9_3d55720a:

    masaru "Let's celebrate!!!"
    masaru "さぁ祝おう！！！"

# game/script.rpy:9302
translate japanese after_minigame_9_fe7b52e0:

    ichiru "TOSHU!"
    ichiru "TOSHU!"

# game/script.rpy:9303
translate japanese after_minigame_9_f52251ed:

    toshu "I-Ichiru!!"
    toshu "I-Ichiru!!"

# game/script.rpy:9305
translate japanese after_minigame_9_33370fa7:

    toshu "Mnn…!"
    toshu "…目撃！"

# game/script.rpy:9307
translate japanese after_minigame_9_a151247b:

    ichiru "I told you I'd win for you…"
    ichiru "私はあなたに勝つと言った…"

# game/script.rpy:9309
translate japanese after_minigame_9_8e951ac7:

    toshu "Ichiru…"
    toshu "イチール…"

# game/script.rpy:9318
translate japanese after_minigame_9_4ecebc3b:

    genji "Woah there Ichiru!"
    genji "うん、そこにIchiru！"

# game/script.rpy:9320
translate japanese after_minigame_9_5d87fa80:

    ichiru "Why, Genji?! You jealous?!"
    ichiru "なぜ、Genji？あなたは嫉妬？"

# game/script.rpy:9322
translate japanese after_minigame_9_09ca876c:

    ichiru "Hahahaha!!!"
    ichiru "ハハハハ!!!"

# game/script.rpy:9323
translate japanese after_minigame_9_79191d71:

    toshu "Ichiru… you kissed me in front of Coach and Captain…"
    toshu "Ichiru …あなたはコーチとキャプテンの前で私にキスした…"

# game/script.rpy:9327
translate japanese after_minigame_9_9e622334:

    genji "Let's go to an all-you-can-eat dinner! MY TREAT!"
    genji "食べ放題のディナーに行きましょう！私の扱い！"

# game/script.rpy:9328
translate japanese after_minigame_9_90f9abb2:

    ichiru "YOSHAAAA!!! I won't refuse that, old man!"
    ichiru "YOSHAAAA !!!私はそれを拒否しない、老人！"

# game/script.rpy:9330
translate japanese after_minigame_9_fe7b52e0_1:

    ichiru "TOSHU!"
    ichiru "TOSHU!"

# game/script.rpy:9332
translate japanese after_minigame_9_667ad5db:

    ichiru "Why are you dazed off again! Let's go and celebr--"
    ichiru "なぜあなたは再び曇っているのですか？さあ、お祝いしましょう -"

# game/script.rpy:9335
translate japanese after_minigame_9_460c36d8:

    ichiru "Oh… j… just a second… my dad is calling…"
    ichiru "ああ… j …ちょっとだけ…私のお父さんが電話しています…"

# game/script.rpy:9338
translate japanese after_minigame_9_cb883b18:

    toshu "I-Ichiru wait!"
    toshu "I-Ichiru wait!"

# game/script.rpy:9347
translate japanese after_minigame_9_16c5dcc2:

    toshu "H…huh?"
    toshu "H …ねえ？"

# game/script.rpy:9348
translate japanese after_minigame_9_9e5fa883:

    toshu "W…what's wrong, Ichiru…?"
    toshu "W …何が間違っている、Ichiru…？"

# game/script.rpy:9370
translate japanese choice_17_A_107c3d46:

    toshu "Ichiru! Are you okay?"
    toshu "Ichiru! Are you okay?"

# game/script.rpy:9371
translate japanese choice_17_A_0675ec0e:

    toshu "What did your father say?"
    toshu "あなたの父は何を言ったのですか？"

# game/script.rpy:9373
translate japanese choice_17_A_118763b1:

    ichiru "Toshu…"
    ichiru "Toshu…"

# game/script.rpy:9375
translate japanese choice_17_A_04b2f796:

    ichiru "I'm…"
    ichiru "私は…"

# game/script.rpy:9379
translate japanese choice_17_A_0d5780e1:

    ichiru "I'M NOT GOING TO LEAVE!"
    ichiru "私は去るつもりはない！"

# game/script.rpy:9381
translate japanese choice_17_A_d51681ff:

    ichiru "My father finally talked to Mr. Saji!"
    ichiru "父はついに匙さんと話しました！"

# game/script.rpy:9382
translate japanese choice_17_A_cefc097b:

    ichiru "They won't force me to marry Tomoka, and I can stay in this school!!!"
    ichiru "彼らは私にトモカとの結婚を強制しないでしょう、そして、私はこの学校に留まることができます！"

# game/script.rpy:9384
translate japanese choice_17_A_a16b107a:

    toshu "T-that's perfect…!"
    toshu "T-それは完璧です…！"

# game/script.rpy:9385
translate japanese choice_17_A_71f56d15:

    ichiru "I'm so happy!!"
    ichiru "私はとても幸せだ！！"

# game/script.rpy:9386
translate japanese choice_17_A_81033f78:

    ichiru "I can always be with you!"
    ichiru "私はいつもあなたと一緒にいることができます！"

# game/script.rpy:9388
translate japanese choice_17_A_2aae31fc:

    ichiru "I LOVE YOU, Toshu!"
    ichiru "I LOVE YOU, Toshu!"

# game/script.rpy:9393
translate japanese choice_17_A_54e7910c:

    toshu_t "My ears rang when Ichiru called me by my name is such a blissful tone…"
    toshu_t "イチールが私の名前で私を呼んだとき、私の耳が鳴った。"

# game/script.rpy:9394
translate japanese choice_17_A_5c481cfb:

    toshu_t "Ichiru confessed to me… It made my heart skip a beat…"
    toshu_t "Ichiruは私に告白した…それは私の心がビートをスキップした…"

# game/script.rpy:9395
translate japanese choice_17_A_9a79210b:

    toshu_t "He then kissed me in the lips…"
    toshu_t "彼はその後、唇の中で私にキスした…"

# game/script.rpy:9396
translate japanese choice_17_A_258841a6:

    toshu_t "And said…"
    toshu_t "そして、言いました…"

# game/script.rpy:9397
translate japanese choice_17_A_6724828b:

    ichiru "I want to be with you forever…"
    ichiru "永遠にあなたと一緒にいたい…"

# game/script.rpy:9402
translate japanese choice_17_A_7420140e:

    toshu "W-wah…"
    toshu "Wワウ…"

# game/script.rpy:9403
translate japanese choice_17_A_8de399fd:

    ichiru "Are you ready, my Toshu?"
    ichiru "あなたは準備ができています、私のToshu？"

# game/script.rpy:9404
translate japanese choice_17_A_b80b8ead:

    toshu " Y-yes, Ichiru…"
    toshu " Y-yes, Ichiru…"

# game/script.rpy:9406
translate japanese choice_17_A_a18b854d:

    toshu "Uwaahh…"
    toshu "うわー…"

# game/script.rpy:9407
translate japanese choice_17_A_951dccb8:

    ichiru "Ooh…"
    ichiru "ああ…"

# game/script.rpy:9408
translate japanese choice_17_A_1978faa9:

    ichiru "You're really tight…"
    ichiru "あなたは本当にタイトです…"

# game/script.rpy:9409
translate japanese choice_17_A_8b960f39:

    toshu "Be gentle, Ichiru…"
    toshu "Be gentle, Ichiru…"

# game/script.rpy:9411
translate japanese choice_17_A_366e62eb:

    ichiru "I can't! You're too hot for me!"
    ichiru "私はできません！あなたは私のために暑すぎます！"

# game/script.rpy:9412
translate japanese choice_17_A_22af386e:

    toshu "I-Ichiru…!!"
    toshu "I-Ichiru…!!"

# game/script.rpy:9414
translate japanese choice_17_A_5b99ffed:

    toshu "Uwah!"
    toshu "うわー！"

# game/script.rpy:9415
translate japanese choice_17_A_92a814bb:

    ichiru "Aaah…!"
    ichiru "ああ…！"

# game/script.rpy:9417
translate japanese choice_17_A_b35a8991:

    ichiru "Haaa… haa…"
    ichiru "Haaa … haa …"

# game/script.rpy:9418
translate japanese choice_17_A_27420b95:

    toshu "That felt great…"
    toshu "それは素晴らしい気分だった…"

# game/script.rpy:9419
translate japanese choice_17_A_93cedc22:

    ichiru "I… I want more!"
    ichiru "私は…もっと欲しい！"

# game/script.rpy:9420
translate japanese choice_17_A_eeef17a8:

    toshu "M-me too…"
    toshu "私も…"

# game/script.rpy:9421
translate japanese choice_17_A_85b5cdde:

    ichiru "I… will go in again!"
    ichiru "私は…再び入ります！"

# game/script.rpy:9423
translate japanese choice_17_A_c01c9a62:

    ichiru "Ughh…"
    ichiru "うーん…"

# game/script.rpy:9424
translate japanese choice_17_A_ea5ca1b0:

    toshu "Huwahh…"
    toshu "フワフ…"

# game/script.rpy:9425
translate japanese choice_17_A_50bfea8c:

    ichiru "You're so sexy, Toshu…"
    ichiru "You're so sexy, Toshu…"

# game/script.rpy:9427
translate japanese choice_17_A_d87313da:

    ichiru "Ever since I first saw you, I was really attracted to you!"
    ichiru "私が初めてあなたを見たときから、本当にあなたに魅了されました！"

# game/script.rpy:9428
translate japanese choice_17_A_ad60133c:

    toshu "A-aahh… Ichiru…!"
    toshu "A-aahh… Ichiru…!"

# game/script.rpy:9429
translate japanese choice_17_A_f6502baf:

    ichiru "I knew right from the start that we'll be great friends!"
    ichiru "私は偉大な友人になることを当初から知っていました！"

# game/script.rpy:9431
translate japanese choice_17_A_25a549ca:

    toshu "Huwaaah…"
    toshu "Huaaah …"

# game/script.rpy:9432
translate japanese choice_17_A_592aac0d:

    ichiru "You're the reason why I want to change…"
    ichiru "あなたは私が変更したい理由です…"

# game/script.rpy:9433
translate japanese choice_17_A_61673f16:

    toshu "I-Ichiru…!"
    toshu "I-Ichiru…!"

# game/script.rpy:9434
translate japanese choice_17_A_0798a616:

    ichiru "I really… really… love you!"
    ichiru "私は本当に、本当にあなたを愛して！"

# game/script.rpy:9436
translate japanese choice_17_A_11a9a6ff:

    toshu "Aaahh!!!"
    toshu "ああ！"

# game/script.rpy:9437
translate japanese choice_17_A_2f53423e:

    ichiru "Ughh!"
    ichiru "うん！"

# game/script.rpy:9439
translate japanese choice_17_A_34cdc545:

    ichiru "You're really the best, Toshu…"
    ichiru "あなたは本当に最高です、Toshu …"

# game/script.rpy:9440
translate japanese choice_17_A_e2bc67f9:

    ichiru "I love you…"
    ichiru "わたしは、あなたを愛しています…"

# game/script.rpy:9441
translate japanese choice_17_A_e1e5086d:

    toshu "I…"
    toshu "私…"

# game/script.rpy:9442
translate japanese choice_17_A_3a3d1b5c:

    toshu "I love you too, Ichiru…"
    toshu "私もあなたを愛しています、イチル…"

# game/script.rpy:9444
translate japanese choice_17_A_ca545bcb:

    toshu_t "This pleasure isn't something new for me…"
    toshu_t "この喜びは私にとって新しいものではありません…"

# game/script.rpy:9445
translate japanese choice_17_A_4e577557:

    toshu_t "Being with Ichiru is the only thing I wanted the most after all…"
    toshu_t "結局一番欲しかったのはIchiruと一緒にいることだけです…"

# game/script.rpy:9456
translate japanese choice_17_B_22a185f9:

    masaru "What happened with him?"
    masaru "彼とは何が起こったのですか？"

# game/script.rpy:9458
translate japanese choice_17_B_c939cf76:

    toshu "It's probably his dad… I hope it's not bad news…"
    toshu "おそらく彼のお父さんでしょう…悪いニュースではないことを祈っています…"

# game/script.rpy:9460
translate japanese choice_17_B_f1633d36:

    genji "I'm not really sure if President Yanai would keep his word about not transferring Ichiru."
    genji "私は、柳井会長がIchiruを移さないという言葉を守るかどうかは分かりません。"

# game/script.rpy:9462
translate japanese choice_17_B_3eb54e01:

    genji "Ichiru's heir of the family business after all… I'm sure the president has plans for Ichiru."
    genji "結局のところ、家族の事業の相棒の一族の… Ichiruのための計画がある大統領は確信しています。"

# game/script.rpy:9464
translate japanese choice_17_B_49c6ae70:

    masaru "Even though we won the tournament…?"
    masaru "私たちはトーナメントに勝利しましたが…？"

# game/script.rpy:9465
translate japanese choice_17_B_c219c130:

    genji "Knowing the president… he has huge plans to continue to family's legacy…"
    genji "大統領を知っている…彼は家族の遺産に続ける巨大な計画を持っている…"

# game/script.rpy:9467
translate japanese choice_17_B_a92d545b:

    masaru "If that's the case… it would really break Ichiru's heart."
    masaru "それが事実なら…それは本当にイチールの心を壊すだろう。"

# game/script.rpy:9469
translate japanese choice_17_B_05ec92c4:

    toshu "Is that more important than Ichiru's own happiness…?"
    toshu "それはイチル自身の幸福よりも重要ですか？"

# game/script.rpy:9471
translate japanese choice_17_B_0dc08655:

    genji "Let's just hope for the best…"
    genji "ベストを祈ってみましょう…"

# game/script.rpy:9473
translate japanese choice_17_B_419c0495:

    # ichiru "…"
    ichiru "…"

# game/script.rpy:9477
translate japanese choice_17_B_107c3d46:

    toshu "Ichiru! Are you okay?"
    toshu "Ichiru! Are you okay?"

# game/script.rpy:9478
translate japanese choice_17_B_0675ec0e:

    toshu "What did your father say?"
    toshu "あなたの父は何を言ったのですか？"

# game/script.rpy:9480
translate japanese choice_17_B_eda559b4:

    ichiru "Toshu… *sniff*"
    ichiru "Toshu… *sniff*"

# game/script.rpy:9481
translate japanese choice_17_B_04b2f796:

    ichiru "I'm…"
    ichiru "私は…"

# game/script.rpy:9485
translate japanese choice_17_B_0d5780e1:

    ichiru "I'M NOT GOING TO LEAVE!"
    ichiru "私は去るつもりはない！"

# game/script.rpy:9487
translate japanese choice_17_B_d51681ff:

    ichiru "My father finally talked to Mr. Saji!"
    ichiru "父はついに匙さんと話しました！"

# game/script.rpy:9488
translate japanese choice_17_B_cefc097b:

    ichiru "They won't force me to marry Tomoka, and I can stay in this school!!!"
    ichiru "彼らは私にトモカとの結婚を強制しないでしょう、そして、私はこの学校に留まることができます！"

# game/script.rpy:9490
translate japanese choice_17_B_e1891616:

    toshu "T-That's perfect…!"
    toshu "T-それは完璧です…！"

# game/script.rpy:9492
translate japanese choice_17_B_95803056:

    ichiru "I'm so happy I won't have to be separated with you guys!!!"
    ichiru "私はとても幸せです。私は皆さんと分け合う必要はありません！"

# game/script.rpy:9494
translate japanese choice_17_B_fe6bf281:

    genji "I have a bad feeling about this…"
    genji "私はこれについて悪い気持ちを持っています…"

# game/script.rpy:9496
translate japanese choice_17_B_e450d17f:

    ichiru "Oh come on, GENJI! Don't be such a party-pooper!"
    ichiru "ああ、GENJI！そんなパーティーじゃないよ！"

# game/script.rpy:9498
translate japanese choice_17_B_dab29430:

    ichiru "Now's the time to celebrate!"
    ichiru "今はお祝いの時間です！"

# game/script.rpy:9499
translate japanese choice_17_B_f8ade2d7:

    ichiru "I'm really looking forward for that treat, ya know!?"
    ichiru "私は本当にその治療を楽しみにしている、あなたは知っている！"

# game/script.rpy:9501
translate japanese choice_17_B_63c487ad:

    masaru "Ichiru's right! We earned this victory!"
    masaru "イチルの権利！私たちはこの勝利を収めました！"

# game/script.rpy:9503
translate japanese choice_17_B_70b4428b:

    toshu "COACH! TREAT USSSSS!"
    toshu "コーチ！使用しています！"

# game/script.rpy:9505
translate japanese choice_17_B_6af2ca52:

    genji "Alright! Let's go then! I'll fill you all plenty!"
    genji "よかった！じゃあ、行きましょう！私はあなたのすべてを十分に満たします！"

# game/script.rpy:9507
translate japanese choice_17_B_d14c4c06:

    toshu_t "We earned this victory as a team!"
    toshu_t "我々はチームとしてこの勝利を得ました！"

# game/script.rpy:9508
translate japanese choice_17_B_8c3685e4:

    toshu_t "I'm glad Ichiru won't have to leave!"
    toshu_t "Ichiruは残す必要はないとうれしいよ！"

# game/script.rpy:9509
translate japanese choice_17_B_077557aa:

    toshu_t "All of our hardwork was worth it!!!"
    toshu_t "すべての私たちのハードワークはそれに値するものでした！"
